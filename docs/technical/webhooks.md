# Webhooks

[[toc]]

Configurations for defining webhooks for selected POST/PUT endpoints[^1]. When a request to these endpoints is successful, the corresponding webhook will be triggered, sending a payload to any matching webhook URL defined in the configuration. User information and the specific endpoint that triggered the webhook will also be added to the payload.

::: warning NOTE
Note that in order to trigger a webhook, the endpoint method must be decorated with the `@webhook` decorator defined in `ella/api/util/util.py`.
:::

## Configuration

Define what API endpoints should trigger a configurable webhook.

-   File: `ella_config.yml`
-   Key: `webhooks`

| Subkey     | Explanation                                                         | Values                 |
| :--------- | :------------------------------------------------------------------ | :--------------------- |
| `endpoint` | Regex pattern that matches the desired API endpoint(s).             | Regex pattern          |
| `hook`     | The webhook URL where the payload will be sent.                     | URL                    |
| `methods`  | The HTTP methods (e.g., POST, PUT) that should trigger the webhook. | [list of HTTP methods] |

### Example

```yaml
webhooks:
    - endpoint: "/api/v1/workflows/action/\d+/actions/finalize"
      hook: "http://my-server/api/notify-workflow-finalize"
      methods:
         - "POST"
```

::: tip TIP
The example config in the testdata will push all data to http://docker.host.internal:58888, so by running e.g. `nc -kldN 58888` on your localhost you can inspect all methods and payloads available for webhooks.
:::
