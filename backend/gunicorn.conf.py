import logging
import multiprocessing
import os

from gunicorn import glogging

from ella.applogger import InterceptHandler, setup_logger


class Logger(glogging.Logger):
    """
    Copied from https://github.com/jmgilman/loguricorn/tree/master

    Implements and overrides the gunicorn logging interface.

    This class inherits from the standard gunicorn logger and overrides it by
    replacing the handlers with `InterceptHandler` in order to route the
    gunicorn logs to loguru.
    """

    def __init__(self, cfg):
        super().__init__(cfg)
        logging.getLogger("gunicorn.error").handlers = [InterceptHandler()]
        logging.getLogger("gunicorn.access").handlers = [InterceptHandler()]
        # Set up log file and log level
        setup_logger()


bind = "0.0.0.0:8000"
backlog = 1024

workers = int(os.getenv("NUM_WORKERS") or multiprocessing.cpu_count() * 2 + 1)
daemon = False
# Logging is handled with logconfig (see applogger.py)
# https://docs.gunicorn.org/en/latest/settings.html#access-log-format
access_log_format = '%(p)s %(h)s %(l)s "%(r)s" %(s)s %(M)s %(b)s "%(f)s" "%(a)s"'
loglevel = os.getenv("LOGLEVEL", os.getenv("LOGLEVEL_CONSOLE", "INFO"))
accesslog = "-"
errorlog = "-"
logger_class = Logger
limit_request_line = 0
timeout = int(os.getenv("GUNICORN_TIMEOUT", 500))
reload = os.getenv("PRODUCTION", "").lower() == "false"
