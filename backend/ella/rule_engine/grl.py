import collections.abc
from collections import OrderedDict
from typing import Any

from .grm import GRM

"""
GenAP Rule Language, GRL

A JSON backed language for gene variant rules
"""


class GRL:

    """
    Parses a JSON object with n rules and returns a code keyed dict of lists of Rule objects
    """

    def parseRules(self, json_object: list[dict[str, Any]]):
        rules: OrderedDict[str, list[GRM.Rule]] = OrderedDict()
        for rule_block in json_object:
            code = rule_block["code"]
            rulespec = rule_block["rule"]
            rules.setdefault(code, []).append(self.parseRule(rulespec, code))
        return rules

    def parseRule(
        self,
        rulebody: collections.abc.Mapping | str | float,
        code: str | None,
        source: str | None = None,
        aggregate=False,
    ):
        if not isinstance(rulebody, collections.abc.Mapping):
            return GRM.InRule([rulebody], source, code)
        for left, right in rulebody.items():
            if left == "$or":
                return GRM.OrRule(
                    [self.parseRule(r, code, source, aggregate) for r in right], code, aggregate
                )
            elif left == "$and":
                return GRM.AndRule(
                    [self.parseRule(r, code, source, aggregate) for r in right], code, aggregate
                )
            elif left == "$in":
                return GRM.InRule(right, source, code)
            elif left == "$gt":
                return GRM.GtRule(right, source, code)
            elif left == "$lt":
                return GRM.LtRule(right, source, code)
            elif left == "$range":
                return GRM.RangeRule(right, source, code)
            elif left == "$not":
                return GRM.NotRule(self.parseRule(right, code, source, aggregate))
            elif left == "$all":
                return GRM.AllRule(right, source, code, aggregate)
            elif left == "$atleast":
                return GRM.AtLeastRule(right[1:], code, right[0], aggregate)
            elif left == "$$aggregate":
                return self.parseRule(right, code, source, True)
            else:
                # Left is a source
                return self.parseRule(right, code, left)
