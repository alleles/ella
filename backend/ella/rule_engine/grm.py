from __future__ import annotations

from typing import override


class GRM:
    """
    GenAP Rule Model, GRM. Base class for rules.
    """

    class Rule:
        """
        Applies this rule to some data. The return value is a list of rules which contributed
        to the result. Empty list means no match. The returned list can be used to inspect which
        data sources contributed to a match.
        """

        def __init__(
            self,
            value: list[str | float] | float | str | None,
            source: str | None,
            code: str | None = None,
            aggregate=False,
        ):
            self.value = value
            self.source = source
            self.match = value  # Holds the matching value. Default is same as value
            self.code = code
            self.aggregate = aggregate
            self.op: str | None = None

        @override
        def __eq__(self, other):
            return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

        @override
        def __hash__(self):
            return hash((self.source, self.value, self.code, self.aggregate))

        @override
        def __repr__(self):
            result = "<Rule "
            for k in ["code", "source", "value", "match", "aggregate"]:
                if hasattr(self, k):
                    result += k + ": " + str(getattr(self, k)) + " "
            result += ">"
            return result

        def query(self, data: list[str | float | int | None] | str | float):
            pass

    class InRule(Rule):
        """
        Evaluates to true if the data is contained in the value which is assumed to be a list
        """

        @override
        def query(self, data):
            datalist = (data,) if not isinstance(data, list) else data
            assert isinstance(self.value, list)
            self.match = list(set(datalist).intersection(set(self.value)))
            if self.match:
                return [self]
            else:
                return []

    class GtRule(Rule):
        """
        Evaluates to true if the data value applied is greater than this rule's value
        """

        # TODO consider making these also work if value is a named source, such as hi_frq_cutoff
        @override
        def query(self, data):
            assert not isinstance(self.value, list) and self.value is not None
            if float(data) > float(self.value):
                return [self]
            else:
                return []

    class LtRule(Rule):
        """
        Evaluates to true if the data value applied is less than this rule's value
        """

        # TODO consider making these also work if value is a named source, such as hi_frq_cutoff
        @override
        def query(self, data):
            assert not isinstance(self.value, list) and self.value is not None
            if float(data) < float(self.value):
                return [self]
            else:
                return []

    class RangeRule(Rule):
        """
        Evaluates to true if the data value applied is in the range (exclusive) indicated by
        this rule's value. The value is a list of 2 elements.
        """

        @override
        def query(self, data):
            assert isinstance(self.value, list)
            if data > self.value[0] and data < self.value[1]:
                return [self]
            else:
                return []

    class NotRule(Rule):
        def __init__(self, subrule: GRM.Rule):
            self.subrule = subrule
            self.value = None
            self.source = None
            self.code = None

        @override
        def query(self, data):
            subresult = self.subrule.query(data)
            if subresult:
                return False
            return [self.subrule]

    class CompositeRule(Rule):
        """
        Rules made up from other rules, for example oneRule AND anotherRule
        """

        def __init__(self, subrules: list[GRM.Rule], code: str | None = None, aggregate=False):
            self.subrules = subrules
            self.code = code
            self.aggregate = aggregate
            self.source = None

        @override
        def query(self, data):
            pass

    class AndRule(CompositeRule):
        @override
        def query(self, data):
            results = [rule.query(data) for rule in self.subrules]
            if all(results):
                # Explode list of lists
                return [rule for resultlist in results for rule in resultlist]
            else:
                return []

    class OrRule(CompositeRule):
        @override
        def query(self, data):
            results = [rule.query(data) for rule in self.subrules]
            if any(results):
                return [rule for resultlist in results for rule in resultlist]
            else:
                return []

    class AllRule(Rule):
        """
        Evaluates to true if the passed set of codes contain all of this rule's value
        """

        @override
        def query(self, data):
            # Support trailing wildcard, so write this out.
            assert isinstance(self.value, list)
            for code in set(self.value):
                codefound = False
                for datacode in set(data):
                    assert isinstance(code, str)
                    if code.endswith("*"):
                        if datacode.startswith(code[:-1]):
                            codefound = True
                    else:
                        if code == datacode:
                            codefound = True
                if not codefound:
                    return []
            return [self]

    class AtLeastRule(Rule):
        """
        Evaluates to true if the passed set of codes contains at least the given number (atleast)
        of this rule's value
        """

        def __init__(
            self, value: list[str | float], code: str | None, atleast: int, aggregate=False
        ):
            self.value = value
            self.code = code
            self.atleast = atleast
            self.aggregate = aggregate
            self.source = None

        @override
        def query(self, codes):
            # Support trailing wildcard, so write this out.
            nfound = 0
            assert isinstance(self.value, list)
            for code in set(self.value):
                for datacode in set(codes):
                    assert isinstance(code, str)
                    if code.endswith("*"):
                        if datacode.startswith(code[:-1]):
                            nfound += 1
                    else:
                        if code == datacode:
                            nfound += 1
            if nfound >= self.atleast:
                return [self]
            return []
