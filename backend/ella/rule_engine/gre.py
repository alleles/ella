from typing import Any

from .gra import GRA
from .grl import GRL


class GRE:

    """
    GenAP Rule Engine, GRE

    Main entry point for GRE. Capable of evaluating a dataset into lists of passed/notpassed ACMG
    codes using rules specified in for example GRL. Pass rules and data as JSON. Returns
    (passed, notpassed), each of which is a list rule objects.
    """

    def query(self, rules: list[dict[str, Any]], data: dict[str, Any]):
        return GRA().applyRules(GRL().parseRules(rules), GRA().parseNodeToSourceKeyedDict(data))
