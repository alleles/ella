import re
from collections.abc import Callable
from functools import reduce
from typing import Any, override


class ACMGClassifier2015:
    """
    GenAP Rule Classifier, GRC

    Creates the final ACMG classification based on result from the GRE rule engine.
    Uses regexp programs to represent code patterns.
    """

    # Regexp patterns
    PVS = re.compile("PVS.*")
    PS = re.compile("PS.*")
    PM = re.compile("PM.*")
    PP = re.compile("PP.*")
    BA = re.compile("BA.*")
    BS = re.compile("BS.*")
    BP = re.compile("BP.*")

    def classify(self, passed_codes: list[str]):
        """
        Call with a list of passed codes to get the correct ClassificationResult.
        """
        codes_by_precedence = self.normalize_codes(passed_codes)

        # Rules according to the official ACMG guidelines
        pathogenic = self.pathogenic(codes_by_precedence)
        likely_pathogenic = self.likely_pathogenic(codes_by_precedence)
        benign = self.benign(codes_by_precedence)
        likely_benign = self.likely_benign(codes_by_precedence)
        contradiction = self.contradict(codes_by_precedence)

        if contradiction:
            contributors = self.contradict(passed_codes)
            return ClassificationResult(3, "Uncertain significance", contributors, "Contradiction")
        if benign:
            contributors = self.benign(passed_codes)
            return ClassificationResult(1, "Benign", contributors, "Benign")
        if pathogenic:
            contributors = self.pathogenic(passed_codes)
            return ClassificationResult(5, "Pathogenic", contributors, "Pathogenic")
        if likely_pathogenic:
            contributors = self.likely_pathogenic(passed_codes)
            return ClassificationResult(4, "Likely pathogenic", contributors, "Likely pathogenic")
        if likely_benign:
            contributors = self.likely_benign(passed_codes)
            return ClassificationResult(2, "Likely benign", contributors, "Likely benign")
        return ClassificationResult(3, "Uncertain significance", [], "None")

    def pathogenic(self, codes: list[str]):
        """
        If the codes given satisfy the requirements for pathogenic, return list of all codes contributing, otherwise
        empty list.
        """
        return self._OR(
            # Not in original guidelines; added to fill logical gap
            self.contrib(self.PVS, codes, lambda n: n >= 2),
            self._AND(
                self.contrib(self.PVS, codes, lambda n: n == 1),
                (
                    self._OR(
                        self.contrib(self.PS, codes, lambda n: n >= 1),
                        # Modified from n >= 2 as per recommendation from Tavtigian (2018), ACGS (Ellard 2020) and CanVIG
                        self.contrib(self.PM, codes, lambda n: n >= 1),
                        # Removed, deprecated with above modification
                        # self._AND(
                        #     self.contrib(self.PM, codes, lambda n: n == 1),
                        #     self.contrib(self.PP, codes, lambda n: n == 1),
                        # ),
                        self.contrib(self.PP, codes, lambda n: n >= 2),
                    )
                ),
            ),
            self.contrib(self.PS, codes, lambda n: n >= 2),
            self._AND(
                self.contrib(self.PS, codes, lambda n: n == 1),
                self._OR(
                    self.contrib(self.PM, codes, lambda n: n >= 3),
                    self._AND(
                        self.contrib(self.PM, codes, lambda n: n == 2),
                        self.contrib(self.PP, codes, lambda n: n >= 2),
                    ),
                    self._AND(
                        self.contrib(self.PM, codes, lambda n: n == 1),
                        self.contrib(self.PP, codes, lambda n: n >= 4),
                    ),
                ),
            ),
        )

    def likely_pathogenic(self, codes: list[str]):
        """
        If the codes given satisfy the requirements for likely pathogenic, return list of all codes contributing, otherwise
        empty list.
        """
        return self._OR(
            self._AND(
                self.contrib(self.PVS, codes, lambda n: n == 1),
                # Removed as per recommendation from Tavtigian (2018), ACGS (Ellard 2020) and CanVIG (now gives Class 5)
                # self.contrib(self.PM, codes, lambda n: n == 1),
                # Not in original guidelines; added to fill logical gap
                self.contrib(self.PP, codes, lambda n: n == 1),
            ),
            self._AND(
                self.contrib(self.PS, codes, lambda n: n == 1),
                self.contrib(self.PM, codes, lambda n: n >= 1),
            ),
            self._AND(
                self.contrib(self.PS, codes, lambda n: n == 1),
                self.contrib(self.PP, codes, lambda n: n >= 2),
            ),
            self.contrib(self.PM, codes, lambda n: n >= 3),
            self._AND(
                self.contrib(self.PM, codes, lambda n: n == 2),
                self.contrib(self.PP, codes, lambda n: n >= 2),
            ),
            self._AND(
                self.contrib(self.PM, codes, lambda n: n == 1),
                self.contrib(self.PP, codes, lambda n: n >= 4),
            ),
        )

    def benign(self, codes: list[str]):
        """
        If the codes given satisfy the requirements for benign, return list of all codes contributing, otherwise
        empty list.
        """
        return self._OR(
            # Modified from n == 1 to fill logical gap
            self.contrib(self.BA, codes, lambda n: n >= 1),
            self.contrib(self.BS, codes, lambda n: n >= 2),
        )

    def likely_benign(self, codes: list[str]):
        """
        If the codes given satisfy the requirements for likely benign, return list of all codes contributing, otherwise
        empty list.
        """
        return self._OR(
            self._AND(
                self.contrib(self.BS, codes, lambda n: n == 1),
                self.contrib(self.BP, codes, lambda n: n == 1),
            ),
            self.contrib(self.BP, codes, lambda n: n >= 2),
        )

    def _OR(self, *lists: list[str]):
        """
        List-or which returns a list containing elems of all contributing lists.
        Will contain duplicates if codes may contribute to the classification in different ways.
        """
        ret = []
        for lst in lists:
            if lst:
                ret.extend(lst)
        return ret

    def _AND(self, *lists: list[str]):
        """
        List-and which returns a list containing elems of all contributing lists.
        Will contain duplicates if codes may contribute to the classification in different ways.
        """
        ret = []
        for lst in lists:
            if lst:
                ret.extend(lst)
            else:
                return []
        return ret

    def contrib(
        self, pattern: re.Pattern[str], codes: list[str], length_constraint: Callable[[int], bool]
    ):
        """
        If the number of occurences of the given pattern in codes passes the given constraint, return the occurences list.
        """
        occ = self.occurences(pattern, codes)
        if length_constraint(len(occ)):
            return occ
        return []

    @staticmethod
    def occurences(pattern: re.Pattern[str], codes: list[str]):
        """
        The occurences matching the given pattern in the codes list. The returned list
        has only unique elements and is sorted.
        """
        occ = [code for code in set(codes) if pattern.match(code)]
        return sorted(occ)

    precedence = ["PVS", "PS", "PM", "PP", "BA", "BS", "BP"]

    def _has_higher_precedence(self, code_a: str, code_b: str):
        """
        Returns True if criteria a has higher precedence than criteria b, else
        Returns False.

        Input may be normal criterias like PS1, PM1 etc. or derived criterias
        like PMxPS1, PMxPVS1 etc. They must be preselected by criteria source (done
        in __accumulate_criteria__).
        """
        # Extracting numbers from criterias so it is possible to
        # do lookup in precedense list using index, i.e. PS1 is
        # converted to PS.
        strength_a = ACMGClassifier2015.find_strength(code_a)
        strength_b = ACMGClassifier2015.find_strength(code_b)

        # The criteria with the lowest index has the highest precedence
        return self.precedence.index(strength_a) < self.precedence.index(strength_b)

    @staticmethod
    def find_base_code(code: str):
        """
        Finding the base code from the derived code, if this is not a
        derived code, the same code is returned.

        Derived codes are always written like this:
        [PVS/PS/PM/PP/BP/BS/BA]x[base]
        """
        derived = code.split("x")
        if len(derived) == 1:
            return code
        return derived[1]

    @staticmethod
    def find_strength(code: str):
        return re.sub(r"\d", "", code.split("x")[0])

    def _select_codes_by_precedence(self, existing_codes: list[str], target_code: str):
        """Selecting the criteria of highest precedence"""
        assert (
            len(existing_codes) <= 1
        ), f"Internal error when selecting criteria with highest precedence: {existing_codes}, there should never be more than one criteria at this state."

        try:
            existing_code = existing_codes.pop(0)
        except IndexError:
            existing_code = None

        if existing_code is None:
            return [target_code]
        else:
            target_strength = ACMGClassifier2015.find_strength(target_code)
            existing_strength = ACMGClassifier2015.find_strength(existing_code)
            if self._has_higher_precedence(target_strength, existing_strength):
                return [target_code]
            else:
                return [existing_code]

    def _accumulate_codes(self, accum: list[str], target_code: str):
        """
        Accumulate criterias according to their precedence. Criterias with higher precedence
        are added. Already existing criterias with lower precedence are removed from the accum
        list.
        """
        if accum == []:
            return accum + [target_code]

        base_code = self.find_base_code(target_code)
        existing_codes_of_this_kind = [code for code in accum if base_code in code]
        other_codes = [code for code in accum if base_code not in code]

        return other_codes + self._select_codes_by_precedence(
            existing_codes_of_this_kind, target_code
        )

    def normalize_codes(self, codes: list[str]):
        """
        Returning a list where all codes with lower precedence are filtered out.
        """
        return reduce(lambda accum, code: self._accumulate_codes(accum, code), codes, [])

    def contradict(self, codes: list[str]):
        """
        If the codes given satisfy the requirements for contradiction, return list of all codes contributing, otherwise
        empty list.
        """
        return self._AND(
            self._OR(
                self.contrib(self.PVS, codes, lambda n: n >= 1),
                self.contrib(self.PS, codes, lambda n: n >= 1),
                self.contrib(self.PM, codes, lambda n: n >= 1),
                self.contrib(self.PP, codes, lambda n: n >= 1),
            ),
            self._OR(
                self.contrib(self.BA, codes, lambda n: n >= 1),
                self.contrib(self.BS, codes, lambda n: n >= 1),
                self.contrib(self.BP, codes, lambda n: n >= 1),
            ),
        )


class ClassificationResult:
    """
    Result of ACMG classification. Aggregate of classification and metadata.
    """

    def __init__(self, clazz: int, classification: str, contributors: list[str], message: str):
        self.clazz = clazz
        self.classification = classification
        self.contributors = contributors
        self.message = message
        self.meta: dict[Any, Any] = {}

    @override
    def __eq__(self, other: object):
        return isinstance(other, self.__class__) and self.__dict__ == other.__dict__
