import copy
import fnmatch
from collections import OrderedDict
from typing import Any

from .grm import GRM


class GRA:
    """
    GenAP Rule Applier, GRA

    Applies data to rules, produces result
    """

    def parseNodeToSourceKeyedDict(self, node: dict[str, Any], parents: tuple[str, ...] = tuple()):
        ret: dict[tuple[str, ...], Any] = {}
        for key, child in node.items():
            if isinstance(child, dict):
                # New level
                ret.update(self.parseNodeToSourceKeyedDict(child, parents + (key,)))
            else:
                # "AA": 0.102587
                # "AFR": 0.05
                # etc
                ret[parents + (key,)] = child
        return ret

    def expand_multi_rules(self, rules: list[GRM.Rule], data_flattened: dict[str, Any]):
        """
        Looks for a rule with a source like refassessment.*.ref_segregation. Go over the data and find matching sources. Create a new rule for each
        match and add to rules. Thereafter normal engine processing.
        """
        for rule in rules:
            if rule.source and ".*." in rule.source:
                new_rules: list[GRM.Rule] = []
                for data_source in list(data_flattened.keys()):
                    if fnmatch.fnmatch(data_source, rule.source):
                        new_rule = copy.deepcopy(rule)
                        new_rule.source = data_source
                        new_rules.append(new_rule)
                # Reinsert the new rules, replacing the current rule at same location
                rule_idx = rules.index(rule)
                rules.pop(rule_idx)
                # Reverse list, since items will be inserted one by one at same position
                # This will keep original order
                new_rules.reverse()
                for n in new_rules:
                    rules.insert(rule_idx, n)

    def applyRules(self, rules: OrderedDict[str, list[GRM.Rule]], data: dict):
        """
        Applies rules, returns (passed, notpassed) tuple
        """
        passed: list[GRM.Rule] = []
        not_passed: list[GRM.Rule] = []
        data_flattened = {".".join(list(k)): v for k, v in data.items()}
        rule_list = [rul for resultlist in list(rules.values()) for rul in resultlist]
        self.expand_multi_rules(rule_list, data_flattened)
        result = (passed, not_passed)
        for rule in rule_list:
            if rule.aggregate:
                if rule.query([r.code for r in passed]):
                    passed.append(rule)
                else:
                    not_passed.append(rule)
            else:
                if rule.source in data_flattened:
                    if rule.query(data_flattened[rule.source]):
                        passed.append(rule)
                    else:
                        not_passed.append(rule)
                else:
                    not_passed.append(rule)
        return result
