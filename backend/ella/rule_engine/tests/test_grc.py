import re

from ..grc import ACMGClassifier2015, ClassificationResult


def test_count_occurences():
    classifier = ACMGClassifier2015()
    assert classifier.occurences(re.compile("PVS1"), ["PVS1"]) == ["PVS1"]
    assert classifier.occurences(re.compile("PVS1"), ["PVS2", "BP1"]) == []
    assert classifier.occurences(re.compile("PS1"), []) == []
    assert classifier.occurences(re.compile("PVS.*"), ["PVS1"]) == ["PVS1"]
    assert classifier.occurences(re.compile("PVS.*"), ["PVS1", "PVS3"]) == ["PVS1", "PVS3"]
    assert classifier.occurences(re.compile("PVS.*"), ["PVS1", "PVS3", "PP1"]) == ["PVS1", "PVS3"]
    assert classifier.occurences(
        re.compile("PM.*"), ["BS2", "BS4", "PVS1", "PVS3", "PM3", "PP2"]
    ) == ["PM3"]
    assert classifier.occurences(
        re.compile("PM.*"), ["BS2", "PM3", "BS4", "PVS1", "PVS3", "PM3", "PP2", "PM1"]
    ) == [
        "PM1",
        "PM3",
    ]


def test_count_occurences_special_cases():
    classifier = ACMGClassifier2015()
    assert classifier.occurences(
        re.compile("PP.*"),
        ["BS2", "PP3", "BS4", "PVS1", "PVS3", "PM3", "PP3", "PP1", "PP2"],
    ) == ["PP1", "PP2", "PP3"]  # The first of the PP3s returned.
    assert classifier.occurences(
        re.compile("PP.*"), ["BS2", "PP3", "BS4", "PVS1", "PVS3", "PM3", "PP3", "PP1", "PP1"]
    ) == ["PP1", "PP3"]  # The first of the PP3s returned.)
    assert classifier.occurences(
        re.compile("BP.*"),
        ["BP4", "PP3", "BP4", "PVS3", "PVS3", "PM3", "BP3", "PP1", "PP1", "BP4", "BP4"],
    ) == ["BP3", "BP4"]
    assert classifier.occurences(
        re.compile("BS.*"),
        ["BS1", "BS1", "BP4", "PVS3", "PVS3", "PM3", "BP3", "PP1", "PP1", "BP4", "BP4"],
    ) == ["BS1"]


def test_classify_pathogenic():
    classifier = ACMGClassifier2015()
    pathogenic_contributors = classifier.pathogenic(["BS2", "BS4", "PVS1", "BP7", "PS3", "PP1"])
    assert pathogenic_contributors == ["PVS1", "PS3"]  # 1PVS* AND >=1PS*

    pathogenic_contributors = classifier.pathogenic(
        ["BS2", "BP4", "BP6", "BP1", "PVS3", "BP3", "PM3", "BM1"]
    )
    assert pathogenic_contributors == ["PVS3", "PM3"]  # 1PVS* AND 1PM*

    pathogenic_contributors = classifier.pathogenic(
        ["BS1", "BP2", "BP6", "BP1", "PVS1", "BP3", "PM3", "BP1", "PM2"]
    )
    assert pathogenic_contributors == ["PVS1", "PM2", "PM3"]  # 1PVS* AND >1PM*

    pathogenic_contributors = classifier.pathogenic(["BS2", "PVS1", "BS4", "PVSX1"])
    assert pathogenic_contributors == ["PVS1", "PVSX1"]  # >=2PVS*

    pathogenic_contributors = classifier.pathogenic(
        ["BS2", "BS4", "PVS1", "PS3", "PS3", "PS3", "PP1"]
    )
    assert pathogenic_contributors == [
        "PVS1",
        "PS3",
    ]  # First 1PVS* AND >=1PS*  # Then >=2 PS*, because of not shortcut/conversion to set

    pathogenic_contributors = classifier.pathogenic(
        ["BS2", "PS4", "PM2", "PM1", "BA3", "PP3", "PP1"]
    )
    assert pathogenic_contributors == [
        "PS4",
        "PM1",
        "PM2",
        "PP1",
        "PP3",
    ]  # 1PS* AND 2PM* AND >=2PP*

    pathogenic_contributors = classifier.pathogenic(
        ["BS2", "PS4", "PP4", "PP6", "PP1", "BA3", "PP3", "PP3", "PM1"]
    )
    assert pathogenic_contributors == [
        "PS4",
        "PM1",
        "PP1",
        "PP3",
        "PP4",
        "PP6",
    ]  # 1PS* AND 1PM* AND >=4PP*
    assert not classifier.pathogenic(["BS2", "PS4", "PP4", "PP6", "BA3", "PP3", "PP3", "PM1"])
    assert not classifier.pathogenic(["BS2", "PP4", "PP1", "PP6", "BA3", "PP3", "PP3", "PM1"])


def test_classify_likely_pathogenic():
    classifier = ACMGClassifier2015()
    likely_pathogenic_contributors = classifier.likely_pathogenic(
        ["BS2", "PS4", "PP4", "PP6", "BA3", "PP3", "PP3", "PM1"]
    )
    assert likely_pathogenic_contributors == ["PS4", "PM1", "PS4", "PP3", "PP4", "PP6"]

    likely_pathogenic_contributors = classifier.likely_pathogenic(
        ["BS2", "PS4", "PP4", "PP6", "PP1", "BA3", "PP3", "PP3", "PM1"]
    )

    expected = [
        "PS4",  # First 1PS* AND 1PM*
        "PM1",
        "PS4",  # Then 1PS* AND >=2PP*
        "PP1",
        "PP3",
        "PP4",
        "PP6",
        "PM1",  # Then 1PM* AND >=4PP*
        "PP1",
        "PP3",
        "PP4",
        "PP6",
    ]
    assert likely_pathogenic_contributors == expected

    likely_pathogenic_contributors = classifier.likely_pathogenic(
        ["BS2", "PP4", "PP6", "PP1", "BA3", "PP3", "PP3", "PM1"]
    )
    assert likely_pathogenic_contributors == ["PM1", "PP1", "PP3", "PP4", "PP6"]  # 1PM* AND >=4PP*

    likely_pathogenic_contributors = classifier.likely_pathogenic(
        ["BS2", "BP4", "BP6", "BP1", "PVS3", "BP3", "PP3", "BM1"]
    )
    assert likely_pathogenic_contributors == ["PVS3", "PP3"]  # 1PVS* AND 1PP*
    assert classifier.likely_pathogenic(["PVSX1", "PP6", "BS2"])


def test_classify_benign():
    classifier = ACMGClassifier2015()
    benign_contributors = classifier.benign(
        ["BS2", "PS4", "BAX1", "PP6", "BA3", "BS1", "BS2", "PM1"]
    )
    assert benign_contributors == ["BA3", "BAX1", "BS1", "BS2"]  # >=1BA* OR >=2BS*
    assert not classifier.benign(["PP7", "PP6", "BS2"])


def test_classify_likely_benign():
    classifier = ACMGClassifier2015()
    likely_benign_contributors = classifier.likely_benign(
        ["PP2", "PS4", "BS2", "PP6", "BA3", "BP3"]
    )
    assert likely_benign_contributors == ["BS2", "BP3"]  # 1BS* AND 1BP*

    likely_benign_contributors = classifier.likely_benign(["BP2", "PS4", "PP6", "BA3", "BP3"])
    assert likely_benign_contributors == ["BP2", "BP3"]  # >=2BP*
    assert not likely_benign_contributors == ["PP7", "PP6", "BS2"]


def test_contradict():
    classifier = ACMGClassifier2015()
    pathogenic = ["PVS1", "PS3"]
    likely_pathogenic = ["PS4", "PM1", "PS4", "PP4", "PP6", "PP3"]
    benign = ["BA3", "BS2", "BS1", "BS2"]
    likely_benign = ["BS2", "BP3"]
    contradict = ["BS2", "PVS1", "PM3"]
    contradict2 = ["BS2", "PVS1", "PS3", "PS3", "PS3", "PP1"]
    contributors = classifier.contradict(pathogenic)
    assert not contributors

    contributors = classifier.contradict(likely_pathogenic)
    assert not contributors

    contributors = classifier.contradict(benign)
    assert not contributors

    contributors = classifier.contradict(likely_benign)
    assert not contributors

    contributors = classifier.contradict(contradict)
    assert contributors == ["PVS1", "PM3", "BS2"]

    contributors = classifier.contradict(contradict2)
    assert contributors == ["PVS1", "PS3", "PP1", "BS2"]


def test_classify():
    classifier = ACMGClassifier2015()
    assert classifier.classify(["PVS1", "PS2", "BA1"]) == ClassificationResult(
        3, "Uncertain significance", ["PVS1", "PS2", "BA1"], "Contradiction"
    )

    passed = ["BS2", "PVS1", "PS3", "PS3", "PS3", "PP1"]
    assert classifier.classify(passed) == ClassificationResult(
        3, "Uncertain significance", ["PVS1", "PS3", "PP1", "BS2"], "Contradiction"
    )

    passed = ["BS2", "PVS1", "PM3"]
    result = classifier.classify(passed)
    expected = ClassificationResult(
        3, "Uncertain significance", ["PVS1", "PM3", "BS2"], "Contradiction"
    )
    assert result.message == expected.message

    passed = ["BS2", "PP4", "PP6", "BA3", "BS1", "BS2", "PM1"]
    result = classifier.classify(passed)
    expected = ClassificationResult(
        3, "Uncertain significance", ["PM1", "PP4", "PP6", "BA3", "BS1", "BS2"], "Contradiction"
    )
    assert result.contributors == expected.contributors

    passed = ["BP4", "BP6", "PM1"]
    assert classifier.classify(passed) == ClassificationResult(
        3, "Uncertain significance", ["PM1", "BP4", "BP6"], "Contradiction"
    )

    passed = ["PM1", "BP6", "PP6", "BP7"]
    assert classifier.classify(passed) == ClassificationResult(
        3, "Uncertain significance", ["PM1", "PP6", "BP6", "BP7"], "Contradiction"
    )

    passed = ["BS1", "BP6", "PP6", "BP7"]
    assert classifier.classify(passed) == ClassificationResult(
        3, "Uncertain significance", ["PP6", "BS1", "BP6", "BP7"], "Contradiction"
    )

    passed = ["BP6", "PM1"]
    assert classifier.classify(passed) == ClassificationResult(
        3, "Uncertain significance", ["PM1", "BP6"], "Contradiction"
    )


def test_presedence():
    classifier = ACMGClassifier2015()
    assert classifier._has_higher_precedence("PVS1", "PS1")
    assert not classifier._has_higher_precedence("PS1", "PVS1")
    assert classifier._has_higher_precedence("PS3", "PMxPS3")
    assert not classifier._has_higher_precedence("PMxPS3", "PS3")
    assert classifier._has_higher_precedence("PVSxPS3", "PMxPS3")
    assert not classifier._has_higher_precedence("PMxPS3", "PVSxPS3")
    assert not classifier._has_higher_precedence("PVSxPS3", "PVS1")
    assert not classifier._has_higher_precedence("PVS1", "PVSxPS3")


def test_find_base_code():
    classifier = ACMGClassifier2015()
    assert classifier.find_base_code("PMxPS1") == "PS1"
    assert classifier.find_base_code("PS1") == "PS1"


def test_normalize_codes():
    classifier = ACMGClassifier2015()
    assert classifier.normalize_codes(["PM1", "PVSxPM1", "PS3", "PMxPS3"]) == ["PVSxPM1", "PS3"]

    # Tests data below are given by domain expert Morten Eike
    assert classifier.normalize_codes(["PM1", "PP1", "PP1", "PP2", "PP3"]) == [
        "PM1",
        "PP1",
        "PP2",
        "PP3",
    ]  # Duplicates PP1 is filtered out
    assert classifier.normalize_codes(["PM1", "PP1", "PP2", "PP3", "PPxPM1"]) == [
        "PP1",
        "PP2",
        "PP3",
        "PM1",
    ]  # PM1 takes precedence above PPxPM1, hence PPxPM1 gets filtered out
    assert classifier.normalize_codes(["PMxPP1", "PM1", "PP1", "PP2", "PPxPS1"]) == [
        "PM1",
        "PMxPP1",
        "PP2",
        "PPxPS1",
    ]  # PM1 takes precedence above PPxPM1, hence PPxPM1 gets filtered out
    assert classifier.normalize_codes(["PSxPM1", "PS1", "PM1"]) == [
        "PS1",
        "PSxPM1",
    ]  # PSxPM1 takes precedence above PS1, hence PS1 gets filtered out
    assert classifier.normalize_codes(["BSxBP1", "BP1", "BS1"]) == [
        "BSxBP1",
        "BS1",
    ]  # BSxBP1 takes precedence above BS1, hence BP1 gets filtered out
    assert classifier.normalize_codes(["BS1", "BS2"]) == ["BS1", "BS2"]
    assert classifier.normalize_codes(["PVS1", "BA1"]) == ["PVS1", "BA1"]


def test_classification_codes_with_precedence():
    # Tests below are given by domain expert Morten Eike
    classifier = ACMGClassifier2015()
    case1 = classifier.classify(["PM1", "PP1", "PP1", "PP2", "PP3"])
    assert case1.clazz == 3
    assert not case1.contributors

    case2 = classifier.classify(["PM1", "PP1", "PP2", "PP3", "PPxPM1"])
    assert case2.clazz == 3
    assert not case2.contributors

    case3 = classifier.classify(["PMxPP1", "PM1", "PP1", "PP2", "PPxPS1"])
    assert case3.clazz == 4
    assert case3.contributors == ["PM1", "PMxPP1", "PP1", "PP2", "PPxPS1"]

    case4 = classifier.classify(["PSxPM1", "PS1", "PM1"])
    assert case4.clazz == 5
    assert (
        case4.contributors == ["PS1", "PSxPM1"]
    )  # 2*PS will short circuit PM according to the rule, thus PM will not be counted as a contributor

    case5 = classifier.classify(["BSxBP1", "BP1", "BS1"])
    assert case5.clazz == 1
    assert case5.contributors == [
        "BS1",
        "BSxBP1",
    ]  # 2*BS is the rule for benign, BP1 will not be counted and therefore BP1 is not a contributor

    case6 = classifier.classify(["BS1", "BS2"])
    assert case6.clazz == 1
    assert case6.contributors == ["BS1", "BS2"]

    case7 = classifier.classify(["PVS1", "BA1"])
    assert case7.clazz == 3
    assert case7.contributors == ["PVS1", "BA1"]
