from typing import Any

from ..grl import GRL
from ..grm import GRM


def parse_json(json_string: list[dict[str, Any]]):
    grl = GRL()
    return grl.parseRules(json_string)


def test_parse_simple_equality_rule():
    json_string = [{"code": "BP2", "rule": {"transcript.splice.effect": "de_novo"}}]
    rules = parse_json(json_string)
    assert rules["BP2"][0].source == "transcript.splice.effect"
    assert rules["BP2"][0].code == "BP2"
    assert rules["BP2"][0].value == ["de_novo"]
    assert isinstance(rules["BP2"][0], GRM.InRule)


def test_parse_basic_rule_list():
    json_string = [
        {
            "code": "BP1",
            "rule": {"transcript.Consequence": {"$in": ["missense_variant", "synonymous_variant"]}},
        },
        {"code": "BP2", "rule": {"transcript.splice.effect": "de_novo"}},
        {"code": "BP1", "rule": {"t.s.e": "d_n"}},
        {
            "code": "rBP7-1",
            "rule": {
                "transcript.Consequence": {
                    "$in": [
                        "start_retained_variant",
                        "stop_retained_variant",
                        "5_prime_UTR_variant",
                        "3_prime_UTR_variant",
                        "non_coding_exon_variant",
                        "nc_transcript_variant",
                        "intron_variant",
                        "upstream_gene_variant",
                        "downstream_gene_variant",
                        "intergenic_variant",
                        "synonymous_variant",
                    ]
                }
            },
        },
        {"code": "rBP7-2", "rule": {"genomic.Conservation": "non_conserved"}},
        {"code": "rBP7-3", "rule": {"transcript.splice.effect": "no_effect"}},
        {"code": "rBP7-4", "rule": {"foo.bar": {"$lt": "34323"}}},
        {"code": "rBP7-5", "rule": {"bar.foo": {"$gt": 0.5}}},
        {"code": "rBP7-6", "rule": {"ba.fo": {"$range": [0, 1.6]}}},
        {"code": "GP1", "rule": {"genepanel.hi_freq_cutoff": 0.01}},
    ]
    rules = parse_json(json_string)
    assert rules["BP1"][0].code == "BP1"
    assert rules["BP1"][1].code == "BP1"
    assert rules["BP1"][1].source == "t.s.e"
    assert rules["BP1"][1].value == ["d_n"]
    assert rules["BP2"][0].code == "BP2"
    assert rules["rBP7-1"][0].code == "rBP7-1"
    assert rules["rBP7-1"][0].source == "transcript.Consequence"
    assert rules["rBP7-1"][0].value == [
        "start_retained_variant",
        "stop_retained_variant",
        "5_prime_UTR_variant",
        "3_prime_UTR_variant",
        "non_coding_exon_variant",
        "nc_transcript_variant",
        "intron_variant",
        "upstream_gene_variant",
        "downstream_gene_variant",
        "intergenic_variant",
        "synonymous_variant",
    ]
    assert rules["rBP7-2"][0].code == "rBP7-2"
    assert rules["rBP7-3"][0].code == "rBP7-3"
    assert rules["rBP7-3"][0].source == "transcript.splice.effect"
    assert rules["rBP7-3"][0].value == ["no_effect"]
    assert rules["rBP7-4"][0].value == "34323"
    assert isinstance(rules["rBP7-4"][0], GRM.LtRule)
    assert rules["rBP7-5"][0].value == 0.5
    assert isinstance(rules["rBP7-5"][0], GRM.GtRule)
    assert isinstance(rules["rBP7-6"][0], GRM.RangeRule)
    assert rules["rBP7-6"][0].value[0] == 0  # type: ignore[index]
    assert rules["rBP7-6"][0].value[1] == 1.6  # type: ignore[index]


def test_parse_composite_rules():
    json_string = [
        {
            "code": "BP1",
            "rule": {"transcript.Consequence": {"$in": ["missense_variant", "synonymous_variant"]}},
        },
        {"code": "BP2", "rule": {"genepanel.hi_freq_cutoff": 0.001}},
        {
            "code": "MYAND",
            "rule": {
                "$and": [
                    {"genepanel.hi_freq_cutoff": 0.001},
                    {"transcript.Consequence": {"$in": ["missense_variant", "synonymous_variant"]}},
                ]
            },
        },
        {
            "code": "MYOR",
            "rule": {
                "$or": [
                    {"genepanel.hi_freq_cutoff": 0.002},
                    {"transcript.Conseq": {"$in": ["missense_v", "synonymous_v"]}},
                ]
            },
        },
        {
            "code": "MYCOMPLEX",
            "rule": {
                "$or": [
                    {"genepanel.hi_freq_cutoff": 0.001},
                    {
                        "$and": [
                            {"a.b": 0.01},
                            {"c.d": {"$in": ["some", "values"]}},
                            {
                                "$or": [
                                    {"e.f.g": "0.001"},
                                    {"h.jjj": {"$in": ["more", "values"]}},
                                ]
                            },
                        ]
                    },
                    {"genepanel.foo": "baz"},
                ]
            },
        },
    ]
    rules = parse_json(json_string)
    assert isinstance(rules["MYAND"][0], GRM.AndRule)
    assert rules["MYAND"][0].subrules[0].source == "genepanel.hi_freq_cutoff"
    assert rules["MYAND"][0].subrules[0].value == [0.001]
    assert isinstance(rules["MYAND"][0].subrules[0], GRM.InRule)
    assert rules["MYAND"][0].subrules[1].source == "transcript.Consequence"
    assert rules["MYAND"][0].subrules[1].value == ["missense_variant", "synonymous_variant"]
    assert isinstance(rules["MYAND"][0].subrules[1], GRM.InRule)
    assert isinstance(rules["MYOR"][0], GRM.OrRule)
    assert rules["MYOR"][0].subrules[0].source == "genepanel.hi_freq_cutoff"
    assert rules["MYOR"][0].subrules[0].value == [0.002]
    assert isinstance(rules["MYOR"][0].subrules[0], GRM.InRule)
    assert rules["MYOR"][0].subrules[1].source == "transcript.Conseq"
    assert rules["MYOR"][0].subrules[1].value == ["missense_v", "synonymous_v"]
    assert isinstance(rules["MYOR"][0].subrules[1], GRM.InRule)
    assert isinstance(rules["MYCOMPLEX"][0], GRM.OrRule)
    rules["MYCOMPLEX"][0].subrules[0].source == "genepanel.hi_freq_cutoff"
    rules["MYCOMPLEX"][0].subrules[0].value == [0.001]
    assert isinstance(rules["MYCOMPLEX"][0].subrules[1], GRM.AndRule)
    assert rules["MYCOMPLEX"][0].subrules[1].subrules[0].source == "a.b"
    assert rules["MYCOMPLEX"][0].subrules[1].subrules[0].value == [0.01]
    assert rules["MYCOMPLEX"][0].subrules[1].subrules[1].source == "c.d"
    assert isinstance(rules["MYCOMPLEX"][0].subrules[1].subrules[1], GRM.InRule)
    assert rules["MYCOMPLEX"][0].subrules[1].subrules[1].value == ["some", "values"]
    assert isinstance(rules["MYCOMPLEX"][0].subrules[1].subrules[2], GRM.OrRule)
    assert isinstance(rules["MYCOMPLEX"][0].subrules[1].subrules[2].subrules[0], GRM.InRule)
    assert rules["MYCOMPLEX"][0].subrules[1].subrules[2].subrules[0].source == "e.f.g"
    assert rules["MYCOMPLEX"][0].subrules[1].subrules[2].subrules[0].value == ["0.001"]
    assert isinstance(rules["MYCOMPLEX"][0].subrules[1].subrules[2].subrules[1], GRM.InRule)
    assert rules["MYCOMPLEX"][0].subrules[1].subrules[2].subrules[1].source == "h.jjj"
    assert rules["MYCOMPLEX"][0].subrules[1].subrules[2].subrules[1].value == ["more", "values"]


def test_rule_aggrmgation_all():
    json_string = [
        {
            "code": "rBP7-1",
            "rule": {
                "transcript.Consequence": {
                    "$in": [
                        "start_retained_variant",
                        "stop_retained_variant",
                        "5_prime_UTR_variant",
                        "3_prime_UTR_variant",
                        "non_coding_exon_variant",
                        "nc_transcript_variant",
                        "intron_variant",
                        "upstream_gene_variant",
                        "downstream_gene_variant",
                        "intergenic_variant",
                        "synonymous_variant",
                    ]
                }
            },
        },
        {"code": "rBP7-2", "rule": {"genomic.Conservation": "non_conserved"}},
        {"code": "rBP7-3", "rule": {"transcript.splice.effect": "no_effect"}},
        {"code": "rBP7-4", "rule": {"transcript.splice.power": "some_effect"}},
        {"code": "BP7", "rule": {"$$aggregate": {"$all": ["rBP7-1", "rBP7-2", "rBP7-3"]}}},
        {"code": "PP7", "rule": {"$$aggregate": {"$all": ["BP7", "rBP7-4"]}}},
        {"code": "PP8", "rule": {"$$aggregate": {"$not": {"$all": ["BP7", "rBP7-4"]}}}},
    ]
    rules = parse_json(json_string)
    assert isinstance(rules["BP7"][0], GRM.AllRule)
    assert rules["BP7"][0].value == ["rBP7-1", "rBP7-2", "rBP7-3"]
    assert rules["BP7"][0].code == "BP7"
    assert rules["PP8"][0].subrule.value == ["BP7", "rBP7-4"]


def test_rule_aggregation_composite():
    json_string = [
        {"code": "rBP7-1", "rule": {"genomic.Conservation": "non_conserved"}},
        {"code": "rBP7-2", "rule": {"transcript.splice.effect": "no_effect"}},
        {"code": "rBP7-3", "rule": {"transcript.splice.power": "some_effect"}},
        {"code": "rBP7-4", "rule": {"transcript.splice.power": "some_effect"}},
        {"code": "rBP7-5", "rule": {"transcript.splice.power": "some_effect"}},
        {"code": "BP7", "rule": {"$$aggregate": {"$all": ["rBP7-1", "rBP7-2", "rBP7-3"]}}},
        {"code": "BP8", "rule": {"$$aggregate": {"$atleast": [1, "rBP7-4", "rBP7-5"]}}},
        {
            "code": "BP9",
            "rule": {"$$aggregate": {"$or": [{"$all": ["BP7"]}, {"$not": {"$all": ["BP8"]}}]}},
        },
    ]
    rules = parse_json(json_string)
    assert rules["BP9"][0].aggregate
    assert isinstance(rules["BP9"][0], GRM.OrRule)
    assert rules["BP9"][0].subrules[0].value == ["BP7"]
    assert rules["BP9"][0].subrules[1].subrule.value == ["BP8"]  # type: ignore[attr-defined]


def test_rule_aggrmgation_atleast():
    json_string = [
        {
            "code": "rBP7-1",
            "rule": {
                "transcript.Consequence": {
                    "$in": [
                        "start_retained_variant",
                        "stop_retained_variant",
                        "5_prime_UTR_variant",
                        "3_prime_UTR_variant",
                        "non_coding_exon_variant",
                        "nc_transcript_variant",
                        "intron_variant",
                        "upstream_gene_variant",
                        "downstream_gene_variant",
                        "intergenic_variant",
                        "synonymous_variant",
                    ]
                }
            },
        },
        {"code": "rBP7-2", "rule": {"genomic.Conservation": "non_conserved"}},
        {"code": "rBP7-3", "rule": {"transcript.splice.effect": "no_effect"}},
        {"code": "rBP7-2", "rule": {"genomic.Onservation": "conserved"}},
        {
            "code": "PP7",
            "rule": {"$$aggregate": {"$atleast": [2, "rBP7-1", "rBP7-2", "rBP7-3"]}},
        },
    ]
    rules = parse_json(json_string)
    assert isinstance(rules["PP7"][0], GRM.AtLeastRule)
    assert rules["PP7"][0].atleast == 2
    assert rules["PP7"][0].value == ["rBP7-1", "rBP7-2", "rBP7-3"]
    assert rules["PP7"][0].code == "PP7"
