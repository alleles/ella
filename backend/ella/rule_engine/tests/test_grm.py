from ..grm import GRM


def test_inrule_single_input_annotation():
    rule = GRM.InRule(["transcript_ablation", "splice_donor_variant"], "VEP consequence")
    assert rule.query(["transcript_ablation"]) == [rule]
    assert not rule.query(["not_there"])

    rule = GRM.InRule(["transcript_ablation"], "VEP consequence")
    assert rule.query(["transcript_ablation"]) == [rule]
    assert not rule.query(["not_there"])

    rule = GRM.InRule([], "VEP consequence")
    assert not rule.query(["transcript_ablation"])


def test_inrule_multiple_input_annotation():
    rule = GRM.InRule(["transcript_ablation", "splice_donor_variant"], "VEP consequence")
    assert rule.query(["transcript_ablation", "something_else_too"]) == [rule]
    assert not rule.query(["not_there", "not_there_neither"])

    rule = GRM.InRule(["transcript_ablation"], "VEP consequence")
    assert rule.query(["transcript_ablation", "splice_donor_variant"]) == [rule]
    assert not rule.query(["not_there", "not_there_neither"])

    rule = GRM.InRule([], "VEP consequence")
    assert not rule.query(["transcript_ablation", "something_else"])


def test_inrule_single_value():
    """
    The InRule when used as a simple {"transcript.splice.effect": "de_novo"}
    """
    rule = GRM.InRule(["de_novo"], "transcript.splice.effect")
    assert rule.query(["de_novo"]) == [rule]
    assert rule.query("de_novo") == [rule]
    assert not rule.query(["not_de_novo"])

    rule = GRM.InRule([99], "transcript.splice.eff")
    assert rule.query([99]) == [rule]
    assert rule.query(99) == [rule]


def test_and_rule():
    rule1 = GRM.InRule(["transcript_ablation"], "VEP consequence")
    rule2 = GRM.InRule(["splice_donor_variant"], "VEP consequence")
    rule3 = GRM.InRule(["stop_gained"], "VEP consequence")
    and_rule = GRM.AndRule([rule1, rule2, rule3])
    assert and_rule.query(["stop_gained", "transcript_ablation", "splice_donor_variant"]) == [
        rule1,
        rule2,
        rule3,
    ]
    assert not and_rule.query(["stop_gained", "splice_donor_variant"])
    assert not and_rule.query([])


def test_or_rule():
    rule1 = GRM.InRule(["transcript_ablation"], "VEP consequence")
    rule2 = GRM.InRule(["splice_donor_variant"], "VEP consequence")
    rule3 = GRM.InRule(["stop_gained"], "VEP consequence")
    or_rule = GRM.OrRule([rule1, rule2, rule3])
    assert or_rule.query(["stop_gained", "splice_donor_variant"]) == [rule2, rule3]
    assert not or_rule.query(["different", "annotations", "altogether"])
    assert not or_rule.query([])


def test_gr_rule():
    rule = GRM.GtRule(0.4, "ESP6500")
    assert rule.query(0.5) == [rule]
    assert rule.query(0.6)[0].source == "ESP6500"
    assert rule.query("0.6")[0].source == "ESP6500"
    assert rule.query("68")[0].source == "ESP6500"
    assert not rule.query(-876)
    assert not rule.query(0.4)


def test_lt_rule():
    rule = GRM.LtRule(0.4, "ESP6501")
    assert rule.query(0.3) == [rule]
    assert rule.query(0.3999999)[0].source == "ESP6501"
    assert rule.query("-678")[0].source == "ESP6501"
    assert not rule.query(876)
    assert not rule.query(0.4)


def test_range_rule():
    rule = GRM.RangeRule([-67.3, 4], "ESP6502")
    assert rule.query(-3) == [rule]
    assert rule.query(-67.2999999)[0].source == "ESP6502"
    assert not rule.query(-875)
    assert not rule.query(4.00002)


def test_complex_composite_rule():
    """
    Test of transcript_ablation AND splice_donor_variant AND stop_gained
    OR ([Phase]in_cis_pathogenic AND [Phase]in_trans_pathogenic)
    """
    rule1_1 = GRM.InRule(["transcript_ablation"], "VEP consequence")
    rule1_2 = GRM.InRule(["splice_donor_variant"], "VEP consequence")
    rule1_3 = GRM.InRule(["stop_gained"], "VEP consequence")
    and_rule1 = GRM.AndRule([rule1_1, rule1_2, rule1_3])

    rule2_1 = GRM.InRule(["in_cis_pathogenic"], "Phase")
    rule2_2 = GRM.InRule(["in_trans_pathogenic"], "Phase")
    and_rule2 = GRM.AndRule([rule2_1, rule2_2])

    or_rule = GRM.OrRule([and_rule1, and_rule2])
    assert or_rule.query(["transcript_ablation", "splice_donor_variant", "stop_gained"]) == [
        rule1_1,
        rule1_2,
        rule1_3,
    ]
    assert (
        or_rule.query(["transcript_ablation", "splice_donor_variant", "stop_gained"])[0].source
        == "VEP consequence"
    )
    assert or_rule.query(["in_cis_pathogenic", "in_trans_pathogenic"]) == [rule2_1, rule2_2]
    assert or_rule.query(["in_cis_pathogenic", "in_trans_pathogenic"])[0].source == "Phase"
    assert not or_rule.query(["transcript_ablation", "splice_donor_variant", "in_cis_pathogenic"])


def test_all_rule():
    rule = GRM.AllRule(["rBP7-1", "rBP7-2", "rBP7-3", "rBP7-4", "rBP7-5"], "BP7")
    assert not rule.query([])
    assert not rule.query(["rBP7-1"])
    assert not rule.query(["rBP7-1", "rBP7-2"])
    assert not rule.query(["rBP7-1", "rBP7-3"])
    assert rule.query(["rBP7-1", "rBP7-2", "rBP7-3", "rBP7-4", "rBP7-5"]) == [rule]
    assert rule.query(["rBP7-1", "rBP7-2", "rBP7-4", "rBP7-3", "rBP7-5", "rBP7-4"]) == [rule]
    assert rule.query(["rBP7-1", "rBP7-2", "rBP7-3", "rBP7-4", "rBP7-5", "and"]) == [rule]
    assert rule.query(["rBP7-1", "rBP7-2", "rBP7-3", "rBP7-4", "rBP7-5", "and", "more"]) == [rule]


def test_all_rule_wildcard():
    rule = GRM.AllRule(["BP6", "PP*"], "BP7")
    assert not rule.query([])
    assert not rule.query(["rBP7-1"])
    assert not rule.query(["rBP7-1", "rBP7-2"])
    assert not rule.query(["rBP7-1", "BP6"])
    assert rule.query(["PP3", "BP6"]) == [rule]
    assert rule.query(["rBP7-1", "rBP7-2", "rBP7-4", "PP5", "rBP7-5", "BP6", "rBP7-4"]) == [rule]


def test_atleast_rule():
    rule = GRM.AtLeastRule(["rBP7-1", "rBP7-2", "rBP7-3", "rBP7-4", "rBP7-5", "rBP7-1"], "BP7", 3)
    assert not rule.query([])
    assert not rule.query(["rBP7-1"])
    assert not rule.query(["rBP7-1", "rBP7-2"])
    assert not rule.query(["rBP7-1", "rBP7-3", "rBP7-1"])
    assert rule.query(["rBP7-1", "rBP7-2", "rBP7-4"]) == [rule]
    assert rule.query(["rBP7-1", "rBP7-2", "rBP7-3", "rBP7-4"]) == [rule]
    assert rule.query(["rBP7-1", "rBP7-2", "rBP7-4", "rBP7-3", "rBP7-5", "rBP7-4"]) == [rule]
    assert rule.query(["rBP7-1", "rBP7-2", "rBP7-3", "rBP7-4", "rBP7-5", "and"]) == [rule]
    assert rule.query(["rBP7-1", "rBP7-2", "rBP7-3", "rBP7-4", "rBP7-5", "and", "more"]) == [rule]


def test_atleast_rule_wildcard():
    rule = GRM.AtLeastRule(["BP7", "BP8", "BP9", "BP1*"], "BP0", 3)
    assert not rule.query([])
    assert not rule.query(["BP7"])
    assert not rule.query(["BP7", "BP7"])
    assert not rule.query(["BP1567", "BP1987"])
    assert not rule.query(["BP1567", "BP1987", "BP23"])
    assert not rule.query(["BP7", "BP8", "BP7"])
    assert rule.query(["BP7", "BP8", "BP9"]) == [rule]
    assert rule.query(["BP7", "BP1234234", "BP9"]) == [rule]
    assert rule.query(["BP7", "BP1234234", "BP9", "BP7"]) == [rule]


def test_not_rule():
    rule1 = GRM.InRule(["transcript_ablation"], "VEP consequence")
    rule2 = GRM.InRule(["splice_donor_variant"], "VEP consequence")
    rule3 = GRM.InRule(["stop_gained"], "VEP consequence")
    and_rule = GRM.AndRule([rule1, rule2, rule3])
    not_rule = GRM.NotRule(and_rule)
    assert not not_rule.query(["stop_gained", "transcript_ablation", "splice_donor_variant"])
    assert not not_rule.query(["stop_gained", "transcript_ablation", "splice_donor_variant", "foo"])
    assert not_rule.query(["stop_gained", "splice_donor_variant"]) == [and_rule]
    assert not_rule.query([]) == [and_rule]
