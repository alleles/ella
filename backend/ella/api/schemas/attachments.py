from __future__ import annotations

import base64
from pathlib import Path
from typing import TYPE_CHECKING

from marshmallow import Schema, fields

from ella.api.config import config
from ella.api.schemas.users import UserSchema

if TYPE_CHECKING:
    from ella.vardb.datamodel import attachment


class AttachmentSchema(Schema):
    class Meta:
        fields = (
            "id",
            "sha256",
            "filename",
            "size",
            "date_created",
            "mimetype",
            "extension",
            "thumbnail",
            "user",
        )

    thumbnail = fields.Method("get_thumbnail")
    user = fields.Nested(UserSchema)

    def get_path(self, obj: attachment.Attachment):
        assert obj.sha256 is not None
        return Path(config["app"]["attachment_storage"]) / obj.sha256[:2] / obj.sha256

    def get_thumbnail(self, obj: attachment.Attachment):
        thumbnail_path = self.get_path(obj).with_suffix(".thumbnail")
        if not thumbnail_path.is_file():
            return None
        else:
            with thumbnail_path.open("rb") as f:
                bdata = f.read()
            data = base64.b64encode(bdata).decode()
            return data
