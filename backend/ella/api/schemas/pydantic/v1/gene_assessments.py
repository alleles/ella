from __future__ import annotations

from datetime import datetime

from ella.api.schemas.pydantic.v1 import BaseModel
from ella.api.schemas.pydantic.v1.common import Comment
from ella.api.schemas.pydantic.v1.users import User


class GeneAssessmentEvaluation(Comment):
    ...


class GeneAssessment(BaseModel):
    id: int
    date_created: datetime
    date_superceeded: datetime | None = None
    gene_id: int
    genepanel_name: str
    genepanel_version: str
    analysis_id: int | None = None
    previous_assessment_id: int | None = None
    user_id: int
    usergroup_id: int
    user: User
    seconds_since_update: float
    evaluation: GeneAssessmentEvaluation
