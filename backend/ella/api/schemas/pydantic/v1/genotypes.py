from __future__ import annotations

from pydantic import ConfigDict, Field

from ella.api.schemas.pydantic.v1 import BaseModel
from ella.api.util.types import GenotypeTypes


class Genotype(BaseModel):
    id: int
    variant_quality: int
    filter_status: str


# TODO: get definitive data schema (as returned from AlleleDataLoader), in the meantime allow
#       extra fields as needed
class GenotypeSampleData(BaseModel):
    model_config = ConfigDict(extra="allow")

    id: int
    type: GenotypeTypes
    multiallelic: bool
    genotype_quality: int | None = None
    genotype_likelihood: list[int] | None = Field(default_factory=list)
    sequencing_depth: int | None = None
    allele_depth: dict = Field(default_factory=dict)
    copy_number: int | None = None
