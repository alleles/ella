from __future__ import annotations

from datetime import datetime

from pydantic import Field

from ella.api.schemas.pydantic.v1 import BaseModel
from ella.api.schemas.pydantic.v1.allele_assessments import (
    AlleleAssessment,
    AlleleAssessmentOverview,
)
from ella.api.schemas.pydantic.v1.allele_reports import AlleleReport
from ella.api.schemas.pydantic.v1.annotations import Annotation
from ella.api.schemas.pydantic.v1.genepanels import GenepanelBasic
from ella.api.schemas.pydantic.v1.references import ReferenceAssessment
from ella.api.schemas.pydantic.v1.samples import Sample
from ella.api.schemas.pydantic.v1.workflow import AlleleInterpretationOverview
from ella.api.util.types import GenomeReference


class Warnings(BaseModel):
    worse_consequence: str | None = None
    nearby_allele: str | None = None


class AlleleGene(BaseModel):
    symbol: str
    hgnc_id: int
    allele_ids: list[int]


class Allele(BaseModel):
    length: int
    genome_reference: GenomeReference
    vcf_ref: str
    change_from: str
    change_to: str
    chromosome: str
    start_position: int
    change_type: str
    open_end_position: int
    vcf_pos: int
    vcf_alt: str
    id: int
    caller_type: str
    annotation: Annotation
    tags: list[str]
    warnings: Warnings | None = None
    reference_assessments: list[ReferenceAssessment] = Field(default_factory=list)
    allele_assessment: AlleleAssessment | AlleleAssessmentOverview | None = None
    allele_report: AlleleReport | None = None
    samples: list[Sample] | None = None


class AlleleOverview(BaseModel):
    genepanel: GenepanelBasic
    allele: Allele
    date_created: datetime
    priority: int
    review_comment: str | None = None
    interpretations: list[AlleleInterpretationOverview]
    warnings: Warnings | None = None
