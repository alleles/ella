from __future__ import annotations

from datetime import datetime
from typing import Any

from pydantic import Field, model_validator

from ella.api.schemas.pydantic.v1 import BaseModel


class LogAssessment(BaseModel):
    allele_id: int
    hgvsc: str
    classification: str
    previous_classification: list[str] | None = None


class LogReport(BaseModel):
    allele_id: int
    hgvsc: str


class CreateInterpretationLog(BaseModel):
    message: str | None = None
    warning_cleared: bool | None = None
    priority: int | None = None
    review_comment: str | None = None

    @model_validator(mode="before")
    def has_content(cls, values: dict[str, Any]):
        if any(values.get(a) is not None for a in cls.model_fields):
            return values
        raise ValueError(f"Must specify at least one of: {', '.join(cls.model_fields.keys())}")


class InterpretationLog(CreateInterpretationLog):
    "Represents one interpretation log item."

    id: int
    date_created: datetime
    user_id: int | None = None
    alleleassessment: dict = Field(default_factory=dict)  # LogAssessment, but empty dict allowed
    allelereport: dict = Field(default_factory=dict)  # LogReport, but empty dict allowed
    editable: bool
