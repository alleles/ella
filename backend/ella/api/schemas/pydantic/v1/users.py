from __future__ import annotations

from typing import TYPE_CHECKING

from ella.api.schemas.pydantic.v1 import BaseModel

if TYPE_CHECKING:
    from ella.api.schemas.pydantic.v1.genepanels import Genepanel  # noqa: TCH004, avoid circular imports


class User(BaseModel):
    id: int
    username: str
    first_name: str
    last_name: str
    full_name: str
    abbrev_name: str
    active: bool
    user_group_name: str


class UserGroup(BaseModel):
    id: int
    name: str
    genepanels: list[Genepanel]
    default_import_genepanel: Genepanel | None
    import_groups: list[str] = []


class UserFull(User):
    def __init__(self, **kwargs):
        # Hack to get the group name from the group object
        # Consolidate models, so that only UserFull is used, and access is done through the group object
        # (user_full.group.name instead of user.user_group_name)
        if "group" in kwargs:
            if hasattr(kwargs["group"], "name"):
                kwargs["user_group_name"] = kwargs["group"].name
            else:
                kwargs["user_group_name"] = kwargs["group"]["name"]
        super().__init__(**kwargs)

    email: str
    password_expiry: str
    group: UserGroup


class OverviewUserStats(BaseModel):
    analyses_cnt: int
    alleles_cnt: int
