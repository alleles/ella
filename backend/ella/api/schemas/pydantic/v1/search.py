from __future__ import annotations

from ella.api.schemas.pydantic.v1 import BaseModel
from ella.api.schemas.pydantic.v1.alleles import Allele
from ella.api.schemas.pydantic.v1.analyses import OverviewAnalysis
from ella.api.schemas.pydantic.v1.workflow import AlleleInterpretationOverview


class SearchGene(BaseModel):
    symbol: str
    hgnc_id: int


class SearchUser(BaseModel):
    username: str
    first_name: str
    last_name: str


class SearchOptions(BaseModel):
    gene: list[SearchGene] | None = None
    user: list[SearchUser] | None = None


class SearchAllele(BaseModel):
    allele: Allele
    interpretations: list[AlleleInterpretationOverview]


class SearchResults(BaseModel):
    alleles: list[SearchAllele]
    analyses: list[OverviewAnalysis]
