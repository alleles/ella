from datetime import datetime

from ella.api.schemas.pydantic.v1 import BaseModel


class Broadcast(BaseModel):
    id: int
    date: datetime
    message: str
