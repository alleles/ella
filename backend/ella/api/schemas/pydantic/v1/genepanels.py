from __future__ import annotations

from ella.api.schemas.pydantic.v1 import BaseModel
from ella.api.schemas.pydantic.v1.common import GenericID
from ella.api.util.types import GenomeReference, StrandSymbol  # noqa: TCH001


class Gene(BaseModel):
    hgnc_id: int
    hgnc_symbol: str


class Transcript(BaseModel):
    transcript_name: str
    gene: Gene


class TranscriptFull(Transcript):
    genome_reference: GenomeReference
    chromosome: str
    tx_start: int
    tx_end: int
    strand: StrandSymbol
    cds_start: int | None
    cds_end: int | None
    exon_starts: list[int]
    exon_ends: list[int]
    tags: list[str] | None


class TranscriptAlternative(BaseModel):
    id: int
    transcript_name: str
    tags: list[str] | None


class PhenotypeBasic(BaseModel):
    id: int
    inheritance: str


class Phenotype(PhenotypeBasic):
    gene: Gene


class PhenotypeAlternative(PhenotypeBasic):
    description: str


class PhenotypeFull(PhenotypeBasic):
    description: str
    omim_id: int
    gene: Gene


class Region(BaseModel):
    id: int
    chromosome: str
    start: int
    end: int
    name: str


class GenepanelBasic(BaseModel):
    name: str
    version: str


class Genepanel(GenepanelBasic):
    official: bool


# uses Full submodels
class GenepanelFull(Genepanel):
    "Panel of genes connected to a certain analysis"

    transcripts: list[TranscriptFull]
    phenotypes: list[PhenotypeFull]


# uses non-Full submodels
class GenepanelTranscripts(Genepanel):
    "Panel of genes connected to a certain analysis"

    transcripts: list[Transcript]
    phenotypes: list[Phenotype]


class GeneAlternative(Gene):
    phenotypes: list[PhenotypeAlternative]
    transcripts: list[TranscriptAlternative]
    inheritance: str


class GenepanelSingle(GenepanelBasic):
    genes: list[GeneAlternative]
    regions: list[Region]


class GenepanelStats(BaseModel):
    name: str
    version: str
    addition_cnt: int
    overlap_cnt: int
    missing_cnt: int


class NewGenepanelGene(BaseModel):
    transcripts: list[GenericID]
    phenotypes: list[GenericID]


# Import here to avoid circular import issues with GeneAssessment -> User -> Genepanel
from ella.api.schemas.pydantic.v1.gene_assessments import GeneAssessment  # noqa: E402, TCH001


class Inheritances(BaseModel):
    inheritance: str
    transcript_name: str
    hgnc_id: int


class GenepanelFullAssessmentsInheritances(GenepanelFull):
    geneassessments: list[GeneAssessment]
    inheritances: list[Inheritances]
