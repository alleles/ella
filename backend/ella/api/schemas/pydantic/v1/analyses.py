from __future__ import annotations

from datetime import datetime

from pydantic import Field

from ella.api.schemas.pydantic.v1 import BaseModel
from ella.api.schemas.pydantic.v1.genepanels import Genepanel
from ella.api.schemas.pydantic.v1.samples import Sample
from ella.api.schemas.pydantic.v1.workflow import AnalysisInterpretationOverview


class AnalysisIdName(BaseModel):
    id: int
    name: str


class AnalysisStats(BaseModel):
    allele_count: int


class Analysis(BaseModel):
    id: int
    name: str
    date_requested: datetime | None
    date_deposited: datetime
    interpretations: list[AnalysisInterpretationOverview] = Field(default_factory=list)
    genepanel: Genepanel
    samples: list[Sample]
    report: str | None = None
    warnings: str | None = None
    attachments: list[str] = Field(default_factory=list)


class OverviewAnalysis(Analysis):
    priority: int
    review_comment: str | None = None
    warning_cleared: bool | None = None
