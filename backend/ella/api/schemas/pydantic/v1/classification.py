from __future__ import annotations

from pydantic import Field

from ella.api.schemas.pydantic.v1 import BaseModel


class ACMGClassification(BaseModel):
    clazz: int = Field(alias="class")
    classification: str
    message: str
    contributors: list[str]
    meta: dict


class ACMGCode(BaseModel):
    code: str
    source: str | None
    value: list[str] | None = None
    match: list[str] | None = None
    op: str | None = None


class ACMGCodeList(BaseModel):
    codes: list[ACMGCode]
