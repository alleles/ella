from __future__ import annotations

from datetime import datetime

from pydantic import Field

from ella.api.schemas.pydantic.v1 import BaseModel
from ella.api.schemas.pydantic.v1.common import Comment
from ella.api.schemas.pydantic.v1.references import ReferenceAssessment
from ella.api.schemas.pydantic.v1.users import User
from ella.api.util.types import AlleleAssessmentClassification


class IncludedItem(BaseModel):
    op: str | None = None
    code: str
    uuid: str
    match: list[str] | None = None
    source: str | None
    comment: str


class SuggestedItem(BaseModel):
    op: str | None = None
    code: str
    match: list[str] | None = None
    source: str | None = None


class SuggestedAcmg(BaseModel):
    included: list[IncludedItem] = []
    suggested: list[SuggestedItem] = []
    suggested_classification: int | None = None


class AlleleAssessmentEvaluation(BaseModel):
    acmg: SuggestedAcmg = Field(default_factory=SuggestedAcmg)
    external: Comment = Field(default_factory=Comment)
    frequency: Comment = Field(default_factory=Comment)
    reference: Comment = Field(default_factory=Comment)
    prediction: Comment = Field(default_factory=Comment)
    classification: Comment = Field(default_factory=Comment)
    similar: Comment = Field(default_factory=Comment)


class AlleleAssessmentUsergroup(BaseModel):
    id: int
    name: str


class AlleleAssessmentOverview(BaseModel):
    "Represents an assessment of one allele for overview"

    id: int
    date_created: datetime
    classification: AlleleAssessmentClassification
    user: User


class AlleleAssessment(BaseModel):
    "Represents an assessment of one allele"

    id: int
    date_created: datetime
    date_superceeded: datetime | None = None
    allele_id: int
    analysis_id: int | None = None
    genepanel_name: str
    genepanel_version: str
    annotation_id: int | None = None
    custom_annotation_id: int | None = None
    previous_assessment_id: int | None = None
    user_id: int
    usergroup_id: int | None = None
    usergroup: AlleleAssessmentUsergroup | None = None
    user: User
    classification: AlleleAssessmentClassification
    seconds_since_update: float
    evaluation: AlleleAssessmentEvaluation
    attachment_ids: list[int] | None = None


class AlleleAssessmentInput(BaseModel):
    "Represents data to create an allele assessment"

    allele_id: int
    analysis_id: int
    genepanel_name: str
    genepanel_version: str
    classification: AlleleAssessmentClassification
    evaluation: dict | None = None
    referenceassessments: list[ReferenceAssessment] | None = None
