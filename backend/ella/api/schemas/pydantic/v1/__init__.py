from __future__ import annotations

import json
from functools import wraps
from logging import getLogger
from typing import TYPE_CHECKING, Any, ClassVar, override

import pydantic
from pydantic_core import to_jsonable_python

from ella.api.config.config import feature_is_enabled

if TYPE_CHECKING:
    from ella.api.util.types import ResourceMethods

logger = getLogger(__name__)
PydanticBase = pydantic.BaseModel

### Functions


def modify_schema(schema: dict[str, Any], model: type[BaseModel]):
    """
    Delete title from schema. This clutters json2ts output by declaring
    every single field as a separate type. Also allow setting arbitrary schema
    values via an overloadable `_meta` class method. ._meta must return a dict which
    is processed as if it were passed to Config.schema_extra.
    NOTE: it is a shallow dict merge and can clobber pydantic generated values
      ref: https://pydantic-docs.helpmanual.io/usage/schema/#schema-customization
    """
    # remove title of fields
    for field_props in schema.get("properties", {}).values():
        field_props.pop("title", None)

    # mark fields with default values as required in schema
    default_fields = set(
        [f.alias or name for name, f in model.model_fields.items() if f.default is not None]
    )

    if default_fields:
        schema["required"] = sorted(set(schema.get("required", [])) | default_fields)

    custom_props = model._meta()
    if custom_props:
        schema.update(**custom_props)


# best placed just after @authenticate decorator. If @paginate is used on the resource, set paginated=True
# NOTE: there are only 16 uses of @paginate vs. 101 uses of @authenticate, so defaults to paginated=False
def validate_output(model_cls: type[ResourceValidator], paginated: bool = False):
    def _validate_output(func):
        @wraps(func)
        def inner(*args, **kwargs):
            # if pydantic validation not enabled, no-op
            if not feature_is_enabled("pydantic"):
                return func(*args, **kwargs)

            if paginated:
                # @paginate returns data in a different structure than otherwise
                result, http_code, headers = func(*args, **kwargs)
            else:
                result = func(*args, **kwargs)

            try:
                ret = model_cls.model_validate(result)
            except Exception:
                logger.error(
                    f"failed to create {model_cls.__name__} object from {json.dumps(result, default=to_jsonable_python, indent=2)}"
                )
                raise

            # ret will evaluate to True unless it is an instance of EmptyResponse/EmptyRequest
            if not ret:
                return

            if paginated:
                return ret, http_code, headers
            else:
                return ret

        return inner

    return _validate_output


### Classes


class BaseModel(PydanticBase):
    model_config = pydantic.ConfigDict(
        # enable from_attributes so model_validate can be used with arbitrary classes as well as dicts
        from_attributes=True,
        json_schema_extra=modify_schema,
        validate_default=True,
        frozen=True,
        extra="forbid",
        coerce_numbers_to_str=True,
    )

    def dump(self, **kwargs):
        "serializes to json string, then json.loads back in. optional params sent directly to self.json()"
        return json.loads(self.model_dump_json(**kwargs))

    @classmethod
    def _meta(cls) -> dict[str, Any] | None:
        "overload to return custom schema.properties modifications"
        return None


class ResourceValidator(BaseModel):
    model_config = pydantic.ConfigDict(extra=None)

    endpoints: ClassVar[dict[str, ResourceMethods]]

    @override
    @classmethod
    def _meta(cls) -> dict[str, Any]:
        "returns dict used by BaseConfig.schema_extra to customize model schema"
        # keep any doc string description that may already be present
        model_desc = f"{cls.__doc__}\n\n" if cls.__doc__ else ""
        # e.g., POST: /api/v1/alleles
        endpoints_str = "\n".join(f"{v}: {k}" for k, v in cls.endpoints.items())

        return {"description": f"{model_desc}{endpoints_str}"}

    def __bool__(self):
        # Always evaluate to True - only subclasses EmptyRequest/EmpyResponse should evaluate to False
        return True


class ResponseValidator(ResourceValidator):
    ...


class RequestValidator(ResourceValidator):
    ...
