from __future__ import annotations

from datetime import datetime
from typing import Literal

from pydantic import field_validator

from ella.api.schemas.pydantic.v1 import BaseModel
from ella.api.util.types import ReferenceEvalRelevance


class AnnotationReference(BaseModel):
    id: int | None = None
    pubmed_id: int | None = None
    source: str
    source_info: str | None = None


class Reference(BaseModel):
    id: int
    authors: str | None = None
    title: str | None = None
    journal: str | None = None
    abstract: str | None = None
    pubmed_id: int | None = None
    published: bool = True
    year: str | None = None


# NOTE: should ref assessments be stored in their own file?


class ReferenceEvaluation(BaseModel):
    comment: str = ""
    sources: list[str] = []
    relevance: ReferenceEvalRelevance | None = None
    ref_quality: str | None = None
    ref_auth_classification: str | None = None
    ref_segregation: str | None = None
    ref_segregation_quality: str | None = None
    ref_ihc: str | None = None
    ref_msi: str | None = None
    ref_prot: str | None = None
    ref_prediction: str | None = None
    ref_prot_quality: str | None = None
    ref_population_affecteds: str | None = None
    ref_rna: str | None = None
    ref_rna_quality: str | None = None
    ref_aa_overlap: str | None = None
    ref_aa_overlap_same_novel: str | None = None
    ref_domain_overlap: str | None = None
    ref_population_healthy: str | None = None
    ref_prediction_tool: str | None = None
    ref_msi_quality: str | None = None
    ref_aa_overlap_aa: str | None = None
    ref_aa_overlap_sim: str | None = None
    ref_aa_overlap_aa_ref: str | None = None
    ref_aa_overlap_quality: str | None = None
    ref_phase: str | None = None
    ref_ihc_quality: str | None = None
    ref_domain_benign: str | None = None
    ref_de_novo: str | None = None

    @field_validator("sources", mode="before")
    def none_to_empty_list(cls, v):
        return v if v is not None else []

    @field_validator("comment", mode="before")
    def none_to_empty_str(cls, v):
        return v if v is not None else ""


class BaseReferenceAssessment(BaseModel):
    allele_id: int
    reference_id: int
    evaluation: ReferenceEvaluation
    analysis_id: int | None = None
    date_superceeded: datetime | None = None


# TODO: should OptRef... being the default and below StrictRef... ?
#       alternatively, is the stricter model actually needed anywhere?
class ReferenceAssessment(BaseReferenceAssessment):
    id: int
    user_id: int
    date_created: datetime
    genepanel_name: str
    genepanel_version: str


class OptReferenceAssessment(BaseReferenceAssessment):
    id: int | None = None
    user_id: int | None = None
    date_created: datetime | None = None
    genepanel_name: str | None = None
    genepanel_version: str | None = None
    reuse: bool | None = None
    reuseCheckedId: int | None = None


class ReusedReferenceAssessment(BaseModel):
    allele_id: int
    reference_id: int
    reuse: Literal[True]
    reuseCheckedId: int


class NonReusedReferenceAssessment(BaseModel):
    allele_id: int
    reference_id: int
    reuse: Literal[False]
    reuseCheckedId: int
    evaluation: ReferenceEvaluation


class NewReferenceAssessment(BaseModel):
    allele_id: int
    reference_id: int
    evaluation: ReferenceEvaluation
