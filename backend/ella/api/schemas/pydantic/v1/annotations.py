from __future__ import annotations

from datetime import datetime
from typing import TYPE_CHECKING, Literal

from pydantic import ConfigDict, Field

from ella.api.schemas.pydantic.v1 import BaseModel
from ella.api.schemas.pydantic.v1.references import AnnotationReference
from ella.api.schemas.pydantic.v1.users import User
from ella.api.util.types import (
    AnnotationJobStatus,
    AnnotationMode,
    Consequence,
    CustomPredictionCategories,
    IntDict,
    Strand,
    YesNo,
)

if TYPE_CHECKING:
    from pydantic.config import JsonDict

rank_pattern = r"[\d\-]+/[\d\-]+"
# NOTE: these are not enforced by pydantic, but it keeps them in the schema spec like they were before
freq_props: JsonDict = {"[a-zA-Z]*": {"type": "number", "minimum": 0, "maximum": 1.0}}
count_props: JsonDict = {"[a-zA-Z]*": {"type": "integer", "minimum": 0}}
filter_props: JsonDict = {"[a-zA-Z]+": {"type": "array", "items": {"type": "string"}}}


class Frequency(BaseModel):
    freq: dict[str, float] = Field(..., json_schema_extra={"patternProperties": freq_props})
    count: IntDict | None = Field(None, json_schema_extra={"patternProperties": count_props})
    hom: IntDict | None = Field(None, json_schema_extra={"patternProperties": count_props})
    hemi: IntDict | None = Field(None, json_schema_extra={"patternProperties": count_props})
    het: IntDict | None = Field(None, json_schema_extra={"patternProperties": count_props})
    num: IntDict | None = Field(None, json_schema_extra={"patternProperties": count_props})
    filter: dict[str, list[str]] | None = Field(
        None, json_schema_extra={"patternProperties": filter_props}
    )
    indications: dict[str, IntDict] | None = None


class Transcript(BaseModel):
    consequences: list[Consequence]
    hgnc_id: int | None = None
    symbol: str | None = None
    HGVSc: str | None = None
    HGVSc_short: str | None = None
    HGVSc_insertion: str | None = None
    HGVSp: str | None = None
    protein: str | None = None
    strand: Strand
    amino_acids: str | None = None
    dbsnp: list[str] | None = None
    exon: str | None = Field(None, pattern=rank_pattern)
    intron: str | None = Field(None, pattern=rank_pattern)
    codons: str | None = None
    transcript: str
    is_canonical: bool
    exon_distance: int | None = None
    coding_region_distance: int | None = None
    in_last_exon: YesNo
    splice: list | None = None


# minimal model schema from: ella/vardb/datamodel/jsonschemas/annotation_v1.json
# + marshmallow schema: ella/api/schemas/annotations.py:AnnotationSchema
class Annotation(BaseModel):
    model_config = ConfigDict(extra="allow")

    annotation_id: int
    schema_version: int
    annotation_config_id: int
    date_superceeded: datetime | None = None
    annotations: list[dict] | None = None
    references: list[AnnotationReference] | None = None
    frequencies: dict[str, Frequency] | None = None
    transcripts: list[Transcript] | None = None


class Indications(BaseModel):
    keys: list[str] | None = None
    threshold: int | None = None


class ViewItem(BaseModel):
    key: str | None = None
    type: Literal["primitives", "objects"] | None = None
    subsource: str | None = None
    url: str | None = None


class ViewConfig(BaseModel):
    columns: dict[str, str] | None = None
    rows: dict[str, str] | None = None
    key_column: str | None = None
    warnings: str | None = None
    indications: Indications | None = None
    names: dict[str, str] | None = None
    items: list[ViewItem] | None = None


class View(BaseModel):
    section: Literal["external", "frequency", "prediction"]
    template: Literal["keyValue", "itemList", "frequencyDetails", "clinvarDetails"]
    source: str
    title: str | None = None
    url: str | None = None
    url_empty: str | None = None
    config: ViewConfig


class AnnotationConfig(BaseModel):
    id: int
    view: list[View]


class CustomAnnotationAnnotations(BaseModel):
    model_config = ConfigDict(extra="allow")

    prediction: dict[CustomPredictionCategories, str] | None = None
    external: dict[str, str] | None = None
    references: list[AnnotationReference] | None = None


class CustomAnnotation(BaseModel):
    id: int
    allele_id: int
    user_id: int
    annotations: CustomAnnotationAnnotations
    date_superceeded: datetime | None
    date_created: datetime


class AnnotationJobBase(BaseModel):
    task_id: str = ""
    status: AnnotationJobStatus | None = None
    message: str = ""
    status_history: dict = Field(default_factory=dict)
    mode: AnnotationMode | None = None
    data: str | None = None
    sample_id: str | None = None
    properties: dict | None = None
    genepanel_name: str | None = None
    genepanel_version: str | None = None


class AnnotationJob(AnnotationJobBase):
    user_id: int
    id: int
    user: User
    date_submitted: datetime
    date_last_update: datetime


class CreateAnnotationJob(AnnotationJobBase):
    user_id: int | None = None


class AnnotationSample(BaseModel):
    model_config = ConfigDict(extra="allow")

    name: str
