from pydantic import ConfigDict, field_validator

from ella.api.schemas.pydantic.v1 import BaseModel


class Comment(BaseModel):
    comment: str = ""

    @field_validator("comment")
    def none_to_empty_str(cls, v):
        return v if v is not None else ""


class SearchFilter(BaseModel):
    search_string: str


class GenericID(BaseModel):
    id: int

    model_config = ConfigDict(extra="allow")
