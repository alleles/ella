from __future__ import annotations

from typing import Literal

from ella.api.schemas.pydantic.v1 import BaseModel
from ella.api.schemas.pydantic.v1.allele_assessments import SuggestedAcmg
from ella.api.schemas.pydantic.v1.common import Comment
from ella.api.schemas.pydantic.v1.references import (
    NewReferenceAssessment,
    NonReusedReferenceAssessment,
    ReusedReferenceAssessment,
)


class Report(BaseModel):
    included: bool


class Analysis(BaseModel):
    comment: str
    notrelevant: bool | None
    verification: str | None


class Workflow(BaseModel):
    reviewed: bool


class Allelereport(BaseModel):
    evaluation: Comment
    reuseCheckedId: int | None = None


class StateEvaluation(BaseModel):
    acmg: SuggestedAcmg
    external: Comment
    frequency: Comment
    reference: Comment
    prediction: Comment
    classification: Comment
    similar: Comment


class ReusedAlleleAssessment(BaseModel):
    reuse: Literal[True]
    reuseCheckedId: int


class NonReusedAlleleAssessment(BaseModel):
    attachment_ids: list[int]
    classification: str | None  # Can be None if classification is not set in the UI
    evaluation: StateEvaluation
    reuse: Literal[False]
    reuseCheckedId: int


class NewAlleleAssessment(BaseModel):
    attachment_ids: list[int]
    classification: str | None  # Can be None if classification is not set in the UI
    evaluation: StateEvaluation


class ReusedAlleleReport(BaseModel):
    reuse: Literal[True]
    reuseCheckedId: int


class NonReusedAlleleReport(BaseModel):
    evaluation: Comment
    reuse: Literal[False]
    reuseCheckedId: int


class NewAlleleReport(BaseModel):
    evaluation: Comment


class AlleleState(BaseModel):
    allele_id: int
    alleleassessment: NewAlleleAssessment | ReusedAlleleAssessment | NonReusedAlleleAssessment
    allelereport: NewAlleleReport | ReusedAlleleReport | NonReusedAlleleReport
    referenceassessments: list[
        NewReferenceAssessment | ReusedReferenceAssessment | NonReusedReferenceAssessment
    ]
    workflow: Workflow


class AnalysisAlleleState(AlleleState):
    report: Report
    analysis: Analysis
