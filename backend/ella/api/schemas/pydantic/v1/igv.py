from __future__ import annotations

from typing import Literal

from pydantic import ConfigDict, Field

from ella.api.schemas.pydantic.v1 import BaseModel


class IgvSearchItem(BaseModel):
    chromosome: str
    start: int
    end: int


# igv.js handles its own config validation
class IgvConfig(BaseModel):
    model_config = ConfigDict(extra="allow")

    name: str | None
    url: str | None


class TrackConfigBase(BaseModel):
    presets: list[str] = Field(default_factory=list)
    type: Literal["roi"] | None = None
    show: bool = False
    description: str = ""


class TrackConfig(TrackConfigBase):
    applied_rules: list[str] = Field(default_factory=list)
    igv: IgvConfig

    def update_igv(self, cfg: IgvConfig):
        self.igv = IgvConfig(**{**self.igv.dump(exclude_none=True), **cfg.dump(exclude_none=True)})


class TrackConfigDefault(TrackConfigBase):
    limit_to_groups: list[str] | None
    igv: IgvConfig | None
