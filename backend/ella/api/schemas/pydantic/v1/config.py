from __future__ import annotations

from typing import Any

from pydantic import Field

from ella.api.schemas.pydantic.v1 import BaseModel
from ella.api.util.types import (
    AlleleInterpretationWorkflowStatus,
    AnalysisInterpretationWorkflowStatus,
    CustomPredictionCategories,
    OverviewViews,
    SidebarCommentType,
)

# ref: ella/api/config/config.schema.json


class AppConfig(BaseModel):
    links_to_clipboard: bool
    non_production_warning: str | None
    annotation_service: str
    attachment_storage: str | None
    max_upload_size: int
    feature_flags: dict[str, bool] | None = None


class AuthConfig(BaseModel):
    password_expiry_days: int
    password_minimum_length: int
    password_match_groups: list[str]
    password_match_groups_descr: list[str]
    password_num_match_groups: int


class OverviewConfig(BaseModel):
    views: list[OverviewViews]


class AlleleFinalizeRequirementsConfig(BaseModel):
    workflow_status: list[AlleleInterpretationWorkflowStatus]


class AlleleWorkflowConfig(BaseModel):
    finalize_requirements: AlleleFinalizeRequirementsConfig
    can_start: bool = True


class AnalysisFinalizeRequirementsConfig(BaseModel):
    workflow_status: list[AnalysisInterpretationWorkflowStatus] | None = None
    allow_unclassified: bool = True


class AnalysisWorkflowConfig(BaseModel):
    finalize_requirements: AnalysisFinalizeRequirementsConfig
    can_start: bool = True


class WorkflowsConfig(BaseModel):
    allele: AlleleWorkflowConfig | None = None
    analysis: AnalysisWorkflowConfig | None = None


class InterpretationConfig(BaseModel):
    autoIgnoreReferencePubmedIds: list[int]


class CommentTemplates(BaseModel):
    name: str
    template: str
    comment_fields: list[str]


class UserConfig(BaseModel):
    overview: OverviewConfig
    workflows: WorkflowsConfig
    interpretation: InterpretationConfig
    acmg: dict[str, Any] | None = None
    deposit: dict | None = None
    comment_templates: list[CommentTemplates] | None = None


class UserConfigMain(BaseModel):
    auth: AuthConfig
    user_config: UserConfig


class UserConfigAuthenticated(UserConfig, UserConfigMain):
    pass


class DisplayConfig(BaseModel):
    field_1: str = Field(..., alias="1")
    field_2: str = Field(..., alias="2")
    field_3: str = Field(..., alias="3")


class PriorityConfig(BaseModel):
    display: DisplayConfig


class FrequencyGroupsConfig(BaseModel):
    external: dict[str, list[str]]
    internal: dict[str, list[str]]


class Frequencies(BaseModel):
    groups: FrequencyGroupsConfig


class GeneGroupsConfig(BaseModel):
    MMR: list[str]


class ClassificationOptionConfig(BaseModel):
    name: str
    value: str
    outdated_after_days: int | None = None
    include_report: bool | None = None
    include_analysis_with_findings: bool | None = None
    sort_index: int | None = None


class Classification(BaseModel):
    gene_groups: GeneGroupsConfig
    options: list[ClassificationOptionConfig]


class ReportConfig(BaseModel):
    classification_text: dict[str, str]


class TranscriptsConfig(BaseModel):
    inclusion_regex: str
    consequences: list[str]


class IgvReferenceConfig(BaseModel):
    fastaURL: str
    cytobandURL: str


class IgvConfig(BaseModel):
    reference: IgvReferenceConfig
    valid_resource_files: list[str]


class ImportConfig(BaseModel):
    automatic_deposit_with_sample_id: bool
    preimport_script: str


class SimilarAllelesConfig(BaseModel):
    max_variants: int
    max_genomic_distance: int


class CommentTypeConfig(BaseModel):
    unclassified: SidebarCommentType | None
    classified: SidebarCommentType | None
    not_relevant: SidebarCommentType | None
    technical: SidebarCommentType | None


class Sidebar(BaseModel):
    columns: list
    classification_options: dict[str, list[str]]
    comment_type: CommentTypeConfig | None = None
    shade_multiple_in_gene: bool
    narrow_comment: bool | None = None


class AnalysisInterpretationConfig(BaseModel):
    priority: PriorityConfig
    sidebar: dict[str, Sidebar]


class CustomAnnotationPredictionConfig(BaseModel):
    key: CustomPredictionCategories
    name: str
    options: list[list[str]]


class CustomAnnotationExternalConfig(BaseModel):
    key: str
    name: str
    only_for_genes: list[int] | None = None
    url_for_genes: dict[str, str] | None = None
    options: list[list[str]] | None = None
    text: bool | None = None


class CustomAnnotationConfig(BaseModel):
    external: list[CustomAnnotationExternalConfig]
    prediction: list[CustomAnnotationPredictionConfig]


class Formatting(BaseModel):
    operators: dict[str, str]


class Codes(BaseModel):
    pathogenic: list[str]
    benign: list[str]
    other: list[str]


class Explanation(BaseModel):
    short_criteria: str
    sources: list[str]
    criteria: str
    notes: str | None = None


class AcmgConfig(BaseModel):
    formatting: Formatting
    codes: Codes
    explanation: dict[str, Explanation]


class Config(BaseModel):
    app: AppConfig
    user: UserConfigMain | UserConfigAuthenticated
    analysis: AnalysisInterpretationConfig
    frequencies: Frequencies
    classification: Classification
    report: ReportConfig
    transcripts: TranscriptsConfig
    igv: IgvConfig
    import_: ImportConfig = Field(..., alias="import")
    similar_alleles: SimilarAllelesConfig
    custom_annotation: CustomAnnotationConfig
    acmg: AcmgConfig
