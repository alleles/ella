from __future__ import annotations

from datetime import datetime

from pydantic import Field

from ella.api.schemas.pydantic.v1 import BaseModel
from ella.api.schemas.pydantic.v1.common import Comment
from ella.api.schemas.pydantic.v1.users import User


class AlleleReportsUsergroup(BaseModel):
    id: int
    name: str


class AlleleReportEvaluation(Comment):
    pass


class AlleleReport(BaseModel):
    "Represents a clinical report for one allele"

    id: int
    date_created: datetime
    date_superceeded: datetime | None = None
    allele_id: int
    analysis_id: int | None = None
    previous_report_id: int | None = None
    user_id: int
    usergroup_id: int
    usergroup: AlleleReportsUsergroup
    user: User
    seconds_since_update: float
    evaluation: AlleleReportEvaluation = Field(default_factory=AlleleReportEvaluation)
