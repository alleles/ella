from __future__ import annotations

from datetime import datetime

from pydantic import Field

from ella.api.schemas.pydantic.v1 import BaseModel
from ella.api.schemas.pydantic.v1.users import User
from ella.api.schemas.pydantic.v1.workflow_allele_state import AlleleState, AnalysisAlleleState
from ella.api.util.types import (
    AlleleInterpretationWorkflowStatus,
    AnalysisInterpretationWorkflowStatus,
    InterpretationStatus,
    WorkflowStatus,
    WorkflowTypes,
)


class Report(BaseModel):
    included: bool


class Workflow(BaseModel):
    reviewed: bool


class InterpretationBase(BaseModel):
    id: int
    status: InterpretationStatus
    # NOTE: no default set in InterpretationMixin.finalized. Is there a difference between None and False?
    finalized: bool | None = None
    date_last_update: datetime
    genepanel_name: str
    genepanel_version: str
    user: User | None = None
    user_id: int | None = None


class InterpretationSnapshot(BaseModel):
    "snapshot of a allele interpretation with context"

    id: int
    allele_id: int
    alleleassessment_id: int | None = None
    allelereport_id: int | None = None
    annotation_id: int | None = None
    customannotation_id: int | None = None
    date_created: datetime


class AlleleCollision(BaseModel):
    type: WorkflowTypes
    user: User | None
    allele_id: int
    analysis_name: str | None
    analysis_id: int | None = None
    workflow_status: WorkflowStatus


class OngoingWorkflow(BaseModel):
    user_id: int | None
    workflow_status: WorkflowStatus
    allele_id: int
    analysis_id: int | None


# Allele Workflow


class ReportComment(BaseModel):
    comment: str = ""
    indicationscomment: str = ""


class AlleleInterpretationState(BaseModel):
    allele: dict[str, AlleleState] = {}


class AnalysisInterpretationState(BaseModel):
    allele: dict[str, AnalysisAlleleState] = {}
    filterconfigId: int | None = None
    manuallyAddedAlleles: list[int] = []
    report: ReportComment = Field(default_factory=ReportComment)


class AlleleInterpretationBase(InterpretationBase):
    workflow_status: AlleleInterpretationWorkflowStatus


# Ref datamodel.workflow.AlleleInterpretation, schemas.alleleinterpretations.AlleleInterpretationSchema
class AlleleInterpretation(AlleleInterpretationBase):
    "Represents one round of interpretation of an allele"

    state: AlleleInterpretationState = Field(default_factory=AlleleInterpretationState)
    user_state: dict | None
    date_created: datetime


class AlleleInterpretationOverview(AlleleInterpretationBase):
    "Represents one round of interpretation of an allele. Overview data fields only."

    allele_id: int


class AlleleInterpretationSnapshot(InterpretationSnapshot):
    "snapshot of a allele interpretation with context"

    alleleinterpretation_id: int


# Analysis Workflow


class AnalysisInterpretationBase(InterpretationBase):
    workflow_status: AnalysisInterpretationWorkflowStatus


class AnalysisInterpretation(AnalysisInterpretationBase):
    "Represents one round of interpretation of an analysis"

    state: AnalysisInterpretationState = Field(default_factory=AnalysisInterpretationState)
    user_state: dict | None
    date_created: datetime | None = None


class AnalysisInterpretationOverview(AnalysisInterpretationBase):
    "Represents one round of interpretation of an analysis. Overview data fields only."

    analysis_id: int


class AnalysisInterpretationSnapshot(InterpretationSnapshot):
    "snapshot of a allele interpretation with context"

    analysisinterpretation_id: int
