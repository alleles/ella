from __future__ import annotations

from typing import Any

from pydantic import field_validator

from ella.api.schemas.pydantic.v1 import BaseModel
from ella.api.util.types import FilterNames


class FilterModel(BaseModel):
    name: FilterNames
    config: dict
    exceptions: list[FilterModel] | None = None

    @field_validator("exceptions", mode="before")
    @classmethod
    def validate_exceptions(cls, exceptions: list[dict[str, Any]] | None):
        if exceptions:
            for exception in exceptions:
                if "exceptions" in exception and exception["exceptions"] is not None:
                    raise ValueError("Exceptions cannot contain further exceptions.")
        return exceptions


class Filters(BaseModel):
    filters: list[FilterModel]


class FilterConfig(BaseModel):
    id: int
    name: str
    filterconfig: Filters
    active: bool
    requirements: list
