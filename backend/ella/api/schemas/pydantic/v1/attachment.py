from __future__ import annotations

from datetime import datetime

from ella.api.schemas.pydantic.v1 import BaseModel
from ella.api.schemas.pydantic.v1.users import User


class Attachment(BaseModel):
    id: int
    sha256: str | None = None
    filename: str
    size: int
    date_created: datetime
    mimetype: str | None = None
    extension: str
    thumbnail: str | None = None
    user: User
