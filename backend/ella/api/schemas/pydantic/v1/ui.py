from __future__ import annotations


from pydantic import Field

from ella.api.schemas.pydantic.v1 import BaseModel


class ExceptionLog(BaseModel):
    id: int
    time: str
    usersession_id: int
    message: str
    location: str
    stacktrace: str
    state: dict = Field(default_factory=dict)
