from __future__ import annotations

from datetime import datetime

from ella.api.schemas.pydantic.v1 import BaseModel
from ella.api.schemas.pydantic.v1.genotypes import GenotypeSampleData
from ella.api.util.types import SampleSex, SampleType


class Sample(BaseModel):
    "Represents one sample. There can be many samples per analysis."

    id: int
    sex: SampleSex | None = None
    identifier: str
    sample_type: SampleType
    date_deposited: datetime
    affected: bool
    proband: bool
    family_id: str | None = None
    father_id: int | None = None
    father: Sample | None = None
    mother_id: int | None = None
    mother: Sample | None = None
    sibling_id: int | None = None
    siblings: list[Sample] | None = None
    genotype: GenotypeSampleData | None = None


Sample.model_rebuild()
