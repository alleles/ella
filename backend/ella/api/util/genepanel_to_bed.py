from collections import OrderedDict

from sqlalchemy import select, tuple_
from sqlalchemy.orm import Session

from ella.vardb.datamodel import gene

BED_COLUMNS: dict = OrderedDict()
BED_COLUMNS["#chromosome"] = lambda t: t.chromosome
BED_COLUMNS["txStart"] = lambda t: str(t.tx_start)
BED_COLUMNS["txEnd"] = lambda t: str(t.tx_end)


def genepanel_to_bed(session: Session, genepanel_name: str, genepanel_version: str):
    genepanel = session.execute(
        select(gene.Genepanel).where(
            tuple_(gene.Genepanel.name, gene.Genepanel.version)
            == (genepanel_name, genepanel_version)
        )
    ).scalar_one()

    bed_data = "\t".join(list(BED_COLUMNS.keys()))

    for t in genepanel.transcripts:
        row = [v(t) for v in list(BED_COLUMNS.values())]
        bed_data += "\n" + "\t".join(row)

    return bed_data
