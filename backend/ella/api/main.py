import json
import os
import sys
import time
from datetime import datetime
from pathlib import Path
from typing import Any

import pytz
from flask import Response, g, make_response
from flask_restful import Api
from pydantic_core import to_jsonable_python

from ella.api import app, db
from ella.api.schemas.pydantic.v1 import BaseModel
from ella.api.util.util import log_request, populate_g_logging, populate_g_user
from ella.api.v1 import ApiV1

sys.path.append(Path(__file__).parents[1].as_posix())

KEYWORD_DEVELOPER_MODE = "DEVELOP"
SCRIPT_DIR = Path(__file__).parent.resolve()

log = app.logger


@app.before_request
def populate_request():
    g.request_start_time = time.time() * 1000.0
    g.timestamp = datetime.now(pytz.utc)
    populate_g_logging()
    populate_g_user()


@app.after_request
def after_request(response: Response):
    log_request(response.status_code, response)
    try:
        db.session.commit()
    except Exception:
        log.exception("Something went wrong when commiting resourcelog entry")
        db.session.rollback()

    # Allow a different front-end running on port 3000 to make requests to the API while developing.
    is_dev = os.getenv(KEYWORD_DEVELOPER_MODE, "").lower() == "true"
    if is_dev:
        response.headers["Access-Control-Allow-Origin"] = os.environ.get(
            "SITE_URL", "http://localhost:3000"
        )
        response.headers["Access-Control-Allow-Credentials"] = "true"
        response.headers["Access-Control-Allow-Headers"] = "Content-Type, Pragma"
        response.headers["Access-Control-Allow-Methods"] = "GET, POST, OPTIONS, PATCH, DELETE"

    return response


@app.teardown_request
def teardown_request(exc):
    if exc:
        # Gunicorn injects a [Errno 11] Resource temporarily unavailable error
        # into every request. We therefore ignore logging this error.
        # https://github.com/benoitc/gunicorn/issues/514
        if hasattr(exc, "errno") and exc.errno == 11:
            return
        log_request(500)
        try:
            db.session.commit()
        except Exception:
            log.exception("Something went wrong when commiting resourcelog entry")
            db.session.rollback()

    # Ensure that sessions are closed - leaving no open transactions (typically in state 'idle in transaction')
    db.session.close()


@app.teardown_appcontext
def shutdown_session(exception=None):
    db.disconnect()


api = Api(app)


# Turn off caching for whole API
@api.representation("application/json")
def output_json(data, code: int, headers: dict[str, Any] | None = None):
    """Makes a Flask response with a JSON encoded body"""

    if isinstance(data, BaseModel):
        # TODO: determine where exclude_none is actually needed for front-end
        json_data = data.model_dump_json(exclude_none=True, by_alias=True)
    else:
        json_data = json.dumps(data, default=to_jsonable_python)
    resp = make_response(json_data, code)
    resp.headers.extend(headers or {})
    if "Cache-Control" not in resp.headers:
        resp.headers[
            "Cache-Control"
        ] = "no-store, no-cache, must-revalidate, post-check=0, pre-check=0"
    return resp


# Setup resources for v1
ApiV1(app, api).setup_api()
