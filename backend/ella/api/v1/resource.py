# -*- coding: latin1 -*-
from typing import Any, TypeVar

import sqlalchemy
from flask_restful import Resource as flask_resource
from marshmallow.schema import Schema
from sqlalchemy import Select, Text, func, select, tuple_
from sqlalchemy.orm.session import Session

from ella.api.schemas.pydantic.v1.common import SearchFilter
from ella.api.util.util import logger, provide_session
from ella.datalayer import filters
from ella.vardb.datamodel import Base, assessment

FILTER_OPERATORS = {
    # Operators which accept two arguments.
    "$eq": lambda f, a: f == a,
    "$neq": lambda f, a: f != a,
    "$gt": lambda f, a: f > a,
    "$lt": lambda f, a: f < a,
    "$gte": lambda f, a: f >= a,
    "$lte": lambda f, a: f <= a,
    "$in": lambda f, a: filters.in_(f, a),
    "$nin": lambda f, a: ~filters.in_(f, a),
    "$like": lambda f, a: f.cast(Text).like(a),
    "$ilike": lambda f, a: f.cast(Text).ilike(a),
}

T = TypeVar("T", bound=Select)


class Resource(flask_resource):
    method_decorators = [logger(hide_payload=True), provide_session]

    def _filter(self, query: T, model, rest_filter) -> T:
        args = list()
        for k, v in rest_filter.items():
            if isinstance(v, list):
                operator = FILTER_OPERATORS["$in"]
                if v:  # Asking for empty list doesn't make sense
                    if isinstance(k, list | tuple):
                        args.append(operator(tuple_(*(getattr(model, _k) for _k in k)), v))
                    else:
                        args.append(operator(getattr(model, k), v))
            elif isinstance(v, dict):
                for op_k, op_v in v.items():
                    args.append(FILTER_OPERATORS[op_k](getattr(model, k), op_v))
            else:
                args.append(FILTER_OPERATORS["$eq"](getattr(model, k), v))
        if args:
            query = query.where(*args)
        return query

    def list_query(
        self,
        session: Session,
        model: type[Base],
        schema: Schema | None = None,
        rest_filter: dict | None = None,
        order_by: list | None = None,
        distinct: list | bool | None = None,
        per_page: int | None = None,
        page: int | None = None,
    ):
        query = select(model)
        if rest_filter:
            # Check if any of the requested filters are empty list, if so user has requested an empty
            # set so we should return nothing.
            # TODO: Review behavior
            if any((isinstance(v, list) and not v) for v in list(rest_filter.values())):
                return list(), 0
            query = self._filter(query, model, rest_filter)

        count = session.execute(select(func.count()).select_from(query.subquery())).scalar_one()
        if order_by:
            query = query.order_by(*order_by)

        if distinct:
            if distinct is True:
                query = query.distinct()
            else:
                query = query.distinct(*distinct)

        if per_page:
            query = query.limit(per_page)
        if page and per_page:
            query = query.offset((page - 1) * per_page)

        s = session.execute(query).scalars().all()
        if schema:
            # FIXME: many=True is broken when some fields are None
            result = [schema.dump(_s).data for _s in s]
            return result, count
        else:
            return s, count

    def list_search(
        self,
        session: Session,
        model: type[assessment.Reference],
        search_filter: SearchFilter,
        schema: Schema | None = None,
        **kwargs,
    ):
        """Searches only full word matches
        Example: String to search in is 'breast and ovarian cancer'

        Match returned on:
        - breast
        - ovarian
        - breast and ovarian
        - breast and ovar
        - ovar and bre
        - breasts and ovarian

        Match not generally returned on misspelled words:
        - brxast

        However, match is returned on some misspellings:
        - ovariance

        The search string is cast to a tsquery by using plainto_tsquery, which returns
        - SELECT plainto_tsqeuery('breast and ovarian cancer') -> 'breast' & 'ovarian' & 'cancer'
        - SELECT plainto_tsquery('breasts and ovariance cancer'); ->  'breast' & 'ovari' & 'cancer'

        It is executed so that any word in the tsquery is matched with words prefixing, that is
        - 'ovari' matches 'ovarian'

        Possible improvements:
        - Use the similarity-function from the pg_trgm extension, so that e.g. 'stromberg' matches 'Str�hmberg'
        - Unaccent all input, so that e.g. 'strohmberg' matches 'Str�hmberg'
        """

        search_string = search_filter.search_string
        query: Select[Any] = select(model)

        words: str = session.execute(
            select(sqlalchemy.func.plainto_tsquery(search_string))
        ).scalar_one()  # .statement.compile(compile_kwargs={"literal_binds": True})
        word_list = words.replace(" ", "").replace("'", "").replace('"', "").split("&")
        search_string = " & ".join([s + ":*" for s in word_list])
        _search_vector = sqlalchemy.func.to_tsquery(sqlalchemy.text("'english'"), search_string)

        query = query.where(model.search.op("@@")(_search_vector))
        query = query.order_by(sqlalchemy.func.ts_rank(model.search, _search_vector))

        count = session.execute(select(func.count()).select_from(query.subquery())).scalar_one()

        if kwargs.get("per_page"):
            query = query.limit(kwargs["per_page"])
        if kwargs.get("page") and kwargs.get("per_page"):
            query = query.offset((kwargs["page"] - 1) * kwargs["per_page"])

        s = session.execute(query).scalars().all()

        if schema:
            result = schema.dump(s, many=True)
            return result.data, count
        else:
            return s, count


class LogRequestResource(Resource):
    method_decorators = [logger(hide_payload=False), provide_session]
