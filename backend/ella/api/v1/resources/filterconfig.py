from sqlalchemy import select
from sqlalchemy.orm import Session

from ella.api import schemas
from ella.api.schemas.pydantic.v1 import validate_output
from ella.api.schemas.pydantic.v1.resources import FilterConfigResponse
from ella.api.util.util import authenticate
from ella.api.v1.resource import LogRequestResource
from ella.vardb.datamodel.sample import FilterConfig, UserGroupFilterConfig
from ella.vardb.datamodel.user import User


class FilterconfigResource(LogRequestResource):
    @authenticate()
    @validate_output(FilterConfigResponse)
    def get(self, session: Session, filterconfig_id: int, user: User):
        filterconfig = session.execute(
            select(FilterConfig)
            .join(UserGroupFilterConfig)
            .where(
                FilterConfig.id == filterconfig_id,
                UserGroupFilterConfig.usergroup_id == user.group_id,
            )
        ).scalar_one()
        serialized_filterconfig = schemas.FilterConfigSchema().dump(filterconfig).data
        serialized_filterconfig["filterconfig"] = serialized_filterconfig[
            "filterconfig"
        ].model_dump()  # serialize Pydantic model
        return serialized_filterconfig
