from sqlalchemy import select
from sqlalchemy.orm import Session

from ella.api import schemas
from ella.api.schemas.pydantic.v1 import validate_output
from ella.api.schemas.pydantic.v1.resources import AnalysisListResponse, AnalysisResponse
from ella.api.util.util import authenticate, paginate, rest_filter
from ella.api.v1.resource import LogRequestResource
from ella.datalayer import filters
from ella.vardb.datamodel import sample, user


class AnalysisListResource(LogRequestResource):
    @authenticate()
    @validate_output(AnalysisListResponse, paginated=True)
    @paginate
    @rest_filter
    def get(
        self,
        session: Session,
        rest_filter: dict | None,
        page: int,
        per_page: int,
        user: user.User,
        **kwargs,
    ):
        """
        Returns a list of analyses.

        * Supports `q=` filtering.
        * Supports pagination.
        ---
        summary: List analyses
        tags:
          - Analysis
        parameters:
          - name: q
            in: query
            type: string
            description: JSON filter query
        responses:
          200:
            schema:
              type: array
              items:
                $ref: '#/definitions/Analysis'
            description: List of analyses
        """
        if rest_filter is None:
            rest_filter = dict()
        if ("genepanel_name", "genepanel_version") not in rest_filter:
            rest_filter[("genepanel_name", "genepanel_version")] = [
                (gp.name, gp.version) for gp in user.group.genepanels
            ]
        return self.list_query(
            session,
            sample.Analysis,
            schema=schemas.AnalysisSchema(),
            rest_filter=rest_filter,
            per_page=per_page,
            page=page,
        )


class AnalysisResource(LogRequestResource):
    @authenticate()
    @validate_output(AnalysisResponse)
    def get(self, session: Session, analysis_id: int, user: user.User):
        """
        Returns a single analysis.
        ---
        summary: Get analysis
        tags:
          - Analysis
        parameters:
          - name: analysis_id
            in: path
            type: integer
            description: Analysis id
        responses:
          200:
            schema:
                $ref: '#/definitions/Analysis'
            description: Analysis object
        """
        a = session.execute(
            select(sample.Analysis).where(
                sample.Analysis.id == analysis_id,
                filters.in_(
                    (sample.Analysis.genepanel_name, sample.Analysis.genepanel_version),
                    ((gp.name, gp.version) for gp in user.group.genepanels),
                ),
            )
        ).scalar_one()
        analysis = schemas.AnalysisSchema().dump(a).data
        return analysis
