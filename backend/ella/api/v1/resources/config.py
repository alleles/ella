import copy

from flask import request
from sqlalchemy import select
from sqlalchemy.orm import Session

from ella.api.config import config
from ella.api.schemas.annotations import AnnotationConfigSchema
from ella.api.schemas.pydantic.v1 import validate_output
from ella.api.schemas.pydantic.v1.config import UserConfig
from ella.api.schemas.pydantic.v1.resources import AnnotationConfigListResponse, ConfigResponse
from ella.api.util.util import authenticate
from ella.api.v1.resource import LogRequestResource
from ella.datalayer import filters
from ella.vardb.datamodel import annotation


class ConfigResource(LogRequestResource):
    @authenticate(user_config=True, optional=True, pydantic=True)
    @validate_output(ConfigResponse)
    def get(self, user_config: UserConfig | None, **kwargs):
        """
        Returns application configuration.
        ---
        summary: Get config
        tags:
          - Config
        responses:
          200:
            schema:
                type: object
            description: Config object
        """

        c = copy.deepcopy(config)
        if user_config:
            c["user"]["user_config"] = user_config

        return c


class AnnotationConfigListResource(LogRequestResource):
    @authenticate()
    @validate_output(AnnotationConfigListResponse)
    def get(self, session: Session, **kwargs):
        raw_ids: str = request.args.get("annotation_config_ids")
        if raw_ids is None:
            raise ValueError("No annotation_config_ids specified")
        annotation_config_ids = [int(i) for i in raw_ids.split(",")]
        annotation_configs = (
            session.execute(
                select(annotation.AnnotationConfig).where(
                    filters.in_(annotation.AnnotationConfig.id, annotation_config_ids)
                )
            )
            .scalars()
            .all()
        )
        assert len(annotation_config_ids) == len(annotation_configs)
        return [AnnotationConfigSchema().dump(x).data for x in annotation_configs]
