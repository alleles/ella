import json
from collections import defaultdict
from collections.abc import Sequence
from typing import Any

from pydantic import ValidationError
from sqlalchemy import ColumnElement, Select, and_, func, select
from sqlalchemy.orm import Session, defer, joinedload

from ella.api import schemas
from ella.api.schemas.pydantic.v1 import validate_output
from ella.api.schemas.pydantic.v1.alleles import AlleleOverview
from ella.api.schemas.pydantic.v1.resources import (
    OverviewAlleleFinalizedResponse,
    OverviewAlleleResponse,
    OverviewAnalysisFinalizedResponse,
    OverviewAnalysisResponse,
    UserStatsResponse,
)
from ella.api.util.types import AlleleIDGenePanel, GenepanelVersion
from ella.api.util.util import authenticate, log, paginate
from ella.api.v1.resource import LogRequestResource
from ella.datalayer import AlleleDataLoader, filters, queries
from ella.datalayer.workflowcategorization import (
    get_categorized_alleles,
    get_categorized_analyses,
    get_finalized_analysis_ids,
)
from ella.vardb.datamodel import allele, gene, sample, user, workflow


def load_alleles(session: Session, allele_id_genepanel: list[AlleleIDGenePanel]):
    """
    Loads in allele data from AlleleDataLoader for all allele ids given by input structure:

    allele_id_genepanel = [
        (1, ('HBOC', 'v01')),
        ...
    ]

    Returns [
        {
            'genepanel': {...genepanel data...},
            'allele': {...allele data...},
            'date_created': '<dateisoformat>',
            'priority': <int>,
            'review_comment': <str>,
            'interpretations': [{...interpretation_data...}, ...],
        },
        ...
    ]
    """

    # Preload all alleles
    all_allele_ids = [a.allele_id for a in allele_id_genepanel]
    alleles_by_id = dict(
        session.execute(
            select(allele.Allele.id, allele.Allele).where(
                filters.in_(allele.Allele.id, all_allele_ids),
            )
        )
        .tuples()
        .all()
    )

    # Preload interpretations for each allele
    interpretations = (
        session.execute(
            select(workflow.AlleleInterpretation)
            .options(
                defer(workflow.AlleleInterpretation.state),
                defer(workflow.AlleleInterpretation.user_state),
            )
            .where(filters.in_(workflow.AlleleInterpretation.allele_id, all_allele_ids))
            .order_by(workflow.AlleleInterpretation.date_last_update)
        )
        .scalars()
        .all()
    )

    # Preload genepanels
    gp_keys = set([a[1] for a in allele_id_genepanel])
    genepanels = (
        session.execute(
            select(gene.Genepanel).where(
                filters.in_((gene.Genepanel.name, gene.Genepanel.version), gp_keys)
            )
        )
        .scalars()
        .all()
    )

    # Load highest priority for each allele.
    priority_by_allele_id = dict(session.execute(queries.workflow_allele_priority()).all())

    # Load review comments
    review_comment_by_allele_id = dict(
        session.execute(queries.workflow_allele_review_comment()).all()
    )

    # Set structures/loaders
    final_alleles: list[AlleleOverview] = list()
    data_loader = AlleleDataLoader(session)
    alleleinterpretation_schema = schemas.AlleleInterpretationOverviewSchema()

    # Create output data
    # Bundle allele_ids with genepanel to increase performance
    gp_allele_ids: defaultdict[GenepanelVersion, set[int]] = defaultdict(set)
    for allele_id, gp_key in allele_id_genepanel:
        gp_allele_ids[gp_key].add(allele_id)

    # for gp_key, allele_ids in sorted(gp_allele_ids.items(), key=lambda x: x[0]):
    for gp_key in sorted(gp_allele_ids):
        allele_ids = gp_allele_ids[gp_key]
        genepanel = next(
            g for g in genepanels if g.name == gp_key.name and g.version == gp_key.version
        )
        gp_alleles = [alleles_by_id[a_id] for a_id in allele_ids]

        loaded_genepanel_alleles = data_loader.from_objs(
            gp_alleles,
            genepanel=genepanel,
            include_allele_assessment=True,  # Display existing class in overview
            # Rest is extra data not needed for our use cases here
            include_custom_annotation=False,
            include_reference_assessments=False,
            include_allele_report=False,
            allele_assessment_schema=schemas.AlleleAssessmentOverviewSchema,
        )

        for allele_dict in loaded_genepanel_alleles:
            allele_interpretations = [
                i for i in interpretations if i.allele_id == allele_dict["id"]
            ]
            dumped_interpretations = [
                alleleinterpretation_schema.dump(i).data for i in allele_interpretations
            ]
            allele_overview_dict = dict(
                genepanel={"name": genepanel.name, "version": genepanel.version},
                allele=allele_dict,
                date_created=min([i.date_created for i in allele_interpretations]).isoformat(),
                priority=priority_by_allele_id.get(allele_dict["id"], 1),
                review_comment=review_comment_by_allele_id.get(allele_dict["id"], None),
                interpretations=dumped_interpretations,
            )
            try:
                allele_overview = AlleleOverview.model_validate(allele_overview_dict)
            except ValidationError:
                log.error(
                    f"Failed to create AlleleOverview object from {json.dumps(allele_overview_dict)}"
                )
                raise

            final_alleles.append(allele_overview)

    return final_alleles


def load_analyses(
    session: Session, analysis_ids: Sequence[int], user: user.User, keep_input_order: bool = False
):
    """
    Loads in analysis data for all analysis ids given in input.
    Analyses are further restricted to the access for the provided user.


    Returns [
        {
            <AnalysisSchema> +
            "review_comment": <str>,
            "priority": <int>,
            "warning_cleared": <bool>
        },
        ...
    ]
    """
    analysis_schema = schemas.AnalysisSchema()

    user_analysis_ids = queries.analysis_ids_for_user(user)

    analyses_select = (
        select(sample.Analysis)
        .options(
            joinedload(sample.Analysis.interpretations)
            .defer(workflow.AnalysisInterpretation.state)
            .defer(workflow.AnalysisInterpretation.user_state)
        )
        .where(
            filters.in_(sample.Analysis.id, user_analysis_ids),
            filters.in_(sample.Analysis.id, analysis_ids),
        )
    )

    if not keep_input_order:
        analyses_select = analyses_select.order_by(
            func.coalesce(sample.Analysis.date_requested, sample.Analysis.date_deposited).desc()
        )

    # FIXME: many=True is broken when some fields (date_requested) are None
    analyses = session.execute(analyses_select).unique().scalars().all()
    loaded_analyses: list[dict[str, Any]] = [analysis_schema.dump(a).data for a in analyses]
    if keep_input_order:
        loaded_analyses.sort(key=lambda x: analysis_ids.index(x["id"]))

    # Load in priority, warning_cleared and review_comment
    analysis_ids = [a.id for a in analyses]
    priorities = session.execute(queries.workflow_analyses_priority(analysis_ids)).all()
    review_comments = session.execute(queries.workflow_analyses_review_comment(analysis_ids)).all()
    warnings_cleared = session.execute(
        queries.workflow_analyses_warning_cleared(analysis_ids)
    ).all()

    for analysis in loaded_analyses:
        priority = next((p.priority for p in priorities if p.analysis_id == analysis["id"]), 1)
        analysis["priority"] = priority
        review_comment = next(
            (rc.review_comment for rc in review_comments if rc.analysis_id == analysis["id"]), None
        )
        if review_comment:
            analysis["review_comment"] = review_comment
        warning_cleared = next(
            (wc.warning_cleared for wc in warnings_cleared if wc.analysis_id == analysis["id"]),
            None,
        )
        if warning_cleared:
            analysis["warning_cleared"] = warning_cleared

    return loaded_analyses


def get_alleleinterpretation_allele_ids_genepanel(
    session: Session, allele_ids: Sequence[int] | Select[tuple[int]]
):
    """
    Creates a list of allele ids with genepanel as matched provided allele_ids.

    Only select the latest interpretation for each allele_id to avoid fetching multiple genepanels
    as they can differ between the interpretations.

    :param session: database session
    :param alleleinterpretation_allele_ids: List of allele ids connected to AlleleInterpretations

    Returns a list of format:
        [
            (1, ('HBOC', 'v01')),
            (2, ('SomethingElse', 'v01')),
        ]
    """

    latest_interpretation = (
        select(workflow.AlleleInterpretation.id)
        .order_by(
            workflow.AlleleInterpretation.allele_id,
            workflow.AlleleInterpretation.date_last_update.desc(),
        )
        .distinct(workflow.AlleleInterpretation.allele_id)  # DISTINCT ON
        .subquery()
    )

    allele_ids_genepanels = (
        select(
            workflow.AlleleInterpretation.genepanel_name,
            workflow.AlleleInterpretation.genepanel_version,
            workflow.AlleleInterpretation.allele_id,
        )
        .join(
            latest_interpretation,
            workflow.AlleleInterpretation.id == latest_interpretation.c.id,
        )
        .where(
            filters.in_(workflow.AlleleInterpretation.allele_id, allele_ids),
        )
        .distinct()
    )

    return [
        AlleleIDGenePanel(a[2], GenepanelVersion(a[0], a[1]))
        for a in session.execute(allele_ids_genepanels).tuples()
    ]


def get_alleles_existing_alleleinterpretation(
    session: Session,
    allele_filter: ColumnElement[bool],
    page: int | None = None,
    per_page: int | None = None,
) -> tuple[Sequence[int], int]:
    """
    Returns allele_ids that has connected AlleleInterpretations,
    given allele_filter from argument.

    Supports pagination.
    """

    # Apply filter using Allele table as base
    allele_ids = select(allele.Allele.id).where(allele_filter)

    # Now get the ones that are actually connected to AlleleInterpretation
    # (distinct allele_ids sorted by date_last_update)
    alleleinterpretation_allele_ids = (
        select(workflow.AlleleInterpretation.allele_id)
        .where(filters.in_(workflow.AlleleInterpretation.allele_id, allele_ids))
        .group_by(workflow.AlleleInterpretation.allele_id)
        .order_by(func.max(workflow.AlleleInterpretation.date_last_update).desc())
    )

    count = session.execute(
        select(func.count()).select_from(alleleinterpretation_allele_ids.subquery())
    ).scalar_one()

    if page and per_page:
        start = (page - 1) * per_page
        end = page * per_page
        alleleinterpretation_allele_ids = alleleinterpretation_allele_ids.slice(start, end)

    return session.execute(alleleinterpretation_allele_ids).scalars().all(), count


class OverviewAlleleResource(LogRequestResource):
    @authenticate()
    @validate_output(OverviewAlleleResponse)
    def get(self, session: Session, user: user.User):
        categorized_allele_ids = get_categorized_alleles(user=user)

        result: dict[str, Any] = dict()
        for key, allele_ids in categorized_allele_ids.items():
            allele_id_genepanels = get_alleleinterpretation_allele_ids_genepanel(
                session, allele_ids
            )
            result[key] = load_alleles(session, allele_id_genepanels)

        return result


class OverviewAlleleFinalizedResource(LogRequestResource):
    @authenticate()
    @validate_output(OverviewAlleleFinalizedResponse, paginated=True)
    @paginate
    def get(self, session: Session, user: user.User, page: int, per_page: int, **kwargs):
        allele_filters = [filters.in_(allele.Allele.id, queries.workflow_alleles_finalized())]
        if user is not None:
            allele_filters.append(
                filters.in_(
                    allele.Allele.id,
                    queries.workflow_alleles_for_genepanels(user.group.genepanels),
                )
            )

        alleleinterpretation_allele_ids, count = get_alleles_existing_alleleinterpretation(
            session, and_(*allele_filters), page=page, per_page=per_page
        )

        allele_ids_genepanel = get_alleleinterpretation_allele_ids_genepanel(
            session, alleleinterpretation_allele_ids
        )

        result = load_alleles(session, allele_ids_genepanel)

        # Re-sort result, since the db queries in load_alleles doesn't handle sorting
        result.sort(key=lambda x: alleleinterpretation_allele_ids.index(x.allele.id))
        return result, count


class OverviewAnalysisFinalizedResource(LogRequestResource):
    @authenticate()
    @validate_output(OverviewAnalysisFinalizedResponse, paginated=True)
    @paginate
    def get(self, session: Session, user: user.User, page: int, per_page: int, **kwargs):
        finalized_analysis_ids, count = get_finalized_analysis_ids(
            session, user=user, page=page, per_page=per_page
        )
        loaded_analyses = load_analyses(
            session,
            session.execute(finalized_analysis_ids).scalars().all(),
            user,
            keep_input_order=True,
        )

        return loaded_analyses, count


class OverviewAnalysisResource(LogRequestResource):
    @authenticate()
    @validate_output(OverviewAnalysisResponse)
    def get(self, session: Session, user: user.User):
        categorized_analysis_ids = get_categorized_analyses(user=user)

        result = {}
        for key, analysis_ids in categorized_analysis_ids.items():
            result[key] = load_analyses(session, analysis_ids, user)

        return result


class OverviewUserStatsResource(LogRequestResource):
    @authenticate()
    @validate_output(UserStatsResponse)
    def get(self, session: Session, user: user.User):
        analyses_cnt = session.execute(
            select(func.count(sample.Analysis.id))
            .join(workflow.AnalysisInterpretation)
            .where(workflow.AnalysisInterpretation.user_id == user.id)
            .distinct()
        ).scalar_one()

        alleles_cnt = session.execute(
            select(func.count(allele.Allele.id))
            .join(workflow.AlleleInterpretation)
            .where(workflow.AlleleInterpretation.user_id == user.id)
            .distinct()
        ).scalar_one()

        return {"analyses_cnt": analyses_cnt, "alleles_cnt": alleles_cnt}
