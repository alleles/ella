import datetime
from collections import defaultdict
from collections.abc import Sequence
from dataclasses import dataclass
from typing import Any, Literal, overload

import pytz
from sqlalchemy import func, literal, select
from sqlalchemy.orm import joinedload
from sqlalchemy.orm.session import Session
from sqlalchemy.sql.schema import Column

from ella.api import ApiError, ConflictError, schemas
from ella.api.schemas.pydantic.v1.config import (
    AlleleFinalizeRequirementsConfig,
    AnalysisFinalizeRequirementsConfig,
    UserConfig,
)
from ella.api.schemas.pydantic.v1.resources import (
    AlleleActionStartRequest,
    CreateInterpretationLogRequest,
    FinalizeAlleleRequest,
    MarkAlleleInterpretationRequest,
    MarkAnalysisInterpretationRequest,
    PatchAlleleInterpretationRequest,
    PatchAnalysisInterpretationRequest,
)
from ella.api.schemas.pydantic.v1.workflow import AlleleCollision, OngoingWorkflow
from ella.api.util.types import FilteredAlleleCategories, WorkflowTypes
from ella.datalayer import (
    AlleleDataLoader,
    AlleleFilter,
    AlleleReportCreator,
    AssessmentCreator,
    SnapshotCreator,
    filters,
    queries,
)
from ella.vardb.datamodel import (
    allele,
    annotation,
    annotationshadow,
    assessment,
    gene,
    genotype,
    sample,
    user,
    workflow,
)

### Magic Metadata

_Interp = workflow.AlleleInterpretation | workflow.AnalysisInterpretation
_InterpType = type[workflow.AlleleInterpretation] | type[workflow.AnalysisInterpretation]
_SnapshotType = (
    type[workflow.AlleleInterpretationSnapshot] | type[workflow.AnalysisInterpretationSnapshot]
)
_InterpSchema = (
    type[schemas.AlleleInterpretationSchema] | type[schemas.AnalysisInterpretationSchema]
)

_MarkInterpretationRequest = MarkAlleleInterpretationRequest | MarkAnalysisInterpretationRequest


@dataclass(frozen=True)
class InterpretationMetadata:
    name: WorkflowTypes
    # See https://github.com/python/mypy/issues/5570 for issue relating to union of types with descriptors
    model: _InterpType
    snapshot: _SnapshotType
    schema: _InterpSchema

    @property
    def model_id_field(self) -> Literal["analysis_id", "allele_id"]:
        return f"{self.name}_id"  # type: ignore[return-value]

    @property
    def snapshot_id_field(self) -> Literal["analysisinterpretation_id", "alleleinterpretation_id"]:
        return f"{self.name}interpretation_id"  # type: ignore[return-value]

    @property
    def model_id(self) -> Column:
        "gets Column object for the model type being interpreted"
        return getattr(self.model, self.model_id_field)

    @property
    def snapshot_id(self) -> Column:
        return getattr(self.snapshot, self.snapshot_id_field)

    def get_latest_interpretation(self, session: Session, model_id: int):
        return (
            session.execute(
                select(self.model).where(self.model_id == model_id).order_by(self.model.id.desc())
            )
            .scalars()
            .first()
        )

    def get_interpretation(
        self,
        session: Session,
        interpretation_id: int,
        genepanels: Sequence[gene.Genepanel] | None = None,
    ):
        "returns interpretation by unique id, optionally filtering by Genepanel"
        q = select(self.model).where(self.model.id == interpretation_id)
        if genepanels:
            q = q.where(self._genepanel_filter(session, genepanels))
        return session.execute(q).scalar_one()

    def get_model_interpretations(
        self,
        session: Session,
        model_id: int,
        genepanels: Sequence[gene.Genepanel] | None = None,
    ):
        "gets all interpretations for the model id, optionally filtering by Genepanel"
        q = select(self.model).where(self.model_id == model_id)
        if genepanels:
            q = q.where(self._genepanel_filter(session, genepanels))
        return session.execute(q.order_by(self.model.id)).scalars().all()

    def get_snapshots(self, session: Session, snap_id: int):
        return (
            session.execute(select(self.snapshot).where(self.snapshot_id == snap_id))
            .scalars()
            .all()
        )

    def _genepanel_filter(self, session: Session, genepanels: Sequence[gene.Genepanel]):
        return filters.in_(
            (self.model.genepanel_name, self.model.genepanel_version),
            ((gp.name, gp.version) for gp in genepanels),
        )


AlleleInterpretationMeta = InterpretationMetadata(
    name=WorkflowTypes.ALLELE,
    model=workflow.AlleleInterpretation,
    snapshot=workflow.AlleleInterpretationSnapshot,
    schema=schemas.AlleleInterpretationSchema,
)

AnalysisInterpretationMeta = InterpretationMetadata(
    name=WorkflowTypes.ANALYSIS,
    model=workflow.AnalysisInterpretation,
    snapshot=workflow.AnalysisInterpretationSnapshot,
    schema=schemas.AnalysisInterpretationSchema,
)

###


# explicit keywords required when calling. values given can also refer to *interpretation_id
def _get_interpretation_meta(
    *,
    allele_id: int | None = None,
    analysis_id: int | None = None,
):
    """
    returns id and InterpretationMetadata object for either AlleleInterpretation or AnalysisInterpretation
    """
    if all([allele_id, analysis_id]) or not any([i is not None for i in [allele_id, analysis_id]]):
        raise ValueError("Must specify one of: allele_id, analysis_id")
    elif allele_id is not None:
        return allele_id, AlleleInterpretationMeta
    elif analysis_id is not None:
        return analysis_id, AnalysisInterpretationMeta
    else:
        raise ValueError("This should never happen")


def _get_uncommitted_interpretation_ids(session: Session, meta: InterpretationMetadata) -> set[int]:
    """
    Get all analysis/allele ids for workflows that have an interpretation that may contain
    work that is not commited.
    Criteria:
    1. Include if more than one interpretation and latest is not finalized.
    The first criterion (more than one) is due to single interpretations
    may be in 'Not started' status and we don't want to include those.
    2. In addition, include all Ongoing interpretations. These will naturally
    contain uncommited work. This case is partly covered in 1. above, but not
    for single interpretations.
    """

    more_than_one = (
        select(meta.model_id)
        .group_by(meta.model_id)
        .having(func.count(meta.model_id) > 1)
        .subquery()
    )

    ongoing = set(
        session.execute(
            queries.workflow_by_status(
                meta.model,
                meta.model_id_field,
                status="Ongoing",
            )
        )
        .scalars()
        .all()
    )

    not_finalized = queries.workflow_by_status(
        meta.model, meta.model_id_field, finalized=False
    ).subquery()

    more_than_one_not_finalized = set(
        session.execute(
            select(getattr(more_than_one.c, meta.model_id_field))
            .join(
                not_finalized,
                getattr(not_finalized.c, meta.model_id_field)
                == getattr(more_than_one.c, meta.model_id_field),
            )
            .distinct()
        )
        .scalars()
        .all()
    )

    return ongoing | more_than_one_not_finalized


def get_alleles(
    session: Session,
    allele_ids: Sequence[int],
    genepanels: Sequence[gene.Genepanel],
    *,
    alleleinterpretation_id: int | None = None,
    analysisinterpretation_id: int | None = None,
    current_allele_data: bool = False,
    filterconfig_id: int | None = None,
):
    """
    Loads all alleles for an interpretation. The interpretation model is dynamically chosen
    based on which argument (alleleinterpretation_id, analysisinterpretation_id) is given.

    If current_allele_data is True, load newest allele data instead of allele data
    at time of interpretation snapshot.

    By default, the latest connected data is loaded (e.g. latest annotations, assessments etc).
    However, if the interpretation is marked as 'Done', it's context is loaded from the snapshot,
    so any annotation, alleleassessments etc for each allele will be what was stored
    at the time of finishing the interpretation.
    """

    alleles = (
        session.execute(select(allele.Allele).where(filters.in_(allele.Allele.id, allele_ids)))
        .scalars()
        .all()
    )

    # Get interpretation to get genepanel and check status
    interpretation_id, meta = _get_interpretation_meta(
        allele_id=alleleinterpretation_id,
        analysis_id=analysisinterpretation_id,
    )

    interpretation = meta.get_interpretation(session, interpretation_id, genepanels)

    link_filter = None  # In case of loading specific data rather than latest available for annotation, custom_annotation etc..
    if not current_allele_data and interpretation.status == "Done":
        # Serve using context data from snapshot
        snapshots = meta.get_snapshots(session, interpretation.id)

        link_filter = {
            "annotation_id": [s.annotation_id for s in snapshots if s.annotation_id is not None],
            "customannotation_id": [
                s.customannotation_id for s in snapshots if s.customannotation_id is not None
            ],
            "alleleassessment_id": [
                s.alleleassessment_id for s in snapshots if s.alleleassessment_id is not None
            ],
            "allelereport_id": [
                s.allelereport_id for s in snapshots if s.allelereport_id is not None
            ],
        }

        # For historical referenceassessments, they should all be connected to the alleleassessments used
        referenceassessment_ids = (
            session.execute(
                select(assessment.ReferenceAssessment.id)
                .join(assessment.AlleleAssessment.referenceassessments)
                .where(
                    filters.in_(assessment.AlleleAssessment.id, link_filter["alleleassessment_id"])
                )
            )
            .scalars()
            .all()
        )
        link_filter["referenceassessment_id"] = list(referenceassessment_ids)

    # Only relevant for analysisinterpretation: Include the genotype for connected samples
    analysis_id = None
    if analysisinterpretation_id is not None:
        analysis_id = interpretation.analysis_id

    kwargs = {
        "include_annotation": True,
        "include_custom_annotation": True,
        "genepanel": interpretation.genepanel,
        "analysis_id": analysis_id,
        "link_filter": link_filter,
        "filterconfig_id": filterconfig_id,
    }

    return AlleleDataLoader(session).from_objs(alleles, **kwargs)


def load_genepanel_for_allele_ids(
    session: Session,
    allele_ids: Sequence[int],
    gp_name: str,
    gp_version: str,
):
    """
    Loads genepanel data using input allele_ids as filter
    for what transcripts and phenotypes to include.
    """
    genepanel = session.execute(
        select(gene.Genepanel).where(
            gene.Genepanel.name == gp_name, gene.Genepanel.version == gp_version
        )
    ).scalar_one()

    hgnc_ids = (
        session.execute(
            select(annotationshadow.AnnotationShadowTranscript.hgnc_id).where(
                filters.in_(annotationshadow.AnnotationShadowTranscript.allele_id, allele_ids)
            )
        )
        .scalars()
        .all()
    )

    annotation_transcripts_genepanel = queries.annotation_transcripts_genepanel(
        session, [(gp_name, gp_version)], allele_ids=allele_ids
    ).subquery()

    transcripts = (
        session.execute(
            select(gene.Transcript)
            .options(joinedload(gene.Transcript.gene))
            .join(gene.Genepanel.transcripts)
            .where(
                gene.Transcript.transcript_name
                == annotation_transcripts_genepanel.c.genepanel_transcript,
            )
            .distinct()
        )
        .scalars()
        .all()
    )

    phenotypes = (
        session.execute(
            select(gene.Phenotype)
            .options(joinedload(gene.Phenotype.gene))
            .join(gene.GenepanelPhenotype)
            .where(
                filters.in_(gene.Phenotype.gene_id, hgnc_ids),
                gene.GenepanelPhenotype.genepanel_name == gp_name,
                gene.GenepanelPhenotype.genepanel_version == gp_version,
            )
        )
        .scalars()
        .all()
    )

    geneassessments = (
        session.execute(
            select(assessment.GeneAssessment).where(
                filters.in_(assessment.GeneAssessment.gene_id, hgnc_ids),
                assessment.GeneAssessment.date_superceeded.is_(None),
            )
        )
        .scalars()
        .all()
    )

    inheritances = (
        session.execute(
            select(
                gene.GenepanelTranscript.inheritance,
                gene.Transcript.gene_id.label("hgnc_id"),
                gene.Transcript.transcript_name,
            )
            .join(
                gene.Transcript,
                gene.Transcript.id == gene.GenepanelTranscript.transcript_id,
            )
            .where(
                filters.in_(gene.Transcript.gene_id, hgnc_ids),
                gene.GenepanelTranscript.genepanel_name == gp_name,
                gene.GenepanelTranscript.genepanel_version == gp_version,
            )
        )
    ).all()

    genepanel_data = schemas.GenepanelSchema().dump(genepanel).data
    genepanel_data["transcripts"] = schemas.TranscriptFullSchema().dump(transcripts, many=True).data
    genepanel_data["inheritances"] = schemas.InheritanceSchema().dump(inheritances, many=True).data
    genepanel_data["phenotypes"] = schemas.PhenotypeFullSchema().dump(phenotypes, many=True).data
    genepanel_data["geneassessments"] = (
        schemas.GeneAssessmentSchema().dump(geneassessments, many=True).data
    )
    return genepanel_data


def update_interpretation(
    session: Session,
    user_id: int,
    data: PatchAlleleInterpretationRequest | PatchAnalysisInterpretationRequest,
    *,
    alleleinterpretation_id: int | None = None,
    analysisinterpretation_id: int | None = None,
):
    """
    Updates the current interpretation inplace.

    **Only allowed for interpretations that are `Ongoing`**

    """
    model_id, meta = _get_interpretation_meta(
        allele_id=alleleinterpretation_id, analysis_id=analysisinterpretation_id
    )
    interpretation = meta.get_interpretation(session, model_id)

    if interpretation.status == "Done":
        raise ConflictError("Cannot PATCH interpretation with status 'DONE'")
    elif interpretation.status == "Not started":
        raise ConflictError(
            "Interpretation not started. Call it's analysis' start action to begin interpretation."
        )

    # Check that user is same as before
    if interpretation.user_id:
        if interpretation.user_id != user_id:
            current_user = session.execute(
                select(user.User).where(user.User.id == user_id)
            ).scalar_one()
            interpretation_user = session.execute(
                select(user.User).where(user.User.id == interpretation.user_id)
            ).scalar_one()
            raise ConflictError(
                f"Interpretation owned by {interpretation_user.first_name}"
                f" {interpretation_user.last_name} cannot be updated by other"
                f" user ({current_user.first_name} {current_user.last_name})"
            )

    # Add current state to history if new state is different:
    if data.state != interpretation.state:
        session.add(
            workflow.InterpretationStateHistory(
                alleleinterpretation_id=alleleinterpretation_id,
                analysisinterpretation_id=analysisinterpretation_id,
                state=interpretation.state,
                user_id=user_id,
            )
        )
    # Overwrite state fields with new values
    interpretation.state = data.state
    interpretation.user_state = data.user_state
    interpretation.date_last_update = datetime.datetime.now(pytz.utc)
    return interpretation


def get_interpretation(
    session: Session,
    genepanels: Sequence[gene.Genepanel],
    user_id: int,
    *,
    alleleinterpretation_id: int | None = None,
    analysisinterpretation_id: int | None = None,
):
    interpretation_id, meta = _get_interpretation_meta(
        allele_id=alleleinterpretation_id, analysis_id=analysisinterpretation_id
    )

    interpretation = meta.get_interpretation(session, interpretation_id, genepanels)
    obj = meta.schema().dump(interpretation).data
    return obj


def get_interpretations(
    session: Session,
    genepanels: Sequence[gene.Genepanel],
    user_id: int,
    *,
    allele_id: int | None = None,
    analysis_id: int | None = None,
    filterconfig_id: int | None = None,
):
    model_id, meta = _get_interpretation_meta(allele_id=allele_id, analysis_id=analysis_id)
    interpretations = meta.get_model_interpretations(session, model_id, genepanels)
    loaded_interpretations = []
    if interpretations:
        loaded_interpretations = meta.schema().dump(interpretations, many=True).data

    return loaded_interpretations


def override_interpretation(
    session: Session,
    user_id: int,
    *,
    workflow_allele_id: int | None = None,
    workflow_analysis_id: int | None = None,
):
    model_id, meta = _get_interpretation_meta(
        allele_id=workflow_allele_id,
        analysis_id=workflow_analysis_id,
    )

    interpretation = meta.get_latest_interpretation(session, model_id)

    # Get user by user id
    new_user = session.execute(select(user.User).where(user.User.id == user_id)).scalar_one()

    if interpretation.status != "Ongoing":
        raise ApiError("Cannot reassign interpretation that is not 'Ongoing'.")

    # db will throw exception if user_id is not a valid id
    # since it's a foreign key
    interpretation.user = new_user
    return interpretation


def start_interpretation(
    session: Session,
    user_id: int,
    data: AlleleActionStartRequest | None,
    *,
    workflow_allele_id: int | None = None,
    workflow_analysis_id: int | None = None,
):
    model_id, meta = _get_interpretation_meta(
        allele_id=workflow_allele_id, analysis_id=workflow_analysis_id
    )
    interpretation = meta.get_latest_interpretation(session, model_id)

    # Get user by username
    start_user = session.execute(select(user.User).where(user.User.id == user_id)).scalar_one()

    if not interpretation:
        interpretation = meta.model()
        setattr(interpretation, meta.model_id.name, model_id)
        session.add(interpretation)
    elif interpretation.status != "Not started":
        raise ApiError(
            f"Cannot start existing interpretation where status = {interpretation.status}"
        )

    # db will throw exception if user_id is not a valid id
    # since it's a foreign key
    interpretation.user = start_user
    interpretation.status = "Ongoing"
    interpretation.date_last_update = datetime.datetime.now(pytz.utc)

    if meta.name is WorkflowTypes.ANALYSIS:
        analysis = session.execute(
            select(sample.Analysis).where(sample.Analysis.id == model_id)
        ).scalar_one()
        interpretation.genepanel = analysis.genepanel
    elif meta.name is WorkflowTypes.ALLELE:
        if data is None:
            raise ValueError("Cannot create allele intepretation without genepanel info")
        # For allele workflow, the user can choose genepanel context for each interpretation
        interpretation.genepanel_name = data.gp_name
        interpretation.genepanel_version = data.gp_version
    else:
        # shouldn't have happen, but just in case
        raise RuntimeError("Missing id argument")

    return interpretation


def mark_interpretation(
    session: Session,
    workflow_status: str,
    data: _MarkInterpretationRequest,
    *,
    workflow_allele_id: int | None = None,
    workflow_analysis_id: int | None = None,
):
    """
    Marks (and copies) an interpretation for a new workflow_status,
    creating Snapshot objects to record history.
    """
    model_id, meta = _get_interpretation_meta(
        allele_id=workflow_allele_id,
        analysis_id=workflow_analysis_id,
    )
    interpretation = meta.get_latest_interpretation(session, model_id)
    assert interpretation is not None, f"No {meta.model.__name__} found for id {model_id}"

    if not interpretation.status == "Ongoing":
        raise ApiError(
            f"Cannot mark as '{workflow_status}' when latest interpretation is not 'Ongoing'"
        )

    excluded_allele_ids = None
    if type(data) is MarkAnalysisInterpretationRequest:
        excluded_allele_ids = data.excluded_allele_ids

    SnapshotCreator(session).insert_from_data(
        data.allele_ids,
        interpretation,
        data.annotation_ids,
        data.custom_annotation_ids,
        data.alleleassessment_ids,
        data.allelereport_ids,
        excluded_allele_ids=excluded_allele_ids,
    )

    interpretation.status = "Done"
    interpretation.finalized = False
    interpretation.date_last_update = datetime.datetime.now(pytz.utc)

    # Create next interpretation
    interpretation_next = meta.model.create_next(interpretation)
    interpretation_next.workflow_status = workflow_status
    session.add(interpretation_next)

    return interpretation, interpretation_next


def marknotready_interpretation(
    session: Session,
    data: _MarkInterpretationRequest,
    *,
    workflow_analysis_id: int | None = None,
):
    return mark_interpretation(
        session,
        "Not ready",
        data,
        workflow_analysis_id=workflow_analysis_id,
    )


def markinterpretation_interpretation(
    session: Session,
    data: _MarkInterpretationRequest,
    *,
    workflow_allele_id: int | None = None,
    workflow_analysis_id: int | None = None,
):
    return mark_interpretation(
        session,
        "Interpretation",
        data,
        workflow_allele_id=workflow_allele_id,
        workflow_analysis_id=workflow_analysis_id,
    )


def markreview_interpretation(
    session: Session,
    data: _MarkInterpretationRequest,
    *,
    workflow_allele_id: int | None = None,
    workflow_analysis_id: int | None = None,
):
    return mark_interpretation(
        session,
        "Review",
        data,
        workflow_allele_id=workflow_allele_id,
        workflow_analysis_id=workflow_analysis_id,
    )


def markmedicalreview_interpretation(
    session: Session,
    data: _MarkInterpretationRequest,
    *,
    workflow_analysis_id: int,
):
    return mark_interpretation(
        session,
        "Medical review",
        data,
        workflow_analysis_id=workflow_analysis_id,
    )


def reopen_interpretation(
    session: Session,
    *,
    workflow_allele_id: int | None = None,
    workflow_analysis_id: int | None = None,
):
    model_id, meta = _get_interpretation_meta(
        allele_id=workflow_allele_id,
        analysis_id=workflow_analysis_id,
    )
    interpretation = meta.get_latest_interpretation(session, model_id)

    if interpretation is None:
        raise ApiError(
            "There are no existing interpretations for this item. Use the start action instead."
        )

    if not interpretation.status == "Done":
        raise ApiError("Interpretation is already 'Not started' or 'Ongoing'. Cannot reopen.")

    # Create next interpretation
    interpretation_next = meta.model.create_next(interpretation)
    interpretation_next.workflow_status = (
        interpretation.workflow_status
    )  # TODO: Where do we want to go?
    session.add(interpretation_next)

    return interpretation, interpretation_next


def finalize_allele(
    session: Session,
    user_id: int,
    usergroup_id: int,
    data: FinalizeAlleleRequest,
    user_config: UserConfig,
    *,
    workflow_allele_id: int | None = None,
    workflow_analysis_id: int | None = None,
):
    """
    Finalizes a single allele.

    This will create any [alleleassessment|referenceassessments|allelereport] objects for the provided allele id (in data).

    **Only works for workflows with a `Ongoing` current interpretation**
    """
    workflow_id, meta = _get_interpretation_meta(
        allele_id=workflow_allele_id, analysis_id=workflow_analysis_id
    )
    interpretation = meta.get_latest_interpretation(session, workflow_id)

    if not interpretation.status == "Ongoing":
        raise ApiError("Cannot finalize allele when latest interpretation is not 'Ongoing'")

    if meta.name is WorkflowTypes.ALLELE:
        assert data.allele_id == workflow_allele_id
    elif meta.name is WorkflowTypes.ANALYSIS:
        assert (
            session.execute(
                select(func.count(allele.Allele.id))
                .join(
                    allele.Allele.genotypes,
                )
                .join(
                    sample.Sample,
                )
                .join(
                    sample.Analysis,
                )
                .where(
                    sample.Analysis.id == workflow_analysis_id,
                    allele.Allele.id == data.allele_id,
                )
                .distinct()
            ).scalar_one()
            == 1
        )

    # Check annotation data
    latest_annotation_id = session.execute(
        select(annotation.Annotation.id).where(
            annotation.Annotation.allele_id == data.allele_id,
            annotation.Annotation.date_superceeded.is_(None),
        )
    ).scalar_one()
    if latest_annotation_id != data.annotation_id:
        raise ApiError(
            "Cannot finalize: provided annotation_id does not match latest annotation id"
        )

    latest_customannotation_id = session.execute(
        select(annotation.CustomAnnotation.id).where(
            annotation.CustomAnnotation.allele_id == data.allele_id,
            annotation.CustomAnnotation.date_superceeded.is_(None),
        )
    ).scalar_one_or_none()

    if latest_customannotation_id != data.custom_annotation_id:
        raise ApiError(
            "Cannot finalize: provided custom_annotation_id does not match latest annotation id"
        )

    # Create/reuse assessments
    assessment_result = AssessmentCreator(session).create_from_data(
        user_id,
        usergroup_id,
        data.allele_id,
        data.annotation_id,
        data.custom_annotation_id,
        interpretation.genepanel_name,
        interpretation.genepanel_version,
        data.alleleassessment.dump(),
        [ra.dump() for ra in data.referenceassessments],
        analysis_id=workflow_analysis_id,
    )

    if assessment_result.created_alleleassessment or assessment_result.created_referenceassessments:
        # Check that we can finalize allele
        finalize_requirements: AlleleFinalizeRequirementsConfig | AnalysisFinalizeRequirementsConfig
        if meta.name is WorkflowTypes.ALLELE:
            finalize_requirements = _get_finalize_reqs(user_config, meta.name)
        elif meta.name is WorkflowTypes.ANALYSIS:
            finalize_requirements = _get_finalize_reqs(user_config, meta.name)
        else:
            raise ValueError(f"Invalid workflow type: {meta.name}")

        if (
            finalize_requirements.workflow_status
            and interpretation.workflow_status not in finalize_requirements.workflow_status
        ):
            raise ApiError(
                f"Cannot finalize: interpretation workflow status is"
                f" {interpretation.workflow_status}, but must be one of:"
                f" {', '.join(finalize_requirements.workflow_status)}"
            )

    alleleassessment = None
    if assessment_result.created_alleleassessment:
        session.add(assessment_result.created_alleleassessment)
    else:
        if assessment_result.created_referenceassessments:
            raise ApiError(
                "Trying to create referenceassessments for allele, while not also creating alleleassessment"
            )

    if assessment_result.created_referenceassessments:
        session.add_all(assessment_result.created_referenceassessments)

    # Create/reuse allelereports
    report_result = AlleleReportCreator(session).create_from_data(
        user_id,
        usergroup_id,
        data.allele_id,
        data.allelereport.dump(),
        alleleassessment,
        analysis_id=workflow_analysis_id,
    )

    if report_result.created_allelereport:
        # Do not check if finalize is allowed, report updates are always allowed
        session.add(report_result.created_allelereport)

    session.flush()

    # Ensure that we created either alleleassessment or allelereport, otherwise finalization
    # makes no sense.
    assert assessment_result.created_alleleassessment or report_result.created_allelereport

    # Create entry in interpretation log
    il_data = {"user_id": user_id, meta.snapshot_id.name: interpretation.id}
    if assessment_result.created_alleleassessment:
        il_data["alleleassessment_id"] = assessment_result.created_alleleassessment.id
    if report_result.created_allelereport:
        il_data["allelereport_id"] = report_result.created_allelereport.id

    il = workflow.InterpretationLog(**il_data)
    session.add(il)

    return {
        "allelereport": schemas.AlleleReportSchema().dump(report_result.allelereport).data,
        "alleleassessment": schemas.AlleleAssessmentSchema()
        .dump(assessment_result.alleleassessment)
        .data,
        "referenceassessments": schemas.ReferenceAssessmentSchema()
        .dump(assessment_result.referenceassessments, many=True)
        .data,
    }


def finalize_workflow(
    session: Session,
    user_id: int,
    data: _MarkInterpretationRequest,
    user_config: UserConfig,
    *,
    workflow_allele_id: int | None = None,
    workflow_analysis_id: int | None = None,
):
    """
    Finalizes an interpretation.

    This sets the allele/analysis' current interpretation's status to `Done`,
    and finalized to True.

    The provided data is recorded in a snapshot to be able to present user
    with the view at end of interpretation round.

    **Only works for analyses with a `Ongoing` current interpretation**
    """
    workflow_id, meta = _get_interpretation_meta(
        allele_id=workflow_allele_id,
        analysis_id=workflow_analysis_id,
    )
    interpretation = meta.get_latest_interpretation(session, workflow_id)

    if interpretation is None:
        raise ApiError(f"No interpretation found for {meta.name} id {workflow_id}")
    elif interpretation.status != "Ongoing":
        raise ApiError("Cannot finalize when latest interpretation is not 'Ongoing'")

    # Sanity checks
    alleleassessment_allele_ids = set(
        session.execute(
            select(assessment.AlleleAssessment.allele_id).where(
                filters.in_(assessment.AlleleAssessment.id, data.alleleassessment_ids)
            )
        )
        .scalars()
        .all()
    )

    allelereport_allele_ids: set[int] = set(
        session.execute(
            select(assessment.AlleleReport.allele_id).where(
                filters.in_(assessment.AlleleReport.id, data.allelereport_ids)
            )
        )
        .scalars()
        .all()
    )

    assert alleleassessment_allele_ids - set(data.allele_ids) == set()
    assert allelereport_allele_ids - set(data.allele_ids) == set()

    # Check that we can finalize
    # There are different criterias deciding when finalization is allowed
    finalize_requirements: AlleleFinalizeRequirementsConfig | AnalysisFinalizeRequirementsConfig
    if meta.name is WorkflowTypes.ALLELE:
        assert type(data) is MarkAlleleInterpretationRequest
        finalize_requirements = _get_finalize_reqs(user_config, meta.name)
        excluded_allele_ids = None
    elif meta.name is WorkflowTypes.ANALYSIS:
        assert type(data) is MarkAnalysisInterpretationRequest
        finalize_requirements = _get_finalize_reqs(user_config, meta.name)
        excluded_allele_ids = data.excluded_allele_ids

        unclassified_allele_ids = (
            set(data.allele_ids)
            - set(data.notrelevant_allele_ids)
            - set(data.technical_allele_ids)
            - alleleassessment_allele_ids
        )
        if finalize_requirements.allow_unclassified is False and unclassified_allele_ids:
            raise ApiError(
                "allow_unclassified is set to false, but some allele ids are missing classification"
            )
    else:
        raise ValueError(f"Invalid workflow: {meta.name}")

    if finalize_requirements.workflow_status is not None and interpretation.workflow_status not in [
        s for s in finalize_requirements.workflow_status
    ]:
        raise ApiError(
            f"Cannot finalize: interpretation workflow status is {interpretation.workflow_status},"
            f" but must be one of: {', '.join(finalize_requirements.workflow_status)}"
        )

    # Create interpretation snapshot objects
    SnapshotCreator(session).insert_from_data(
        data.allele_ids,
        interpretation,
        data.annotation_ids,
        data.custom_annotation_ids,
        data.alleleassessment_ids,
        data.allelereport_ids,
        excluded_allele_ids=excluded_allele_ids,
    )

    # Update interpretation
    interpretation.status = "Done"
    interpretation.finalized = True
    interpretation.date_last_update = datetime.datetime.now(pytz.utc)


def get_workflow_allele_collisions(
    session: Session,
    allele_ids: Sequence[int],
    *,
    analysis_id: int | None = None,
    allele_id: int | None = None,
):
    """
    Check for possible collisions in other allele or analysis workflows,
    which happens to have overlapping alleles with the ids given in 'allele_ids'.

    If you're checking a specific workflow, include the analysis_id or allele_id argument
    to specify which workflow to exclude from the check.
    For instance, if you want to check analysis 3, having e.g. 20 alleles, you don't want
    to include analysis 3 in the collision check as it's not informative
    to see a collision with itself. You would pass in analysis_id=3 to exclude it.

    :note: Alleles with valid alleleassessments are excluded from causing collision.
    """
    workflow_analysis_ids = _get_uncommitted_interpretation_ids(session, AnalysisInterpretationMeta)

    # Exclude "ourself" if applicable
    if analysis_id in workflow_analysis_ids:
        workflow_analysis_ids.remove(analysis_id)

    ongoing: dict[WorkflowTypes, list[OngoingWorkflow]] = {}
    # Get all ongoing analyses connected to provided allele ids
    # For analyses we exclude alleles with valid alleleassessments
    ongoing[WorkflowTypes.ANALYSIS] = [
        OngoingWorkflow.model_validate(row)
        for row in session.execute(
            select(
                workflow.AnalysisInterpretation.user_id,
                workflow.AnalysisInterpretation.workflow_status,
                allele.Allele.id.label("allele_id"),
                workflow.AnalysisInterpretation.analysis_id,
            )
            .join(genotype.Genotype.alleles)
            .join(sample.Sample)
            .join(sample.Analysis)
            .join(workflow.AnalysisInterpretation)
            .where(
                filters.in_(sample.Analysis.id, workflow_analysis_ids),
                filters.in_(allele.Allele.id, allele_ids),
                ~filters.in_(
                    allele.Allele.id,
                    queries.allele_ids_with_valid_alleleassessments(),
                ),
            )
            .distinct(workflow.AnalysisInterpretation.analysis_id, allele.Allele.id)
            .order_by(
                workflow.AnalysisInterpretation.analysis_id,
                allele.Allele.id,
                workflow.AnalysisInterpretation.date_created.desc(),
            )
        )
        .tuples()
        .all()
    ]

    # Allele workflow
    workflow_allele_ids = _get_uncommitted_interpretation_ids(session, AlleleInterpretationMeta)

    # Exclude "ourself" if applicable
    if allele_id in workflow_allele_ids:
        workflow_allele_ids.remove(allele_id)

    ongoing[WorkflowTypes.ALLELE] = [
        OngoingWorkflow.model_validate(row)
        for row in session.execute(
            select(
                workflow.AlleleInterpretation.user_id,
                workflow.AlleleInterpretation.workflow_status,
                workflow.AlleleInterpretation.allele_id,
                literal(None).label("analysis_id"),
            )
            .where(
                filters.in_(workflow.AlleleInterpretation.allele_id, workflow_allele_ids),
                filters.in_(workflow.AlleleInterpretation.allele_id, allele_ids),
            )
            .distinct(workflow.AlleleInterpretation.allele_id)
            .order_by(
                workflow.AlleleInterpretation.allele_id,
                workflow.AlleleInterpretation.date_created.desc(),
            )
        )
        .tuples()
        .all()
    ]

    # Preload users, analysis names
    user_ids: set[int] = set()
    analysis_ids: list[int] = []
    for workflow_type in ongoing:
        for wf in ongoing[workflow_type]:
            if wf.user_id is not None:
                user_ids.add(wf.user_id)
            if workflow_type is WorkflowTypes.ANALYSIS and wf.analysis_id:
                analysis_ids.append(wf.analysis_id)
    user_map: dict[int, dict[str, Any]] = {
        u["id"]: u
        for u in schemas.UserSchema()
        .dump(
            session.execute(select(user.User).where(filters.in_(user.User.id, user_ids)))
            .scalars()
            .all(),
            many=True,
        )
        .data
    }

    # Preload the analysis names
    analysis_name_by_id = {
        row.id: row.name
        for row in session.execute(
            select(sample.Analysis.id, sample.Analysis.name).where(
                filters.in_(sample.Analysis.id, analysis_ids)
            )
        ).all()
    }

    collisions: list[AlleleCollision] = list()
    for workflow_type, workflow_entries in ongoing.items():
        for workflow_obj in workflow_entries:
            # If a workflow is in review, it will have no user assigned...
            collisions.append(
                AlleleCollision.model_validate(
                    {
                        **workflow_obj.dump(exclude_none=True, exclude={"user_id"}),
                        "type": workflow_type,
                        "user": None
                        if workflow_obj.user_id is None
                        else user_map.get(workflow_obj.user_id),
                        "analysis_name": analysis_name_by_id.get(workflow_obj.analysis_id),
                    }
                )
            )

    return collisions


def get_interpretationlog(
    session: Session, user_id: int, *, allele_id: int | None = None, analysis_id: int | None = None
):
    workflow_id, meta = _get_interpretation_meta(allele_id=allele_id, analysis_id=analysis_id)

    latest_interpretation = meta.get_latest_interpretation(session, workflow_id)
    # If there's no interpretations, there cannot be any logs either
    if latest_interpretation is None:
        return {"logs": [], "users": []}

    logs = (
        session.execute(
            select(workflow.InterpretationLog)
            .join(meta.model)
            .where(meta.model_id == workflow_id)
            .order_by(workflow.InterpretationLog.date_created)
        )
        .scalars()
        .all()
    )

    id_field = "alleleinterpretation_id" if allele_id else "analysisinterpretation_id"
    editable = {
        l.id: getattr(l, id_field) == latest_interpretation.id
        and (not latest_interpretation.finalized or latest_interpretation.status != "Done")
        and l.user_id == user_id
        and l.message is not None
        for l in logs
    }
    loaded_logs: list[dict[str, Any]] = schemas.InterpretationLogSchema().dump(logs, many=True).data

    for loaded_log in loaded_logs:
        loaded_log["editable"] = editable[loaded_log["id"]]

    # AlleleAssessments and AlleleReports are special, we need to load more data
    alleleassessment_ids: set[int] = set(
        [l["alleleassessment_id"] for l in loaded_logs if l["alleleassessment_id"]]
    )
    alleleassessments = session.execute(
        select(
            assessment.AlleleAssessment.id,
            assessment.AlleleAssessment.allele_id,
            assessment.AlleleAssessment.classification,
            assessment.AlleleAssessment.previous_assessment_id,
        ).where(filters.in_(assessment.AlleleAssessment.id, alleleassessment_ids))
    ).all()
    alleleassessments_by_id = {a.id: a for a in alleleassessments}

    previous_assessment_ids = [
        a.previous_assessment_id for a in alleleassessments if a.previous_assessment_id
    ]
    previous_alleleassessment_classifications = session.execute(
        select(assessment.AlleleAssessment.id, assessment.AlleleAssessment.classification).where(
            filters.in_(assessment.AlleleAssessment.id, previous_assessment_ids)
        )
    ).all()
    previous_alleleassessment_classifications_by_id = {
        a.id: a.classification for a in previous_alleleassessment_classifications
    }

    allele_ids = [a.allele_id for a in alleleassessments]

    allelereport_ids = set([l["allelereport_id"] for l in loaded_logs if l["allelereport_id"]])
    allelereports = session.execute(
        select(assessment.AlleleReport.id, assessment.AlleleReport.allele_id).where(
            filters.in_(assessment.AlleleReport.id, allelereport_ids)
        )
    ).all()
    allelereports_by_id = {a.id: a for a in allelereports}

    allele_ids.extend([a.allele_id for a in allelereports])
    annotation_genepanel = queries.annotation_transcripts_genepanel(
        session,
        [(latest_interpretation.genepanel_name, latest_interpretation.genepanel_version)],
        allele_ids=allele_ids,
    ).subquery()

    allele_hgvs = session.execute(
        select(
            allele.Allele.id,
            allele.Allele.chromosome,
            allele.Allele.vcf_pos,
            allele.Allele.vcf_ref,
            allele.Allele.vcf_alt,
            annotation_genepanel.c.annotation_symbol,
            annotation_genepanel.c.annotation_hgvsc,
        )
        .outerjoin(annotation_genepanel, annotation_genepanel.c.allele_id == allele.Allele.id)
        .where(filters.in_(allele.Allele.id, allele_ids))
    ).all()

    formatted_allele_by_id: defaultdict[int, list] = defaultdict(list)
    for a in allele_hgvs:
        if a.annotation_hgvsc:
            formatted_allele_by_id[a.id].append(f"{a.annotation_symbol} {a.annotation_hgvsc}")
        else:
            formatted_allele_by_id[a.id].append(
                f"{a.chromosome}:{a.vcf_pos} {a.vcf_ref}>{a.vcf_alt}"
            )

    for loaded_log in loaded_logs:
        alleleassessment_id = loaded_log.pop("alleleassessment_id", None)
        if alleleassessment_id:
            log_alleleassessment = alleleassessments_by_id[alleleassessment_id]
            loaded_log["alleleassessment"] = {
                "allele_id": log_alleleassessment.allele_id,
                "hgvsc": sorted(formatted_allele_by_id[log_alleleassessment.allele_id]),
                "classification": log_alleleassessment.classification,
                "previous_classification": previous_alleleassessment_classifications_by_id[
                    log_alleleassessment.previous_assessment_id
                ]
                if log_alleleassessment.previous_assessment_id
                else None,
            }

        allelereport_id = loaded_log.pop("allelereport_id", None)
        if allelereport_id:
            log_allelereport = allelereports_by_id[allelereport_id]
            loaded_log["allelereport"] = {
                "allele_id": log_allelereport.allele_id,
                "hgvsc": sorted(formatted_allele_by_id[log_allelereport.allele_id]),
            }

    # Load all relevant user data
    user_ids = set([l["user_id"] for l in loaded_logs])
    users = (
        session.execute(select(user.User).where(filters.in_(user.User.id, user_ids)))
        .scalars()
        .all()
    )

    loaded_users = schemas.UserSchema().dump(users, many=True).data

    return {"logs": loaded_logs, "users": loaded_users}


def create_interpretationlog(
    session: Session,
    user_id: int,
    data: CreateInterpretationLogRequest,
    *,
    allele_id: int | None = None,
    analysis_id: int | None = None,
):
    model_id, meta = _get_interpretation_meta(allele_id=allele_id, analysis_id=analysis_id)
    if meta.name is WorkflowTypes.ALLELE and data.warning_cleared is not None:
        raise ApiError("warning_cleared is not supported for alleles as they have no warnings")

    latest_interpretation = meta.get_latest_interpretation(session, model_id)

    if not latest_interpretation:
        # Shouldn't be possible for an analysis
        assert (
            meta.name is WorkflowTypes.ALLELE
        ), f"Failed to find latest interpretation for analysis {model_id}"

        latest_interpretation = workflow.AlleleInterpretation(
            allele_id=allele_id,
            workflow_status="Interpretation",
            status="Not started",
        )
        session.add(latest_interpretation)
        session.flush()

    new_log = data.model_dump()
    new_log[meta.snapshot_id_field] = latest_interpretation.id
    new_log["user_id"] = user_id

    interpretationlog = workflow.InterpretationLog(**new_log)

    session.add(interpretationlog)
    return interpretationlog


def patch_interpretationlog(
    session: Session,
    user_id: int,
    interpretationlog_id: int,
    message: str,
    *,
    allele_id: int | None = None,
    analysis_id: int | None = None,
):
    il = session.execute(
        select(workflow.InterpretationLog).where(
            workflow.InterpretationLog.id == interpretationlog_id
        )
    ).scalar_one()

    if il.user_id != user_id:
        raise ApiError("Cannot edit interpretation log, item doesn't match user's id.")

    model_id, meta = _get_interpretation_meta(allele_id=allele_id, analysis_id=analysis_id)
    latest_interpretation = meta.get_latest_interpretation(session, model_id)

    match = False
    if meta.name is WorkflowTypes.ALLELE:
        match = il.alleleinterpretation_id == latest_interpretation.id
    elif meta.name is WorkflowTypes.ANALYSIS:
        match = il.analysisinterpretation_id == latest_interpretation.id

    if not match:
        raise ApiError("Can only edit entries created for latest interpretation id.")

    il.message = message


def delete_interpretationlog(
    session: Session,
    user_id: int,
    interpretationlog_id: int,
    *,
    allele_id: int | None = None,
    analysis_id: int | None = None,
):
    il = session.execute(
        select(workflow.InterpretationLog).where(
            workflow.InterpretationLog.id == interpretationlog_id
        )
    ).scalar_one()

    if il.user_id != user_id:
        raise ApiError("Cannot delete interpretation log, item doesn't match user's id.")

    model_id, meta = _get_interpretation_meta(allele_id=allele_id, analysis_id=analysis_id)
    latest_interpretation = meta.get_latest_interpretation(session, model_id)

    match = False
    if meta.name is WorkflowTypes.ALLELE:
        match = il.alleleinterpretation_id == latest_interpretation.id
    elif meta.name is WorkflowTypes.ANALYSIS:
        match = il.analysisinterpretation_id == latest_interpretation.id

    if not match:
        raise ApiError("Can only delete entries created for latest interpretation id.")

    session.delete(il)


"""
returns
    {
        "cnv": [1,2,3,4],
        "snv": [5,6]
    }
"""


def fetch_allele_ids_by_caller_type(
    session: Session, excluded_allele_ids: Sequence[int]
) -> dict[Literal["cnv", "snv"], list[int]]:
    return dict(
        session.execute(
            select(allele.Allele.caller_type, func.array_agg(allele.Allele.id))
            .where(filters.in_(allele.Allele.id, excluded_allele_ids))
            .group_by(allele.Allele.caller_type)
        )
        .tuples()
        .all()
    )


def filtered_by_caller_type(
    session: Session, filtered_alleles: dict[Literal["cnv", "snv"], list[int]]
):
    # concatenates all the ids of each
    flattened_ids: list[int] = sum(filtered_alleles.values(), [])
    alleles_by_caller_type = fetch_allele_ids_by_caller_type(session, flattened_ids)
    filtered_by_caller_type: dict[Literal["snv", "cnv"], dict] = {"snv": {}, "cnv": {}}
    for caller_type in filtered_by_caller_type:
        for filter_type in filtered_alleles:
            ids = filtered_alleles[filter_type]
            if len(ids) == 0:
                filtered_by_caller_type[caller_type][filter_type] = []
            else:
                if caller_type in alleles_by_caller_type:
                    active_alleles = alleles_by_caller_type[caller_type]
                    filtered_by_caller_type[caller_type][filter_type] = list(
                        set(active_alleles) & set(ids)
                    )
                else:
                    filtered_by_caller_type[caller_type][filter_type] = []
    return filtered_by_caller_type


# Revert to old way
def get_filtered_alleles(
    session: Session, interpretation: _Interp, filter_config_id: int | None = None
) -> tuple[list[int], dict[str, list[int]] | None]:
    """
    Return filter results for interpretation.
    - If AlleleInterpretation, return only allele id for interpretation (no alleles excluded)
    - If AnalysisInterpretation:
        - filter_config_id not provided: Return snapshot results. Requires interpretation to be 'Done'-
        - filter_config_id provided: Run filter on analysis, and return results.
    """
    if isinstance(interpretation, workflow.AlleleInterpretation):
        return [interpretation.allele_id], None
    elif isinstance(interpretation, workflow.AnalysisInterpretation):
        if filter_config_id is None:
            if interpretation.status != "Done":
                raise RuntimeError("Interpretation is not done, and no filter config is provided.")

            if not interpretation.snapshots:
                # snapshots will be empty if there are no variants
                has_alleles = any(
                    [s.genotypes for s in interpretation.analysis.samples if s.proband]
                )

                if has_alleles:
                    raise RuntimeError("Missing snapshots for interpretation.")

            allele_ids: list[int] = []
            excluded_allele_ids: dict[str, list[int]] = {
                k.value: [] for k in FilteredAlleleCategories
            }

            for snapshot in interpretation.snapshots:
                if snapshot.filtered and snapshot.filtered in FilteredAlleleCategories.__members__:
                    excluded_allele_ids[str(FilteredAlleleCategories[snapshot.filtered])].append(
                        snapshot.allele_id
                    )
                else:
                    allele_ids.append(snapshot.allele_id)

            return allele_ids, excluded_allele_ids
        else:
            analysis_id = interpretation.analysis_id
            analysis_allele_ids = (
                session.execute(
                    select(allele.Allele.id)
                    .join(genotype.Genotype.alleles)
                    .join(sample.Sample)
                    .join(sample.Analysis)
                    .where(sample.Analysis.id == analysis_id)
                )
                .scalars()
                .all()
            )

            af = AlleleFilter(session)
            filter_config = session.execute(
                select(sample.FilterConfig.filterconfig).where(
                    sample.FilterConfig.id == filter_config_id
                )
            ).scalar_one()
            filtered_alleles = af.run_filter_chain(filter_config, analysis_id, analysis_allele_ids)

            return filtered_alleles["allele_ids"], filtered_alleles["excluded_allele_ids"]
    else:
        raise RuntimeError(f"Unknown interpretation type {interpretation}")


@overload
def _get_finalize_reqs(
    cfg: UserConfig, wf_type: Literal[WorkflowTypes.ALLELE]
) -> AlleleFinalizeRequirementsConfig:
    ...


@overload
def _get_finalize_reqs(
    cfg: UserConfig, wf_type: Literal[WorkflowTypes.ANALYSIS]
) -> AnalysisFinalizeRequirementsConfig:
    ...


def _get_finalize_reqs(cfg, wf_type):
    if not cfg.workflows:
        raise ValueError("User config missing: workflows")
    elif wf_type is WorkflowTypes.ALLELE:
        if not cfg.workflows.allele:
            raise ValueError("User config missing: workflows.allele")
        return cfg.workflows.allele.finalize_requirements
    elif wf_type is WorkflowTypes.ANALYSIS:
        if not cfg.workflows.analysis:
            raise ValueError("User config missing: workflows.analysis")
        return cfg.workflows.analysis.finalize_requirements
