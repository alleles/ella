from sqlalchemy import select
from sqlalchemy.orm import Session

from ella.api import schemas
from ella.api.schemas.pydantic.v1 import validate_output
from ella.api.schemas.pydantic.v1.resources import (
    GeneAssessmentListResponse,
    GeneAssessmentPostRequest,
    GeneAssessmentResponse,
)
from ella.api.util.util import authenticate, paginate, request_json, rest_filter
from ella.api.v1.resource import LogRequestResource
from ella.datalayer import GeneAssessmentCreator
from ella.vardb.datamodel import assessment, user


class GeneAssessmentResource(LogRequestResource):
    @authenticate()
    @validate_output(GeneAssessmentResponse)
    def get(self, session: Session, ga_id: int, **kwargs):
        """
        Returns a single geneassessment.
        ---
        summary: Get geneassessment
        tags:
          - GeneAssessment
        parameters:
          - name: ga_id
            in: path
            type: integer
            description: GeneAssessment id
        responses:
          200:
            schema:
                $ref: '#/definitions/GeneAssessment'
            description: GeneAssessment object
        """
        gene_assessment = session.execute(
            select(assessment.GeneAssessment).where(assessment.GeneAssessment.id == ga_id)
        ).scalar_one()
        result = schemas.GeneAssessmentSchema(strict=True).dump(gene_assessment).data
        return result


class GeneAssessmentListResource(LogRequestResource):
    @authenticate()
    @validate_output(GeneAssessmentListResponse, paginated=True)
    @paginate
    @rest_filter
    def get(
        self,
        session: Session,
        rest_filter: dict | None,
        page: int,
        per_page: int,
        **kwargs,
    ):
        """
        Returns a list of geneassessments.

        * Supports `q=` filtering.
        * Supports pagination.
        ---
        summary: List geneassessments
        tags:
          - GeneAssessment
        parameters:
          - name: q
            in: query
            type: string
            description: JSON filter query
        responses:
          200:
            schema:
              type: array
              items:
                $ref: '#/definitions/GeneAssessment'
            description: List of geneassessments
        """
        # TODO: Figure out how to deal with pagination
        return self.list_query(
            session,
            assessment.GeneAssessment,
            schemas.GeneAssessmentSchema(strict=True),
            rest_filter=rest_filter,
            page=page,
            per_page=per_page,
        )

    @authenticate()
    @validate_output(GeneAssessmentResponse)
    @request_json(model=GeneAssessmentPostRequest)
    def post(self, session: Session, data: GeneAssessmentPostRequest, user: user.User):
        ga = GeneAssessmentCreator(session)
        created = ga.create_from_data(
            user.id,
            user.group_id,
            data.gene_id,
            data.genepanel_name,
            data.genepanel_version,
            data.evaluation.dump(),
            data.reuseCheckedId,
            data.analysis_id,
        )

        session.add(created)
        session.commit()
        return schemas.GeneAssessmentSchema().dump(created).data
