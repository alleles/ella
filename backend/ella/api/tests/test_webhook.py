from datetime import datetime
from importlib.metadata import version
from unittest.mock import MagicMock, patch

import pytest
import pytz
from flask import json, jsonify

from ella.api import app
from ella.api.config import config
from ella.api.schemas.pydantic.v1.resources import MarkAnalysisInterpretationRequest
from ella.api.tests.util import DatetimeMock, FlaskClientProxy
from ella.api.util.util import requests, webhook

WEBHOOK_URL = "http://www.mock.com/webhook"
API_PATH = "/api/v1/test/test_webhook"
API_FAILURE_PATH = "/api/v1/test/test_webhook_failure"
DUMMY_PAYLOAD = {"dummy_field": "dummy_value"}


@pytest.fixture(autouse=True)
def webhooks_config(monkeypatch: pytest.MonkeyPatch):
    """Add the webhook configuration to the config."""
    monkeypatch.setitem(
        config,
        "webhooks",
        [
            {
                "endpoint": API_PATH,
                "hook": WEBHOOK_URL,
                "methods": ["POST"],
                "ignore_errors": True,
            }
        ],
    )


@app.route(API_PATH, methods=["POST"])
@webhook()
def api_endpoint():
    return jsonify({"message": "Test api endpoint hit"})


@app.route(API_FAILURE_PATH, methods=["POST"])
@webhook()
def api_endpoint_failure():
    raise Exception("Failure in endpoint")


@patch.object(requests, "post")
def test_webhook(mock_post: MagicMock, client: FlaskClientProxy):
    """Test that the webhook is called with the correct payload."""
    fixed_time = datetime.now(pytz.utc)
    expected_payload = {
        "payload": DUMMY_PAYLOAD,
        "user": {"id": 1, "username": "testuser1"},
        "endpoint": API_PATH,
        "method": "POST",
        "ella_version": version("ella"),
        "timestamp": fixed_time.isoformat(),
    }

    DatetimeMock.fixed_time = fixed_time
    with patch("ella.api.main.datetime", new=DatetimeMock):
        response = client.post(API_PATH, data=DUMMY_PAYLOAD)

    assert response.status_code == 200
    assert json.loads(response.data)["message"] == "Test api endpoint hit"
    mock_post.assert_called_once_with(WEBHOOK_URL, json=expected_payload)


@patch.object(requests, "post")
def test_webhook_endpoint_failure(mock_post: MagicMock, client: FlaskClientProxy):
    """Test that the webhook is not called if the endpoint fails."""
    response = client.post(API_FAILURE_PATH, data=DUMMY_PAYLOAD)

    assert response.status_code == 500
    mock_post.assert_not_called()


@patch.object(requests, "post", side_effect=requests.RequestException("Test exception"))
def test_webhook_failure(
    mock_post: MagicMock, client: FlaskClientProxy, monkeypatch: pytest.MonkeyPatch
):
    """
    Test that a failure in the webhook does not prevent the API endpoint from returning a response
    unless explicitly configured to do so.
    """
    response = client.post(API_PATH, data=DUMMY_PAYLOAD)

    assert response.status_code == 200
    assert json.loads(response.data)["message"] == "Test api endpoint hit"

    monkeypatch.setitem(config["webhooks"][0], "ignore_errors", False)
    response = client.post(API_PATH, data=DUMMY_PAYLOAD)
    assert response.status_code == 500

    assert mock_post.call_count == 2


def test_reported_allele_ids_presence():
    """Test that MarkAnalysisInterpretationRequest contain reported_allele_ids.

    As this field is not used in ELLA, only required by ELLA-Sapio, this test
    ensures that the field is not removed by mistake."""
    model = MarkAnalysisInterpretationRequest.model_validate(
        {
            "allele_ids": [],
            "alleleassessment_ids": [],
            "allelereport_ids": [],
            "annotation_ids": [],
            "custom_annotation_ids": [],
            "excluded_allele_ids": {},
        }
    )
    assert model.reported_allele_ids == []
