import pytest
from sqlalchemy import select
from sqlalchemy.orm.session import Session

from ella.api.tests.util import FlaskClientProxy
from ella.vardb.datamodel import gene, user
from ella.vardb.util.testdatabase import TestDatabase


def test_create_new(test_database: TestDatabase, client: FlaskClientProxy, session: Session):
    test_database.refresh()  # Reset db

    r = client.get("/api/v1/genepanels/")
    r = client.get(f"/api/v1/genepanels/{r.get_json()[0]['name']}/{r.get_json()[0]['version']}/")
    genepanel_to_copy = r.get_json()

    original_name = genepanel_to_copy["name"]
    original_version = genepanel_to_copy["version"]

    genepanel_to_copy["name"] = "NewPanel"
    genepanel_to_copy["version"] = "NewVersion"
    genepanel_to_copy["usergroups"] = ["testgroup01", "testgroup02"]
    r = client.post("/api/v1/genepanels/", genepanel_to_copy)
    assert r.status_code == 200

    # Check that panel is correctly created
    original = session.execute(
        select(gene.Genepanel).where(
            gene.Genepanel.name == original_name, gene.Genepanel.version == original_version
        )
    ).scalar_one()

    created = session.execute(
        select(gene.Genepanel).where(
            gene.Genepanel.name == genepanel_to_copy["name"],
            gene.Genepanel.version == genepanel_to_copy["version"],
        )
    ).scalar_one()

    original_phenotype_ids = [p.id for p in original.phenotypes]
    created_phenotype_ids = [p.id for p in created.phenotypes]
    assert set(original_phenotype_ids) == set(created_phenotype_ids)

    original_transcript_ids = [p.id for p in original.transcripts]
    created_transcript_ids = [p.id for p in created.transcripts]
    assert set(original_transcript_ids) == set(created_transcript_ids)

    assert created.name == genepanel_to_copy["name"]
    assert created.version == genepanel_to_copy["version"]

    # Check that groups are correct
    original_usergroups = (
        session.execute(
            select(user.UserGroup.name)
            .join(
                user.UserGroupGenepanel,
                user.UserGroupGenepanel.usergroup_id == user.UserGroup.id,
            )
            .where(
                user.UserGroupGenepanel.genepanel_name == original_name,
                user.UserGroupGenepanel.genepanel_version == original_version,
            )
        )
        .scalars()
        .all()
    )

    assert set(original_usergroups) == set(["testgroup01", "testgroup03"])

    created_usergroups = (
        session.execute(
            select(user.UserGroup.name)
            .join(
                user.UserGroupGenepanel,
                user.UserGroupGenepanel.usergroup_id == user.UserGroup.id,
            )
            .where(
                user.UserGroupGenepanel.genepanel_name == genepanel_to_copy["name"],
                user.UserGroupGenepanel.genepanel_version == genepanel_to_copy["version"],
            )
        )
        .scalars()
        .all()
    )

    assert set(created_usergroups) == set(genepanel_to_copy["usergroups"])

    # Check testgroup01 not allowed to import to testgroup03
    genepanel_to_copy["name"] = "NewPanel2"
    genepanel_to_copy["version"] = "NewVersion2"
    genepanel_to_copy["usergroups"] = ["testgroup01", "testgroup03"]
    with pytest.raises(AssertionError):
        r = client.post("/api/v1/genepanels/", genepanel_to_copy)
        assert r.status_code == 200


def test_genepanel_stats(session, client):
    """
    Simple test of /api/v1/genepanels/<gp_name>/<gp_version>/stats/

    Non-essential piece of code, so keep it simple
    """
    r = client.get("/api/v1/genepanels/HBOCUTV/v1.0.0/stats/")
    # {'overlap': [{'name': 'HBOC', 'version': 'v1.0.0', 'addition_cnt': 0, 'overlap_cnt': 2, 'missing_cnt': 5}]}
    assert r.json["overlap"]

    # Shouldn't contain itself in the overlap
    with pytest.raises(StopIteration):
        next(x for x in r.json["overlap"] if x["name"] == "HBOCUTV")

    hboc_overlap = next(x for x in r.json["overlap"] if x["name"] == "HBOC")
    assert hboc_overlap["missing_cnt"] == 5
    assert hboc_overlap["addition_cnt"] == 0
    assert hboc_overlap["overlap_cnt"] == 2

    r = client.get("/api/v1/genepanels/HBOC/v1.0.0/stats/")
    assert r.json["overlap"]

    # Shouldn't contain itself in the overlap
    with pytest.raises(StopIteration):
        next(x for x in r.json["overlap"] if x["name"] == "HBOC")
    # {'overlap': [{'name': 'HBOCUTV', 'version': 'v1.0.0', 'addition_cnt': 5, 'overlap_cnt': 2, 'missing_cnt': 0}]}
    hboc_utv_overlap = next(x for x in r.json["overlap"] if x["name"] == "HBOCUTV")
    assert hboc_utv_overlap["missing_cnt"] == 0
    assert hboc_utv_overlap["addition_cnt"] == 5
    assert hboc_utv_overlap["overlap_cnt"] == 2
