import json
from typing import Any

import pytest

from ella.api.tests.util import FlaskClientProxy
from ella.vardb.util.testdatabase import TestDatabase


@pytest.fixture
def annotation_template():
    return {"allele_id": 1, "annotations": {"test": 1}, "user_id": 1}


class TestCustomAnnotation:
    def test_create_new(
        self,
        test_database: TestDatabase,
        annotation_template: dict[str, Any],
        client: FlaskClientProxy,
    ):
        test_database.refresh()  # Reset db

        # Insert new CustomAnnotation
        insert_response = client.post("/api/v1/customannotations/", annotation_template)
        id_of_first_created = insert_response.get_json()["id"]

        # Check that it's inserted
        insert_response = client.get(
            f"/api/v1/customannotations/?q={json.dumps({'id': [id_of_first_created]})}"
        ).json
        assert insert_response[0]["annotations"] == annotation_template["annotations"]
        assert insert_response[0]["date_created"]

        # Create a new one by updating an existing
        annotation_template["annotations"] = {"test": 2}
        update_response = client.post("/api/v1/customannotations/", annotation_template)
        id_of_created_by_update = update_response.get_json()["id"]

        # Check that the new annotation exists:
        second_response = client.get(
            f"/api/v1/customannotations/?q={json.dumps({'id': [id_of_created_by_update]})}"
        ).json
        assert second_response[0]["annotations"] == annotation_template["annotations"]

        # Check that the "original" has been superceeded:
        first_response = client.get(
            f"/api/v1/customannotations/?q={json.dumps({'id': [id_of_first_created]})}"
        ).json
        assert first_response[0]["date_superceeded"]
        assert first_response[0]["annotations"]["test"] == 1
        assert first_response[0]["id"] != id_of_created_by_update
