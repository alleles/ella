from ella.api.schemas.pydantic.v1.references import (
    NewReferenceAssessment,
    ReusedReferenceAssessment,
)
from ella.api.schemas.pydantic.v1.workflow import (
    AlleleInterpretationState,
    AnalysisInterpretationState,
)
from ella.api.schemas.pydantic.v1.workflow_allele_state import (
    AlleleState,
    AnalysisAlleleState,
    NewAlleleAssessment,
    NewAlleleReport,
    NonReusedAlleleAssessment,
    NonReusedAlleleReport,
    ReusedAlleleAssessment,
    ReusedAlleleReport,
)
from ella.api.tests import interpretation_helper as ih

"""
Performs various interpretation rounds for analyses and alleles.
"""


def _assert_all_interpretations_are_done(interpretations):
    expected_status = "Done"
    assert all(
        [i["status"] == expected_status for i in interpretations]
    ), f"Expected all to be {expected_status}"


def make_frontend_excluded_allele_backend_compliant(excluded_alleles_by_caller_type):
    flattened_excluded_allele_ids = {}
    if "snv" in excluded_alleles_by_caller_type:
        snv_filtered = excluded_alleles_by_caller_type["snv"]
        cnv_filtered = excluded_alleles_by_caller_type["cnv"]
        flattened_excluded_allele_ids = {**snv_filtered, **cnv_filtered}
    else:
        flattened_excluded_allele_ids = excluded_alleles_by_caller_type
    return flattened_excluded_allele_ids


class WorkflowHelper:
    def __init__(
        self, workflow_type, workflow_id, genepanel_name, genepanel_version, filterconfig_id=None
    ):
        assert genepanel_name and genepanel_version

        if workflow_type == "analysis" and not filterconfig_id:
            raise RuntimeError("Missing required filterconfig id when workflow_type == 'analysis'")

        self.type = workflow_type
        self.id = workflow_id
        self.filterconfig_id = filterconfig_id
        self.genepanel_name = genepanel_name
        self.genepanel_version = genepanel_version
        self.interpretation_extras = {"gp_name": genepanel_name, "gp_version": genepanel_version}

    def _update_comments(self, state, comment):
        for allelestate in state["allele"].values():
            allelestate["allelereport"]["evaluation"]["comment"] = comment
            for section in [
                "external",
                "frequency",
                "reference",
                "prediction",
                "classification",
                "similar",
            ]:
                allelestate["alleleassessment"]["evaluation"][section]["comment"] = comment
            for referenceassessment in allelestate["referenceassessments"]:
                referenceassessment["evaluation"]["comment"] = comment

    def reopen(self, username):
        r = ih.reopen_analysis(self.type, self.id, username)
        assert r.status_code == 200
        return r.get_json()

    def start_interpretation(self, username):
        return ih.start_interpretation(
            self.type, self.id, username, extra=self.interpretation_extras
        )

    def perform_round(self, interpretation, comment, new_workflow_status="Interpretation"):
        """
        :param interpretation: interpretation object from start_interpretation()
        """
        # Use pydantic model to store state
        state: AlleleInterpretationState | AnalysisInterpretationState
        if self.type == "analysis":
            state = AnalysisInterpretationState(**interpretation["state"])
        else:
            state = AlleleInterpretationState(**interpretation["state"])

        if self.type == "analysis":
            allele_ids = ih.get_filtered_alleles(
                self.type, self.id, interpretation["id"], filterconfig_id=self.filterconfig_id
            ).get_json()["allele_ids"]
        else:
            allele_ids = [self.id]

        r = ih.get_alleles(self.type, self.id, interpretation["id"], allele_ids)
        assert r.status_code == 200
        alleles = r.get_json()

        # Put assessments and reports in the state:
        for allele in alleles:
            annotation_references = allele["annotation"].get("references", [])
            ref_ids = [r["pubmed_id"] for r in annotation_references]
            q = {"pubmed_id": ref_ids}
            references = ih.get_entities_by_query("references", q).get_json()

            allelestate_dict = {
                "allele_id": allele["id"],
                "alleleassessment": ih.allele_assessment_template(),
                "allelereport": {"evaluation": {"comment": "Original comment"}},
                "referenceassessments": [
                    ih.reference_assessment_template(allele["id"], r["id"]) for r in references
                ],
                "workflow": {"reviewed": False},
            }

            allele_state: AlleleState | AnalysisAlleleState
            if self.type == "analysis":
                allelestate_dict["report"] = {"included": False}
                allelestate_dict["analysis"] = {
                    "comment": "",
                    "notrelevant": None,
                    "verification": None,
                }

                allele_state = AnalysisAlleleState(**allelestate_dict)
            else:
                allele_state = AlleleState(**allelestate_dict)

            state.allele[str(allele["id"])] = allele_state  # type: ignore[assignment]

        # Put pydantic object back into interpretation, and update comments
        interpretation["state"] = state.model_dump()
        self._update_comments(interpretation["state"], comment)

        ih.save_interpretation_state(
            self.type, interpretation, self.id, interpretation["user"]["username"]
        )

        interpretation_cnt = len(ih.get_interpretations(self.type, self.id).get_json())

        r = ih.save_interpretation_state(
            self.type, interpretation, self.id, interpretation["user"]["username"]
        )
        assert r.status_code == 200

        finish_method = {
            "Not ready": ih.mark_notready,
            "Interpretation": ih.mark_interpretation,
            "Review": ih.mark_review,
            "Medical review": ih.mark_medicalreview,
        }

        r = finish_method[
            new_workflow_status
        ](
            self.type,
            self.id,
            ih.round_template(
                allele_ids=allele_ids
            ),  # We don't bother to provide real data for normal rounds, we are just testing workflow
            interpretation["user"]["username"],
        )
        assert r.status_code == 200, r.get_data()

        # Check that new interpretation was created
        assert len(ih.get_interpretations(self.type, self.id).get_json()) == interpretation_cnt + 1

        # Check that data was updated like it should
        reloaded_interpretation = ih.get_interpretation(
            self.type, self.id, interpretation["id"]
        ).get_json()

        assert reloaded_interpretation["state"] == interpretation["state"]

        # self._check_comments(reloaded_interpretation["state"], comment)
        assert reloaded_interpretation["state"] == interpretation["state"]

        assert reloaded_interpretation["finalized"] is False
        assert reloaded_interpretation["status"] == "Done"
        assert reloaded_interpretation["user"]["username"] == interpretation["user"]["username"]

    def perform_finalize_round(self, interpretation, comment):
        self._update_comments(interpretation["state"], comment)

        response = ih.save_interpretation_state(
            self.type, interpretation, self.id, interpretation["user"]["username"]
        )
        assert response.status_code == 200

        # check that no new interpretation is created
        interpretation_cnt = len(ih.get_interpretations(self.type, self.id).get_json())

        response = ih.save_interpretation_state(
            self.type, interpretation, self.id, interpretation["user"]["username"]
        )
        assert response.status_code == 200

        if self.type == "analysis":
            filtered_allele_ids = ih.get_filtered_alleles(
                self.type, self.id, interpretation["id"], filterconfig_id=self.filterconfig_id
            ).get_json()
            allele_ids = filtered_allele_ids["allele_ids"]
            excluded_allele_ids = filtered_allele_ids["excluded_alleles_by_caller_type"]
        else:
            allele_ids = [self.id]
            excluded_allele_ids = {}

        r = ih.get_alleles(self.type, self.id, interpretation["id"], allele_ids)
        assert r.status_code == 200
        alleles = r.get_json()

        # Finalize all alleles
        annotation_ids = []
        custom_annotation_ids = []
        alleleassessment_ids = []
        allelereport_ids = []
        for allele_id in allele_ids:
            loaded_allele = next(a for a in alleles if a["id"] == allele_id)
            allele_state = interpretation["state"]["allele"][str(allele_id)]
            if existing_aa := loaded_allele.get("allele_assessment"):
                allele_assessment = NonReusedAlleleAssessment(
                    classification=allele_state["alleleassessment"]["classification"],
                    evaluation=allele_state["alleleassessment"]["evaluation"],
                    attachment_ids=[],
                    reuse=False,
                    reuseCheckedId=existing_aa["id"],
                )
            else:
                allele_assessment = NewAlleleAssessment(
                    classification=allele_state["alleleassessment"]["classification"],
                    evaluation=allele_state["alleleassessment"]["evaluation"],
                    attachment_ids=[],
                )  # type: ignore[assignment]

            if existing_ar := loaded_allele.get("allele_report"):
                allele_report = NonReusedAlleleReport(
                    evaluation=allele_state["allelereport"]["evaluation"],
                    reuse=False,
                    reuseCheckedId=existing_ar["id"],
                )
            else:
                allele_report = NewAlleleReport(
                    evaluation=allele_state["allelereport"]["evaluation"],
                )  # type: ignore[assignment]

            allele_reference_assessments = [
                NewReferenceAssessment(
                    evaluation=ra["evaluation"],
                    reference_id=ra["reference_id"],
                    allele_id=allele_id,
                )
                for ra in allele_state["referenceassessments"]
            ]
            allele = next(a for a in alleles if a["id"] == allele_id)
            annotation_id = allele["annotation"]["annotation_id"]
            annotation_ids.append(annotation_id)
            custom_annotation_id = (
                allele["annotation"]["custom_annotation_id"]
                if "custom_annotation_id" in allele["annotation"]
                else None
            )
            if custom_annotation_id:
                custom_annotation_ids.append(custom_annotation_id)
            response = ih.finalize_allele(
                self.type,
                self.id,
                allele_id,
                annotation_id,
                custom_annotation_id,
                allele_assessment.model_dump(),
                allele_report.model_dump(),
                [ra.model_dump() for ra in allele_reference_assessments],
                interpretation["user"]["username"],
            )
            assert response.status_code == 200
            # Check that objects were created as they should
            (
                created_alleleassessment_id,
                created_allelereport_id,
                created_referenceassessment_ids,
            ) = self.check_finalize_allele(allele_id, interpretation)
            alleleassessment_ids.append(created_alleleassessment_id)
            allelereport_ids.append(created_allelereport_id)

        if "snv" in excluded_allele_ids:
            snv_filtered = excluded_allele_ids["snv"]
            cnv_filtered = excluded_allele_ids["cnv"]
            flattened_excluded_allele_ids = {**snv_filtered, **cnv_filtered}
        else:
            flattened_excluded_allele_ids = excluded_allele_ids

        # Finalize workflow
        r = ih.finalize(
            self.type,
            self.id,
            allele_ids,
            annotation_ids,
            custom_annotation_ids,
            alleleassessment_ids,
            allelereport_ids,
            interpretation["user"]["username"],
            excluded_allele_ids=flattened_excluded_allele_ids,
        )

        assert r.status_code == 200

        interpretations = ih.get_interpretations(self.type, self.id).get_json()
        assert len(interpretations) == interpretation_cnt
        _assert_all_interpretations_are_done(interpretations)

        # Check that data was updated like it should
        reloaded_interpretation = ih.get_interpretation(
            self.type, self.id, interpretation["id"]
        ).get_json()

        assert reloaded_interpretation["state"] == interpretation["state"]

        assert reloaded_interpretation["finalized"] is True
        assert reloaded_interpretation["status"] == "Done"
        assert reloaded_interpretation["user"]["username"] == interpretation["user"]["username"]

    def perform_reopened_round(self, interpretation, comment):
        if self.type == "analysis":
            filtered_allele_ids = ih.get_filtered_alleles(
                self.type, self.id, interpretation["id"], filterconfig_id=self.filterconfig_id
            ).get_json()
            allele_ids = filtered_allele_ids["allele_ids"]
            excluded_allele_ids = make_frontend_excluded_allele_backend_compliant(
                filtered_allele_ids["excluded_alleles_by_caller_type"]
            )
        else:
            allele_ids = [self.id]
            excluded_allele_ids = None

        r = ih.get_alleles(self.type, self.id, interpretation["id"], allele_ids)
        assert r.status_code == 200
        alleles = r.get_json()

        # Finalize all alleles
        annotation_ids = []
        custom_annotation_ids = []
        alleleassessment_ids = []
        allelereport_ids = []
        for allele_id in allele_ids:
            allele = next(a for a in alleles if a["id"] == allele_id)
            # For a twist, only update allele report
            allele_state = interpretation["state"]["allele"][str(allele_id)]
            allele_assessment = ReusedAlleleAssessment(
                reuseCheckedId=allele["allele_assessment"]["id"],
                reuse=True,
            )
            if comment == allele["allele_report"]["evaluation"]["comment"]:
                allele_report = ReusedAlleleReport(
                    reuse=True,
                    reuseCheckedId=allele["allele_report"]["id"],
                )
            else:
                allele_report = NonReusedAlleleReport(
                    reuseCheckedId=allele["allele_report"]["id"],
                    evaluation={"comment": comment},
                    reuse=False,
                )  # type: ignore[assignment]
                allele_state["allelereport"]["evaluation"]["comment"] = comment

            allele_reference_assessments: list[ReusedReferenceAssessment] = []

            for ra in allele_state["referenceassessments"]:
                db_id = next(
                    db_ra
                    for db_ra in allele["reference_assessments"]
                    if db_ra["reference_id"] == ra["reference_id"]
                )["id"]
                # Reuse reference assessment
                allele_reference_assessments.append(
                    ReusedReferenceAssessment(
                        allele_id=allele_id,
                        reference_id=ra["reference_id"],
                        reuse=True,
                        reuseCheckedId=db_id,
                    )
                )

            annotation_id = allele["annotation"]["annotation_id"]
            annotation_ids.append(annotation_id)
            custom_annotation_id = (
                allele["annotation"]["custom_annotation_id"]
                if "custom_annotation_id" in allele["annotation"]
                else None
            )
            if custom_annotation_id:
                custom_annotation_ids.append(custom_annotation_id)
            response = ih.finalize_allele(
                self.type,
                self.id,
                allele_id,
                annotation_id,
                custom_annotation_id,
                allele_assessment.model_dump(),
                allele_report.model_dump(),
                [ra.model_dump() for ra in allele_reference_assessments],
                interpretation["user"]["username"],
            )
            assert response.status_code == 200
            # Check that objects were created as they should
            (
                created_alleleassessment_id,
                created_allelereport_id,
                created_referenceassessment_ids,
            ) = self.check_finalize_allele(allele_id, interpretation)
            alleleassessment_ids.append(created_alleleassessment_id)
            allelereport_ids.append(created_allelereport_id)

        # Finalize workflow
        ih.save_interpretation_state(
            self.type, interpretation, self.id, interpretation["user"]["username"]
        )

        r = ih.finalize(
            self.type,
            self.id,
            allele_ids,
            annotation_ids,
            custom_annotation_ids,
            alleleassessment_ids,
            allelereport_ids,
            interpretation["user"]["username"],
            excluded_allele_ids=excluded_allele_ids,
        )
        assert r.status_code == 200

        interpretations = ih.get_interpretations(self.type, self.id).get_json()
        _assert_all_interpretations_are_done(interpretations)

        # Check that data was updated like it should
        reloaded_interpretation = ih.get_interpretation(
            self.type, self.id, interpretation["id"]
        ).get_json()

        assert reloaded_interpretation["finalized"] is True
        assert reloaded_interpretation["status"] == "Done"
        assert reloaded_interpretation["user"]["username"] == interpretation["user"]["username"]
        assert reloaded_interpretation["state"] == interpretation["state"]

    def check_finalize_allele(self, allele_id, interpretation):
        # Check alleleassessment in database
        state_alleleassessment = interpretation["state"]["allele"][str(allele_id)][
            "alleleassessment"
        ]
        allele_assessments_in_db = ih.get_allele_assessments_by_allele(allele_id).get_json()
        assert allele_assessments_in_db
        latest_allele_assessment = next(
            a for a in allele_assessments_in_db if not a.get("date_superceeded")
        )
        if not state_alleleassessment.get("reuse"):
            assert (
                latest_allele_assessment["classification"]
                == state_alleleassessment["classification"]
            )
            assert latest_allele_assessment["evaluation"] == state_alleleassessment["evaluation"]
        else:
            assert latest_allele_assessment["id"] == state_alleleassessment["reuseCheckedId"]

        # Check allelereport in database
        state_allelereport = interpretation["state"]["allele"][str(allele_id)]["allelereport"]
        allele_reports_in_db = ih.get_allele_reports_by_allele(allele_id).get_json()
        assert allele_reports_in_db
        latest_allele_report = next(
            a for a in allele_reports_in_db if not a.get("date_superceeded")
        )
        if not state_allelereport.get("reuse"):
            assert latest_allele_report["evaluation"] == state_allelereport["evaluation"]
        else:
            assert latest_allele_assessment["id"] == state_allelereport["reuseCheckedId"]

        # Check on reference assessments:
        state_referenceassessments = interpretation["state"]["allele"][str(allele_id)][
            "referenceassessments"
        ]
        result_reference_assessment_ids = []
        if state_referenceassessments:
            reference_assessments_in_db = ih.get_reference_assessments_by_allele(
                allele_id
            ).get_json()
            for state_referenceassessment in state_referenceassessments:
                matching_reference_assessment = next(
                    ra
                    for ra in reference_assessments_in_db
                    if ra["allele_id"] == allele_id
                    and ra["reference_id"] == state_referenceassessment["reference_id"]
                    and ra.get("date_superceeded") is None
                )
                if "id" not in state_referenceassessment:
                    assert (
                        matching_reference_assessment["evaluation"]["comment"]
                        == state_referenceassessment["evaluation"]["comment"]
                    )
                else:
                    assert matching_reference_assessment["id"] == state_referenceassessment["id"]
                result_reference_assessment_ids.append(matching_reference_assessment["id"])
        return (
            latest_allele_assessment["id"],
            latest_allele_report["id"],
            result_reference_assessment_ids,
        )
