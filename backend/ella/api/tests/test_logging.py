import json
from datetime import datetime
from unittest.mock import patch

import pytz
from sqlalchemy import select
from sqlalchemy.orm.session import Session

from ella.vardb.datamodel import log
from ella.vardb.util.testdatabase import TestDatabase

from .util import DatetimeMock, FlaskClientProxy


def test_resourcelog(client: FlaskClientProxy, test_database: TestDatabase, session: Session):
    """
    Test that requests to the API are logged correctly in the 'ResourceLog' table.

    These tests are by default logged in as testuser1, with usersession_id of 1.
    """
    test_database.refresh()

    usersession_id = 1
    remote_addr = "127.0.0.1"
    fixed_time = datetime.now(pytz.utc)

    # Without payload
    DatetimeMock.fixed_time = fixed_time
    with patch("ella.api.main.datetime", new=DatetimeMock):
        r = client.get("/api/v1/config/")

    statuscode = r.status_code
    response_size = int(r.headers.get("Content-Length"))

    rlogs = session.execute(select(log.ResourceLog)).scalars().all()
    assert len(rlogs) == 2  # 2 entries since API did a login as first entry

    rl = rlogs[-1]
    assert rl.remote_addr == remote_addr
    assert rl.usersession_id == usersession_id
    assert rl.method == "GET"
    assert rl.resource == "/api/v1/config/"
    assert rl.statuscode == statuscode
    assert rl.response_size == response_size
    assert rl.payload is None
    assert rl.payload_size == 0
    assert rl.query == ""
    assert rl.duration > 0
    assert rl.time == fixed_time

    # With payload
    payload_data = {
        "allele_ids": [1],
        "gp_name": "HBOCUTV",
        "gp_version": "v1.0.0",
        "referenceassessments": [],
    }
    with patch("ella.api.main.datetime", new=DatetimeMock):
        r = client.post("/api/v1/acmg/alleles/?dummy=data", payload_data)

    payload = json.dumps(payload_data)
    payload_size = len(payload)
    statuscode = r.status_code
    response_size = int(r.headers.get("Content-Length"))

    rlogs = session.execute(select(log.ResourceLog)).scalars().all()
    assert len(rlogs) == 4  # 4 since /currentuser is called to check whether logged in

    rl = rlogs[-1]
    assert statuscode == 200
    assert rl.remote_addr == remote_addr
    assert rl.usersession_id == usersession_id
    assert rl.method == "POST"
    assert rl.resource == "/api/v1/acmg/alleles/"
    assert rl.statuscode == statuscode
    assert rl.response_size == response_size
    assert rl.payload == payload
    assert rl.payload_size == payload_size
    assert rl.query == "dummy=data"
    assert rl.duration > 0
    assert rl.time == fixed_time

    # Make sure /login doesn't log passwords
    payload_data = {"username": "abc", "password": "123"}
    with patch("ella.api.main.datetime", new=DatetimeMock):
        r = client.post("/api/v1/users/actions/login/", payload_data)

    statuscode = r.status_code
    response_size = int(r.headers.get("Content-Length"))

    rlogs = session.execute(select(log.ResourceLog)).scalars().all()
    assert len(rlogs) == 6  # 6 since /currentuser is called to check whether logged in

    rl = rlogs[-1]
    assert statuscode == 401  # User doesn't exist
    assert rl.remote_addr == remote_addr
    assert rl.usersession_id == usersession_id
    assert rl.method == "POST"
    assert rl.resource == "/api/v1/users/actions/login/"
    assert rl.statuscode == statuscode
    assert rl.response_size == response_size
    assert rl.payload is None
    assert rl.payload_size == 0
    assert rl.query == ""
    assert rl.duration > 0
    assert rl.time == fixed_time

    # Test logging when not logged in
    payload_data = {
        "allele_ids": [1],
        "gp_name": "HBOCUTV",
        "gp_version": "v01",
        "referenceassessments": [],
    }
    client.logout()
    with patch("ella.api.main.datetime", new=DatetimeMock):
        r = client.post("/api/v1/acmg/alleles/?dummy=data", payload_data, username=None)

    payload = json.dumps(payload_data)
    payload_size = len(payload)
    statuscode = r.status_code
    response_size = int(r.headers.get("Content-Length"))

    rlogs = session.execute(select(log.ResourceLog)).scalars().all()
    assert len(rlogs) == 9  # logout counts as 1

    rl = rlogs[-1]
    assert statuscode == 403
    assert rl.remote_addr == remote_addr
    assert rl.usersession_id is None
    assert rl.method == "POST"
    assert rl.resource == "/api/v1/acmg/alleles/"
    assert rl.statuscode == statuscode
    assert rl.response_size == response_size
    assert rl.payload == payload
    assert rl.payload_size == payload_size
    assert rl.query == "dummy=data"
    assert rl.time == fixed_time
