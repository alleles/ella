import datetime

import pytest
from sqlalchemy import select
from sqlalchemy.orm.session import Session

from ella.api.schemas.pydantic.v1.allele_assessments import AlleleAssessmentEvaluation
from ella.api.schemas.pydantic.v1.references import ReferenceEvaluation
from ella.api.schemas.pydantic.v1.workflow_allele_state import NewAlleleReport
from ella.api.tests import interpretation_helper as ih
from ella.api.tests.workflow_helper import (
    WorkflowHelper,
    make_frontend_excluded_allele_backend_compliant,
)
from ella.vardb.datamodel import assessment
from ella.vardb.datamodel import user as user_model
from ella.vardb.util.testdatabase import TestDatabase

"""
Test the interpretation workflow of
 - a sample/analysis
 - a single variant

The single variant is chosen not be included in the sample/analysis.
So any assessments done in either sample or variant
workflow won't affect each other.

The tests for variant and sample workflows are similar.
The differences are handled using parametrized tests, some
workflow specific test fixture and branching based on workflow.

"""

ANALYSIS_ID = 2  # analysis_id = 2, allele 1-6
ALLELE_ID = 18  # allele id 18, brca2 c.1275A>G,  GRCh37/13-32906889-32906890-A-G?gp_name=HBOCUTV&gp_version=v1.0.0
FILTERCONFIG_ID = 1

ANALYSIS_ALLELE_IDS = [1, 3, 4, 7, 12, 13]

ANALYSIS_USERNAMES = ["testuser1", "testuser2", "testuser3"]  # username for each round
ALLELE_USERNAMES = ["testuser4", "testuser1", "testuser2"]


@pytest.fixture(scope="module")
def analysis_wh():
    return WorkflowHelper(
        "analysis", ANALYSIS_ID, "HBOCUTV", "v1.0.0", filterconfig_id=FILTERCONFIG_ID
    )


@pytest.fixture(scope="module")
def allele_wh():
    return WorkflowHelper("allele", ALLELE_ID, "HBOCUTV", "v1.0.0")


def update_user_config(session: Session, username: str, user_config: dict):
    user = session.execute(
        select(user_model.User).where(user_model.User.username == username)
    ).scalar_one()

    user.config = user_config
    session.commit()


class TestAnalysisInterpretationWorkflow:
    """
    Tests the whole workflow of
    interpretations on one analysis.

    - Interpretation -> Not ready
    - Not ready -> Interpretation
    - Interpretation -> Review
    - Review -> Medical review
    - Finalize
    - Reopen -> one round -> finalize
    """

    def test_init_data(self, test_database: TestDatabase):
        test_database.refresh()

    def test_round_one_notready(self, analysis_wh: WorkflowHelper):
        interpretation = analysis_wh.start_interpretation(ANALYSIS_USERNAMES[0])
        analysis_wh.perform_round(
            interpretation, "Not ready comment", new_workflow_status="Not ready"
        )

    def test_round_two_interpretation(self, analysis_wh: WorkflowHelper):
        interpretation = analysis_wh.start_interpretation(ANALYSIS_USERNAMES[0])
        analysis_wh.perform_round(
            interpretation, "Interpretation comment", new_workflow_status="Interpretation"
        )

    def test_round_three_review(self, analysis_wh: WorkflowHelper):
        interpretation = analysis_wh.start_interpretation(ANALYSIS_USERNAMES[0])
        analysis_wh.perform_round(interpretation, "Review comment", new_workflow_status="Review")

    def test_round_four_review(self, analysis_wh: WorkflowHelper):
        interpretation = analysis_wh.start_interpretation(ANALYSIS_USERNAMES[0])
        analysis_wh.perform_round(
            interpretation, "Medical review comment", new_workflow_status="Medical review"
        )

    def test_round_five_finalize(self, analysis_wh: WorkflowHelper):
        interpretation = analysis_wh.start_interpretation(ANALYSIS_USERNAMES[1])
        analysis_wh.perform_finalize_round(interpretation, "Finalize comment")

    def test_round_three_reopen_and_finalize(self, analysis_wh: WorkflowHelper):
        analysis_wh.reopen(ANALYSIS_USERNAMES[2])
        interpretation = analysis_wh.start_interpretation(ANALYSIS_USERNAMES[2])
        analysis_wh.perform_reopened_round(interpretation, "Reopened comment")


class TestAlleleInterpretationWorkflow:
    """
    Tests the whole workflow of
    interpretations on one allele.

    - Interpretation -> Review
    - Finalize
    - Reopen -> one round -> finalize
    """

    def test_init_data(self, test_database: TestDatabase):
        test_database.refresh()

    def test_round_one_review(self, allele_wh: WorkflowHelper):
        interpretation = allele_wh.start_interpretation(ALLELE_USERNAMES[0])
        allele_wh.perform_round(interpretation, "Review comment", new_workflow_status="Review")

    def test_round_two_finalize(self, allele_wh: WorkflowHelper):
        interpretation = allele_wh.start_interpretation(ALLELE_USERNAMES[1])
        allele_wh.perform_finalize_round(interpretation, "Finalize comment")

    def test_round_three_reopen_and_finalize(self, allele_wh: WorkflowHelper):
        allele_wh.reopen(ALLELE_USERNAMES[2])
        interpretation = allele_wh.start_interpretation(ALLELE_USERNAMES[2])
        allele_wh.perform_reopened_round(interpretation, "Reopened comment")


class TestOther:
    """
    Tests other scenarios not covered in normal workflow
    """

    def test_disallow_referenceassessment_no_alleleassessment(
        self, test_database: TestDatabase, session: Session
    ):
        """
        Test case where one tries to create referenceassessments for alleles where
        no alleleassessments will be created at the same time.
        """

        test_database.refresh()

        # Create dummy alleleassessment
        aa = assessment.AlleleAssessment(
            user_id=1,
            allele_id=1,
            classification="1",
            evaluation=AlleleAssessmentEvaluation(),
            genepanel_name="HBOC",
            genepanel_version="v1.0.0",
        )
        session.add(aa)
        session.commit()

        interpretation = ih.start_interpretation(
            "allele", 1, "testuser1", extra={"gp_name": "HBOC", "gp_version": "v1.0.0"}
        )

        r = ih.get_alleles("allele", 1, interpretation["id"], [1])
        assert r.status_code == 200
        alleles = r.get_json()

        # Reuse alleleassessment
        alleleassessment = {"reuse": True, "reuseCheckedId": 1}
        allelereport = {"evaluation": {"comment": "asdasd"}}
        # Create new referenceassessment
        referenceassessments = [
            {
                "reference_id": 1,
                "allele_id": 1,
                "evaluation": {"comment": "Something"},
            }
        ]

        r = ih.finalize_allele(
            "allele",
            1,
            1,
            alleles[0]["annotation"]["annotation_id"],
            alleles[0]["annotation"]["custom_annotation_id"],
            alleleassessment,
            allelereport,
            referenceassessments,
            "testuser1",
        )
        assert (
            "Trying to create referenceassessments for allele, while not also creating alleleassessment"
            in str(r.get_json()["message"])
        )
        assert r.status_code == 500

    def test_reusing_superceded_referenceassessment(
        self, test_database: TestDatabase, session: Session
    ):
        """
        Test case where one tries to reuse superceded referenceassessment.
        """

        test_database.refresh()

        # Create dummy referenceassessment
        ra = assessment.ReferenceAssessment(
            user_id=1,
            allele_id=1,
            reference_id=1,
            evaluation=ReferenceEvaluation(),
            genepanel_name="HBOC",
            genepanel_version="v1.0.0",
            date_superceeded=datetime.datetime.now(),
        )
        session.add(ra)
        session.commit()

        # Will get id = 2
        ra = assessment.ReferenceAssessment(
            user_id=1,
            allele_id=1,
            reference_id=1,
            evaluation=ReferenceEvaluation(),
            genepanel_name="HBOC",
            genepanel_version="v1.0.0",
            previous_assessment_id=1,
        )

        session.add(ra)
        session.commit()

        interpretation = ih.start_interpretation(
            "allele", 1, "testuser1", extra={"gp_name": "HBOC", "gp_version": "v1.0.0"}
        )

        r = ih.get_alleles("allele", 1, interpretation["id"], [1])
        assert r.status_code == 200
        alleles = r.get_json()

        # Create alleleassessment
        alleleassessment = ih.allele_assessment_template()
        # Create allelereport
        allelereport = NewAlleleReport(evaluation={"comment": "Original comment"})

        # Reusing old referenceassessment
        referenceassessments = [
            {"reference_id": 1, "reuse": True, "reuseCheckedId": 1, "allele_id": 1}
        ]  # outdated

        r = ih.finalize_allele(
            "allele",
            1,
            1,
            alleles[0]["annotation"]["annotation_id"],
            alleles[0]["annotation"]["custom_annotation_id"],
            alleleassessment,
            allelereport.model_dump(),
            referenceassessments,
            "testuser1",
        )
        assert "Found no matching referenceassessment" in r.get_json()["message"]
        assert r.status_code == 500

    def test_reusing_superceded_alleleassessment(
        self, test_database: TestDatabase, session: Session
    ):
        """
        Test case where one tries to reuse superceded referenceassessment.
        """

        test_database.refresh()

        # Create dummy alleleassessment
        aa = assessment.AlleleAssessment(
            user_id=1,
            allele_id=1,
            classification="1",
            evaluation=AlleleAssessmentEvaluation(),
            genepanel_name="HBOC",
            genepanel_version="v1.0.0",
            date_superceeded=datetime.datetime.now(),
        )
        session.add(aa)
        session.commit()

        # Will get id = 2
        aa = assessment.AlleleAssessment(
            user_id=1,
            allele_id=1,
            classification="2",
            evaluation=AlleleAssessmentEvaluation(),
            genepanel_name="HBOC",
            genepanel_version="v1.0.0",
            previous_assessment_id=1,
        )
        session.add(aa)
        session.commit()

        interpretation = ih.start_interpretation(
            "allele", 1, "testuser1", extra={"gp_name": "HBOC", "gp_version": "v1.0.0"}
        )

        r = ih.get_alleles("allele", 1, interpretation["id"], [1])
        assert r.status_code == 200
        alleles = r.get_json()

        # Reuse outdated alleleassessment
        alleleassessment = {"reuse": True, "reuseCheckedId": 1}
        # Create allelereport
        allelereport = NewAlleleReport(evaluation={"comment": "Original comment"})

        referenceassessments: list = []

        r = ih.finalize_allele(
            "allele",
            1,
            1,
            alleles[0]["annotation"]["annotation_id"],
            alleles[0]["annotation"]["custom_annotation_id"],
            alleleassessment,
            allelereport.model_dump(),
            referenceassessments,
            "testuser1",
        )
        assert (
            "'reuseCheckedId': 1 does not match latest existing alleleassessment id: 2"
            == r.get_json()["message"]
        )
        assert r.status_code == 500


class TestFinalizationRequirements:
    def test_required_workflow_status_allele(self, test_database: TestDatabase, session: Session):
        """
        Test finalizing when in and when not in the required workflow status.
        """

        excluded_allele_ids: dict = {}
        test_database.refresh()

        # Default user id is 1
        user_config = {
            "workflows": {"allele": {"finalize_requirements": {"workflow_status": ["Review"]}}}
        }
        user = session.execute(select(user_model.User).where(user_model.User.id == 1)).scalar_one()

        user.config = user_config
        session.commit()

        interpretation = ih.start_interpretation(
            "allele", 1, "testuser1", extra={"gp_name": "HBOC", "gp_version": "v1.0.0"}
        )

        r = ih.get_alleles("allele", 1, interpretation["id"], [1])

        assert r.status_code == 200
        alleles = r.get_json()

        # Create alleleassessment
        alleleassessment = ih.allele_assessment_template()
        referenceassessments: list = []
        allelereport = NewAlleleReport(evaluation={"comment": "Original comment"})

        # Check finalize allele
        r = ih.finalize_allele(
            "allele",
            1,
            1,
            alleles[0]["annotation"]["annotation_id"],
            alleles[0]["annotation"]["custom_annotation_id"],
            alleleassessment,
            allelereport.model_dump(),
            referenceassessments,
            "testuser1",
        )
        assert r.status_code == 500
        assert (
            "Cannot finalize: interpretation workflow status is Interpretation, but must be one of: Review"
            in r.get_json()["message"]
        )

        # Check finalize workflow
        r = ih.finalize(
            "allele",
            1,
            [1],
            [a["annotation"]["annotation_id"] for a in alleles],
            [
                a["annotation"]["custom_annotation_id"]
                for a in alleles
                if "custom_annotation_id" in a["annotation"]
            ],
            [],
            [],
            "testuser1",
        )

        assert r.status_code == 500
        assert (
            "Cannot finalize: interpretation workflow status is Interpretation, but must be one of: Review"
            in r.get_json()["message"]
        )

        # Send to review, and try again
        r = ih.mark_review(
            "allele",
            1,
            {
                "allele_ids": [1],
                "annotation_ids": [a["annotation"]["annotation_id"] for a in alleles],
                "custom_annotation_ids": [
                    a["annotation"]["custom_annotation_id"]
                    for a in alleles
                    if "custom_annotation_id" in a["annotation"]
                ],
                "alleleassessment_ids": [],
                "allelereport_ids": [],
            },
            "testuser1",
        )
        r.status_code == 200

        ih.start_interpretation(
            "allele", 1, "testuser1", extra={"gp_name": "HBOC", "gp_version": "v1.0.0"}
        )

        r = ih.finalize_allele(
            "allele",
            1,
            1,
            alleles[0]["annotation"]["annotation_id"],
            alleles[0]["annotation"]["custom_annotation_id"],
            alleleassessment,
            allelereport.model_dump(),
            referenceassessments,
            "testuser1",
        )
        assert r.status_code == 200

        alleleassessment_id = r.get_json()["alleleassessment"]["id"]

        r = ih.finalize(
            "allele",
            1,
            [1],
            [a["annotation"]["annotation_id"] for a in alleles],
            [
                a["annotation"]["custom_annotation_id"]
                for a in alleles
                if "custom_annotation_id" in a["annotation"]
            ],
            [alleleassessment_id],
            [],
            "testuser1",
            excluded_allele_ids,
        )
        assert r.status_code == 200

    def test_required_workflow_status_analysis(self, test_database: TestDatabase, session: Session):
        """
        Test finalizing when in and when not in the required workflow status.
        """

        test_database.refresh()

        # Default user id is 1
        user_config = {
            "workflows": {
                "analysis": {
                    "finalize_requirements": {"workflow_status": ["Review", "Medical review"]}
                }
            }
        }
        update_user_config(session, "testuser1", user_config)

        interpretation = ih.start_interpretation(
            "analysis", 1, "testuser1", extra={"gp_name": "HBOC", "gp_version": "v1.0.0"}
        )

        filtered_allele_ids = ih.get_filtered_alleles(
            "analysis", 1, interpretation["id"], filterconfig_id=1
        ).get_json()

        r = ih.get_alleles("analysis", 1, interpretation["id"], filtered_allele_ids["allele_ids"])
        assert r.status_code == 200
        alleles = r.get_json()

        # Try finalizing a single allele
        allele_id = alleles[0]["id"]
        alleleassessment = ih.allele_assessment_template()
        referenceassessments: list = []
        allelereport = NewAlleleReport(evaluation={"comment": "Original comment"})
        r = ih.finalize_allele(
            "analysis",
            allele_id,
            allele_id,
            alleles[0]["annotation"]["annotation_id"],
            alleles[0]["annotation"]["custom_annotation_id"],
            alleleassessment,
            allelereport.model_dump(),
            referenceassessments,
            "testuser1",
        )
        assert r.status_code == 500
        assert (
            "Cannot finalize: interpretation workflow status is Interpretation, but must be one of: Review, Medical review"
            in r.get_json()["message"]
        )

        annotation_ids = [a["annotation"]["annotation_id"] for a in alleles]
        allele_ids = [a["id"] for a in alleles]

        flattened_excluded_allele_ids = make_frontend_excluded_allele_backend_compliant(
            filtered_allele_ids["excluded_alleles_by_caller_type"]
        )

        # Send to medical review, and try again
        ih.mark_medicalreview(
            "analysis",
            1,
            ih.round_template(
                allele_ids=allele_ids,
                annotation_ids=annotation_ids,
                excluded_allele_ids=flattened_excluded_allele_ids,
            ),
            "testuser1",
        )

        interpretation = ih.start_interpretation(
            "analysis", 1, "testuser1", extra={"gp_name": "HBOC", "gp_version": "v1.0.0"}
        )

        # Finalize allele
        alleleassessment_ids = []
        allelereport_ids = []
        for allele in alleles:
            alleleassessment = ih.allele_assessment_template()
            allelereport = NewAlleleReport(evaluation={"comment": "Original comment"})
            referenceassessments = []
            r = ih.finalize_allele(
                "analysis",
                1,
                allele["id"],
                allele["annotation"]["annotation_id"],
                1 if allele["id"] == 1 else None,
                alleleassessment,
                allelereport.model_dump(),
                referenceassessments,
                "testuser1",
            )
            assert r.status_code == 200
            response = r.get_json()
            alleleassessment_ids.append(response["alleleassessment"]["id"])
            allelereport_ids.append(response["allelereport"]["id"])

        # Finalize workflow
        ih.finalize(
            "analysis",
            1,
            allele_ids,
            annotation_ids,
            [],
            alleleassessment_ids,
            allelereport_ids,
            "testuser1",
            excluded_allele_ids=flattened_excluded_allele_ids,
        )
        assert r.status_code == 200

    def test_allow_unclassified(self, test_database: TestDatabase, session: Session):
        """
        Test finalization requirement allow_unclassified
        """

        test_database.refresh()

        # Default user is testuser1
        user_config = {
            "workflows": {
                "analysis": {
                    "finalize_requirements": {
                        "allow_unclassified": False,
                    }
                }
            }
        }
        update_user_config(session, "testuser1", user_config)

        interpretation = ih.start_interpretation(
            "analysis", 1, "testuser1", extra={"gp_name": "HBOC", "gp_version": "v1.0.0"}
        )

        filtered_allele_ids = ih.get_filtered_alleles(
            "analysis", 1, interpretation["id"], filterconfig_id=1
        ).get_json()

        r = ih.get_alleles("analysis", 1, interpretation["id"], filtered_allele_ids["allele_ids"])
        assert r.status_code == 200
        alleles = r.get_json()

        alleleassessment_ids = []
        # Create assessment for all except first allele
        for al in alleles[1:]:
            aa = assessment.AlleleAssessment(
                user_id=1,
                allele_id=al["id"],
                classification="1",
                evaluation=AlleleAssessmentEvaluation(),
                genepanel_name="HBOC",
                genepanel_version="v1.0.0",
            )
            session.add(aa)
            session.commit()
            alleleassessment_ids.append(aa.id)

        # Make one variant unclassified
        allele_ids = [a["id"] for a in alleles]
        reported_allele_ids: list[int] = []
        notrelevant_allele_ids: list = []
        technical_allele_ids: list = []
        allelereport_ids: list = []
        annotation_ids = [a["annotation"]["annotation_id"] for a in alleles]
        custom_annotation_ids: list = []
        flattened_excluded_allele_ids = make_frontend_excluded_allele_backend_compliant(
            filtered_allele_ids["excluded_alleles_by_caller_type"]
        )

        # allow_unclassified is False, so it should fail
        r = ih.finalize(
            "analysis",
            1,
            allele_ids,
            annotation_ids,
            custom_annotation_ids,
            alleleassessment_ids,
            allelereport_ids,
            "testuser1",
            excluded_allele_ids=flattened_excluded_allele_ids,
            technical_allele_ids=technical_allele_ids,
            notrelevant_allele_ids=notrelevant_allele_ids,
            reported_allele_ids=reported_allele_ids,
        )
        assert r.status_code == 500
        assert (
            r.get_json()["message"]
            == "allow_unclassified is set to false, but some allele ids are missing classification"
        )

        # Allow unclassified and try again
        user_config = {
            "workflows": {
                "analysis": {
                    "finalize_requirements": {
                        "allow_unclassified": True,
                    }
                }
            }
        }
        update_user_config(session, "testuser1", user_config)

        r = ih.finalize(
            "analysis",
            1,
            allele_ids,
            annotation_ids,
            custom_annotation_ids,
            alleleassessment_ids,
            allelereport_ids,
            "testuser1",
            excluded_allele_ids=flattened_excluded_allele_ids,
            technical_allele_ids=technical_allele_ids,
            notrelevant_allele_ids=notrelevant_allele_ids,
            reported_allele_ids=reported_allele_ids,
        )
        assert r.status_code == 200
