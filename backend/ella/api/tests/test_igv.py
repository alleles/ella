def test_igv_endpoints(client):
    "Simple test to ensure that IGV endpoints are working"

    # /api/v1/igv/tracks/analyses/<int:analysis_id>/<filename>: ResourceMethods.GET,
    r = client.get("/api/v1/igv/tracks/1/")
    assert r.status_code == 200
    assert r.data, "Expected non-empty data"
    # "/api/v1/igv/tracks/dynamic/classifications/": ResourceMethods.GET,
    r = client.get("/api/v1/igv/tracks/dynamic/classifications/")
    assert r.status_code == 200
    assert r.data == b"", "Expected empty data"

    # "/api/v1/igv/tracks/dynamic/genepanel/<gp_name>/<gp_version>/": ResourceMethods.GET,
    r = client.get("/api/v1/igv/tracks/dynamic/genepanel/HBOC/v1.0.0/")
    assert r.status_code == 200
    assert r.data, "Expected non-empty data"

    allele_ids = [1, 2, 3]
    # "/api/v1/igv/tracks/dynamic/regions_of_interest/<int:analysis_id>/": ResourceMethods.GET,
    r = client.get(
        "/api/v1/igv/tracks/dynamic/regions_of_interest/1/?allele_ids="
        + ",".join(map(str, allele_ids))
    )
    assert r.data, "Expected non-empty data"
    non_empty_lines = [x for x in r.data.split(b"\n") if x]
    assert len(non_empty_lines) == len(allele_ids)

    # "/api/v1/igv/tracks/dynamic/variants/<int:analysis_id>/": ResourceMethods.GET,
    r = client.get(
        "/api/v1/igv/tracks/dynamic/variants/1/?allele_ids=" + ",".join(map(str, allele_ids))
    )
    assert r.status_code == 200
    assert r.data, "Expected non-empty data"
    data_lines = [x for x in r.data.split(b"\n") if x and not x.startswith(b"#")]
    assert len(data_lines) == len(allele_ids)

    # "/api/v1/igv/tracks/static/<filepath>": ResourceMethods.GET,
    r = client.get("/api/v1/igv/tracks/static/ncbiRefSeqCurated.sorted.gtf.gz?index=1")
    assert r.status_code == 200
    assert r.data, "Expected non-empty data"
    r = client.get("/api/v1/igv/tracks/static/ncbiRefSeqCurated.sorted.gtf.gz")
    assert r.status_code == 200
    assert r.data, "Expected non-empty data"
