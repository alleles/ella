import json
import os
import subprocess
import sys
from pathlib import Path

from ella.vardb.util.testdatabase import TestDatabase


def test_preimport():
    # Need to make sure we have a database, since preimport tries to connect to it
    TestDatabase().refresh()
    TESTDATA_GENEPANELS = Path("/ella/ella-testdata/testdata/clinicalGenePanels")
    for gp_dir in TESTDATA_GENEPANELS.iterdir():
        if gp_dir.is_dir():
            gp_name, gp_version = gp_dir.name.split("_", 1)
            s = subprocess.check_output(
                [sys.executable, "/ella/backend/ella/polling/preimport.py"],
                env={
                    "GENEPANEL_NAME": gp_name,
                    "GENEPANEL_VERSION": gp_version,
                    "PRIORITY": "",
                    **os.environ,
                },
            )
            d = json.loads(s)
            d["files"] = {k: Path(v) for k, v in d["files"].items()}

            ignore_files = [
                f"{gp_name}_{gp_version}.json",
                f"{gp_name}_{gp_version}_genetikkportalen.tsv",
                "genes.csv",
                "panel_build_log.txt",
                "files_legend.txt",
            ]

            # Check that all files are present, and are equal (except for header line)
            for gp_file in gp_dir.iterdir():
                if gp_file.name in ignore_files:
                    continue
                matching_file = Path(next(x for x in d["files"].values() if x.name == gp_file.name))
                assert matching_file != gp_file, (
                    "Same file! This should not happen, and indicates an error with the test setup. "
                    "Does the preimport script output files to the testdata directory?"
                )
                with gp_file.open() as f1:
                    gp_lines = set(l for l in f1.readlines() if not l.startswith("#"))
                with matching_file.open() as f2:
                    matching_lines = set(l for l in f2.readlines() if not l.startswith("#"))
                assert gp_lines == matching_lines, gp_file
