import binascii
import datetime
import json
import logging
import os
import subprocess
import tempfile
import time
import traceback
import urllib.error
import urllib.parse
import urllib.request
from collections.abc import Sequence
from pathlib import Path
from urllib.parse import urlencode

import pytz
from sqlalchemy import inspect, select
from sqlalchemy.exc import OperationalError
from sqlalchemy.orm import Session

from ella.api.config import config
from ella.api.util.genepanel_to_bed import genepanel_to_bed
from ella.datalayer import filters
from ella.vardb.datamodel.annotationjob import AnnotationJob
from ella.vardb.deposit.analysis_config import AnalysisConfigData
from ella.vardb.deposit.deposit_alleles import DepositAlleles
from ella.vardb.deposit.deposit_analysis import DepositAnalysis

log = logging.getLogger(__name__)


def run_preimport(job: AnnotationJob):
    preimport_script = config["import"].get("preimport_script")
    if preimport_script is None:
        return {"files": {}, "variables": {}}

    assert Path(preimport_script).exists()
    priority = job.properties.get("priority", 1) if job.properties else 1

    args = [
        f"GENEPANEL_NAME={job.genepanel_name}",
        f"GENEPANEL_VERSION={job.genepanel_version}",
        f"PRIORITY={priority}",
    ]

    cmd = " ".join(args + [preimport_script])
    output = subprocess.check_output(cmd, shell=True).decode()
    unparsed_data: dict = json.loads(output)

    parsed_data: dict[str, dict[str, tuple[str, str]]] = {}
    parsed_data["variables"] = {}
    parsed_data["variables"] = unparsed_data["variables"]

    parsed_data["files"] = {}
    for key, filepath in unparsed_data["files"].items():
        path = Path(filepath)
        contents = path.read_text()
        parsed_data["files"][key] = (path.name, contents)
    return parsed_data


def encode_multipart_formdata(fields: dict, files: dict):
    LIMIT = "-" * 10 + binascii.hexlify(os.urandom(10)).decode()
    CRLF = "\r\n"
    L = []
    for key, value in fields.items():
        L.append("--" + LIMIT)
        L.append('Content-Disposition: form-data; name="%s"' % key)
        L.append("")
        if isinstance(value, str):
            if not value.startswith('"'):
                value = '"' + value
            if not value.endswith('"'):
                value = value + '"'
        value = str(value)
        L.append(value)
    for key, (filename, value) in files.items():
        L.append("--" + LIMIT)
        L.append(f'Content-Disposition: form-data; name="{key}"; filename="{filename}"')
        L.append("Content-Type: application/octet-stream")
        L.append("")
        L.append(value)
    L.append("--" + LIMIT + "--")
    L.append("")
    body = CRLF.join(L)
    content_type = f"multipart/form-data; boundary={LIMIT}"
    return content_type, body


ANNOTATION_SERVICE_URL = config["app"]["annotation_service"]


def get_error_message(e):
    try:
        msg = json.loads(e.read().decode())["message"]
    except Exception:
        msg = "Unable to determine error"
    return msg


class AnnotationJobsInterface:
    def __init__(self, session: Session):
        self.session = session

    def get_all(self):
        return self.session.execute(select(AnnotationJob)).scalars().all()

    def get_with_status(self, status: str | tuple[str, ...] | list[str]):
        if isinstance(status, str):
            status = [status]
        assert isinstance(status, tuple | list)
        return (
            self.session.execute(
                select(AnnotationJob).where(filters.in_(AnnotationJob.status, status))
            )
            .scalars()
            .all()
        )

    def get_with_id(self, id: int):
        return (
            self.session.execute(select(AnnotationJob).where(AnnotationJob.id == id))
            .scalars()
            .one()
        )

    def create(self, data):
        return AnnotationJob(**data)

    def patch(self, id: int, **kwargs):
        patched = False
        job = self.get_with_id(id)
        allowed_keys = ["status", "message", "task_id"]
        assert len(set(kwargs) - set(allowed_keys)) == 0, "Illegal values passed to patch: " + str(
            set(kwargs) - set(allowed_keys)
        )

        if kwargs.get("status") and kwargs["status"] != job.status:
            if "history" not in job.status_history:
                job.status_history["history"] = list()

            job.status_history["history"].insert(
                0,
                {
                    "time": datetime.datetime.now(pytz.utc).isoformat(),
                    "status": job.status,
                },
            )
            patched = True

        for k, v in list(kwargs.items()):
            assert hasattr(job, k)
            if v is not None and getattr(job, k) != v:
                setattr(job, k, v)
                patched = True

        if patched:
            job.date_last_update = datetime.datetime.now(pytz.utc)
        return job

    def deposit(self, id: int, annotated_vcf: str):
        job = self.get_with_id(id)
        mode = job.mode
        with tempfile.TemporaryDirectory() as folder:
            vcf_file = Path(folder) / f"{job.id}.vcf"
            analysis_file = Path(folder) / f"{job.id}.analysis"
            with vcf_file.open("w") as vcf, analysis_file.open("w") as af:
                vcf.write(annotated_vcf)
                vcf.flush()

                gp_name = job.genepanel_name
                gp_version = job.genepanel_version

                if mode == "Analysis":
                    analysis_name = job.properties["analysis_name"]
                    append = job.properties["create_or_append"] != "Create"
                    if append:
                        analysis_name = job.properties["analysis_name"]
                    else:
                        analysis_name = f"{analysis_name}.{gp_name}_{gp_version}"

                    af_data = {
                        "name": analysis_name,
                        "genepanel_name": job.genepanel_name,
                        "genepanel_version": job.genepanel_version,
                        "priority": job.properties.get("priority", 1),
                        "data": [
                            {
                                "vcf": str(vcf_file),
                                "technology": job.properties["sample_type"],
                            }
                        ],
                    }

                    json.dump(af_data, af)
                    af.flush()

                    analysis_config_data = AnalysisConfigData(analysis_file)
                    da = DepositAnalysis(self.session)
                    da.import_vcf(analysis_config_data, append=append)

                elif mode in ["Variants", "Single variant"]:
                    deposit = DepositAlleles(self.session)
                    deposit.import_vcf(str(vcf_file), gp_name, gp_version)
                else:
                    raise RuntimeError(f"Unknown mode: {mode}")

    def delete(self, job: AnnotationJob):
        self.session.delete(job)

    def add(self, data: AnnotationJob):
        self.session.add(data)

    def commit(self):
        self.session.commit()

    def rollback(self):
        self.session.rollback()


# NOTE: This is calling anno API
class AnnotationServiceInterface:
    def __init__(self, url: str, session: Session):
        self.base = f"{url}/api/v1"
        self.session = session

    def annotate(self, job: AnnotationJob):
        r = urllib.request.Request(
            f"{self.base}/annotate",
            data=json.dumps({"input": job.data}).encode(),
            headers={"Content-type": "application/json"},
        )
        k = urllib.request.urlopen(r)
        return json.loads(k.read().decode())

    def annotate_sample(self, job: AnnotationJob):
        assert job.genepanel_name is not None and job.genepanel_version is not None
        regions = genepanel_to_bed(self.session, job.genepanel_name, job.genepanel_version)

        data = run_preimport(job)

        data["files"]["regions"] = ("regions.bed", regions)
        data["variables"]["sample_id"] = job.sample_id

        content_type, body = encode_multipart_formdata(data["variables"], data["files"])

        r = urllib.request.Request(
            f"{self.base}/samples/annotate",
            data=body.encode(),
            headers={"Content-type": content_type},
        )
        k = urllib.request.urlopen(r)
        return json.loads(k.read().decode())

    def process(self, task_id: str):
        k = urllib.request.urlopen(f"{self.base}/process/{task_id}")
        return k.read().decode()

    def status(self, task_id: str | None = None):
        """Get status of task_id or all tasks"""
        if task_id:
            k = urllib.request.urlopen(f"{self.base}/status/{task_id}")
        else:
            k = urllib.request.urlopen(f"{self.base}/status")
        resp = json.loads(k.read().decode())
        return resp

    def search_samples(self, search_term: str, limit: int | None) -> list[dict]:
        """Search for samples and return results"""
        d: dict[str, dict[str, str] | str] = {"q": {"name": str(search_term)}}
        if limit:
            d["limit"] = str(limit)
        q = urlencode(d)

        k = urllib.request.urlopen(f"{self.base}/samples/?" + q)
        resp = json.loads(k.read().decode())
        result = []
        for k, v in resp.items():
            v.update(name=k)
            result.append(v)

        return sorted(result, key=lambda x: x["name"])

    def annotation_service_running(self) -> bool:
        try:
            urllib.request.urlopen(f"{self.base}/status")
            return True
        except (urllib.error.HTTPError, urllib.error.URLError):
            return False


def process_running(
    annotation_service: AnnotationServiceInterface, running_jobs: Sequence[AnnotationJob]
):
    for job in running_jobs:
        id = job.id
        status = job.status
        task_id = job.task_id
        try:
            response = annotation_service.status(task_id)
            if not response["active"]:
                status = "ANNOTATED"
            message = ""
        except urllib.error.HTTPError as e:
            status = "FAILED (ANNOTATION)"
            message = get_error_message(e)

        yield id, {"task_id": task_id, "status": status, "message": message}


def process_submitted(
    annotation_service: AnnotationServiceInterface,
    submitted_jobs: Sequence[AnnotationJob],
):
    for job in submitted_jobs:
        id = job.id
        # data = job.data
        # sample_name = job.sample_name
        try:
            if job.data is not None:
                resp = annotation_service.annotate(job)
            elif job.sample_id is not None:
                resp = annotation_service.annotate_sample(job)
            else:
                raise RuntimeError("data or sample_name must be specified to run annotation")
            status = "RUNNING"
            message = ""
            task_id = resp["task_id"]
        except Exception as e:
            status = "FAILED (SUBMISSION)"
            message = get_error_message(e)
            task_id = ""

        # Update
        yield id, {"task_id": task_id, "status": status, "message": message}


def process_annotated(
    annotation_service: AnnotationServiceInterface,
    annotation_jobs: AnnotationJobsInterface,
    annotated_jobs: Sequence[AnnotationJob],
):
    for job in annotated_jobs:
        id = job.id
        task_id = job.task_id
        assert task_id
        try:
            annotated_vcf = annotation_service.process(task_id)
            annotation_jobs.commit()
        except urllib.error.HTTPError as e:
            status = "FAILED (PROCESSING)"
            message = get_error_message(e)
            yield id, {"status": status, "message": message}
            continue
        if job.sample_id is not None and not config["import"]["automatic_deposit_with_sample_id"]:
            status = "DONE"
            message = "Analysis has not been automatically imported"
            yield id, {"status": status, "message": message}
            continue
        try:
            annotation_jobs.deposit(id, annotated_vcf)
            status = "DONE"
            message = ""
        except Exception as e:
            annotation_jobs.rollback()
            status = "FAILED (DEPOSIT)"
            message = e.__class__.__name__ + ": " + getattr(e, "message", str(e))

        yield id, {"status": status, "message": message}


def patch_annotation_job(annotation_jobs: AnnotationJobsInterface, id: int, updates: dict):
    try:
        annotation_jobs.patch(id, **updates)
        annotation_jobs.commit()
    except Exception as e:
        log.error(
            f"Failed patch of annotation job {id} ({str(updates)}): {getattr(e, 'message', str(e))}"
        )
        annotation_jobs.rollback()


def polling(session: Session):
    annotation_jobs = AnnotationJobsInterface(session)
    annotation_service = AnnotationServiceInterface(ANNOTATION_SERVICE_URL, session)

    def loop(session: Session):
        assert session.bind

        while True:
            try:
                session.execute(select(AnnotationJob)).scalars().all()
                if not inspect(session.bind).get_table_names():
                    # Database is not populated
                    session.close()
                    time.sleep(5)
                    continue

                # Process running
                running_jobs = annotation_jobs.get_with_status("RUNNING")
                for id, update in process_running(annotation_service, running_jobs):
                    patch_annotation_job(annotation_jobs, id, update)
                    log.info(f"Processed running job {id} with data {str(update)}")

                # Process submitted
                submitted_jobs = annotation_jobs.get_with_status("SUBMITTED")
                for id, update in process_submitted(annotation_service, submitted_jobs):
                    patch_annotation_job(annotation_jobs, id, update)
                    log.info(f"Processed submitted job {id} with data {str(update)}")

                # Process annotated
                annotated_jobs = annotation_jobs.get_with_status("ANNOTATED")
                for id, update in process_annotated(
                    annotation_service, annotation_jobs, annotated_jobs
                ):
                    patch_annotation_job(annotation_jobs, id, update)
                    log.info(f"Processed annotated job {id} with data {str(update)}")

                # Remove session to avoid a hanging session
                session.close()
                time.sleep(5)
            except OperationalError as e:
                # Database is not alive
                log.warning(f"Failed to poll annotation jobs ({e})")
                session.close()
                time.sleep(5)
                continue

    try:
        loop(session)
    except Exception as e:
        session.close()
        traceback.print_exc()
        raise e


if __name__ == "__main__":
    from ella.applogger import setup_logger

    setup_logger()

    from ella.vardb.util import DB

    db = DB()
    db.connect()

    log.info("Starting polling worker")
    log.info(f"Using annotation service at: {ANNOTATION_SERVICE_URL}")
    polling(db.session)
