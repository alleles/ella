import inspect
import logging
import logging.config
import os
import socket
import sys
from pathlib import Path
from typing import override

from loguru import logger


class InterceptHandler(logging.Handler):
    """Logging handler to intercept standard logging and redirect it to Loguru.

    Copied from https://loguru.readthedocs.io/en/stable/overview.html#entirely-compatible-with-standard-logging
    """

    @override
    def emit(self, record: logging.LogRecord) -> None:
        # Get corresponding Loguru level if it exists.
        level: str | int
        try:
            level = logger.level(record.levelname).name
        except ValueError:
            level = record.levelno

        # Find caller from where originated the logged message.
        frame, depth = inspect.currentframe(), 0
        while frame and (depth == 0 or frame.f_code.co_filename == logging.__file__):
            frame = frame.f_back
            depth += 1
        logger.opt(depth=depth, exception=record.exc_info).log(level, record.getMessage())


# Intercept standard logging and redirect it to Loguru.
logging.basicConfig(handlers=[InterceptHandler()], level=os.environ.get("LOGLEVEL", "INFO"))


def setup_logger() -> None:
    # Set log file path from env variables if given, otherwise default to /var/logs/ella/<hostname>.log
    log_file = Path(os.environ.get("LOGFILE", socket.gethostname() + ".log"))
    log_path = Path(os.environ.get("LOGPATH", "/var/logs/ella"))

    if not log_file.is_absolute():
        log_path.mkdir(parents=True, exist_ok=True)
        log_file = log_path / log_file

    log_level = os.environ.get("LOGLEVEL", "INFO")
    log_level_console = os.environ.get("LOGLEVEL_CONSOLE", log_level)
    log_level_file = os.environ.get("LOGLEVEL_FILE", log_level)
    logger.remove()
    logger.add(sys.stderr, level=log_level_console.upper())

    logger.add(
        log_file,
        level=log_level_file.upper(),
        rotation="00:00",
        retention="5 years",
        # Important for multi-process applications, to avoid file write conflicts.
        enqueue=True,
    )
    logger.info(
        f"Logger setup complete. Log level: {log_level.upper()}. Log file: {log_file} (log level: {log_level_file.upper()})"
    )
