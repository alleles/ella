import logging
from typing import IO

import cyvcf2

from ella.vardb.util.vcfrecord import VCFRecord

log = logging.getLogger(__name__)


RESERVED_GT_HEADERS = {
    "AD": {"Number": "R", "Type": "Integer", "Description": "Injected. Read depth for each allele"},
    "ADF": {
        "Number": "R",
        "Type": "Integer",
        "Description": "Injected. Read depth for each allele on the forward strand",
    },
    "ADR": {
        "Number": "R",
        "Type": "Integer",
        "Description": "Injected. Read depth for each allele on the reverse strand",
    },
    "DP": {"Number": "1", "Type": "Integer", "Description": "Injected. Read depth"},
    "EC": {
        "Number": "A",
        "Type": "Integer",
        "Description": "Injected. Expected alternate allele counts",
    },
    "FT": {
        "Number": "1",
        "Type": "String",
        "Description": "Injected. Filter indicating if this genotype was “called”",
    },
    "GL": {"Number": "G", "Type": "Float", "Description": "Injected. Genotype likelihoods"},
    "GP": {
        "Number": "G",
        "Type": "Float",
        "Description": "Injected. Genotype posterior probabilities",
    },
    "GQ": {
        "Number": "1",
        "Type": "Integer",
        "Description": "Injected. Conditional genotype quality",
    },
    "GT": {"Number": "1", "Type": "String", "Description": "Injected. Genotype"},
    "HQ": {"Number": "2", "Type": "Integer", "Description": "Injected. Haplotype quality"},
    "MQ": {"Number": "1", "Type": "Integer", "Description": "Injected. RMS mapping quality"},
    "PL": {
        "Number": "G",
        "Type": "Integer",
        "Description": "Injected. Phred-scaled genotype likelihoods rounded to the closest integer",
    },
    "PP": {
        "Number": "G",
        "Type": "Integer",
        "Description": "Injected. Phred-scaled genotype posterior probabilities rounded to the closest integer",
    },
    "PQ": {"Number": "1", "Type": "Integer", "Description": "Injected. Phasing quality"},
    "PS": {"Number": "1", "Type": "Integer", "Description": "Injected. Phase"},
}


class VcfIterator:
    def __init__(self, path_or_fileobject: str | IO):
        self.path_or_fileobject = path_or_fileobject
        self.reader = cyvcf2.VCF(self.path_or_fileobject, gts012=True)
        self.samples = self.reader.samples
        self.meta: dict[str, list] = {}
        self.header_format: dict[str, str] = {}
        for header in self.reader.header_iter():
            info = header.info()
            if header.type not in self.meta:
                self.meta[header.type] = []
            self.meta[header.type].append(info)
            if "ID" in info:
                self.header_format[info["ID"]] = header.type

        self.add_format_headers()

    def add_format_headers(self):
        "Add format headers if they do not exist. This is a subset of the reserved genotype keys from https://samtools.github.io/hts-specs/VCFv4.3.pdf (table 2)"
        for key, fmt in RESERVED_GT_HEADERS.items():
            if key in self.reader and self.header_format.get(key) == "FORMAT":
                existing_header_line = self.reader[key]
                if (
                    existing_header_line["Number"] != fmt["Number"]
                    or existing_header_line["Type"] != fmt["Type"]
                ):
                    log.warning(
                        f"Header for format field {key} in VCF does not match VCF spec. Ignoring."
                    )
            else:
                self.reader.add_format_to_header({**fmt, **{"ID": key}})

    def iter_raw(self):
        for variant in self.reader:
            yield str(variant), variant

    def __iter__(self):
        for variant in self.reader:
            r = VCFRecord(variant, self.samples, self.meta)
            yield r
