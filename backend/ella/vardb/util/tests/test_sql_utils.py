import json
from datetime import datetime

from sqlalchemy import select, text
from sqlalchemy.orm import Session
from sqlalchemy.sql.expression import TableClause

from ella.vardb.datamodel import user
from ella.vardb.util.sql_utils import create_temp_table, get_execution_plan


def get_statement():
    return (
        select(user.User)
        .join(user.UserGroup)
        .where(user.UserGroup.name == "testgroup01")
        .order_by(user.User.id)
    )


def test_execution_plan(session: Session, capsys):
    # Test that JSON output is valid JSON
    statement = get_statement()
    explained = get_execution_plan(session, statement, analyze=True, as_json=True, stdout=False)
    [json.loads(e) for e in explained]

    # Test that stdout is printed
    get_execution_plan(session, statement)
    captured = capsys.readouterr()
    assert "QUERY PLAN" in captured.out


def table_exists(session: Session, table: TableClause) -> bool | None:
    return session.execute(
        text(
            "SELECT EXISTS ("
            "   SELECT 1 "
            "   FROM information_schema.tables "
            "   WHERE table_schema LIKE 'pg_temp_%'"
            f"   AND table_name = '{table.name}')"
        )
    ).scalar()


def is_analyzed(session: Session, table: TableClause):
    """Check if table has been analyzed"""
    return isinstance(
        session.execute(
            text(f"SELECT last_analyze FROM pg_stat_all_tables WHERE relname = '{table.name}'")
        ).scalar(),
        datetime,
    )


def has_index(session: Session, table: TableClause, column: str) -> bool | None:
    """Check if table has index on column"""
    return session.execute(
        text(
            "SELECT EXISTS ("
            "   SELECT 1 "
            "   FROM pg_indexes "
            f"   WHERE tablename = '{table.name}' AND indexdef LIKE '%{column}%')"
        )
    ).scalar()


def test_temp_table(session: Session):
    # Test that temp table exists, before being dropped on commit
    statement = get_statement()
    name = "usergroup"
    temp_table = create_temp_table(session, statement, name, analyze=False)
    assert table_exists(session, temp_table)
    session.commit()
    assert not table_exists(session, temp_table)

    # Test that ANALYZE works as intended
    temp_table = create_temp_table(session, statement, name)
    assert is_analyzed(session, temp_table)
    temp_table = create_temp_table(session, statement, name, analyze=False)
    assert not is_analyzed(session, temp_table)

    # Test that index is created
    idx_columns = ["username", "first_name"]
    temp_table = create_temp_table(session, statement, name, index_columns=idx_columns)
    for column in idx_columns:
        assert has_index(session, temp_table, column)

    # Test that temp_table returns same results as direct query
    temp_table_results = session.execute(select(temp_table)).all()
    users = session.execute(statement).scalars().all()
    for user_obj, temp_table_row in zip(users, temp_table_results):
        for column, value in zip(temp_table.columns.keys(), temp_table_row):
            assert getattr(user_obj, column) == value
