import logging
import os

import requests

from ella.api import db
from ella.api.config import config
from ella.vardb.datamodel.annotationshadow import update_annotation_shadow_columns

logging.getLogger("urllib3.connectionpool").setLevel(logging.WARNING)


class TestDatabase:
    # Do not collect this class for pytest
    __test__ = False

    def __init__(self):
        db_url = os.environ["DB_URL"]
        self.migration = bool(os.environ.get("MIGRATION"))
        self.db_name = db_url.rsplit("/", 1)[-1]

    def refresh(self):
        """
        Wipes out whole database, and recreates a clean copy from the template.
        """
        print("Refreshing database...")
        params: dict[str, str | bool] = {
            "database": self.db_name,
            "testset": "integration_testing",
            "migration": self.migration,
            "clean": False,
        }
        response = requests.get(
            "http://testdata:23232/reset",
            params=params,
        )
        assert response.status_code == 200
        # Update mapping of annotation shadow tables based on global config
        update_annotation_shadow_columns(config)

    def cleanup(self):
        print("Disconnecting...")
        db.disconnect()
