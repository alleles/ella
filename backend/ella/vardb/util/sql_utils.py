"""Utility functions for printing execution plans of SQL statements and creating ON COMMIT DROP
temporary tables.
"""

import json
import logging
import random
import string

from sqlalchemy import CompoundSelect, Select, table
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.orm import Session
from sqlalchemy.sql import text
from sqlalchemy.sql.compiler import SQLCompiler
from sqlalchemy.sql.expression import ClauseElement, Executable

logger = logging.getLogger(__name__)


class Explain(Executable, ClauseElement):
    inherit_cache = True  # enable SQL compilation caching

    def __init__(self, statement: Select | CompoundSelect, analyze=False, json=False):
        self.statement = statement
        self.analyze = analyze
        self.json = json
        self.inline = getattr(statement, "inline", None)  # helps with INSERT statements


@compiles(Explain, "postgresql")
def compile_explain(element: Explain, compiler: SQLCompiler, **kw):
    command = "EXPLAIN "
    if element.json:
        command += "(ANALYZE, COSTS, VERBOSE, BUFFERS, FORMAT JSON)"
    elif element.analyze:
        command += "ANALYZE "
    command += compiler.process(element.statement, **kw)
    return command


def get_execution_plan(
    session: Session,
    statement: Select | CompoundSelect,
    analyze=False,
    as_json=False,
    stdout=True,
):
    """Prints or returns the execution plan (EXPLAIN ANALYZE) of an SQL query to aid in
    performance debugging.

    By default, prints to stdout without any optional EXPLAIN parameters. Setting analyze=True
    includes the ANALYZE parameter, and as_json=True further adds VERBOSE, COSTS, BUFFERS,
    and FORMAT JSON. The json option is for use with http://tatiyants.com/pev/

    Call using any Select construct:
        statement = Select(...).join(...).where(...) ...

        get_execution_plan(session, statement)
    """
    explained = session.execute(Explain(statement, analyze=analyze, json=as_json)).scalars().all()
    if as_json:
        explained = [json.dumps(e) for e in explained]

    if stdout:
        print(
            """                                                       QUERY PLAN
---------------------------------------------------------------------------------------------------------------------------"""
        )
        for e in explained:
            print(e)
    else:
        return explained


class CreateTempTableAs(Executable, ClauseElement):
    inherit_cache = True  # enable SQL compilation caching

    def __init__(self, name: str, statement: Select | CompoundSelect):
        self.name = name
        self.statement = statement


@compiles(CreateTempTableAs, "postgresql")
def compile_create_temp_table_as(element: CreateTempTableAs, compiler: SQLCompiler, **kw):
    return f"CREATE TEMP TABLE {element.name} ON COMMIT DROP AS {compiler.process(element.statement, **kw)}"


def create_temp_table(
    session: Session,
    statement: Select | CompoundSelect,
    name: str,
    analyze=True,
    index_columns: list[str] | None = None,
):
    """Creates ON COMMIT DROP temporary table from SQL statement with provided name.

    Optionally analyzes table and constructs indexes on the specified columns.
    name is prefixed with 'tmp_table' and a random hash to avoid name clashing, e.g.
    tmp_table_edomlfph_<name>

    :warning: name is not escaped.

    Returns table() structure of select statement. If database user does not have write-access,
    returns self.subquery(name) instead.
    """
    has_write_access = session.execute(
        text(
            "SELECT * FROM pg_catalog.has_schema_privilege(current_user, current_schema(), 'CREATE')"
        )
    ).scalar()

    if not has_write_access:
        logger.warning(
            "User does not have write access on current schema. Will not create temp table."
        )
        return statement.subquery(name)

    prefix = "".join(random.choice(string.ascii_lowercase) for _ in range(8))

    prefix_name = "tmp_table_" + prefix + "_" + name
    session.execute(text(f"DROP TABLE IF EXISTS {prefix_name}"))
    session.execute(CreateTempTableAs(prefix_name, statement))

    if analyze:
        session.execute(text(f"ANALYZE {prefix_name}"))

    if index_columns:
        for column in index_columns:
            session.execute(text(f"CREATE INDEX idx_{prefix}_{column} ON {prefix_name} ({column})"))

    columns = []
    for c in statement.subquery().columns.values():
        c.table = None  # type: ignore[attr-defined]
        columns.append(c)
    return table(prefix_name, *columns)  # type: ignore[arg-type]
