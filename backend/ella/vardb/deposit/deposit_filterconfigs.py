import datetime
import logging

import pytz
from sqlalchemy import select
from sqlalchemy.orm import Session

from ella.api.config import config
from ella.vardb.datamodel import annotationshadow, sample, user
from ella.vardb.datamodel.pydantic.filterconfig import FilterConfigModel

log = logging.getLogger(__name__)


def deposit_filterconfigs(session: Session, fc_configs: list[dict]):
    result: dict[str, list] = {
        "fc_updated": [],
        "fc_created": [],
        "ugfc_created": [],
        "ugfc_updated": [],
    }
    for fc_config in fc_configs:
        filterconfig = FilterConfigModel(**fc_config["filterconfig"])
        name = fc_config["name"]
        requirements = fc_config["requirements"]
        fc = {"name": name, "filterconfig": filterconfig, "requirements": requirements}

        existing = session.execute(
            select(sample.FilterConfig).where(
                sample.FilterConfig.name == fc["name"],
                sample.FilterConfig.date_superceeded.is_(None),
            )
        ).scalar_one_or_none()

        if (
            existing
            and fc["filterconfig"] == existing.filterconfig
            and fc["requirements"] == existing.requirements
        ):
            if not existing.active:
                log.warning(
                    f"Filter config {existing} exists with same configuration, but is set as inactive. Will not be updated."
                )
            fc_obj = existing
        else:
            if existing:
                if not existing.active:
                    log.warning(
                        f"Filter config {existing} already set as inactive. Will be set as superceeded."
                    )
                existing.date_superceeded = datetime.datetime.now(pytz.utc)
                fc["previous_filterconfig_id"] = existing.id
                existing.active = False

            # Check that filterconfig is supported by available annotationshadowfrequency columns
            annotationshadow.check_filterconfig(filterconfig, config)
            fc_obj = sample.FilterConfig(**fc)
            session.add(fc_obj)
            session.flush()
            if existing:
                result["fc_updated"].append(fc_obj.id)
            else:
                result["fc_created"].append(fc_obj.id)

        for usergroup in fc_config["usergroups"]:
            usergroup_name = usergroup["name"]
            usergroup_id = session.execute(
                select(user.UserGroup.id).where(user.UserGroup.name == usergroup_name)
            ).scalar_one()

            ugfc = {
                "usergroup_id": usergroup_id,
                "filterconfig_id": fc_obj.id,
                "order": usergroup["order"],
            }

            existing_ugfc = session.execute(
                select(sample.UserGroupFilterConfig).where(
                    sample.UserGroupFilterConfig.usergroup_id == usergroup_id,
                    sample.UserGroupFilterConfig.filterconfig_id == fc_obj.id,
                )
            ).scalar_one_or_none()

            if existing_ugfc and existing_ugfc.order != usergroup["order"]:
                log.info(
                    f"Updating order of filterconfig {fc_obj} for usergroup {usergroup_name} from"
                    f" {existing_ugfc.order} to {usergroup['order']}"
                )
                existing_ugfc.order = usergroup["order"]
                result["ugfc_updated"].append(existing_ugfc.id)
            elif not existing_ugfc:
                ugfc_obj = sample.UserGroupFilterConfig(**ugfc)
                session.add(ugfc_obj)
                session.flush()
                result["ugfc_created"].append(ugfc_obj.id)

    session.flush()

    return result
