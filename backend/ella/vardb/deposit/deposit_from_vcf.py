#!/usr/bin/env python
"""
Code for loading the contents of VCF files into the vardb database.

Use one transaction for whole file, and prompts user before committing.
Adds annotation if supplied annotation is different than what is already in db.
Can use specific annotation parsers to split e.g. allele specific annotation.
"""


import logging
from collections import defaultdict

import sqlalchemy.orm.exc
from sqlalchemy import and_, select
from sqlalchemy.orm import Session

from ella.vardb.datamodel import gene
from ella.vardb.deposit.importers import (
    AlleleImporter,
    AlleleInterpretationImporter,
    AnalysisImporter,
    AnalysisInterpretationImporter,
    AnnotationImporter,
    GenotypeImporter,
    SampleImporter,
)

log = logging.getLogger(__name__)


class DepositFromVCF:
    def __init__(self, session: Session):
        self.session = session
        self.sample_importer = SampleImporter(self.session)
        self.annotation_importer = AnnotationImporter(self.session)
        self.allele_importer = AlleleImporter(self.session)
        self.genotype_importer = GenotypeImporter(self.session)
        self.analysis_importer = AnalysisImporter(self.session)
        self.analysis_interpretation_importer = AnalysisInterpretationImporter(self.session)
        self.allele_interpretation_importer = AlleleInterpretationImporter(self.session)
        self.counter: defaultdict[str, int] = defaultdict(int)

    def get_genepanel(self, gp_name: str, gp_version: str):
        try:
            genepanel = self.session.execute(
                select(gene.Genepanel).where(
                    and_(gene.Genepanel.name == gp_name, gene.Genepanel.version == gp_version)
                )
            ).scalar_one()
        except sqlalchemy.orm.exc.NoResultFound:
            log.warning(f"Genepanel {gp_name} version {gp_version} not available in varDB")
            raise RuntimeError(f"Genepanel {gp_name} version {gp_version} not available in varDB")
        return genepanel

    def import_vcf(self, *args, **kwargs):
        raise RuntimeError("import_vcf must be overloaded in subclass")

    def is_within_genepanel(self, record, genepanel: gene.Genepanel) -> bool:
        chr = record.variant.CHROM
        pos = record.variant.POS - 1  # We use zero-based transcripts
        for tx in genepanel.transcripts:
            if chr == tx.chromosome and (tx.tx_start <= pos <= tx.tx_end):
                return True
        for region in genepanel.regions:
            if chr == region.chromosome and (region.start <= pos <= region.end):
                return True
        return False

    def getCounter(self):
        counter = dict(self.counter)
        counter.update(self.sample_importer.counter)
        counter.update(self.allele_importer.counter)
        counter.update(self.annotation_importer.counter)
        counter.update(self.genotype_importer.counter)
        return counter

    def printStats(self):
        stats = self.getCounter()
        print(f"Samples to add: {stats['nSamplesAdded']}")
        print(f"Variants in file: {stats.get('nVariantsInFile', '???')}")
        print(f"Alternative alleles to add: {stats.get('nAltAlleles', '???')}")
        print(f"Novel alt alleles to add: {stats.get('nNovelAltAlleles', '???')}")
        print()
        print(f"Novel annotations to add: {stats.get('nNovelAnnotation', '???')}")
        print(f"Updated annotations: {stats.get('nUpdatedAnnotation', '???')}")
        print(f"Annotations unchanged: {stats.get('nNoChangeAnnotation', '???')}")
        print()
        print(f"Genotypes hetro ref: {stats.get('nGenotypeHetroRef', '???')}")
        print(f"Genotypes homo nonref: {stats.get('nGenotypeHomoNonRef', '???')}")
        print(f"Genotypes hetro nonref: {stats.get('nGenotypeHetroNonRef', '???')}")
        print(
            f"Genotypes not added (not variant/not called/sample not added):"
            f" {stats.get('nGenotypeNotAdded', '???')}"
        )
