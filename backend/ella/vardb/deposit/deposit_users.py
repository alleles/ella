import argparse
import json
import logging
from pathlib import Path
from typing import Any

from sqlalchemy import delete, insert, or_, select
from sqlalchemy.orm import Session

from ella.api.config import config
from ella.datalayer import filters
from ella.vardb.datamodel import annotationshadow, gene, user
from ella.vardb.util import DB

log = logging.getLogger(__name__)


def import_groups(session: Session, groups, log=log.info):
    # Store import_groups data (has to be inserted after groups)
    import_groups: dict[str, list[str]] = dict()

    for g in groups:
        group_data = dict(g)
        group_name = group_data["name"]

        existing_group = session.execute(
            select(user.UserGroup).where(user.UserGroup.name == group_data["name"])
        ).scalar_one_or_none()

        db_official_genepanels = (
            session.execute(
                select(gene.Genepanel).where(
                    or_(
                        # If genepanel is specified with version, use that
                        filters.in_(
                            (gene.Genepanel.name, gene.Genepanel.version),
                            [gp for gp in group_data["genepanels"] if len(gp) == 2],
                        ),
                        # Otherwise, pick all versions of a genepanel
                        filters.in_(
                            gene.Genepanel.name,
                            [gp[0] for gp in group_data["genepanels"] if len(gp) == 1],
                        ),
                    ),
                    gene.Genepanel.official.is_(True),
                )
            )
            .scalars()
            .all()
        )

        for group_panel in group_data["genepanels"]:
            if len(group_panel) == 2:
                assert tuple(group_panel) in [
                    (gp.name, gp.version) for gp in db_official_genepanels
                ], f"Could not find genepanel {group_panel} in database"
            else:
                assert group_panel[0] in [
                    gp.name for gp in db_official_genepanels
                ], f"Could not find any genepanels named '{group_panel[0]}' in database"

        import_groups[group_name] = group_data.pop("import_groups", [group_name])

        if group_data.get("default_import_genepanel"):
            default_import_genepanel = group_data.pop("default_import_genepanel")

            # Use specified version if provided, otherwise, use latest version
            if len(default_import_genepanel) == 1:
                latest_version = (
                    session.execute(
                        select(gene.Genepanel.version)
                        .where(
                            gene.Genepanel.name == default_import_genepanel[0],
                            gene.Genepanel.official.is_(True),
                        )
                        .order_by(gene.Genepanel.date_created.desc())
                    )
                    .scalars()
                    .first()
                )
                assert latest_version, f"Could not find genepanel {default_import_genepanel[0]}"
                default_import_genepanel = (default_import_genepanel[0], latest_version)

            assert tuple(default_import_genepanel) in [
                (gp.name, gp.version) for gp in db_official_genepanels
            ], f"Could not find genepanel {default_import_genepanel} in database"

            group_data["default_import_genepanel_name"] = default_import_genepanel[0]
            group_data["default_import_genepanel_version"] = default_import_genepanel[1]

        if not existing_group:
            group_data["genepanels"] = db_official_genepanels
            new_group = user.UserGroup(**group_data)
            session.add(new_group)
            log(f"Added user group {group_name}")
        else:
            # Keep unofficial genepanels for group
            db_unofficial_genepanels = [
                gp for gp in existing_group.genepanels if gp.official is False
            ]
            group_data["genepanels"] = list(db_official_genepanels) + list(db_unofficial_genepanels)
            log(f"User group {group_data['name']} already exists, updating record...")
            for k, v in group_data.items():
                setattr(existing_group, k, v)

        # Check that usergroup's ACMG config is compatible with annotationshadowfrequency columns
        annotationshadow.check_usergroup_config(
            existing_group if existing_group else new_group, config
        )

    # Handle import_groups
    session.flush()
    usergroup_id_names = session.execute(select(user.UserGroup.id, user.UserGroup.name)).all()
    usergroup_name_id: dict[str, int] = {gn: gid for gid, gn in usergroup_id_names}
    for group_name, import_group_names in import_groups.items():
        # Delete all usergroupimport rows for this group before re-inserting new
        session.execute(
            delete(user.UserGroupImport).where(
                user.UserGroupImport.usergroup_id == usergroup_name_id[group_name]
            )
        )
        for import_group_name in import_group_names:
            session.execute(
                insert(user.UserGroupImport).values(
                    usergroup_id=usergroup_name_id[group_name],
                    usergroupimport_id=usergroup_name_id[import_group_name],
                )
            )

    session.commit()


def import_users(session: Session, users: list[dict[str, Any]]):
    for u in users:
        existing_user = session.execute(
            select(user.User).where(user.User.username == u["username"])
        ).scalar_one_or_none()

        if "group" not in u:
            raise RuntimeError(f"User {u['username']} is not in any group.")
        u["group"] = session.execute(
            select(user.UserGroup).where(user.UserGroup.name == u["group"])
        ).scalar_one_or_none()

        if not existing_user:
            new_user = user.User(**u)
            session.add(new_user)
            log.info(f"Adding user {u['username']}")
        else:
            log.info(f"Username {u['username']} already exists, updating record...")
            for k, v in u.items():
                setattr(existing_user, k, v)

    session.commit()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="""Adds new users from JSON file.""")
    parser.add_argument(
        "--users", action="store", dest="users", required=False, help="Path to users JSON file"
    )
    parser.add_argument(
        "--groups", action="store", dest="groups", required=False, help="Path to groups JSON file"
    )

    args = parser.parse_args()

    if not args.users and not args.groups:
        raise RuntimeError("You must specify either --users or --groups")

    if args.users:
        with Path(args.users).open() as fd:
            users = json.load(fd)

    if args.groups:
        with Path(args.groups).open() as fd:
            groups = json.load(fd)

    db = DB()
    db.connect()

    # User can reference group, so we need to import groups first
    if groups:
        import_groups(db.session, groups)
    if users:
        import_users(db.session, users)
