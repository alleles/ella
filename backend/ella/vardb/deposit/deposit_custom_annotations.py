import argparse
import json
import logging
from io import TextIOWrapper
from pathlib import Path

from sqlalchemy.orm import Session

from ella.vardb.datamodel import annotation
from ella.vardb.util import DB

"""
Populate customannotation database in E||A
"""

SCRIPT_DIR = Path(__file__).parent.resolve()
log = logging.getLogger(__name__)


def get_custom_annotation_batches(f: TextIOWrapper):
    batch = []
    for ca in f:
        ca_as_dict = json.loads(ca)
        batch.append(ca_as_dict)
    return batch


def import_custom_annotations(session: Session, filepath: Path):
    """
    :param session: an sqlalchemy 'session' of E||A database
    :param filepath: a file with one json-formatted custom annotation per line
    :return : update E||A database with annotations from 'filepath'
    """
    log.info(f"Importing custom annotations from {filepath}")
    with filepath.open() as f:
        for ca in get_custom_annotation_batches(f):
            log.info("Adding custom annotation")
            session.add(annotation.CustomAnnotation(**ca))
        session.commit()

    log.info("Custom annotations successfully imported!")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Deposit custom annotations from file")
    parser.add_argument(
        "json_file", type=str, help="relative path to file containing list of custom annotations"
    )
    args = parser.parse_args()

    from applogger import setup_logger

    setup_logger()

    filepath = Path(args.json_file).resolve()
    # Import argparse, add CLI for getting path to JSON file.
    db = DB()
    db.connect()
    import_custom_annotations(db.session, filepath)
