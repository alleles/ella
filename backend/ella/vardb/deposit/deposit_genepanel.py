#!/usr/bin/env python
"""
Code for adding or modifying gene panels in varDB.
"""

import argparse
import logging
import sys
from pathlib import Path

from pydantic import BaseModel, ConfigDict, Field, ValidationError, field_validator
from sqlalchemy import and_, delete, insert, select
from sqlalchemy.orm import Session

from ella.vardb.datamodel import gene
from ella.vardb.deposit.importers import bulk_insert_nonexisting
from ella.vardb.util import DB

log = logging.getLogger(__name__)


class TranscriptRecord(BaseModel):
    chromosome: str = Field(alias="chromosome")
    start: int = Field(alias="read start")
    end: int = Field(alias="read end")
    name: str = Field(alias="name")
    # score: float = Field(default=0.0, alias="score")
    strand: str = Field(default="+", alias="strand")
    tags: list[str] | None = Field(default=None, alias="tags")
    hgnc_id: int = Field(alias="HGNC id")
    gene_symbol: str = Field(alias="HGNC symbol")
    inheritance: str = Field(alias="inheritance")
    coding_start: int | None = Field(default=None, alias="coding start")
    coding_end: int | None = Field(default=None, alias="coding end")
    exon_starts: list[int] = Field(alias="exon starts")
    exon_ends: list[int] = Field(alias="exon ends")
    # metadata: Optional[str] = Field(default=None, alias="metadata")

    @field_validator("coding_start", "coding_end", mode="before")
    def validate_coding_start_end(cls, v):
        if v:
            return v
        return None

    @field_validator("exon_starts", "exon_ends", mode="before")
    def validate_exon_starts_ends(cls, v):
        if v is None:
            return None
        return [int(x) for x in v.split(",") if x]

    @field_validator("tags", mode="before")
    def validate_tags(cls, v):
        if v == "" or v is None:
            return None
        return [x.strip() for x in v.split(",")]

    def as_row(self, genome_ref: str):
        return {
            "gene_id": self.hgnc_id,  # foreign key to gene
            "transcript_name": self.name,
            "type": "RefSeq",
            "chromosome": self.chromosome,
            "tx_start": self.start,
            "tx_end": self.end,
            "strand": self.strand,
            "tags": self.tags,
            "cds_start": self.coding_start,
            "cds_end": self.coding_end,
            "exon_starts": self.exon_starts,
            "exon_ends": self.exon_ends,
            "genome_reference": genome_ref,
        }

    model_config = ConfigDict(extra="ignore")


class RegionRecord(BaseModel):
    chromosome: str
    start: int = Field(alias="read start")
    end: int = Field(alias="read end")
    inheritance: str
    name: str

    def as_row(self, genome_ref: str):
        return {
            "name": self.name,
            "chromosome": self.chromosome,
            "start": self.start,
            "end": self.end,
            "genome_reference": genome_ref,
        }


class PhenotypeRecord(BaseModel):
    hgnc_id: int = Field(alias="HGNC id")
    hgnc_symbol: str = Field(alias="HGNC symbol")
    inheritance: str = Field(default="N/A")
    omim_id: int | None = Field(alias="phenotype MIM number")
    description: str = Field(alias="phenotype")

    @field_validator("omim_id", mode="before")
    def empty_str_to_none(cls, v):
        if v == "":
            return None
        return v

    @field_validator("inheritance", mode="before")
    def empty_inheritance_to_n_a(cls, v):
        if v == "":
            return "N/A"
        return v

    def as_row(self):
        return {
            "gene_id": self.hgnc_id,
            "description": self.description,
            "inheritance": self.inheritance,
            "omim_id": self.omim_id,
        }


def load_phenotypes(phenotypes_path: Path):
    header = None
    phenotypes = []
    with phenotypes_path.open() as f:
        for line in f:
            if line.startswith("#"):
                continue
            if line.startswith("HGNC id"):
                header = line.strip().split("\t")
                continue
            if not header:
                raise RuntimeError(
                    f"No valid header found in {phenotypes_path}."
                    " Make sure the file header starts with 'HGNC id'."
                )

            data = dict(list(zip(header, [l.strip() for l in line.split("\t")])))
            ph_record = PhenotypeRecord(**data)
            phenotypes.append(ph_record)

    return phenotypes


def load_transcripts_and_regions(
    transcripts_path: Path,
) -> tuple[list[TranscriptRecord], list[RegionRecord]]:
    header = None
    transcripts = []
    regions = []
    with transcripts_path.open() as f:
        for l in f:
            if l.startswith("#chromosome"):
                header = l.strip().split("\t")
                header[0] = header[0][1:]  # strip leading '#'
            if l.startswith("#"):
                continue
            if not header:
                raise RuntimeError(
                    f"No valid header found in {transcripts_path}."
                    " Make sure the file header starts with '#chromosome'."
                )

            data = dict(zip(header, [l.strip() for l in l.split("\t")]))
            try:
                tx_record = TranscriptRecord.model_validate(data)
                transcripts.append(tx_record)
            except ValidationError as e:
                log.debug(f"Failed to validate data as transcript: {e} - trying as region")
                region_record = RegionRecord.model_validate(data)
                regions.append(region_record)
    return transcripts, regions


class DepositGenepanel:
    def __init__(self, session: Session):
        self.session = session

    def insert_genes(self, transcript_data: list[TranscriptRecord]):
        # Avoid duplicate genes
        distinct_genes = set()
        for t in transcript_data:
            distinct_genes.add((t.hgnc_id, t.gene_symbol))

        gene_rows = list()
        for d in list(distinct_genes):
            gene_rows.append({"hgnc_id": d[0], "hgnc_symbol": d[1]})

        gene_inserted_count = 0
        gene_reused_count = 0
        for existing, created in bulk_insert_nonexisting(
            self.session,
            gene.Gene,
            gene_rows,
            include_pk="hgnc_id",
            compare_keys=["hgnc_id"],
            replace=True,
        ):
            gene_inserted_count += len(created)
            gene_reused_count += len(existing)
        return gene_inserted_count, gene_reused_count

    def insert_transcripts(
        self,
        transcript_data: list[TranscriptRecord],
        genepanel_name: str,
        genepanel_version: str,
        genome_ref: str,
        replace: bool = False,
    ):
        transcript_rows = [t.as_row(genome_ref) for t in transcript_data]
        inheritances = {t.name: t.inheritance for t in transcript_data}

        transcript_inserted_count = 0
        transcript_reused_count = 0

        # If replacing, delete old connections in junction table
        if replace:
            log.debug("Replacing transcripts connected to genepanel.")
            self.session.execute(
                delete(gene.GenepanelTranscript).where(
                    and_(
                        gene.GenepanelTranscript.genepanel_name == genepanel_name,
                        gene.GenepanelTranscript.genepanel_version == genepanel_version,
                    )
                )
            )

        for existing, created in bulk_insert_nonexisting(
            self.session,
            gene.Transcript,
            transcript_rows,
            include_pk="id",
            compare_keys=["transcript_name"],
            replace=replace,
        ):
            transcript_inserted_count += len(created)
            transcript_reused_count += len(existing)

            # Connect to genepanel by inserting into the junction table
            junction_values = list()
            for tx in existing + created:
                junction_values.append(
                    {
                        "genepanel_name": genepanel_name,
                        "genepanel_version": genepanel_version,
                        "transcript_id": tx["id"],
                        "inheritance": inheritances[tx["transcript_name"]],
                    }
                )
            self.session.execute(insert(gene.GenepanelTranscript), junction_values)

        return transcript_inserted_count, transcript_reused_count

    def insert_phenotypes(
        self,
        phenotype_data: list[PhenotypeRecord],
        genepanel_name,
        genepanel_version,
        replace=False,
    ):
        phenotype_rows = [ph.as_row() for ph in phenotype_data]

        phenotype_inserted_count = 0
        phenotype_reused_count = 0

        # If replacing, delete old connections in junction table
        if replace:
            log.debug("Replacing transcripts connected to genepanel.")
            self.session.execute(
                delete(gene.GenepanelPhenotype).where(
                    and_(
                        gene.GenepanelPhenotype.genepanel_name == genepanel_name,
                        gene.GenepanelPhenotype.genepanel_version == genepanel_version,
                    )
                )
            )

        for existing, created in bulk_insert_nonexisting(
            self.session,
            gene.Phenotype,
            phenotype_rows,
            include_pk="id",
            compare_keys=["gene_id", "description", "inheritance"],
            replace=replace,
        ):
            phenotype_inserted_count += len(created)
            phenotype_reused_count += len(existing)
            # Connect to genepanel by inserting into the junction table
            junction_values = list()
            pks = [i["id"] for i in existing + created]
            for pk in pks:
                junction_values.append(
                    {
                        "genepanel_name": genepanel_name,
                        "genepanel_version": genepanel_version,
                        "phenotype_id": pk,
                    }
                )
            self.session.execute(insert(gene.GenepanelPhenotype), junction_values)

        return phenotype_inserted_count, phenotype_reused_count

    def insert_regions(
        self,
        region_data: list[RegionRecord],
        genepanel_name: str,
        genepanel_version: str,
        genome_ref: str,
        replace: bool = False,
    ):
        region_rows = [r.as_row(genome_ref) for r in region_data]
        inheritances = {r.name: r.inheritance for r in region_data}

        region_inserted_count = 0
        region_reused_count = 0

        # If replacing, delete old connections in junction table
        if replace:
            log.debug("Replacing regions connected to genepanel.")
            self.session.execute(
                delete(gene.GenepanelRegion).where(
                    and_(
                        gene.GenepanelRegion.genepanel_name == genepanel_name,
                        gene.GenepanelRegion.genepanel_version == genepanel_version,
                    )
                )
            )

        for existing, created in bulk_insert_nonexisting(
            self.session,
            gene.Region,
            region_rows,
            include_pk="id",
            compare_keys=["chromosome", "start", "end", "genome_reference", "name"],
            replace=replace,
        ):
            region_inserted_count += len(created)
            region_reused_count += len(existing)

            # Connect to genepanel by inserting into the junction table
            junction_values = list()
            for r in existing + created:
                junction_values.append(
                    {
                        "genepanel_name": genepanel_name,
                        "genepanel_version": genepanel_version,
                        "region_id": r["id"],
                        "inheritance": inheritances[r["name"]],
                    }
                )
            self.session.execute(insert(gene.GenepanelRegion), junction_values)

        return region_inserted_count, region_reused_count

    def add_genepanel(
        self,
        transcripts_path: Path | None,
        phenotypes_path: Path | None,
        genepanel_name: str,
        genepanel_version: str,
        genomeRef="GRCh37",
        replace=False,
    ):
        if not transcripts_path:
            raise RuntimeError("Missing mandatory argument: path to transcript file")
        if not phenotypes_path:
            raise RuntimeError("Missing mandatory argument: path to phenotypes file")

        # Insert genepanel
        existing_panel = self.session.execute(
            select(gene.Genepanel).where(
                gene.Genepanel.name == genepanel_name, gene.Genepanel.version == genepanel_version
            )
        ).scalar_one_or_none()

        if not existing_panel:
            # We connect transcripts and phenotypes to genepanel later
            genepanel = gene.Genepanel(
                name=genepanel_name,
                version=genepanel_version,
                genome_reference=genomeRef,
                official=True,
            )
            self.session.add(genepanel)

        else:
            if replace:
                log.info(
                    f"Genepanel {genepanel_name} {genepanel_version} exists in database, will overwrite."
                )
                existing_panel.genome_reference = genomeRef
            else:
                log.info(
                    f"Genepanel {genepanel_name} {genepanel_version} exists in database, backing"
                    " out. Use the 'replace' to force overwrite."
                )
                return

        transcript_data, region_data = load_transcripts_and_regions(transcripts_path)
        if not transcript_data and not region_data:
            raise RuntimeError("Found no transcripts or regions in file")
        phenotype_data = load_phenotypes(phenotypes_path)
        if not phenotype_data:
            raise RuntimeError("Found no phenotypes in file")

        # Genes
        gene_inserted_count, gene_reused_count = self.insert_genes(transcript_data)
        log.info(f"GENES: Created {gene_inserted_count}, reused {gene_reused_count}")

        # Transcripts
        transcript_inserted_count, transcript_reused_count = self.insert_transcripts(
            transcript_data, genepanel_name, genepanel_version, genomeRef, replace=replace
        )

        log.info(
            f"TRANSCRIPTS: Created {transcript_inserted_count}, reused {transcript_reused_count}"
        )

        # Regions
        regions_inserted_count, regions_reused_count = self.insert_regions(
            region_data, genepanel_name, genepanel_version, genomeRef, replace=replace
        )

        log.info(f"REGIONS: Created {regions_inserted_count}, reused {regions_reused_count}")

        # Phenotypes
        phenotype_inserted_count, phenotype_reused_count = self.insert_phenotypes(
            phenotype_data, genepanel_name, genepanel_version, replace=replace
        )

        log.info(f"PHENOTYPES: Created {phenotype_inserted_count}, reused {phenotype_reused_count}")

        self.session.commit()
        log.info(
            f"Added {genepanel_name} {genepanel_version} with {gene_inserted_count + gene_reused_count} genes,"
            f" {transcript_inserted_count + transcript_reused_count} transcripts,"
            f" {regions_inserted_count + regions_reused_count} regions, and"
            f" {phenotype_inserted_count + phenotype_reused_count} phenotypes to database"
        )
        return 0


def main(argv=None):
    """
    Example:
        ./deposit_genepanel.py \
            --transcripts=./clinicalGenePanels/HBOC/HBOC_genes_transcripts.tsv \
            --phenotypes=./clinicalGenePanels/HBOC/HBOC_phenotypes.tsv \
    """
    argv = argv or sys.argv[1:]
    parser = argparse.ArgumentParser(
        description="""Adds or updates gene panels in varDB.
                       Use any or all of --transcripts/phenotypes.
                       If the panel exists you must add the --replace option.\n
                       When creating a new panel, use -transcripts and --phenotypes without --replace"""
    )
    parser.add_argument(
        "--transcripts",
        action="store",
        dest="transcriptsPath",
        required=True,
        help="Path to gene panel transcripts file",
    )
    parser.add_argument(
        "--phenotypes",
        action="store",
        dest="phenotypesPath",
        required=True,
        help="Path to gene panel phenotypes file",
    )
    parser.add_argument(
        "--genepanelname",
        action="store",
        dest="genepanelName",
        required=True,
        help="Name for gene panel",
    )
    parser.add_argument(
        "--genepanelversion",
        action="store",
        dest="genepanelVersion",
        required=True,
        help="Version of this gene panel",
    )
    parser.add_argument(
        "--genomeref",
        action="store",
        dest="genomeRef",
        required=False,
        default="GRCh37",
        help="Genomic reference sequence name",
    )
    parser.add_argument(
        "--replace",
        action="store_true",
        dest="replace",
        required=False,
        default=False,
        help="Overwrite existing data in database",
    )
    args = parser.parse_args(argv)

    from applogger import setup_logger

    setup_logger()

    genepanel_name = args.genepanelName
    genepanel_version = args.genepanelVersion
    assert genepanel_version.startswith("v")

    db = DB()
    db.connect()

    dg = DepositGenepanel(db.session)
    return dg.add_genepanel(
        args.transcriptsPath,
        args.phenotypesPath,
        genepanel_name,
        genepanel_version,
        genomeRef=args.genomeRef,
        replace=args.replace,
    )


if __name__ == "__main__":
    sys.exit(main())
