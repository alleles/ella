import json
import re
import warnings
from datetime import datetime
from pathlib import Path
from typing import Any

from pydantic import BaseModel, ConfigDict, Field, field_validator

FOLDER_FIELDS_RE = re.compile(
    r"(?P<analysis_name>.+[.-](?P<genepanel_name>.+)[-_](?P<genepanel_version>.+))"
)
VCF_FIELDS_RE = re.compile(FOLDER_FIELDS_RE.pattern + r"\.vcf(?:\.gz)?")


class Data(BaseModel):
    """JSON subschema for data in AnalysisConfigData"""

    model_config = ConfigDict(extra="forbid")  # allow no additional attributes
    vcf: str
    ped: str | None = None
    technology: str = "HTS"
    callerName: str | None = None


class AnalysisConfigData(BaseModel):
    """Implements the JSON schema for analysis config data"""

    model_config = ConfigDict(extra="forbid")  # allow no additional attributes
    name: str
    genepanel_name: str
    genepanel_version: str
    priority: int = Field(1, ge=1, le=3)
    data: list[Data]
    report: str | None = None
    warnings: str | None = None
    date_requested: datetime | None = None

    @field_validator("date_requested", mode="before")
    def date_requested_validator(cls, value: datetime | str | None):
        "Support date strings in the format 'YYYY-MM-DD' as well as datetime objects"
        if isinstance(value, str):
            try:
                return datetime.strptime(value, "%Y-%m-%d")
            except ValueError:
                return value
        return value

    def __init__(self, folder_or_file: Path):
        args = {}
        folder_or_file = folder_or_file.absolute()
        assert folder_or_file.exists()
        root = folder_or_file.parent
        if folder_or_file.is_dir():
            root = folder_or_file
            args.update(self._init_from_folder(folder_or_file))
            args.update(self.__load_file(root / "warnings.txt"))
            args.update(self.__load_file(root / "report.txt"))
        elif folder_or_file.is_file() and is_vcf_file(folder_or_file):
            args.update(self._init_from_vcf(folder_or_file))
        elif folder_or_file.is_file() and folder_or_file.suffix == ".analysis":
            args.update(self._init_from_analysis_file(folder_or_file))
            args.update(self.__load_file(root / "warnings.txt"))
            args.update(self.__load_file(root / "report.txt"))

        self._absolute_filepaths(args, root)
        super().__init__(**args)

    def _absolute_filepaths(self, args: dict[str, Any], root: Path):
        """Make sure all .vcf and .ped filepaths exist and are absolute"""
        for data in args["data"]:
            for key in ["vcf", "ped"]:
                if key in data and data[key] is not None:
                    if not Path(data[key]).is_absolute():
                        data[key] = str(root / data[key])
                    assert Path(data[key]).is_file(), f"Unable to find file at {data[key]}"

    def __load_file(self, filepath: Path):
        if filepath.is_file():
            return {filepath.stem: filepath.read_text()}
        else:
            return {}

    def _init_from_vcf(self, vcf: Path):
        """Initialize from a single vcf file"""
        warnings.warn(
            "This method is considered deprecated and will be removed in the future.",
            DeprecationWarning,
        )
        matches = re.match(VCF_FIELDS_RE, vcf.name)
        assert matches
        return {
            "name": matches.group("analysis_name"),
            "genepanel_name": matches.group("genepanel_name"),
            "genepanel_version": matches.group("genepanel_version"),
            "data": [{"vcf": vcf.as_posix()}],
        }

    def _init_from_analysis_file(self, filepath: Path):
        """Initialize from a .analysis file"""
        with filepath.open() as file:
            analysis_dict = json.load(file)
        return analysis_dict

    def _init_from_folder(self, folder: Path):
        """Initialize from folder containing a .analysis file or .vcf file(s) with optional
        .ped file(s)
        """
        analysis_files = [path.name for path in folder.rglob("*.analysis")]
        assert (
            len(analysis_files) <= 1
        ), f"Multiple .analysis-files found in folder {folder}: {analysis_files}. This is not supported."
        assert len(analysis_files) == 1, f"No .analysis-file found in folder {folder}"
        return self._init_from_analysis_file(folder / analysis_files[0])


def is_vcf_file(path: Path):
    return path.suffix == ".vcf" or path.suffixes[-2:] == [".vcf", ".gz"]
