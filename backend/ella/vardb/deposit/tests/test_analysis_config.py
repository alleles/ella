import json
import shutil
import string
import tempfile
from contextlib import contextmanager
from pathlib import Path
from typing import Any, Literal

import hypothesis as ht
from hypothesis import strategies as st

from ella.vardb.deposit.analysis_config import AnalysisConfigData

WARNING_TEXT = "Some warnings"
REPORT_TEXT = "Some report text"


@contextmanager
def temporary_directory(dirname="analysisconfigdata"):
    tmp_dir = Path(tempfile.gettempdir())
    full_path = tmp_dir / dirname
    full_path.mkdir()
    try:
        yield full_path
    finally:
        shutil.rmtree(full_path)


def create_file(folder: Path, name: str, contents: str | None = None):
    full_path = folder / name
    with full_path.open("w") as f:
        if contents:
            f.write(contents)
    return full_path


@ht.given(
    st.text(string.ascii_letters, min_size=1),
    st.text(string.ascii_letters, min_size=1),
    st.text(string.ascii_letters, min_size=1),
    st.text(string.ascii_letters, min_size=1),
    st.lists(
        st.tuples(
            st.booleans(),
            st.sampled_from([None, "Technology1", "Technology2"]),
            st.sampled_from([None, "Caller1", "Caller2"]),
        ),
        min_size=1,
    ),
    st.one_of(st.just("vcf"), st.just("vcf.gz")),
)
def test_analysis_file(
    filename_stem: str,
    analysis_name: str,
    genepanel_name: str,
    genepanel_version: str,
    data: list[tuple[bool, str | None, str | None]],
    vcf_suffix: Literal["vcf", "vcf.gz"],
):
    analysis_file_contents: dict[str, Any] = {
        "name": analysis_name,
        "genepanel_name": genepanel_name,
        "genepanel_version": genepanel_version,
        "data": [],
    }
    with temporary_directory() as temp_dir:
        for i, (has_ped, technology, caller) in enumerate(data):
            entry: dict[str, Any] = {"vcf": f"vcf_file{i}.{vcf_suffix}"}
            create_file(temp_dir, f"vcf_file{i}.{vcf_suffix}")
            if has_ped:
                create_file(temp_dir, f"ped_file{i}.ped")
                entry["ped"] = f"ped_file{i}.ped"

            if technology is not None:
                entry["technology"] = technology

            entry["callerName"] = caller

            analysis_file_contents["data"].append(entry)

        analysis_file = create_file(
            temp_dir, filename_stem + ".analysis", json.dumps(analysis_file_contents, indent=4)
        )

        analysis_config_data_file = AnalysisConfigData(analysis_file)
        analysis_config_data_dir = AnalysisConfigData(temp_dir)

    expected = dict(analysis_file_contents)
    expected.setdefault("priority", 1)
    expected.setdefault("warnings", None)
    expected.setdefault("report", None)
    expected.setdefault("date_requested", None)
    for entry in expected["data"]:
        entry["vcf"] = str(temp_dir / entry["vcf"])
        if "ped" in entry:
            entry["ped"] = str(temp_dir / entry["ped"])
        else:
            entry["ped"] = None
        entry.setdefault("technology", "HTS")

    assert analysis_config_data_file.model_dump() == expected
    assert analysis_config_data_dir.model_dump() == expected
