from collections.abc import Sequence
from enum import Enum

from ella.vardb.deposit.annotationconverters.annotationconverter import (
    AnnotationConverter,
    ConverterArgs,
    TypeConverter,
)
from ella.vardb.deposit.annotationconverters.clinvarjsonconverter import CLINVARJSONConverter
from ella.vardb.deposit.annotationconverters.hgmdconverter import HGMDConverter
from ella.vardb.deposit.annotationconverters.jsonconverter import JSONConverter
from ella.vardb.deposit.annotationconverters.keyvalueconverter import KeyValueConverter
from ella.vardb.deposit.annotationconverters.mappingconverter import MappingConverter
from ella.vardb.deposit.annotationconverters.metaconverter import MetaConverter
from ella.vardb.deposit.annotationconverters.referenceconverters import (
    ClinVarReferencesConverter,
    HGMDExtraRefsConverter,
    HGMDPrimaryReportConverter,
)
from ella.vardb.deposit.annotationconverters.vepconverter import VEPConverter
from ella.vardb.util.vcfrecord import Primitives


class AnnotationConverters(Enum):
    # Generic converters
    json = JSONConverter
    keyvalue = KeyValueConverter
    meta = MetaConverter
    mapping = MappingConverter
    # Specific converters
    vep = VEPConverter
    clinvarjson = CLINVARJSONConverter
    hgmd = HGMDConverter
    hgmdextrarefs = HGMDExtraRefsConverter
    hgmdprimaryreport = HGMDPrimaryReportConverter
    clinvarreferences = ClinVarReferencesConverter

    @classmethod
    def keys(cls) -> list[str]:
        return [ac.name for ac in cls]

    @classmethod
    def values(cls) -> Sequence[AnnotationConverter]:
        return [ac.value for ac in cls]
