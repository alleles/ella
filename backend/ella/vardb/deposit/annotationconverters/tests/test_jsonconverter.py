import base64
import json
from typing import Any

import pytest

from conftest import converter_builder
from ella.vardb.deposit.annotationconverters import ConverterArgs


@pytest.mark.parametrize("encoding", ["base16", "base32", "base64"])
def test_jsonconverter(encoding):
    if encoding == "base16":
        encoder = base64.b16encode
    elif encoding == "base32":
        encoder = base64.b32encode
    elif encoding == "base64":
        encoder = base64.b64encode

    data_dict: dict[str, Any] = {
        "a1": {"b1": {"c": [1, 2, 3]}, "b2": {"d": "dabla"}},
        "a2": ["foo", "bar", "baz"],
    }
    data_encoded = encoder(json.dumps(data_dict).encode())
    converter = converter_builder.get_converter("json", encoding=encoding)

    processed = converter(ConverterArgs(data_encoded))
    assert processed == data_dict

    subpaths = {
        "a1": data_dict["a1"],
        "a2": data_dict["a2"],
        "a1.b1": data_dict["a1"]["b1"],
        "a1.b2": data_dict["a1"]["b2"],
        "a1.b1.c": data_dict["a1"]["b1"]["c"],
        "a1.b2.d": data_dict["a1"]["b2"]["d"],
        "doesnotexist": None,
        "a1.b1.doesnotexist": None,
    }

    for subpath, expected_result in subpaths.items():
        converter = converter_builder.get_converter("json", encoding=encoding, subpath=subpath)
        processed = converter(ConverterArgs(data_encoded))
        assert processed == expected_result, f"{subpath}"
