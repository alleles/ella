from typing import TYPE_CHECKING

import pytest

from conftest import converter_builder
from ella.vardb.deposit.annotationconverters import ConverterArgs

if TYPE_CHECKING:
    from ella.vardb.util.vcfrecord import Primitives


def test_keyvalueconverter_identity():
    converter = converter_builder.get_converter("keyvalue")
    examples: list[Primitives | None] = [1, "1", None, True]
    for value in examples:
        processed = converter(ConverterArgs(value))
        assert processed == value, str(value)


def test_keyvalueconverter_int():
    converter = converter_builder.get_converter("keyvalue", target_type="int")
    valid_examples: list[tuple[Primitives, int]] = [(1, 1), ("1", 1), (True, 1), (False, 0)]
    for valid_value, expected in valid_examples:
        processed = converter(ConverterArgs(valid_value))
        assert processed == expected, str(valid_value)

    invalid_examples: list[Primitives | None] = ["1.0", None, "dabla"]
    for invalid_value in invalid_examples:
        with pytest.raises(ValueError):
            converter(ConverterArgs(invalid_value))


def test_keyvalueconverter_float():
    converter = converter_builder.get_converter("keyvalue", target_type="float")
    valid_examples: list[tuple[Primitives, float]] = [
        (1, 1.0),
        ("1", 1.0),
        ("1.0", 1.0),
        (True, 1.0),
    ]
    for valid_value, expected in valid_examples:
        processed = converter(ConverterArgs(valid_value))
        assert processed == expected, str(valid_value)

    invalid_examples: list[Primitives | None] = [None, "dabla"]
    for invalid_value in invalid_examples:
        with pytest.raises(ValueError):
            converter(ConverterArgs(invalid_value))


def test_keyvalueconverter_bool():
    converter = converter_builder.get_converter("keyvalue", target_type="bool")
    true_examples: list[Primitives | list[int]] = [
        True,
        1,
        2,
        0.1,
        "1",
        "True",
        "true",
        "t",
        "y",
        "T",
        "Y",
        "yes",
        "on",
        [0],
    ]
    for true_value in true_examples:
        processed = converter(ConverterArgs(true_value))
        assert processed is True, str(true_value)

    false_examples: list[Primitives | None | list] = [
        False,
        None,
        0,
        0.0,
        "0",
        "False",
        "false",
        "f",
        "F",
        "N",
        "n",
        "no",
        "off",
        [],
    ]
    for false_value in false_examples:
        processed = converter(ConverterArgs(false_value))
        assert processed is False, str(false_value)

    invalid_examples: list[str] = ["dabla"]
    for invalid_value in invalid_examples:
        with pytest.raises(ValueError):
            converter(ConverterArgs(invalid_value))


def test_keyvalueconverter_string():
    converter = converter_builder.get_converter("keyvalue", target_type="string")
    examples: list[Primitives] = [1, "1"]
    for value in examples:
        processed = converter(ConverterArgs(value))
        assert processed == str(value), str(value)


def test_keyvalueconverter_split():
    converter = converter_builder.get_converter("keyvalue", split="|")
    processed = converter(ConverterArgs("1|2|3"))
    assert processed == ["1", "2", "3"]

    converter = converter_builder.get_converter("keyvalue", target_type="int", split="|")
    processed = converter(ConverterArgs("1|2|3"))
    assert processed == [1, 2, 3]

    with pytest.raises(ValueError):
        processed = converter(ConverterArgs("1|2|dabla"))

    converter = converter_builder.get_converter(
        "keyvalue", target_type="int", target_type_throw=False, split="|"
    )
    processed = converter(ConverterArgs("1|2|dabla"))
    assert processed is None
