from __future__ import annotations

from dataclasses import dataclass, field
from enum import Enum, auto
from typing import TYPE_CHECKING, Any

if TYPE_CHECKING:
    from collections.abc import Mapping, Sequence

    from ella.vardb.util.vcfrecord import Primitives


class TypeConverter(Enum):
    int = auto()
    float = auto()
    str = auto()
    bool = auto()
    identity = auto()
    # use string as alias for str so can use builtin func directly
    # i.e., TypeConverter.string.name == "str"
    string = str

    def __call__(self, val: Any) -> Primitives:
        if self is TypeConverter.identity:
            return val
        elif self is TypeConverter.bool:
            from ella.api.util.util import strtobool  # Avoid circular import

            return bool(strtobool(val) if isinstance(val, str) else val)
        else:
            if isinstance(val, str):
                # eval f-string needs quotes so value of val not interpreted as variable name
                return eval(f"{self.name}('{val}')")
            else:
                return eval(f"{self.name}({val})")


@dataclass(frozen=True)
class ConverterArgs:
    value: Primitives | Sequence[Primitives] | None
    additional_values: Mapping[str, Any] | None = None


class AnnotationConverter:
    meta: Mapping[str, str] | None
    config: Config

    @dataclass(frozen=True, init=False)
    class Config:
        source: str
        target: str
        target_mode: str = "insert"
        additional_sources: list[str] = field(default_factory=list)

        def __init__(self, *args, **kwargs) -> None:
            raise NotImplementedError("Config class must be implemented in subclasss")

    def __init__(self, config: Config, meta: Mapping[str, str] | None = None):
        self.config = config
        self.meta = meta

    def setup(self):
        "Code here will be executed before first __call__"
        pass

    def __call__(self, args: ConverterArgs):
        raise NotImplementedError("Must be implemented in subclass")
