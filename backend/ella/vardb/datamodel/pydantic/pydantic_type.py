import json
from types import UnionType
from typing import Any, get_args, override

from pydantic import BaseModel
from sqlalchemy import Null
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.engine.interfaces import Dialect
from sqlalchemy.types import TypeDecorator


class PydanticType(TypeDecorator):
    """Pydantic-specific column type for JSONB columns.

    Adapted from
    https://roman.pt/posts/pydantic-in-sqlalchemy-fields/

    SAVING:
    - Uses SQLAlchemy JSONB type under the hood.
    - Converts Pydantic model to dict on save.
    - SQLAlchemy engine JSONB-encodes dict to string.

    RETRIEVING:
    - Pulls string from database.
    - SQLAlchemy engine JSONB-decodes string to dict.
    - Converts dict to Pydantic model.
    """

    impl = JSONB
    cache_ok = True

    def __init__(self, pydantic_model: type[BaseModel] | UnionType, default_factory=None, **kwargs):
        super().__init__(**kwargs)
        self.pydantic_model = pydantic_model
        self.default_factory = default_factory or Null

    @override
    def load_dialect_impl(self, dialect: Dialect):
        assert dialect.name == "postgresql"
        return dialect.type_descriptor(JSONB())

    @override
    def process_bind_param(self, value: Any | None, dialect: Dialect):
        return value if value else self.default_factory()

    @override
    def process_result_value(self, value: Any | None, dialect: Dialect) -> Any | None:
        if value is None:
            return None

        if type(self.pydantic_model) is not UnionType:
            return self.pydantic_model(**value)
        else:
            for pydantic_model in get_args(self.pydantic_model):
                try:
                    return pydantic_model(**value)
                except Exception:
                    continue
            else:
                raise ValueError(f"Could not convert {value} to {self.pydantic_model}")


class CustomPydanticJSONEncoder(json.JSONEncoder):
    """Custom JSON encoder for Pydantic models.

    Adapted from
    https://github.com/pydantic/pydantic/discussions/6652#discussioncomment-6483693
    """

    @override
    def default(self, obj: Any) -> Any:
        if isinstance(obj, BaseModel):
            return obj.model_dump(mode="json", exclude_defaults=True)
        return super().default(obj)


def json_serializer(*args, **kwargs):
    """Custom JSON serializer"""
    return json.dumps(*args, cls=CustomPydanticJSONEncoder, **kwargs)
