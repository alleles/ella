from __future__ import annotations

from enum import Enum
from typing import Annotated, Any, Literal

from pydantic import (
    BaseModel,
    ConfigDict,
    Field,
    NonNegativeFloat,
    NonNegativeInt,
    NonPositiveInt,
    ValidationInfo,
    field_validator,
    model_validator,
)

OperatorType = Literal[">", "<", ">=", "<=", "=="]


class NoExtraBaseModel(BaseModel):
    model_config = ConfigDict(extra="forbid")


class FilterConfigModel(NoExtraBaseModel):
    """Pydantic model forFilterType filterconfig."""

    filters: list[FilterType]


class Filter(NoExtraBaseModel):
    name: str
    config: ConfigType
    exceptions: list[FilterType] | None = None

    @field_validator("exceptions", mode="before")
    @classmethod
    def validate_exceptions(cls, exceptions: list[dict[str, Any]] | None):
        if exceptions:
            for exception in exceptions:
                if (hasattr(exception, "get") and exception.get("exceptions")) or getattr(
                    exception, "exception", None
                ):
                    raise ValueError("Exceptions cannot contain further exceptions.")
        return exceptions


class CallertypeModel(Filter):
    class CallertypeConfig(NoExtraBaseModel):
        caller_types: list[Literal["cnv", "snv"]]

    name: Literal["callertype"]
    config: CallertypeConfig


class SizeModel(Filter):
    class SizeConfig(NoExtraBaseModel):
        threshold: NonNegativeInt
        mode: OperatorType

    name: Literal["size"]
    config: SizeConfig


class GeneModel(Filter):
    class GeneConfig(NoExtraBaseModel):
        genes: list[int]
        mode: Literal["one", "all"] | None = None
        inverse: bool | None = None

    name: Literal["gene"]
    config: GeneConfig


class SegregationModel(Filter):
    class SegregationConfig(NoExtraBaseModel):
        class EnableBase(NoExtraBaseModel):
            enable: bool = True

        class Denovo(EnableBase):
            class GqThreshold(NoExtraBaseModel):
                proband: NonNegativeInt
                mother: NonNegativeInt
                father: NonNegativeInt

            gq_threshold: GqThreshold | None = None

        no_coverage_parents: EnableBase = Field(default_factory=EnableBase)
        denovo: Denovo = Field(default_factory=Denovo)
        parental_mosaicism: EnableBase = Field(default_factory=EnableBase)
        compound_heterozygous: EnableBase = Field(default_factory=EnableBase)
        recessive_homozygous: EnableBase = Field(default_factory=EnableBase)

    name: Literal["segregation"]
    config: SegregationConfig = Field(default_factory=SegregationConfig)


class QualityModel(Filter):
    class QualityConfig(NoExtraBaseModel):
        class FilterStatus(NoExtraBaseModel):
            pattern: str
            inverse: bool | None = None
            filter_empty: bool | None = None

        quality: NonNegativeFloat | None = None
        allele_ratio: Annotated[float, Field(ge=0.0, le=1.0)] | None = None
        filter_status: FilterStatus | None = None

        @model_validator(mode="after")
        def validate_defined(self):
            if all(
                [
                    self.quality is None,
                    self.filter_status is None,
                    self.allele_ratio is None,
                ]
            ):
                raise ValueError(
                    "At least one of quality, allele_ratio or filter_status must be set"
                )
            return self

    name: Literal["quality"]
    config: QualityConfig


class ConsequenceModel(Filter):
    class ConsequenceConfig(NoExtraBaseModel):
        class Consequence(Enum):
            transcript_ablation = "transcript_ablation"
            splice_donor_variant = "splice_donor_variant"
            splice_acceptor_variant = "splice_acceptor_variant"
            stop_gained = "stop_gained"
            frameshift_variant = "frameshift_variant"
            start_lost = "start_lost"
            initiator_codon_variant = "initiator_codon_variant"
            stop_lost = "stop_lost"
            inframe_insertion = "inframe_insertion"
            inframe_deletion = "inframe_deletion"
            missense_variant = "missense_variant"
            protein_altering_variant = "protein_altering_variant"
            transcript_amplification = "transcript_amplification"
            splice_region_variant = "splice_region_variant"
            splice_donor_5th_base_variant = "splice_donor_5th_base_variant"
            splice_donor_region_variant = "splice_donor_region_variant"
            splice_polypyrimidine_tract_variant = "splice_polypyrimidine_tract_variant"
            incomplete_terminal_codon_variant = "incomplete_terminal_codon_variant"
            synonymous_variant = "synonymous_variant"
            start_retained_variant = "start_retained_variant"
            stop_retained_variant = "stop_retained_variant"
            coding_sequence_variant = "coding_sequence_variant"
            mature_miRNA_variant = "mature_miRNA_variant"
            field_5_prime_UTR_variant = "5_prime_UTR_variant"
            field_3_prime_UTR_variant = "3_prime_UTR_variant"
            non_coding_transcript_exon_variant = "non_coding_transcript_exon_variant"
            non_coding_transcript_variant = "non_coding_transcript_variant"
            intron_variant = "intron_variant"
            NMD_transcript_variant = "NMD_transcript_variant"
            upstream_gene_variant = "upstream_gene_variant"
            downstream_gene_variant = "downstream_gene_variant"
            TFBS_ablation = "TFBS_ablation"
            TFBS_amplification = "TFBS_amplification"
            TF_binding_site_variant = "TF_binding_site_variant"
            regulatory_region_variant = "regulatory_region_variant"
            regulatory_region_ablation = "regulatory_region_ablation"
            regulatory_region_amplification = "regulatory_region_amplification"
            feature_elongation = "feature_elongation"
            feature_truncation = "feature_truncation"
            intergenic_variant = "intergenic_variant"

        consequences: set[Consequence]
        genepanel_only: bool | None = None

    name: Literal["consequence"]
    config: ConsequenceConfig


class InheritanceModel(Filter):
    class InheritanceConfig(NoExtraBaseModel):
        filter_mode: Literal["recessive_non_candidates", "recessive_candidates"]

    name: Literal["inheritancemodel"]
    config: InheritanceConfig


class PolyPyrimidineModel(Filter):
    class PolyPyrimidineConfig(NoExtraBaseModel):
        ppy_tract_region: tuple[NonPositiveInt, NonPositiveInt]

    name: Literal["ppy"]
    config: PolyPyrimidineConfig


class NumStars(BaseModel):
    operator: OperatorType
    value: int = Field(ge=0, le=4)


class Combinations(BaseModel):
    left: Literal["pathogenic", "uncertain", "benign"]
    operator: OperatorType
    right: Literal["pathogenic", "uncertain", "benign"] | Annotated[int, Field(ge=0)]


class ExternalModel(Filter):
    class ExternalConfig(NoExtraBaseModel):
        class HGMD(NoExtraBaseModel):
            tags: list[Literal["FP", "DM", "DFP", "R", "DP", "DM?"] | None]
            inverse: bool | None = None

        class ClinVar(NoExtraBaseModel):
            num_stars: NumStars | None = None
            combinations: list[Combinations] | None = None
            inverse: bool | None = None

            @field_validator("num_stars", mode="before")
            @classmethod
            def validate_num_stars(cls, num_stars: list | tuple | NumStars | dict | None):
                if isinstance(num_stars, tuple | list):
                    return NumStars(operator=num_stars[0], value=num_stars[1])
                return num_stars

            @field_validator("combinations", mode="before")
            @classmethod
            def validate_combinations(cls, combinations: list):
                for combination in combinations:
                    if isinstance(combination, list | tuple):
                        if len(combination) != 3:
                            raise ValueError(
                                "combinations must be a list of lists of the format [[right, operator, left], ...]"
                            )
                        yield Combinations(
                            left=combination[0], operator=combination[1], right=combination[2]
                        )
                    else:
                        yield combination

        hgmd: HGMD | None = None
        clinvar: ClinVar | None = None

    name: Literal["external"]
    config: ExternalConfig


class ClassificationModel(Filter):
    class ClassificationConfig(NoExtraBaseModel):
        classes: set[Literal["1", "2", "3", "4", "5", "DR", "NP", "RF"]]
        exclude_outdated: bool | None = None

    name: Literal["classification"]
    config: ClassificationConfig


class RegionModel(Filter):
    class RegionConfig(NoExtraBaseModel):
        splice_region: tuple[int, int] | None = None
        utr_region: tuple[int, int] | None = None

        @field_validator("splice_region", "utr_region", mode="before")
        @classmethod
        def validate_splice_region(cls, region: list[int] | None, info: ValidationInfo):
            if not region or region[0] <= 0 and region[1] >= 0:
                return region
            raise ValueError(
                f"{info.field_name} must be a list with format [int_a, int_b] where int_a <= 0 and int_b >= 0"
            )

    name: Literal["region"]
    config: RegionConfig


class FreqThresholds(NoExtraBaseModel):
    hi_freq_cutoff: Annotated[float, Field(ge=0.0, le=1.0)] | None = None
    lo_freq_cutoff: Annotated[float, Field(ge=0.0, le=1.0)] | None = None


class BaseThresholds(NoExtraBaseModel):
    external: FreqThresholds | None = None
    internal: FreqThresholds | None = None

    @field_validator("*", mode="before")
    def validate_thresholds(cls, thresholds):
        "If given as simple form (e.g 0.005), convert it to complex form {'hi_freq_cutoff': 0.005}"
        if thresholds and not isinstance(thresholds, dict):
            return {"hi_freq_cutoff": thresholds}
        return thresholds


NumThresholds = dict[
    str,
    dict[
        str,
        NonNegativeInt,
    ],
]


class FrequencyModel(Filter):
    class FrequencyConfig(NoExtraBaseModel):
        class Genes(NoExtraBaseModel):
            thresholds: BaseThresholds

        class Groups(NoExtraBaseModel):
            external: dict[str, list[str]]
            internal: dict[str, list[str]]

        class Thresholds(NoExtraBaseModel):
            AD: BaseThresholds
            default: BaseThresholds

        genes: dict[int, Genes] | None = None
        num_thresholds: NumThresholds
        groups: Groups
        thresholds: Thresholds

    name: Literal["frequency"]
    config: FrequencyConfig


class GenericAnnotationModel(Filter):
    class GenericAnnotationConfig(NoExtraBaseModel):
        class Rule(NoExtraBaseModel):
            key: str
            operator: Literal[
                "is",
                "is_not",
                "==",
                "!=",
                ">",
                ">=",
                "<",
                "<=",
                "in",
                "not_in",
                "contains",
                "not_contains",
                "str_contains",
                "str_not_contains",
                "overlap",
                "not_overlap",
            ]
            value: bool | float | int | str | None | list[bool | float | int | str]
            type_str: Literal["string", "number", "boolean"]

        target: str = Field(pattern=r"^[a-zA-Z0-9]+(\.[a-zA-Z0-9]+)*$")
        is_array: bool
        rules: list[Rule]
        mode: Literal["all", "any"] = "all"

    name: Literal["genericannotation"]
    config: GenericAnnotationConfig


ConfigType = (
    FrequencyModel.FrequencyConfig
    | RegionModel.RegionConfig
    | ClassificationModel.ClassificationConfig
    | ExternalModel.ExternalConfig
    | PolyPyrimidineModel.PolyPyrimidineConfig
    | InheritanceModel.InheritanceConfig
    | ConsequenceModel.ConsequenceConfig
    | QualityModel.QualityConfig
    | SegregationModel.SegregationConfig
    | GeneModel.GeneConfig
    | SizeModel.SizeConfig
    | CallertypeModel.CallertypeConfig
    | GenericAnnotationModel.GenericAnnotationConfig
)

FilterType = Annotated[
    FrequencyModel
    | RegionModel
    | ClassificationModel
    | GenericAnnotationModel
    | ExternalModel
    | PolyPyrimidineModel
    | InheritanceModel
    | ConsequenceModel
    | QualityModel
    | SegregationModel
    | GeneModel
    | SizeModel
    | CallertypeModel,
    Field(discriminator="name"),
]
