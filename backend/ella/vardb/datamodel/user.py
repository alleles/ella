from __future__ import annotations

from datetime import datetime
from typing import TYPE_CHECKING

import pytz
from sqlalchemy import DateTime, ForeignKey, UniqueConstraint
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import Mapped, mapped_column, relationship
from sqlalchemy.schema import ForeignKeyConstraint

from ella.vardb.datamodel import Base
from ella.vardb.util.mutjson import JSONMutableDict

if TYPE_CHECKING:
    from ella.vardb.datamodel.gene import Genepanel


class UserGroupGenepanel(Base):
    """Association table between UserGroup and Genepanel for many-to-many relationship"""

    __tablename__ = "usergroupgenepanel"

    usergroup_id: Mapped[int | None] = mapped_column(ForeignKey("usergroup.id"), primary_key=True)
    genepanel_name: Mapped[str] = mapped_column(primary_key=True)
    genepanel_version: Mapped[str] = mapped_column(primary_key=True)

    __table_args__ = (
        ForeignKeyConstraint(
            ["genepanel_name", "genepanel_version"], ["genepanel.name", "genepanel.version"]
        ),
        UniqueConstraint("usergroup_id", "genepanel_name", "genepanel_version"),
    )


class User(Base):
    """Represents a user."""

    __tablename__ = "user"

    id: Mapped[int] = mapped_column(primary_key=True)
    username: Mapped[str] = mapped_column(unique=True)
    first_name: Mapped[str]
    last_name: Mapped[str]
    email: Mapped[str | None]
    group_id: Mapped[int] = mapped_column(ForeignKey("usergroup.id"))
    group: Mapped[UserGroup] = relationship(back_populates="users")
    password: Mapped[str]
    password_expiry: Mapped[datetime] = mapped_column(DateTime(timezone=True))
    active: Mapped[bool] = mapped_column(default=True)
    incorrect_logins: Mapped[int] = mapped_column(default=0)
    config: Mapped[dict | None] = mapped_column(JSONMutableDict.as_mutable(JSONB), default={})


class UserGroup(Base):
    __tablename__ = "usergroup"

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(unique=True)
    genepanels: Mapped[list[Genepanel]] = relationship(secondary=UserGroupGenepanel.__table__)
    config: Mapped[dict | None] = mapped_column(JSONMutableDict.as_mutable(JSONB), default={})
    # keep both directives w/o params, needed for composite foreign keys
    default_import_genepanel_name: Mapped[str | None] = mapped_column()
    default_import_genepanel_version: Mapped[str | None] = mapped_column()
    default_import_genepanel: Mapped[Genepanel | None] = relationship()
    users: Mapped[list[User]] = relationship(back_populates="group")

    __table_args__ = (
        ForeignKeyConstraint(
            [default_import_genepanel_name, default_import_genepanel_version],
            ["genepanel.name", "genepanel.version"],
        ),
    )


class UserGroupImport(Base):
    """Represents relationship of what groups are allowed to import data into which groups"""

    __tablename__ = "usergroupimport"

    usergroup_id: Mapped[int] = mapped_column(ForeignKey("usergroup.id"), primary_key=True)
    usergroupimport_id: Mapped[int] = mapped_column(ForeignKey("usergroup.id"), primary_key=True)

    __table_args__ = (UniqueConstraint("usergroup_id", "usergroupimport_id"),)


class UserSession(Base):
    """Represents a user session"""

    __tablename__ = "usersession"

    id: Mapped[int] = mapped_column(primary_key=True)
    user_id: Mapped[int | None] = mapped_column(ForeignKey("user.id"))  # TODO: non-nullable?
    user: Mapped[User | None] = relationship()
    token: Mapped[str] = mapped_column(unique=True)
    issued: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=lambda: datetime.now(pytz.utc)
    )
    lastactivity: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=lambda: datetime.now(pytz.utc)
    )
    expires: Mapped[datetime] = mapped_column(DateTime(timezone=True))
    logged_out: Mapped[datetime | None] = mapped_column(DateTime(timezone=True))


class UserOldPassword(Base):
    __tablename__ = "useroldpassword"
    id: Mapped[int] = mapped_column(primary_key=True)
    user_id: Mapped[int | None] = mapped_column(ForeignKey("user.id"))
    password: Mapped[str]
    expired: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=lambda: datetime.now(pytz.utc)
    )
