from __future__ import annotations

from datetime import datetime
from typing import TYPE_CHECKING

import pytz
from sqlalchemy import BigInteger, DateTime, ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from ella.vardb.datamodel import Base

if TYPE_CHECKING:
    from ella.vardb.datamodel.user import User


class Attachment(Base):
    __tablename__ = "attachment"

    id: Mapped[int] = mapped_column(primary_key=True)
    sha256: Mapped[str | None]
    filename: Mapped[str]
    size: Mapped[int | None] = mapped_column(BigInteger)
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=lambda: datetime.now(pytz.utc)
    )
    mimetype: Mapped[str | None]
    extension: Mapped[str | None]
    user_id: Mapped[int] = mapped_column(ForeignKey("user.id"))
    user: Mapped[User] = relationship()
