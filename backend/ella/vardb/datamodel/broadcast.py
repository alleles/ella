from datetime import datetime

import pytz
from sqlalchemy import DateTime
from sqlalchemy.orm import Mapped, mapped_column

from ella.vardb.datamodel import Base


class Broadcast(Base):
    """Broadcast message"""

    __tablename__ = "broadcast"

    id: Mapped[int] = mapped_column(primary_key=True)
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=lambda: datetime.now(pytz.utc)
    )
    message: Mapped[str]
    active: Mapped[bool]
