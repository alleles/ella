"""vardb datamodel Assessment class"""
from __future__ import annotations

from datetime import datetime
from typing import TYPE_CHECKING, override

import pytz
from sqlalchemy import DateTime, Enum, ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship
from sqlalchemy.schema import ForeignKeyConstraint, Index
from sqlalchemy_searchable import SearchQueryMixin
from sqlalchemy_utils.types import TSVectorType

from ella.api.schemas.pydantic.v1.allele_assessments import AlleleAssessmentEvaluation
from ella.api.schemas.pydantic.v1.allele_reports import AlleleReportEvaluation
from ella.api.schemas.pydantic.v1.gene_assessments import GeneAssessmentEvaluation
from ella.api.schemas.pydantic.v1.references import ReferenceEvaluation
from ella.vardb.datamodel import Base
from ella.vardb.datamodel.pydantic.pydantic_type import PydanticType

if TYPE_CHECKING:
    from ella.vardb.datamodel.allele import Allele
    from ella.vardb.datamodel.annotation import Annotation, CustomAnnotation
    from ella.vardb.datamodel.attachment import Attachment
    from ella.vardb.datamodel.gene import Gene, Genepanel
    from ella.vardb.datamodel.user import User, UserGroup


class AlleleAssessmentReferenceAssessment(Base):
    """Association table between AlleleAssessment and ReferenceAssessment for many-to-many relationship"""

    __tablename__ = "alleleassessmentreferenceassessment"

    alleleassessment_id: Mapped[int | None] = mapped_column(
        ForeignKey("alleleassessment.id"), primary_key=True
    )
    referenceassessment_id: Mapped[int | None] = mapped_column(
        ForeignKey("referenceassessment.id"), primary_key=True
    )


class AlleleAssessmentAttachment(Base):
    """Association table between AlleleAssessment and Attachment for many-to-many relationship"""

    __tablename__ = "alleleassessmentattachment"

    alleleassessment_id: Mapped[int | None] = mapped_column(
        ForeignKey("alleleassessment.id"), primary_key=True
    )
    attachment_id: Mapped[int | None] = mapped_column(ForeignKey("attachment.id"), primary_key=True)


class AlleleAssessment(Base):
    """Represents an assessment of one allele."""

    __tablename__ = "alleleassessment"

    id: Mapped[int] = mapped_column(primary_key=True)
    classification: Mapped[str] = mapped_column(
        Enum("1", "2", "3", "4", "5", "NP", "RF", "DR", name="alleleassessment_classification")
    )
    evaluation: Mapped[AlleleAssessmentEvaluation] = mapped_column(
        PydanticType(AlleleAssessmentEvaluation)
    )
    user_id: Mapped[int] = mapped_column(ForeignKey("user.id"))
    user: Mapped[User] = relationship()
    usergroup_id: Mapped[int | None] = mapped_column(ForeignKey("usergroup.id"))
    usergroup: Mapped[UserGroup | None] = relationship()
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=lambda: datetime.now(pytz.utc)
    )
    date_superceeded: Mapped[datetime | None] = mapped_column(DateTime(timezone=True))
    previous_assessment_id: Mapped[int | None] = mapped_column(ForeignKey("alleleassessment.id"))
    previous_assessment: Mapped[AlleleAssessment | None] = relationship()
    allele_id: Mapped[int] = mapped_column(ForeignKey("allele.id"))
    allele: Mapped[Allele] = relationship(back_populates="assessments")
    # keep both directives w/o params, needed for composite foreign keys
    genepanel_name: Mapped[str] = mapped_column()
    genepanel_version: Mapped[str] = mapped_column()
    genepanel: Mapped[Genepanel] = relationship()
    analysis_id: Mapped[int | None] = mapped_column(ForeignKey("analysis.id", ondelete="SET NULL"))
    annotation_id: Mapped[int | None] = mapped_column(ForeignKey("annotation.id"))
    annotation: Mapped[Annotation | None] = relationship()
    custom_annotation_id: Mapped[int | None] = mapped_column(ForeignKey("customannotation.id"))
    custom_annotation: Mapped[CustomAnnotation | None] = relationship()

    referenceassessments: Mapped[list[ReferenceAssessment]] = relationship(
        secondary=AlleleAssessmentReferenceAssessment.__table__
    )
    attachments: Mapped[list[Attachment]] = relationship(
        secondary=AlleleAssessmentAttachment.__table__
    )

    __table_args__ = (
        Index(
            "ix_assessment_alleleid_unique",
            "allele_id",
            postgresql_where=(date_superceeded.is_(None)),
            unique=True,
        ),
        ForeignKeyConstraint(
            [genepanel_name, genepanel_version], ["genepanel.name", "genepanel.version"]
        ),
    )

    @override
    def __repr__(self):
        return f"<AlleleAssessment('{self.id}','{self.allele_id}', '{self.classification}')>"

    @override
    def __str__(self):
        return f"{self.classification}, {self.date_created}"


class ReferenceAssessment(Base):
    """Association object between assessments and references.

    Note that Assessment uses an association proxy;
    usage of AssessmentReference can therefore be sidestepped
    if it is not necessary to change values to the extra attributes in this class.
    """

    __tablename__ = "referenceassessment"

    id: Mapped[int] = mapped_column(primary_key=True)
    reference_id: Mapped[int] = mapped_column(ForeignKey("reference.id"))
    reference: Mapped[Reference] = relationship()
    evaluation: Mapped[ReferenceEvaluation] = mapped_column(PydanticType(ReferenceEvaluation))
    user_id: Mapped[int] = mapped_column(ForeignKey("user.id"))
    user: Mapped[User] = relationship()
    usergroup_id: Mapped[int | None] = mapped_column(ForeignKey("usergroup.id"))
    usergroup: Mapped[UserGroup | None] = relationship()
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=lambda: datetime.now(pytz.utc)
    )
    date_superceeded: Mapped[datetime | None] = mapped_column(DateTime(timezone=True))
    # keep both directives w/o params, needed for composite foreign keys
    genepanel_name: Mapped[str] = mapped_column()
    genepanel_version: Mapped[str] = mapped_column()
    genepanel: Mapped[Genepanel] = relationship()
    allele_id: Mapped[int] = mapped_column(ForeignKey("allele.id"))
    allele: Mapped[Allele] = relationship()
    previous_assessment_id: Mapped[int | None] = mapped_column(ForeignKey("referenceassessment.id"))
    previous_assessment: Mapped[ReferenceAssessment | None] = relationship()
    analysis_id: Mapped[int | None] = mapped_column(ForeignKey("analysis.id", ondelete="SET NULL"))

    __table_args__ = (
        Index(
            "ix_referenceassessment_alleleid_referenceid_unique",
            "allele_id",
            "reference_id",
            postgresql_where=(date_superceeded.is_(None)),
            unique=True,
        ),
        ForeignKeyConstraint(
            [genepanel_name, genepanel_version], ["genepanel.name", "genepanel.version"]
        ),
    )

    @override
    def __str__(self):
        return f"{str(self.user)}, {self.reference}, {self.evaluation}"


class Reference(Base, SearchQueryMixin):
    """Represents a reference that brings information to this assessment."""

    __tablename__ = "reference"

    id: Mapped[int] = mapped_column(primary_key=True)
    authors: Mapped[str | None]
    title: Mapped[str | None]
    journal: Mapped[str | None]
    abstract: Mapped[str | None]
    year: Mapped[str | None]
    pubmed_id: Mapped[int | None] = mapped_column(unique=True)
    published: Mapped[bool] = mapped_column(default=True)
    attachment_id: Mapped[int | None] = mapped_column(ForeignKey("attachment.id"))
    attachment: Mapped[Attachment | None] = relationship()

    search = mapped_column(
        TSVectorType(
            "authors",
            "title",
            "journal",
            "year",
            weights={"authors": "A", "title": "A", "journal": "B", "year": "C"},
        )
    )

    __table_args__ = (Index("ix_pubmedid", "pubmed_id", unique=True),)

    @override
    def __repr__(self):
        return f"<Reference('{self.authors}','{self.title}', '{self.year}')>"

    @override
    def __str__(self):
        return f"{self.authors} ({self.year}) {self.journal}"

    def citation(self):
        return f"{self.authors} ({self.year}) {self.title} {self.journal}"


class AlleleReport(Base):
    """Represents a report for one allele. The report is aimed at the
    clinicians as compared to alleleassessment which is aimed at fellow
    interpreters. The report might not change as often as the alleleassessment."""

    __tablename__ = "allelereport"

    id: Mapped[int] = mapped_column(primary_key=True)
    evaluation: Mapped[AlleleReportEvaluation] = mapped_column(PydanticType(AlleleReportEvaluation))
    user_id: Mapped[int] = mapped_column(ForeignKey("user.id"))
    user: Mapped[User] = relationship()
    usergroup_id: Mapped[int | None] = mapped_column(ForeignKey("usergroup.id"))
    usergroup: Mapped[UserGroup | None] = relationship()
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=lambda: datetime.now(pytz.utc)
    )
    date_superceeded: Mapped[datetime | None] = mapped_column(DateTime(timezone=True))
    previous_report_id: Mapped[int | None] = mapped_column(ForeignKey("allelereport.id"))
    previous_report: Mapped[AlleleReport | None] = relationship()
    allele_id: Mapped[int] = mapped_column(ForeignKey("allele.id"))
    allele: Mapped[Allele] = relationship(back_populates="reports")
    analysis_id: Mapped[int | None] = mapped_column(ForeignKey("analysis.id", ondelete="SET NULL"))
    alleleassessment_id: Mapped[int | None] = mapped_column(ForeignKey("alleleassessment.id"))
    alleleassessment: Mapped[AlleleAssessment] = relationship()

    __table_args__ = (
        Index(
            "ix_allelereport_alleleid_unique",
            "allele_id",
            postgresql_where=(date_superceeded.is_(None)),
            unique=True,
        ),
    )

    @override
    def __repr__(self):
        return f"<AlleleReport('{self.id}','{self.allele_id}', '{str(self.user)}')>"


class GeneAssessment(Base):
    """Represents an assessment for a single gene."""

    __tablename__ = "geneassessment"

    id: Mapped[int] = mapped_column(primary_key=True)
    evaluation: Mapped[GeneAssessmentEvaluation] = mapped_column(
        PydanticType(GeneAssessmentEvaluation)
    )
    user_id: Mapped[int] = mapped_column(ForeignKey("user.id"))
    user: Mapped[User] = relationship()
    usergroup_id: Mapped[int | None] = mapped_column(ForeignKey("usergroup.id"))
    usergroup: Mapped[UserGroup | None] = relationship()
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=lambda: datetime.now(pytz.utc)
    )
    genepanel_name: Mapped[str]
    genepanel_version: Mapped[str]
    date_superceeded: Mapped[datetime | None] = mapped_column(DateTime(timezone=True))
    previous_assessment_id: Mapped[int | None] = mapped_column(ForeignKey("geneassessment.id"))
    previous_assessment: Mapped[GeneAssessment | None] = relationship()
    gene_id: Mapped[int] = mapped_column(ForeignKey("gene.hgnc_id"))
    gene: Mapped[Gene] = relationship(back_populates="assessments")
    analysis_id: Mapped[int | None] = mapped_column(ForeignKey("analysis.id", ondelete="SET NULL"))

    __table_args__ = (
        Index(
            "ix_geneassessment_geneid_unique",
            "gene_id",
            postgresql_where=(date_superceeded.is_(None)),
            unique=True,
        ),
    )

    @override
    def __repr__(self):
        return f"<GeneAssessment('{self.id}','{self.gene_id}', '{str(self.user)}')>"
