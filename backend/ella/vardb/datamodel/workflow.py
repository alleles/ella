#!/usr/bin/env python
from __future__ import annotations

from datetime import datetime
from typing import TYPE_CHECKING, Literal, override

import pytz
from sqlalchemy import (
    DateTime,
    Enum,
    ForeignKey,
    ForeignKeyConstraint,
    Index,
    UniqueConstraint,
    select,
)
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import Mapped, Session, mapped_column, relationship

from ella.api.schemas.pydantic.v1.workflow import (
    AlleleInterpretationState,
    AnalysisInterpretationState,
)
from ella.vardb.datamodel import Base
from ella.vardb.datamodel.allele import Allele
from ella.vardb.datamodel.pydantic.pydantic_type import PydanticType
from ella.vardb.util.mutjson import JSONMutableDict

if TYPE_CHECKING:
    from ella.vardb.datamodel.gene import Genepanel
    from ella.vardb.datamodel.sample import Analysis
    from ella.vardb.datamodel.user import User


class InterpretationMixin:
    id: Mapped[int] = mapped_column(primary_key=True)
    genepanel_name: Mapped[str]
    genepanel_version: Mapped[str]
    user_state: Mapped[dict | None] = mapped_column(JSONMutableDict.as_mutable(JSONB), default={})

    status: Mapped[str] = mapped_column(
        Enum("Not started", "Ongoing", "Done", name="interpretation_status"),
        default="Not started",
    )
    finalized: Mapped[bool | None]
    date_last_update: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=lambda: datetime.now(pytz.utc)
    )
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=lambda: datetime.now(pytz.utc)
    )

    __next_attrs__: list[str]

    @declared_attr
    def user_id(cls) -> Mapped[int | None]:
        return mapped_column(ForeignKey("user.id"))

    @declared_attr
    def user(cls) -> Mapped[User | None]:
        return relationship()

    @declared_attr
    def genepanel(cls) -> Mapped[Genepanel]:
        return relationship()

    @declared_attr
    def __table_args__(cls):
        return (
            ForeignKeyConstraint(
                ["genepanel_name", "genepanel_version"], ["genepanel.name", "genepanel.version"]
            ),
        )

    @classmethod
    def create_next(cls, old):
        next_obj = cls()
        for attr in cls.__next_attrs__:
            setattr(next_obj, attr, getattr(old, attr))
        return next_obj


class InterpretationSnapshotMixin:
    id: Mapped[int] = mapped_column(primary_key=True)
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=lambda: datetime.now(pytz.utc)
    )
    filtered: Mapped[str | None] = mapped_column(
        Enum(
            "CLASSIFICATION",
            "FREQUENCY",
            "REGION",
            "POLYPYRIMIDINE",
            "GENE",
            "QUALITY",
            "CONSEQUENCE",
            "SEGREGATION",
            "INHERITANCEMODEL",
            name="interpretationsnapshot_filtered",
        )
    )  # If the allele was filtered, this describes which type of filtering

    @declared_attr
    def annotation_id(cls) -> Mapped[int | None]:
        return mapped_column(ForeignKey("annotation.id"))  # None for an excluded allele

    @declared_attr
    def customannotation_id(cls) -> Mapped[int | None]:
        return mapped_column(ForeignKey("customannotation.id"))

    @declared_attr
    def alleleassessment_id(cls) -> Mapped[int | None]:
        return mapped_column(ForeignKey("alleleassessment.id"))

    @declared_attr
    def allelereport_id(cls) -> Mapped[int | None]:
        return mapped_column(ForeignKey("allelereport.id"))


class AnalysisInterpretation(Base, InterpretationMixin):
    """Represents an Interpretation by a labengineer

    This corresponds to one interpretation 'round' of an analysis.
    The table stores both normal state and user-specific state for each round,
    while keeping a history of the state upon update.
    """

    __tablename__ = "analysisinterpretation"

    __next_attrs__ = ["analysis_id", "state", "genepanel_name", "genepanel_version"]

    analysis_id: Mapped[int] = mapped_column(ForeignKey("analysis.id", ondelete="CASCADE"))
    analysis: Mapped[Analysis] = relationship()
    workflow_status: Mapped[str] = mapped_column(
        Enum(
            "Not ready",
            "Interpretation",
            "Review",
            "Medical review",
            name="analysisinterpretation_workflow_status",
        ),
        default="Interpretation",
    )
    state: Mapped[AnalysisInterpretationState] = mapped_column(
        PydanticType(AnalysisInterpretationState, default_factory=AnalysisInterpretationState)
    )
    snapshots: Mapped[list[AnalysisInterpretationSnapshot]] = relationship(
        back_populates="analysisinterpretation"
    )

    @override
    def __repr__(self):
        return f"<Interpretation('{str(self.analysis_id)}', '{self.status}')>"


Index(
    "ix_analysisinterpretation_analysisid_ongoing_unique",
    AnalysisInterpretation.analysis_id,
    postgresql_where=(AnalysisInterpretation.status.in_(["Ongoing", "Not started"])),  # type: ignore[ella-mypy]
    unique=True,
)


class AnalysisInterpretationSnapshot(Base, InterpretationSnapshotMixin):
    """
    Represents a snapshot of a allele interpretation,
    at the time when it was marked as 'Done'.
    It's logging all relevant context for the interpretation.
    """

    __tablename__ = "analysisinterpretationsnapshot"

    analysisinterpretation_id: Mapped[int] = mapped_column(
        ForeignKey("analysisinterpretation.id", ondelete="CASCADE")
    )
    analysisinterpretation: Mapped[AnalysisInterpretation] = relationship(
        back_populates="snapshots"
    )
    allele_id: Mapped[int] = mapped_column(ForeignKey("allele.id"))
    __table_args__ = (UniqueConstraint("analysisinterpretation_id", "allele_id"),)


class AlleleInterpretation(Base, InterpretationMixin):
    """Represents an interpretation of an allele by a labengineer

    This corresponds to one interpretation 'round' of an allele.
    The table stores both normal state and user-specific state for each round,
    while keeping a history of the state upon update.
    """

    __tablename__ = "alleleinterpretation"

    __next_attrs__ = ["allele_id", "state", "genepanel_name", "genepanel_version"]

    allele_id: Mapped[int] = mapped_column(ForeignKey("allele.id"))
    allele: Mapped[Allele] = relationship()
    state: Mapped[AlleleInterpretationState] = mapped_column(
        PydanticType(AlleleInterpretationState, default_factory=AlleleInterpretationState)
    )
    workflow_status: Mapped[Literal["Interpretation", "Review"]] = mapped_column(
        Enum("Interpretation", "Review", name="alleleinterpretation_workflow_status"),
        default="Interpretation",
    )
    snapshots: Mapped[list[AlleleInterpretationSnapshot]] = relationship(
        back_populates="alleleinterpretation"
    )

    @override
    @classmethod
    def get_or_create(cls, session: Session, defaults=None, allele_id=None, **kwargs):
        "Override to check that allele is not CNV."
        caller_type = session.execute(
            select(Allele.caller_type).where(Allele.id == allele_id)
        ).scalar_one()

        if caller_type.lower() == "cnv":
            raise NotImplementedError("Standalone CNV variants not yet supported")

        return super().get_or_create(session, defaults=defaults, allele_id=allele_id, **kwargs)

    @override
    def __repr__(self):
        return f"<AlleleInterpretation('{str(self.allele_id)}', '{self.status}')>"


Index(
    "ix_alleleinterpretation_alleleid_ongoing_unique",
    AlleleInterpretation.allele_id,
    postgresql_where=(AlleleInterpretation.status.in_(["Ongoing", "Not started"])),  # type: ignore[ella-mypy]
    unique=True,
)


class AlleleInterpretationSnapshot(Base, InterpretationSnapshotMixin):
    """
    Represents a snapshot of a allele interpretation,
    at the time when it was marked as 'Done'.
    It's logging all relevant context for the interpretation.
    """

    __tablename__ = "alleleinterpretationsnapshot"

    alleleinterpretation_id: Mapped[int] = mapped_column(
        ForeignKey("alleleinterpretation.id", ondelete="CASCADE")
    )
    alleleinterpretation: Mapped[AlleleInterpretation] = relationship(back_populates="snapshots")
    allele_id: Mapped[int] = mapped_column(ForeignKey("allele.id"))
    __table_args__ = (UniqueConstraint("alleleinterpretation_id", "allele_id"),)


class InterpretationStateHistory(Base):
    """
    Holds the history of the state for the interpretations.
    Every time the [allele|analysis]interpretation state is updated (i.e. when user saves),
    it's copied into this table.
    """

    __tablename__ = "interpretationstatehistory"

    id: Mapped[int] = mapped_column(primary_key=True)
    alleleinterpretation_id: Mapped[int | None] = mapped_column(
        ForeignKey("alleleinterpretation.id", ondelete="CASCADE")
    )
    analysisinterpretation_id: Mapped[int | None] = mapped_column(
        ForeignKey("analysisinterpretation.id", ondelete="CASCADE")
    )
    user_id: Mapped[int] = mapped_column(ForeignKey("user.id"))
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=lambda: datetime.now(pytz.utc)
    )
    state: Mapped[AnalysisInterpretationState | AlleleInterpretationState] = mapped_column(
        PydanticType(AlleleInterpretationState | AnalysisInterpretationState)
    )


class InterpretationLog(Base):
    __tablename__ = "interpretationlog"

    id: Mapped[int] = mapped_column(primary_key=True)
    alleleinterpretation_id: Mapped[int | None] = mapped_column(
        ForeignKey("alleleinterpretation.id", ondelete="CASCADE")
    )
    analysisinterpretation_id: Mapped[int | None] = mapped_column(
        ForeignKey("analysisinterpretation.id", ondelete="CASCADE")
    )
    user_id: Mapped[int | None] = mapped_column(
        ForeignKey("user.id")
    )  # Can be null if created by system
    user: Mapped[User | None] = relationship()
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=lambda: datetime.now(pytz.utc)
    )
    message: Mapped[str | None]
    priority: Mapped[int | None]
    review_comment: Mapped[str | None]
    warning_cleared: Mapped[bool | None]
    alleleassessment_id: Mapped[int | None] = mapped_column(ForeignKey("alleleassessment.id"))
    allelereport_id: Mapped[int | None] = mapped_column(ForeignKey("allelereport.id"))
