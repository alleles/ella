"""varDB datamodel Annotation class"""
from __future__ import annotations

from datetime import datetime
from typing import TYPE_CHECKING, override

import pytz
from sqlalchemy import DateTime, FetchedValue, ForeignKey, Index
from sqlalchemy.dialects.postgresql import JSON, JSONB
from sqlalchemy.orm import Mapped, mapped_column, relationship

from ella.vardb.datamodel import Base
from ella.vardb.util.mutjson import JSONMutableDict, JSONMutableList

if TYPE_CHECKING:
    from ella.vardb.datamodel.allele import Allele
    from ella.vardb.datamodel.user import User


class Annotation(Base):
    """Represents a set of annotations for an allele"""

    __tablename__ = "annotation"

    id: Mapped[int] = mapped_column(primary_key=True)
    allele_id: Mapped[int | None] = mapped_column(ForeignKey("allele.id"), index=True)
    allele: Mapped[Allele | None] = relationship()
    annotations: Mapped[dict | None] = mapped_column(JSONMutableDict.as_mutable(JSONB))
    schema_version: Mapped[int] = mapped_column(server_default=FetchedValue())
    previous_annotation_id: Mapped[int | None] = mapped_column(ForeignKey("annotation.id"))
    # use remote_side to store foreignkey for previous_annotation in 'this' parent:
    previous_annotation: Mapped[Annotation] = relationship(remote_side=id)
    date_superceeded: Mapped[datetime | None] = mapped_column(
        "date_superceeded", DateTime(timezone=True)
    )
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=lambda: datetime.now(pytz.utc)
    )
    annotation_config_id: Mapped[int] = mapped_column(ForeignKey("annotationconfig.id"), index=True)
    annotation_config: Mapped[AnnotationConfig] = relationship(back_populates="annotations")

    __table_args__ = (
        Index(
            "ix_annotation_unique",
            "allele_id",
            postgresql_where=(date_superceeded.is_(None)),
            unique=True,
        ),
    )

    @override
    def __repr__(self):
        return f"Annotation('{self.annotations}', '{self.previous_annotation}', '{self.date_superceeded}')"


class CustomAnnotation(Base):
    """Represents a set of annotations for an allele, created by a user"""

    __tablename__ = "customannotation"

    id: Mapped[int] = mapped_column(primary_key=True)
    annotations: Mapped[dict | None] = mapped_column(JSONMutableDict.as_mutable(JSONB))

    allele_id: Mapped[int | None] = mapped_column(ForeignKey("allele.id"))
    allele: Mapped[Allele | None] = relationship()
    user_id: Mapped[int | None] = mapped_column(ForeignKey("user.id"))
    user: Mapped[User | None] = relationship()
    previous_annotation_id: Mapped[int | None] = mapped_column(ForeignKey("customannotation.id"))
    # use remote_side to store foreignkey for previous_annotation in 'this' parent:
    previous_annotation: Mapped[CustomAnnotation | None] = relationship(remote_side=id)
    date_superceeded: Mapped[datetime | None] = mapped_column(DateTime(timezone=True))
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=lambda: datetime.now(pytz.utc)
    )

    __table_args__ = (
        Index(
            "ix_customannotation_unique",
            "allele_id",
            postgresql_where=(date_superceeded.is_(None)),
            unique=True,
        ),
    )

    @override
    def __repr__(self):
        return f"<CustomAnnotation('{self.annotations}')>"


class AnnotationConfig(Base):
    __tablename__ = "annotationconfig"

    id: Mapped[int] = mapped_column(primary_key=True)
    # Use JSON instead of JSONB, to preserve order of keys
    deposit: Mapped[dict] = mapped_column(
        JSONMutableList.as_mutable(JSON),  # type: ignore[arg-type]
        default=lambda: {},
    )
    view: Mapped[dict] = mapped_column(
        JSONMutableList.as_mutable(JSON),  # type: ignore[arg-type]
        default=lambda: {},
    )
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=lambda: datetime.now(pytz.utc)
    )
    annotations: Mapped[list[Annotation]] = relationship(back_populates="annotation_config")
