#!/usr/bin/env python
"""varDB datamodel classes for entities that relate to samples."""
from __future__ import annotations

from datetime import datetime
from typing import TYPE_CHECKING, Literal, override

import pytz
from sqlalchemy import DateTime, Enum, ForeignKey
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import Mapped, mapped_column, relationship
from sqlalchemy.schema import ForeignKeyConstraint, Index

from ella.vardb.datamodel import Base
from ella.vardb.datamodel.pydantic.filterconfig import FilterConfigModel
from ella.vardb.datamodel.pydantic.pydantic_type import PydanticType
from ella.vardb.util.mutjson import JSONMutableDict

if TYPE_CHECKING:
    from ella.vardb.datamodel.gene import Genepanel
    from ella.vardb.datamodel.genotype import Genotype
    from ella.vardb.datamodel.workflow import AnalysisInterpretation


class Sample(Base):
    """Represents a sample (aka one sequencing run of 1 biological sample)

    Can represent samples from two types of technologies, Sanger and HTS.

    Note: there can be multiple samples with same name in database, and they might differ in genotypes.
    This happens when multiple analyses, using the same sample data in pipeline, is imported.
    They can have been run on different regions.
    """

    __tablename__ = "sample"

    id: Mapped[int] = mapped_column(primary_key=True)
    identifier: Mapped[str]
    analysis_id: Mapped[int] = mapped_column(ForeignKey("analysis.id", ondelete="CASCADE"))
    analysis: Mapped[Analysis] = relationship(back_populates="samples")
    sample_type: Mapped[Literal["HTS", "Sanger"]] = mapped_column(
        Enum("HTS", "Sanger", name="sample_type")
    )
    proband: Mapped[bool]
    family_id: Mapped[str | None]
    father_id: Mapped[int | None] = mapped_column(ForeignKey("sample.id"))  # Set on proband
    mother_id: Mapped[int | None] = mapped_column(ForeignKey("sample.id"))  # Set on proband
    sibling_id: Mapped[int | None] = mapped_column(
        ForeignKey("sample.id")
    )  # Set for siblings pointing to proband (i.e. _not_ on proband)
    sex: Mapped[str | None] = mapped_column(
        Enum("Male", "Female", name="sample_sex")
    )  # Can be unknown
    affected: Mapped[bool]
    date_deposited: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=lambda: datetime.now(pytz.utc)
    )
    genotypes: Mapped[list[Genotype]] = relationship(back_populates="sample")

    __table_args__ = (Index("ix_sampleidentifier", "identifier"),)

    @override
    def __repr__(self):
        return f"<Sample('{self.identifier}', '{self.sample_type}')>"


class Analysis(Base):
    """Represents a bioinformatical pipeline analysis

    An Analysis will have produced variant descriptions (e.g. VCF),
    that are an object for Interpretation.
    """

    __tablename__ = "analysis"

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(unique=True)
    # keep both directives w/o params, needed for composite foreign keys
    genepanel_name: Mapped[str | None] = mapped_column()  # TODO: non-nullable?
    genepanel_version: Mapped[str | None] = mapped_column()  # TODO: non-nullable?
    genepanel: Mapped[Genepanel | None] = relationship()
    warnings: Mapped[str | None]
    report: Mapped[str | None]
    date_requested: Mapped[datetime | None] = mapped_column(DateTime(timezone=True))
    date_deposited: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=lambda: datetime.now(pytz.utc)
    )
    interpretations: Mapped[list[AnalysisInterpretation]] = relationship(
        order_by="AnalysisInterpretation.id", viewonly=True
    )
    properties: Mapped[dict | None] = mapped_column(
        JSONMutableDict.as_mutable(JSONB)
    )  # Holds commments, tags etc
    __table_args__ = (
        ForeignKeyConstraint(
            [genepanel_name, genepanel_version], ["genepanel.name", "genepanel.version"]
        ),
    )

    samples: Mapped[list[Sample]] = relationship()

    @override
    def __repr__(self):
        return f"<Analysis('{self.samples}, {self.genepanel_name}, {self.genepanel_version}')>"


class FilterConfig(Base):
    """
    Represents a configuration of an analysis,
    e.g. ACMG configuration and filter configuration.

    Note that one analysis can utilise several AnalysisConfigs as part of
    a workflow/interpretation.
    """

    __tablename__ = "filterconfig"

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]
    filterconfig: Mapped[FilterConfigModel] = mapped_column(PydanticType(FilterConfigModel))
    requirements: Mapped[dict] = mapped_column(JSONB, default=[])
    date_superceeded: Mapped[datetime | None] = mapped_column(
        "date_superceeded", DateTime(timezone=True)
    )
    previous_filterconfig_id: Mapped[int | None] = mapped_column(ForeignKey("filterconfig.id"))
    active: Mapped[bool] = mapped_column(default=True)

    __table_args__ = (
        Index(
            "uq_filterconfig_name_active_unique",
            "name",
            postgresql_where=active.is_(True),
            unique=True,
        ),
    )

    @override
    def __repr__(self):
        return f"<FilterConfig({self.id}, {self.name})>"


class UserGroupFilterConfig(Base):
    __tablename__ = "usergroupfilterconfig"

    id: Mapped[int] = mapped_column(primary_key=True)
    usergroup_id: Mapped[int] = mapped_column(ForeignKey("usergroup.id"))
    filterconfig_id: Mapped[int] = mapped_column(ForeignKey("filterconfig.id"))
    order: Mapped[int]

    __table_args__ = (
        Index(
            "uq_usergroupfilterconfig_unique",
            "usergroup_id",
            "filterconfig_id",
            unique=True,
        ),
    )
