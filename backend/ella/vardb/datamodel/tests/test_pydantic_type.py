import pytest
from sqlalchemy import select, text
from sqlalchemy.orm import Mapped, Session, mapped_column

from ella.api.schemas.pydantic.v1 import BaseModel
from ella.vardb.datamodel.migration.migration_base import Base
from ella.vardb.datamodel.pydantic.pydantic_type import PydanticType


class NestedPydanticModel(BaseModel):
    without_default: str
    with_default: str = "default_value"


class PydanticModel(BaseModel):
    field: str
    field_with_default: str = "default_value"
    nested_field: NestedPydanticModel
    nested_field_with_default: NestedPydanticModel = NestedPydanticModel(without_default="init")


class PydanticModelWithDefaultsOnly(BaseModel):
    field_with_default: str = "default_value"
    nested_field_with_default: NestedPydanticModel = NestedPydanticModel(without_default="init")


class TableForTesting(Base):
    __tablename__ = "table_for_testing"
    id: Mapped[int] = mapped_column(primary_key=True)
    pydantic_field: Mapped[PydanticModel] = mapped_column(PydanticType(PydanticModel))
    pydantic_field_with_defaults_only: Mapped[PydanticModelWithDefaultsOnly] = mapped_column(
        PydanticType(PydanticModelWithDefaultsOnly, default_factory=PydanticModelWithDefaultsOnly)
    )


@pytest.fixture(scope="function", autouse=True)
def create_table(session: Session):
    Base.metadata.create_all(session.bind)


def test_pydantic_type(session: Session):
    "Test that we can insert and read back a Pydantic object."
    # Create a PydanticModel object
    pydantic_instance = PydanticModel(
        field="field_value",
        nested_field=NestedPydanticModel(without_default="foo_value", with_default="bar_value"),
    )

    obj = TableForTesting(pydantic_field=pydantic_instance)
    session.add(obj)
    session.flush()

    res = session.execute(select(TableForTesting)).scalars().one()
    assert res.pydantic_field == pydantic_instance


def test_pydantic_type_defaults(session: Session):
    "Test that the object is stored in the database without default values"
    pydantic_instance = PydanticModel(
        field="field_value", nested_field=NestedPydanticModel(without_default="foo_value")
    )
    obj = TableForTesting(pydantic_field=pydantic_instance)
    session.add(obj)
    session.flush()

    # Raw SQL query to check that the default values are not stored in the database
    res = session.execute(text("SELECT pydantic_field FROM table_for_testing")).scalar_one()

    assert res == {
        "field": "field_value",
        "nested_field": {"without_default": "foo_value"},
    }
    assert PydanticModel(**res) == pydantic_instance

    # Raw SQL query to check that the default values are not stored in the database
    res = session.execute(
        text("SELECT pydantic_field_with_defaults_only FROM table_for_testing")
    ).scalar_one()

    assert res == {}
