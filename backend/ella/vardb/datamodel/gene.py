"""varDB datamodel classes for Gene and Transcript"""
from __future__ import annotations

from datetime import datetime
from typing import TYPE_CHECKING, override

import pytz
from sqlalchemy import DateTime, Enum, ForeignKey, Index, Integer, String, Text, func
from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy.orm import Mapped, mapped_column, relationship
from sqlalchemy.schema import ForeignKeyConstraint, UniqueConstraint

from ella.vardb.datamodel import Base

if TYPE_CHECKING:
    from ella.vardb.datamodel.assessment import GeneAssessment
    from ella.vardb.datamodel.user import User


class Gene(Base):
    """Represents a gene abstraction"""

    __tablename__ = "gene"

    hgnc_id: Mapped[int] = mapped_column(primary_key=True)
    hgnc_symbol: Mapped[str] = mapped_column()  # keep, needed for Index definition below
    assessments: Mapped[list[GeneAssessment]] = relationship(back_populates="gene")

    __table_args__ = (
        Index(
            "ix_gene_hgnc_symbol",
            func.lower(hgnc_symbol),
            unique=True,
            postgresql_ops={"data": "text_pattern_ops"},
        ),
    )

    @override
    def __repr__(self):
        return f"<Gene({self.hgnc_id}, '{self.hgnc_symbol}')>"


class Transcript(Base):
    """Represents a gene transcript"""

    __tablename__ = "transcript"

    id: Mapped[int] = mapped_column(primary_key=True)
    gene_id: Mapped[int] = mapped_column(ForeignKey("gene.hgnc_id"))
    gene: Mapped[Gene] = relationship(lazy="joined")
    transcript_name: Mapped[str] = mapped_column(unique=True)
    type: Mapped[str] = mapped_column(Enum("RefSeq", "Ensembl", "LRG", name="transcript_type"))
    tags: Mapped[list[str] | None] = mapped_column(ARRAY(Text))
    genome_reference: Mapped[str]
    chromosome: Mapped[str]
    tx_start: Mapped[int]
    tx_end: Mapped[int]
    strand: Mapped[str] = mapped_column(String(1))
    cds_start: Mapped[int | None]
    cds_end: Mapped[int | None]
    exon_starts: Mapped[list[int]] = mapped_column(ARRAY(Integer))
    exon_ends: Mapped[list[int]] = mapped_column(ARRAY(Integer))

    @override
    def __repr__(self):
        return (
            f"<Transcript('{self.gene}','{self.transcript_name}', '{self.chromosome}',"
            f" '{self.tx_start}', '{self.tx_end}', '{self.strand}')>"
        )

    @override
    def __str__(self):
        return (
            f"{self.gene}, {self.transcript_name}, {self.chromosome}, {self.tx_start},"
            f" {self.tx_end}, {self.strand}"
        )


class GenepanelTranscript(Base):
    """Association table between Genepanel and Transcript for many-to-many relationship

    Uses ForeignKeyConstraint for referencing composite primary key in Genepanel.
    """

    __tablename__ = "genepanel_transcript"

    genepanel_name: Mapped[str] = mapped_column(primary_key=True)
    genepanel_version: Mapped[str] = mapped_column(primary_key=True)
    transcript_id: Mapped[int] = mapped_column(
        ForeignKey("transcript.id"), primary_key=True, index=True
    )
    inheritance: Mapped[str] = mapped_column(Text)

    __table_args__ = (
        ForeignKeyConstraint(
            ["genepanel_name", "genepanel_version"],
            ["genepanel.name", "genepanel.version"],
            ondelete="CASCADE",
        ),
    )


class Phenotype(Base):
    """Represents a gene phenotype"""

    __tablename__ = "phenotype"

    id: Mapped[int] = mapped_column(primary_key=True)

    gene_id: Mapped[int] = mapped_column(ForeignKey("gene.hgnc_id"))
    gene: Mapped[Gene] = relationship(lazy="joined")

    description: Mapped[str]
    inheritance: Mapped[str]
    omim_id: Mapped[int | None]

    __table_args__ = (UniqueConstraint("gene_id", "description", "inheritance"),)

    @override
    def __repr__(self):
        return f"<Phenotype('{self.description[:20]}')>"


class GenepanelPhenotype(Base):
    """Association table between Genepanel and Phenotype for many-to-many relationship

    Uses ForeignKeyConstraint for referencing composite primary key in Genepanel.
    """

    __tablename__ = "genepanel_phenotype"

    genepanel_name: Mapped[str] = mapped_column(primary_key=True)
    genepanel_version: Mapped[str] = mapped_column(primary_key=True)
    phenotype_id: Mapped[int] = mapped_column(ForeignKey("phenotype.id"), primary_key=True)

    __table_args__ = (
        ForeignKeyConstraint(
            ["genepanel_name", "genepanel_version"],
            ["genepanel.name", "genepanel.version"],
            ondelete="CASCADE",
        ),
    )


class Region(Base):
    __tablename__ = "region"

    id: Mapped[int] = mapped_column(primary_key=True)
    genome_reference: Mapped[str]
    chromosome: Mapped[str]
    start: Mapped[int]
    end: Mapped[int]
    name: Mapped[str]

    __table_args__ = (UniqueConstraint("genome_reference", "chromosome", "start", "end", "name"),)


class GenepanelRegion(Base):
    __tablename__ = "genepanel_region"

    genepanel_name: Mapped[str] = mapped_column(primary_key=True)
    genepanel_version: Mapped[str] = mapped_column(primary_key=True)
    region_id: Mapped[int] = mapped_column(ForeignKey("region.id"), primary_key=True, index=True)
    inheritance: Mapped[str] = mapped_column(Text)

    __table_args__ = (
        ForeignKeyConstraint(
            ["genepanel_name", "genepanel_version"],
            ["genepanel.name", "genepanel.version"],
            ondelete="CASCADE",
        ),
    )


class Genepanel(Base):
    """Represents a gene panel"""

    __tablename__ = "genepanel"

    name: Mapped[str] = mapped_column(primary_key=True)
    version: Mapped[str] = mapped_column(primary_key=True)
    genome_reference: Mapped[str]
    official: Mapped[bool] = mapped_column(default=False)
    date_created: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=lambda: datetime.now(pytz.utc)
    )
    user_id: Mapped[int | None] = mapped_column(ForeignKey("user.id"))
    user: Mapped[User | None] = relationship()
    transcripts: Mapped[list[Transcript]] = relationship(secondary=GenepanelTranscript.__table__)
    phenotypes: Mapped[list[Phenotype]] = relationship(secondary=GenepanelPhenotype.__table__)
    regions: Mapped[list[Region]] = relationship(secondary=GenepanelRegion.__table__)

    @override
    def __repr__(self):
        return f"<Genepanel('{self.name}','{self.version}', '{self.genome_reference}')>"

    @override
    def __str__(self):
        return f"{self.name}_{self.version}_{self.genome_reference}"
