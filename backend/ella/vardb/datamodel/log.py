from datetime import datetime

import pytz
from sqlalchemy import DateTime, ForeignKey
from sqlalchemy.dialects.postgresql import INET, JSONB
from sqlalchemy.orm import Mapped, mapped_column

from ella.vardb.datamodel import Base
from ella.vardb.util.mutjson import JSONMutableDict


class ResourceLog(Base):
    """Logs HTTP resource access"""

    __tablename__ = "resourcelog"

    id: Mapped[int] = mapped_column(primary_key=True)
    remote_addr: Mapped[str] = mapped_column(INET)
    time: Mapped[datetime] = mapped_column(DateTime(timezone=True), index=True)
    duration: Mapped[int]
    usersession_id: Mapped[int | None] = mapped_column(ForeignKey("usersession.id"))
    method: Mapped[str]
    resource: Mapped[str]
    query: Mapped[str]
    response: Mapped[str | None]
    response_size: Mapped[int]
    payload: Mapped[str | None]
    payload_size: Mapped[int]
    statuscode: Mapped[int]


class CliLog(Base):
    """Logs CLI actions"""

    __tablename__ = "clilog"

    id: Mapped[int] = mapped_column(primary_key=True)
    time: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=lambda: datetime.now(pytz.utc)
    )
    user: Mapped[str]
    group: Mapped[str]
    groupcommand: Mapped[str]
    command: Mapped[str]
    reason: Mapped[str | None]
    output: Mapped[str]


class UiExceptionLog(Base):
    """Logs CLI actions"""

    __tablename__ = "uiexception"

    id: Mapped[int] = mapped_column(primary_key=True)
    time: Mapped[datetime] = mapped_column(
        DateTime(timezone=True), default=lambda: datetime.now(pytz.utc)
    )
    usersession_id: Mapped[int | None] = mapped_column(ForeignKey("usersession.id"))
    message: Mapped[str]
    location: Mapped[str | None]
    stacktrace: Mapped[str | None]
    state: Mapped[dict | None] = mapped_column(JSONMutableDict.as_mutable(JSONB), default={})
