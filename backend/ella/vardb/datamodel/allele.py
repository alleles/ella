"""vardb datamodel Allele class"""
from __future__ import annotations

from typing import TYPE_CHECKING, Literal, override

from sqlalchemy import Enum
from sqlalchemy.orm import Mapped, mapped_column, relationship
from sqlalchemy.schema import Index, UniqueConstraint

from ella.vardb.datamodel import Base

if TYPE_CHECKING:
    from ella.vardb.datamodel.assessment import AlleleAssessment, AlleleReport
    from ella.vardb.datamodel.genotype import Genotype


class Allele(Base):
    """Represents an allele (a variant type in a genomic position)"""

    __tablename__ = "allele"

    id: Mapped[int] = mapped_column(primary_key=True)
    genome_reference: Mapped[str]
    genotypes: Mapped[list[Genotype]] = relationship(
        primaryjoin="or_(Allele.id==Genotype.allele_id, " "Allele.id==Genotype.secondallele_id)",
        viewonly=True,
    )
    chromosome: Mapped[str]
    start_position: Mapped[int]
    open_end_position: Mapped[int]
    change_from: Mapped[str]
    change_to: Mapped[str]
    change_type: Mapped[str] = mapped_column(
        Enum(
            "SNP",
            "del",
            "ins",
            "indel",
            "dup",
            "dup_tandem",
            "del_me",
            "ins_me",
            "inv",
            "bnd",
            name="change_type",
        ),
    )

    caller_type: Mapped[Literal["cnv", "snv"]] = mapped_column(
        Enum("snv", "cnv", name="caller_type")
    )
    vcf_pos: Mapped[int]
    vcf_ref: Mapped[str]
    vcf_alt: Mapped[str]
    length: Mapped[int]
    assessments: Mapped[list[AlleleAssessment]] = relationship(back_populates="allele")
    reports: Mapped[list[AlleleReport]] = relationship(back_populates="allele")

    __table_args__ = (
        Index("ix_alleleloci", "chromosome", "start_position", "open_end_position"),
        UniqueConstraint(
            "chromosome",
            "start_position",
            "open_end_position",
            "change_from",
            "change_to",
            "change_type",
            "vcf_pos",
            "vcf_ref",
            "vcf_alt",
            "length",
            "caller_type",
            name="ucAllele",
        ),
    )

    @override
    def __repr__(self):
        return (
            f"<Allele('{self.chromosome}', '{self.start_position}', '{self.open_end_position}',"
            f" '{self.change_type}', '{self.change_from}', '{self.change_to}', '{self.length}',"
            f" '{self.caller_type}')>"
        )
