"""Gene panel custom regions

Revision ID: e9eb70a95732
Revises: 1e034870b0a3
Create Date: 2025-02-20 07:39:35.341754

"""

# revision identifiers, used by Alembic.
revision = 'e9eb70a95732'
down_revision = '1e034870b0a3'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('region',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('genome_reference', sa.String(), nullable=False),
    sa.Column('chromosome', sa.String(), nullable=False),
    sa.Column('start', sa.Integer(), nullable=False),
    sa.Column('end', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(), nullable=False),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_region'))
    )
    op.create_table('genepanel_region',
    sa.Column('genepanel_name', sa.String(), nullable=False),
    sa.Column('genepanel_version', sa.String(), nullable=False),
    sa.Column('region_id', sa.Integer(), nullable=False),
    sa.Column('inheritance', sa.Text(), nullable=False),
    sa.ForeignKeyConstraint(['genepanel_name', 'genepanel_version'], ['genepanel.name', 'genepanel.version'], name=op.f('fk_genepanel_region_genepanel_name_genepanel'), ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['region_id'], ['region.id'], name=op.f('fk_genepanel_region_region_id_region')),
    sa.PrimaryKeyConstraint('genepanel_name', 'genepanel_version', 'region_id', name=op.f('pk_genepanel_region'))
    )
    op.create_index(op.f('ix_genepanel_region_region_id'), 'genepanel_region', ['region_id'], unique=False)

    op.create_unique_constraint(
        None,
        "region",
        ["genome_reference", "chromosome", "start", "end", "name"]
    )



def downgrade():
    raise NotImplementedError("Downgrade not supported")
