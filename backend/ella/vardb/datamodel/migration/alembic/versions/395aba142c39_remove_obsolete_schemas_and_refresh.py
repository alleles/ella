"""Remove obsolete schemas and refresh

Revision ID: 395aba142c39
Revises: 1e23c5e9dcb7
Create Date: 2024-03-21 08:37:48.810351

"""

# revision identifiers, used by Alembic.
revision = '395aba142c39'
down_revision = '1e23c5e9dcb7'
branch_labels = None
depends_on = None

import sqlalchemy as sa
from alembic import op
from sqlalchemy.orm.session import Session

from ella.vardb.datamodel.jsonschemas.update_schemas import update_schemas


def upgrade():
    # Filterconfig schema is deprecated, and annotation schema is update, but is fully backward compatible.
    # Therefore, replace all existing schemas by first dropping them, and update_schemas again 
    op.execute("DELETE FROM jsonschema")
    session = Session(bind=op.get_bind())
    update_schemas(session)
    session.flush()


def downgrade():
    raise NotImplementedError()
