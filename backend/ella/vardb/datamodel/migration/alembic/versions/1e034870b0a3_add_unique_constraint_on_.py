"""Add unique constraint on UserGroupGenepanel

Revision ID: 1e034870b0a3
Revises: dafc920d2722
Create Date: 2024-08-09 13:08:43.430247

"""

# revision identifiers, used by Alembic.
revision = "1e034870b0a3"
down_revision = "dafc920d2722"
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    # Remove duplicate rows
    op.execute("""
    DELETE FROM usergroupgenepanel a
    USING usergroupgenepanel b
    WHERE 
        a.usergroup_id = b.usergroup_id AND
        a.genepanel_name = b.genepanel_name AND
        a.genepanel_version = b.genepanel_version AND
        a.ctid > b.ctid
    """)

    op.create_unique_constraint(
        op.f("uq_usergroupgenepanel_usergroup_id"),
        "usergroupgenepanel",
        ["usergroup_id", "genepanel_name", "genepanel_version"],
    )


def downgrade():
    op.drop_constraint(
        op.f("uq_usergroupgenepanel_usergroup_id"), "usergroupgenepanel", type_="unique"
    )
