"""Consolidate interpretation state

Revision ID: b9cc216401bd
Revises: 395aba142c39
Create Date: 2024-07-02 10:31:59.179015

"""

# revision identifiers, used by Alembic.
revision = "b9cc216401bd"
down_revision = "395aba142c39"
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

from ella.api.schemas.pydantic.v1.references import (
    NewReferenceAssessment,
    NonReusedReferenceAssessment,
    ReusedReferenceAssessment,
)
from ella.api.schemas.pydantic.v1.workflow import AlleleInterpretationState, AnalysisInterpretationState


from ella.api.schemas.pydantic.v1.workflow_allele_state import (
    AlleleState,
    AnalysisAlleleState,
    NewAlleleAssessment,
    NewAlleleReport,
    NonReusedAlleleAssessment,
    NonReusedAlleleReport,
    ReusedAlleleAssessment,
    ReusedAlleleReport,
)

DATABASE_ALLELE_REPORTS = {}


def migrate_reference_assessments(state_referenceassessments: list[dict]):
    for index, reference_assessment in enumerate(state_referenceassessments):
        for key in [
            "genepanel_name",
            "genepanel_version",
            "user_id",
            "analysis_id",
            "date_created",
            "date_superceeded",
        ]:
            reference_assessment.pop(key, None)

        # Remove empty evaluation
        if "evaluation" in reference_assessment and not reference_assessment["evaluation"]:
            reference_assessment.pop("evaluation")

        # Silently pop 'id'
        if "id" in reference_assessment:
            if "reuseCheckedId" not in reference_assessment:
                reference_assessment["reuseCheckedId"] = reference_assessment["id"]
            assert reference_assessment["reuseCheckedId"] == reference_assessment.pop("id")

        ra_keys = set(reference_assessment.keys())

        if set(["allele_id", "reference_id", "evaluation", "reuseCheckedId"]) == ra_keys:
            reference_assessment["reuse"] = False
        elif set(["allele_id", "reference_id", "reuseCheckedId"]) == ra_keys:
            reference_assessment["reuse"] = True

        # If it is reused, it should not contain evaluation
        if reference_assessment.get("reuse"):
            reference_assessment.pop("evaluation", None)
        elif "reuse" not in reference_assessment and "evaluation" not in reference_assessment:
            reference_assessment["evaluation"] = {"comment": "", "sources": []}
            if "reuseCheckedId" in reference_assessment:
                reference_assessment["reuse"] = False

        match reference_assessment.get("reuse"):
            case True:
                state_referenceassessments[index] = ReusedReferenceAssessment(
                    **reference_assessment
                )
            case False:
                state_referenceassessments[index] = NonReusedReferenceAssessment(
                    **reference_assessment
                )
            case None:
                state_referenceassessments[index] = NewReferenceAssessment(**reference_assessment)
            case _:
                raise ValueError("reuse must be True, False or None")

    return state_referenceassessments


def migrate_allele_assessment(alleleassessment: dict):
    if aa_evaluation := alleleassessment.get("evaluation", {}):
        evaluation_fields = set(
            [
                "acmg",
                "external",
                "frequency",
                "reference",
                "prediction",
                "classification",
                "similar",
            ]
        )
        for k in evaluation_fields:
            if k not in aa_evaluation:
                aa_evaluation[k] = {"comment": ""}

        for k in set(aa_evaluation.keys()) - evaluation_fields:
            aa_evaluation.pop(k)

    alleleassessment.pop("allele_id", None)

    if alleleassessment.get("reuse"):
        alleleassessment = ReusedAlleleAssessment(
            reuse=True,
            reuseCheckedId=alleleassessment["reuseCheckedId"],
        )
    elif alleleassessment.get("reuse") is False and "reuseCheckedId" in alleleassessment:
        alleleassessment = NonReusedAlleleAssessment(
            attachment_ids=alleleassessment["attachment_ids"],
            classification=alleleassessment["classification"],
            evaluation=alleleassessment["evaluation"],
            reuse=False,
            reuseCheckedId=alleleassessment["reuseCheckedId"],
        )
    else:
        alleleassessment = NewAlleleAssessment(
            attachment_ids=alleleassessment["attachment_ids"],
            classification=alleleassessment["classification"],
            evaluation=aa_evaluation,
        )
    return alleleassessment


def migrate_allele_report(allelereport: dict):
    if "copiedFromId" in allelereport:
        allelereport["reuseCheckedId"] = allelereport.pop("copiedFromId")

    if db_report := DATABASE_ALLELE_REPORTS.get(allelereport.get("reuseCheckedId")):
        if allelereport.get("evaluation") == db_report:
            allelereport["reuse"] = True
            allelereport.pop("evaluation")
        else:
            allelereport["reuse"] = False

    match allelereport.get("reuse"):
        case True:
            allelereport = ReusedAlleleReport(
                reuse=True,
                reuseCheckedId=allelereport["reuseCheckedId"],
            )
        case False:
            allelereport = NonReusedAlleleReport(
                evaluation=allelereport["evaluation"],
                reuse=False,
                reuseCheckedId=allelereport["reuseCheckedId"],
            )
        case None:
            allelereport = NewAlleleReport(evaluation=allelereport["evaluation"])

    return allelereport


def migrate_allele_state_common(allelestate: dict):
    if "quality" in allelestate:
        if "analysis" in allelestate:
            allelestate.pop("quality")
        else:
            allelestate["analysis"] = allelestate.pop("quality")

    if "verification" in allelestate:
        if "analysis" in allelestate:
            allelestate["analysis"]["verification"] = allelestate.pop("verification")
        else:
            allelestate["analysis"] = {"verification": allelestate.pop("verification")}

    # Frontend migrations (frontend-legacy/src/js/store/common/helpers/alleleState.js)
    if "alleleAssessmentCopiedFromId" in allelestate:
        allelestate["alleleassessment"]["reuseCheckedId"] = allelestate.pop(
            "alleleAssessmentCopiedFromId"
        )

    if "autoReuseAlleleAssessmentCheckedId" in allelestate:
        # Migrate reuseCheckedId
        allelestate["alleleassessment"]["reuseCheckedId"] = allelestate.pop(
            "autoReuseAlleleAssessmentCheckedId"
        )

    if "alleleReportCopiedFromId" in allelestate:
        allelestate["allelereport"]["reuseCheckedId"] = allelestate.pop("alleleReportCopiedFromId")

    for key in [
        "alleleAssessmentCopiedFromId",
        "presented_alleleassessment_id",
        "presented_allelereport_id",
        "alleleReportCopiedFromId",
    ]:
        if key in allelestate:
            allelestate.pop(key)

    if "workflow" not in allelestate:
        allelestate["workflow"] = {}
    if "reviewed" not in allelestate["workflow"]:
        allelestate["workflow"]["reviewed"] = False

    if "referenceassessments" in allelestate:
        allelestate["referenceassessments"] = migrate_reference_assessments(
            allelestate["referenceassessments"]
        )

    if "alleleassessment" in allelestate:
        allelestate["alleleassessment"] = migrate_allele_assessment(allelestate["alleleassessment"])

    if "allelereport" in allelestate:
        allelestate["allelereport"] = migrate_allele_report(allelestate["allelereport"])

    return allelestate


def migrate_allele_state(allelestate: dict):
    allelestate = migrate_allele_state_common(allelestate)
    allelestate.pop("analysis", None)
    allelestate.pop("report", None)
    return AlleleState(**allelestate)


def migrate_analysis_allele_state(allelestate: dict):
    allelestate = migrate_allele_state_common(allelestate)

    if "analysis" not in allelestate:
        allelestate["analysis"] = {}

    if "comment" not in allelestate["analysis"]:
        allelestate["analysis"]["comment"] = ""

    if "verification" not in allelestate["analysis"]:
        allelestate["analysis"]["verification"] = None

    if "notrelevant" not in allelestate["analysis"]:
        allelestate["analysis"]["notrelevant"] = None

    if "report" not in allelestate:
        allelestate["report"] = {}

    if "included" not in allelestate["report"]:
        allelestate["report"]["included"] = False

    return AnalysisAlleleState(**allelestate)


def migrate_alleleinterpretation_state(state: dict):
    for k in list(state.keys()):
        if k == "allele":
            continue
        state.pop(k)

    if "allele" not in state:
        state["allele"] = {}

    for k, v in state["allele"].items():
        if "allele_id" not in v:
            v["allele_id"] = int(k)
        assert v["allele_id"] == int(k)
        state["allele"][k] = migrate_allele_state(v)

    return AlleleInterpretationState(**state)


def migrate_analysisinterpretation_state(state: dict):
    for k in list(state.keys()):
        if k in ["allele", "manuallyAddedAlleles", "report", "filterconfigId"]:
            continue
        state.pop(k)

    if "report" not in state:
        state["report"] = {}
    if "comment" not in state["report"]:
        state["report"]["comment"] = ""
    if "indicationscomment" not in state["report"]:
        state["report"]["indicationscomment"] = ""

    if "manuallyAddedAlleles" not in state:
        state["manuallyAddedAlleles"] = []

    if "allele" not in state:
        state["allele"] = {}

    for k, v in state["allele"].items():
        if "allele_id" not in v:
            v["allele_id"] = int(k)
        state["allele"][k] = migrate_analysis_allele_state(v)

    return AnalysisInterpretationState(**state)


AlleleInterpretation = sa.table(
    "alleleinterpretation",
    sa.column("id", sa.Integer()),
    sa.column("state", postgresql.JSONB(astext_type=sa.Text())),
)

AnalysisInterpretation = sa.table(
    "analysisinterpretation",
    sa.column("id", sa.Integer()),
    sa.column("state", postgresql.JSONB(astext_type=sa.Text())),
)

InterpretationStateHistory = sa.table(
    "interpretationstatehistory",
    sa.column("id", sa.Integer()),
    sa.column("analysisinterpretation_id", sa.Integer()),
    sa.column("alleleinterpretation_id", sa.Integer()),
    sa.column("state", postgresql.JSONB(astext_type=sa.Text())),
)

AlleleReport = sa.table(
    "allelereport",
    sa.column("id", sa.Integer()),
    sa.column("evaluation", postgresql.JSONB(astext_type=sa.Text())),
)


def upgrade():
    conn = op.get_bind()

    # Migrate all interpretation states
    # Note that we need to use yield_per to avoid loading all rows into memory - however, this needs to be reset before updating the state
    # It also needs to be reset after the loops, to avoid issues with other queries

    # To migrate allele reports, we need to know whether they are identical to existing reports (whether they are reused)
    # This is not possible to determine from the state alone, so we need to do this by first fetching all existing reports
    # and then comparing them to the reports in state

    for row in conn.execute(sa.select(AlleleReport.c.id, AlleleReport.c.evaluation)):
        DATABASE_ALLELE_REPORTS[row.id] = row.evaluation

    for row in conn.execution_options(yield_per=10000).execute(
        sa.select(
            InterpretationStateHistory.c.id,
            InterpretationStateHistory.c.analysisinterpretation_id,
            InterpretationStateHistory.c.alleleinterpretation_id,
            InterpretationStateHistory.c.state
        )
    ):
        if row.analysisinterpretation_id:
            migrated_state = migrate_analysisinterpretation_state(row.state)
        elif row.alleleinterpretation_id:
            migrated_state = migrate_alleleinterpretation_state(row.state)

        conn.execution_options(yield_per=None).execute(
            sa.update(InterpretationStateHistory)
            .where(InterpretationStateHistory.c.id == row.id)
            .values(state=migrated_state.model_dump(exclude_defaults=True))
        )

    for row in conn.execution_options(yield_per=10000).execute(
        sa.select(AlleleInterpretation.c.id, AlleleInterpretation.c.state)
    ):
        migrated_state = migrate_alleleinterpretation_state(row.state)
        conn.execution_options(yield_per=None).execute(
            sa.update(AlleleInterpretation)
            .where(AlleleInterpretation.c.id == row.id)
            .values(state=migrated_state.model_dump(exclude_defaults=True))
        )

    for row in conn.execution_options(yield_per=10000).execute(
        sa.select(AnalysisInterpretation.c.id, AnalysisInterpretation.c.state)
    ):
        migrated_state = migrate_analysisinterpretation_state(row.state)
        conn.execution_options(yield_per=None).execute(
            sa.update(AnalysisInterpretation)
            .where(AnalysisInterpretation.c.id == row.id)
            .values(state=migrated_state.model_dump(exclude_defaults=True))
        )

    conn.execution_options(yield_per=None)

    op.alter_column(
        "alleleinterpretation",
        "state",
        existing_type=postgresql.JSONB(astext_type=sa.Text()),
        nullable=False,
    )
    op.alter_column(
        "analysisinterpretation",
        "state",
        existing_type=postgresql.JSONB(astext_type=sa.Text()),
        nullable=False,
    )
    op.alter_column(
        "interpretationstatehistory",
        "state",
        existing_type=postgresql.JSONB(astext_type=sa.Text()),
        nullable=False,
    )


def downgrade():
    raise NotImplementedError("Downgrade not supported")
