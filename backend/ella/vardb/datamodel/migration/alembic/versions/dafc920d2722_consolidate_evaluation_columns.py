"""Consolidate evaluation columns

Revision ID: dafc920d2722
Revises: b9cc216401bd
Create Date: 2024-07-04 07:54:27.988298

"""

# revision identifiers, used by Alembic.
revision = "dafc920d2722"
down_revision = "b9cc216401bd"
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

from ella.api.schemas.pydantic.v1.allele_assessments import AlleleAssessmentEvaluation
from ella.api.schemas.pydantic.v1.allele_reports import AlleleReportEvaluation
from ella.api.schemas.pydantic.v1.gene_assessments import GeneAssessmentEvaluation
from ella.api.schemas.pydantic.v1.references import ReferenceEvaluation


def migrate_allele_assessment_evaluation(evaluation: dict):
    evaluation.pop("comment", None)
    evaluation.pop("undefined", None)

    return AlleleAssessmentEvaluation(**evaluation)


def migrate_allele_report_evaluation(evaluation: dict):
    return AlleleReportEvaluation(**evaluation)


def migrate_reference_assessment_evaluation(evaluation: dict):
    return ReferenceEvaluation(**evaluation)


def migrate_gene_assessment_evaluation(evaluation: dict):
    return GeneAssessmentEvaluation(**evaluation)


AlleleReport = sa.table(
    "allelereport",
    sa.column("id", sa.Integer()),
    sa.column("evaluation", postgresql.JSONB(astext_type=sa.Text())),
)

AlleleAssessment = sa.table(
    "alleleassessment",
    sa.column("id", sa.Integer()),
    sa.column("evaluation", postgresql.JSONB(astext_type=sa.Text())),
)

ReferenceAssessment = sa.table(
    "referenceassessment",
    sa.column("id", sa.Integer()),
    sa.column("evaluation", postgresql.JSONB(astext_type=sa.Text())),
)

GeneAssessment = sa.table(
    "geneassessment",
    sa.column("id", sa.Integer()),
    sa.column("evaluation", postgresql.JSONB(astext_type=sa.Text())),
)


def upgrade():
    conn = op.get_bind()

    for row in conn.execute(sa.select(AlleleReport.c.id, AlleleReport.c.evaluation)):
        evaluation = migrate_allele_report_evaluation(row.evaluation)
        conn.execute(
            AlleleReport.update()
            .where(AlleleReport.c.id == row.id)
            .values(evaluation=evaluation.model_dump(exclude_defaults=True))
        )

    for row in conn.execute(sa.select(AlleleAssessment.c.id, AlleleAssessment.c.evaluation)):
        evaluation = migrate_allele_assessment_evaluation(row.evaluation)
        conn.execute(
            AlleleAssessment.update()
            .where(AlleleAssessment.c.id == row.id)
            .values(evaluation=evaluation.model_dump(exclude_defaults=True))
        )

    for row in conn.execute(
        sa.select(ReferenceAssessment.c.id, ReferenceAssessment.c.evaluation)
    ):
        evaluation = migrate_reference_assessment_evaluation(row.evaluation)
        conn.execute(
            ReferenceAssessment.update()
            .where(ReferenceAssessment.c.id == row.id)
            .values(evaluation=evaluation.model_dump(exclude_defaults=True))
        )

    for row in conn.execute(sa.select(GeneAssessment.c.id, GeneAssessment.c.evaluation)):
        evaluation = migrate_gene_assessment_evaluation(row.evaluation)
        conn.execute(
            GeneAssessment.update()
            .where(GeneAssessment.c.id == row.id)
            .values(evaluation=evaluation.model_dump(exclude_defaults=True))
        )

    op.alter_column(
        "allelereport",
        "evaluation",
        existing_type=postgresql.JSONB(astext_type=sa.Text()),
        nullable=False,
    )
    op.alter_column(
        "alleleassessment",
        "evaluation",
        existing_type=postgresql.JSONB(astext_type=sa.Text()),
        nullable=False,
    )
    op.alter_column(
        "referenceassessment",
        "evaluation",
        existing_type=postgresql.JSONB(astext_type=sa.Text()),
        nullable=False,
    )
    op.alter_column(
        "geneassessment",
        "evaluation",
        existing_type=postgresql.JSONB(astext_type=sa.Text()),
        nullable=False,
    )


def downgrade():
    raise NotImplementedError("Downgrade not supported")
