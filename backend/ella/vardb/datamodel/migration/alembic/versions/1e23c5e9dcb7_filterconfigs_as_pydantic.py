"""FilterConfigs as pydantic

Revision ID: 1e23c5e9dcb7
Revises: 11ec14c56ae2
Create Date: 2024-03-20 07:19:11.244875

"""

# revision identifiers, used by Alembic.
revision = '1e23c5e9dcb7'
down_revision = '11ec14c56ae2'
branch_labels = None
depends_on = None

import json
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql


def migrate_frequency(frequencyconfig):
    """Migrate frequency filter config

    Ensure that groups and thresholds are defined
    """
    if not frequencyconfig.get("groups"):
        frequencyconfig["groups"] = {"external": {}, "internal": {}}
    if not frequencyconfig.get("thresholds"):
        frequencyconfig["thresholds"] = {"AD": {}, "default": {}}
    if not frequencyconfig.get("num_thresholds"):
        frequencyconfig["num_thresholds"] = {}
    return frequencyconfig


def migrate_callertype(callertypeconfig):
    """Migrate callertype filter config

    Change uppercase callertype to lowercase
    """
    if "callerTypes" in callertypeconfig:
        callertypeconfig["caller_types"] = callertypeconfig.pop("callerTypes")
    callertypeconfig["caller_types"] = [x.lower() for x in callertypeconfig["caller_types"]]
    return callertypeconfig


def migrate_quality(qualityconfig):
    """Migrate quality filter config

    Rename qual to quality
    """
    if "qual" in qualityconfig:
        qualityconfig["quality"] = qualityconfig.pop("qual")
    return qualityconfig


def migrate_classification(classificationconfig):
    """Migrate classification filter config

    Replace U with NP
    """
    if not classificationconfig.get("classes"):
        classificationconfig["classes"] = []
    for i, clazz in enumerate(classificationconfig["classes"]):
        if clazz == "U":
            classificationconfig["classes"][i] = "NP"
    return classificationconfig


def migrate_filter(filter):
    """Migrate filter config to fit with pydantic model - migrate exceptions recursively"""
    if filter["name"] == "frequency":
        filter["config"] = migrate_frequency(filter["config"])
    elif filter["name"] == "callertype":
        filter["config"] = migrate_callertype(filter["config"])
    elif filter["name"] == "classification":
        filter["config"] = migrate_classification(filter["config"])
    elif filter["name"] == "quality":
        filter["config"] = migrate_quality(filter["config"])
    exceptions = filter.get("exceptions")
    if exceptions:
        migrated_exceptions = []
        for exception in exceptions:
            migrated_exceptions.append(migrate_filter(exception))
        filter["exceptions"] = migrated_exceptions
    return filter

def upgrade():
    conn = op.get_bind()
    op.execute("DROP TRIGGER IF EXISTS filterconfig_schema_version ON filterconfig")
    op.execute("DROP FUNCTION IF EXISTS filterconfig_schema_version_function CASCADE")
    op.drop_column('filterconfig', 'schema_version')

    FilterConfig = sa.table(
        "filterconfig",
        sa.column("id", sa.Integer()),
        sa.column("filterconfig", postgresql.JSONB()),
    )

    for id, filterconfigs in conn.execute(
        sa.select(FilterConfig.c)
    ):
        for filterconfig in filterconfigs["filters"]:
            migrate_filter(filterconfig)
        op.execute(
            sa.sql.text(
                "UPDATE filterconfig SET filterconfig = :filterconfig WHERE id = :id"
            ).bindparams(
                filterconfig=json.dumps(filterconfigs), id=id
            )
        )

def downgrade():
    raise NotImplementedError("Downgrade not supported")
