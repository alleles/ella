from __future__ import annotations

from typing import TypeVar

from sqlalchemy import MetaData, select
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import DeclarativeBase, Session
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy_searchable import make_searchable

T = TypeVar("T", bound="Base")


class Base(DeclarativeBase):
    """Base class for the declarative base."""

    @classmethod
    def get_or_create(cls: type[T], session: Session, defaults=None, **kwargs) -> tuple[T, bool]:
        """
        Clone of Django's equivalent function.
        Looks up an object with the given kwargs, creating one if necessary.
        If an object is created, defaults are applied to object.
        Returns a tuple of (object, created), where created is a boolean
        specifying whether an object was created or not.

        # TODO: error when querying using '' as an integer values an_integer_field = '' is illegal
        """
        instance = session.execute(select(cls).filter_by(**kwargs)).scalars().first()
        # Facilitate mock testing by creating new object if session is mocked.
        if isinstance(instance, cls) and instance:
            return instance, False
        else:
            params = dict(kwargs)
            params.update(defaults or {})
            instance = cls(**params)
            try:
                session.add(instance)
                session.flush()
                return instance, True
            # Avoid concurrency problems, if an object
            # is inserted by another process between query and flush
            except IntegrityError as e:
                session.rollback()
                try:
                    # If there's no result, then we probably had a real integrity error
                    return session.execute(select(cls).filter_by(**kwargs)).scalar_one(), False
                except NoResultFound:
                    raise e


# Add manual naming conventions to assist consistency when
# writing migration scripts
convention = {
    "ix": "ix_%(column_0_label)s",
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s",
}

Base.metadata = MetaData(naming_convention=convention)
make_searchable(Base.metadata)  # Create triggers to keep search vectors up to date

# Don't remove!
from ella.vardb.datamodel import (
    allele,
    annotation,
    annotationjob,
    annotationshadow,
    assessment,
    attachment,
    broadcast,
    gene,
    genotype,
    jsonschema,
    log,
    sample,
    user,
    workflow,
)
