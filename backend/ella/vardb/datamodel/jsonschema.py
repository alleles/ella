from typing import override

from sqlalchemy import Index
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import Mapped, mapped_column

from ella.vardb.datamodel import Base
from ella.vardb.util.mutjson import JSONMutableDict


class JSONSchema(Base):
    """Table for all JSON schemas used in the application"""

    __tablename__ = "jsonschema"

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]
    version: Mapped[int]
    schema: Mapped[dict] = mapped_column(JSONMutableDict.as_mutable(JSONB))

    __table_args__ = (Index("ix_jsonschema_unique", "name", "version", unique=True),)

    @override
    def __repr__(self):
        return f"<JSONSchema('{self.name}', '{self.version}')>"
