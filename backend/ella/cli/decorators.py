import getpass
import logging
import sys
from functools import update_wrapper

import click

from ella.vardb.datamodel import log
from ella.vardb.util import DB


class CliLogger:
    def __init__(self, ctx: click.Context):
        self.ctx = ctx
        self.echoed: list[str] = []
        self.reason = None
        self.log = log.CliLog()
        # Check username here, before command is executed to prevent
        # action being performed without username in log
        user = getpass.getuser()
        if not user:
            raise RuntimeError(
                "Couldn't find your system username. A username is required for auditing purposes."
            )
        self.log.user = user

    def echo(self, message, db_only=False):
        self.echoed.append(str(message))
        if not db_only:
            click.echo(message)

    def exception(self, message):
        logging.exception(message)

    def commit(self):
        group = ""
        if self.ctx.parent and self.ctx.parent.command.name:
            group = self.ctx.parent.command.name
        self.log.group = group
        assert self.ctx.command.name
        self.log.groupcommand = self.ctx.command.name
        self.log.output = "\n".join(self.echoed)
        self.log.command = " ".join(sys.argv[1:])
        self.log.reason = self.reason

        db = DB()
        db.connect()
        session = db.session
        session.add(self.log)
        session.commit()
        db.disconnect()


def cli_logger(prompt_reason=False):
    def wrapped(f):
        """Decorator for logging cli commands to database."""

        def new_func(*args, **kwargs):
            ctx = click.get_current_context()
            if not getattr(ctx, "clilogger", None):
                ctx.clilogger = CliLogger(ctx)  # type: ignore[attr-defined]
                if prompt_reason:
                    reason = click.prompt("Enter reason (optional):", default="None")
                    if reason != "None":
                        ctx.clilogger.reason = reason  # type: ignore[attr-defined]
            try:
                result = f(ctx.clilogger, *args, **kwargs)  # type: ignore[attr-defined]
                return result
            except Exception as e:
                ctx.clilogger.echoed.append(str(e))  # type: ignore[attr-defined]
                raise
            finally:
                ctx.clilogger.commit()  # type: ignore[attr-defined]

        return update_wrapper(new_func, f)

    return wrapped


def session(f):
    """Decorator providing a database session."""

    def new_func(*args, **kwargs):
        ctx = click.get_current_context()
        if not getattr(ctx, "session", None):
            db = DB()
            db.connect()
            ctx.db = db  # type: ignore[attr-defined]
            ctx.session = db.session  # type: ignore[attr-defined]
        try:
            return f(ctx.session, *args, **kwargs)  # type: ignore[attr-defined]
        finally:
            ctx.db.disconnect()  # type: ignore[attr-defined]

    return update_wrapper(new_func, f)
