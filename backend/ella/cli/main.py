#!/usr/bin/env python
"""
Ella command line interface
"""
import os
from pathlib import Path

import click

from ella.api import DEVELOPMENT_MODE
from ella.cli.commands.annotationconfig.annotationconfig import annotationconfig
from ella.cli.commands.broadcast.broadcast import broadcast
from ella.cli.commands.database.database import database
from ella.cli.commands.delete.delete import delete
from ella.cli.commands.deposit.deposit import deposit
from ella.cli.commands.dev.dev import dev
from ella.cli.commands.filterconfigs.filterconfigs import filterconfigs
from ella.cli.commands.references.references import references
from ella.cli.commands.users.users import users

SCRIPT_DIR = Path(__file__).parent.absolute()


@click.command("igv-download", help="Download IGV.js data")
@click.argument("target")
def download_igv(target):
    os.system(f"{SCRIPT_DIR / 'commands' / 'fetch-igv-data.sh'} {target}")


@click.group()
def cli_group():
    pass


cli_group.add_command(broadcast)
cli_group.add_command(database)
cli_group.add_command(deposit)
cli_group.add_command(delete)
cli_group.add_command(download_igv)
cli_group.add_command(references)
cli_group.add_command(users)
cli_group.add_command(filterconfigs)
cli_group.add_command(annotationconfig)
if DEVELOPMENT_MODE:
    cli_group.add_command(dev)


def cli():
    from ella.applogger import setup_logger

    setup_logger()
    cli_group(prog_name="ella-cli")


if __name__ == "__main__":
    cli()
