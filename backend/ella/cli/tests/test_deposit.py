import os
from collections.abc import Callable
from pathlib import Path
from typing import Any

from click.testing import Result
from sqlalchemy import func, select, text, tuple_
from sqlalchemy.orm.session import Session

from ella.cli.tests.conftest import assert_result
from ella.vardb.datamodel import assessment, gene, sample, user
from ella.vardb.util.testdatabase import TestDatabase

TESTDATA = Path(os.environ["TESTDATA"])
VCF = TESTDATA / "analyses/default/brca_sample_3.HBOCUTV_v1.0.0/brca_sample_3.HBOCUTV_v1.0.0.vcf.gz"


def test_deposit_analysis(session: Session, run_command: Callable[..., Any]):
    # Check that analysis does not exist
    analysis_name = VCF.name[: str(VCF.name).index(".vcf")]
    assert (
        session.execute(
            select(func.count(sample.Analysis.id)).where(sample.Analysis.name == analysis_name)
        ).scalar_one()
        == 0
    )

    result = run_command(["deposit", "analysis", str(VCF.parent)])
    assert_result(result)
    session.execute(select(sample.Analysis).where(sample.Analysis.name == analysis_name)).one()


def test_deposit_alleles(run_command: Callable[..., Any]):
    result = run_command(["deposit", "alleles", str(VCF)])
    assert_result(result)


def test_deposit_annotation(run_command: Callable[..., Any]):
    result = run_command(["deposit", "annotation", str(VCF)])
    assert_result(result)


def test_deposit_references(session: Session, run_command: Callable[..., Any]):
    session.execute(text("TRUNCATE TABLE reference CASCADE"))
    session.commit()

    assert not session.execute(func.count(assessment.Reference.id)).scalar_one()
    references = TESTDATA / "fixtures/references.json"
    num_references_in_file = len(references.open().readlines())
    assert num_references_in_file > 0
    result = run_command(["deposit", "references", references.as_posix()])
    assert_result(result)
    assert (
        session.execute(select(func.count(assessment.Reference.id))).scalar_one()
        == num_references_in_file
    )


def test_deposit_genepanel(session: Session, run_command: Callable[..., Result]):
    session.execute(text("TRUNCATE TABLE genepanel CASCADE"))
    session.commit()

    genepanel_name = "Ciliopati"
    genepanel_version = "v6.0.0"

    # Check that gene panel does not exist
    assert (
        session.execute(
            select(gene.Genepanel).where(
                tuple_(gene.Genepanel.name, gene.Genepanel.version)
                == (genepanel_name, genepanel_version)
            )
        ).one_or_none()
        is None
    )

    genepanel_folder = TESTDATA / f"clinicalGenePanels/{genepanel_name}_{genepanel_version}"
    result = run_command(["deposit", "genepanel", "--folder", genepanel_folder])
    assert_result(result)
    # Check that gene panel exists
    session.execute(
        select(gene.Genepanel).where(
            tuple_(gene.Genepanel.name, gene.Genepanel.version)
            == (genepanel_name, genepanel_version)
        )
    ).one()


def test_append_genepanel_to_usergroup(
    session: Session, test_database: TestDatabase, run_command: Callable[..., Any]
):
    test_database.refresh()
    genepanel_name = "Ciliopati"
    genepanel_version = "v6.0.0"
    usergroup = "testgroup01"

    ug = session.execute(
        select(user.UserGroup).where(user.UserGroup.name == usergroup)
    ).scalar_one()
    assert (genepanel_name, genepanel_version) not in [
        (gp.name, gp.version) for gp in ug.genepanels
    ]

    result = run_command(
        ["deposit", "append_genepanel_to_usergroup", genepanel_name, genepanel_version, usergroup]
    )
    assert_result(result)

    session.refresh(ug)

    assert (genepanel_name, genepanel_version) in [(gp.name, gp.version) for gp in ug.genepanels]
