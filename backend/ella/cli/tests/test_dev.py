import tempfile
from collections.abc import Callable
from pathlib import Path

from sqlalchemy import text

from ella.cli.tests.conftest import assert_result
from ella.vardb.datamodel.sample import Analysis
from ella.vardb.util.db import DB


def test_db_dump_and_restore(run_command: Callable):
    db = DB()
    with tempfile.TemporaryDirectory() as tmpdir:
        dumpfile = Path(tmpdir) / "dumpfile"
        result = run_command(["dev", "dump", str(dumpfile)])
        assert_result(result)
        assert dumpfile.exists()

        # Make some changes to the database, and disconnect
        db.connect()
        db.session.execute(text("UPDATE analysis SET report='test'"))
        db.session.commit()
        assert db.session.query(Analysis).where(Analysis.report == "test").count() > 0
        db.disconnect()

        # Restore the database
        result = run_command(["dev", "restore", str(dumpfile)])

        # Check that the changes are reverted
        db.connect()
        assert db.session.query(Analysis).where(Analysis.report == "test").count() == 0
