from collections.abc import Callable

from click.testing import Result
from sqlalchemy import func, select, text
from sqlalchemy.orm import Session

from ella.cli.tests.conftest import assert_result
from ella.vardb.datamodel import annotationshadow


def test_database_refresh(session: Session, run_command: Callable[..., Result]):
    session.execute(text("DROP TABLE IF EXISTS annotationshadowtranscript"))
    session.execute(text("DROP TABLE IF EXISTS annotationshadowfrequency"))

    session.commit()

    result = run_command(["database", "refresh", "-f"])
    assert_result(result)

    assert (
        session.execute(
            select(func.count(annotationshadow.AnnotationShadowFrequency.id))
        ).scalar_one()
        > 0
    )
    assert (
        session.execute(
            select(func.count(annotationshadow.AnnotationShadowTranscript.id))
        ).scalar_one()
        > 0
    )
