import pytest

# setattr(DB, "__del__", db_del)
from click.testing import CliRunner, Result

from ella.cli.main import cli_group
from ella.vardb.util import DB


# Sessions/connections are not closed after cli-call. Add this explicit disconnect here to remedy this.
def db_del(*args, **kwargs):
    try:
        args[0].disconnect()
    except:
        pass


setattr(DB, "__del__", db_del)


def assert_result(result: Result):
    if result.exit_code != 0:
        assert result.exception
        raise result.exception


@pytest.fixture
def run_command():
    runner = CliRunner()
    return lambda *args, **kwargs: runner.invoke(cli_group, *args, **kwargs)
