import shutil
import subprocess


def test_cli_in_path():
    # Check that ella-cli is available in the PATH, and that we can execute it
    assert shutil.which("ella-cli") is not None
    subprocess.check_call(["ella-cli", "--help"])
