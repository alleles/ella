from collections.abc import Callable
from typing import Literal

import pytest
from click.testing import Result
from sqlalchemy import select
from sqlalchemy.orm.session import Session

from ella.cli.tests.conftest import assert_result
from ella.vardb.datamodel import sample
from ella.vardb.util.testdatabase import TestDatabase

REFRESHED = False


@pytest.mark.parametrize("analysis_id", [1, 2, 3, 4])
def test_analysis_delete(
    session: Session,
    test_database: TestDatabase,
    run_command: Callable[..., Result],
    analysis_id: Literal[1, 2, 3, 4],
):
    # Need to refresh once for this test
    global REFRESHED
    if not REFRESHED:
        test_database.refresh()
        REFRESHED = True

    result = run_command(["delete", "analysis", str(analysis_id)], input="Some reason\ny\n")
    assert_result(result)
    a = session.execute(
        select(sample.Analysis).where(sample.Analysis.id == analysis_id)
    ).one_or_none()

    assert a is None
