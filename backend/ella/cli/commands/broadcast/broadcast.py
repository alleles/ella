import click
from sqlalchemy import select
from sqlalchemy.orm import Session

from ella.cli.decorators import CliLogger, cli_logger, session
from ella.vardb.datamodel import broadcast as broadcast_model


@click.group(help="Broadcast")
def broadcast():
    pass


@broadcast.command("list", help="List all active messages")
@click.option("--all", is_flag=True, default=False, help="List all messages")
@click.option("--tail", is_flag=True, default=False, help="List last 10 messages")
@session
def cmd_list_active(session: Session, all: bool, tail: bool):
    """
    Print all active broadcast messages to console
    """

    if tail:
        all = True

    filters = []
    if not all:
        filters.append(broadcast_model.Broadcast.active.is_(True))
    message_query = select(broadcast_model.Broadcast)

    if filters:
        message_query = message_query.where(*filters)

    if not filters and tail:
        message_query = message_query.order_by(broadcast_model.Broadcast.date_created.desc()).limit(
            10
        )

    messages = list(session.execute(message_query).scalars().all())

    if tail:
        messages.reverse()

    row_format = " {id:^5} | {date_created:^33} | {active:<10} | {message:<80} |"

    if messages:
        click.echo(
            row_format.format(id="id", date_created="date", active="active", message="message")
        )
        click.echo(
            row_format.format(id="-" * 5, date_created="-" * 33, active="-" * 10, message="-" * 80)
        )
        for msg in messages:
            click.echo(
                row_format.format(
                    id=msg.id,
                    date_created=msg.date_created.isoformat(),
                    active="true" if msg.active else "false",
                    message=msg.message,
                )
            )
    else:
        click.echo("No messages")


@broadcast.command("new", help="Create new message. Activated immediately.")
@click.argument("message", nargs=-1, type=click.UNPROCESSED)
@session
@cli_logger()
def cmd_new_message(logger: CliLogger, session: Session, message: list[str]):
    parsed_message = " ".join(message)

    if not parsed_message:
        logger.echo("Message empty")
        return

    new_message = broadcast_model.Broadcast(message=parsed_message, active=True)
    session.add(new_message)
    session.commit()

    logger.echo(f"Message with id {new_message.id} added")


@broadcast.command("deactivate", help="Deactivate a message.")
@click.argument("message_id", type=click.INT)
@session
@cli_logger()
def cmd_deactivate_message(logger: CliLogger, session: Session, message_id: int):
    message = session.execute(
        select(broadcast_model.Broadcast).where(broadcast_model.Broadcast.id == message_id)
    ).scalar_one_or_none()

    if not message:
        logger.echo(f"Found no message with id {message_id}")
        return

    message.active = False
    session.commit()
    logger.echo(f"Message with id {message.id} set as inactive")
