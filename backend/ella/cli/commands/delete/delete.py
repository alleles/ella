from datetime import datetime

import click
from sqlalchemy import delete as sa_delete
from sqlalchemy import func, select
from sqlalchemy.orm import Session

from ella.api.schemas.alleleinterpretations import AlleleInterpretationOverviewSchema
from ella.cli.decorators import CliLogger, cli_logger, session
from ella.datalayer import filters
from ella.datalayer.queries import annotation_transcripts_genepanel
from ella.vardb.datamodel import allele, sample, workflow


def delete_analysis(session: Session, analysis_id: int):
    session.execute(sa_delete(sample.Analysis).where(sample.Analysis.id == analysis_id))


@click.group(help="Delete actions")
def delete():
    pass


@delete.command("analysis")
@click.argument("analysis_id", type=int)
@session
@cli_logger(prompt_reason=True)
def cmd_analysis_delete(logger: CliLogger, session: Session, analysis_id: int):
    """
    Deletes an analysis, removing it's samples and genotypes
    in the process. Any alleles that were imported as part of the
    analysis are kept, as we cannot know which alleles that
    that only belongs to the analysis and which alleles that
    were also imported by other means.

    Does not delete any allele assessments.
    """

    analysis_name = session.execute(
        select(sample.Analysis.name).where(sample.Analysis.id == analysis_id)
    ).scalar_one()

    review_comment = session.execute(
        select(workflow.InterpretationLog.review_comment)
        .join(workflow.AnalysisInterpretation)
        .where(workflow.AnalysisInterpretation.analysis_id == analysis_id)
        .where(~workflow.InterpretationLog.review_comment.is_(None))
        .order_by(workflow.InterpretationLog.date_created.desc())
        .limit(1)
    ).scalar_one_or_none()

    if review_comment:
        overview_comment = f"overview comment '{review_comment}'"
    else:
        overview_comment = "no overview comment"

    workflow_status = (
        session.execute(
            select(
                workflow.AnalysisInterpretation.status,
                workflow.AnalysisInterpretation.workflow_status,
            )
            .where(workflow.AnalysisInterpretation.analysis_id == analysis_id)
            .order_by(workflow.AnalysisInterpretation.id.desc())
            .limit(1)
        )
        .tuples()
        .one()
    )

    answer = input(
        f"Are you sure you want to delete analysis {analysis_name} with {overview_comment} in workflow status:"
        f"{'{} ({})'.format(*workflow_status)}\n"
        "Type 'y' to confirm.\n"
    )

    if answer == "y":
        try:
            delete_analysis(session, analysis_id)
            session.commit()
            logger.echo(f"Analysis {analysis_id} ({analysis_name}) deleted successfully")
        except Exception:
            logger.exception(f"Something went wrong while deleting analysis {analysis_id}")
    else:
        logger.echo("Lacking confirmation, aborting...")


@delete.command("alleleinterpretation")
@click.argument("allele_id", type=int)
@click.option(
    "--delete-all",
    is_flag=True,
    help="Delete all allele interpretations, not just since last finalize",
)
@session
@cli_logger(prompt_reason=True)
def cmd_alleleinterpretation_delete(
    logger: CliLogger, session: Session, allele_id: int, delete_all: bool
):
    """
    Delete allele interpretations for a given allele_id. If --delete-all flag is set, then all
    allele interpretations are deleted. If not, then only allele interpretations since the allele
    was last finalized.

    Does not delete any allele assessments.
    """
    since_last_finalized = not delete_all

    # Find all allele interpretations to be deleted
    query = select(workflow.AlleleInterpretation).where(
        workflow.AlleleInterpretation.allele_id == allele_id
    )
    total_count = session.execute(select(func.count()).select_from(query.subquery())).scalar_one()
    if since_last_finalized:
        last_finalized = session.execute(
            query.where(workflow.AlleleInterpretation.finalized.is_(True))
            .order_by(workflow.AlleleInterpretation.date_created.desc())
            .limit(1)
        ).scalar_one_or_none()
        if last_finalized:
            query = query.where(
                workflow.AlleleInterpretation.date_created > last_finalized.date_created
            )

    delete_count = session.execute(select(func.count()).select_from(query.subquery())).scalar_one()
    if delete_count == 0:
        logger.echo("Nothing to delete!")
        return
    # Logic to print out info to the user on what is to be deleted
    start_count = total_count - delete_count

    genepanel = session.execute(query.limit(1)).scalar_one().genepanel
    genepanel_annotation = annotation_transcripts_genepanel(
        session, [(genepanel.name, genepanel.version)], allele_ids=[allele_id]
    ).subquery()

    annotations = (
        session.execute(
            select(
                genepanel_annotation.c.annotation_transcript,
                genepanel_annotation.c.annotation_symbol,
                genepanel_annotation.c.annotation_hgvsc,
            )
        )
        .tuples()
        .all()
    )

    allele_obj = session.execute(
        select(allele.Allele).where(allele.Allele.id == allele_id)
    ).scalar_one()
    logger.echo("Ready to delete interpretations for allele:")
    logger.echo(str(allele_obj))
    if annotations:
        for tx, symbol, hgvsc in annotations:
            logger.echo(f"{tx}({symbol}):{hgvsc}")
    else:
        logger.echo("(No available HGVSc annotations)")

    review_comment = session.execute(
        select(workflow.InterpretationLog.review_comment)
        .join(workflow.AlleleInterpretation)
        .where(workflow.AlleleInterpretation.allele_id == allele_id)
        .where(~workflow.InterpretationLog.review_comment.is_(None))
        .order_by(workflow.InterpretationLog.date_created.desc())
        .limit(1)
    ).one_or_none()
    if review_comment:
        overview_comment = f"Overview comment '{review_comment[0]}'"
    else:
        overview_comment = "No overview comment"

    logger.echo("\n" + overview_comment + "\n")

    logger.echo("\nInterpretations to delete:")
    interpretations_to_delete = session.execute(query).scalars().all()
    dumped_interpretations = AlleleInterpretationOverviewSchema().dump(
        interpretations_to_delete, many=True
    )[0]
    for i, interpretation in enumerate(dumped_interpretations):
        s = f"{start_count + i+1} - {interpretation['workflow_status']}"
        if interpretation["status"] == "Ongoing":
            s += " (Ongoing)"
        elif interpretation["finalized"]:
            s += " (Finalized)"

        if interpretation.get("user"):
            s += f" - {interpretation.get('user').get('abbrev_name')} - {datetime.strftime(datetime.fromisoformat(interpretation['date_last_update']), '%Y-%m-%d %H:%M')}"
        logger.echo(s)

    # Confirm deletion
    answer = input(
        "Are you sure you want to delete these alleleinterpretations? \nType 'y' to confirm.\n"
    )

    if answer == "y":
        try:
            with session.no_autoflush:
                session.execute(
                    sa_delete(workflow.AlleleInterpretation).where(
                        filters.in_(
                            workflow.AlleleInterpretation.id,
                            [i.id for i in interpretations_to_delete],
                        )
                    )
                )
            session.commit()
            logger.echo(f"Interepretation(s) for allele {allele_id} deleted successfully")
        except Exception:
            logger.exception(f"Something went wrong while deleting allele {allele_id}")
    else:
        logger.echo("Lacking confirmation, aborting...")
