import json
from logging import Logger
from typing import Any

import click
import yaml
from sqlalchemy import select
from sqlalchemy.orm import Session

from ella.cli.decorators import cli_logger, session
from ella.vardb.datamodel import annotation
from ella.vardb.deposit.annotation_config import deposit_annotationconfig


@click.group(help="Annotation config management")
def annotationconfig():
    # noop
    ...


@annotationconfig.command("update")
@click.argument("annotationconfig", type=click.File("r"))
@session
@cli_logger(prompt_reason=True)
def cmd_update_annotationconfig(logger: Logger, session: Session, annotationconfig: str):
    """
    Updates annotationconfigs from the input YAML file.
    """

    config_obj: dict[str, Any] = yaml.safe_load(annotationconfig)
    deposit_annotationconfig(session, config_obj)
    session.commit()
    print("Updated annotation config")


@annotationconfig.command("list")
@session
def list(session: Session):
    print("\nCurrent active annotationconfig:\n")

    active_annotationconfig = session.execute(
        select(annotation.AnnotationConfig).order_by(annotation.AnnotationConfig.id.desc()).limit(1)
    ).scalar_one()
    print("Deposit:\n")
    print(json.dumps(active_annotationconfig.deposit, indent=4))
    print("View:\n")
    print(json.dumps(active_annotationconfig.view, indent=4))
