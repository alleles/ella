import re
from pathlib import Path

import click
from sqlalchemy import select
from sqlalchemy.orm import Session

from ella.cli.decorators import CliLogger, cli_logger, session
from ella.vardb.datamodel import gene, user
from ella.vardb.deposit.analysis_config import AnalysisConfigData
from ella.vardb.deposit.deposit_alleles import DepositAlleles
from ella.vardb.deposit.deposit_analysis import DepositAnalysis
from ella.vardb.deposit.deposit_custom_annotations import import_custom_annotations
from ella.vardb.deposit.deposit_genepanel import DepositGenepanel
from ella.vardb.deposit.deposit_references import import_references

VCF_FIELDS_RE = re.compile(
    r"(?P<analysis_name>.+[.-](?P<genepanel_name>.+)[-_](?P<genepanel_version>.+))\.vcf"
)


@click.group(help="Data deposit")
def deposit():
    pass


@deposit.command("analysis")
@click.argument("file_or_folder", type=click.Path(exists=True, path_type=Path))
@session
@cli_logger()
def cmd_deposit_analysis(logger: CliLogger, session: Session, file_or_folder: Path):
    """
    Deposit an analysis given input vcf.
    File should be in format of {analysis_name}.{genepanel_name}-{genepanel_version}.vcf
    """

    da = DepositAnalysis(session)
    analysis_config_data = AnalysisConfigData(file_or_folder)
    analysis = da.import_vcf(analysis_config_data)
    session.commit()
    logger.echo(f"Analysis {analysis.name} deposited successfully")


@deposit.command("alleles")
@click.argument("vcf", nargs=-1, type=click.Path(exists=True, path_type=Path))
@click.option("--genepanel_name")
@click.option("--genepanel_version")
@session
@cli_logger()
def cmd_deposit_alleles(
    logger, session: Session, vcf: list[Path], genepanel_name: str, genepanel_version: str
):
    """
    Deposit alleles given input vcf.

    If genepanel not given by options, get it from the filename assuming
    format of {something}.{genepanel_name}_{genepanel_version}.vcf
    """
    da = DepositAlleles(session)
    for f in vcf:
        if not genepanel_name:
            matches = re.match(VCF_FIELDS_RE, f.name)
            assert matches, "Could not parse genepanel name and version from filename"
            genepanel_name = matches.group("genepanel_name")
            genepanel_version = matches.group("genepanel_version")
        da.import_vcf(f, genepanel_name, genepanel_version)
    session.commit()
    logger.echo("Deposited " + str(len(vcf)) + " files.")


@deposit.command("annotation")
@click.argument("vcf", type=click.Path(exists=True, path_type=Path))
@session
@cli_logger()
def cmd_deposit_annotation(logger: CliLogger, session: Session, vcf: Path):
    """
    Update/deposit alleles with annotation only given input vcf.
    No analysis/variant interpretation is created.
    File should be in format of {something}.{genepanel_name}_{genepanel_version}.vcf
    """
    matches = re.match(VCF_FIELDS_RE, vcf.name)
    assert matches, "Could not parse genepanel name and version from filename"
    da = DepositAlleles(session)
    da.import_vcf(
        vcf,
        matches.group("genepanel_name"),
        matches.group("genepanel_version"),
        annotation_only=True,
    )
    session.commit()
    logger.echo("Annotation imported successfully")


@deposit.command("references")
@click.argument("references_json", type=click.Path(exists=True, path_type=Path))
@session
@cli_logger()
def cmd_deposit_references(logger: CliLogger, session: Session, references_json: Path):
    """
    Deposit/update a set of references into database given by DB_URL.

    Input is a line separated JSON file, with one reference object per line.
    """
    import_references(session, references_json)
    logger.echo("References imported successfully")


@deposit.command("custom_annotation")
@click.argument("custom_annotation_json", type=click.Path(exists=True, path_type=Path))
@session
@cli_logger()
def cmd_deposit_custom_annotations(
    logger: CliLogger, session: Session, custom_annotation_json: Path
):
    """
    Deposit/update a set of custom annotations into database given by DB_URL.

    Input is a line separated JSON file, with one custom annotation object per line.
    """
    import_custom_annotations(session, custom_annotation_json)
    logger.echo("Custom annotation imported successfully")


@deposit.command("genepanel")
@click.option("--genepanel_name")
@click.option("--genepanel_version")
@click.option("--transcripts_path", type=click.Path(exists=True, path_type=Path))
@click.option("--phenotypes_path", type=click.Path(exists=True, path_type=Path))
@click.option("--replace", is_flag=True)
@click.option(
    "--folder",
    help="Folder to look for files assuming standard filenames",
    type=click.Path(exists=True, path_type=Path),
)
@session
@cli_logger()
def cmd_deposit_genepanel(
    logger: CliLogger,
    session: Session,
    genepanel_name: str | None,
    genepanel_version: str | None,
    transcripts_path: Path | None,
    phenotypes_path: Path | None,
    replace: bool,
    folder: Path | None,
):
    """
    Create or replace genepanel. If replacing genepanel, use --replace flag.
    """
    if folder:
        prefix = folder.parts[-1]
        transcripts_path = folder / (prefix + "_genes_transcripts.tsv")
        phenotypes_path = folder / (prefix + "_phenotypes.tsv")
        genepanel_name, genepanel_version = prefix.split("_", 1)
        assert genepanel_version.startswith("v")
    else:
        assert genepanel_name and genepanel_version and transcripts_path and phenotypes_path

    dg = DepositGenepanel(session)
    dg.add_genepanel(
        transcripts_path, phenotypes_path, genepanel_name, genepanel_version, replace=replace
    )
    logger.echo(f"Genepanel {genepanel_name} {genepanel_version} deposited successfully")


@deposit.command("append_genepanel_to_usergroup")
@click.argument("genepanel_name", required=True)
@click.argument("genepanel_version", required=True)
@click.argument("user_group_name", required=True)
@session
@cli_logger()
def cmd_append_genepanel_to_usergroup(
    logger: CliLogger,
    session: Session,
    genepanel_name: str,
    genepanel_version: str,
    user_group_name: str,
):
    """
    Append a genepanel to the given user group.
    :param genepanel_name:
    :param genepanel_version:
    :param user_group_name:
    :return:
    """
    user_group = session.execute(
        select(user.UserGroup).where(user.UserGroup.name == user_group_name)
    ).scalar_one()

    gp = session.execute(
        select(gene.Genepanel).where(
            gene.Genepanel.name == genepanel_name, gene.Genepanel.version == genepanel_version
        )
    ).scalar_one()

    if gp in user_group.genepanels:
        logger.echo(
            f"Genepanel ({genepanel_name},{genepanel_version}) already exists in user group {user_group_name}"
        )
        return

    user_group.genepanels.append(gp)

    session.commit()

    ug = session.execute(
        select(user.UserGroup).where(user.UserGroup.name == user_group_name)
    ).scalar_one()
    logger.echo(str(ug.genepanels))

    logger.echo(
        f"Appended genepanel ({genepanel_name},{genepanel_version}) to user group {user_group_name}"
    )
