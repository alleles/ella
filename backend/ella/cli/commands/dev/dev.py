import json
import os
import subprocess
from collections.abc import Generator
from io import BufferedReader
from pathlib import Path

import click
import requests


@click.group(help="Development commands")
def dev():
    ...


def dump_database() -> Generator[bytes, None, None]:
    args = ["pg_dump", os.environ["DB_URL"], "-Fc"]
    process = subprocess.Popen(args, stdout=subprocess.PIPE)
    assert process.stdout is not None
    while process.poll() is None:
        yield process.stdout.read(2**19)  # 512kb chunks
    assert process.returncode == 0, f"pg_dump failed with return code {process.returncode}"


def restore_database(buffer: BufferedReader) -> None:
    args = ["pg_restore", "-d", os.environ["DB_URL"], "-c"]
    process = subprocess.Popen(args, stdin=subprocess.PIPE)
    assert process.stdin is not None
    while chunk := buffer.read(2**19):  # 512kb chunks
        process.stdin.write(chunk)
        process.stdin.flush()
    process.stdin.close()
    process.communicate()
    assert process.returncode == 0, f"pg_restore failed with return code {process.returncode}"


@dev.command(
    help="Reset database with given testset (force overwrite) - default values are set in the API container, all values are optional"
)
@click.option("--testset", type=str, help="Testset to reset")
@click.option(
    "--database",
    type=str,
    help="Optionally specify database to reset",
)
@click.option(
    "--migration",
    is_flag=True,
    help="Whether to run migrations or not",
)
@click.option(
    "--clean",
    is_flag=True,
    help="Whether to clean template database or not - used when changes are introduced in the testdata",
)
@click.option("--no-overwrite", is_flag=True, help="Whether to overwrite existing database or not")
def reset_database(testset, database, migration, clean, no_overwrite):
    params = {
        "testset": testset,
        "database": database,
        "migration": migration,
        "clean": clean,
        "overwrite": not no_overwrite,
    }
    params = {k: v for k, v in params.items() if v is not None}
    r = requests.get("http://testdata:23232/reset", params=params)
    assert r.status_code == 200
    print(json.dumps(r.json(), indent=2))
    print("Database reset")


@dev.command(
    "dump",
    help="Dump the database to a file",
)
@click.argument(
    "filename", type=click.Path(file_okay=False, dir_okay=False, exists=False, path_type=Path)
)
def dump(filename: Path):
    with filename.open("wb") as f:
        for chunk in dump_database():
            f.write(chunk)
    click.echo("Database dumped")


@dev.command(
    "restore",
    help="Restore the database from a file",
)
@click.argument(
    "filename", type=click.Path(file_okay=True, dir_okay=False, exists=True, path_type=Path)
)
def restore(filename: Path):
    with filename.open("rb") as f:
        restore_database(f)
    click.echo("Database restored")
