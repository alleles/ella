#!/usr/bin/env python
"""
Script for creating all tables in a vardb database.
"""

from ella.api.config import config
from ella.vardb.datamodel import *  # noqa: F403
from ella.vardb.datamodel.annotationshadow import create_shadow_tables, create_tmp_shadow_tables
from ella.vardb.datamodel.jsonschemas.update_schemas import update_schemas


def refresh_tmp(db):
    # Although the annotationshadow tables were created above in create_all()
    # they have extra logic with triggers on dynamic fields, so we need to (re)create them
    create_tmp_shadow_tables(db.session, config)


def refresh(db, use_prepared_tmp_tables=False):
    # Add json schemas to table
    update_schemas(db.session)

    create_shadow_tables(db.session, config, use_prepared_tmp_tables=use_prepared_tmp_tables)
