#!/usr/bin/env python
"""
Script for dropping all tables in a vardb database.
"""
import os
from contextlib import contextmanager

import click
import psycopg2
from sqlalchemy import text
from sqlalchemy.orm import Session

from ella.cli.decorators import cli_logger, session
from ella.vardb.util import DB

from .migration_db import (
    make_migration_base_db,
    migration_compare,
    migration_current,
    migration_downgrade,
    migration_history,
    migration_upgrade,
)
from .refresh_db import refresh, refresh_tmp

DEFAULT_WARNING = (
    f"THIS WILL WIPE OUT {os.environ.get('DB_URL')} COMPLETELY! \n"
    "Are you sure you want to proceed? Type 'CONFIRM' to confirm.\n"
)


@contextmanager
def confirm(echo_func, success_msg, force=False, warning=DEFAULT_WARNING):
    if not force:
        confirmation = input(warning)
        if confirmation == "CONFIRM":
            yield
            echo_func(success_msg)
        else:
            echo_func("Lacking confirmation, aborting...")
            raise RuntimeError("Lacking confirmation, aborting...")
    else:
        yield
        echo_func(success_msg)


@click.group(help="Database management (create/drop/migrate/etc)")
def database():
    pass


@database.command("refresh", help="Refresh shadow tables in database.")
@click.option("-f", is_flag=True, help="Do not ask for confirmation.")
@cli_logger()
def cmd_refresh(logger, f=None):
    db = DB()
    db.connect()
    logger.echo(
        "Creating temporary shadow tables and (re)creating triggers. This can take some time..."
    )

    refresh_tmp(db)
    logger.echo("Done!")
    warning = "Ready to replace existing shadow tables. Make sure ELLA is not running before continuing.\nType 'CONFIRM' to confirm.\n"
    with confirm(
        logger.echo,
        "Shadow tables should now have been refreshed. ELLA can now be restarted again",
        force=f,
        warning=warning,
    ):
        logger.echo("Replacing existing tables with created temporary tables")
        refresh(db, use_prepared_tmp_tables=True)
        db.session.commit()
        ast_count = list(
            db.session.execute(text("SELECT COUNT(*) FROM annotationshadowtranscript"))
        )[0][0]
        asf_count = list(
            db.session.execute(text("SELECT COUNT(*) FROM annotationshadowfrequency"))
        )[0][0]
        db.disconnect()

        conn = psycopg2.connect(os.environ["DB_URL"])
        conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
        conn.cursor().execute("VACUUM(ANALYZE)")

        logger.echo(f"AnnotationShadowTranscript count: {ast_count}")
        logger.echo(f"AnnotationShadowFrequency count: {asf_count}")


@database.command(
    "make-migration-base",
    help="Creates MIGRATION BASE tables in database.",
    short_help="Create database from migration base",
)
@click.option("-f", is_flag=True, help="Do not ask for confirmation.")
def cmd_make_migration_base(f=None):
    with confirm(click.echo, "Tables should now have been created.", force=f):
        make_migration_base_db()


@database.command(
    "upgrade",
    help="Upgrade database from current to a specific revision.",
    short_help="Upgrade to version",
)
@click.argument("revision")
@cli_logger()
def cmd_upgrade(logger, revision):
    migration_upgrade(revision)


@database.command(
    "downgrade",
    help="Downgrade database from current to a specific revision.",
    short_help="Downgrade to version",
)
@click.argument("revision")
@cli_logger()
def cmd_downgrade(logger, revision):
    migration_downgrade(revision)


@database.command("current", help="Show revision of current migration.", short_help="Show current")
def cmd_current():
    click.echo(migration_current())


@database.command(
    "history", help="Show all migrations that have been applied", short_help="Show all migrations"
)
@click.option("-v", is_flag=True, help="Verbose: more details.")
@click.option("--range", help="Revision range")
def cmd_history(v, range):
    migration_history(range=range, verbose=v)


@database.command(
    "compare",
    help="Compares database HEAD revision to migration script HEAD revision. Error on mismatch.",
    short_help="Compare current DB and script revisions.",
)
def cmd_migration_compare():
    migration_compare()


@database.command(
    "make-production",
    help="Initializes an empty database for production.",
    short_help="Create production db",
)
@click.option("-f", is_flag=True, help="Do not ask for confirmation.")
def cmd_make_production(f=None):
    db = DB()
    db.connect()
    table_count = list(
        db.session.execute(
            text("select count(*) from information_schema.tables where table_schema = 'public'")
        )
    )[0][0]
    if table_count == 0:
        click.echo("Database is empty, creating tables...")
        make_migration_base_db()
    click.echo("Migrating to latest version")
    migration_upgrade("head")
    db.session.commit()
    click.echo("All done!")


@database.command(
    "create",
    help=f"Create database {os.environ.get("DB_URL", "$DB_URL")} if it does not exist",
    short_help="Create database",
)
@click.option(
    "-m", "--maintenance-db", default="template1", help="Template to use for database creation"
)
@click.option(
    "-f", is_flag=True, help="Do not ask for confirmation - will not drop existing database."
)
def cmd_create(maintenance_db, f=None):
    db_url = os.environ["DB_URL"]
    try:
        conn = psycopg2.connect(db_url)
        click.echo("Database already exists")
        return
    except:
        pass
    db_url_base, db_name = db_url.rsplit("/", 1)
    maintenance_db_url = f"{db_url_base}/{maintenance_db}"

    with confirm(
        click.echo,
        "Database should now have been created.",
        force=f,
        warning=f"This will create the database {db_url}. Are you sure you want to proceed? Type 'CONFIRM' to confirm.\n",
    ):
        conn = psycopg2.connect(maintenance_db_url)
        try:
            conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
            conn.cursor().execute(f"CREATE DATABASE {db_name}")
        finally:
            conn.close()


@database.command(
    "check-config-consistency",
    help="Check that the current config is consistent with annotationshadowfrequency table",
)
@session
def check_config_consistency(session: Session):
    from ella.api.config import config
    from ella.vardb.datamodel import annotationshadow

    try:
        annotationshadow.check_db_consistency(session, config)
        click.echo("Config is consistent with annotationshadowfrequency table")
    except AssertionError as e:
        click.echo("Config is not consistent with annotationshadowfrequency table")
        click.echo(e)
        exit(1)
