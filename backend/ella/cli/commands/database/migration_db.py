#!/usr/bin/env python

import os
import sys
from pathlib import Path

from alembic import command
from alembic.config import Config
from alembic.migration import MigrationContext
from alembic.script import ScriptDirectory

from ella.vardb.util import DB

SCRIPT_DIR = Path(__file__).resolve().parent
ALEMBIC_CFG = SCRIPT_DIR / "../../../vardb/datamodel/migration/alembic.ini"
ALEMBIC_DIR = SCRIPT_DIR / "../../../vardb/datamodel/migration/alembic/"


def _get_alembic_config():
    alembic_cfg = Config(ALEMBIC_CFG.as_posix())
    alembic_cfg.set_main_option("script_location", ALEMBIC_DIR.as_posix())
    return alembic_cfg


def make_migration_base_db():
    from ella.vardb.datamodel.migration.migration_base import Base as MigrationBase

    db = DB()
    db.connect()
    MigrationBase.metadata.create_all(db.engine)  # noqa: F405


def migration_upgrade(rev: str, db: DB | None = None):
    alembic_cfg = _get_alembic_config()
    if db is None:
        db = DB()
        db.connect()
    assert db.engine
    alembic_cfg.attributes["db_url"] = db.engine.url
    command.upgrade(alembic_cfg, rev)


def migration_downgrade(rev: str, db: DB | None = None):
    alembic_cfg = _get_alembic_config()
    if db is None:
        db = DB()
        db.connect()
    assert db.engine
    alembic_cfg.attributes["db_url"] = db.engine.url
    return command.downgrade(alembic_cfg, rev)


def migration_current(db: DB | None = None):
    alembic_cfg = _get_alembic_config()
    if db is None:
        db = DB()
        db.connect()
    assert db.engine
    alembic_cfg.attributes["db_url"] = db.engine.url
    return command.current(alembic_cfg)


def migration_history(range: str | None = None, verbose=False):
    alembic_cfg = _get_alembic_config()
    db = DB()
    db.connect()
    assert db.engine
    with db.engine.begin() as connection:
        alembic_cfg.attributes["connection"] = connection
        command.history(alembic_cfg, rev_range=range, verbose=verbose)


def migration_compare(quiet=False):
    f = sys.stdout
    if quiet:
        f = Path(os.devnull).open()
    alembic_cfg = _get_alembic_config()
    db = DB()
    db.connect()
    assert db.engine
    with db.engine.begin() as connection:
        context = MigrationContext.configure(connection)
        db_head_rev = context.get_current_revision()

        alembic_cfg.attributes["connection"] = connection
        script = ScriptDirectory.from_config(alembic_cfg)
        script_rev_head = script.get_current_head()

        print(f"Database HEAD: {db_head_rev}", file=f)
        print(f"Migration script HEAD: {script_rev_head}", file=f)
        if not db_head_rev == script_rev_head:
            print("Database HEAD does not match migration script HEAD!")
            sys.exit(1)
        else:
            print("Migration status OK!", file=f)
