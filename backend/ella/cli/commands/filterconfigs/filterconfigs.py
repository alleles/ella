import json
from io import FileIO

import click
from sqlalchemy import case, literal, select
from sqlalchemy.orm import Session

from ella.api.util.util import query_print_table
from ella.cli.decorators import CliLogger, cli_logger, session
from ella.datalayer import filters
from ella.vardb.datamodel import sample, user
from ella.vardb.deposit.deposit_filterconfigs import deposit_filterconfigs


@click.group(help="Filter config management")
def filterconfigs():
    ...


@filterconfigs.command("update")
@click.argument("filterconfig", type=click.File("r"))
@session
@cli_logger(prompt_reason=True)
def cmd_analysis_updatefilterconfig(logger: CliLogger, session: Session, filterconfig: FileIO):
    """
    Updates filterconfigs from the input JSON file.
    """

    filterconfigs = json.load(filterconfig)
    result = deposit_filterconfigs(session, filterconfigs)
    changed = case(
        (
            filters.in_(sample.FilterConfig.id, result["fc_created"]),
            literal("Filterconfig created"),
        ),
        (
            filters.in_(sample.FilterConfig.id, result["fc_updated"]),
            literal("Filterconfig updated"),
        ),
        (
            filters.in_(sample.UserGroupFilterConfig.id, result["ugfc_created"]),
            literal("Added to usergroup"),
        ),
        (
            filters.in_(sample.UserGroupFilterConfig.id, result["ugfc_updated"]),
            literal("Order changed"),
        ),
        else_=literal(""),
    )

    summary = (
        select(
            sample.FilterConfig.name,
            sample.UserGroupFilterConfig.order,
            user.UserGroup.name.label("usergroup"),
            changed.label("change"),
        )
        .select_from(sample.FilterConfig)
        .join(sample.UserGroupFilterConfig)
        .join(user.UserGroup)
        .where(sample.FilterConfig.active.is_(True))
        .order_by(user.UserGroup.name, sample.UserGroupFilterConfig.order)
    )
    logger.echo("\nSummary:\n")
    query_print_table(summary, print_function=logger.echo)

    session.commit()


@filterconfigs.command("deactivate")
@click.argument("filterconfig_id", type=int)
@session
@cli_logger(prompt_reason=True)
def deactivate(logger: CliLogger, session: Session, filterconfig_id: int):
    filterconfig = session.execute(
        select(sample.FilterConfig).where(sample.FilterConfig.id == filterconfig_id)
    ).scalar_one()

    if not filterconfig.active:
        logger.echo(f"Filterconfig {filterconfig} already inactive")
        return

    # Check that user group still has at least one active filter config
    usergroup_filterconfigs_select = (
        select(sample.UserGroupFilterConfig)
        .join(sample.FilterConfig)
        .where(
            sample.UserGroupFilterConfig.filterconfig_id == filterconfig_id,
            sample.FilterConfig.active.is_(True),
        )
        .order_by(sample.UserGroupFilterConfig.order)
    )
    usergroup_filterconfigs = session.execute(usergroup_filterconfigs_select).all()

    if len(usergroup_filterconfigs) == 1:
        logger.echo(
            "\n!!! You are about to disable the only active filter config for usergroup"
            f" {usergroup_filterconfigs[0].usergroup.name} !!!\n"
        )
    elif filterconfig.requirements == []:  # type: ignore[comparison-overlap]
        # Check that user group still has at least one active filter config without requirements
        noreq_filterconfigs = (
            session.execute(
                usergroup_filterconfigs_select.where(sample.FilterConfig.requirements == [])
            )
            .scalars()
            .all()
        )
        if len(noreq_filterconfigs) == 1:
            usergroup = session.execute(
                select(user.UserGroup).where(
                    user.UserGroup.id == noreq_filterconfigs[0].usergroup_id
                )
            ).scalar_one()
            logger.echo(
                "\n!!! You are about to disable the only active filter config for usergroup"
                f" {usergroup.name} without requirements !!!\n"
            )

    answer = input(
        f"Are you sure you want to deactivate filter config {filterconfig}?\nType 'y' to confirm.\n"
    )

    if answer == "y":
        filterconfig.active = False
        session.commit()
        logger.echo(f"Filterconfig {filterconfig.name} deactivated")
    else:
        logger.echo("Lacking confirmation, aborting...")


@filterconfigs.command("list")
def list():
    print("\nCurrent active filterconfigs:\n")

    q = (
        select(
            sample.FilterConfig.id,
            sample.FilterConfig.name,
            sample.UserGroupFilterConfig.order,
            user.UserGroup.name.label("usergroup"),
            sample.FilterConfig.requirements,
        )
        .join(sample.UserGroupFilterConfig)
        .join(user.UserGroup)
        .where(sample.FilterConfig.active.is_(True))
        .order_by(user.UserGroup.name, sample.UserGroupFilterConfig.order)
    )

    query_print_table(q)
