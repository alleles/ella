from collections.abc import Iterable
from typing import Any, Literal, overload

from sqlalchemy import (
    Column,
    ColumnElement,
    ScalarSelect,
    cast,
    func,
    literal_column,
    select,
    tuple_,
)
from sqlalchemy import Tuple as SQLTuple
from sqlalchemy.dialects.postgresql import ARRAY, array
from sqlalchemy.engine.row import Row
from sqlalchemy.orm import InstrumentedAttribute, Query
from sqlalchemy.sql.expression import false
from sqlalchemy.sql.operators import ColumnOperators
from sqlalchemy.sql.selectable import Selectable
from sqlalchemy.types import Integer, String

AttrType = (
    InstrumentedAttribute[int]
    | InstrumentedAttribute[str]
    | InstrumentedAttribute[int | None]
    | InstrumentedAttribute[str | None]
)


@overload
def in_(
    attr: tuple[
        AttrType | Column,
        ...,
    ],
    values: Iterable[tuple[str | int, ...]],
    force_unnest: bool = False,
) -> ColumnElement[bool]:
    ...


@overload
def in_(
    attr: AttrType | Column,
    values: Iterable[str | int],
    force_unnest: bool = False,
) -> ColumnElement[bool]:
    ...


@overload
def in_(
    attr: AttrType | Iterable[AttrType] | Column,
    values: Query | Selectable,
    force_unnest: Literal[False] = False,
) -> ColumnElement[bool]:
    ...


def in_(
    attr: tuple[
        AttrType | Column,
        ...,
    ]
    | AttrType
    | Column
    | Iterable[AttrType],
    values: Iterable[str | int] | Iterable[tuple[str | int, ...]] | Query | Selectable,
    force_unnest: bool = False,
) -> ColumnElement[bool]:
    """
    A drop-in replacement for sqlalchemy's `in_` filter, that uses a subquery with unnest
    for performance (if length of values is above 100).

    See https://levelup.gitconnected.com/why-is-in-query-slow-on-indexed-column-in-postgresql-b716d9b563e2
    for an explanation of why this is the case.

    The following are functionally equivalent:

    ```
    select(
        Allele
    ).where(
        in_(
            session,
            Allele.id,
            (1,2,3),
        )
    )
    ```

    and

    ```
    select(
        Genepanel
    ).where(
        Allele.id.in_([1,2,3])
    )
    ```

    The first example is generally more efficient, as it uses a subquery with unnest, which avoids
    a sequential scan of the genepanel table for large numbers of values.

    Similarly, the following are functionally equivalent:

    ```
    select(
        Genepanel
    ).where(
        in_(
            session,
            (Genepanel.name, Genepanel.version),
            (("panel1", "v01"), ("panel2", "v02")),
        )
    )
    ```

    and

    ```
    select(
        Genepanel
    ).where(
        tuple_(
            Genepanel.name,
            Genepanel.version
        ).in_(
            [("panel1", "v01"), ("panel2", "v02")]
        )
    )
    ```

    """
    if isinstance(attr, Iterable):
        attr = tuple_(*attr)  # type: ignore[assignment]

    assert isinstance(attr, SQLTuple) or not isinstance(attr, Iterable)  # type: ignore[redundant-expr, unreachable]

    if isinstance(values, Query):
        return attr.in_(values.selectable)  # type: ignore[arg-type, ella-mypy]
    elif isinstance(values, Selectable) or isinstance(values, ScalarSelect):
        return attr.in_(values)  # type: ignore[arg-type, ella-mypy]

    if not isinstance(values, list | tuple):
        values = list(values)

    if not values:
        # Empty list, return a filter that will never match
        return false()

    if len(values) < 100 and not force_unnest:
        # Use the default in_ filter for small lists
        return attr.in_(values)  # type: ignore[ella-mypy]

    if isinstance(values[0], tuple) or isinstance(values[0], Row):
        assert isinstance(values[0][0], str) or isinstance(values[0][0], int)
        # map(list, zip(*values)))
        # is equivalent to
        # [[k[0] for k in values], [k[1] for k in values]],
        # for a list of tuples of length 2, but more generic
        # (assuming all tuples have the same length)
        q: Any = select(literal_column("*")).select_from(func.unnest(*map(list, zip(*values))))

        return attr.in_(q)  # type: ignore[ella-mypy]

    assert isinstance(attr, ColumnOperators)

    cast_type: Any
    if isinstance(values[0], str):
        cast_type = String
    elif isinstance(values[0], int):
        cast_type = Integer
    else:
        raise ValueError(f"Unsupported type: {type(values[0])}")

    return attr.in_(select(func.unnest(cast(array(values), ARRAY(cast_type)))))  # type: ignore[ella-mypy]
