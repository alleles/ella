from collections.abc import Sequence

import hypothesis as ht
import hypothesis.strategies as st
from sqlalchemy import select
from sqlalchemy.orm.session import Session

from ella.datalayer.allelefilter.sizefilter import SizeFilter
from ella.vardb.datamodel import allele
from ella.vardb.datamodel.pydantic.filterconfig import SizeModel


@st.composite
def allele_sizes(draw: st.DrawFn):
    sizes = draw(
        st.lists(elements=st.integers(min_value=1, max_value=10000000), min_size=1, max_size=10)
    )
    return sizes


@st.composite
def filter_config(draw: st.DrawFn):
    mode = draw(st.sampled_from([">", "<", ">=", "<=", "=="]))
    threshold = draw(st.integers(min_value=0, max_value=10000000))
    return SizeModel.SizeConfig(**{"mode": mode, "threshold": threshold})


def local_sizefilter_impl(filter_config: SizeModel.SizeConfig, alleles: Sequence[allele.Allele]):
    filtered_alleles = []

    for a in alleles:
        if filter_config.mode == ">":
            if a.length > int(filter_config.threshold):
                filtered_alleles.append(a.id)
        elif filter_config.mode == "<":
            if a.length < int(filter_config.threshold):
                filtered_alleles.append(a.id)
        elif filter_config.mode == ">=":
            if a.length >= int(filter_config.threshold):
                filtered_alleles.append(a.id)
        elif filter_config.mode == "<=":
            if a.length <= int(filter_config.threshold):
                filtered_alleles.append(a.id)
        elif filter_config.mode == "==":
            if a.length == filter_config.threshold:
                filtered_alleles.append(a.id)

        else:
            raise RuntimeError(f"filter config mode {filter_config.mode} is not supported")

    return filtered_alleles


@ht.given(st.one_of(allele_sizes()), st.one_of(filter_config()))
def test_sizefilter(session: Session, allele_sizes: list[int], filter_config: SizeModel.SizeConfig):
    allele_objs = session.execute(select(allele.Allele).limit(len(allele_sizes))).scalars().all()
    for obj, size in zip(allele_objs, allele_sizes):
        obj.length = size
    allele_ids = [obj.id for obj in allele_objs]
    session.flush()
    expected_result = local_sizefilter_impl(filter_config, allele_objs)

    size_filter = SizeFilter(session, None)
    result = size_filter.filter_with_alleles(allele_ids, filter_config)
    assert set(result) == set(expected_result)
