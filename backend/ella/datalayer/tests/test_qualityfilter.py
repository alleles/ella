import re
from typing import Any

import hypothesis as ht
import hypothesis.strategies as st
from sqlalchemy.orm.session import Session

from conftest import mock_allele
from ella.datalayer.allelefilter.qualityfilter import QualityFilter
from ella.vardb.datamodel import genotype, sample
from ella.vardb.datamodel.pydantic.filterconfig import QualityModel


def add_data(session: Session, genotype_data: list[dict | None]):
    analysis = sample.Analysis(
        name="QualityTest", genepanel_name="HBOC", genepanel_version="v1.0.0"
    )
    session.add(analysis)
    session.flush()
    allele_genotype_data: dict[int, list] = {}

    sample_ids: list[int] = []

    def get_sample_id(idx):
        while idx >= len(sample_ids):
            sa = sample.Sample(
                identifier="QualityTestSample",
                proband=True,
                sample_type="HTS",
                affected=True,
                analysis_id=analysis.id,
            )
            session.add(sa)
            session.flush()
            sample_ids.append(sa.id)

        return sample_ids[idx]

    for allele_genotypes in genotype_data:
        allele = mock_allele(session)
        allele_genotype_data[allele.id] = []

        for i, data in enumerate(allele_genotypes):  # type: ignore[arg-type]
            if data is None:
                # No genotype for this sample
                continue
            sample_id = get_sample_id(i)
            secondallele = bool(
                data.get("variant_quality") is not None and data["variant_quality"] % 7 == 0
            )  # Run some variants with secondallele = True

            genotype_obj = genotype.Genotype(
                allele_id=allele.id,
                secondallele_id=allele.id if secondallele else None,  # Mock the secondallele
                sample_id=sample_id,
                variant_quality=data.get("variant_quality"),
                filter_status=data.get("filter_status"),
            )
            session.add(genotype_obj)
            session.flush()

            genotype_sample_data = genotype.GenotypeSampleData(
                sample_id=sample_id,
                genotype_id=genotype_obj.id,
                allele_ratio=data.get("genotype_allele_ratio"),
                type="Heterozygous",
                secondallele=secondallele,
                multiallelic=False,
            )

            session.add(genotype_sample_data)
            session.flush()

            allele_genotype_data[allele.id].append(data)

    return analysis.id, allele_genotype_data


@st.composite
def filter_data(draw) -> list[dict | None]:
    N = draw(st.integers(min_value=1, max_value=3))  # Max 3 samples
    genotype_data: list[dict[str, None | str | int | float] | None] = []
    for _ in range(N):
        has_genotype = draw(st.booleans())
        if has_genotype:
            genotype_data.append(
                {
                    "filter_status": draw(
                        st.sampled_from([None, "PASS", ".", "VQSRTrancheSNP99.00to99.90"])
                    ),
                    "variant_quality": draw(st.integers(min_value=0, max_value=250)),
                    "genotype_allele_ratio": draw(st.floats(min_value=0, max_value=1.0, width=16)),
                }
            )
        else:
            genotype_data = [None] + genotype_data  # type: ignore[assignment, operator]
    ht.assume(any(x is not None for x in genotype_data))
    return genotype_data


@st.composite
def filter_config(draw: st.DrawFn):
    filter_config: dict[str, Any] = {}
    has_quality = draw(st.booleans())
    if has_quality:
        filter_config["quality"] = draw(st.integers(min_value=0, max_value=250))
    has_allele_ratio = draw(st.booleans())
    if has_allele_ratio:
        filter_config["allele_ratio"] = draw(st.floats(min_value=0, max_value=1.0, width=16))
    has_filter = draw(st.booleans())
    if has_filter:
        filter_config["filter_status"] = {
            "pattern": draw(st.sampled_from(["PASS", ".*VQSRTrancheSNP.*"])),
            "inverse": draw(st.booleans()),
            "filter_empty": draw(st.booleans()),
        }

    ht.assume(has_allele_ratio or has_quality or has_filter)  # Disallow empty filterconfig
    return QualityModel.QualityConfig(**filter_config)


@ht.example(
    [[{"genotype_allele_ratio": 0.24, "variant_quality": 100}]],
    QualityModel.QualityConfig(quality=100, allele_ratio=0.25),
    [],
)  # Should not filter where only one criteria if fulfilled (in this case, allele_ratio)
@ht.example(
    [[{"genotype_allele_ratio": 0.26, "variant_quality": 99}]],
    QualityModel.QualityConfig(quality=100, allele_ratio=0.25),
    [],
)  # Should not filter where only one criteria if fulfilled (in this case, variant_quality)
@ht.example(
    [[{"genotype_allele_ratio": 0.24, "variant_quality": 99}]],
    QualityModel.QualityConfig(quality=100, allele_ratio=0.25),
    [0],
)  # Should filter when both criteria fulfilled
@ht.example(
    [[{"genotype_allele_ratio": 0.24, "variant_quality": 99}]],
    QualityModel.QualityConfig(allele_ratio=0.25),
    [0],
)  # Should disregard variant quality when not set in filter config
@ht.example(
    [[{"genotype_allele_ratio": 0.26, "variant_quality": 99}]],
    QualityModel.QualityConfig(allele_ratio=0.25),
    [],
)  # Should disregard variant quality when not set in filter config
@ht.example(
    [[{"genotype_allele_ratio": 0.24, "variant_quality": 99}]],
    QualityModel.QualityConfig(quality=100),
    [0],
)  # Should disregard allele ratio when not set in filter config
@ht.example(
    [[{"genotype_allele_ratio": 0.24, "variant_quality": 101}]],
    QualityModel.QualityConfig(quality=100),
    [],
)  # Should disregard allele ratio when not set in filter config
@ht.example(
    [[{"genotype_allele_ratio": 0.24, "filter_status": "FAIL", "variant_quality": 101}]],
    QualityModel.QualityConfig(quality=100),
    [],
)  # Should disregard filter status when not set in filter config
@ht.example(
    [[{"genotype_allele_ratio": 0.24, "variant_quality": None}]],
    QualityModel.QualityConfig(quality=100),
    [],
)  # Should not filter out alleles with QUAL null
@ht.example(
    [[{"genotype_allele_ratio": None, "variant_quality": 99}]],
    QualityModel.QualityConfig(allele_ratio=0.25),
    [],
)  # Should not filter out alleles with AR null
@ht.example(
    [[{"genotype_allele_ratio": 0.0, "variant_quality": 99}]],
    QualityModel.QualityConfig(allele_ratio=0.25),
    [],
)  # Should not filter out alleles with AR==0.0
@ht.example(
    [[{"filter_status": "."}], [{"filter_status": None}]],
    QualityModel.QualityConfig(filter_status={"pattern": "\\."}),
    [],
)  # Should not filter out alleles with filter_status None or '.' (implicit)
@ht.example(
    [[{"filter_status": "."}], [{"filter_status": None}]],
    QualityModel.QualityConfig(filter_status={"pattern": "\\.", "filter_empty": False}),
    [],
)  # Should not filter out alleles with filter_status None or '.' (explicit)
@ht.example(
    [[{"filter_status": "."}], [{"filter_status": None}]],
    QualityModel.QualityConfig(filter_status={"pattern": "PASS", "filter_empty": True}),
    [0, 1],
)  # Should filter out alleles with filter_status None or '.' when specified
@ht.example(
    [[{"filter_status": "."}], [{"filter_status": None}]],
    QualityModel.QualityConfig(
        filter_status={"pattern": "PASS", "filter_empty": True, "inverse": True}
    ),
    [],
)  # Should not filter out alleles with filter_status None or '.' when inverse
@ht.example(
    [[{"filter_status": "FAIL"}]],
    QualityModel.QualityConfig(filter_status={"pattern": "PASS", "inverse": True}),
    [0],
)  # Should filter out alleles with inverse = True
@ht.example(
    [
        [
            # Sample 1
            {"genotype_allele_ratio": 0.24, "variant_quality": 99},
            # Sample 2, same allele
            {"genotype_allele_ratio": 0.26, "variant_quality": 99},
        ]
    ],
    QualityModel.QualityConfig(allele_ratio=0.25),
    [],
)  # Should not filter when not all samples fulfill the criteria for the genotype
@ht.example(
    [
        [
            {"genotype_allele_ratio": 0.24, "variant_quality": 101},
            {"genotype_allele_ratio": 0.26, "variant_quality": 99},
        ]
    ],
    QualityModel.QualityConfig(quality=100),
    [],
)  # Should not filter when not all samples fulfill the criteria for the genotype
@ht.example(
    [[{"filter_status": "VQSRTrancheSNP99.00to99.90"}, {"filter_status": "PASS"}]],
    QualityModel.QualityConfig(filter_status={"pattern": ".*VQSR.*"}),
    [],
)  # Should not filter when not all samples fulfill the criteria for the genotype
@ht.example(
    [
        [
            {"genotype_allele_ratio": 0.24, "variant_quality": 99},
            {"genotype_allele_ratio": 0.24, "variant_quality": 99},
        ]
    ],
    QualityModel.QualityConfig(allele_ratio=0.25),
    [0],
)  # Should filter when all samples fulfill the criteria for the genotype
@ht.example(
    [[{"filter_status": "VQSRTrancheSNP99.00to99.90"}]],
    QualityModel.QualityConfig(filter_status={"pattern": ".*VQSRTranche.*"}),
    [0],
)  # Should filter when only sample fulfill the criteria for the genotype
@ht.example(
    [
        [
            {"filter_status": "VQSRTrancheSNP99.00to99.90"},
            {"filter_status": "VQSRTrancheSNP99.00to99.90"},
        ]
    ],
    QualityModel.QualityConfig(filter_status={"pattern": ".*VQSRTranche.*"}),
    [0],
)  # Should filter when all samples fulfill the criteria for the genotype
@ht.example(
    [
        [None, {"genotype_allele_ratio": 0.24, "variant_quality": 99}],
        [{"genotype_allele_ratio": 0.24, "variant_quality": 99}],
    ],
    QualityModel.QualityConfig(allele_ratio=0.25),
    [0, 1],
)  # Should filter if one sample does not have the genotype
@ht.example(
    [
        [None, {"genotype_allele_ratio": 0.26, "variant_quality": 99}],
        [{"genotype_allele_ratio": 0.24, "variant_quality": 99}],
    ],
    QualityModel.QualityConfig(allele_ratio=0.25),
    [1],
)  # Should filter if one sample does not have the genotype
@ht.example(
    [
        [None, {"genotype_allele_ratio": 0.24, "variant_quality": 99}],
        [{"genotype_allele_ratio": 0.24, "variant_quality": 101}],
    ],
    QualityModel.QualityConfig(quality=100),
    [0],
)  # Should filter if one sample does not have the genotype
@ht.example(
    [
        [None, {"genotype_allele_ratio": 0.26, "variant_quality": 99}],
        [{"genotype_allele_ratio": 0.26, "variant_quality": 99}],
    ],
    QualityModel.QualityConfig(allele_ratio=0.25),
    [],
)  # Should filter if one sample does not have the genotype
@ht.example(
    [
        [None, {"genotype_allele_ratio": 0.26, "variant_quality": 99, "filter_status": "PASS"}],
        [{"genotype_allele_ratio": 0.26, "variant_quality": 99, "filter_status": "PASS"}],
    ],
    QualityModel.QualityConfig(filter_status={"pattern": "PASS"}),
    [0, 1],
)  # Should filter if one sample does not have the genotype
@ht.given(st.lists(filter_data(), min_size=1), st.one_of(filter_config()), st.just(None))
@ht.settings(deadline=5000)
def test_qualityfilter(
    session: Session,
    genotype_data: list[dict | None],
    filter_config: QualityModel.QualityConfig,
    manually_curated_result: list[int] | None,
):
    """genotype_data is of the form:
    [
        [sample1 genotype, sample2 genotype, ...], # Allele 1
        [sample1 genotype, sample2 genotype, ...], # Allele 2
    ]
    """
    session.rollback()

    # Add generated data to database
    analysis_id, allele_genotype_data = add_data(session, genotype_data)

    # Run filter
    quality_filter = QualityFilter(session, None)
    allele_ids = [int(k) for k in allele_genotype_data.keys()]
    result = quality_filter.filter_with_analysis(analysis_id, allele_ids, filter_config)

    # Check manually curated result if provided
    if manually_curated_result is not None:
        assert set(result) == set(allele_ids[k] for k in manually_curated_result)

    expected_result = []
    for allele_id, sample_genotype_data in allele_genotype_data.items():
        if filter_config.quality is not None and any(
            genotype_data["variant_quality"] is None
            or genotype_data["variant_quality"] >= filter_config.quality
            for genotype_data in sample_genotype_data
            if genotype_data is not None
        ):
            # Variant quality None or above threshold
            continue
        if filter_config.allele_ratio is not None and any(
            genotype_data["genotype_allele_ratio"] is None
            or genotype_data["genotype_allele_ratio"] == 0.0
            or genotype_data["genotype_allele_ratio"] >= filter_config.allele_ratio
            for genotype_data in sample_genotype_data
            if genotype_data is not None
        ):
            # Allele ratio None, 0.0 or above threshold
            continue

        if filter_config.filter_status:

            def apply_filter(text: str | None):
                def apply_inverse(result: bool | None):
                    return not result if filter_config.filter_status.inverse else result  # type: ignore[union-attr]

                if text is None or text == ".":
                    return apply_inverse(filter_config.filter_status.filter_empty)  # type: ignore[union-attr]

                pattern = re.compile(filter_config.filter_status.pattern)  # type: ignore[union-attr]
                match = bool(re.match(pattern, text))
                return apply_inverse(match)

            # ALL samples must be filtered for allele to be filtered
            if any(
                not apply_filter(genotype_data["filter_status"])
                for genotype_data in sample_genotype_data
                if genotype_data is not None
            ):
                continue

        expected_result.append(allele_id)

    assert set(result) == set(expected_result)
