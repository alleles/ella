from typing import Literal

import hypothesis as ht
import hypothesis.strategies as st
from sqlalchemy import select
from sqlalchemy.orm.session import Session

from ella.datalayer.allelefilter.callertypefilter import CallerTypeFilter
from ella.vardb.datamodel import allele
from ella.vardb.datamodel.pydantic.filterconfig import CallertypeModel


@st.composite
def filter_configs(draw: st.DrawFn):
    modes = draw(st.lists(st.sampled_from(["cnv", "snv"]), unique=True))
    return [CallertypeModel.CallertypeConfig(caller_types=[x]) for x in modes]


@ht.given(
    st.one_of(st.lists(elements=st.sampled_from(("cnv", "snv")), unique=True)),
    st.one_of(filter_configs()),
)
def test_callertypefilter(
    session: Session,
    allele_caller_types: list[Literal["cnv", "snv"]],
    caller_type_configs: list[CallertypeModel.CallertypeConfig],
):
    # Apply sizes to some allele objects
    # We can filter on callertypes, extracting only the exact caller_type
    # Then we can do filtering on all filter_configs, check that they
    # are exclusive, hence, one should contain all, and the other none in result
    allele_objs = (
        session.execute(select(allele.Allele).limit(len(allele_caller_types))).scalars().all()
    )

    allele_ids = [obj.id for obj in allele_objs]
    session.flush()

    ctf = CallerTypeFilter(session, None)

    for filter_config in caller_type_configs:
        caller_type = filter_config.caller_types

        localCheck = [obj for obj in allele_objs if obj.caller_type == caller_type[0]]

        result = ctf.filter_with_alleles(allele_ids, filter_config)
        assert len(localCheck) == len(result)
