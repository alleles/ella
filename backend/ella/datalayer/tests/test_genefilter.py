"""
Integration/unit test for the AlleleFilter module.
Since it consists mostly of database queries, it's tested on a live database.
"""

from collections.abc import Callable

import hypothesis as ht
import hypothesis.strategies as st
from sqlalchemy import insert
from sqlalchemy.orm.session import Session

from conftest import mock_allele_with_annotation
from ella.datalayer.allelefilter.genefilter import GeneFilter
from ella.vardb.datamodel import gene
from ella.vardb.datamodel.pydantic.filterconfig import GeneModel
from ella.vardb.util.testdatabase import TestDatabase


def create_genepanel(session: Session, hgnc_ids_transcripts: list[tuple[int, str]]):
    # Create fake genepanel for testing purposes

    genepanel = gene.Genepanel(name="testpanel", version="v01", genome_reference="GRCh37")
    session.add(genepanel)
    session.flush()

    for hgnc_id, transcript_name in hgnc_ids_transcripts:
        g = gene.Gene(hgnc_id=hgnc_id, hgnc_symbol=f"GENE{hgnc_id}")

        tx = gene.Transcript(
            gene=g,
            transcript_name=transcript_name,
            type="RefSeq",
            genome_reference="",
            tags=None,
            chromosome="1",
            tx_start=1000,
            tx_end=1500,
            strand="+",
            cds_start=1230,
            cds_end=1430,
            exon_starts=[1100, 1200, 1300, 1400],
            exon_ends=[1160, 1260, 1360, 1460],
        )
        session.add(tx)
        session.flush()
        session.execute(
            insert(gene.GenepanelTranscript),
            {
                "transcript_id": tx.id,
                "genepanel_name": genepanel.name,
                "genepanel_version": genepanel.version,
                "inheritance": "AD/AR",
            },
        )

    return genepanel


def get_hgnc_id():
    return st.integers(min_value=int(1e6), max_value=int(1e6 + 20))


@st.composite
def gene_transcripts(draw: Callable[[st.SearchStrategy[int]], int]):
    hgnc_id = draw(get_hgnc_id())
    transcript = f"NM_{hgnc_id}.1"
    return (hgnc_id, transcript)


@st.composite
def annotations(draw: Callable[[st.SearchStrategy[int]], int]):
    # Some alleles could be without annotations
    N = draw(st.integers(min_value=0, max_value=4))
    anno: dict[str, list[dict[str, str | int]]] = {"transcripts": []}
    for _ in range(N):
        hgnc_id = draw(get_hgnc_id())
        transcript = f"NM_{hgnc_id}.1"
        anno["transcripts"].append({"hgnc_id": hgnc_id, "transcript": transcript})
    return anno


@st.composite
def filter_config(draw: st.DrawFn):
    # We run all combinations of mode and inverse in the test, so no need to specify here
    return GeneModel.GeneConfig(**{"genes": draw(st.lists(get_hgnc_id(), unique=True, min_size=1))})


class TestGeneFilter:
    def test_prepare_data(self, test_database: TestDatabase):
        test_database.refresh()

    @ht.example(
        [(1, "NM_1.1")],
        [{"transcripts": [{"hgnc_id": 1, "transcript": "NM_1.1"}]}],
        GeneModel.GeneConfig(**{"genes": [1], "mode": "all"}),
        [0],
    )  # Simple case
    @ht.example(
        [(1, "NM_alternative1.1")],  # Genepanel has different transcript from annotation
        [{"transcripts": [{"hgnc_id": 1, "transcript": "NM_1.1"}]}],
        GeneModel.GeneConfig(**{"genes": [1], "mode": "one"}),
        [],
    )
    @ht.example(
        [(1, "NM_1.1"), (2, "NM_2.1")],
        [{"transcripts": [{"hgnc_id": 2, "transcript": "NM_2.1"}]}],
        GeneModel.GeneConfig(**{"genes": [1], "mode": "all"}),
        [],
    )
    @ht.example(
        [(1, "NM_1.1"), (2, "NM_2.1")],
        [{"transcripts": [{"hgnc_id": 1, "transcript": "NM_1.1"}]}],  # Only annotated with one
        GeneModel.GeneConfig(**{"genes": [1], "mode": "one"}),  # Mode one
        [0],
    )
    @ht.example(
        [(1, "NM_1.1"), (2, "NM_2.1")],
        [{"transcripts": [{"hgnc_id": 1, "transcript": "NM_1.1"}]}],  # Only annotated with one
        GeneModel.GeneConfig(**{"genes": [1], "mode": "all"}),  # Mode all
        [0],
    )
    @ht.example(
        [(1, "NM_1.1"), (2, "NM_2.1")],
        [
            {
                "transcripts": [
                    {"hgnc_id": 1, "transcript": "NM_1.1"},
                    {"hgnc_id": 2, "transcript": "NM_2.1"},
                ]
            }
        ],  # Annotated with both
        GeneModel.GeneConfig(**{"genes": [1], "mode": "all"}),
        [],
    )
    @ht.example(
        [(1, "NM_1.1"), (2, "NM_2.1")],
        [
            {
                "transcripts": [
                    {"hgnc_id": 1, "transcript": "NM_1.1"},
                    {"hgnc_id": 2, "transcript": "NM_2.1"},
                ]
            }
        ],  # Annotated with both
        GeneModel.GeneConfig(**{"genes": [1, 2], "mode": "all"}),  # Both genes in filter
        [0],
    )
    @ht.given(
        st.lists(gene_transcripts(), min_size=1, unique=True),
        st.lists(annotations(), min_size=5),
        st.one_of(filter_config()),
        st.just(None),
    )
    @ht.settings(deadline=None)
    def test_gene_filter(
        self,
        session: Session,
        gene_transcripts: list[tuple[int, str]],
        annotations: list[dict[str, list[dict[str, str | int]]]],
        filter_config: GeneModel.GeneConfig,
        manually_curated_result: list[int] | None,
    ):
        session.rollback()
        gp = create_genepanel(session, gene_transcripts)
        session.add(gp)
        gp_tx = [tx.transcript_name for tx in gp.transcripts]

        allele_ids = []
        allele_id_genes = {}
        for ann in annotations:
            al, an = mock_allele_with_annotation(session, annotations=ann)

            session.flush()
            allele_ids.append(al.id)
            allele_id_genes[al.id] = [
                tx["hgnc_id"] for tx in ann["transcripts"] if tx["transcript"] in gp_tx
            ]

        gene_filter = GeneFilter(session, None)

        # Check if result has been curated manually
        if manually_curated_result is not None:
            result = gene_filter.filter_with_genepanel(
                (gp.name, gp.version), allele_ids, filter_config
            )
            expected_result = [allele_ids[x] for x in manually_curated_result]
            assert set(expected_result) == set(result)

        # Run all combinations of filterconfig options
        for mode in ["all", "one"]:
            filter_config.mode = mode  # type: ignore[assignment]
            filter_config.inverse = False

            result = gene_filter.filter_with_genepanel(
                (gp.name, gp.version), allele_ids, filter_config
            )

            if mode == "one":
                expected_result = [
                    allele_id
                    for allele_id, genes in allele_id_genes.items()
                    if set(genes) and set(genes) & set(filter_config.genes)  # Overlaps
                ]
            else:
                expected_result = [
                    allele_id
                    for allele_id, genes in allele_id_genes.items()
                    if set(genes) and set(genes) - set(filter_config.genes) == set()  # Contained in
                ]

            assert set(expected_result) == set(result)

            # Run inverse filter
            filter_config.inverse = True
            result = gene_filter.filter_with_genepanel(
                (gp.name, gp.version), allele_ids, filter_config
            )
            assert result == set(allele_ids) - set(expected_result)
