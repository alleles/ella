import math
import operator
import random
import string
from collections.abc import Callable
from dataclasses import dataclass
from functools import partial, reduce
from typing import TYPE_CHECKING, Any, Literal

import hypothesis as ht
import hypothesis.strategies as st
from sqlalchemy import select
from sqlalchemy.orm import Session

from conftest import mock_allele_with_annotation
from ella.datalayer import filters
from ella.datalayer.allelefilter.genericannotationfilter import GenericAnnotationFilter
from ella.vardb.datamodel.annotation import Annotation
from ella.vardb.datamodel.pydantic.filterconfig import GenericAnnotationModel

if TYPE_CHECKING:
    from ella.vardb.datamodel.allele import Allele

AnnotationsType = dict[str, dict[str, list[dict[str, Any]]]] | dict[str, dict[str, dict[str, Any]]]


@dataclass
class AnnotationsValue:
    """Helper class to pair annotations and values, along with primitive type of the annotation."""

    annotations_list: list[AnnotationsType]
    type_str: Literal["boolean", "string", "number"]
    value: Any = None


@st.composite
def arbitrary_annotations(
    draw: st.DrawFn,
    elements: st.SearchStrategy,
    array_strategy: st.SearchStrategy[bool],
    value_strategy: st.SearchStrategy,
) -> AnnotationsValue:
    """Generate arbitrary annotations (to be used when constructing mock alleles) and values for
    testing generic annotation filters.
    """

    def determine_cast_type(elements: st.SearchStrategy) -> Literal["boolean", "string", "number"]:
        def get_repr_str(elements: st.SearchStrategy):
            if hasattr(elements, "element_strategies"):
                for strat in elements.element_strategies:
                    if repr(strat) != "none()":
                        return repr(strat).lower()
            elif hasattr(elements, "element_strategy"):
                return repr(elements.element_strategy).lower()
            elif hasattr(elements, "wrapped_strategy"):
                return repr(elements.wrapped_strategy).lower()
            return repr(elements).lower()

        repr_str = get_repr_str(elements)
        if "int" in repr_str or "float" in repr_str:
            return "number"
        elif "text" in repr_str or "char" in repr_str:
            return "string"
        elif "bool" in repr_str:
            return "boolean"
        raise ValueError(f"Unsupported element type: {repr_str}")

    annotations_list: list[AnnotationsType] = []
    n_fields = draw(st.integers(min_value=1, max_value=10))
    n_anno = draw(st.integers(min_value=1, max_value=20))

    # Ensure the same values are present in at least some annotation fields
    target_value_options = [
        [draw(elements) for _ in range(n_fields)] for _ in range(max(math.floor(n_anno / 2), 1))
    ]

    # Determine cast type
    type_str = determine_cast_type(elements)

    annotations: AnnotationsType
    is_array = draw(array_strategy)
    for _ in range(n_anno):
        target_values = random.choice(target_value_options)
        if is_array:
            annotations = {
                "prediction": {
                    "arbitrary": [{f"arbitrary{j}": target_values[j] for j in range(n_fields)}]
                }
            }
        else:
            annotations = {
                "prediction": {
                    "arbitrary": {f"arbitrary{j}": target_values[j] for j in range(n_fields)}
                }
            }
        annotations_list.append(annotations)
    return AnnotationsValue(annotations_list, type_str, draw(value_strategy))


def is_target_array(
    anno_value: AnnotationsValue,
):
    return isinstance(anno_value.annotations_list[0]["prediction"]["arbitrary"], list)


def build_filter_config(
    anno_value: AnnotationsValue,
    is_array: bool,
    op_str: str,
    mode: Literal["all", "any"],
):
    if is_array:
        n_rules = len(anno_value.annotations_list[0]["prediction"]["arbitrary"][0].keys())  # type: ignore[index]
    else:
        n_rules = len(anno_value.annotations_list[0]["prediction"]["arbitrary"].keys())  # type: ignore[union-attr]

    return GenericAnnotationModel.GenericAnnotationConfig.model_validate(
        {
            "target": "prediction.arbitrary",
            "is_array": is_array,
            "rules": [
                {
                    "key": f"arbitrary{i}",
                    "operator": op_str,
                    "value": anno_value.value,
                    "type_str": anno_value.type_str,
                }
                for i in range(random.randint(1, max(n_rules - 1, 1)))
            ],
            "mode": mode,
        }
    )


def match_value_to_annotation(
    anno_value: AnnotationsValue,
    filter_config: GenericAnnotationModel.GenericAnnotationConfig,
):
    """Avoid trivial tests where value is absent from annotations"""
    for annotation in random.sample(anno_value.annotations_list, len(anno_value.annotations_list)):
        if filter_config.is_array:
            idx = random.randint(0, len(annotation["prediction"]["arbitrary"]) - 1)
            target = annotation["prediction"]["arbitrary"][idx]  # type: ignore[index]
        else:
            target = annotation["prediction"]["arbitrary"]

        key_values = {}
        for rule in filter_config.rules:
            if target[rule.key] is None:
                if filter_config.mode == "all":
                    break
                elif filter_config.mode == "any":
                    continue
            key_values[rule.key] = target[rule.key]

        for rule in filter_config.rules:
            rule.value = getattr(key_values, rule.key, rule.value)


def create_annotated_alleles(session: Session, anno_value: AnnotationsValue):
    alleles: list[Allele]
    alleles, _ = zip(  # type: ignore[assignment]
        *[
            mock_allele_with_annotation(session, annotations=annotations)
            for annotations in anno_value.annotations_list
        ]
    )
    return alleles


def phony_allele_filter(
    session: Session,
    allele_ids: list[int],
    filter_config: GenericAnnotationModel.GenericAnnotationConfig,
    op_func: Callable,
):
    def evaluate(annotation: Annotation):
        def is_none_not_identity(x: Any, rule: GenericAnnotationModel.GenericAnnotationConfig.Rule):
            return x is None and rule.operator not in ["is", "is_not"]

        target = reduce(
            lambda d, key: d[key], filter_config.target.split("."), annotation.annotations
        )
        result: list[bool] = []
        for rule in filter_config.rules:
            if filter_config.is_array:
                temp_result: list[bool] = []
                for target_element in target:
                    if rule.key not in target_element or is_none_not_identity(
                        target_element[rule.key], rule
                    ):
                        temp_result.append(False)
                        continue
                    temp_result.append(op_func(target_element[rule.key], rule.value))
                result.append(any(temp_result))
            else:
                if rule.key not in target or is_none_not_identity(target[rule.key], rule):
                    result.append(False)
                    continue
                result.append(op_func(target[rule.key], rule.value))

        if filter_config.mode == "all":
            return all(result)
        else:
            return any(result)

    annotations = (
        session.execute(
            select(Annotation)
            .where(filters.in_(Annotation.allele_id, allele_ids))
            .order_by(Annotation.allele_id)
        )
        .scalars()
        .all()
    )
    filtered_allele_ids = [al_id for al_id, anno in zip(allele_ids, annotations) if evaluate(anno)]
    return set(filtered_allele_ids)


@ht.example(  # match any of the rules
    anno_value=AnnotationsValue(
        annotations_list=[
            {
                "prediction": {
                    "arbitrary": {"arbitrary0": "None", "arbitrary1": None},
                }
            },
            {"prediction": {"arbitrary": {"arbitrary0": "foo1", "arbitrary1": ""}}},
            {"prediction": {"arbitrary": {"arbitrary0": None, "arbitrary1": None}}},
        ],
        type_str="string",
    ),
    negate=False,
    mode="any",
    filter_config=GenericAnnotationModel.GenericAnnotationConfig.model_validate(
        {
            "target": "prediction.arbitrary",
            "is_array": False,
            "rules": [
                {
                    "key": "arbitrary0",
                    "operator": "is",
                    "value": None,
                    "type_str": "string",
                },
                {
                    "key": "arbitrary1",
                    "operator": "is",
                    "value": None,
                    "type_str": "string",
                },
            ],
            "mode": "any",
        }
    ),
    expected_results=[0, 2],
)
@ht.example(  # match all of the rules
    anno_value=AnnotationsValue(
        annotations_list=[
            {
                "prediction": {
                    "arbitrary": {"arbitrary0": False, "arbitrary1": True},
                }
            },
            {"prediction": {"arbitrary": {"arbitrary0": True, "arbitrary1": False}}},
            {"prediction": {"arbitrary": {"arbitrary0": True, "arbitrary1": True}}},
        ],
        type_str="boolean",
    ),
    negate=False,
    mode="all",
    filter_config=GenericAnnotationModel.GenericAnnotationConfig.model_validate(
        {
            "target": "prediction.arbitrary",
            "is_array": False,
            "rules": [
                {
                    "key": "arbitrary0",
                    "operator": "is",
                    "value": True,
                    "type_str": "boolean",
                },
                {
                    "key": "arbitrary1",
                    "operator": "is",
                    "value": True,
                    "type_str": "boolean",
                },
            ],
        }
    ),
    expected_results=[2],
)
@ht.given(
    st.one_of(
        arbitrary_annotations(
            elements=st.one_of(
                st.none(), st.floats(allow_infinity=False, allow_nan=False, width=16)
            ),
            array_strategy=st.booleans(),
            value_strategy=st.none(),
        ),
        arbitrary_annotations(
            elements=st.one_of(st.none(), st.integers()),
            array_strategy=st.booleans(),
            value_strategy=st.none(),
        ),
        arbitrary_annotations(
            elements=st.one_of(
                st.none(),
                st.text(alphabet=string.printable),
            ),
            array_strategy=st.booleans(),
            value_strategy=st.none(),
        ),
        arbitrary_annotations(
            elements=st.one_of(st.none(), st.booleans()),
            array_strategy=st.booleans(),
            value_strategy=st.none(),
        ),
        arbitrary_annotations(
            elements=st.booleans(), array_strategy=st.booleans(), value_strategy=st.booleans()
        ),
        arbitrary_annotations(
            elements=st.one_of(st.none(), st.booleans()),
            array_strategy=st.booleans(),
            value_strategy=st.booleans(),
        ),
    ),
    st.booleans(),
    st.one_of(st.just("all"), st.just("any")),
    st.just(None),
    st.just(None),
)
@ht.settings(deadline=None)
def test_identity(
    session: Session,
    anno_value: AnnotationsValue,
    negate: bool,
    mode: Literal["all", "any"],
    filter_config: GenericAnnotationModel.GenericAnnotationConfig | None,
    expected_results: list[int] | None,
):
    """Test the identity operators"""
    op_func = operator.is_ if not negate else operator.is_not
    op_str = "is" if not negate else "is_not"

    if filter_config is None:
        is_array = is_target_array(anno_value)
        filter_config = build_filter_config(anno_value, is_array, op_str, mode)
    alleles = create_annotated_alleles(session, anno_value)
    allele_ids = [al.id for al in alleles]

    # Filter alleles
    anno_filter = GenericAnnotationFilter(session, None)
    filtered_allele_ids = anno_filter.filter_with_alleles(allele_ids, filter_config)
    non_filtered_allele_ids = set(
        [al_id for al_id in allele_ids if al_id not in filtered_allele_ids]
    )

    # Filter alleles on annotations directly
    phony_filtered_allele_ids = phony_allele_filter(session, allele_ids, filter_config, op_func)
    phony_non_filtered_allele_ids = set(
        [al_id for al_id in allele_ids if al_id not in phony_filtered_allele_ids]
    )

    assert filtered_allele_ids == phony_filtered_allele_ids
    assert non_filtered_allele_ids == phony_non_filtered_allele_ids

    # Compare with manually curated results
    if expected_results is not None:
        assert set(allele_ids[x] for x in expected_results) == filtered_allele_ids


@ht.example(
    anno_value=AnnotationsValue(
        annotations_list=[
            {
                "variantDBs": {
                    "varde": {
                        "CLASS_UNNAMG": "5",
                        "YEAR_UNNAMG": 2018,
                        "CLASS_STOAMG": "4",
                    }
                }
            },
            {
                "variantDBs": {
                    "varde": {
                        "CLASS_UNNAMG": "1",
                        "YEAR_UNNAMG": 2023,
                    }
                }
            },
        ],
        type_str="string",
    ),
    negate=False,
    match=False,
    mode="any",
    filter_config=GenericAnnotationModel.GenericAnnotationConfig.model_validate(
        {
            "target": "variantDBs.varde",
            "is_array": False,
            "rules": [
                {"key": "CLASS_UNNAMG", "operator": "==", "value": "5", "type_str": "string"},
                {"key": "CLASS_UNNAMG", "operator": "==", "value": "4", "type_str": "string"},
                {"key": "CLASS_STOAMG", "operator": "==", "value": "5", "type_str": "string"},
                {"key": "CLASS_STOAMG", "operator": "==", "value": "4", "type_str": "string"},
            ],
            "mode": "any",
        }
    ),
    expected_results=[0],
)
@ht.given(
    st.one_of(
        arbitrary_annotations(
            elements=st.one_of(st.none(), st.text(alphabet=string.printable)),
            array_strategy=st.booleans(),
            value_strategy=st.text(alphabet=string.printable),
        ),
        arbitrary_annotations(
            elements=st.one_of(st.none(), st.integers()),
            array_strategy=st.booleans(),
            value_strategy=st.integers(),
        ),
        arbitrary_annotations(
            elements=st.one_of(
                st.none(), st.floats(allow_infinity=False, allow_nan=False, width=16)
            ),
            array_strategy=st.booleans(),
            value_strategy=st.floats(allow_infinity=False, allow_nan=False, width=16),
        ),
    ),
    st.booleans(),
    st.booleans(),
    st.one_of(st.just("all"), st.just("any")),
    st.just(None),
    st.just(None),
)
@ht.settings(deadline=None)
def test_equality(
    session: Session,
    anno_value: AnnotationsValue,
    negate: bool,
    match: bool,
    mode: Literal["all", "any"],
    filter_config: GenericAnnotationModel.GenericAnnotationConfig | None,
    expected_results: list[int] | None,
):
    """Test the equality operators"""
    op_func = operator.eq if not negate else operator.ne
    op_str = "==" if not negate else "!="

    if filter_config is None:
        is_array = is_target_array(anno_value)
        filter_config = build_filter_config(anno_value, is_array, op_str, mode)
    if match:
        match_value_to_annotation(anno_value, filter_config)

    alleles = create_annotated_alleles(session, anno_value)
    allele_ids = [al.id for al in alleles]

    # Filter alleles
    anno_filter = GenericAnnotationFilter(session, None)
    filtered_allele_ids = anno_filter.filter_with_alleles(allele_ids, filter_config)
    non_filtered_allele_ids = set(
        [al_id for al_id in allele_ids if al_id not in filtered_allele_ids]
    )

    # Filter alleles on annotations directly
    phony_filtered_allele_ids = phony_allele_filter(session, allele_ids, filter_config, op_func)
    phony_non_filtered_allele_ids = set(
        [al_id for al_id in allele_ids if al_id not in phony_filtered_allele_ids]
    )

    assert filtered_allele_ids == phony_filtered_allele_ids
    assert non_filtered_allele_ids == phony_non_filtered_allele_ids

    # Compare with manually curated results
    if expected_results is not None:
        assert set(allele_ids[x] for x in expected_results) == filtered_allele_ids


@ht.example(
    anno_value=AnnotationsValue(
        annotations_list=[
            {
                "prediction": {
                    "spliceai": [
                        {
                            "DP_AG": -5,
                            "DP_AL": 3,
                            "DP_DG": 37,
                            "DP_DL": 3,
                            "DS_AG": 0.2,
                            "DS_AL": 0.14,
                            "DS_DG": 0.0,
                            "DS_DL": 0.0,
                            "SYMBOL": "NM_001369.2",
                        },
                    ]
                }
            },
            {
                "prediction": {
                    "spliceai": [
                        {
                            "DP_AG": -7,
                            "DP_AL": 3,
                            "DP_DG": 12,
                            "DP_DL": 3,
                            "DS_AG": 0.01,
                            "DS_AL": 0.02,
                            "DS_DG": 0.1,
                            "DS_DL": 0.2,
                            "SYMBOL": "NM_001370.2",
                        },
                    ]
                }
            },
        ],
        type_str="number",
    ),
    equal=True,
    greater=True,
    match=False,
    mode="all",
    filter_config=GenericAnnotationModel.GenericAnnotationConfig.model_validate(
        {
            "target": "prediction.spliceai",
            "is_array": True,
            "rules": [
                {"key": "DS_AG", "operator": ">=", "value": 0.05, "type_str": "number"},
                {"key": "DS_AL", "operator": ">=", "value": 0.05, "type_str": "number"},
            ],
        }
    ),
    expected_results=[0],
)
@ht.given(
    st.one_of(
        arbitrary_annotations(
            elements=st.one_of(st.none(), st.integers()),
            array_strategy=st.booleans(),
            value_strategy=st.integers(),
        ),
        arbitrary_annotations(
            elements=st.one_of(
                st.none(), st.floats(allow_infinity=False, allow_nan=False, width=16)
            ),
            array_strategy=st.booleans(),
            value_strategy=st.floats(allow_infinity=False, allow_nan=False, width=16),
        ),
    ),
    st.booleans(),
    st.booleans(),
    st.booleans(),
    st.one_of(st.just("all"), st.just("any")),
    st.just(None),
    st.just(None),
)
@ht.settings(deadline=None)
def test_inequality(
    session: Session,
    anno_value: AnnotationsValue,
    equal: bool,
    greater: bool,
    match: bool,
    mode: Literal["all", "any"],
    filter_config: GenericAnnotationModel.GenericAnnotationConfig | None,
    expected_results: list[int] | None,
):
    """Test the inequality operators"""
    if equal:
        op_func = operator.ge if greater else operator.le
        op_str = ">=" if greater else "<="
    else:
        op_func = operator.gt if greater else operator.lt
        op_str = ">" if greater else "<"

    if filter_config is None:
        is_array = is_target_array(anno_value)
        filter_config = build_filter_config(anno_value, is_array, op_str, mode)
    if match:
        match_value_to_annotation(anno_value, filter_config)

    alleles = create_annotated_alleles(session, anno_value)
    allele_ids = [al.id for al in alleles]

    # Filter alleles
    anno_filter = GenericAnnotationFilter(session, None)
    filtered_allele_ids = anno_filter.filter_with_alleles(allele_ids, filter_config)
    non_filtered_allele_ids = set(
        [al_id for al_id in allele_ids if al_id not in filtered_allele_ids]
    )

    # Filter alleles on annotations directly
    phony_filtered_allele_ids = phony_allele_filter(session, allele_ids, filter_config, op_func)
    phony_non_filtered_allele_ids = set(
        [al_id for al_id in allele_ids if al_id not in phony_filtered_allele_ids]
    )

    assert filtered_allele_ids == phony_filtered_allele_ids
    assert non_filtered_allele_ids == phony_non_filtered_allele_ids

    # Compare with manually curated results
    if expected_results is not None:
        assert set(allele_ids[x] for x in expected_results) == filtered_allele_ids


@ht.example(
    anno_value=AnnotationsValue(
        [
            {"prediction": {"arbitrary": {"arbitrary0": "foo", "arbitrary1": "bar"}}},
            {"prediction": {"arbitrary": {"arbitrary0": "foo1", "arbitrary1": "bar1"}}},
        ],
        type_str="string",
    ),
    negate=False,
    match=False,
    mode="all",
    filter_config=GenericAnnotationModel.GenericAnnotationConfig.model_validate(
        {
            "target": "prediction.arbitrary",
            "is_array": False,
            "rules": [
                {
                    "key": "arbitrary0",
                    "operator": "in",
                    "value": ["foo", "bar"],
                    "type_str": "string",
                },
                {
                    "key": "arbitrary1",
                    "operator": "in",
                    "value": ["foo", "bar"],
                    "type_str": "string",
                },
            ],
        }
    ),
    expected_results=[0],
)
@ht.example(  # negate the membership operator
    anno_value=AnnotationsValue(
        [
            {"prediction": {"arbitrary": {"arbitrary0": "foo", "arbitrary1": "bar"}}},
            {"prediction": {"arbitrary": {"arbitrary0": "foo1", "arbitrary1": "bar1"}}},
        ],
        type_str="string",
    ),
    negate=True,
    match=False,
    mode="all",
    filter_config=GenericAnnotationModel.GenericAnnotationConfig.model_validate(
        {
            "target": "prediction.arbitrary",
            "is_array": False,
            "rules": [
                {
                    "key": "arbitrary0",
                    "operator": "not_in",
                    "value": ["foo", "bar"],
                    "type_str": "string",
                },
                {
                    "key": "arbitrary1",
                    "operator": "not_in",
                    "value": ["foo", "bar"],
                    "type_str": "string",
                },
            ],
        }
    ),
    expected_results=[1],
)
@ht.given(
    st.one_of(
        arbitrary_annotations(
            elements=st.one_of(st.none(), st.integers()),
            array_strategy=st.booleans(),
            value_strategy=st.lists(st.integers(), min_size=1),
        ),
        arbitrary_annotations(
            elements=st.one_of(
                st.none(), st.floats(allow_infinity=False, allow_nan=False, width=16)
            ),
            array_strategy=st.booleans(),
            value_strategy=st.lists(
                st.floats(allow_infinity=False, allow_nan=False, width=16), min_size=1
            ),
        ),
        arbitrary_annotations(
            elements=st.one_of(st.none(), st.text(alphabet=string.printable)),
            array_strategy=st.booleans(),
            value_strategy=st.lists(st.text(alphabet=string.printable), min_size=1),
        ),
    ),
    st.booleans(),
    st.booleans(),
    st.one_of(st.just("all"), st.just("any")),
    st.just(None),
    st.just(None),
)
@ht.settings(deadline=None)
def test_membership(
    session: Session,
    anno_value: AnnotationsValue,
    negate: bool,
    match: bool,
    mode: Literal["all", "any"],
    filter_config: GenericAnnotationModel.GenericAnnotationConfig | None,
    expected_results: list[int] | None,
):
    """Test the membership operators"""

    def op_func(x: float | None, y: list[float]):
        return x in y if not negate else x not in y

    op_str = "in" if not negate else "not_in"

    if filter_config is None:
        is_array = is_target_array(anno_value)
        filter_config = build_filter_config(anno_value, is_array, op_str, mode)
    if match:
        match_value_to_annotation(anno_value, filter_config)

    alleles = create_annotated_alleles(session, anno_value)
    allele_ids = [al.id for al in alleles]

    # Filter alleles
    anno_filter = GenericAnnotationFilter(session, None)
    filtered_allele_ids = anno_filter.filter_with_alleles(allele_ids, filter_config)
    non_filtered_allele_ids = set(
        [al_id for al_id in allele_ids if al_id not in filtered_allele_ids]
    )

    # Filter alleles on annotations directly
    phony_filtered_allele_ids = phony_allele_filter(session, allele_ids, filter_config, op_func)
    phony_non_filtered_allele_ids = set(
        [al_id for al_id in allele_ids if al_id not in phony_filtered_allele_ids]
    )

    assert filtered_allele_ids == phony_filtered_allele_ids
    assert non_filtered_allele_ids == phony_non_filtered_allele_ids

    # Compare with manually curated results
    if expected_results is not None:
        assert set(allele_ids[x] for x in expected_results) == filtered_allele_ids


def contains_func(x: list[float | str], y: list[float | str], negate: bool):
    result = all(val in x for val in y)
    return result if not negate else not result


@ht.example(
    anno_value=AnnotationsValue(
        [
            {"prediction": {"arbitrary": {"arbitrary0": [0.3, 0.8, 0.99]}}},
            {"prediction": {"arbitrary": {"arbitrary0": [0.3, 0.25, 0.12]}}},
        ],
        type_str="number",
    ),
    negate=False,
    match=False,
    mode="all",
    filter_config=GenericAnnotationModel.GenericAnnotationConfig.model_validate(
        {
            "target": "prediction.arbitrary",
            "is_array": False,
            "rules": [
                {
                    "key": "arbitrary0",
                    "operator": "contains",
                    "value": [0.3, 0.8],
                    "type_str": "number",
                },
            ],
        }
    ),
    expected_results=[0],
)
@ht.example(
    anno_value=AnnotationsValue(
        [
            {"prediction": {"arbitrary": {"arbitrary0": [0.3, 0.8, 0.99]}}},
            {"prediction": {"arbitrary": {"arbitrary0": [0.3, 0.25, 0.12]}}},
        ],
        type_str="number",
    ),
    negate=False,
    match=False,
    mode="all",
    filter_config=GenericAnnotationModel.GenericAnnotationConfig.model_validate(
        {
            "target": "prediction.arbitrary",
            "is_array": False,
            "rules": [
                {
                    "key": "arbitrary0",
                    "operator": "contains",
                    "value": [0.3, 0.8, 0.9],
                    "type_str": "number",
                },
            ],
        }
    ),
    expected_results=[],
)
@ht.given(
    st.one_of(
        arbitrary_annotations(
            elements=st.one_of(st.none(), st.lists(st.integers(), min_size=1)),
            array_strategy=st.booleans(),
            value_strategy=st.lists(st.integers(), min_size=1),
        ),
        arbitrary_annotations(
            elements=st.one_of(
                st.none(),
                st.lists(st.floats(allow_infinity=False, allow_nan=False, width=16), min_size=1),
            ),
            array_strategy=st.booleans(),
            value_strategy=st.lists(
                st.floats(allow_infinity=False, allow_nan=False, width=16), min_size=1
            ),
        ),
        arbitrary_annotations(
            elements=st.one_of(st.none(), st.lists(st.text(alphabet=string.printable), min_size=1)),
            array_strategy=st.booleans(),
            value_strategy=st.lists(st.text(alphabet=string.printable), min_size=1),
        ),
    ),
    st.booleans(),
    st.booleans(),
    st.one_of(st.just("all"), st.just("any")),
    st.just(None),
    st.just(None),
)
@ht.settings(deadline=None)
def test_array_contains(
    session: Session,
    anno_value: AnnotationsValue,
    negate: bool,
    match: bool,
    mode: Literal["all", "any"],
    filter_config: GenericAnnotationModel.GenericAnnotationConfig | None,
    expected_results: list[int] | None,
):
    """Test the array contains operators"""
    op_func = partial(contains_func, negate=negate)
    op_str = "contains" if not negate else "not_contains"

    if filter_config is None:
        is_array = is_target_array(anno_value)
        filter_config = build_filter_config(anno_value, is_array, op_str, mode)
    if match:
        match_value_to_annotation(anno_value, filter_config)

    alleles = create_annotated_alleles(session, anno_value)
    allele_ids = [al.id for al in alleles]

    # Filter alleles
    anno_filter = GenericAnnotationFilter(session, None)
    filtered_allele_ids = anno_filter.filter_with_alleles(allele_ids, filter_config)
    non_filtered_allele_ids = set(
        [al_id for al_id in allele_ids if al_id not in filtered_allele_ids]
    )

    # Filter alleles on annotations directly
    phony_filtered_allele_ids = phony_allele_filter(session, allele_ids, filter_config, op_func)
    phony_non_filtered_allele_ids = set(
        [al_id for al_id in allele_ids if al_id not in phony_filtered_allele_ids]
    )

    assert filtered_allele_ids == phony_filtered_allele_ids
    assert non_filtered_allele_ids == phony_non_filtered_allele_ids

    # Compare with manually curated results
    if expected_results is not None:
        assert set(allele_ids[x] for x in expected_results) == filtered_allele_ids


# TODO Support for wildcard characters?
@ht.example(
    anno_value=AnnotationsValue(
        [
            {"prediction": {"arbitrary": {"arbitrary0": "str_with_foo_and_bar"}}},
            {"prediction": {"arbitrary": {"arbitrary0": "str_with_bar_only"}}},
        ],
        type_str="string",
    ),
    negate=False,
    match=False,
    mode="all",
    filter_config=GenericAnnotationModel.GenericAnnotationConfig.model_validate(
        {
            "target": "prediction.arbitrary",
            "is_array": False,
            "rules": [
                {
                    "key": "arbitrary0",
                    "operator": "str_contains",
                    "value": "foo",
                    "type_str": "string",
                },
                {
                    "key": "arbitrary0",
                    "operator": "str_contains",
                    "value": "bar",
                    "type_str": "string",
                },
            ],
        }
    ),
    expected_results=[0],
)
@ht.given(
    st.one_of(
        arbitrary_annotations(
            elements=st.one_of(
                st.none(),
                st.text(
                    alphabet=string.printable.replace("%", "").replace("_", "").replace("\\", "")
                ),
            ),
            array_strategy=st.booleans(),
            value_strategy=st.text(
                alphabet=string.printable.replace("%", "").replace("_", "").replace("\\", ""),
                min_size=1,
                max_size=3,
            ),
        ),
    ),
    st.booleans(),
    st.booleans(),
    st.one_of(st.just("all"), st.just("any")),
    st.just(None),
    st.just(None),
)
@ht.settings(deadline=None)
def test_str_contains(
    session: Session,
    anno_value: AnnotationsValue,
    negate: bool,
    match: bool,
    mode: Literal["all", "any"],
    filter_config: GenericAnnotationModel.GenericAnnotationConfig | None,
    expected_results: list[int] | None,
):
    """Test the string membership operators (i.e. string contains)"""

    def op_func(x: str, y: str):
        return y in x if not negate else y not in x  # opposite of the membership operator

    op_str = "str_contains" if not negate else "str_not_contains"

    if filter_config is None:
        is_array = is_target_array(anno_value)
        filter_config = build_filter_config(anno_value, is_array, op_str, mode)
    if match:
        match_value_to_annotation(anno_value, filter_config)

    alleles = create_annotated_alleles(session, anno_value)
    allele_ids = [al.id for al in alleles]

    # Filter alleles
    anno_filter = GenericAnnotationFilter(session, None)
    filtered_allele_ids = anno_filter.filter_with_alleles(allele_ids, filter_config)
    non_filtered_allele_ids = set(
        [al_id for al_id in allele_ids if al_id not in filtered_allele_ids]
    )

    # Filter alleles on annotations directly
    phony_filtered_allele_ids = phony_allele_filter(session, allele_ids, filter_config, op_func)
    phony_non_filtered_allele_ids = set(
        [al_id for al_id in allele_ids if al_id not in phony_filtered_allele_ids]
    )

    assert filtered_allele_ids == phony_filtered_allele_ids
    assert non_filtered_allele_ids == phony_non_filtered_allele_ids

    # Compare with manually curated results
    if expected_results is not None:
        assert set(allele_ids[x] for x in expected_results) == filtered_allele_ids


def overlap_func(x: list[float | str], y: list[float | str], negate: bool):
    result = any(val in x for val in y)
    return result if not negate else not result


@ht.example(
    anno_value=AnnotationsValue(
        [
            {"prediction": {"arbitrary": {"arbitrary0": [0.3, 0.8, 0.99]}}},
            {"prediction": {"arbitrary": {"arbitrary0": [0.3, 0.25, 0.12]}}},
        ],
        type_str="number",
    ),
    negate=False,
    match=False,
    mode="all",
    filter_config=GenericAnnotationModel.GenericAnnotationConfig.model_validate(
        {
            "target": "prediction.arbitrary",
            "is_array": False,
            "rules": [
                {
                    "key": "arbitrary0",
                    "operator": "overlap",
                    "value": [0.3, 0.8, 0.9],
                    "type_str": "number",
                },
            ],
        }
    ),
    expected_results=[0, 1],
)
@ht.given(
    st.one_of(
        arbitrary_annotations(
            elements=st.one_of(st.none(), st.lists(st.integers(), min_size=1)),
            array_strategy=st.booleans(),
            value_strategy=st.lists(st.integers(), min_size=1),
        ),
        arbitrary_annotations(
            elements=st.one_of(
                st.none(),
                st.lists(st.floats(allow_infinity=False, allow_nan=False, width=16), min_size=1),
            ),
            array_strategy=st.booleans(),
            value_strategy=st.lists(
                st.floats(allow_infinity=False, allow_nan=False, width=16), min_size=1
            ),
        ),
        arbitrary_annotations(
            elements=st.one_of(st.none(), st.lists(st.text(alphabet=string.printable), min_size=1)),
            array_strategy=st.booleans(),
            value_strategy=st.lists(st.text(alphabet=string.printable), min_size=1),
        ),
    ),
    st.booleans(),
    st.booleans(),
    st.one_of(st.just("all"), st.just("any")),
    st.just(None),
    st.just(None),
)
@ht.settings(deadline=None)
def test_array_overlap(
    session: Session,
    anno_value: AnnotationsValue,
    negate: bool,
    match: bool,
    mode: Literal["all", "any"],
    filter_config: GenericAnnotationModel.GenericAnnotationConfig | None,
    expected_results: list[int] | None,
):
    """Test the array overlap operators"""
    op_func = partial(overlap_func, negate=negate)
    op_str = "overlap" if not negate else "not_overlap"

    if filter_config is None:
        is_array = is_target_array(anno_value)
        filter_config = build_filter_config(anno_value, is_array, op_str, mode)
    if match:
        match_value_to_annotation(anno_value, filter_config)

    alleles = create_annotated_alleles(session, anno_value)
    allele_ids = [al.id for al in alleles]

    # Filter alleles
    anno_filter = GenericAnnotationFilter(session, None)
    filtered_allele_ids = anno_filter.filter_with_alleles(allele_ids, filter_config)
    non_filtered_allele_ids = set(
        [al_id for al_id in allele_ids if al_id not in filtered_allele_ids]
    )

    # Filter alleles on annotations directly
    phony_filtered_allele_ids = phony_allele_filter(session, allele_ids, filter_config, op_func)
    phony_non_filtered_allele_ids = set(
        [al_id for al_id in allele_ids if al_id not in phony_filtered_allele_ids]
    )

    assert filtered_allele_ids == phony_filtered_allele_ids
    assert non_filtered_allele_ids == phony_non_filtered_allele_ids

    # Compare with manually curated results
    if expected_results is not None:
        assert set(allele_ids[x] for x in expected_results) == filtered_allele_ids
