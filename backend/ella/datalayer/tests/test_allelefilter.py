"""
Integration/unit test for the AlleleFilter module.
Since it consists mostly of database queries, it's tested on a live database.
"""
from __future__ import annotations

from typing import TYPE_CHECKING, Any, override

import pytest
from pydantic import BaseModel

from ella.datalayer import AlleleFilter
from ella.datalayer.allelefilter.filterbase import FilterBase

if TYPE_CHECKING:
    from collections.abc import Callable

    from sqlalchemy.orm.session import Session

FILTER_CONFIG_NUM = 0


class TestFilterConfigModel(BaseModel):
    """Test model for FilterConfigModel to accommodate the mock filters"""

    __test__ = False

    class FilterModel(BaseModel):
        name: str
        config: None = None
        exceptions: list[TestFilterConfigModel.FilterModel] = []

    filters: list[FilterModel]


def build_filter_config(filter_config: dict[str, Any]):
    return TestFilterConfigModel(**filter_config)


def mock_filters(
    filter_with_alleles: Callable[[list[int]], set[int]],
) -> tuple[type[FilterBase], type[FilterBase], type[FilterBase]]:
    class Filter(FilterBase):
        CONTEXT_DEPENDENT = False
        FILTER_WITH = "allele"

        @override
        def filter_with_alleles(self, allele_ids, filter_config):
            return filter_with_alleles(allele_ids)

    class FilterAnalysis(FilterBase):
        FILTER_WITH = "analysis"

        @override
        def filter_with_analysis(
            self, analysis_id: int, allele_ids: list[int], filter_config: dict[str, Any]
        ) -> set[int]:
            return filter_with_alleles(allele_ids)

    class FilterGenepanel(FilterBase):
        FILTER_WITH = "genepanel"

        @override
        def filter_with_genepanel(
            self, gp_key: tuple[str, str], allele_ids: list[int], filter_config: dict[str, Any]
        ) -> set[int]:
            return filter_with_alleles(allele_ids)

    return Filter, FilterAnalysis, FilterGenepanel


@pytest.fixture
def allele_filter(session: Session):
    allele_filter = AlleleFilter(session, config={})

    # Mock the built-in filters

    FilterOne, FilterOneAnalysis, FilterOneGenepanel = mock_filters(
        lambda allele_ids: set(allele_ids) & set([1])
    )

    FilterOneTwo, FilterOneTwoAnalysis, FilterOneTwoGenepanel = mock_filters(
        lambda allele_ids: set(allele_ids) & set([1, 2])
    )

    FilterThreeFour, FilterThreeFourAnalysis, FilterThreeFourGenepanel = mock_filters(
        lambda allele_ids: set(allele_ids) & set([3, 4])
    )

    FilterFiveSix, FilterFiveSixAnalysis, FilterFiveSixGenepanel = mock_filters(
        lambda allele_ids: set(allele_ids) & set([5, 6])
    )

    FilterNone, FilterNoneAnalysis, FilterNoneGenepanel = mock_filters(lambda allele_ids: set([]))

    FilterOneThreeIfOne, FilterOneThreeIfOneAnalysis, FilterOneThreeIfOneGenepanel = mock_filters(
        lambda allele_ids: set(allele_ids) & set([1, 3] if 1 in allele_ids else set())
    )

    assert FilterOneThreeIfOne(session, None).filter_with_alleles([1, 3], dict()) == set([1, 3])
    assert FilterOneThreeIfOne(session, None).filter_with_alleles([3], dict()) == set([])
    assert FilterOneThreeIfOne(session, None).filter_with_alleles([1], dict()) == set([1])

    allele_filter.filter_objects = {
        "allele_one": FilterOne(session, None),
        "allele_one_two": FilterOneTwo(session, None),
        "allele_duplicate_one_two": FilterOneTwo(session, None),
        "allele_three_four": FilterThreeFour(session, None),
        "allele_five_six": FilterFiveSix(session, None),
        "allele_filter_one_three_if_one": FilterOneThreeIfOne(session, None),
        "allele_none": FilterNone(session, None),
        "analysis_one_two": FilterOneTwoAnalysis(session, None),
        "analysis_duplicate_one_two": FilterOneTwoAnalysis(session, None),
        "analysis_three_four": FilterThreeFourAnalysis(session, None),
        "analysis_five_six": FilterFiveSixAnalysis(session, None),
        "analysis_filter_one_three_if_one": FilterOneThreeIfOneAnalysis(session, None),
        "analysis_none": FilterNoneAnalysis(session, None),
        "genepanel_one_two": FilterOneTwoGenepanel(session, None),
        "genepanel_duplicate_one_two": FilterOneTwoGenepanel(session, None),
        "genepanel_three_four": FilterThreeFourGenepanel(session, None),
        "genepanel_five_six": FilterFiveSixGenepanel(session, None),
        "genepanel_filter_one_three_if_one": FilterOneThreeIfOneGenepanel(session, None),
        "genepanel_none": FilterNoneGenepanel(session, None),
    }

    return allele_filter


class TestAlleleFilter:
    def test_filter_alleles(self, allele_filter: AlleleFilter):
        # ---------

        # Test simple allele filter
        filter_config = build_filter_config({"filters": [{"name": "allele_one_two"}]})

        testdata = [1, 2]
        result = allele_filter.run_filter_chain(filter_config, 1, testdata)
        expected_result: dict = {
            "allele_ids": [],
            "excluded_allele_ids": {"allele_one_two": [1, 2]},
        }

        assert result == expected_result

        # ---------

        testdata = [1, 4]
        result = allele_filter.run_filter_chain(filter_config, 1, testdata)
        expected_result = {"allele_ids": [4], "excluded_allele_ids": {"allele_one_two": [1]}}
        assert result == expected_result

        # Test multiple allele filters
        filter_config = build_filter_config(
            {
                "filters": [
                    {"name": "allele_one_two"},
                    {"name": "allele_duplicate_one_two"},
                    {"name": "allele_three_four"},
                    {"name": "allele_five_six"},
                    {"name": "allele_none"},
                ]
            }
        )

        testdata = [1, 2, 3, 4, 5, 6, 7, 8, 9]

        result = allele_filter.run_filter_chain(filter_config, 1, testdata)

        expected_result = {
            "allele_ids": [7, 8, 9],
            "excluded_allele_ids": {
                "allele_one_two": [1, 2],
                "allele_duplicate_one_two": [],
                "allele_three_four": [3, 4],
                "allele_five_six": [5, 6],
                "allele_none": [],
            },
        }
        assert result == expected_result

        # ---------

        # Test exceptions

        # Test allele exception on allele filter
        filter_config = build_filter_config(
            {"filters": [{"name": "allele_one_two", "exceptions": [{"name": "allele_one"}]}]}
        )

        testdata = [1, 2, 3, 4]

        result = allele_filter.run_filter_chain(filter_config, 1, testdata)

        expected_result = {
            "allele_ids": [1, 3, 4],
            "excluded_allele_ids": {"allele_one_two": [2]},
        }

        assert result == expected_result

        # ---------
        # Test that exceptions only apply to the filter specified to
        filter_config = build_filter_config(
            {
                "filters": [
                    {"name": "allele_one_two", "exceptions": [{"name": "allele_three_four"}]},
                    {"name": "allele_three_four", "exceptions": [{"name": "allele_one_two"}]},
                ]
            }
        )

        testdata = [1, 2, 3, 4]

        result = allele_filter.run_filter_chain(filter_config, 1, testdata)

        expected_result = {
            "allele_ids": [],
            "excluded_allele_ids": {
                "allele_one_two": [1, 2],
                "allele_three_four": [3, 4],
            },
        }

        assert result == expected_result

    def testfilter_with_analysis(self, allele_filter: AlleleFilter):
        # ---------

        # Test single analysis filter
        filter_config = build_filter_config({"filters": [{"name": "analysis_one_two"}]})

        testdata = [1, 2, 3, 4]

        result = allele_filter.run_filter_chain(filter_config, 1, testdata)

        expected_result = {
            "allele_ids": [3, 4],
            "excluded_allele_ids": {"analysis_one_two": [1, 2]},
        }
        assert result == expected_result

        # ---------

        # Test multiple analysis filters
        filter_config = build_filter_config(
            {
                "filters": [
                    {"name": "analysis_one_two"},
                    {"name": "analysis_duplicate_one_two"},
                    {"name": "analysis_three_four"},
                    {"name": "analysis_five_six"},
                    {"name": "analysis_none"},
                ]
            }
        )

        testdata = [1, 2, 3, 4, 5, 6, 7, 8, 9]

        result = allele_filter.run_filter_chain(filter_config, 1, testdata)
        expected_result = {
            "allele_ids": [7, 8, 9],
            "excluded_allele_ids": {
                "analysis_one_two": [1, 2],
                "analysis_duplicate_one_two": [],
                "analysis_three_four": [3, 4],
                "analysis_five_six": [5, 6],
                "analysis_none": [],
            },
        }
        assert result == expected_result

        # ---------

        # Test combining analysis and allele filters
        filter_config = build_filter_config(
            {
                "filters": [
                    # Overlapping allele and analysis filter
                    {"name": "allele_one_two"},
                    {"name": "analysis_one_two"},
                    {"name": "analysis_three_four"},
                    {"name": "allele_five_six"},
                    {"name": "analysis_none"},
                ]
            }
        )

        testdata = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        result = allele_filter.run_filter_chain(filter_config, 1, testdata)

        expected_result = {
            "allele_ids": [7, 8, 9],
            "excluded_allele_ids": {
                "allele_one_two": [1, 2],
                "analysis_one_two": [],
                "analysis_three_four": [3, 4],
                "allele_five_six": [5, 6],
                "analysis_none": [],
            },
        }
        assert result == expected_result

        # ---------
        # Test allele exception on analysis filter
        filter_config = build_filter_config(
            {"filters": [{"name": "analysis_one_two", "exceptions": [{"name": "allele_one"}]}]}
        )

        testdata = [1, 2, 3, 4]
        result = allele_filter.run_filter_chain(filter_config, 1, testdata)

        expected_result = {
            "allele_ids": [1, 3, 4],
            "excluded_allele_ids": {"analysis_one_two": [2]},
        }

        assert result == expected_result

        # ---------
        # Test analysis exception on analysis filter
        filter_config = build_filter_config(
            {
                "filters": [
                    {"name": "analysis_one_two", "exceptions": [{"name": "analysis_one_two"}]},
                    {"name": "analysis_three_four", "exceptions": [{"name": "analysis_one_two"}]},
                ]
            }
        )

        testdata = [1, 2, 3, 4]

        result = allele_filter.run_filter_chain(filter_config, 1, testdata)

        expected_result = {
            "allele_ids": [1, 2],
            "excluded_allele_ids": {
                "analysis_one_two": [],
                "analysis_three_four": [3, 4],
            },
        }

        assert result == expected_result

        # ---------

        filter_config = build_filter_config(
            {"filters": [{"name": "analysis_one_two"}, {"name": "allele_one_two"}]}
        )

        testdata = [1, 2, 3, 4]

        result = allele_filter.run_filter_chain(filter_config, 1, testdata)

        expected_result = {
            "allele_ids": [3, 4],
            "excluded_allele_ids": {"analysis_one_two": [1, 2], "allele_one_two": []},
        }
        assert result == expected_result

        # ---------
        # Test filters working conditionally to make sure
        # previously filtered are not sent to next
        # Four cases

        # analysis -> analysis

        filter_config = build_filter_config(
            {
                "filters": [
                    {"name": "analysis_one_two"},
                    {"name": "analysis_filter_one_three_if_one"},
                ]
            }
        )

        testdata = [1, 2, 3, 4]
        result = allele_filter.run_filter_chain(filter_config, 1, testdata)
        expected_result = {
            "allele_ids": [3, 4],
            "excluded_allele_ids": {
                "analysis_one_two": [1, 2],
                "analysis_filter_one_three_if_one": [],
            },
        }
        assert result == expected_result

        # allele -> analysis
        filter_config = build_filter_config(
            {"filters": [{"name": "allele_one_two"}, {"name": "analysis_filter_one_three_if_one"}]}
        )

        testdata = [1, 2, 3, 4]
        result = allele_filter.run_filter_chain(filter_config, 1, testdata)
        expected_result = {
            "allele_ids": [3, 4],
            "excluded_allele_ids": {
                "allele_one_two": [1, 2],
                "analysis_filter_one_three_if_one": [],
            },
        }
        assert result == expected_result

        # allele -> analysis
        filter_config = build_filter_config(
            {"filters": [{"name": "analysis_one_two"}, {"name": "allele_filter_one_three_if_one"}]}
        )

        testdata = [1, 2, 3, 4]
        result = allele_filter.run_filter_chain(filter_config, 1, testdata)
        expected_result = {
            "allele_ids": [3, 4],
            "excluded_allele_ids": {
                "analysis_one_two": [1, 2],
                "allele_filter_one_three_if_one": [],
            },
        }
        assert result == expected_result

        # allele -> allele
        filter_config = build_filter_config(
            {"filters": [{"name": "allele_one_two"}, {"name": "allele_filter_one_three_if_one"}]}
        )

        testdata = [1, 2, 3, 4]
        result = allele_filter.run_filter_chain(filter_config, 1, testdata)
        expected_result = {
            "allele_ids": [3, 4],
            "excluded_allele_ids": {
                "allele_one_two": [1, 2],
                "allele_filter_one_three_if_one": [],
            },
        }
        assert result == expected_result

    def testfilter_with_genepanel(self, allele_filter: AlleleFilter):
        # ---------

        # Test single analysis filter
        filter_config = build_filter_config({"filters": [{"name": "genepanel_one_two"}]})

        testdata = [1, 2, 3, 4]

        result = allele_filter.run_filter_chain(filter_config, 1, testdata)

        expected_result = {
            "allele_ids": [3, 4],
            "excluded_allele_ids": {"genepanel_one_two": [1, 2]},
        }
        assert result == expected_result

        # ---------

        # Test multiple analysis filters
        filter_config = build_filter_config(
            {
                "filters": [
                    {"name": "genepanel_one_two"},
                    {"name": "genepanel_duplicate_one_two"},
                    {"name": "genepanel_three_four"},
                    {"name": "genepanel_five_six"},
                    {"name": "genepanel_none"},
                ]
            }
        )

        testdata = [1, 2, 3, 4, 5, 6, 7, 8, 9]

        result = allele_filter.run_filter_chain(filter_config, 1, testdata)
        expected_result = {
            "allele_ids": [7, 8, 9],
            "excluded_allele_ids": {
                "genepanel_one_two": [1, 2],
                "genepanel_duplicate_one_two": [],
                "genepanel_three_four": [3, 4],
                "genepanel_five_six": [5, 6],
                "genepanel_none": [],
            },
        }
        assert result == expected_result

        # ---------

        # Test combining analysis and allele filters
        filter_config = build_filter_config(
            {
                "filters": [
                    # Overlapping allele and analysis filter
                    {"name": "allele_one_two"},
                    {"name": "genepanel_one_two"},
                    {"name": "genepanel_three_four"},
                    {"name": "allele_five_six"},
                    {"name": "genepanel_none"},
                ]
            }
        )

        testdata = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        result = allele_filter.run_filter_chain(filter_config, 1, testdata)

        expected_result = {
            "allele_ids": [7, 8, 9],
            "excluded_allele_ids": {
                "allele_one_two": [1, 2],
                "genepanel_one_two": [],
                "genepanel_three_four": [3, 4],
                "allele_five_six": [5, 6],
                "genepanel_none": [],
            },
        }
        assert result == expected_result

        # ---------
        # Test allele exception on analysis filter
        filter_config = build_filter_config(
            {"filters": [{"name": "genepanel_one_two", "exceptions": [{"name": "allele_one"}]}]}
        )

        testdata = [1, 2, 3, 4]
        result = allele_filter.run_filter_chain(filter_config, 1, testdata)

        expected_result = {
            "allele_ids": [1, 3, 4],
            "excluded_allele_ids": {"genepanel_one_two": [2]},
        }

        assert result == expected_result

        # ---------
        # Test analysis exception on analysis filter
        filter_config = build_filter_config(
            {
                "filters": [
                    {"name": "genepanel_one_two", "exceptions": [{"name": "genepanel_one_two"}]},
                    {"name": "genepanel_three_four", "exceptions": [{"name": "genepanel_one_two"}]},
                ]
            }
        )

        testdata = [1, 2, 3, 4]

        result = allele_filter.run_filter_chain(filter_config, 1, testdata)

        expected_result = {
            "allele_ids": [1, 2],
            "excluded_allele_ids": {
                "genepanel_one_two": [],
                "genepanel_three_four": [3, 4],
            },
        }

        assert result == expected_result

        # ---------

        filter_config = build_filter_config(
            {"filters": [{"name": "genepanel_one_two"}, {"name": "allele_one_two"}]}
        )

        testdata = [1, 2, 3, 4]

        result = allele_filter.run_filter_chain(filter_config, 1, testdata)

        expected_result = {
            "allele_ids": [3, 4],
            "excluded_allele_ids": {"genepanel_one_two": [1, 2], "allele_one_two": []},
        }
        assert result == expected_result

        # ---------
        # Test filters working conditionally to make sure
        # previously filtered are not sent to next
        # Four cases

        # analysis -> analysis

        filter_config = build_filter_config(
            {
                "filters": [
                    {"name": "genepanel_one_two"},
                    {"name": "genepanel_filter_one_three_if_one"},
                ]
            }
        )

        testdata = [1, 2, 3, 4]
        result = allele_filter.run_filter_chain(filter_config, 1, testdata)
        expected_result = {
            "allele_ids": [3, 4],
            "excluded_allele_ids": {
                "genepanel_one_two": [1, 2],
                "genepanel_filter_one_three_if_one": [],
            },
        }
        assert result == expected_result

        # allele -> analysis
        filter_config = build_filter_config(
            {"filters": [{"name": "allele_one_two"}, {"name": "genepanel_filter_one_three_if_one"}]}
        )

        testdata = [1, 2, 3, 4]
        result = allele_filter.run_filter_chain(filter_config, 1, testdata)
        expected_result = {
            "allele_ids": [3, 4],
            "excluded_allele_ids": {
                "allele_one_two": [1, 2],
                "genepanel_filter_one_three_if_one": [],
            },
        }
        assert result == expected_result

        # allele -> analysis
        filter_config = build_filter_config(
            {"filters": [{"name": "genepanel_one_two"}, {"name": "allele_filter_one_three_if_one"}]}
        )

        testdata = [1, 2, 3, 4]
        result = allele_filter.run_filter_chain(filter_config, 1, testdata)
        expected_result = {
            "allele_ids": [3, 4],
            "excluded_allele_ids": {
                "genepanel_one_two": [1, 2],
                "allele_filter_one_three_if_one": [],
            },
        }
        assert result == expected_result

        # allele -> allele
        filter_config = build_filter_config(
            {"filters": [{"name": "allele_one_two"}, {"name": "allele_filter_one_three_if_one"}]}
        )

        testdata = [1, 2, 3, 4]
        result = allele_filter.run_filter_chain(filter_config, 1, testdata)
        expected_result = {
            "allele_ids": [3, 4],
            "excluded_allele_ids": {
                "allele_one_two": [1, 2],
                "allele_filter_one_three_if_one": [],
            },
        }
        assert result == expected_result
