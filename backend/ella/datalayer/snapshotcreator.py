import itertools
from collections.abc import Sequence
from typing import TYPE_CHECKING

from sqlalchemy import insert, select
from sqlalchemy.orm.session import Session

from ella.api.util.types import FilteredAlleleCategories
from ella.datalayer import filters
from ella.vardb.datamodel import Base, annotation, assessment, workflow

if TYPE_CHECKING:
    from sqlalchemy.orm.attributes import InstrumentedAttribute


class SnapshotCreator:
    def __init__(self, session: Session):
        self.session = session

    def _allele_id_model_id(self, model: type[Base], model_ids: Sequence[int]):
        allele_id: InstrumentedAttribute[int] = getattr(model, "allele_id")
        id: InstrumentedAttribute[int] = getattr(model, "id")
        allele_ids_model_ids = self.session.execute(
            select(allele_id, id).where(filters.in_(id, model_ids))
        ).all()
        assert len(allele_ids_model_ids) == len(model_ids)

        return {a[0]: a[1] for a in allele_ids_model_ids}

    def insert_from_data(
        self,
        allele_ids: Sequence[int],
        interpretation: workflow.AnalysisInterpretation | workflow.AlleleInterpretation,
        annotation_ids: Sequence[int],
        custom_annotation_ids: Sequence[int],
        alleleassessment_ids: Sequence[int],
        allelereport_ids: Sequence[int],
        excluded_allele_ids: dict | None = None,
    ) -> Sequence[dict]:
        excluded: dict = {}

        if isinstance(interpretation, workflow.AnalysisInterpretation):
            assert excluded_allele_ids is not None
            excluded = excluded_allele_ids

            all_allele_ids = list(
                set(allele_ids).union(set(itertools.chain(*list(excluded.values()))))
            )

        # 'excluded' is not a concept for alleleinterpretation
        elif isinstance(interpretation, workflow.AlleleInterpretation):
            all_allele_ids = [interpretation.allele_id]

        allele_ids_annotation_ids = self._allele_id_model_id(annotation.Annotation, annotation_ids)
        allele_ids_custom_annotation_ids = self._allele_id_model_id(
            annotation.CustomAnnotation, custom_annotation_ids
        )
        allele_ids_alleleassessment_ids = self._allele_id_model_id(
            assessment.AlleleAssessment, alleleassessment_ids
        )
        allele_ids_allelereport_ids = self._allele_id_model_id(
            assessment.AlleleReport, allelereport_ids
        )

        snapshot_items = list()
        for allele_id in all_allele_ids:
            # Check if allele_id is in any of the excluded categories
            excluded_category = next((k for k, v in excluded.items() if allele_id in v), None)

            snapshot_item = {
                "allele_id": allele_id,
                "annotation_id": allele_ids_annotation_ids.get(allele_id),
                "customannotation_id": allele_ids_custom_annotation_ids.get(allele_id),
                "alleleassessment_id": allele_ids_alleleassessment_ids.get(allele_id),
                "allelereport_id": allele_ids_allelereport_ids.get(allele_id),
            }

            if isinstance(interpretation, workflow.AnalysisInterpretation):
                snapshot_item["analysisinterpretation_id"] = interpretation.id
                snapshot_item["filtered"] = (
                    FilteredAlleleCategories(excluded_category).name
                    if excluded_category is not None
                    else None
                )
            elif isinstance(interpretation, workflow.AlleleInterpretation):
                snapshot_item["alleleinterpretation_id"] = interpretation.id

            snapshot_items.append(snapshot_item)

        if snapshot_items:
            if isinstance(interpretation, workflow.AnalysisInterpretation):
                self.session.execute(
                    insert(workflow.AnalysisInterpretationSnapshot),
                    snapshot_items,
                )
            elif isinstance(interpretation, workflow.AlleleInterpretation):
                self.session.execute(insert(workflow.AlleleInterpretationSnapshot), snapshot_items)

        return snapshot_items
