import datetime
from collections.abc import Sequence
from typing import TYPE_CHECKING, Literal

import pytz
from sqlalchemy import Select, Text, and_, func, literal_column, or_, select, text
from sqlalchemy.orm import Session
from sqlalchemy.sql.sqltypes import Integer

from ella.api.config import config
from ella.api.util import filterconfig_requirements
from ella.datalayer import filters
from ella.vardb.datamodel import (
    allele,
    annotation,
    annotationshadow,
    assessment,
    gene,
    sample,
    workflow,
)
from ella.vardb.datamodel import user as user_module
from ella.vardb.util.sql_utils import create_temp_table

if TYPE_CHECKING:
    from sqlalchemy.sql.elements import ColumnClause


def valid_alleleassessments_filter():
    """
    Filter for including alleleassessments that have valid (not outdated) classifications.
    """
    classification_filters = []
    # Create classification filters, filtering on classification and optionally outdated threshold
    for option in config["classification"]["options"]:
        internal_filters = [assessment.AlleleAssessment.classification == option["value"]]
        if "outdated_after_days" in option:
            outdated_time = datetime.datetime.now(pytz.utc) - datetime.timedelta(
                days=option["outdated_after_days"]
            )
            internal_filters.append(assessment.AlleleAssessment.date_created > outdated_time)
        # Add our filter using and_
        classification_filters.append(and_(*internal_filters))
    return [or_(*classification_filters), assessment.AlleleAssessment.date_superceeded.is_(None)]


def allele_ids_with_valid_alleleassessments():
    """
    Query for all alleles that has no valid alleleassessments,
    as given by configuration's classification options.

    Different scenarios:
    - Allele has alleleassessment and it's not outdated
    - Allele has alleleassessment, but it's outdated
    - Allele has no alleleassessments at all.

    """
    return (
        select(allele.Allele.id)
        .join(assessment.AlleleAssessment)
        .where(*valid_alleleassessments_filter())
    )


def workflow_by_status(
    model: type[workflow.AlleleInterpretation] | type[workflow.AnalysisInterpretation],
    model_id_attr: Literal["allele_id", "analysis_id"],
    workflow_status: str | None = None,
    status: str | None = None,
    finalized: bool | None = None,
) -> Select[tuple[int]]:
    """
    Fetches all allele_id/analysis_id where the last interpretation matches provided
    workflow status and/or status.

    :param model: AlleleInterpretation or AnalysisInterpretation
    :param model_id_attr: 'allele_id' or 'analysis_id'

    Query resembles something like this:
     SELECT s.id FROM (select DISTINCT ON (analysis_id) id, workflow_status, status
     from analysisinterpretation order by analysis_id, date_last_update desc) AS
     s where s.workflow_status = :status;

    Using DISTINCT ON and ORDER BY will select one row, giving us the latest interpretation workflow status.
    See https://www.postgresql.org/docs/10.0/static/sql-select.html#SQL-DISTINCT
    """

    if workflow_status is None and status is None and finalized is None:
        raise RuntimeError(
            "You must provide either 'workflow_status', 'status' or 'finalized' argument"
        )

    assert model_id_attr in ["allele_id", "analysis_id"]

    latest_interpretation = (
        select(getattr(model, model_id_attr), model.workflow_status, model.status, model.finalized)
        .order_by(getattr(model, model_id_attr), model.date_created.desc())
        .distinct(getattr(model, model_id_attr))  # DISTINCT ON
        .subquery()
    )

    workflow_filters = []
    if workflow_status:
        workflow_filters.append(latest_interpretation.c.workflow_status == workflow_status)
    if status:
        workflow_filters.append(latest_interpretation.c.status == status)
    if finalized is not None:
        if finalized:
            workflow_filters.append(latest_interpretation.c.finalized.is_(True))
        else:
            workflow_filters.append(
                or_(
                    latest_interpretation.c.finalized.is_(None),
                    latest_interpretation.c.finalized.is_(False),
                )
            )
    return select(getattr(latest_interpretation.c, model_id_attr).label(model_id_attr)).where(
        *workflow_filters
    )


def workflow_analyses_finalized():
    return workflow_by_status(
        workflow.AnalysisInterpretation,
        "analysis_id",
        workflow_status=None,
        status=None,
        finalized=True,
    )


def workflow_analyses_notready_not_started():
    return workflow_by_status(
        workflow.AnalysisInterpretation,
        "analysis_id",
        workflow_status="Not ready",
        status="Not started",
    )


def workflow_analyses_interpretation_not_started():
    return workflow_by_status(
        workflow.AnalysisInterpretation,
        "analysis_id",
        workflow_status="Interpretation",
        status="Not started",
    )


def workflow_analyses_review_not_started():
    return workflow_by_status(
        workflow.AnalysisInterpretation,
        "analysis_id",
        workflow_status="Review",
        status="Not started",
    )


def workflow_analyses_medicalreview_not_started():
    return workflow_by_status(
        workflow.AnalysisInterpretation,
        "analysis_id",
        workflow_status="Medical review",
        status="Not started",
    )


def workflow_analyses_ongoing():
    return workflow_by_status(
        workflow.AnalysisInterpretation,
        "analysis_id",
        workflow_status=None,
        status="Ongoing",
    )


def workflow_analyses_for_genepanels(genepanels: list[gene.Genepanel]):
    return select(sample.Analysis.id).where(
        filters.in_(
            (sample.Analysis.genepanel_name, sample.Analysis.genepanel_version),
            [(gp.name, gp.version) for gp in genepanels],
        )
    )


def workflow_alleles_finalized():
    return workflow_by_status(
        workflow.AlleleInterpretation,
        "allele_id",
        workflow_status=None,
        status=None,
        finalized=True,
    )


def workflow_alleles_interpretation_not_started():
    return workflow_by_status(
        workflow.AlleleInterpretation,
        "allele_id",
        workflow_status="Interpretation",
        status="Not started",
    )


def workflow_alleles_review_not_started():
    return workflow_by_status(
        workflow.AlleleInterpretation,
        "allele_id",
        workflow_status="Review",
        status="Not started",
    )


def workflow_alleles_ongoing():
    return workflow_by_status(
        workflow.AlleleInterpretation, "allele_id", workflow_status=None, status="Ongoing"
    )


def workflow_alleles_for_genepanels(genepanels: list[gene.Genepanel]):
    """
    Get all allele_ids for allele workflow connected to given genepanels.
    """
    allele_ids_for_alleleinterpretation = (
        select(workflow.AlleleInterpretation.allele_id)
        .where(
            filters.in_(
                (
                    workflow.AlleleInterpretation.genepanel_name,
                    workflow.AlleleInterpretation.genepanel_version,
                ),
                [(gp.name, gp.version) for gp in genepanels],
            )
        )
        .distinct()
    )

    return allele_ids_for_alleleinterpretation


def analysis_ids_for_user(user: user_module.User):
    # Restrict analyses to analyses matching this user's group's genepanels
    analysis_ids_for_user = select(sample.Analysis.id).where(
        filters.in_(
            sample.Analysis.id,
            workflow_analyses_for_genepanels(user.group.genepanels),
        )
    )
    return analysis_ids_for_user


def latest_interpretationlog_field(
    model: type[workflow.AnalysisInterpretation] | type[workflow.AlleleInterpretation],
    model_id_attr: str,
    field: str,
    model_ids: list[int] | None = None,
):
    interpretationlog_filters = [~getattr(workflow.InterpretationLog, field).is_(None)]
    if model_ids:
        interpretationlog_filters.append(filters.in_(getattr(model, model_id_attr), model_ids))
    return (
        select(getattr(model, model_id_attr), getattr(workflow.InterpretationLog, field))
        .join(workflow.InterpretationLog)
        .where(*interpretationlog_filters)
        .order_by(getattr(model, model_id_attr), workflow.InterpretationLog.date_created.desc())
        .distinct(getattr(model, model_id_attr))
    )


def workflow_analyses_priority(analysis_ids: list[int] | None = None):
    return latest_interpretationlog_field(
        workflow.AnalysisInterpretation, "analysis_id", "priority", model_ids=analysis_ids
    )


def workflow_analyses_review_comment(analysis_ids: list[int] | None = None):
    return latest_interpretationlog_field(
        workflow.AnalysisInterpretation,
        "analysis_id",
        "review_comment",
        model_ids=analysis_ids,
    )


def workflow_analyses_warning_cleared(analysis_ids: list[int] | None = None):
    return latest_interpretationlog_field(
        workflow.AnalysisInterpretation,
        "analysis_id",
        "warning_cleared",
        model_ids=analysis_ids,
    )


def workflow_allele_priority(allele_ids: list[int] | None = None):
    return latest_interpretationlog_field(
        workflow.AlleleInterpretation, "allele_id", "priority", model_ids=allele_ids
    )


def workflow_allele_review_comment(allele_ids: list[int] | None = None):
    return latest_interpretationlog_field(
        workflow.AlleleInterpretation, "allele_id", "review_comment", model_ids=allele_ids
    )


def distinct_inheritance_hgnc_ids_for_genepanel(
    inheritance: str, genepanel_name: str, genepanel_version: str
):
    """
    Fetches all hgnc_ids with _only_ {inheritance} transcripts.

    e.g. only 'AD' or only 'AR'
    """
    # Get genes with transcripts having only provided inheritance
    # e.g. only 'AD' or only 'AR' etc...
    hgnc_ids = (
        select(gene.Transcript.gene_id)
        .where(
            gene.GenepanelTranscript.transcript_id == gene.Transcript.id,
            gene.GenepanelTranscript.genepanel_name == genepanel_name,
            gene.GenepanelTranscript.genepanel_version == genepanel_version,
        )
        .group_by(
            gene.Transcript.gene_id,
        )
        .having(func.every(gene.GenepanelTranscript.inheritance == inheritance))
        .distinct()
    )

    return hgnc_ids


def inheritance_for_genepanel(
    genepanel_name: str, genepanel_version: str, hgnc_ids: list[int] | None = None
):
    """
    Fetches inheritance for all genes in a gene panel or a subset of genes in a gene panel.

    Also includes the transcript name for each inheritance.
    """
    inheritances = (
        select(
            gene.GenepanelTranscript.inheritance,
            gene.Transcript.gene_id.label("hgnc_id"),
            gene.Transcript.transcript_name,
        )
        .join(
            gene.Transcript,
            gene.Transcript.id == gene.GenepanelTranscript.transcript_id,
        )
        .where(
            gene.GenepanelTranscript.genepanel_name == genepanel_name,
            gene.GenepanelTranscript.genepanel_version == genepanel_version,
        )
    )

    if hgnc_ids is not None:
        inheritances = inheritances.where(
            filters.in_(
                gene.Transcript.gene_id,
                hgnc_ids,
            )
        )
    return inheritances


def allele_genepanels(
    genepanel_keys: Sequence[tuple[str, str]],
    allele_ids: Sequence[int] | None = None,
):
    result = (
        select(
            allele.Allele.id.label("allele_id"),
            gene.Genepanel.name.label("name"),
            gene.Genepanel.version.label("version"),
            gene.Genepanel.official.label("official"),
        )
        .join(gene.Genepanel.transcripts)
        .where(filters.in_((gene.Genepanel.name, gene.Genepanel.version), genepanel_keys))
        .where(
            allele.Allele.chromosome == gene.Transcript.chromosome,
            or_(
                and_(
                    allele.Allele.start_position >= gene.Transcript.tx_start,
                    allele.Allele.start_position <= gene.Transcript.tx_end,
                ),
                and_(
                    allele.Allele.open_end_position > gene.Transcript.tx_start,
                    allele.Allele.open_end_position < gene.Transcript.tx_end,
                ),
            ),
        )
    )

    if allele_ids is not None:
        result = result.where(filters.in_(allele.Allele.id, allele_ids))

    return result


def annotation_transcripts_genepanel(
    session: Session,
    genepanel_keys: Sequence[tuple[str, str]],
    allele_ids: Sequence[int] | None = None,
    annotation_ids: Sequence[int] | None = None,
) -> Select:
    """
    Returns a joined representation of annotation transcripts against genepanel transcripts
    for given genepanel_keys.

    genepanel_keys = [('HBOC', 'v01'), ('LYNCH', 'v01'), ...]

    Returns Query object, representing:
    -----------------------------------------------------------------------------
    | allele_id | name | version | annotation_transcript | genepanel_transcript |
    -----------------------------------------------------------------------------
    | 1         | HBOC | v01     | NM_000059.2           | NM_000059.3          |
    | 1         | HBOC | v01     | ENST00000530893       | ENST00000530893      |
      etc...

    :warning: If there is no match between the genepanel and the annotation,
    the allele won't be included in the result.
    Therefore, do _not_ use this as basis for inclusion of alleles in an analysis.
    Use it only to get annotation data for further filtering,
    where a non-match wouldn't exclude the allele in the analysis.
    """
    assert not (allele_ids and annotation_ids)

    # If annotation ids is specified, we should not use the default AnnotationShadowTranscript, as this contains only
    # the most recent annotations. Instead, we create a temp-table matching the structure of AnnotationShadowTranscript,
    # with the annotations requested
    if annotation_ids:
        tx = func.jsonb_array_elements(annotation.Annotation.annotations.op("->")("transcripts"))
        annotation_shadow_transcript_table = (
            create_temp_table(
                session,
                select(
                    annotation.Annotation.allele_id,
                    tx.op("->>")("hgnc_id").cast(Integer).label("hgnc_id"),
                    tx.op("->>")("symbol").cast(Text).label("symbol"),
                    tx.op("->>")("transcript").cast(Text).label("transcript"),
                    tx.op("->>")("HGVSc").cast(Text).label("hgvsc"),
                    tx.op("->>")("protein").cast(Text).label("protein"),
                    tx.op("->>")("HGVSp").cast(Text).label("hgvsp"),
                ).where(filters.in_(annotation.Annotation.id, annotation_ids)),
                "annotationshadowtranscript",
            )
        ).columns
    elif allele_ids is not None:
        # Create a subset of the annotationshadowtranscript table
        # Using the full table messes with query planning
        annotation_shadow_transcript_table = create_temp_table(
            session,
            select(annotationshadow.AnnotationShadowTranscript).where(
                # Yes, use sqlalchemy's in_ function, not filters.in_
                annotationshadow.AnnotationShadowTranscript.allele_id.in_(allele_ids)
            ),
            "annotationshadowtranscript_subset",
            index_columns=["hgnc_id", "transcript"],
        ).columns

    else:
        annotation_shadow_transcript_table = annotationshadow.AnnotationShadowTranscript

    genepanel_transcripts = (
        select(
            gene.Genepanel.name,
            gene.Genepanel.version,
            gene.Transcript.transcript_name,
            gene.Transcript.gene_id,
        )
        .join(gene.Genepanel.transcripts)
        .where(filters.in_((gene.Genepanel.name, gene.Genepanel.version), genepanel_keys))
        .subquery()
    )

    # Column for version difference between annotation transcript and genepanel transcript
    tx_version: ColumnClause[str] = literal_column(
        "substring(split_part(transcript, '.', 2) FROM '^[0-9]+')::int"  # Fetch annotation transcript version: NM_00111.2 -> 2
        + "- 0.5*(split_part(transcript, '.', 2) !~ '^[0-9]+$')::int"  # Reduce version by 0.5 if version is not an integer, e.g. NM_00111.2_dupl18
    )

    # Join genepanel and annotation tables together, using transcript as key
    # and splitting out the version number of the transcript (if it has one)
    result = select(
        annotation_shadow_transcript_table.allele_id.label("allele_id"),
        genepanel_transcripts.c.name.label("name"),
        genepanel_transcripts.c.version.label("version"),
        genepanel_transcripts.c.gene_id.label("genepanel_hgnc_id"),
        genepanel_transcripts.c.transcript_name.label("genepanel_transcript"),
        annotation_shadow_transcript_table.transcript.label("annotation_transcript"),
        annotation_shadow_transcript_table.symbol.label("annotation_symbol"),
        annotation_shadow_transcript_table.hgnc_id.label("annotation_hgnc_id"),
        annotation_shadow_transcript_table.hgvsc.label("annotation_hgvsc"),
        annotation_shadow_transcript_table.hgvsp.label("annotation_hgvsp"),
    ).join(
        genepanel_transcripts,
        and_(
            text("transcript_name like split_part(transcript, '.', 1) || '%'"),
            genepanel_transcripts.c.gene_id == annotation_shadow_transcript_table.hgnc_id,
        ),
    )

    # Order and distinct:
    # For each distinct allele, genepanel, gene and genepanel transcript, select either:
    # - The annotation transcript that matches the genepanel transcript on version
    # - Otherwise, select the latest available transcript version
    result = result.order_by(
        annotation_shadow_transcript_table.allele_id,
        genepanel_transcripts.c.name,
        genepanel_transcripts.c.version,
        genepanel_transcripts.c.gene_id,
        genepanel_transcripts.c.transcript_name,
        (
            genepanel_transcripts.c.transcript_name == annotation_shadow_transcript_table.transcript
        ).desc(),
        tx_version.desc().nullslast(),
    )

    result = result.distinct(
        annotation_shadow_transcript_table.allele_id,
        genepanel_transcripts.c.name,
        genepanel_transcripts.c.version,
        genepanel_transcripts.c.gene_id,
        genepanel_transcripts.c.transcript_name,
    )

    return result


def get_valid_filter_configs(
    session: Session, usergroup_id: int, analysis_id: int | None = None
) -> Select[tuple[sample.FilterConfig]]:
    usergroup_filterconfigs = get_filter_configs_by_usergroup(usergroup_id)
    if analysis_id is None:
        if len(session.execute(usergroup_filterconfigs.limit(2)).all()) > 1:
            return usergroup_filterconfigs.where(sample.FilterConfig.requirements == [])
        else:
            return usergroup_filterconfigs

    valid_ids = []
    for fc in session.execute(usergroup_filterconfigs).scalars().all():
        reqs_fulfilled = []
        for req in fc.requirements:
            req_fulfilled = getattr(filterconfig_requirements, req["function"])(
                session, analysis_id, req["params"]
            )
            reqs_fulfilled.append(req_fulfilled)
        if all(reqs_fulfilled):
            valid_ids.append(fc.id)

    if not valid_ids:
        raise RuntimeError(
            f"Unable to find any valid filter configs for analysis id {analysis_id} and usergroup id {usergroup_id}"
        )

    return usergroup_filterconfigs.where(filters.in_(sample.FilterConfig.id, valid_ids))


def get_filter_configs_by_usergroup(usergroup_id: int) -> Select[tuple[sample.FilterConfig]]:
    return (
        select(sample.FilterConfig)
        .join(sample.UserGroupFilterConfig)
        .where(
            sample.UserGroupFilterConfig.usergroup_id == usergroup_id,
            sample.FilterConfig.active.is_(True),
        )
        .order_by(sample.UserGroupFilterConfig.order)
    )
