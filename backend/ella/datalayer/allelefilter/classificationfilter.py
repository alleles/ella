from typing import override

from sqlalchemy import select

from ella.datalayer import filters, queries
from ella.datalayer.allelefilter.filterbase import FilterBase
from ella.vardb.datamodel import assessment
from ella.vardb.datamodel.pydantic.filterconfig import ClassificationModel


class ClassificationFilter(FilterBase):
    CONTEXT_DEPENDENT = False
    FILTER_WITH = "allele"

    @override
    def filter_with_alleles(
        self, allele_ids: list[int], filter_config: ClassificationModel.ClassificationConfig
    ) -> set[int]:
        """
        Return the allele ids, among the provided allele_ids,
        that have an existing classification in the provided filter_config.classes,
        and are not outdated per the application config, if flag `exclude_outdated` is set to True.

        `exclude_outdated` defaults to False, to keep backwards compatibility. This is suitable when used as an exception.
        Setting this to True is more suitable when used as a forward filter, to e.g. filter out class 2 variants only if
        they have a valid date.
        """
        filter_classes = filter_config.classes
        available_classes = list(
            assessment.AlleleAssessment.classification.property.columns[0].type.enums
        )

        assert not set(filter_classes) - set(available_classes), (
            f"Invalid class(es) to filter on in {filter_classes}."
            f" Available classes are {available_classes}."
        )

        # Only apply filter to assessments within date set in config if exclude_outdated is True.
        # Note: date_superceeded is _not_ related "outdatedness", this is to just get the latest
        # assessments for the allele (regardless of date)
        if filter_config.exclude_outdated:
            valid_assessments = queries.valid_alleleassessments_filter()
        else:
            valid_assessments = [assessment.AlleleAssessment.date_superceeded.is_(None)]

        filtered_allele_ids = (
            self.session.execute(
                select(assessment.AlleleAssessment.allele_id).where(
                    filters.in_(assessment.AlleleAssessment.allele_id, allele_ids),
                    filters.in_(assessment.AlleleAssessment.classification, filter_classes),
                    *valid_assessments,
                )
            )
            .scalars()
            .all()
        )

        return set(filtered_allele_ids)
