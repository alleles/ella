from typing import override

from sqlalchemy import func, select

from ella.datalayer.allelefilter.filterbase import FilterBase
from ella.datalayer.queries import annotation_transcripts_genepanel
from ella.vardb.datamodel.pydantic.filterconfig import GeneModel


class GeneFilter(FilterBase):
    CONTEXT_DEPENDENT = False
    FILTER_WITH = "genepanel"

    MODES_OPERATOR = {
        "one": "&&",  # Postgres overlap operator
        "all": "<@",  # Postgres 'is contained by' operator
    }

    @override
    def filter_with_genepanel(
        self,
        genepanel_key: tuple[str, str],
        allele_ids: list[int],
        filter_config: GeneModel.GeneConfig,
    ) -> set[int]:
        """
        Return the allele ids, among the provided allele_ids,
        that matches the genes given in filter_config. The filter uses annotation on the genepanel only,
        meaning

        1. If the genes are not in the gene panel, they will have no effect
        2. The annotation transcripts are matched on gene panel transcripts (excl version), meaning that for an allele to be filtered,
           it must be annotated with a transcript present in the gene panel.

        The filter can be configured with the following settings:

        - genes (required):
            List of hgnc ids
        - mode (optional, 'one' or 'all', default 'all'):
            If 'all': variant must be annotated with genes from the 'genes' list only. Useful for filtering out.
            If 'one', variant annotation must include at least one gene from the gene list (but could be annotated with other genes). Useful for exceptions.
        - inverse (optional, default False):
            If True, run the filter in inverse.
        """
        mode = filter_config.mode or "all"
        assert mode in GeneFilter.MODES_OPERATOR, (
            f"Mode {mode} for GeneFilter not supported."
            f" Supported values are {GeneFilter.MODES_OPERATOR.keys()}."
        )

        operator = GeneFilter.MODES_OPERATOR[mode]

        # We rely on the annotation of the variants, and only consider annotation from the genepanel transcripts
        allele_ids_annotation_genepanel = annotation_transcripts_genepanel(
            self.session, [genepanel_key], allele_ids
        ).subquery()

        # Group HGNC ids per allele, excluding alleles without any hgnc ids to avoid empty arrays
        # -----------------------------
        # | allele_id | hgnc_ids       |
        # -----------------------------
        # | 1         | [329]          |
        # | 2         | [3084]         |
        # | 3         | [25567]        |
        # | 4         | [25186, 25567] |
        # | 5         | [25186, 25567] |
        # | 6         | [19104]        |
        # | 7         | [13281]        |
        # | 8         | [18806]        |
        # | 9         | [18806]        |
        # | 10        | [18806]        |
        allele_id_genes = (
            select(
                allele_ids_annotation_genepanel.c.allele_id,
                func.array_agg(allele_ids_annotation_genepanel.c.genepanel_hgnc_id).label(
                    "hgnc_ids"
                ),
            )
            .where(
                ~allele_ids_annotation_genepanel.c.genepanel_hgnc_id.is_(None)
            )  # IMPORTANT! Avoid empty arrays, as empty arrays always overlap or is contained within another
            .group_by(allele_ids_annotation_genepanel.c.allele_id)
            .subquery()
        )

        filtered_allele_ids = select(allele_id_genes.c.allele_id).where(
            allele_id_genes.c.hgnc_ids.op(operator)(filter_config.genes)
        )
        result = self.session.execute(filtered_allele_ids).scalars().all()
        if filter_config.inverse:
            return set(allele_ids) - set(result)
        else:
            return set(result)
