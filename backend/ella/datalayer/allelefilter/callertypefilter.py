from typing import override

from sqlalchemy import select

from ella.datalayer import filters
from ella.datalayer.allelefilter.filterbase import FilterBase
from ella.vardb.datamodel import allele
from ella.vardb.datamodel.pydantic.filterconfig import CallertypeModel


class CallerTypeFilter(FilterBase):
    CONTEXT_DEPENDENT = False
    FILTER_WITH = "allele"

    @override
    def filter_with_alleles(
        self, allele_ids: list[int], filter_config: CallertypeModel.CallertypeConfig
    ) -> set[int]:
        """
        Return the allele ids, among the provided allele_ids,
        that have an existing callerType in the provided filter_config.caller_types.
        """

        available_callertypes = list(allele.Allele.caller_type.property.columns[0].type.enums)
        filter_callertypes = filter_config.caller_types

        assert filter_callertypes, "callerTypes was not found in filterconfig"

        assert not set(filter_callertypes) - set(available_callertypes), (
            f"Invalid callertype(s) to filter on in {filter_callertypes}."
            f" Available callertypes are {available_callertypes}."
        )

        filtered_allele_ids = (
            self.session.execute(
                select(allele.Allele.id).where(
                    filters.in_(allele.Allele.id, allele_ids),
                    filters.in_(allele.Allele.caller_type, filter_callertypes),
                )
            )
            .scalars()
            .all()
        )
        return set(filtered_allele_ids)
