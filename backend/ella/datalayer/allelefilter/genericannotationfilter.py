import operator
from collections.abc import Callable
from typing import TYPE_CHECKING, Any, Literal, override

from sqlalchemy import (
    Boolean,
    ColumnElement,
    ColumnOperators,
    Numeric,
    String,
    Subquery,
    and_,
    case,
    cast,
    func,
    literal_column,
    null,
    or_,
    select,
)
from sqlalchemy.dialects.postgresql import array_agg

from ella.datalayer.allelefilter.filterbase import FilterBase
from ella.vardb.datamodel import annotation
from ella.vardb.datamodel.pydantic.filterconfig import GenericAnnotationModel

if TYPE_CHECKING:
    from sqlalchemy.sql.elements import BinaryExpression


class AdditionalColumnOperators(ColumnOperators):
    def not_contains(self, other: str):
        return ~self.contains(other)

    def array_contains(self, other: list[Any]):
        return self.op("@>")(other)

    def not_array_contains(self, other: list[Any]):
        return ~self.op("@>")(other)

    def overlap(self, other: list[Any]):
        return self.op("&&")(other)

    def not_overlap(self, other: list[Any]):
        return ~self.overlap(other)


OPERATORS: dict[str, Callable] = {
    "is": ColumnOperators.is_,
    "is_not": ColumnOperators.is_not,
    "==": operator.eq,
    "!=": operator.ne,
    ">": operator.gt,
    ">=": operator.ge,
    "<": operator.lt,
    "<=": operator.le,
    "in": ColumnOperators.in_,
    "not_in": ColumnOperators.not_in,
    "contains": AdditionalColumnOperators.array_contains,
    "not_contains": AdditionalColumnOperators.not_array_contains,
    "str_contains": ColumnOperators.contains,
    "str_not_contains": AdditionalColumnOperators.not_contains,
    "overlap": AdditionalColumnOperators.overlap,
    "not_overlap": AdditionalColumnOperators.not_overlap,
}

CAST_TYPES: dict[Literal["boolean", "string", "number"], type[String | Boolean | Numeric]] = {
    "boolean": Boolean,
    "string": String,
    "number": Numeric,
}


class GenericAnnotationFilter(FilterBase):
    FILTER_WITH = "allele"
    CONTEXT_DEPENDENT = False

    def _build_subquery(self, filter_config: GenericAnnotationModel.GenericAnnotationConfig):
        """Build a subquery to extract JSONB elements from annotations.

        The filter config

        {
            "target": "prediction.spliceai",
            "is_array": True,
            "rules": [
                {"key": "DS_AG", "operator": "<", "value": 0.05, "type_str": "number"},
                {"key": "DS_AL", "operator": "<", "value": 0.05, "type_str": "number"},
            ],
            "mode": "all",
        }

        will generate the following subquery:

        ┌-------------------------------------------------------------------┐
        | allele_id | prediction.spliceai.DS_AG | prediction.spliceai.DS_AL |
        |-------------------------------------------------------------------|
        | 713       | 0.04                      | 0.01                      |
        | 745       | 0.1                       | 0.0                       |
        | 707       | 0.0                       | 0.0                       |
        | 708       | 0.0                       | 0.0                       |
        | 709       | 0.0                       | 0.02                      |
        | 710       | 0.0                       | 0.0                       |
        ...
        """
        # Construct path to target JSONB element
        path = "annotations->" + "->".join([f"'{x}'" for x in filter_config.target.split(".")])

        # If array, expand to set of JSONB elements
        jsonb_label = "jsonb_label"
        if filter_config.is_array:
            jsonb_elements = func.jsonb_array_elements(literal_column(path)).label(jsonb_label)
        else:
            jsonb_elements = literal_column(path).label(jsonb_label)

        jsonb_subquery = (
            select(annotation.Annotation.allele_id, jsonb_elements)
            .where(annotation.Annotation.date_superceeded.is_(None))
            .subquery()
        )

        subquery_columns = []
        for i, rule in enumerate(filter_config.rules):
            cast_type = CAST_TYPES[rule.type_str]

            # Add index to target label to avoid column name conflicts whenever multiple rules
            # to the same target are applied
            target_label = filter_config.target + "." + rule.key + f"_{i}"

            # If JSONB target is an array; unpack, cast elements, and aggregate to SQL array
            if rule.operator in ["contains", "not_contains", "overlap", "not_overlap"]:
                jsonb_column = getattr(jsonb_subquery.c, jsonb_label).op("->")(rule.key)

                # Define a lateral join for array elements and cast each element
                casted_label = "element_casted"
                lateral_join = select(
                    cast(
                        func.jsonb_array_elements_text(jsonb_column).column_valued(),
                        cast_type,
                    ).label(casted_label)
                ).lateral()

                # Aggregate the casted elements
                aggregated_elements = select(
                    array_agg(getattr(lateral_join.c, casted_label))
                ).scalar_subquery()

                # Add aggregated elements (or NULL whenever target is null)
                subquery_columns.append(
                    case(
                        (
                            func.jsonb_typeof(jsonb_column) == "array",
                            aggregated_elements,
                        ),
                        else_=null(),
                    ).label(target_label)
                )
            else:
                jsonb_column = getattr(jsonb_subquery.c, jsonb_label).op("->>")(rule.key)
                subquery_columns.append(cast(jsonb_column, cast_type).label(target_label))

        return (
            select(jsonb_subquery.c.allele_id, *subquery_columns)
            .select_from(jsonb_subquery)
            .subquery()
        )

    def _build_filters(
        self, filter_config: GenericAnnotationModel.GenericAnnotationConfig, subquery: Subquery
    ):
        subquery_filters: list[BinaryExpression] = []

        for i, rule in enumerate(filter_config.rules):
            op = OPERATORS[rule.operator]
            target_label = filter_config.target + "." + rule.key + f"_{i}"
            subquery_filters.append(op(getattr(subquery.c, target_label), rule.value))
        return subquery_filters

    @override
    def filter_with_alleles(
        self, allele_ids: list[int], filter_config: GenericAnnotationModel.GenericAnnotationConfig
    ) -> set[int]:
        """
        Filter alleles on generic annotation.

        See Pydantic model for filter config specifications.
        """
        bool_operator: Callable[..., ColumnElement[bool]]
        if filter_config.mode == "all":
            bool_operator = and_
        else:
            bool_operator = or_

        subquery = self._build_subquery(filter_config)
        subquery_filters = self._build_filters(filter_config, subquery)

        filtered_allele_ids = select(subquery.c.allele_id).where(
            subquery.c.allele_id.in_(  # use in_() over filters.in_() due to performance issue
                allele_ids
            ),
            bool_operator(
                *subquery_filters,
            ),
        )

        result: set[int] = set(self.session.execute(filtered_allele_ids).scalars().all())

        return result
