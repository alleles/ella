from typing import override

from sqlalchemy import and_, not_, or_, select

from ella.datalayer.allelefilter.filterbase import FilterBase
from ella.datalayer.genotypetable import get_genotype_temp_table
from ella.vardb.datamodel import sample
from ella.vardb.datamodel.pydantic.filterconfig import QualityModel


class QualityFilter(FilterBase):
    CONTEXT_DEPENDENT = False
    FILTER_WITH = "analysis"

    @override
    def filter_with_analysis(
        self, analysis_id: int, allele_ids: list[int], filter_config: QualityModel.QualityConfig
    ) -> set[int]:
        """
        Returns allele_ids that can be filtered _out_ from an analysis.
        """
        proband_sample_ids = (
            self.session.execute(
                select(sample.Sample.id).where(
                    sample.Sample.analysis_id == analysis_id, sample.Sample.proband.is_(True)
                )
            )
            .scalars()
            .all()
        )

        genotype_table = get_genotype_temp_table(
            self.session,
            allele_ids,
            proband_sample_ids,
            genotype_extras={"qual": "variant_quality", "filter_status": "filter_status"},
            genotypesampledata_extras={"ar": "allele_ratio"},
        )

        # We consider an allele to be filtered out if it is to be filtered out in ALL proband samples it occurs in.
        # To achieve this, we work backwards, by finding all alleles that should not be filtered in each sample, and
        # subtracting it from the set of all allele ids.
        considered_allele_ids: set[int] = set()
        filtered_allele_ids = set(allele_ids)

        for sample_id in proband_sample_ids:
            filter_conditions = []
            if filter_config.quality is not None:
                filter_conditions.extend(
                    [
                        ~getattr(genotype_table.c, f"{sample_id}_qual").is_(None),
                        getattr(genotype_table.c, f"{sample_id}_qual") < filter_config.quality,
                    ]
                )

            if filter_config.allele_ratio is not None:
                filter_conditions.extend(
                    [
                        ~getattr(genotype_table.c, f"{sample_id}_ar").is_(None),
                        getattr(genotype_table.c, f"{sample_id}_ar") < filter_config.allele_ratio,
                        getattr(genotype_table.c, f"{sample_id}_ar")
                        != 0.0,  # Allele ratios can sometimes be misleading 0.0. Avoid filtering these out.,
                    ]
                )

            if filter_config.filter_status:
                # Filter according to regex pattern
                filter_status_pattern = getattr(genotype_table.c, f"{sample_id}_filter_status").op(
                    "~"
                )(filter_config.filter_status.pattern)

                # If filter_empty, always filter out None or '.'
                if filter_config.filter_status.filter_empty:
                    filter_status_condition = or_(
                        filter_status_pattern,
                        getattr(genotype_table.c, f"{sample_id}_filter_status").is_(None),
                        getattr(genotype_table.c, f"{sample_id}_filter_status") == ".",
                    )
                # else, never filter out None or '.' (even if regex says '\.')
                else:
                    filter_status_condition = and_(
                        filter_status_pattern,
                        ~getattr(genotype_table.c, f"{sample_id}_filter_status").is_(None),
                        getattr(genotype_table.c, f"{sample_id}_filter_status") != ".",
                    )

                # If inverse, run whole expression in reverse
                if filter_config.filter_status.inverse:
                    filter_status_condition = not_(filter_status_condition)

                filter_conditions.append(filter_status_condition)

            alleles_in_sample = select(genotype_table.c.allele_id).where(
                ~getattr(genotype_table.c, f"{sample_id}_genotypeid").is_(None)
            )
            considered_allele_ids |= set(self.session.execute(alleles_in_sample).scalars().all())

            not_filtered_allele_ids = alleles_in_sample.where(~and_(*filter_conditions))
            filtered_allele_ids -= set(
                self.session.execute(not_filtered_allele_ids).scalars().all()
            )

        assert considered_allele_ids == set(allele_ids)
        return filtered_allele_ids
