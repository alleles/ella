from typing import override

from sqlalchemy import and_, case, func, or_, select, tuple_

from ella.datalayer import filters
from ella.datalayer.allelefilter.filterbase import FilterBase
from ella.vardb.datamodel import allele, gene
from ella.vardb.datamodel.pydantic.filterconfig import PolyPyrimidineModel


class PolypyrimidineTractFilter(FilterBase):
    CONTEXT_DEPENDENT = False
    FILTER_WITH = "genepanel"

    @override
    def filter_with_genepanel(
        self,
        gp_key: tuple[str, str],
        allele_ids: list[int],
        filter_config: PolyPyrimidineModel.PolyPyrimidineConfig,
    ) -> set[int]:
        """
        Filter alleles in the polypyrimidine tract (defined by filter_config['ppy_tract_region'],
        strandedness taken into account)

        Only filter out alleles that are either SNPs from C->T, T->C, or deletions of
        C, T, CC, TT, CT, TC (strandedness taken into account)

        We do *not* filter out alleles where a deletion is flanked by A (positive strand)
        or C (negative strand), as this could create a new AG splice site.
        Ideally, we would check if the deletion was flanked by A on one side and G on another,
        but since we do not use a FASTA-file, we are unable to check both sides. The one side we
        do check comes from the vcf_ref-column in the allele-table
        """
        ppy_tract_region = filter_config.ppy_tract_region
        ppy_padding = max(abs(v) for v in ppy_tract_region)

        # Extract transcripts associated with the genepanel
        # To potentially limit the number of regions we need to check,
        # exclude transcripts where we have no alleles overlapping in the
        # region [tx_start-ppy_padding, tx_end+ppy_padding]. The filter clause in the query # type: ignore
        # should have no effect on the result, but is included only for performance

        genepanel_transcripts = (
            select(gene.Transcript)
            .join(
                gene.GenepanelTranscript,
                and_(
                    gene.GenepanelTranscript.transcript_id == gene.Transcript.id,
                    tuple_(
                        gene.GenepanelTranscript.genepanel_name,
                        gene.GenepanelTranscript.genepanel_version,
                    )
                    == gp_key,
                ),
            )
            .where(
                filters.in_(allele.Allele.id, allele_ids),
                allele.Allele.chromosome == gene.Transcript.chromosome,
                or_(
                    and_(
                        allele.Allele.start_position >= gene.Transcript.tx_start - ppy_padding,
                        allele.Allele.start_position <= gene.Transcript.tx_end + ppy_padding,
                    ),
                    and_(
                        allele.Allele.open_end_position > gene.Transcript.tx_start - ppy_padding,
                        allele.Allele.open_end_position < gene.Transcript.tx_end + ppy_padding,
                    ),
                ),
            )
            .subquery()
        )

        # Unwrap the exons for the genepanel transcript
        genepanel_transcript_exons = select(
            genepanel_transcripts.c.id,
            genepanel_transcripts.c.gene_id,
            genepanel_transcripts.c.chromosome,
            genepanel_transcripts.c.strand,
            genepanel_transcripts.c.cds_start,
            genepanel_transcripts.c.cds_end,
            func.unnest(genepanel_transcripts.c.exon_starts).label("exon_start"),
            func.unnest(genepanel_transcripts.c.exon_ends).label("exon_end"),
        ).subquery()

        # Regions with applied padding
        def _create_region(transcripts, region_start, region_end):
            return select(
                transcripts.c.chromosome.label("chromosome"),
                transcripts.c.strand.label("strand"),
                region_start.label("region_start"),
                region_end.label("region_end"),
            ).distinct()

        # Note: ppy_tract_region[0]/ppy_tract_region[1] is a *negative* number
        # Upstream region for positive strand transcript
        # is [exon_start+ppy_tract[0], exon_start+ppy_tract[1]]
        # Upstream region for reverse strand transcript
        # is (exon_end-ppy_tract_region[0], exon_end-exon_upstream-ppy_tract_region[1]]
        ppytract_start = case(
            (
                genepanel_transcript_exons.c.strand == "-",
                genepanel_transcript_exons.c.exon_end - ppy_tract_region[1],
            ),
            else_=genepanel_transcript_exons.c.exon_start + ppy_tract_region[0],
        )

        ppytract_end = case(
            (
                genepanel_transcript_exons.c.strand == "-",
                genepanel_transcript_exons.c.exon_end - ppy_tract_region[0],
            ),
            else_=genepanel_transcript_exons.c.exon_start + ppy_tract_region[1],
        )

        ppytract = _create_region(
            genepanel_transcript_exons, ppytract_start, ppytract_end
        ).subquery()

        # Find allele ids within ppy tract region
        ppy_allele_ids = select(allele.Allele.id).where(
            filters.in_(allele.Allele.id, allele_ids),
            allele.Allele.chromosome == ppytract.c.chromosome,
            # Check that the allele is within the polypyrimidine tract region
            or_(
                and_(
                    allele.Allele.start_position >= ppytract.c.region_start,
                    allele.Allele.start_position <= ppytract.c.region_end,
                ),
                and_(
                    allele.Allele.open_end_position > ppytract.c.region_start,
                    allele.Allele.open_end_position < ppytract.c.region_end,
                ),
                and_(
                    allele.Allele.start_position <= ppytract.c.region_start,
                    allele.Allele.open_end_position > ppytract.c.region_end,
                ),
            ),
            # Check that the variant is a C/T SNP or deletion of C/T of length <= 2
            or_(
                and_(
                    ppytract.c.strand == "+",
                    or_(
                        and_(
                            allele.Allele.change_type == "del",
                            filters.in_(
                                allele.Allele.change_from, ["C", "T", "CT", "CC", "TT", "TC"]
                            ),
                            # Special case to avoid possible new AG splice sites
                            allele.Allele.vcf_alt != "A",
                        ),
                        and_(
                            allele.Allele.change_type == "SNP",
                            filters.in_(allele.Allele.change_from, ["T", "C"]),
                            filters.in_(allele.Allele.change_to, ["T", "C"]),
                        ),
                    ),
                ),
                and_(
                    # On reverse strand, we substitute C/T with G/A
                    ppytract.c.strand == "-",
                    or_(
                        and_(
                            allele.Allele.change_type == "del",
                            filters.in_(
                                allele.Allele.change_from, ["G", "A", "GA", "GG", "AA", "AG"]
                            ),
                            # Special case to avoid possible new AG splice sites (reverse: CT)
                            allele.Allele.vcf_alt != "C",
                        ),
                        and_(
                            allele.Allele.change_type == "SNP",
                            filters.in_(allele.Allele.change_from, ["A", "G"]),
                            filters.in_(allele.Allele.change_to, ["A", "G"]),
                        ),
                    ),
                ),
            ),
        )

        return set(self.session.execute(ppy_allele_ids).scalars().all())
