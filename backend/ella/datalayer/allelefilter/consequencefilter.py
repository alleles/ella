from typing import TYPE_CHECKING, override

from sqlalchemy import or_, select, text, tuple_

from ella.datalayer import filters
from ella.datalayer.allelefilter.filterbase import FilterBase
from ella.vardb.datamodel import annotationshadow, gene
from ella.vardb.datamodel.pydantic.filterconfig import ConsequenceModel

if TYPE_CHECKING:
    from sqlalchemy.orm.attributes import InstrumentedAttribute


class ConsequenceFilter(FilterBase):
    CONTEXT_DEPENDENT = False
    FILTER_WITH = "genepanel"

    @override
    def filter_with_genepanel(
        self,
        gp_key: tuple[str, str],
        allele_ids: list[int],
        filter_config: ConsequenceModel.ConsequenceConfig,
    ) -> set[int]:
        """
        Filter alleles that have consequence in list of consequences for any of the transcripts matching
        the global include regex (if specified). Can be specified to look at genepanel genes only.
        """
        assert self.config

        consequences = [c.value for c in filter_config.consequences]
        available_consequences = self.config["transcripts"]["consequences"]
        assert not set(consequences) - set(
            available_consequences
        ), f"Invalid consequences passed to filter: {consequences}"

        ast_allele_id: InstrumentedAttribute[
            int
        ] = annotationshadow.AnnotationShadowTranscript.allele_id
        allele_ids_with_consequence = (
            select(ast_allele_id)
            .where(
                filters.in_(annotationshadow.AnnotationShadowTranscript.allele_id, allele_ids),
                annotationshadow.AnnotationShadowTranscript.consequences.op("&&")(consequences),
            )
            .distinct()
        )

        inclusion_regex = self.config.get("transcripts", {}).get("inclusion_regex")
        if inclusion_regex:
            allele_ids_with_consequence = allele_ids_with_consequence.where(
                text("transcript ~ :reg").params(reg=inclusion_regex)
            )

        # Include genes in genepanel only
        if filter_config.genepanel_only:
            gp_genes = self.session.execute(
                select(gene.Transcript.gene_id, gene.Gene.hgnc_symbol)
                .join(gene.Genepanel.transcripts)
                .join(gene.Gene)
                .where(tuple_(gene.Genepanel.name, gene.Genepanel.version) == gp_key)
            )

            gp_gene_ids, gp_gene_symbols = list(zip(*[(g[0], g[1]) for g in gp_genes]))

            allele_ids_with_consequence = allele_ids_with_consequence.where(
                or_(
                    filters.in_(
                        annotationshadow.AnnotationShadowTranscript.hgnc_id,
                        gp_gene_ids,
                    ),
                    filters.in_(
                        annotationshadow.AnnotationShadowTranscript.symbol,
                        gp_gene_symbols,
                    ),
                )
            )
        return set(self.session.execute(allele_ids_with_consequence).scalars().all())
