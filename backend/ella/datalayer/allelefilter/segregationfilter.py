import logging
from collections.abc import Sequence
from typing import override

from sqlalchemy import ColumnElement, Select, and_, func, or_, select, text
from sqlalchemy.sql.schema import Table
from sqlalchemy.sql.selectable import Subquery

from ella.datalayer import filters
from ella.datalayer.allelefilter.filterbase import FilterBase
from ella.datalayer.denovo_probability import denovo_probability
from ella.datalayer.genotypetable import extend_genotype_table_with_allele, get_genotype_temp_table
from ella.vardb.datamodel import allele, annotationshadow, genotype, sample
from ella.vardb.datamodel.pydantic.filterconfig import SegregationModel

log = logging.getLogger(__name__)

# X chromosome PAR regions for GRCh37 (1-based)
PAR1_START = 60001
PAR1_END = 2_699_520
PAR2_START = 154_931_044
PAR2_END = 155_260_560


class SegregationFilter(FilterBase):
    CONTEXT_DEPENDENT = True
    FILTER_WITH = "analysis"

    def denovo_p_value(
        self,
        allele_ids: Sequence[int],
        genotype_table: Table,
        proband_sample_id: int,
        father_sample_id: int,
        mother_sample_id: int,
    ):
        assert all(
            [
                f"{sample_id}_gl" in genotype_table.columns.keys()
                for sample_id in [proband_sample_id, father_sample_id, mother_sample_id]
            ]
        ), "get_genotype_temp_table needs to be created with genotypesampledata_extra={'gl': 'genotype_likelihood'} for this function"

        genotype_with_allele_table = extend_genotype_table_with_allele(genotype_table)
        genotype_with_allele_table = genotype_with_allele_table.subquery()
        x_minus_par_filter = self.get_x_minus_par_filter(genotype_with_allele_table)

        denovo_mode_map = {
            "Xmale": {"Reference": 0, "Homozygous": 1},
            "default": {"Reference": 0, "Heterozygous": 1, "Homozygous": 2},
        }

        def _compute_denovo_probabilities(genotype_with_allele: Select, x_minus_par=False):
            dp: dict[str, str | float] = dict()
            for row in self.session.execute(genotype_with_allele):
                if not all(
                    [
                        getattr(row, f"{proband_sample_id}_gl"),
                        getattr(row, f"{father_sample_id}_gl"),
                        getattr(row, f"{mother_sample_id}_gl"),
                    ]
                ):
                    dp[row.allele_id] = "-"
                    continue

                if x_minus_par and getattr(row, f"{proband_sample_id}_sex") == "Male":
                    denovo_mode = [
                        denovo_mode_map["Xmale"][getattr(row, f"{father_sample_id}_type")],
                        denovo_mode_map["Xmale"][getattr(row, f"{mother_sample_id}_type")],
                        denovo_mode_map["Xmale"][getattr(row, f"{proband_sample_id}_type")],
                    ]
                else:
                    denovo_mode = [
                        denovo_mode_map["default"][getattr(row, f"{father_sample_id}_type")],
                        denovo_mode_map["default"][getattr(row, f"{mother_sample_id}_type")],
                        denovo_mode_map["default"][getattr(row, f"{proband_sample_id}_type")],
                    ]

                # It should not come up as a denovo candidate if either mother or father has the same called genotype
                assert denovo_mode.count(denovo_mode[2]) == 1
                try:
                    p = denovo_probability(
                        getattr(row, f"{proband_sample_id}_gl"),
                        getattr(row, f"{father_sample_id}_gl"),
                        getattr(row, f"{mother_sample_id}_gl"),
                        x_minus_par,
                        getattr(row, f"{proband_sample_id}_sex") == "Male",
                        denovo_mode,
                    )
                except Exception as e:
                    log.error(
                        f"Unable to compute denovo probability for {row}. Setting to -1.0.: {e}"
                    )
                    dp[row.allele_id] = -1.0
                    continue

                dp[row.allele_id] = p
            return dp

        genotype_with_denovo_allele_table = select(*genotype_with_allele_table.c)

        # Compute denovo probabilities for autosomal and x-linked regions separately
        denovo_probabilities: dict[str, str | float] = dict()
        denovo_probabilities.update(
            _compute_denovo_probabilities(
                genotype_with_denovo_allele_table.where(~x_minus_par_filter), False
            )
        )
        denovo_probabilities.update(
            _compute_denovo_probabilities(
                genotype_with_denovo_allele_table.where(x_minus_par_filter), True
            )
        )

        return denovo_probabilities

    def get_x_minus_par_filter(self, genotype_with_allele_table: Subquery) -> ColumnElement[bool]:
        """
        PAR1 X:60001-2699520 (GRCh37)
        PAR2 X:154931044-155260560 (GRCh37)
        """

        # Only GRCh37 is supported for now
        assert not self.session.execute(
            select(genotype_with_allele_table.c.allele_id)
            .where(genotype_with_allele_table.c.genome_reference != "GRCh37")
            .limit(1)
        ).scalar()

        x_minus_par_filter = and_(
            genotype_with_allele_table.c.chromosome == "X",
            # Allele table has 0-indexed positions
            or_(
                # Before PAR1
                genotype_with_allele_table.c.open_end_position <= PAR1_START,
                # Between PAR1 and PAR2
                and_(
                    genotype_with_allele_table.c.start_position > PAR1_END,
                    genotype_with_allele_table.c.open_end_position <= PAR2_START,
                ),
                # After PAR2
                genotype_with_allele_table.c.start_position > PAR2_END,
            ),
        )

        return x_minus_par_filter

    def denovo(
        self,
        genotype_table: Table,
        proband_sample_id: int,
        father_sample_id: int,
        mother_sample_id: int,
        denovo_config: SegregationModel.SegregationConfig.Denovo,
    ) -> set[int]:
        """
        Denovo mutations

        Based on denovo classification from FILTUS (M. Vigeland et al):
        https://academic.oup.com/bioinformatics/article/32/10/1592/1743466

        Denovo patterns:
        Autosomal:
            - 0/0 + 0/0 = 0/1
            - 0/0 + 0/0 = 1/1
            - 0/0 + 0/1 = 1/1
            - 0/1 + 0/0 = 1/1
        X-linked, child is boy:
            - 0 + 0/0 = 1
        X-linked, child is girl:
            - 0 + 0/0 = 0/1
            - 0 + 0/0 = 1/1
            - 0 + 0/1 = 1/1

        A variant is treated as X-linked in this context only if it is
        located outside of the pseudoautosomal regions PAR1 and PAR2 on the X chromosome.

        Note followig special conditions, which are _not included_ in the results:
        - Missing genotype in father or mother (i.e. no coverage).
        - A male member is given as heterozygous for an X-linked variant.

        If configured, the denovo results are filtered on genotype quality to remove technical
        artifacts or likely non-pathogenic variants.

        """

        if not mother_sample_id or not father_sample_id:
            return set()

        genotype_with_allele_table = extend_genotype_table_with_allele(genotype_table).subquery(
            "genotype_with_allele_table"
        )
        x_minus_par_filter = self.get_x_minus_par_filter(genotype_with_allele_table)

        denovo_allele_ids = select(genotype_with_allele_table.c.allele_id).where(
            # Exclude no coverage
            getattr(genotype_with_allele_table.c, f"{father_sample_id}_type") != "No coverage",
            getattr(genotype_with_allele_table.c, f"{mother_sample_id}_type") != "No coverage",
            or_(
                # Autosomal
                and_(
                    ~x_minus_par_filter,
                    or_(
                        # 0/0 + 0/0 = 0/1
                        # 0/0 + 0/0 = 1/1
                        and_(
                            or_(
                                getattr(genotype_with_allele_table.c, f"{proband_sample_id}_type")
                                == "Homozygous",
                                getattr(genotype_with_allele_table.c, f"{proband_sample_id}_type")
                                == "Heterozygous",
                            ),
                            getattr(genotype_with_allele_table.c, f"{father_sample_id}_type")
                            == "Reference",
                            getattr(genotype_with_allele_table.c, f"{mother_sample_id}_type")
                            == "Reference",
                        ),
                        # 0/0 + 0/1 = 1/1
                        # 0/1 + 0/0 = 1/1
                        and_(
                            getattr(genotype_with_allele_table.c, f"{proband_sample_id}_type")
                            == "Homozygous",
                            or_(
                                and_(
                                    getattr(
                                        genotype_with_allele_table.c, f"{father_sample_id}_type"
                                    )
                                    == "Heterozygous",
                                    getattr(
                                        genotype_with_allele_table.c, f"{mother_sample_id}_type"
                                    )
                                    == "Reference",
                                ),
                                and_(
                                    getattr(
                                        genotype_with_allele_table.c, f"{father_sample_id}_type"
                                    )
                                    == "Reference",
                                    getattr(
                                        genotype_with_allele_table.c, f"{mother_sample_id}_type"
                                    )
                                    == "Heterozygous",
                                ),
                            ),
                        ),
                    ),
                ),
                # X-linked
                and_(
                    x_minus_par_filter,
                    or_(
                        # Male proband
                        and_(
                            # 0 + 0/0 = 1
                            getattr(genotype_with_allele_table.c, f"{proband_sample_id}_sex")
                            == "Male",
                            getattr(genotype_with_allele_table.c, f"{proband_sample_id}_type")
                            == "Homozygous",
                            getattr(genotype_with_allele_table.c, f"{father_sample_id}_type")
                            == "Reference",
                            getattr(genotype_with_allele_table.c, f"{mother_sample_id}_type")
                            == "Reference",
                        ),
                        # Female proband
                        and_(
                            getattr(genotype_with_allele_table.c, f"{proband_sample_id}_sex")
                            == "Female",
                            or_(
                                # 0 + 0/0 = 0/1
                                # 0 + 0/0 = 1/1
                                and_(
                                    or_(
                                        getattr(
                                            genotype_with_allele_table.c,
                                            f"{proband_sample_id}_type",
                                        )
                                        == "Homozygous",
                                        getattr(
                                            genotype_with_allele_table.c,
                                            f"{proband_sample_id}_type",
                                        )
                                        == "Heterozygous",
                                    ),
                                    getattr(
                                        genotype_with_allele_table.c, f"{father_sample_id}_type"
                                    )
                                    == "Reference",
                                    getattr(
                                        genotype_with_allele_table.c, f"{mother_sample_id}_type"
                                    )
                                    == "Reference",
                                ),
                                # 0 + 0/1 = 1/1
                                and_(
                                    getattr(
                                        genotype_with_allele_table.c, f"{proband_sample_id}_type"
                                    )
                                    == "Homozygous",
                                    getattr(
                                        genotype_with_allele_table.c, f"{father_sample_id}_type"
                                    )
                                    == "Reference",
                                    getattr(
                                        genotype_with_allele_table.c, f"{mother_sample_id}_type"
                                    )
                                    == "Heterozygous",
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        )

        gq_threshold = denovo_config.gq_threshold
        if gq_threshold:
            denovo_allele_ids = denovo_allele_ids.where(
                or_(
                    getattr(genotype_with_allele_table.c, f"{proband_sample_id}_gq").is_(None),
                    getattr(genotype_with_allele_table.c, f"{proband_sample_id}_gq")
                    >= gq_threshold.proband,
                ),
                or_(
                    getattr(genotype_with_allele_table.c, f"{mother_sample_id}_gq").is_(None),
                    getattr(genotype_with_allele_table.c, f"{mother_sample_id}_gq")
                    >= gq_threshold.mother,
                ),
                or_(
                    getattr(genotype_with_allele_table.c, f"{father_sample_id}_gq").is_(None),
                    getattr(genotype_with_allele_table.c, f"{father_sample_id}_gq")
                    >= gq_threshold.father,
                ),
            )

        denovo_result = set(self.session.execute(denovo_allele_ids).scalars().all())
        return denovo_result

    def parental_mosaicism(
        self,
        genotype_table: Table,
        proband_sample_id: int,
        father_sample_id: int,
        mother_sample_id: int,
    ) -> set[int]:
        """
        Parental mosaicism

        Checks whether there are variants that is inherited from a parent with possible mosasicm.

        - In autosomal regions:
            - Proband has variant
            - Father or mother has allele_ratio between given heterozygous thresholds
        - In X:
            - Proband has variant
            - Father or mother has allele_ratio between given (mother: heterozygous, father: homozygous) thresholds
        """

        assert all(
            [
                f"{sample_id}_ar" in genotype_table.columns.keys()
                for sample_id in [proband_sample_id, father_sample_id, mother_sample_id]
            ]
        ), "get_genotype_temp_table needs to be created with genotypesampledata_extra={'ar': 'allele_ratio'} for this function"

        MOSAICISM_HETEROZYGOUS_THRESHOLD = [0, 0.3]  # (start, end]
        MOSAICISM_HOMOZYGOUS_THRESHOLD = [0, 0.8]  # (start, end]

        NON_MOSAICISM_THRESHOLD = 0.3

        if not mother_sample_id or not father_sample_id:
            return set()

        genotype_with_allele_table = extend_genotype_table_with_allele(genotype_table).subquery(
            "genotype_with_allele_table"
        )
        x_minus_par_filter = self.get_x_minus_par_filter(genotype_with_allele_table)

        parental_mosaicism_allele_ids = select(genotype_with_allele_table.c.allele_id).where(
            # Exclude no coverage
            func.coalesce(
                getattr(genotype_with_allele_table.c, f"{father_sample_id}_type"), "No coverage"
            )
            != "No coverage",
            func.coalesce(
                getattr(genotype_with_allele_table.c, f"{mother_sample_id}_type"), "No coverage"
            )
            != "No coverage",
            or_(
                # Autosomal
                # Proband heterozygous, parent with mosaicism ratio
                and_(
                    ~x_minus_par_filter,
                    getattr(genotype_with_allele_table.c, f"{proband_sample_id}_type")
                    == "Heterozygous",
                    getattr(genotype_with_allele_table.c, f"{proband_sample_id}_ar")
                    > NON_MOSAICISM_THRESHOLD,
                    # We don't check the genotype for the parents,
                    # as in this case we care more about the ratio than the called result
                    or_(
                        # Father mosaicism and mother not normal allele ratio
                        and_(
                            getattr(genotype_with_allele_table.c, f"{father_sample_id}_ar")
                            > MOSAICISM_HETEROZYGOUS_THRESHOLD[0],
                            getattr(genotype_with_allele_table.c, f"{father_sample_id}_ar")
                            <= MOSAICISM_HETEROZYGOUS_THRESHOLD[1],
                            getattr(genotype_with_allele_table.c, f"{mother_sample_id}_ar")
                            <= MOSAICISM_HETEROZYGOUS_THRESHOLD[1],
                        ),
                        # Mother mosaicism and father not normal allele ratio
                        and_(
                            getattr(genotype_with_allele_table.c, f"{mother_sample_id}_ar")
                            > MOSAICISM_HETEROZYGOUS_THRESHOLD[0],
                            getattr(genotype_with_allele_table.c, f"{mother_sample_id}_ar")
                            <= MOSAICISM_HETEROZYGOUS_THRESHOLD[1],
                            getattr(genotype_with_allele_table.c, f"{father_sample_id}_ar")
                            <= MOSAICISM_HETEROZYGOUS_THRESHOLD[1],
                        ),
                    ),
                ),
                # X-linked
                # Treat father mosaicism different than mothers
                and_(
                    x_minus_par_filter,
                    or_(
                        getattr(genotype_with_allele_table.c, f"{proband_sample_id}_type")
                        == "Heterozygous",
                        getattr(genotype_with_allele_table.c, f"{proband_sample_id}_type")
                        == "Homozygous",
                    ),
                    getattr(genotype_with_allele_table.c, f"{proband_sample_id}_ar")
                    > NON_MOSAICISM_THRESHOLD,
                    or_(
                        # Father mosaicism and mother not normal allele ratio
                        and_(
                            getattr(genotype_with_allele_table.c, f"{father_sample_id}_ar")
                            > MOSAICISM_HOMOZYGOUS_THRESHOLD[0],
                            getattr(genotype_with_allele_table.c, f"{father_sample_id}_ar")
                            <= MOSAICISM_HOMOZYGOUS_THRESHOLD[1],
                            getattr(genotype_with_allele_table.c, f"{mother_sample_id}_ar")
                            <= MOSAICISM_HETEROZYGOUS_THRESHOLD[1],
                        ),
                        # Mother mosaicism and father not normal allele ratio
                        and_(
                            getattr(genotype_with_allele_table.c, f"{mother_sample_id}_ar")
                            > MOSAICISM_HETEROZYGOUS_THRESHOLD[0],
                            getattr(genotype_with_allele_table.c, f"{mother_sample_id}_ar")
                            <= MOSAICISM_HETEROZYGOUS_THRESHOLD[1],
                            getattr(genotype_with_allele_table.c, f"{father_sample_id}_ar")
                            <= MOSAICISM_HOMOZYGOUS_THRESHOLD[1],
                        ),
                    ),
                ),
            ),
        )

        return set(self.session.execute(parental_mosaicism_allele_ids).scalars().all())

    def autosomal_recessive_homozygous(
        self,
        genotype_table: Table,
        proband_sample_id: int,
        father_sample_id: int | None,
        mother_sample_id: int | None,
        affected_sibling_sample_ids: list[int] | None = None,
        unaffected_sibling_sample_ids: list[int] | None = None,
    ) -> set[int]:
        """
        Autosomal recessive transmission

        Autosomal recessive:
        A variant must be:
        - Homozygous in proband
        - Heterozygous in both parents
        - Not homozygous in unaffected siblings
        - Homozygous in affected siblings
        - In chromosome 1-22 or X pseudoautosomal region (PAR1, PAR2)
        """

        if not father_sample_id or not mother_sample_id:
            return set()

        genotype_with_allele_table = extend_genotype_table_with_allele(genotype_table).subquery(
            "genotype_with_allele_table"
        )
        x_minus_par_filter = self.get_x_minus_par_filter(genotype_with_allele_table)

        genotype_filters = [
            getattr(genotype_with_allele_table.c, f"{proband_sample_id}_type") == "Homozygous",
            getattr(genotype_with_allele_table.c, f"{father_sample_id}_type") == "Heterozygous",
            getattr(genotype_with_allele_table.c, f"{mother_sample_id}_type") == "Heterozygous",
            ~x_minus_par_filter,  # In chromosome 1-22 or in X PAR
        ]
        if unaffected_sibling_sample_ids:
            genotype_filters += [
                func.coalesce(getattr(genotype_with_allele_table.c, f"{s}_type"), "Reference")
                != "Homozygous"
                for s in unaffected_sibling_sample_ids
            ]

        if affected_sibling_sample_ids:
            genotype_filters += [
                getattr(genotype_with_allele_table.c, f"{s}_type") == "Homozygous"
                for s in affected_sibling_sample_ids
            ]

        autosomal_allele_ids = select(genotype_with_allele_table.c.allele_id).where(
            *genotype_filters
        )

        return set(self.session.execute(autosomal_allele_ids).scalars().all())

    def xlinked_recessive_homozygous(
        self,
        genotype_table: Table,
        proband_sample_id: int,
        father_sample_id: int | None,
        mother_sample_id: int | None,
        affected_sibling_sample_ids: list[int] | None = None,
        unaffected_sibling_sample_ids: list[int] | None = None,
    ) -> set[int]:
        """
        X-linked recessive

        A variant must be:
        - Homozygous in proband (for girls this requires a denovo, but still valid case)
        - Heterozygous in mother
        - Not present in father
        - Not homozygous in unaffected siblings
        - Homozygous in affected siblings
        - In chromosome X, but not pseudoautosomal region (PAR1, PAR2)
        """

        if not father_sample_id or not mother_sample_id:
            return set()

        genotype_with_allele_table = extend_genotype_table_with_allele(genotype_table).subquery(
            "genotype_with_allele_table"
        )
        x_minus_par_filter = self.get_x_minus_par_filter(genotype_with_allele_table)

        genotype_filters = [
            getattr(genotype_with_allele_table.c, f"{proband_sample_id}_type") == "Homozygous",
            getattr(genotype_with_allele_table.c, f"{father_sample_id}_type") == "Reference",
            getattr(genotype_with_allele_table.c, f"{mother_sample_id}_type") == "Heterozygous",
            x_minus_par_filter,  # In X chromosome (minus PAR)
        ]
        if unaffected_sibling_sample_ids:
            genotype_filters += [
                func.coalesce(getattr(genotype_with_allele_table.c, f"{s}_type"), "Reference")
                != "Homozygous"
                for s in unaffected_sibling_sample_ids
            ]

        if affected_sibling_sample_ids:
            genotype_filters += [
                getattr(genotype_with_allele_table.c, f"{s}_type") == "Homozygous"
                for s in affected_sibling_sample_ids
            ]

        xlinked_allele_ids = select(genotype_with_allele_table.c.allele_id).where(*genotype_filters)

        return set(self.session.execute(xlinked_allele_ids).scalars().all())

    def compound_heterozygous(
        self,
        genotype_table: Table,
        proband_sample_id: int,
        father_sample_id: int | None = None,
        mother_sample_id: int | None = None,
        affected_sibling_sample_ids: list[int] | None = None,
        unaffected_sibling_sample_ids: list[int] | None = None,
    ) -> set[int]:
        """
        Autosomal recessive transmission: Compound heterozygous

        Based on rule set from:
        Filtering for compound heterozygous sequence variant in non-consanguineous pedigrees.
        (Kamphans T et al. (2013), PLoS ONE: 8(8), e70151)

        1. A variant has to be in a heterozygous state in all affected individuals.

        2. A variant must not occur in a homozygous state in any of the unaffected
        individuals.

        3. A variant that is heterozygous in an affected child must be heterozygous
        in exactly one of the parents.

        4. A gene must have two or more heterozygous variants in each of the
        affected individuals.

        5. There must be at least one variant transmitted from the paternal side
        and one transmitted from the maternal side.

        For the second part of the third rule - "in exactly one of the parents" - note
        this excerpt from article:
        "Rule 3b is applicable only if we assume that no de novo mutations occurred.
        The number of de novo mutations is estimated to be below five per exome per generation [2], [3],
        thus, the likelihood that an individual is compound heterozygous and at least one
        of these mutations arose de novo is low.
        If more than one family member is affected, de novo mutations are even orders
        of magnitudes less likely as a recessive disease cause.
        On the other hand, excluding these variants from the further analysis helps to
        remove many sequencing artifacts. In linkage analysis for example it is common practice
        of data cleaning to exclude variants as Mendelian errors if they cannot be explained
        by Mendelian inheritance."

        :note: Alleles with 'No coverage' in either parent are not included.
        """

        assert proband_sample_id

        sample_ids = [proband_sample_id]
        affected_sample_ids = [proband_sample_id]
        unaffected_sample_ids = []
        if father_sample_id:
            sample_ids.append(father_sample_id)
            unaffected_sample_ids.append(father_sample_id)
        if mother_sample_id:
            sample_ids.append(mother_sample_id)
            unaffected_sample_ids.append(mother_sample_id)
        if affected_sibling_sample_ids:
            sample_ids += affected_sibling_sample_ids
            affected_sample_ids += affected_sibling_sample_ids
        if unaffected_sibling_sample_ids:
            sample_ids += unaffected_sibling_sample_ids
            unaffected_sample_ids += unaffected_sibling_sample_ids

        # If only proband, we are not able to compute compound heterozygous candidate alleles.
        if len(sample_ids) == 1:
            return set()

        # Get candidates for compound heterozygosity. Covers the following rules:
        # 1. A variant has to be in a heterozygous state in all affected individuals.
        # 2. A variant must not occur in a homozygous state in any of the unaffected individuals.
        # 3. A variant that is heterozygous in an affected child must be
        #    heterozygous in exactly one of the parents.

        compound_candidates_filters: list[ColumnElement[bool]] = []
        # Heterozygous in affected
        compound_candidates_filters += [
            getattr(genotype_table.c, f"{s}_type") == "Heterozygous" for s in affected_sample_ids
        ]
        # Not homozygous in unaffected
        compound_candidates_filters += [
            # Normally null would be included in below filter,
            # set as Reference to make comparison work
            func.coalesce(getattr(genotype_table.c, f"{s}_type"), "Reference") != "Homozygous"
            for s in unaffected_sample_ids
        ]
        if father_sample_id and mother_sample_id:
            # Heterozygous in _exactly one_ parent.
            # Note: This will also exclude any alleles with 'No coverage' in either parent.
            compound_candidates_filters.append(
                or_(
                    and_(
                        getattr(genotype_table.c, f"{father_sample_id}_type") == "Heterozygous",
                        getattr(genotype_table.c, f"{mother_sample_id}_type") == "Reference",
                    ),
                    and_(
                        getattr(genotype_table.c, f"{father_sample_id}_type") == "Reference",
                        getattr(genotype_table.c, f"{mother_sample_id}_type") == "Heterozygous",
                    ),
                )
            )

        compound_candidates = (
            select(
                genotype_table.c.allele_id,
                *[getattr(genotype_table.c, f"{s}_type") for s in sample_ids],
            )
            .where(*compound_candidates_filters)
            .subquery("compound_candidates")
        )

        # Group per gene and get the gene symbols with >= 2 candidates.
        #
        # Covers the following rules:
        # 4. A gene must have two or more heterozygous variants in each of the affected individuals.
        # 5. There must be at least one variant transmitted from the paternal side
        #    and one transmitted from the maternal side.
        #
        # Note: We use symbols instead of HGNC id since some
        #  symbols have no id in the annotation data
        #  One allele can be in several genes, and the gene symbol set is more extensive than only
        #  the symbols having HGNC ids so it should be safer to user.
        #
        # candidates_with_genes example:
        # ----------------------------------------------------------------------
        # | allele_id | Proband_type | Father_type  | Mother_type   | symbol   |
        # ----------------------------------------------------------------------
        # | 6006      | Heterozygous | Heterozygous | Reference     | KIAA0586 |
        # | 6005      | Heterozygous | Reference    | Heterozygous  | KIAA0586 |
        # | 6004      | Heterozygous | Reference    | Heterozygous  | KIAA0586 |
        # | 5528      | Heterozygous | Heterozygous | Reference     | TUBA1A   |
        # | 5529      | Heterozygous | Heterozygous | Reference     | TUBA1A   |
        #
        # In the above example, 6004, 6005 and 6006 satisfy the rules.

        filter_list = []
        if self.config and "inclusion_regex" in self.config["transcripts"]:
            filter_list.append(
                text("annotationshadowtranscript.transcript ~ :reg").params(
                    reg=self.config["transcripts"]["inclusion_regex"]
                )
            )
        candidates_with_genes_columns = [
            compound_candidates.c.allele_id,
            annotationshadow.AnnotationShadowTranscript.symbol,
        ]
        if father_sample_id and mother_sample_id:
            candidates_with_genes_columns += [
                getattr(compound_candidates.c, f"{father_sample_id}_type"),
                getattr(compound_candidates.c, f"{mother_sample_id}_type"),
            ]
        candidates_with_genes = (
            select(*candidates_with_genes_columns)
            .join(
                annotationshadow.AnnotationShadowTranscript,
                compound_candidates.c.allele_id
                == annotationshadow.AnnotationShadowTranscript.allele_id,
            )
            .where(*filter_list)
            .distinct()
            .subquery("candidates_with_genes")
        )

        # General criteria, 2 or more alleles in this gene
        # (rule 1 above covered that they are heterozygous in affected)
        compound_heterozygous_symbols_having = [func.count(candidates_with_genes.c.allele_id) > 1]

        # If parents, heterozygous in both
        if father_sample_id and mother_sample_id:
            compound_heterozygous_symbols_having += [
                # bool_or: at least one allele in this gene is 'Heterozygous'
                func.bool_or(
                    getattr(candidates_with_genes.c, f"{father_sample_id}_type") == "Heterozygous"
                ),
                func.bool_or(
                    getattr(candidates_with_genes.c, f"{mother_sample_id}_type") == "Heterozygous"
                ),
            ]
        compound_heterozygous_symbols = (
            select(candidates_with_genes.c.symbol)
            .group_by(candidates_with_genes.c.symbol)
            .having(and_(*compound_heterozygous_symbols_having))
        )

        compound_heterozygous_allele_ids = (
            select(candidates_with_genes.c.allele_id)
            .where(filters.in_(candidates_with_genes.c.symbol, compound_heterozygous_symbols))
            .distinct()
        )

        return set(self.session.execute(compound_heterozygous_allele_ids).scalars().all())

    def no_coverage_father_mother(
        self, genotype_table: Table, father_sample_id: int, mother_sample_id: int
    ) -> set[int]:
        if not father_sample_id or not mother_sample_id:
            return set()

        no_coverage_allele_ids = select(genotype_table.c.allele_id).where(
            or_(
                getattr(genotype_table.c, f"{father_sample_id}_type") == "No coverage",
                getattr(genotype_table.c, f"{mother_sample_id}_type") == "No coverage",
            )
        )

        return set(self.session.execute(no_coverage_allele_ids).scalars().all())

    def homozygous_unaffected_siblings(
        self, genotype_table: Table, proband_sample: str, unaffected_sibling_sample_ids: list[str]
    ) -> set[int]:
        """
        Checks whether a homozygous variant in proband is also homozgyous in unaffected sibling.

        Returns all variants that are also homozygous in unaffected siblings.
        """

        # If no unaffected, we'll have no alleles in the result
        if not unaffected_sibling_sample_ids:
            return set()

        filters = list()
        for s in [proband_sample] + unaffected_sibling_sample_ids:
            filters.append(getattr(genotype_table.c, f"{s}_type") == "Homozygous")

        homozygous_unaffected_result = select(genotype_table.c.allele_id).where(*filters)

        return set(self.session.execute(homozygous_unaffected_result).scalars().all())

    def get_family_ids(self, analysis_id: int):
        return (
            self.session.execute(
                select(sample.Sample.family_id).where(
                    sample.Sample.analysis_id == analysis_id,
                    sample.Sample.proband.is_(True),
                    ~sample.Sample.family_id.is_(None),
                )
            )
            .scalars()
            .all()
        )

    def get_family_sample_ids(self, analysis_id: int, family_id: str):
        return (
            self.session.execute(
                select(sample.Sample.id).where(
                    sample.Sample.analysis_id == analysis_id, sample.Sample.family_id == family_id
                )
            )
            .scalars()
            .all()
        )

    def get_allele_ids_in_samples(
        self, allele_ids: Sequence[int], family_sample_ids: Sequence[int]
    ):
        return (
            self.session.execute(
                select(allele.Allele.id)
                .join(genotype.Genotype.alleles)
                .join(sample.Sample)
                .where(
                    filters.in_(allele.Allele.id, allele_ids),
                    filters.in_(sample.Sample.id, family_sample_ids),
                )
            )
            .scalars()
            .all()
        )

    def get_proband_sample(self, analysis_id: int, sample_family_id: str):
        proband_sample = self.session.execute(
            select(sample.Sample).where(
                sample.Sample.proband.is_(True),
                sample.Sample.affected.is_(True),
                sample.Sample.analysis_id == analysis_id,
                sample.Sample.family_id == sample_family_id,
            )
        ).scalar_one()

        return proband_sample

    def get_father_sample(self, proband_sample: sample.Sample):
        father_sample = self.session.execute(
            select(sample.Sample).where(sample.Sample.id == proband_sample.father_id)
        ).scalar_one_or_none()

        return father_sample

    def get_mother_sample(self, proband_sample):
        mother_sample = self.session.execute(
            select(sample.Sample).where(sample.Sample.id == proband_sample.mother_id)
        ).scalar_one_or_none()

        return mother_sample

    def get_siblings_samples(self, proband_sample, affected=False):
        siblings_samples = (
            self.session.execute(
                select(sample.Sample).where(
                    sample.Sample.family_id == proband_sample.family_id,
                    sample.Sample.sibling_id == proband_sample.id,
                    sample.Sample.affected.is_(affected),
                )
            )
            .scalars()
            .all()
        )

        return siblings_samples

    def get_segregation_results(
        self,
        analysis_id: int,
        allele_ids: list[int],
        filter_config: SegregationModel.SegregationConfig,
    ) -> dict[str, set[int]]:
        family_ids = self.get_family_ids(analysis_id)

        result: dict[str, set[int]] = dict()
        # All filters below need a family data set to work on
        assert family_ids, "No family data set found for analysis"

        # Only one family id (i.e. data set) is supported at the moment
        assert len(family_ids) == 1

        proband_sample = self.get_proband_sample(analysis_id, family_ids[0])
        father_sample = self.get_father_sample(proband_sample)
        mother_sample = self.get_mother_sample(proband_sample)
        affected_sibling_samples = self.get_siblings_samples(proband_sample, affected=True)
        unaffected_sibling_samples = self.get_siblings_samples(proband_sample, affected=False)

        proband_sample_id = proband_sample.id
        father_sample_id = father_sample.id if father_sample else None
        mother_sample_id = mother_sample.id if mother_sample else None
        affected_sibling_sample_ids = [s.id for s in affected_sibling_samples]
        unaffected_sibling_sample_ids = [s.id for s in unaffected_sibling_samples]
        family_sample_ids = self.get_family_sample_ids(analysis_id, family_ids[0])

        # Exclude allele ids not part of the family samples
        # Happens when there are non-trio proband sample(s)
        family_allele_ids = self.get_allele_ids_in_samples(allele_ids, family_sample_ids)
        non_family_allele_ids = set(allele_ids) - set(family_allele_ids)

        genotype_table = get_genotype_temp_table(
            self.session,
            family_allele_ids,
            family_sample_ids,
            genotypesampledata_extras={
                "ar": "allele_ratio",
                "gl": "genotype_likelihood",
                "gq": "genotype_quality",
            },
        )

        result["segregation_not_applicable"] = non_family_allele_ids

        if filter_config.no_coverage_parents.enable:
            result["no_coverage_parents"] = self.no_coverage_father_mother(
                genotype_table, father_sample_id, mother_sample_id
            )
        else:
            result["no_coverage_parents"] = set()

        if filter_config.denovo.enable:
            result["denovo"] = self.denovo(
                genotype_table,
                proband_sample_id,
                father_sample_id,
                mother_sample_id,
                filter_config.denovo,
            )
        else:
            result["denovo"] = set()

        if filter_config.parental_mosaicism.enable:
            result["parental_mosaicism"] = self.parental_mosaicism(
                genotype_table, proband_sample_id, father_sample_id, mother_sample_id
            )
        else:
            result["parental_mosaicism"] = set()

        if filter_config.compound_heterozygous.enable:
            result["compound_heterozygous"] = self.compound_heterozygous(
                genotype_table,
                proband_sample_id,
                father_sample_id,
                mother_sample_id,
                affected_sibling_sample_ids=affected_sibling_sample_ids,
                unaffected_sibling_sample_ids=unaffected_sibling_sample_ids,
            )
        else:
            result["compound_heterozygous"] = set()

        if filter_config.recessive_homozygous.enable:
            result["autosomal_recessive_homozygous"] = self.autosomal_recessive_homozygous(
                genotype_table,
                proband_sample_id,
                father_sample_id,
                mother_sample_id,
                affected_sibling_sample_ids=affected_sibling_sample_ids,
                unaffected_sibling_sample_ids=unaffected_sibling_sample_ids,
            )

            result["xlinked_recessive_homozygous"] = self.xlinked_recessive_homozygous(
                genotype_table,
                proband_sample_id,
                father_sample_id,
                mother_sample_id,
                affected_sibling_sample_ids=affected_sibling_sample_ids,
                unaffected_sibling_sample_ids=unaffected_sibling_sample_ids,
            )

            result["homozygous_unaffected_siblings"] = self.homozygous_unaffected_siblings(
                genotype_table, proband_sample_id, unaffected_sibling_sample_ids
            )
        else:
            result.update(
                {
                    "autosomal_recessive_homozygous": set(),
                    "xlinked_recessive_homozygous": set(),
                    "homozygous_unaffected_siblings": set(),
                }
            )
        return result

    @override
    def filter_with_analysis(
        self,
        analysis_id: int,
        allele_ids: list[int],
        filter_config: SegregationModel.SegregationConfig,
    ) -> set[int]:
        """
        Returns allele_ids that can be filtered _out_ from an analysis.
        """
        segregation_results = self.get_segregation_results(analysis_id, allele_ids, filter_config)

        family_ids = self.get_family_ids(analysis_id)
        assert len(family_ids) == 1, "Multiple family ids are not supported yet"

        filtered: set[int] = set()
        # Most filtering needs a trio
        proband_sample = self.get_proband_sample(analysis_id, family_ids[0])
        has_parents = proband_sample.father_id and proband_sample.mother_id

        if has_parents:
            non_filtered = (
                segregation_results["denovo"]
                | segregation_results["parental_mosaicism"]
                | segregation_results["compound_heterozygous"]
                | segregation_results["autosomal_recessive_homozygous"]
                | segregation_results["xlinked_recessive_homozygous"]
                | segregation_results["no_coverage_parents"]
            )

            filtered = set(allele_ids) - non_filtered

        # Following can always be added to filtered (is empty when no siblings)
        filtered = filtered | segregation_results["homozygous_unaffected_siblings"]

        # Allele ids not applicable for segregation filtering should never be filtered here
        filtered -= segregation_results["segregation_not_applicable"]

        return filtered
