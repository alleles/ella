import operator
from typing import override

from sqlalchemy import select

from ella.datalayer import filters
from ella.datalayer.allelefilter.filterbase import FilterBase
from ella.vardb.datamodel import allele
from ella.vardb.datamodel.pydantic.filterconfig import SizeModel

OPERATORS = {
    "==": operator.eq,
    ">=": operator.ge,
    "<=": operator.le,
    ">": operator.gt,
    "<": operator.lt,
}


class SizeFilter(FilterBase):
    CONTEXT_DEPENDENT = False
    FILTER_WITH = "allele"

    @override
    def filter_with_alleles(
        self, allele_ids: list[int], filter_config: SizeModel.SizeConfig
    ) -> set[int]:
        """
        Return the allele ids, among the provided allele_ids, that have an allele
        length specified by the size in threshold, and operator set in mode.

        Example configuration:

        {
            "name": "size",
            "config": {
                "threshold": 10000,
                "mode": ">"
            }
        }

        Keeping all alleles with length above 10kb
        """

        return set(
            self.session.execute(
                select(allele.Allele.id).where(
                    filters.in_(allele.Allele.id, allele_ids),
                    OPERATORS[filter_config.mode](allele.Allele.length, filter_config.threshold),
                )
            )
            .scalars()
            .all()
        )
