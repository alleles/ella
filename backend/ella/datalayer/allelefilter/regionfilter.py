from collections.abc import Iterable
from typing import Any, override

from sqlalchemy import and_, case, func, literal, or_, select, tuple_
from sqlalchemy.sql.expression import Case
from sqlalchemy.sql.schema import Table
from sqlalchemy.sql.selectable import TableClause

from ella.datalayer import filters, queries
from ella.datalayer.allelefilter.filterbase import FilterBase
from ella.vardb.datamodel import allele, annotationshadow, gene
from ella.vardb.datamodel.pydantic.filterconfig import RegionModel
from ella.vardb.util.sql_utils import create_temp_table


class RegionFilter(FilterBase):
    CONTEXT_DEPENDENT = False
    FILTER_WITH = "genepanel"

    def create_gene_padding_table(
        self, genepanel_key: tuple[str, str], filter_config: RegionModel.RegionConfig
    ) -> Table:
        """
        Create a temporary table for the gene specific padding of the form
        ------------------------------------------------------------------------------------------------
        | hgnc_id | exon_upstream | exon_downstream | coding_region_upstream | coding_region_downstream |
        ------------------------------------------------------------------------------------------------
        | 26113   | -35           | 15              | -20                    | 20                       |
        | 28163   | -20           | 6               | -20                    | 20                       |
        | 2567    | -20           | 6               | -50                    | 50                       |

        Returns an ORM-representation of this table
        """

        # Fetch all gene ids associated with the genepanel
        assert filter_config.utr_region and filter_config.splice_region
        return create_temp_table(
            self.session,
            select(
                gene.Transcript.gene_id.label("hgnc_id"),
                literal(filter_config.splice_region[0]).label("exon_upstream"),
                literal(filter_config.splice_region[1]).label("exon_downstream"),
                literal(filter_config.utr_region[0]).label("coding_region_upstream"),
                literal(filter_config.utr_region[1]).label("coding_region_downstream"),
            )
            .join(gene.Genepanel.transcripts)
            .join(gene.Gene)
            .where(tuple_(gene.Genepanel.name, gene.Genepanel.version) == genepanel_key),
            "tmp_gene_padding",
        )

    def create_genepanel_transcripts_table(
        self, genepanel_key: tuple[str, str], allele_ids: Iterable[int], max_padding: int
    ) -> TableClause:
        # Extract transcripts associated with the genepanel. Unnest all exons (start, end).
        # To potentially limit the number of regions we need to check, exclude transcripts where we have no alleles
        # overlapping in the region [tx_start-max_padding, tx_end+max_padding]. The filter clause in the select
        # should have no effect on the result, but is included only for performance
        #
        # Note: Exons and coding regions are transformed to closed intervals [start, end], rather than the half-open database representation [start, end)
        #
        # Returned temp table is of the form:
        # -----------------------------------------------------------------------------------------------
        # | gene_id | transcript_name | strand | cds_start | cds_end | exon_start | exon_end | allele_id |
        # -----------------------------------------------------------------------------------------------
        # | 329     | NM_198576.3     | +      | 955552    | 990360  | 955502     | 955752   | 1         |
        # | 329     | NM_198576.3     | +      | 955552    | 990360  | 957580     | 957841   | 1         |
        # | 329     | NM_198576.3     | +      | 955552    | 990360  | 970656     | 970703   | 1         |
        # | 329     | NM_198576.3     | +      | 955552    | 990360  | 976044     | 976259   | 1         |
        # | 329     | NM_198576.3     | +      | 955552    | 990360  | 976552     | 976776   | 1         |
        # | 329     | NM_198576.3     | +      | 955552    | 990360  | 976857     | 977081   | 1         |

        statement = (
            select(
                gene.Transcript.gene_id,
                gene.Transcript.transcript_name,
                gene.Transcript.strand,
                gene.Transcript.cds_start,
                (gene.Transcript.cds_end - 1).label("cds_end"),
                func.unnest(gene.Transcript.exon_starts).label("exon_start"),
                (func.unnest(gene.Transcript.exon_ends) - 1).label("exon_end"),
                allele.Allele.id.label("allele_id"),
            )
            .join(
                gene.GenepanelTranscript,
                and_(
                    gene.GenepanelTranscript.transcript_id == gene.Transcript.id,
                    tuple_(
                        gene.GenepanelTranscript.genepanel_name,
                        gene.GenepanelTranscript.genepanel_version,
                    )
                    == genepanel_key,
                ),
            )
            .join(
                allele.Allele,
                and_(
                    filters.in_(allele.Allele.id, allele_ids),
                    allele.Allele.chromosome == gene.Transcript.chromosome,
                    or_(
                        and_(
                            allele.Allele.start_position >= gene.Transcript.tx_start - max_padding,
                            allele.Allele.start_position
                            <= gene.Transcript.tx_end - 1 + max_padding,
                        ),
                        and_(
                            allele.Allele.open_end_position
                            > gene.Transcript.tx_start - max_padding,
                            allele.Allele.open_end_position < gene.Transcript.tx_end + max_padding,
                        ),
                    ),
                ),
            )
        )
        return create_temp_table(
            self.session, statement, "tmp_region_filter_internal_genepanel_regions"
        )

    def get_coding_regions(self, genepanel_tx_regions: TableClause):
        # Coding regions
        # The coding regions may start within an exon, and we truncate the exons where this is the case
        # For example, if an exon is defined by positions [10,20], but cds_start is 15, we include the region [15,20]
        # |          +--------------+------------+           +-------------+             +-----------+-------+        |
        # |----------+              cccccccccccccc-----------ccccccccccccccc-------------ccccccccccccc       +--------|
        # |          +--------------+------------+           +-------------+             +-----------+-------+        |
        coding_start = case(
            (
                genepanel_tx_regions.c.cds_start > genepanel_tx_regions.c.exon_start,
                genepanel_tx_regions.c.cds_start,
            ),
            else_=genepanel_tx_regions.c.exon_start,
        )

        coding_end = case(
            (
                genepanel_tx_regions.c.cds_end < genepanel_tx_regions.c.exon_end,
                genepanel_tx_regions.c.cds_end,
            ),
            else_=genepanel_tx_regions.c.exon_end,
        )

        transcript_coding_regions = select(
            genepanel_tx_regions.c.transcript_name.label("transcript_name"),
            coding_start.label("region_start"),
            coding_end.label("region_end"),
        ).where(
            # Exclude exons outside the coding region
            genepanel_tx_regions.c.exon_start <= genepanel_tx_regions.c.cds_end,
            genepanel_tx_regions.c.exon_end >= genepanel_tx_regions.c.cds_start,
        )

        return transcript_coding_regions

    # Regions with applied padding
    def _create_padded_regions(
        self,
        transcripts: TableClause,
        region_start: Case[Any],
        region_end: Case[Any],
        tmp_gene_padding: Table,
    ):
        return (
            select(
                transcripts.c.transcript_name.label("transcript_name"),
                region_start.label("region_start"),
                region_end.label("region_end"),
            )
            .join(tmp_gene_padding, tmp_gene_padding.c.hgnc_id == transcripts.c.gene_id)
            # Only include valid regions. region_start will be greater than region_end when padding == 0 .
            # Example: [cds_end + 1, cds_end + padding]
            .where(region_end >= region_start)
            .distinct()
        )

    def get_splice_regions(self, genepanel_tx_regions: TableClause, tmp_gene_padding: Table):
        # Splicing upstream
        # Include region upstream of the exon (not including exon starts or ends)

        # Transcript on positive strand:
        # |          +--------------+------------+           +-----------------+             +-----------+-------+        |
        # |-------iii+              |            +--------iii+                 +----------iii+           |       +--------|
        # |          +--------------+------------+           +-----------------+             +-----------+-------+        |
        # Transcript on reverse strand:
        # |          +--------------+------------+           +-----------------+             +-----------+-------+        |
        # |----------+              |            +iii--------+                 +iii----------+           |       +iii-----|
        # |          +--------------+------------+           +-----------------+             +-----------+-------+        |

        # Upstream region for positive strand transcript is [exon_start+exon_upstream, exon_start-1]
        # Downstream region for reverse strand transcript is [exon_end+1, exon_end-exon_upstream]
        # Note: exon_upstream is a *negative* number

        splicing_upstream_start = case(
            (genepanel_tx_regions.c.strand == "-", genepanel_tx_regions.c.exon_end + 1),
            else_=genepanel_tx_regions.c.exon_start + tmp_gene_padding.c.exon_upstream,
        )

        splicing_upstream_end = case(
            (
                genepanel_tx_regions.c.strand == "-",
                genepanel_tx_regions.c.exon_end - tmp_gene_padding.c.exon_upstream,
            ),
            else_=genepanel_tx_regions.c.exon_start - 1,
        )

        splicing_region_upstream = self._create_padded_regions(
            genepanel_tx_regions, splicing_upstream_start, splicing_upstream_end, tmp_gene_padding
        )

        # Splicing downstream
        # Include region downstream of the exon (not including exon starts or ends)
        # Transcript on positive strand:
        # |          +--------------+------------+           +----------------+             +-----------+-------+        |
        # |----------+              |            +ii---------+                +ii-----------+           |       +ii------|
        # |          +--------------+------------+           +----------------+             +-----------+-------+        |
        # Transcript on reverse strand:
        # |          +--------------+------------+           +----------------+             +-----------+-------+        |
        # |--------ii+              |            +---------ii+                +-----------ii+           |       +--------|
        # |          +--------------+------------+           +----------------+             +-----------+-------+        |

        # Downstream region for positive strand transcript is [exon_end+1, exon_end+exon_downstream]
        # Downstream region for reverse strand transcript is [exon_start-exon_downstream, exon_start-1]
        splicing_downstream_start = case(
            (
                genepanel_tx_regions.c.strand == "-",
                genepanel_tx_regions.c.exon_start - tmp_gene_padding.c.exon_downstream,
            ),
            else_=genepanel_tx_regions.c.exon_end + 1,
        )

        splicing_downstream_end = case(
            (genepanel_tx_regions.c.strand == "-", genepanel_tx_regions.c.exon_start - 1),
            else_=genepanel_tx_regions.c.exon_end + tmp_gene_padding.c.exon_downstream,
        )

        splicing_region_downstream = self._create_padded_regions(
            genepanel_tx_regions,
            splicing_downstream_start,
            splicing_downstream_end,
            tmp_gene_padding,
        )

        return splicing_region_upstream.union(splicing_region_downstream)

    def get_utr_regions(self, genepanel_tx_regions: TableClause, tmp_gene_padding: Table):
        # UTR upstream
        # Do not include cds_start or cds_end, as these are not in the UTR
        # Transcript on positive strand:
        # |          +--------------+------------+           +---------------+             +-----------+-------+        |
        # |----------+          uuuu|            +-----------+               +-------------+           |       +--------|
        # |          +--------------+------------+           +---------------+             +-----------+-------+        |
        # Transcript on reverse strand:
        # |          +--------------+------------+           +---------------+             +-----------+-------+        |
        # |----------+              |            +-----------+               +-------------+           |uuuu   +--------|
        # |          +--------------+------------+           +---------------+             +-----------+-------+        |

        # UTR upstream region for positive strand transcript is [cds_start-1, cds_start+coding_region_upstream]
        # UTR upstream region for reverse strand transcript is [cds_end-coding_region_upstream, cds_end+1]
        # Note: coding_region_upstream is a *negative* number
        utr_upstream_start = case(
            (genepanel_tx_regions.c.strand == "-", genepanel_tx_regions.c.cds_end + 1),
            else_=genepanel_tx_regions.c.cds_start + tmp_gene_padding.c.coding_region_upstream,
        )

        utr_upstream_end = case(
            (
                genepanel_tx_regions.c.strand == "-",
                genepanel_tx_regions.c.cds_end - tmp_gene_padding.c.coding_region_upstream,
            ),
            else_=genepanel_tx_regions.c.cds_start - 1,
        )

        utr_region_upstream = self._create_padded_regions(
            genepanel_tx_regions, utr_upstream_start, utr_upstream_end, tmp_gene_padding
        )

        # UTR downstream
        # Do not include cds_start or cds_end, as these are not in the UTR
        # Transcript on positive strand:
        # |          +--------------+------------+           +--------------+             +-----------+-------+        |
        # |----------+              |            +-----------+              +-------------+           |uuuu   +--------|
        # |          +--------------+------------+           +--------------+             +-----------+-------+        |
        # Transcript on reverse strand:
        # |          +--------------+------------+           +--------------+             +-----------+-------+        |
        # |----------+          uuuu|            +-----------+              +-------------+           |       +--------|
        # |          +--------------+------------+           +--------------+             +-----------+-------+        |

        # UTR downstream region for positive strand transcript is [cds_end+1, cds_end+coding_region_downstream]
        # UTR downstream region for reverse strand transcript is [cds_start-coding_region_downstream, cds_start-1]
        utr_downstream_start = case(
            (
                genepanel_tx_regions.c.strand == "-",
                genepanel_tx_regions.c.cds_start - tmp_gene_padding.c.coding_region_downstream,
            ),
            else_=genepanel_tx_regions.c.cds_end + 1,
        )

        utr_downstream_end = case(
            (genepanel_tx_regions.c.strand == "-", genepanel_tx_regions.c.cds_start - 1),
            else_=genepanel_tx_regions.c.cds_end + tmp_gene_padding.c.coding_region_downstream,
        )
        utr_region_downstream = self._create_padded_regions(
            genepanel_tx_regions, utr_downstream_start, utr_downstream_end, tmp_gene_padding
        )

        return utr_region_upstream.union(utr_region_downstream)

    @override
    def filter_with_genepanel(
        self,
        genepanel_key: tuple[str, str],
        allele_ids: list[int],
        filter_config: RegionModel.RegionConfig,
    ) -> set[int]:
        """
        Filter alleles outside regions of interest.
        Regions of interest are based on these criteria:
         - Coding region
         - Splice region
         - UTR region (upstream/downstream of coding start/coding end)
         - Custom regions

        These are all based on the transcript definition of the genepanel
        transcripts with genomic coordinates for
        - Transcript (tx) start and end
        - Coding region (cds) start and end
        - Exon start and end

        UTR regions are upstream/downstream of cds start/end, with padding specified in config
        Splice regions are upstream/downstream of exon start/end, with padding specified in config

          |          +--------------+------------+           +------------------------+             +-----------+-------+        |
          |----------+              |            +-----------+                        +-------------+           |       +--------|
          |          +--------------+------------+           +------------------------+             +-----------+-------+        |
         tx        exon           coding        exon        exon                     exon          exon       coding   exon     tx
         start     start          start         end         start                    end           start       end     end      end

        """

        custom_regions = (
            select(
                gene.Region.chromosome,
                gene.Region.start,
                gene.Region.end,
            )
            .join(gene.GenepanelRegion)
            .where(
                tuple_(gene.GenepanelRegion.genepanel_name, gene.GenepanelRegion.genepanel_version)
                == genepanel_key,
            )
            .subquery()
        )

        allele_ids_in_custom_regions = select(allele.Allele.id).where(
            filters.in_(allele.Allele.id, allele_ids),
            allele.Allele.chromosome == custom_regions.c.chromosome,
            or_(
                # Contained within or overlapping region
                and_(
                    allele.Allele.start_position >= custom_regions.c.start,
                    allele.Allele.start_position <= custom_regions.c.end,
                ),
                and_(
                    allele.Allele.open_end_position > custom_regions.c.start,
                    allele.Allele.open_end_position < custom_regions.c.end,
                ),
                # Region contained within variant
                and_(
                    allele.Allele.start_position <= custom_regions.c.start,
                    allele.Allele.open_end_position >= custom_regions.c.end,
                ),
            ),
        )

        allele_ids_outside_region = set(allele_ids) - set(
            self.session.execute(allele_ids_in_custom_regions).scalars().all()
        )

        tmp_gene_padding = self.create_gene_padding_table(genepanel_key, filter_config)

        select_max_padding = select(
            func.abs(func.max(tmp_gene_padding.c.exon_upstream)),
            func.abs(func.max(tmp_gene_padding.c.exon_downstream)),
            func.abs(func.max(tmp_gene_padding.c.coding_region_upstream)),
            func.abs(func.max(tmp_gene_padding.c.coding_region_downstream)),
        )
        max_padding = max(*self.session.execute(select_max_padding).tuples().all())

        # Create temp tables
        genepanel_tx_regions = self.create_genepanel_transcripts_table(
            genepanel_key, allele_ids_outside_region, max_padding
        )

        # Create queries for
        # - Coding regions
        # - Splice regions
        # - UTR regions
        transcript_coding_regions = self.get_coding_regions(genepanel_tx_regions)
        splicing_regions = self.get_splice_regions(genepanel_tx_regions, tmp_gene_padding)
        utr_regions = self.get_utr_regions(genepanel_tx_regions, tmp_gene_padding)

        all_regions = create_temp_table(
            self.session,
            transcript_coding_regions.union(splicing_regions, utr_regions),
            "all_regions",
            index_columns=[
                "transcript_name"
            ],  # Use transcript name as index for faster lookups (faster than chromosome - which is not available in the temp table)
        )

        # Find allele ids within genomic region
        # We need to restrict comparison for each allele to happen on positions
        # inside the transcript(s) (or else we could compare across same position on different chromosomes)
        # (using transcript(s) rather than chromosome as key is a lot faster)
        allele_transcripts = (
            select(genepanel_tx_regions.c.allele_id, genepanel_tx_regions.c.transcript_name)
            .distinct()
            .subquery()
        )

        allele_ids_in_genomic_region = (
            select(allele.Allele.id)
            .join(allele_transcripts, allele_transcripts.c.allele_id == allele.Allele.id)
            .join(
                all_regions,
                all_regions.c.transcript_name == allele_transcripts.c.transcript_name,
            )
            .where(
                # The following filter should be redundant, since allele_transcripts is
                # already limited to allele_ids, but we'll keep it for extra safety
                filters.in_(allele.Allele.id, allele_ids_outside_region),
                or_(
                    # Contained within or overlapping region
                    and_(
                        allele.Allele.start_position >= all_regions.c.region_start,
                        allele.Allele.start_position <= all_regions.c.region_end,
                    ),
                    and_(
                        allele.Allele.open_end_position > all_regions.c.region_start,
                        allele.Allele.open_end_position < all_regions.c.region_end,
                    ),
                    # Region contained within variant
                    and_(
                        allele.Allele.start_position <= all_regions.c.region_start,
                        allele.Allele.open_end_position >= all_regions.c.region_end,
                    ),
                ),
            )
        )

        allele_ids_outside_region -= set(
            self.session.execute(allele_ids_in_genomic_region).scalars().all()
        )

        #
        # Save alleles based on computed HGVSc distance
        #
        # We look at computed exon_distance/coding_region_distance from annotation
        # on transcripts present in the genepanel (disregarding version number)
        # For alleles with computed distance within splice_region or utr_region,
        # they will not be filtered out
        # This can happen when there is a mismatch between genomic position and annotated HGVSc.
        # Observed for alleles in repeated regions: In the imported VCF,
        # the alleles are left aligned. VEP left aligns w.r.t. *transcript direction*,
        # and therefore, there could be a mismatch in position
        # See for example
        # https://variantvalidator.org/variantvalidation/?variant=NM_020366.3%3Ac.907-16_907-14delAAT&primary_assembly=GRCh37&alignment=splign
        annotation_transcripts_genepanel = create_temp_table(
            self.session,
            queries.annotation_transcripts_genepanel(
                self.session, [genepanel_key], list(allele_ids_outside_region)
            ),
            "tmp_annotation_transcript_genepanel",
        )

        q_allele_ids_in_hgvsc_region = (
            select(
                annotationshadow.AnnotationShadowTranscript.allele_id,
            )
            .join(
                # Join in transcripts used in annotation
                # Note: The annotation_transcripts_genepanel only contains transcripts
                # matching transcripts in the genepanel.
                # Therefore, we are sure that we only filter here on genepanel
                # transcripts (disregarding version number)
                annotation_transcripts_genepanel,
                and_(
                    annotation_transcripts_genepanel.c.annotation_transcript
                    == annotationshadow.AnnotationShadowTranscript.transcript,
                    annotation_transcripts_genepanel.c.allele_id
                    == annotationshadow.AnnotationShadowTranscript.allele_id,
                ),
            )
            .join(
                # Join in gene padding table, to use gene specific padding
                tmp_gene_padding,
                tmp_gene_padding.c.hgnc_id == annotation_transcripts_genepanel.c.genepanel_hgnc_id,
            )
            .where(
                annotationshadow.AnnotationShadowTranscript.exon_distance
                >= tmp_gene_padding.c.exon_upstream,
                annotationshadow.AnnotationShadowTranscript.exon_distance
                <= tmp_gene_padding.c.exon_downstream,
                or_(
                    # We do not save exonic UTR alleles if they are
                    # outside [coding_region_upstream, coding_region_downstream]
                    # For coding exonic variant, the coding_region_distance is None
                    annotationshadow.AnnotationShadowTranscript.coding_region_distance.is_(None),
                    and_(
                        annotationshadow.AnnotationShadowTranscript.coding_region_distance
                        >= tmp_gene_padding.c.coding_region_upstream,
                        annotationshadow.AnnotationShadowTranscript.coding_region_distance
                        <= tmp_gene_padding.c.coding_region_downstream,
                    ),
                ),
            )
        )

        allele_ids_in_hgvsc_region = set(
            self.session.execute(q_allele_ids_in_hgvsc_region).scalars().all()
        )
        allele_ids_outside_region -= allele_ids_in_hgvsc_region

        return allele_ids_outside_region
