from typing import Any, ClassVar, Literal

from sqlalchemy.orm.session import Session


class FilterBase:
    # If FILTER_WITH is set to "allele", the filter function used is filter_with_alleles
    # If FILTER_WITH is set to "analysis", the filter function used is filter_with_analysis
    # If FILTER_WITH is set to "genepanel", the filter function used is filter_with_genepanel
    FILTER_WITH: ClassVar[Literal["analysis", "genepanel", "allele"]] = "analysis"

    # If True, the filter is dependent on the allele in context of the other alleles passed to the filter
    # That is, these two ways of calling the filter will give (potentially) different results:
    #
    # result_a = set()
    # for allele_id in allele_ids:
    #    result.update(filter.filter_with_alleles([allele_id], filter_config))
    #
    #
    # result_b = filter.filter_with_alleles(allele_ids, filter_config)
    #
    # An example is the InheritanceModel filter which checks for compound heterozygote candidates,
    # If we send in one allele at a time, we will not see that they are compound candidates.
    CONTEXT_DEPENDENT = True

    def __init__(self, session: Session, config: dict[str, Any] | None) -> None:
        self.session = session
        self.config = config

    def filter_with_alleles(self, allele_ids: list[int], filter_config: Any) -> set[int]:
        raise NotImplementedError

    def filter_with_analysis(
        self, analysis_id: int, allele_ids: list[int], filter_config: Any
    ) -> set[int]:
        raise NotImplementedError

    def filter_with_genepanel(
        self,
        gp_key: tuple[str, str],
        allele_ids: list[int],
        filter_config: Any,
    ) -> set[int]:
        raise NotImplementedError
