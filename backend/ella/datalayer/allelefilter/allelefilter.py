import logging
from collections.abc import Sequence
from typing import TYPE_CHECKING

from sqlalchemy import select
from sqlalchemy.orm.session import Session

from ella.api.config import config as global_config
from ella.datalayer.allelefilter.callertypefilter import CallerTypeFilter
from ella.datalayer.allelefilter.classificationfilter import ClassificationFilter
from ella.datalayer.allelefilter.consequencefilter import ConsequenceFilter
from ella.datalayer.allelefilter.externalfilter import ExternalFilter
from ella.datalayer.allelefilter.frequencyfilter import FrequencyFilter
from ella.datalayer.allelefilter.genefilter import GeneFilter
from ella.datalayer.allelefilter.genericannotationfilter import GenericAnnotationFilter
from ella.datalayer.allelefilter.inheritancemodelfilter import InheritanceModelFilter
from ella.datalayer.allelefilter.polypyrimidinetractfilter import PolypyrimidineTractFilter
from ella.datalayer.allelefilter.qualityfilter import QualityFilter
from ella.datalayer.allelefilter.regionfilter import RegionFilter
from ella.datalayer.allelefilter.segregationfilter import SegregationFilter
from ella.datalayer.allelefilter.sizefilter import SizeFilter
from ella.vardb.datamodel import sample
from ella.vardb.datamodel.pydantic.filterconfig import (
    ConfigType,
    FilterConfigModel,
)

if TYPE_CHECKING:
    from ella.datalayer.allelefilter.filterbase import FilterBase

log = logging.getLogger(__name__)


class AlleleFilter:
    def __init__(self, session: Session, config: dict | None = None) -> None:
        self.session = session
        self.config = global_config if not config else config

        self.filter_objects: dict[str, FilterBase] = {
            "callertype": CallerTypeFilter(self.session, self.config),
            "classification": ClassificationFilter(self.session, self.config),
            "consequence": ConsequenceFilter(self.session, self.config),
            "external": ExternalFilter(self.session, self.config),
            "frequency": FrequencyFilter(self.session, self.config),
            "gene": GeneFilter(self.session, self.config),
            "genericannotation": GenericAnnotationFilter(self.session, self.config),
            "inheritancemodel": InheritanceModelFilter(self.session, self.config),
            "ppy": PolypyrimidineTractFilter(self.session, self.config),
            "quality": QualityFilter(self.session, self.config),
            "region": RegionFilter(self.session, self.config),
            "segregation": SegregationFilter(self.session, self.config),
            "size": SizeFilter(self.session, self.config),
        }

    def _apply_filter(
        self,
        filter_name: str,
        filter_config: ConfigType,
        genepanel: tuple[str, str],
        analysis_id: int,
        allele_ids: set[int],
    ) -> set[int]:
        if filter_name not in self.filter_objects:
            raise RuntimeError(f"Requested filter {filter_name} is not a valid filter name")

        filter_obj = self.filter_objects[filter_name]
        if filter_obj.FILTER_WITH == "allele":
            return filter_obj.filter_with_alleles(list(allele_ids), filter_config)
        elif filter_obj.FILTER_WITH == "genepanel":
            return filter_obj.filter_with_genepanel(genepanel, list(allele_ids), filter_config)
        elif filter_obj.FILTER_WITH == "analysis":
            return filter_obj.filter_with_analysis(analysis_id, list(allele_ids), filter_config)

    def run_filter_chain(
        self,
        filterconfig: FilterConfigModel,
        analysis_id: int,
        allele_ids: Sequence[int],
    ):
        """
        Filters alleles for a single analysis.

        Returns result:
            {
                'allele_ids': [1, 2, 3],
                'excluded_allele_ids': {
                    'frequency': [4, 5],
                    'region': [12, 45],
                    'segregation': [6, 8],
                }
            }
        """
        remaining_allele_ids = set(allele_ids)
        analysis_genepanel: tuple[str, str] = (
            self.session.execute(
                select(sample.Analysis.genepanel_name, sample.Analysis.genepanel_version).where(  # type: ignore[assignment]
                    sample.Analysis.id == analysis_id
                )
            )
            .tuples()
            .one()
        )

        result: dict = {"allele_ids": [], "excluded_allele_ids": dict()}

        for _filter in filterconfig.filters:
            try:
                filtered_allele_ids = self._apply_filter(
                    _filter.name,
                    _filter.config,
                    analysis_genepanel,
                    analysis_id,
                    remaining_allele_ids,
                )

                # We send in remaining_allele_ids for the CONTEXT_DEPENDENT filters
                # since they might need to take all alleles before filtering into account.
                # For the non CONTEXT_DEPENDENT filters it would not affect the end result if we sent
                # in remaining_allele_ids, but it would be less efficient
                # (since we would be filtering on a larger number of alleles)
                filter_exceptions: set[int] = set()

                for exception in _filter.exceptions or []:
                    filter_obj = self.filter_objects[exception.name]

                    if filter_obj.CONTEXT_DEPENDENT:
                        filter_exceptions |= self._apply_filter(
                            exception.name,
                            exception.config,
                            analysis_genepanel,
                            analysis_id,
                            remaining_allele_ids,
                        )
                    else:
                        filter_exceptions |= self._apply_filter(
                            exception.name,
                            exception.config,
                            analysis_genepanel,
                            analysis_id,
                            filtered_allele_ids,
                        )

                filtered_allele_ids = set(filtered_allele_ids) - filter_exceptions
                # Ensure that filter doesn't return allele_ids not part of input
                assert not filtered_allele_ids - set(
                    allele_ids
                ), f"Filter {_filter.name} returned allele_ids not in input"

                result["excluded_allele_ids"][_filter.name] = sorted(list(filtered_allele_ids))
                remaining_allele_ids -= filtered_allele_ids

            except Exception:
                log.error(f"Error while running filter '{_filter.name}'")
                raise

        result["allele_ids"] = sorted(list(remaining_allele_ids))
        return result
