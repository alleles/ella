import operator
from typing import override

from sqlalchemy import Subquery, and_, func, literal_column, or_, select
from sqlalchemy.sql.elements import BinaryExpression

from ella.datalayer import filters
from ella.datalayer.allelefilter.filterbase import FilterBase
from ella.vardb.datamodel import annotation
from ella.vardb.datamodel.pydantic.filterconfig import Combinations, ExternalModel

OPERATORS = {
    "==": operator.eq,
    ">=": operator.ge,
    "<=": operator.le,
    ">": operator.gt,
    "<": operator.lt,
}

HGMD_TAGS = set([None, "FP", "DM", "DFP", "R", "DP", "DM?"])


CLINVAR_CLINSIG_GROUPS = {
    "pathogenic": [
        "pathogenic",
        "likely pathogenic",
        "pathologic",
        "suspected pathogenic",
        "likely pathogenic - adrenal bilateral pheochromocy",
        "likely pathogenic - adrenal pheochromocytoma",
        "probable-pathogenic",
        "probably pathogenic",
        "pathogenic variant for bardet-biedl syndrome",
    ],
    "uncertain": ["uncertain", "variant of unknown significance", "uncertain significance"],
    "benign": [
        "benign",
        "suspected benign",
        "probable-non-pathogenic",
        "probably not pathogenic",
        "likely benign",
    ],
}

CLINVAR_NUM_STARS = {
    "no assertion criteria provided": 0,
    "no assertion provided": 0,
    "criteria provided, conflicting interpretations": 1,
    "criteria provided, single submitter": 1,
    "criteria provided, multiple submitters, no conflicts": 2,
    "reviewed by expert panel": 3,
    "practice guideline": 4,
}


# Ensure everything is lowercase
CLINVAR_CLINSIG_GROUPS = {
    k.lower(): [x.lower() for x in v] for k, v in CLINVAR_CLINSIG_GROUPS.items()
}


class ExternalFilter(FilterBase):
    CONTEXT_DEPENDENT = False
    FILTER_WITH = "allele"

    def _build_clinvar_filters(
        self, clinsig_counts: Subquery, combinations: list[Combinations]
    ) -> list[BinaryExpression]:
        """
        Combinations is given as a list of lists, like

            [["benign", ">", 5], # More than 5 benign submissions
            ["pathogenic", "==", 0], # No pathogenic submissions
            ["benign", ">", "uncertain"]] # More benign than uncertain submissions

        """

        def get_filter_count(v: str | int):
            if isinstance(v, str):
                assert v in ["benign", "pathogenic", "uncertain"]
                return getattr(clinsig_counts.c, v)
            else:
                assert isinstance(v, int | float)
                return v

        clinvar_filters = []
        for c in combinations:
            clinsig, op, count = c.left, OPERATORS[c.operator], get_filter_count(c.right)
            clinvar_filters.append(op(getattr(clinsig_counts.c, clinsig), count))
        return clinvar_filters

    def _filter_clinvar(
        self, allele_ids: list[int], clinvar_config: ExternalModel.ExternalConfig.ClinVar
    ) -> set[int]:
        # Use this to evaluate the number of stars
        if clinvar_config.num_stars:
            op, num_stars = clinvar_config.num_stars.operator, clinvar_config.num_stars.value
        else:
            op, num_stars = (">=", 0)
        star_op = OPERATORS[op]

        # Extract clinical_significance_status that matches the num_stars criteria
        # The clinical_significance_status to stars mapping is given in the config
        filter_signifiance_descr = [
            k for k, v in list(CLINVAR_NUM_STARS.items()) if star_op(v, num_stars)
        ]

        combinations = clinvar_config.combinations if clinvar_config.combinations else []

        # Expand clinvar submissions, where clinical_significance_status satisfies
        clinvar_entries = (
            select(
                annotation.Annotation.allele_id,
                literal_column(
                    "jsonb_array_elements(annotations->'external'->'CLINVAR'->'items')"
                ).label("entry"),
            )
            .where(
                filters.in_(annotation.Annotation.allele_id, allele_ids),
                annotation.Annotation.date_superceeded.is_(None),
                filters.in_(  # type: ignore[call-overload]
                    annotation.Annotation.annotations.op("->")("external")
                    .op("->")("CLINVAR")
                    .op("->>")("variant_description"),
                    filter_signifiance_descr,
                ),
            )
            .subquery()
        )

        # Extract clinical significance for all SCVs
        clinvar_clinsigs = (
            select(
                clinvar_entries.c.allele_id,
                clinvar_entries.c.entry.op("->>")("clinical_significance_descr").label("clinsig"),
            )
            .where(clinvar_entries.c.entry.op("->>")("rcv").op("ILIKE")("SCV%"))
            .subquery()
        )

        def count_matches(category: str):
            return func.count(clinvar_clinsigs.c.clinsig).filter(
                filters.in_(  # type: ignore[call-overload]
                    func.lower(clinvar_clinsigs.c.clinsig), CLINVAR_CLINSIG_GROUPS[category]
                )
            )

        # Count the number of Pathogenic/Likely pathogenic, Uncertain significance, and Benign/Likely benign
        # Note: This does not match any clinsig with e.g. Drug response or similar
        clinsig_counts = (
            select(
                clinvar_clinsigs.c.allele_id,
                *[count_matches(category).label(category) for category in CLINVAR_CLINSIG_GROUPS],
            )
            .group_by(clinvar_clinsigs.c.allele_id)
            .order_by(clinvar_clinsigs.c.allele_id)
            .subquery()
        )

        clinvar_filters = self._build_clinvar_filters(clinsig_counts, combinations)

        # Extract allele ids that matches the config rules
        filtered_allele_ids = select(clinsig_counts.c.allele_id).where(
            and_(True, *clinvar_filters)
        )  # Add True to make we don't filter on an empty and_-construct

        result = self.session.execute(filtered_allele_ids).scalars().all()
        inverse = clinvar_config.inverse
        if inverse:
            return set(allele_ids) - set(result)
        else:
            return set(result)

    def _filter_hgmd(
        self, allele_ids: list[int], hgmd_config: ExternalModel.ExternalConfig.HGMD
    ) -> set[int]:
        hgmd_tags = hgmd_config.tags
        assert hgmd_tags, "No tags provided to hgmd filter, even though config is defined"
        assert (
            not set(hgmd_tags) - HGMD_TAGS  # type: ignore[operator]
        ), f"Invalid tag(s) to filter on in {hgmd_tags}. Available tags are {HGMD_TAGS}."

        # Need to separate check for specific tag and check for no HGMD data (tag is None)
        hgmd_filters = []
        if None in hgmd_tags:
            hgmd_tags.pop(hgmd_tags.index(None))
            hgmd_filters.append(
                annotation.Annotation.annotations.op("->")("external")
                .op("->")("HGMD")
                .op("->>")("tag")
                .is_(None)
            )

        if hgmd_tags:
            hgmd_filters.append(
                filters.in_(  # type: ignore[call-overload]
                    annotation.Annotation.annotations.op("->")("external")
                    .op("->")("HGMD")
                    .op("->>")("tag"),
                    hgmd_tags,
                )
            )

        filtered_allele_ids = select(annotation.Annotation.allele_id).where(
            annotation.Annotation.date_superceeded.is_(None),
            filters.in_(annotation.Annotation.allele_id, allele_ids),
            or_(*hgmd_filters),
        )

        result = self.session.execute(filtered_allele_ids).scalars().all()
        if hgmd_config.inverse:
            return set(allele_ids) - set(result)
        else:
            return set(result)  # type: ignore[arg-type]

    @override
    def filter_with_alleles(
        self, allele_ids: list[int], filter_config: ExternalModel.ExternalConfig
    ) -> set[int]:
        """
        Filter alleles on external annotations. Supported external databases are clinvar and hgmd.
        Filters only alleles which satisify *both* clinvar and hgmd configurations.
        If only one of clinvar or hgmd is specified, filters on this alone.

        filter_config is specified like
        {
            "clinvar": {
                "combinations": [
                    ["benign", ">", 5], # More than 5 benign submissions
                    ["pathogenic", "==", 0], # No pathogenic submissions
                    ["benign", ">", "uncertain"] # More benign than uncertain submissions
                ],
                "num_stars": [">=", 2] # Only include variants with 2 or more stars
            },
            "hgmd": {
                "tags": [None], # Not in HGMD
            }
        }

        """

        clinvar_config = filter_config.clinvar
        if clinvar_config:
            clinvar_filtered_allele_ids = self._filter_clinvar(allele_ids, clinvar_config)

        hgmd_config = filter_config.hgmd
        if hgmd_config:
            hgmd_filtered_allele_ids = self._filter_hgmd(allele_ids, hgmd_config)

        # Union hgmd filtered and clinvar filtered if both have been run, otherwise return the result of the run one
        if clinvar_config and hgmd_config:
            result = clinvar_filtered_allele_ids & hgmd_filtered_allele_ids
        elif clinvar_config:
            result = clinvar_filtered_allele_ids
        elif hgmd_config:
            result = hgmd_filtered_allele_ids
        else:
            result = set()

        return result
