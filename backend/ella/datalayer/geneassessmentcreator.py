import datetime
import logging

import pytz
from sqlalchemy import func, select
from sqlalchemy.orm import Session

from ella.api import ApiError
from ella.api.schemas.pydantic.v1.gene_assessments import GeneAssessmentEvaluation
from ella.vardb.datamodel import assessment, sample
from ella.vardb.datamodel.assessment import GeneAssessment

log = logging.getLogger(__name__)


class GeneAssessmentCreator:
    def __init__(self, session: Session):
        self.session = session

    def create_from_data(
        self,
        user_id: int,
        usergroup_id: int,
        gene_id: int,
        genepanel_name: str,
        genepanel_version: str,
        evaluation: dict[str, dict[str, str]],
        reuseCheckedId: int | None = None,
        analysis_id: int | None = None,
    ) -> assessment.GeneAssessment:
        """
        Takes in geneassessment data and creates a new GeneAssessment object.

        The created object is not added to session/database.
        """

        if analysis_id:
            assert (
                self.session.execute(
                    select(func.count(sample.Analysis.id)).where(sample.Analysis.id == analysis_id)
                ).scalar_one()
                == 1
            )

        existing_assessment = self.session.execute(
            select(assessment.GeneAssessment).where(
                assessment.GeneAssessment.gene_id == gene_id,
                assessment.GeneAssessment.date_superceeded.is_(
                    None
                ),  # Only allowed to reuse valid assessment
            )
        ).scalar_one_or_none()

        # If existing, check that user saw the current previous version
        if existing_assessment and reuseCheckedId != existing_assessment.id:
            raise ApiError(
                f"'reuseCheckedId': {reuseCheckedId} does not match latest existing geneassessment id: {existing_assessment.id}"
            )

        assessment_obj = GeneAssessment(
            gene_id=gene_id,
            user_id=user_id,
            usergroup_id=usergroup_id,
            evaluation=GeneAssessmentEvaluation(**evaluation),
            genepanel_name=genepanel_name,
            genepanel_version=genepanel_version,
            analysis_id=analysis_id,
            previous_assessment_id=existing_assessment.id if existing_assessment else None,
        )

        # If there's an existing assessment for this gene, we want to supercede it
        if existing_assessment:
            existing_assessment.date_superceeded = datetime.datetime.now(pytz.utc)

        log.debug(
            "Created assessment for gene id: %s, it supercedes: %s",
            assessment_obj.gene_id,
            assessment_obj.previous_assessment_id,
        )

        return assessment_obj
