import datetime
import logging
from collections.abc import Sequence
from dataclasses import dataclass, field

import pytz
from sqlalchemy import select
from sqlalchemy.orm import Session

from ella.api import ApiError
from ella.api.schemas.pydantic.v1.allele_assessments import AlleleAssessmentEvaluation
from ella.api.schemas.pydantic.v1.references import ReferenceEvaluation
from ella.datalayer import filters
from ella.vardb.datamodel import assessment, attachment, sample

log = logging.getLogger(__name__)


@dataclass
class AssessmentCreatorResult:
    created_alleleassessment: assessment.AlleleAssessment | None
    reused_alleleassessment: assessment.AlleleAssessment | None
    created_referenceassessments: list[assessment.ReferenceAssessment] = field(default_factory=list)
    reused_referenceassessments: list[assessment.ReferenceAssessment] = field(default_factory=list)

    @property
    def alleleassessment(self) -> assessment.AlleleAssessment | None:
        return self.created_alleleassessment or self.reused_alleleassessment

    @property
    def referenceassessments(self) -> list[assessment.ReferenceAssessment]:
        return self.created_referenceassessments + self.reused_referenceassessments


class AssessmentCreator:
    def __init__(self, session: Session):
        self.session = session

    def create_from_data(
        self,
        user_id: int,
        usergroup_id: int,
        allele_id: int,
        annotation_id: int,
        custom_annotation_id: int | None,
        genepanel_name: str,
        genepanel_version: str,
        alleleassessment: dict,
        referenceassessments: Sequence[dict] | None,
        analysis_id: int | None = None,
    ) -> AssessmentCreatorResult:
        """
        Takes in alleleassessment/referenceassessment data and (if not reused),
        creates new assessment objects.

        The created objects are not added to session/database.
        """

        # Pydantic models converted to dict may have the key, but no value. Use the provided in that case
        for sub_dict in [alleleassessment, *(referenceassessments or [])]:
            if sub_dict.get("genepanel_name") is None:
                sub_dict["genepanel_name"] = genepanel_name
            if sub_dict.get("genepanel_version") is None:
                sub_dict["genepanel_version"] = genepanel_version

        ra_created, ra_reused = self._create_or_reuse_referenceassessments(
            allele_id,
            user_id,
            usergroup_id,
            genepanel_name,
            genepanel_version,
            referenceassessments,
            analysis_id=analysis_id,
        )

        aa, reused = self._create_or_reuse_alleleassessment(
            allele_id,
            user_id,
            usergroup_id,
            annotation_id,
            genepanel_name,
            genepanel_version,
            alleleassessment,
            custom_annotation_id=custom_annotation_id,
            referenceassessments=ra_created + ra_reused,
            analysis_id=analysis_id,
        )

        return AssessmentCreatorResult(
            aa if not reused else None, aa if reused else None, ra_created, ra_reused
        )

    def _create_or_reuse_alleleassessment(
        self,
        allele_id: int,
        user_id: int,
        usergroup_id: int,
        annotation_id: int,
        genepanel_name: str,
        genepanel_version: str,
        alleleassessment: dict,
        custom_annotation_id: int | None = None,
        referenceassessments: list[assessment.ReferenceAssessment] | None = None,
        analysis_id: int | None = None,
    ) -> tuple[assessment.AlleleAssessment, bool]:
        analysis = None
        if analysis_id:
            analysis = self.session.execute(
                select(sample.Analysis).where(sample.Analysis.id == analysis_id)
            ).scalar_one()

        existing_assessment = self.session.execute(
            select(assessment.AlleleAssessment).where(
                assessment.AlleleAssessment.allele_id == allele_id,
                assessment.AlleleAssessment.date_superceeded.is_(
                    None
                ),  # Only allowed to reuse valid assessment
            )
        ).scalar_one_or_none()

        result_alleleassessment = None
        result_reused = False

        # If existing, check that user saw the current previous version
        if existing_assessment:
            presented_id = alleleassessment["reuseCheckedId"]
            if presented_id != existing_assessment.id:
                raise ApiError(
                    f"'reuseCheckedId': {presented_id} does not match latest existing alleleassessment id: {existing_assessment.id}"
                )

        if alleleassessment.get("reuse"):
            assert existing_assessment
            result_reused = True
            log.debug("Reused assessment %s for allele %s", existing_assessment.id, allele_id)
            result_alleleassessment = existing_assessment

        else:  # create a new assessment
            result_reused = False
            assert "id" not in alleleassessment
            # attachment_ids are not part of internal alleleassessment schema
            attachment_ids = alleleassessment.pop("attachment_ids")
            attachment_objs = (
                self.session.execute(
                    select(attachment.Attachment).where(
                        filters.in_(attachment.Attachment.id, attachment_ids)
                    )
                )
                .scalars()
                .all()
            )

            assessment_obj = assessment.AlleleAssessment(
                allele_id=allele_id,
                user_id=user_id,
                usergroup_id=usergroup_id,
                annotation_id=annotation_id,
                genepanel_name=genepanel_name,
                genepanel_version=genepanel_version,
                custom_annotation_id=custom_annotation_id,
                evaluation=AlleleAssessmentEvaluation(**alleleassessment["evaluation"]),
                analysis_id=analysis_id if analysis else None,
                previous_assessment_id=existing_assessment.id if existing_assessment else None,
                referenceassessments=referenceassessments if referenceassessments else [],
                attachments=list(attachment_objs),
                classification=alleleassessment["classification"],
            )

            # If there's an existing assessment for this allele, we want to supercede it
            if existing_assessment:
                existing_assessment.date_superceeded = datetime.datetime.now(pytz.utc)

            log.debug(
                "Created assessment for allele: %s, it supercedes: %s",
                assessment_obj.allele_id,
                assessment_obj.previous_assessment_id,
            )

            result_alleleassessment = assessment_obj

        return result_alleleassessment, result_reused

    def _create_or_reuse_referenceassessments(
        self,
        allele_id: int,
        user_id: int,
        usergroup_id: int,
        genepanel_name: str,
        genepanel_version: str,
        referenceassessments: Sequence[dict] | None,
        analysis_id: int | None = None,
    ) -> tuple[list[assessment.ReferenceAssessment], list[assessment.ReferenceAssessment]]:
        if not referenceassessments:
            return list(), list()

        analysis: sample.Analysis | None = None
        if analysis_id:
            analysis = self.session.execute(
                select(sample.Analysis).where(sample.Analysis.id == analysis_id)
            ).scalar_one()

        reference_ids = [ra["reference_id"] for ra in referenceassessments]

        existing = (
            self.session.execute(
                select(assessment.ReferenceAssessment).where(
                    assessment.ReferenceAssessment.allele_id == allele_id,
                    filters.in_(assessment.ReferenceAssessment.reference_id, reference_ids),
                    assessment.ReferenceAssessment.date_superceeded.is_(
                        None
                    ),  # Only allowed to reuse valid assessment
                )
            )
            .scalars()
            .all()
        )

        reused: list[assessment.ReferenceAssessment] = list()
        created: list[assessment.ReferenceAssessment] = list()
        # When an 'id' is provided, we check and reuse that assessment instead of creating it
        for ra in referenceassessments:
            if ra.get("reuse"):
                to_reuse = next((e for e in existing if ra["reuseCheckedId"] == e.id), None)
                if not to_reuse:
                    raise ApiError(
                        f"Found no matching referenceassessment for allele_id: {ra['allele_id']}"
                        f", reference_id: {ra['reference_id']}, reuseCheckedId: {ra['reuseCheckedId']}. Either the"
                        " assessment is outdated or it doesn't exist."
                    )
                reused.append(to_reuse)
            else:
                # Check if there's an existing assessment for this allele/reference. If so, we want to supercede it
                to_supercede = next(
                    (e for e in existing if ra["reference_id"] == e.reference_id), None
                )

                assessment_obj = assessment.ReferenceAssessment(
                    allele_id=allele_id,
                    reference_id=ra["reference_id"],
                    user_id=user_id,
                    usergroup_id=usergroup_id,
                    genepanel_name=genepanel_name,
                    genepanel_version=genepanel_version,
                    evaluation=ReferenceEvaluation(**ra["evaluation"]),
                    analysis_id=analysis_id if analysis else None,
                )

                if to_supercede:
                    to_supercede.date_superceeded = datetime.datetime.now(pytz.utc)
                created.append(assessment_obj)

        return created, reused
