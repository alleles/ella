from sqlalchemy import Select, func, select
from sqlalchemy.orm import Session

from ella.api.util.types import AlleleCategories, AnalysisCategories
from ella.datalayer import filters, queries
from ella.vardb.datamodel import user, workflow


def get_categorized_alleles(
    user: user.User | None = None,
) -> dict[AlleleCategories, Select[tuple[int]]]:
    """
    Categorizes alleles according to workflow status and returns subqueries
    for their allele_ids per category.
    """
    categories = {
        AlleleCategories.NOT_STARTED: queries.workflow_alleles_interpretation_not_started(),
        AlleleCategories.MARKED_REVIEW: queries.workflow_alleles_review_not_started(),
        AlleleCategories.ONGOING: queries.workflow_alleles_ongoing(),
    }

    categorized_allele_ids = dict()
    for key, subquery in categories.items():
        sq_filters = [filters.in_(workflow.AlleleInterpretation.allele_id, subquery)]
        if user:
            sq_filters.append(
                filters.in_(
                    workflow.AlleleInterpretation.allele_id,
                    queries.workflow_alleles_for_genepanels(user.group.genepanels),
                )
            )
        categorized_allele_ids[key] = (
            select(workflow.AlleleInterpretation.allele_id).where(*sq_filters).distinct()
        )

    return categorized_allele_ids


def get_categorized_analyses(user: user.User | None = None):
    """
    Categorizes analyses according to workflow status and returns subqueries
    for their analysis_ids per category.
    """
    categories = {
        AnalysisCategories.NOT_READY: queries.workflow_analyses_notready_not_started(),
        AnalysisCategories.NOT_STARTED: queries.workflow_analyses_interpretation_not_started(),
        AnalysisCategories.MARKED_REVIEW: queries.workflow_analyses_review_not_started(),
        AnalysisCategories.MARKED_MEDICALREVIEW: queries.workflow_analyses_medicalreview_not_started(),
        AnalysisCategories.ONGOING: queries.workflow_analyses_ongoing(),
    }
    return categories


def get_finalized_analysis_ids(
    session: Session, user: user.User, page: int | None = None, per_page: int | None = None
):
    user_analysis_ids = queries.analysis_ids_for_user(user)

    finalized_analyses = (
        select(
            workflow.AnalysisInterpretation.analysis_id,
        )
        .where(
            filters.in_(
                workflow.AnalysisInterpretation.analysis_id,
                queries.workflow_analyses_finalized(),
            ),
            filters.in_(workflow.AnalysisInterpretation.analysis_id, user_analysis_ids),
        )
        .order_by(
            func.max(workflow.AnalysisInterpretation.date_last_update).desc(),
        )
        .group_by(workflow.AnalysisInterpretation.analysis_id)
    )

    count = session.execute(
        select(func.count()).select_from(finalized_analyses.subquery())
    ).scalar_one()

    if page and per_page:
        start = (page - 1) * per_page
        end = page * per_page
        finalized_analyses = finalized_analyses.slice(start, end)

    return (finalized_analyses, count)
