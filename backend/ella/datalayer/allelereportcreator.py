import datetime
import logging
from dataclasses import dataclass

import pytz
from sqlalchemy import select
from sqlalchemy.orm import Session

from ella.api import ApiError
from ella.api.schemas.pydantic.v1.allele_reports import AlleleReportEvaluation
from ella.vardb.datamodel import assessment

log = logging.getLogger(__name__)


@dataclass
class AlleleReportCreatorResult:
    created_allelereport: assessment.AlleleReport | None
    reused_allelereport: assessment.AlleleReport | None

    @property
    def allelereport(self) -> assessment.AlleleReport | None:
        return self.created_allelereport or self.reused_allelereport


class AlleleReportCreator:
    def __init__(self, session: Session):
        self.session = session

    def create_from_data(
        self,
        user_id: int,
        usergroup_id: int,
        allele_id: int,
        allelereport: dict,
        alleleassessment: assessment.AlleleAssessment | None = None,
        analysis_id: int | None = None,
    ) -> AlleleReportCreatorResult:
        """
        If alleleassessment is provided, the allelereport will be connected to
        the alleleassessment. If not, it will be stored with alleleassessment_id empty.
        """

        allelereport_obj, reused = self._create_or_reuse_allelereport(
            user_id,
            usergroup_id,
            allele_id,
            allelereport,
            alleleassessment=alleleassessment,
            analysis_id=analysis_id,
        )

        return AlleleReportCreatorResult(
            allelereport_obj if not reused else None, allelereport_obj if reused else None
        )

    def _create_or_reuse_allelereport(
        self,
        user_id: int,
        usergroup_id: int,
        allele_id: int,
        allelereport: dict,
        alleleassessment: assessment.AlleleAssessment | None = None,
        analysis_id: int | None = None,
    ) -> tuple[assessment.AlleleReport, bool]:
        existing_report = self.session.execute(
            select(assessment.AlleleReport).where(
                assessment.AlleleReport.allele_id == allele_id,
                assessment.AlleleReport.date_superceeded.is_(
                    None
                ),  # Only allowed to reuse valid allelereport
            )
        ).scalar_one_or_none()

        result_reused: bool = False

        # If existing, check that user saw the current previous version
        if existing_report:
            presented_id = allelereport["reuseCheckedId"]
            if presented_id != existing_report.id:
                raise ApiError(
                    f"'reuseCheckedId': {presented_id} does not match latest existing allelereport id: {existing_report.id}"
                )

        if allelereport.get("reuse"):
            assert existing_report
            result_reused = True
            result_allelereport = existing_report
            log.debug("Reused report %s for allele %s", existing_report.id, allele_id)

        else:  # create a new report
            result_reused = False
            assert "id" not in allelereport
            report_obj = assessment.AlleleReport(
                allele_id=allele_id,
                user_id=user_id,
                usergroup_id=usergroup_id,
                evaluation=AlleleReportEvaluation(**allelereport["evaluation"]),
                previous_report_id=existing_report.id if existing_report else None,
                alleleassessment_id=alleleassessment.id if alleleassessment else None,
                analysis_id=analysis_id,
            )

            # Check if there's an existing allelereport for this allele. If so, we want to supercede it
            if existing_report:
                existing_report.date_superceeded = datetime.datetime.now(pytz.utc)

            log.debug(
                "Created report for allele: %s, it supercedes: %s",
                report_obj.allele_id,
                report_obj.previous_report_id,
            )

            result_allelereport = report_obj

        return result_allelereport, result_reused
