"""
ELLA specific mypy plugin. Used to generate custom error messages for ELLA specific
code patterns.
"""
from typing import override

from mypy.errorcodes import ErrorCode
from mypy.nodes import CallExpr, NameExpr, StrExpr
from mypy.plugin import DynamicClassDefContext, FunctionContext, MethodContext, Plugin
from mypy.types import AnyType, CallableType, Instance, TupleType
from sqlalchemy import Boolean, Integer, String

ERROR_CODE = ErrorCode("ella-mypy", "ELLA generated mypy failure", "ELLA")


def do_not_use_method(method_name: str, alternative: str):
    "Wrapper for methods that should not be used, but has an alternative"

    def inner(att_ctx: MethodContext):
        att_ctx.api.fail(
            f"Do not use {method_name} - use {alternative} instead",
            att_ctx.context,
            code=ERROR_CODE,
        )
        return att_ctx.default_return_type

    return inner


def do_not_use_class(class_name: str, alternative: str):
    "Wrapper for classes that should not be used, but has an alternative"

    def inner(cls_ctx: DynamicClassDefContext):
        cls_ctx.api.fail(
            f"Do not use {class_name} - use {alternative} instead",
            cls_ctx.call,
            code=ERROR_CODE,
        )
        return

    return inner


def check_relationship_args(ctx: FunctionContext):
    """Check for the use of legacy/unnecessary arguments in relationship"""

    args = ["backref", "uselist"]
    alternatives = [
        "explicit relationship constructs with back_populates",
        "Mapped type annotations with collection types",
    ]
    arg_names = [an for sublist in ctx.arg_names for an in sublist]
    for arg, alternative in zip(args, alternatives):
        if arg in arg_names:
            ctx.api.fail(
                f"Do not use {arg} in relationship - use {alternative} instead",
                ctx.context,
                code=ERROR_CODE,
            )


def check_relationship_related_class(ctx: FunctionContext):
    """Check for explicit declaration of class that is to be related in relationship"""

    if ctx.arg_types[0] and isinstance(ctx.arg_types[0][0], CallableType):
        ctx.api.fail(
            "Do not use related class in relationship - use Mapped type annotations instead",
            ctx.context,
            code=ERROR_CODE,
        )


def check_relationship(ctx: FunctionContext):
    """Check proper relationship syntax and parameter usage"""

    check_relationship_args(ctx)
    check_relationship_related_class(ctx)
    return ctx.default_return_type


def check_mapped_column(ctx: FunctionContext):
    """Check if types without arguments are used in mapped_column"""
    if ctx.args[0]:
        expr = ctx.args[0][0]
        if isinstance(expr, StrExpr) and ctx.args[1]:
            expr = ctx.args[1][0]
        if isinstance(expr, CallExpr) and expr.args:
            return ctx.default_return_type
        if isinstance(expr, NameExpr) and repr(ctx.arg_types[0][0]) not in (
            repr(String) or repr(Boolean) or repr(Integer)
        ):
            return ctx.default_return_type
        ctx.api.fail(
            "Do not use types without arguments in mapped_column - use Mapped type annotations instead",
            ctx.context,
            code=ERROR_CODE,
        )
    return ctx.default_return_type


def check_scalar_used_on_multiple(ctx: MethodContext):
    "Check if Result.scalar is used on multiple columns"

    assert isinstance(ctx.type, Instance)
    assert len(ctx.type.args) == 1
    if type(ctx.type.args[0]) is AnyType:
        return ctx.default_return_type

    assert type(ctx.type.args[0]) is TupleType
    if len(ctx.type.args[0].items) > 1:
        ctx.api.fail(
            "Multiple columns selected on, but scalars() was used - this will only return the first column",
            ctx.context,
            code=ERROR_CODE,
        )
    return ctx.default_return_type


class ELLAMypyPlugin(Plugin):
    @override
    def get_function_hook(self, fullname: str):
        if fullname == "sqlalchemy.orm._orm_constructors.relationship":
            return check_relationship
        elif fullname == "sqlalchemy.orm._orm_constructors.mapped_column":
            return check_mapped_column

    @override
    def get_dynamic_class_hook(self, fullname: str):
        if fullname == "sqlalchemy.sql.schema.Column":
            return do_not_use_class("Column", "mapped_column")

    @override
    def get_method_hook(self, fullname: str):
        if fullname == "sqlalchemy.orm.session.Session.query":
            return do_not_use_method("Session.query", "Session.execute")
        elif fullname == "sqlalchemy.orm.session.Session.get":
            return do_not_use_method("Session.get", "Session.execute")
        elif fullname in {
            "sqlalchemy.engine.result.Result.scalars",
            "sqlalchemy.engine.result.Result.scalar_one",
            "sqlalchemy.engine.result.Result.scalar_one_or_none",
        }:
            return check_scalar_used_on_multiple
        elif fullname == "sqlalchemy.sql.selectable.Select.filter":
            return do_not_use_method("Select.filter", "Select.where")
        elif fullname == "sqlalchemy.sql.dml.Delete.filter":
            return do_not_use_method("Delete.filter", "Delete.where")
        elif fullname == "sqlalchemy.sql.dml.Update.filter":
            return do_not_use_method("Update.filter", "Update.where")
        elif fullname == "sqlalchemy.sql.elements.KeyedColumnElement.in_":
            return do_not_use_method("KeyedColumnElement.in_", "datalayer.filters.in_")
        elif fullname == "sqlalchemy.orm.attributes.InstrumentedAttribute.in_":
            return do_not_use_method("InstrumentedAttribute.in_", "datalayer.filters.in_")
        elif fullname == "sqlalchemy.sql.elements.Tuple.in_":
            return do_not_use_method("Tuple.in_", "datalayer.filters.in_")
        elif fullname == "sqlalchemy.sql.elements.BinaryExpression.in_":
            return do_not_use_method("BinaryExpression.in_", "datalayer.filters.in_")
        elif fullname == "sqlalchemy.sql.functions.Function.in_":
            return do_not_use_method("Function.in_", "datalayer.filters.in_")


def plugin(version: str):
    return ELLAMypyPlugin
