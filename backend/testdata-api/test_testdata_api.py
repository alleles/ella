import psycopg2
import pytest
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from reset_testdata import AVAILABLE_TESTSETS
from main import api, app, get_db_url


@pytest.fixture(scope="session")
def client():
    api.init_app(app)
    yield app.test_client()


def clean_db():
    service_db_url = get_db_url("postgres")
    service_db = psycopg2.connect(service_db_url)
    service_db.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cursor = service_db.cursor()
    cursor.execute(
        "SELECT datname FROM pg_database WHERE datname NOT IN ('postgres', 'template1', 'template0', 'vardb')"
    )
    for db in cursor.fetchall():
        cursor.execute(f'DROP DATABASE "{db[0]}"')
    service_db.close()


def get_db_contents(cursor):
    contents = {}
    for tbl in [
        "allele",
        "annotation",
        "annotationconfig",
        "genepanel",
        '"user"',
        "analysis",
        "sample",
        "genotype",
        "genotypesampledata",
    ]:
        cursor.execute(f"SELECT * FROM {tbl}")
        contents[tbl] = cursor.fetchall()
    return contents


def test_ping(client):
    resp = client.get("/healthcheck")
    assert resp.status_code == 200
    assert resp.json["message"] == "ok"


def test_reset(client):
    resp = client.get("/reset")
    assert resp.status_code == 200
    assert resp.json["message"] == "ok"
    assert resp.json["database_name"] != resp.json["template_database_name"]

    # Check that we have contents in the database
    connection = psycopg2.connect(resp.json["db_url"])
    assert connection.get_dsn_parameters()["dbname"] == resp.json["database_name"]
    cursor = connection.cursor()
    contents = get_db_contents(cursor)
    for tbl_content in contents.values():
        assert len(tbl_content) > 0

    # Check that the contents in the template database matches
    template_url = resp.json["db_url"].replace(
        resp.json["database_name"], resp.json["template_database_name"]
    )
    template_connection = psycopg2.connect(template_url)
    assert template_connection.get_dsn_parameters()["dbname"] == resp.json["template_database_name"]
    template_cursor = template_connection.cursor()
    template_contents = get_db_contents(template_cursor)
    assert template_contents == contents
    template_connection.close()

    # Make some modifications to database
    cursor.execute("TRUNCATE TABLE allele CASCADE")
    contents = get_db_contents(cursor)
    assert contents["allele"] == []
    assert contents != template_contents

    # Ensure that contents are updated from template
    client.get("/reset")
    connection = psycopg2.connect(resp.json["db_url"])
    cursor = connection.cursor()
    contents = get_db_contents(cursor)
    assert contents == template_contents


@pytest.mark.parametrize("testset", ["empty"] + AVAILABLE_TESTSETS)
def test_reset_with_testset(client, testset):
    clean_db()
    # Initial reset
    resp = client.get(f"/reset?testset={testset}")
    assert resp.status_code == 200
    assert resp.json["message"] == "ok"
    assert resp.json["made_template"] is True

    # On subsequent resets, it should use the template
    resp = client.get(f"/reset?testset={testset}")
    assert resp.status_code == 200
    assert resp.json["message"] == "ok"
    assert resp.json["made_template"] is False


def test_migration(client):
    resp = client.get("/reset?testset=empty&migration=true")
    assert resp.status_code == 200
    assert resp.json["message"] == "ok"

    connection = psycopg2.connect(resp.json["db_url"])
    cursor = connection.cursor()
    cursor.execute("SELECT count(*) FROM alembic_version")
    assert cursor.fetchone()[0] > 0


def test_clean_reset(client):
    # Ensure that we have a template database
    resp = client.get("/reset?testset=integration_testing")
    assert resp.status_code == 200
    assert resp.json["message"] == "ok"

    # Modify template database
    template_connection = psycopg2.connect(get_db_url(resp.json["template_database_name"]))
    template_cursor = template_connection.cursor()
    template_contents_original = get_db_contents(template_cursor)
    template_cursor.execute("TRUNCATE TABLE allele CASCADE")
    template_contents_modified = get_db_contents(template_cursor)
    assert len(template_contents_modified["allele"]) == 0
    assert len(template_contents_original["allele"]) > 0
    template_connection.close()

    # Check that clean=True resets the template database
    resp = client.get("/reset?testset=integration_testing&clean=true")
    assert resp.status_code == 200
    assert resp.json["message"] == "ok"

    template_connection = psycopg2.connect(get_db_url(resp.json["template_database_name"]))
    template_cursor = template_connection.cursor()
    template_contents = get_db_contents(template_cursor)
    assert len(template_contents_original["allele"]) == len(template_contents["allele"])
    template_connection.close()


def test_overwrite_false(client):
    resp = client.get("/reset?testset=integration_testing")
    assert resp.status_code == 200
    assert resp.json["message"] == "ok"

    # Modify database
    connection = psycopg2.connect(get_db_url(resp.json["database_name"]))
    cursor = connection.cursor()
    contents_original = get_db_contents(cursor)
    assert len(contents_original["allele"]) > 0
    cursor.execute("TRUNCATE TABLE allele CASCADE")
    contents_modified = get_db_contents(cursor)
    assert len(contents_modified["allele"]) == 0
    connection.commit()
    connection.close()

    # Check that database has not been overwritten
    resp = client.get("/reset?testset=integration_testing&overwrite=false")
    assert resp.status_code == 200
    connection = psycopg2.connect(get_db_url(resp.json["database_name"]))
    cursor = connection.cursor()
    contents = get_db_contents(cursor)
    assert contents == contents_modified


def test_clean(client):
    connection = psycopg2.connect(get_db_url("postgres"))
    cursor = connection.cursor()

    # Create a template database
    resp = client.get("/reset?testset=empty")
    assert resp.status_code == 200
    assert resp.json["message"] == "ok"
    cursor.execute("SELECT count(datname) FROM pg_database WHERE datname LIKE '%-template'")
    assert cursor.fetchone()[0] > 0

    # Trigger a clean
    resp = client.get("/clean")
    assert resp.status_code == 200
    assert resp.json["message"] == "ok"

    # Ensure that template database(s) are removed
    connection = psycopg2.connect(get_db_url("postgres"))
    cursor = connection.cursor()
    cursor.execute("SELECT count(datname) FROM pg_database WHERE datname LIKE '%-template'")
    assert cursor.fetchone()[0] == 0
