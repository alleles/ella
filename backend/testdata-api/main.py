#!/usr/bin/env python3

import datetime
import json
import logging
import os
import traceback
from typing import Any

from flask import Flask, make_response, request
from flask_restful import Api, Resource
from pydantic import BaseModel, ConfigDict, Field
from pydantic_core import to_jsonable_python
from reset_testdata import AVAILABLE_TESTSETS, DepositTestdata, link_analyses
from sqlalchemy import text

from ella.cli.commands.database.migration_db import (
    migration_upgrade,
)
from ella.cli.commands.database.refresh_db import refresh
from ella.vardb.datamodel import Base
from ella.vardb.datamodel.migration.migration_base import Base as MigrationBase
from ella.vardb.util.db import DB

logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s", level=logging.INFO)


class NoHealthCheck(logging.Filter):
    "Output /healtcheck 200 logs as DEBUG instead of INFO"

    def filter(self, record: logging.LogRecord) -> bool:
        if "GET /healthcheck" in record.getMessage() and "200" in record.getMessage():
            record.levelno = logging.DEBUG
        return not ("GET /healthcheck" in record.getMessage() and "200" in record.getMessage())


logging.getLogger("werkzeug").addFilter(NoHealthCheck())

###

app = Flask("testdata-server")
app.config["RESTFUL_JSON"] = {
    "default": to_jsonable_python,
    "separators": (",", ":"),
    "indent": None,
}
api = Api(app)


@app.errorhandler(Exception)
def handle_exception(e):
    """
    Return the full error with traceback, for debugging purposes

    This is usually bad practice, since it highlights internal details of the server,
    but we don't care about that here.
    """
    return make_response(traceback.format_exc(), 500)


###


class ApiResponse(BaseModel):
    message: str | BaseModel
    status: int = 200
    ts: datetime.datetime = Field(default_factory=datetime.datetime.now)
    error: dict[str, Any] | None = None

    model_config = ConfigDict(extra="allow")


###


@api.representation("application/json")
def output_json(data, code: int, headers: dict[str, Any] | None = None):
    data_obj = ApiResponse(**data, status=code)
    resp = make_response(data_obj.model_dump_json(exclude_none=True), code)
    resp.headers.extend(headers or {})
    return resp


###


def has_db(db: DB, db_name: str):
    with db.engine.connect() as conn:
        if conn.execute(text(f"SELECT 1 from pg_database WHERE datname='{db_name}'")).scalar():
            return True
    return False


def drop_db(db: DB, db_name: str):
    "Drop a database, if it exists. Use FORCE to drop even if there are active connections"
    with db.engine.connect().execution_options(isolation_level="AUTOCOMMIT") as conn:
        conn.execute(text(f'DROP DATABASE IF EXISTS "{db_name}" WITH (FORCE)'))


def create_db(db: DB, db_name: str, template: str = None):
    "Create a database, from the given template database if provided"
    sql = f'CREATE DATABASE "{db_name}"'
    if template:
        sql += f' TEMPLATE "{template}"'
    with db.engine.connect().execution_options(isolation_level="AUTOCOMMIT") as conn:
        conn.execute(text(sql))


def get_service_db():
    "Service DB is the database we connect to when creating/dropping databases"
    db = DB()
    db.connect(get_db_url("template1"))
    return db


def get_db_url(db_name: str):
    """
    Returns a postgresql database url for the given database name

    Hard code to avoid risk of accidentally resetting production database
    """
    return os.environ.get(
        "TESTDATA_DB_URL", f"postgresql://postgres:postgres@postgres:5432/{db_name}"
    )


class ResetParams(BaseModel):
    database: str = "vardb"
    testset: str = "default"
    migration: bool = False
    clean: bool = False
    overwrite: bool = True

    @property
    def template_database(self):
        return f"{self.testset}-template" + ("-migration" if self.migration else "")


def reset(params: ResetParams):
    print(f"Resetting triggered with {params}")
    service_db = get_service_db()

    resp: dict[str, bool | str] = {
        "cleaned": False,
        "made_template": False,
        "overwritten": False,
        "database_name": params.database,
        "template_database_name": params.template_database,
        "db_url": get_db_url(params.database),
    }
    link_analyses(params.testset)

    if has_db(service_db, params.database) and not params.overwrite:
        resp["message"] = f"Database {params.database} already exists, and overwrite is not enabled"
        return resp

    resp["overwritten"] = True
    drop_db(service_db, params.database)

    if params.clean:
        drop_db(service_db, params.template_database)
        resp["cleaned"] = True

    # Create template database if it doesn't exist,
    # and migrate it if migration is enabled
    # Then, populate template database with testdata
    if not has_db(service_db, params.template_database):
        create_db(service_db, params.template_database)
        db = DB()
        db.connect(get_db_url(params.template_database))
        if params.migration:
            MigrationBase.metadata.create_all(db.engine)
            migration_upgrade("head", db=db)
        else:
            Base.metadata.create_all(db.engine)
        refresh(db)
        if params.testset != "empty":
            DepositTestdata(db).deposit_all(test_set=params.testset)
        db.disconnect()
        resp["made_template"] = True

    # Create database from the template database - this is much faster than
    # creating a database from scratch
    create_db(service_db, params.database, template=params.template_database)
    resp["message"] = "ok"
    return resp


class ResetTestdata(Resource):
    def get(self):
        params = ResetParams(**request.args)
        resp = reset(params)
        return resp, 200


class Healthcheck(Resource):
    def get(self):
        return {"message": "ok"}, 200


class CleanTestdata(Resource):
    def get(self):
        service_db = get_service_db()
        # Drop all test database templates, so that any new testdata will be recreated
        for testset in AVAILABLE_TESTSETS + ["empty"]:
            drop_db(service_db, f"{testset}-template")
            drop_db(service_db, f"{testset}-template-migration")
        return {"message": "ok"}, 200


api.add_resource(ResetTestdata, "/reset")
api.add_resource(CleanTestdata, "/clean")
api.add_resource(Healthcheck, "/healthcheck")

###

if __name__ == "__main__":
    # Run an initial reset to ensure that we have data in the database upon startup
    if os.environ.get("INITIAL_DB_RESET_PARAMS"):
        reset(ResetParams(**json.loads(os.environ["INITIAL_DB_RESET_PARAMS"])))
    else:
        reset(ResetParams(overwrite=False))

    # Set debug=True while developing this API
    app.run(port=23232, host="0.0.0.0", debug=False, use_debugger=False)
