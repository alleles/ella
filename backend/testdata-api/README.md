# ELLA Testdata

The data used during development and testing is stored in <https://gitlab.com/alleles/ella-testdata.git>.
The scripts stored here are used for downloading that data, creating new datasets based on database
state and easily switching between datasets.

## Standard datasets

The main datasets are `default`, `e2e`, and `integration_testing`. As might be expected, `default` is the general purpose
dataset, `e2e` is used while running the end-to-end tests, and `integration_testing` is used for all backend (pytest) tests.

## Custom datasets

When writing a new feature or new tests, it may be useful to easily reset the database to a specific
state that is not either of the standard datasets. This can be done via the testdata API.

## API

To enable database reset from any container (or host), the testdata API is exposed on port 23232.

On initial reset, testdata is deposited using the code in `reset_testdata.py` into a (template database)[https://www.postgresql.org/docs/current/manage-ag-templatedbs.html#MANAGE-AG-TEMPLATEDBS]. On subsequent resets, the default behaviour is to copy this template database into the requested database.

Some example commands (assuming containers have been started with docker compose up):

```bash
# replace http://testdata with http://localhost if running from host machine

# reset to the default dataset
curl http://testdata:23232/reset
curl http://testdata:23232/reset?testset=default
curl http://testdata:23232/reset?testset=default&database=vardb&migration=false&clean=false

# remove testsets template database, before depositing from scratch
curl http://testdata:23232/reset?clean=true

# reset to the e2e
curl http://testdata:23232/database/reset?testset=e2e

# reset db only if it does not exist
curl http://testdata:23232/database/reset?overwrite=false

# reset to a different database
curl http://testdata:23232/reset?database=vardb-test

# run migrations before depositing testdata
curl http://testdata:23232/reset?migration=true

# remove all template databases
curl http://testdata:23232/clean
```
