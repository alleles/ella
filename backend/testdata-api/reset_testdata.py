#!/usr/bin/env python3
from __future__ import annotations

import datetime
import json
import logging
import os
import re
import sys
from dataclasses import dataclass
from enum import Enum
from pathlib import Path
from typing import TYPE_CHECKING, Any, cast

import yaml
from sqlalchemy.engine import Engine

from ella.api.config.config import feature_is_enabled
from ella.vardb.deposit.annotation_config import deposit_annotationconfig
from ella.vardb.deposit.deposit_alleles import DepositAlleles
from ella.vardb.deposit.deposit_analysis import DepositAnalysis
from ella.vardb.deposit.deposit_custom_annotations import import_custom_annotations
from ella.vardb.deposit.deposit_filterconfigs import deposit_filterconfigs
from ella.vardb.deposit.deposit_genepanel import DepositGenepanel
from ella.vardb.deposit.deposit_references import import_references
from ella.vardb.deposit.deposit_users import import_groups, import_users
from ella.vardb.watcher.analysis_watcher import AnalysisConfigData

if TYPE_CHECKING:
    from sqlalchemy.orm import scoped_session

    from ella.vardb.util.db import DB

logger = logging.getLogger(__file__)
logger.setLevel(logging.INFO)

ROOT = Path(__file__).absolute().parents[2]
TESTDATA_REPO_DIR = ROOT / "ella-testdata"
TESTDATA_SUBMODULE = ROOT / ".git" / "modules" / "ella-testdata"
TESTDATA_DIR = TESTDATA_REPO_DIR / "testdata"
GP_DIR = TESTDATA_DIR / "clinicalGenePanels"
FIXTURES_DIR = TESTDATA_DIR / "fixtures"
ANALYSES_DIR = TESTDATA_DIR / "analyses"

###
### Find and load metadata for all available and versioned testsets
###


class Fixtures(Enum):
    USERS = FIXTURES_DIR / "users.json"
    USERGROUPS = FIXTURES_DIR / "usergroups.json"
    FILTERCONFIGS = FIXTURES_DIR / "filterconfigs.json"
    ANNOTATIONCONFIG = FIXTURES_DIR / "annotation-config.yml"
    REFERENCES = FIXTURES_DIR / "references.json"
    CUSTOM_ANNO = FIXTURES_DIR / "custom_annotation_test.json"


@dataclass(frozen=True)
class AnalysisInfo:
    path: Path
    name: str
    is_default: bool = False


@dataclass(frozen=True)
class GenepanelInfo:
    transcripts: Path
    phenotypes: Path
    name: str
    version: str


@dataclass(frozen=True)
class AlleleInfo:
    path: str
    genepanel: tuple[str, str]


REPORT_EXAMPLE = TESTDATA_DIR / "example_report"
WARNINGS_EXAMPLE = TESTDATA_DIR / "example_warning"
GENEPANELS: list[GenepanelInfo] = []
for gp_item in GP_DIR.iterdir():
    if not gp_item.is_dir():
        continue
    GENEPANELS.append(
        GenepanelInfo(
            gp_item / f"{gp_item.name}_genes_transcripts.tsv",
            gp_item / f"{gp_item.name}_phenotypes.tsv",
            *gp_item.name.split("_"),
        )
    )

ANALYSES: list[AnalysisInfo] = []
DEFAULT_TESTSET = "default"
for an_item in ANALYSES_DIR.iterdir():
    if not an_item.is_dir():
        continue
    ANALYSES.append(AnalysisInfo(an_item, an_item.name, an_item.name == DEFAULT_TESTSET))
AVAILABLE_TESTSETS = [a.name for a in ANALYSES]

# TODO: determine a way to not have this hard coded
ALLELES = [
    AlleleInfo(
        "analyses/default/brca_sample_1.HBOC_v1.0.0/brca_sample_1.HBOC_v1.0.0.vcf.gz",
        ("HBOC", "v1.0.0"),
    ),
]


###
### Classes
###


def analysis_paths(testset_str: str):
    if testset_str is None:
        testset = next(v for v in ANALYSES if v.is_default)
    else:
        testset = next((v for v in ANALYSES if v.name == testset_str), None)
    if testset is None:
        return []

    testset_path = TESTDATA_DIR / testset.path
    analysis_paths = [d for d in testset_path.iterdir() if d.is_dir()]
    analysis_paths.sort()
    return analysis_paths


class DepositTestdata:
    session: scoped_session
    engine: Engine

    ANALYSIS_FILE_RE = re.compile(
        r"(?P<analysis_name>.+\.(?P<genepanel_name>.+)_(?P<genepanel_version>.+))\.vcf.gz"
    )

    def __init__(self, db: DB):
        assert db.session and db.engine
        self.engine = cast(Engine, db.engine)
        self.session = db.session

    def deposit_users(self):
        import_groups(self.session, json.loads(Fixtures.USERGROUPS.value.read_text()))
        import_users(self.session, json.loads(Fixtures.USERS.value.read_text()))

    def deposit_filter_configs(self):
        filter_configs = json.loads(Fixtures.FILTERCONFIGS.value.read_text())
        deposit_filterconfigs(self.session, filter_configs)
        logger.info(f"Added {len(filter_configs)} filter configs")

    def deposit_annotation_config(self):
        annotation_config: dict[str, Any] = yaml.safe_load(
            Fixtures.ANNOTATIONCONFIG.value.read_text()
        )
        deposit_annotationconfig(self.session, annotation_config)
        logger.info("Added annotation config")

    def deposit_analyses(self, test_set: str):
        """
        :param test_set: Which set to import.
        """

        for analysis_path in analysis_paths(test_set):
            analysis_files = [f for f in analysis_path.iterdir() if f.name.endswith(".analysis")]
            if len(analysis_files) > 1:
                if feature_is_enabled("cnv"):
                    analysis_file = next(
                        f for f in analysis_files if f.name.endswith("cnv.analysis")
                    )
                else:
                    analysis_file = next(
                        f for f in analysis_files if not f.name.endswith("cnv.analysis")
                    )
                config_path = analysis_file
            else:
                config_path = analysis_path

            try:
                analysis_config_data = AnalysisConfigData(config_path)
                analysis_config_data.warnings = (
                    WARNINGS_EXAMPLE.read_text()
                    if analysis_config_data.genepanel_name == "HBOC"
                    else None
                )
                analysis_config_data.report = REPORT_EXAMPLE.read_text()
                analysis_config_data.date_requested = datetime.datetime.now().strftime("%Y-%m-%d")

                da = DepositAnalysis(self.session)
                da.import_vcf(analysis_config_data)

                logger.info(f"Deposited {analysis_config_data.name} as analysis")
                self.session.commit()

            except UserWarning as e:
                logger.exception(str(e))
                sys.exit()

    def deposit_alleles(self):
        for allele in ALLELES:
            vcf_path = TESTDATA_DIR / allele.path

            da = DepositAlleles(self.session)
            da.import_vcf(vcf_path, allele.genepanel[0], allele.genepanel[1])
            logger.info(f"Deposited {vcf_path} as single alleles")
            self.session.commit()

    def deposit_genepanels(self):
        dg = DepositGenepanel(self.session)
        for gpdata in GENEPANELS:
            dg.add_genepanel(
                gpdata.transcripts,
                gpdata.phenotypes,
                gpdata.name,
                gpdata.version,
                replace=False,
            )

    def deposit_all(self, test_set: str):
        if test_set not in AVAILABLE_TESTSETS:
            raise RuntimeError(
                f"Test set {test_set} not part of available test sets: {','.join(AVAILABLE_TESTSETS)}"
            )

        logger.info("--------------------")
        logger.info("Starting a DB reset")
        logger.info(f"on {self.engine.url} with {test_set}")
        logger.info("--------------------")
        self.deposit_annotation_config()
        self.deposit_genepanels()
        self.deposit_users()
        self.deposit_filter_configs()
        import_references(self.session, Fixtures.REFERENCES.value)
        self.deposit_analyses(test_set=test_set)
        self.deposit_alleles()
        import_custom_annotations(self.session, Fixtures.CUSTOM_ANNO.value)

        logger.info("--------------------")
        logger.info(" DB Reset Complete!")
        logger.info("--------------------")


def link_analyses(test_set):
    # Link analyses to ANALYSES_PATH to get raw data and tracks
    analyses_path = Path(os.environ["ANALYSES_PATH"])
    analyses_path.mkdir(exist_ok=True, parents=True)
    for f in analyses_path.glob("*"):
        f.unlink()

    for analysis_path in analysis_paths(test_set):
        (analyses_path / analysis_path.name).symlink_to(
            analysis_path.relative_to(analyses_path, walk_up=True)
        )
