#!/bin/bash -ue

echo "Starting ELLA backend using DB_URL: ${DB_URL}"

if [[ "${PRODUCTION}" != "false" ]]; then
    # The following is (and needs to be) idempotent
    # Check if database exists, and create if not
    ella-cli database create -f

    # make-production will create tables if required, and run migrations
    ella-cli database make-production -f
    ella-cli database compare

    # Check (and refresh if necessary) the consistency between the application config and annotationshadowfrequency
    ella-cli database check-config-consistency || ella-cli database refresh -f

    gunicorn -c /ella/backend/gunicorn.conf.py ella.api.main:app
else
    # Prevent the container from exiting whenever the gunicorn process crashes
    # (e.g. due to syntax errors in the code during development)
    set +e
    while true; do
        gunicorn -c /ella/backend/gunicorn.conf.py ella.api.main:app
        sleep 3
    done
fi
