require('core-js/fn/object/entries')

let LoginPage = require('../pageobjects/loginPage')
let util = require('../pageobjects/util')
let Page = require('../pageobjects/page')

let loginPage = new LoginPage()
let page = new Page()

var failFast = require('jasmine-fail-fast')

// Utilities to mock an annotation service at localhost:6000 (mapped to gateway 172.17.0.1:6000 through exposed port)

describe('Documentation and 404 redirects', function () {
    it('can open docs page by clicking link', function () {
        loginPage.open()

        // Able to open docs from login page
        util.element("[href*='/docs']").click()
        browser.switchWindow('ELLA documentation')
        expect(browser.getUrl()).toContain('/docs')
        element = util.element('.site-name')
        expect(element.getText()).toBe('ELLA documentation')
    })

    it('can open docs by changing url', function () {
        loginPage.open()
        expect(browser.getUrl()).toContain('/login')

        // Able to open docs from url
        page.open('docs')
        expect(browser.getUrl()).toContain('/docs')
        element = util.element('.site-name')
        expect(element.getText()).toBe('ELLA documentation')
    })

    it('gets redirected to login page when trying to access 404', function () {
        // Gets redirected to login page when trying to access 404
        page.open('does/not/exist')
        browser.pause(500) // Will redirect through /overview, so needs a minor pause
        expect(browser.getUrl()).toContain('/login')
    })

    it('gets docs 404 page when trying to access 404 under /docs', function () {
        // Gets docs 404 page when trying to access 404 under /docs
        page.open('docs/does/not/exist')
        browser.pause(500)
        expect(browser.getUrl()).toContain('/docs/does/not/exist')
        expect($('p').getText()).toBe('The requested path could not be found')
    })
})
