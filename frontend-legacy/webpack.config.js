const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const DeadcodeWebpackPlugin = require('webpack-deadcode-plugin')

module.exports = (env, argv) => {
    const production = argv.mode === 'production'

    return {
        entry: {
            app: './src/js/index.js',
        },
        performance: { hints: false }, // Disable asset size warning
        output: {
            path: path.resolve(__dirname, 'build/'),
            filename: '[name].js',
        },
        devtool: production ? 'source-map' : 'inline-source-map', // inline-source-map is very large, but better
        // Serve build content on port 9000, with hot reloading on changes
        devServer: {
            contentBase: path.join(__dirname, 'build'),
            compress: true,
            host: '0.0.0.0',
            port: 9000,
            watchContentBase: true,
            hot: true,
            writeToDisk: true,
            historyApiFallback: true, // Redirect to `/` on 404
            disableHostCheck: true,
        },
        module: {
            rules: [
                {
                    test: /\.js?$/,
                    loader: 'babel-loader',
                    exclude: /node_modules/,
                    query: {
                        cacheDirectory: true,
                    },
                },
                {
                    test: /\.(sa|sc|c)ss$/,
                    use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
                },
                // Loads all files with ngtmpl.html into 'templates' module in angular
                {
                    test: /\.ngtmpl\.html$/,
                    loader: 'ng-cache-loader?prefix=&module=templates',
                },
                {
                    test: /\.(woff(2)?|ttf|eot|svg)$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: '[name].[ext]',
                                outputPath: 'fonts/',
                            },
                        },
                    ],
                },
            ],
        },
        plugins: [
            // Creates separate css file
            new MiniCssExtractPlugin({
                filename: '[name].css',
            }),
            // Creates index.html with automatic <script> tags
            new HtmlWebpackPlugin({ template: './src/index.html' }),
            new CleanWebpackPlugin([path.resolve(__dirname, 'build/')], {
                verbose: true,
                exclude: ['docs'],
            }),
            new DeadcodeWebpackPlugin({
                patterns: ['src/**/*.(js|css|html)'],
                exclude: ['**/*.spec.js'],
            }),
        ],
    }
}
