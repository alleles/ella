import { Module } from 'cerebral'
import logException from './signals/logException'
import updateBroadcastTriggered from './signals/updateBroadcastTriggered'

export default Module({
    state: {
        config: null,
        broadcast: {
            messages: null,
        },
        user: null,
    },
    signals: {
        updateBroadcastTriggered,
        logException,
    },
})
