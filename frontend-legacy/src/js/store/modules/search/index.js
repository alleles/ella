import { Module } from 'cerebral'

import modals from './modals'
import optionsSearchChanged from './signals/optionsSearchChanged'
import pageChanged from './signals/pageChanged'
import queryChanged from './signals/queryChanged'

export default Module({
    state: {
        query: {
            freetext: null,
            gene: null,
            user: null,
        },
        page: 1,
        per_page: 10,
        limit: 100,
        options: {
            genepanel: null,
            user: null,
        },
        results: null,
        loading: false,
    },
    signals: {
        optionsSearchChanged,
        queryChanged,
        pageChanged,
    },
    modules: {
        modals,
    },
})
