import { Module } from 'cerebral'
import copyAnalysesForAlleleClicked from './signals/copyAnalysesForAlleleClicked'
import dismissClicked from './signals/dismissClicked'
import showAnalysesForAlleleClicked from './signals/showAnalysesForAlleleClicked'
import warningAcceptedClicked from './signals/warningAcceptedClicked'

export default Module({
    modules: {},
    state: {},
    signals: {
        showAnalysesForAlleleClicked,
        warningAcceptedClicked,
        copyAnalysesForAlleleClicked,
        dismissClicked,
    },
})
