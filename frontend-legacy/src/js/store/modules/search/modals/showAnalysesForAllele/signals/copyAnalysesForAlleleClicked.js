import toast from '../../../../../common/factories/toast'
import copyAnalysesForAlleleClipboard from '../actions/copyAnalysesForAlleleClipboard'

export default [copyAnalysesForAlleleClipboard, toast('info', 'Copied analyses to clipboard')]
