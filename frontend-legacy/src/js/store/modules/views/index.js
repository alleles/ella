import { Module } from 'cerebral'

import { redirect } from '../../common/factories/route'
import DashboardModule from './dashboard'
import LoginModule from './login'
import OverviewModule from './overview'
import WorkflowsModule from './workflows'

export default Module({
    modules: {
        overview: OverviewModule,
        workflows: WorkflowsModule,
        login: LoginModule,
        dashboard: DashboardModule,
    },
    signals: {
        defaultRouted: redirect('overview/analyses/'),
    },
})
