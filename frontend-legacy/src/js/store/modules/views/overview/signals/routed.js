import { sequence } from 'cerebral'

import interval from '../../../../common/factories/interval'
import setNavbarTitle from '../../../../common/factories/setNavbarTitle'
import checkAndSelectValidSection from '../actions/checkAndSelectValidSection'
import loadOverviewState from '../actions/loadOverviewState'
import redirectToSection from '../actions/redirectToSection'
import saveOverviewState from '../actions/saveOverviewState'
import setSections from '../actions/setSections'
import loadOverview from '../sequences/loadOverview'

const UPDATE_OVERVIEW_INTERVAL = 180

export default sequence('routed', [
    setNavbarTitle(null),
    loadOverviewState,
    setSections,
    checkAndSelectValidSection,
    {
        valid: [
            // Unset by changeView
            interval(
                'start',
                'views.overview.updateOverviewTriggered',
                {},
                UPDATE_OVERVIEW_INTERVAL * 1000,
                false,
            ),
            saveOverviewState, // Store selected section
            loadOverview,
        ],
        invalid: [redirectToSection],
    },
])
