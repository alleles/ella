import loadDefaultGenepanel from './loadDefaultGenepanel'
import loadSelectedGenepanel from './loadSelectedGenepanel'

export default [loadDefaultGenepanel, loadSelectedGenepanel]
