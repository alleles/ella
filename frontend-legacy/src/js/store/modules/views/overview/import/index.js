import { Module } from 'cerebral'
import addAllTranscriptsClicked from './signals/addAllTranscriptsClicked'
import addTranscriptClicked from './signals/addTranscriptClicked'
import addedFilterChanged from './signals/addedFilterChanged'
import applyFilterBatchClicked from './signals/applyFilterBatchClicked'
import candidatesFilterBatchChanged from './signals/candidatesFilterBatchChanged'
import candidatesFilterChanged from './signals/candidatesFilterChanged'
import clearFilterBatchClicked from './signals/clearFilterBatchClicked'
import copyFilterBatchClicked from './signals/copyFilterBatchClicked'
import customGenepanelNameChanged from './signals/customGenepanelNameChanged'
import customGenepanelSelected from './signals/customGenepanelSelected'
import importClicked from './signals/importClicked'
import importHistoryPageChanged from './signals/importHistoryPageChanged'
import importSourceTypeSelected from './signals/importSourceTypeSelected'
import importUserGroupsChanged from './signals/importUserGroupsChanged'
import priorityChanged from './signals/priorityChanged'
import removeAllTranscriptsClicked from './signals/removeAllTranscriptsClicked'
import removeTranscriptClicked from './signals/removeTranscriptClicked'
import resetImportJobClicked from './signals/resetImportJobClicked'
import sampleSelected from './signals/sampleSelected'
import samplesSearchChanged from './signals/samplesSearchChanged'
import selectedAddedPageChanged from './signals/selectedAddedPageChanged'
import selectedCandidatesPageChanged from './signals/selectedCandidatesPageChanged'
import selectedFilterModeChanged from './signals/selectedFilterModeChanged'
import selectedGenepanelChanged from './signals/selectedGenepanelChanged'
import updateImportJobsTriggered from './signals/updateImportJobsTriggered'

export default Module({
    state: {}, // State set in changeView
    signals: {
        candidatesFilterChanged,
        candidatesFilterBatchChanged,
        addedFilterChanged,
        addTranscriptClicked,
        customGenepanelSelected,
        importSourceTypeSelected,
        removeTranscriptClicked,
        addAllTranscriptsClicked,
        removeAllTranscriptsClicked,
        importHistoryPageChanged,
        importClicked,
        customGenepanelNameChanged,
        resetImportJobClicked,
        sampleSelected,
        samplesSearchChanged,
        selectedCandidatesPageChanged,
        selectedAddedPageChanged,
        selectedGenepanelChanged,
        priorityChanged,
        selectedFilterModeChanged,
        applyFilterBatchClicked,
        updateImportJobsTriggered,
        clearFilterBatchClicked,
        copyFilterBatchClicked,
        importUserGroupsChanged,
    },
})
