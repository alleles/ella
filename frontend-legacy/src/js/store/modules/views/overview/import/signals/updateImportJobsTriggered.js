import progress from '../../../../../common/factories/progress'
import loadImportJobs from '../sequences/loadImportJobs'

export default [progress('start'), loadImportJobs, progress('done')]
