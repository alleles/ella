import { set } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'

export default [
    set(state`views.overview.import.custom.candidates.selectedPage`, props`selectedPage`),
]
