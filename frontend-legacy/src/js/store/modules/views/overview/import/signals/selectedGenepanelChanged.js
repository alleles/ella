import { set } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import loadSelectedGenepanel from '../sequences/loadSelectedGenepanel'

export default [
    set(state`views.overview.import.selectedGenepanel`, props`genepanel`),
    loadSelectedGenepanel,
]
