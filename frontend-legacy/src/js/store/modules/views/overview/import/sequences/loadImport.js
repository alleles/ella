import { sequence } from 'cerebral'
import { set } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import toast from '../../../../../common/factories/toast'
import getGenepanels from '../actions/getGenepanels'
import setDefaultSelectedGenepanel from '../actions/setDefaultSelectedGenepanel'
import loadImportJobs from './loadImportJobs'

export default sequence('loadImport', [
    getGenepanels,
    {
        success: [
            set(state`views.overview.import.data.genepanels`, props`result`),
            setDefaultSelectedGenepanel,
        ],
        error: [toast('error', 'Failed to load genepanels')],
    },
    loadImportJobs,
])
