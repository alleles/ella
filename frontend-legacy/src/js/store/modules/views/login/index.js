import { Module } from 'cerebral'

import { initApp } from '../../../common/factories'
import changeView from '../factories/changeView'
import reset from './actions/reset'
import changePasswordClicked from './signals/changePasswordClicked'
import confirmNewPasswordChanged from './signals/confirmNewPasswordChanged'
import loginClicked from './signals/loginClicked'
import modeChanged from './signals/modeChanged'
import newPasswordChanged from './signals/newPasswordChanged'
import passwordChanged from './signals/passwordChanged'
import routed from './signals/routed'
import usernameChanged from './signals/usernameChanged'

export default Module({
    state: {}, // State set in changeView
    signals: {
        routed: initApp([changeView('login'), routed]),
        modeChanged,
        usernameChanged,
        passwordChanged,
        newPasswordChanged,
        confirmNewPasswordChanged,
        changePasswordClicked,
        loginClicked,
        reset: [reset],
    },
})
