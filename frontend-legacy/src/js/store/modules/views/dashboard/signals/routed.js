import { parallel } from 'cerebral'
import progress from '../../../../common/factories/progress'
import loadUserStats from './loadUserStats'
import loadUsers from './loadUsers'

export default [progress('start'), parallel([loadUsers, loadUserStats]), progress('done')]
