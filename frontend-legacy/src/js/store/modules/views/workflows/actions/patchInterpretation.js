import { deepCopy } from '../../../../../util'
import getSelectedInterpretation from '../computed/getSelectedInterpretation'
const TYPES = {
    analysis: 'analyses',
    allele: 'alleles',
}

function patchInterpretation({ state, http, path, resolve }) {
    const type = TYPES[state.get('views.workflows.type')]
    const id = state.get('views.workflows.id')
    const interpretation = resolve.value(getSelectedInterpretation)

    if (interpretation.status !== 'Ongoing') {
        throw Error('Trying to save when interpretation status is not Ongoing')
    }

    const currentState = deepCopy(state.get('views.workflows.interpretation.state'))
    const currentUserState = deepCopy(state.get('views.workflows.interpretation.userState'))
    const selectedId = state.get('views.workflows.interpretation.selectedId')

    if (type === 'alleles') {
        delete currentState.manuallyAddedAlleles
        for (let [aId, allele] of Object.entries(currentState.allele)) {
            delete allele.report
            delete allele.analysis
        }
    }

    for (let [aId, allele] of Object.entries(currentState.allele)) {
        if (allele.alleleassessment && allele.alleleassessment.reuse) {
            delete allele.alleleassessment.evaluation
        }
    }

    const payload = {
        id: selectedId,
        state: currentState,
        user_state: currentUserState,
    }
    return http
        .patch(`workflows/${type}/${id}/interpretations/${interpretation.id}/`, payload)
        .then((response) => {
            return path.success({ result: response.result })
        })
        .catch((response) => {
            console.error(response)
            return path.error({ response: response.response })
        })
}

export default patchInterpretation
