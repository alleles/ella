import { when } from 'cerebral/operators'
import { props } from 'cerebral/tags'
import addAcmgCode from '../../interpretation/actions/addAcmgCode'
import removeAcmgCode from '../../interpretation/actions/removeAcmgCode'
import setDirty from '../../interpretation/actions/setDirty'
import canUpdateAlleleAssessment from '../../interpretation/operators/canUpdateAlleleAssessment'
import updateSuggestedClassification from '../../interpretation/sequences/updateSuggestedClassification'

export default [
    canUpdateAlleleAssessment,
    {
        true: [
            // If provided code has uuid, it must have been added
            // already. If so, remove it
            when(props`code.uuid`),
            {
                true: [removeAcmgCode],
                false: [addAcmgCode],
            },
            setDirty,
            updateSuggestedClassification,
        ],
        false: [],
    },
]
