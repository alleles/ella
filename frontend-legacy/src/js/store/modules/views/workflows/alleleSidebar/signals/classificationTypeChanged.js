import { set } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'

export default [
    set(state`views.workflows.alleleSidebar.classificationType`, props`classificationType`),
]
