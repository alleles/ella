import { Module } from 'cerebral'
import alleleRowClicked from './signals/alleleRowClicked'
import alleleRowToggled from './signals/alleleRowToggled'
import callerTypeSelectedChanged from './signals/callerTypeSelectedChanged'
import classificationTypeChanged from './signals/classificationTypeChanged'
import filterconfigChanged from './signals/filterconfigChanged'
import orderByChanged from './signals/orderByChanged'
import quickClassificationClicked from './signals/quickClassificationClicked'
import reviewedClicked from './signals/reviewedClicked'

export default Module({
    state: {},
    signals: {
        alleleRowClicked,
        alleleRowToggled,
        reviewedClicked,
        orderByChanged,
        classificationTypeChanged,
        callerTypeSelectedChanged,
        quickClassificationClicked,
        filterconfigChanged,
    },
})
