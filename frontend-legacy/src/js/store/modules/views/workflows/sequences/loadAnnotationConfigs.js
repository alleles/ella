import { sequence } from 'cerebral'
import { set } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import toast from '../../../../common/factories/toast'
import getAnnotationConfigs from '../actions/getAnnotationConfigs'

export default sequence('loadAnnotationConfigs', [
    getAnnotationConfigs(`views.workflows.interpretation.data.alleles`),
    {
        success: [set(state`views.workflows.data.annotationConfigs`, props`result`)],
        error: [toast('error', 'Failed to load annotation configs')],
    },
])
