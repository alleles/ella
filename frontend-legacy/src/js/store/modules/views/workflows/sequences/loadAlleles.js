import { parallel, sequence } from 'cerebral'
import { set, when } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import toast from '../../../../common/factories/toast'
import getAlleles from '../actions/getAlleles'
import prepareComponents from '../actions/prepareComponents'
import allelesChanged from '../alleleSidebar/sequences/allelesChanged'
import loadInterpretationLogs from '../worklog/sequences/loadInterpretationLogs'
import loadAnnotationConfigs from './loadAnnotationConfigs'
import loadCollisions from './loadCollisions'
import loadSimilarAlleles from './loadSimilarAlleles'
import prepareInterpretationState from './prepareInterpretationState'
import selectedAlleleChanged from './selectedAlleleChanged'

export default sequence('loadAlleles', [
    getAlleles,
    {
        success: [
            set(state`views.workflows.interpretation.data.alleles`, props`result`),
            parallel([
                // Annotation configs and interpretationlogs are required for prepareComponents
                [
                    parallel([loadAnnotationConfigs, loadInterpretationLogs]),
                    prepareComponents,
                    prepareInterpretationState,
                    allelesChanged,
                ],
                [loadCollisions],
                [loadSimilarAlleles],
            ]),
            when(state`views.workflows.selectedAllele`),
            {
                true: [
                    set(props`alleleId`, state`views.workflows.selectedAllele`),
                    selectedAlleleChanged,
                ],
                false: [],
            },
        ],
        error: [toast('error', 'Failed to load variant(s)', 30000)],
    },
])
