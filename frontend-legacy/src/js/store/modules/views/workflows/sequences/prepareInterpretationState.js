import { sequence } from 'cerebral'
import copyExistingAlleleReports from '../actions/copyExistingAlleleReports'
import prepareAlleleState from '../actions/prepareAlleleState'
import prepareInterpretationState from '../actions/prepareInterpretationState'
import autoReuseExistingAlleleassessments from '../interpretation/actions/autoReuseExistingAlleleassessments'
import autoReuseExistingReferenceAssessments from '../interpretation/actions/autoReuseExistingReferenceAssessments'
import checkAddRemoveAlleleToReport from '../interpretation/actions/checkAddRemoveAllelesToReport'
import issueToastNewAlleleAssessments from '../interpretation/actions/issueToastNewAlleleAssessments'

export default sequence('prepareInterpretationState', [
    prepareInterpretationState,
    prepareAlleleState,
    autoReuseExistingAlleleassessments,
    copyExistingAlleleReports,
    autoReuseExistingReferenceAssessments,
    checkAddRemoveAlleleToReport,
    issueToastNewAlleleAssessments,
])
