import { sequence } from 'cerebral'
import { set } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import toast from '../../../../../common/factories/toast'
import getSuggestedClassification from '../actions/getSuggestedClassification'
import setDirty from '../actions/setDirty'
import canUpdateAlleleAssessment from '../operators/canUpdateAlleleAssessment'

export default sequence('updateSuggestedClassification', [
    canUpdateAlleleAssessment,
    {
        true: [
            getSuggestedClassification,
            {
                success: [
                    set(
                        state`views.workflows.interpretation.state.allele.${props`alleleId`}.alleleassessment.evaluation.acmg.suggested_classification`,
                        props`result.class`,
                    ),
                ],
                aborted: [],
                error: [toast('error', 'Failed to load suggested classification')],
            },
        ],
        false: [], // noop
    },
])
