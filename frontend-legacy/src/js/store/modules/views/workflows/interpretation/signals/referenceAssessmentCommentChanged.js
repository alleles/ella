import { debounce } from 'cerebral/operators'
import setDirty from '../actions/setDirty'
import setReferenceAssessment from '../actions/setReferenceAssessment'
import canUpdateAlleleAssessment from '../operators/canUpdateAlleleAssessment'

export default [
    debounce(200),
    {
        continue: [
            canUpdateAlleleAssessment,
            {
                true: [setDirty, setReferenceAssessment],
                false: [],
            },
        ],
        discard: [],
    },
]
