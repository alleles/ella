import { set } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import toast from '../../../../../common/factories/toast'
import allelesChanged from '../../alleleSidebar/sequences/allelesChanged'
import checkAddRemoveAlleleToReport from '../actions/checkAddRemoveAllelesToReport'
import setDirty from '../actions/setDirty'
import canUpdateAlleleAssessment from '../operators/canUpdateAlleleAssessment'

export default [
    canUpdateAlleleAssessment,
    {
        true: [
            setDirty,
            set(
                state`views.workflows.interpretation.state.allele.${props`alleleId`}.alleleassessment.classification`,
                props`classification`,
            ),
            // Prepare props for checkAddRemoveAlleleToReport
            ({ props }) => {
                return {
                    checkReportAlleleIds: [props.alleleId],
                }
            },
            checkAddRemoveAlleleToReport,
            allelesChanged,
        ],
        false: [toast('error', 'Cannot change classification when interpretation is not Ongoing')],
    },
]
