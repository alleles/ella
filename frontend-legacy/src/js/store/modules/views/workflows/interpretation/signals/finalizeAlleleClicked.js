import { set, when } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import progress from '../../../../../common/factories/progress'
import toast from '../../../../../common/factories/toast'
import loadAlleles from '../../sequences/loadAlleles'
import saveInterpretation from '../../sequences/saveInterpretation'
import loadInterpretationLogs from '../../worklog/sequences/loadInterpretationLogs'
import postFinalizeAllele from '../actions/postFinalizeAllele'
import isReadOnly from '../operators/isReadOnly'

export default [
    isReadOnly,
    {
        false: [
            saveInterpretation([
                postFinalizeAllele,
                {
                    success: [
                        progress('start'),
                        loadAlleles,
                        set(
                            state`views.workflows.interpretation.state.allele.${props`alleleId`}.workflow.reviewed`,
                            true,
                        ),
                        progress('done'),
                        // Don't let progress shown to user
                        // depend on work log, but "load in background"
                        loadInterpretationLogs,
                    ],
                    error: [toast('error', 'Failed to create/update classification.')],
                },
            ]),
        ],
        true: [],
    },
]
