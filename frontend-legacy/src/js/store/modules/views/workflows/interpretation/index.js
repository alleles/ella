import { Module } from 'cerebral'
import acmgCodeChanged from './signals/acmgCodeChanged'
import addAcmgClicked from './signals/addAcmgClicked'
import alleleReportCommentChanged from './signals/alleleReportCommentChanged'
import analysisCommentChanged from './signals/analysisCommentChanged'
import classificationChanged from './signals/classificationChanged'
import collapseAllAlleleSectionboxClicked from './signals/collapseAllAlleleSectionboxChangedClicked'
import collapseAlleleSectionboxChanged from './signals/collapseAlleleSectionboxChanged'
import evaluationCommentChanged from './signals/evaluationCommentChanged'
import finalizeAlleleClicked from './signals/finalizeAlleleClicked'
import geneAssessmentChanged from './signals/geneAssessmentChanged'
import ignoreReferenceClicked from './signals/ignoreReferenceClicked'
import indicationsCommentChanged from './signals/indicationsCommentChanged'
import interpretationUserStateChanged from './signals/interpretationUserStateChanged'
import referenceAssessmentCommentChanged from './signals/referenceAssessmentCommentChanged'
import removeAcmgClicked from './signals/removeAcmgClicked'
import removeAttachmentClicked from './signals/removeAttachmentClicked'
import reportCommentChanged from './signals/reportCommentChanged'
import reuseAlleleAssessmentClicked from './signals/reuseAlleleAssessmentClicked'
import reuseAlleleReportClicked from './signals/reuseAlleleReportClicked'
import showExcludedReferencesClicked from './signals/showExcludedReferencesClicked'
import undoGeneAssessmentClicked from './signals/undoGeneAssessmentClicked'
import updateGeneAssessmentClicked from './signals/updateGeneAssessmentClicked'
import upgradeDowngradeAcmgClicked from './signals/upgradeDowngradeAcmgClicked'
import uploadAttachmentTriggered from './signals/uploadAttachmentTriggered'

export default Module({
    state: {},
    signals: {
        addAcmgClicked,
        geneAssessmentChanged,
        undoGeneAssessmentClicked,
        updateGeneAssessmentClicked,
        referenceAssessmentCommentChanged,
        collapseAllAlleleSectionboxClicked,
        collapseAlleleSectionboxChanged,
        acmgCodeChanged,
        finalizeAlleleClicked,
        classificationChanged,
        evaluationCommentChanged,
        analysisCommentChanged,
        ignoreReferenceClicked,
        alleleReportCommentChanged,
        reportCommentChanged,
        reuseAlleleReportClicked,
        removeAcmgClicked,
        removeAttachmentClicked,
        reuseAlleleAssessmentClicked,
        interpretationUserStateChanged,
        showExcludedReferencesClicked,
        upgradeDowngradeAcmgClicked,
        uploadAttachmentTriggered,
        indicationsCommentChanged,
    },
})
