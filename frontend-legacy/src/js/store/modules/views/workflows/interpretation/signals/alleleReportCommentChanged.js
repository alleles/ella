import { debounce, set, when } from 'cerebral/operators'
import { module, props } from 'cerebral/tags'
import toast from '../../../../../common/factories/toast'
import setDirty from '../actions/setDirty'
import canUpdateAlleleReport from '../operators/canUpdateAlleleReport'

export default [
    debounce(200),
    {
        continue: [
            canUpdateAlleleReport,
            {
                true: [
                    setDirty,
                    set(
                        module`state.allele.${props`alleleId`}.allelereport.evaluation.comment`,
                        props`comment`,
                    ),
                    when(
                        module`state.allele.${props`alleleId`}.allelereport.reuseCheckedId`,
                        (value) => Boolean(value),
                    ),
                    {
                        true: [
                            set(module`state.allele.${props`alleleId`}.allelereport.reuse`, false),
                        ],
                        false: [],
                    },
                ],
                false: [],
            },
        ],
        discard: [],
    },
]
