import { set, when } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import copyInterpretationState from '../actions/copyInterpretationState'
import loadInterpretationData from './loadInterpretationData'

export default [
    when(props`interpretationId`),
    {
        true: [
            set(state`views.workflows.interpretation.selectedId`, props`interpretationId`),
            copyInterpretationState,
            loadInterpretationData,
        ],
        false: [],
    },
]
