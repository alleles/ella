import { set } from 'cerebral/operators'
import { props, state, string } from 'cerebral/tags'
import progress from '../../../../common/factories/progress'
import setNavbarTitle from '../../../../common/factories/setNavbarTitle'
import loadAcmg from '../sequences/loadAcmg'
import loadAlleles from '../sequences/loadAlleles'
import loadGenepanel from '../sequences/loadGenepanel'

export default [
    progress('start'),
    set(state`views.workflows.selectedGenepanel`, props`genepanel`),
    loadGenepanel,
    loadAlleles,
    progress('inc'),
    loadAcmg,
    progress('inc'),
    // This signal is only relevant in allele workflow
    setNavbarTitle(
        string`${state`views.workflows.interpretation.data.alleles.${state`views.workflows.id`}.formatted.display`} (${state`views.workflows.selectedGenepanel.name`}_${state`views.workflows.selectedGenepanel.version`})`,
    ),
    progress('done'),
]
