import { set, when } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import toast from '../../../../../common/factories/toast'
import createInterpretationLog from '../actions/createInterpretationLog'
import postInterpretationLog from '../actions/postInterpretationLog'
import loadInterpretationLogs from '../sequences/loadInterpretationLogs'

export default [
    when(props`message`),
    {
        true: [
            createInterpretationLog,
            postInterpretationLog,
            {
                success: [set(state`views.workflows.worklog.message`, ''), loadInterpretationLogs],
                error: [toast('error', 'Failed to add message')],
            },
        ],
        false: [],
    },
]
