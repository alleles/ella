import { Module } from 'cerebral'
import addMessageClicked from './signals/addMessageClicked'
import clearWarningClicked from './signals/clearWarningClicked'
import deleteMessageClicked from './signals/deleteMessageClicked'
import editMessageClicked from './signals/editMessageClicked'
import messageChanged from './signals/messageChanged'
import priorityChanged from './signals/priorityChanged'
import showMessagesOnlyChanged from './signals/showMessagesOnlyChanged'
import updateReviewCommentClicked from './signals/updateReviewCommentClicked'

export default Module({
    state: {},
    signals: {
        messageChanged,
        addMessageClicked,
        editMessageClicked,
        deleteMessageClicked,
        clearWarningClicked,
        priorityChanged,
        updateReviewCommentClicked,
        showMessagesOnlyChanged,
    },
})
