import { sequence } from 'cerebral'
import { set } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import toast from '../../../../../common/factories/toast'
import getInterpretationLogs from '../actions/getInterpretationLogs'
import updateMessages from '../actions/updateMessages'

export default sequence('loadInterpretationLogs', [
    getInterpretationLogs,
    {
        success: [
            set(state`views.workflows.data.interpretationlogs`, props`result`),
            updateMessages,
        ],
        error: [toast('error', 'Failed to load work log')],
    },
])
