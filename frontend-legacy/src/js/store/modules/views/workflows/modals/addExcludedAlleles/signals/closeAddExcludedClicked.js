import { set } from 'cerebral/operators'
import { props, state } from 'cerebral/tags'
import loadAlleles from '../../../sequences/loadAlleles'
import loadGenepanel from '../../../sequences/loadGenepanel'
import loadReferences from '../../../sequences/loadReferences'
import loadVisualization from '../../../visualization/sequences/loadVisualization'
import setManuallyAddedAlleleIds from '../actions/setManuallyAddedAlleleIds'

export default [
    setManuallyAddedAlleleIds,
    set(state`views.workflows.modals.addExcludedAlleles`, { show: false }),
    loadGenepanel,
    loadAlleles,
    loadReferences,
    loadVisualization,
]
