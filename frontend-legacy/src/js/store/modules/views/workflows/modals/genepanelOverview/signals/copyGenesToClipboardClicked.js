import toast from '../../../../../../common/factories/toast'
import copyGenesToClipboard from '../actions/copyGenesToClipboard'

export default [copyGenesToClipboard, toast('info', 'Copied genes to clipboard')]
