import { Module } from 'cerebral'
import dismissClicked from './signals/dismissClicked'
import saveClicked from './signals/saveClicked'
import selectionChanged from './signals/selectionChanged'
import showAddExternalClicked from './signals/showAddExternalClicked'

export default Module({
    state: {
        show: false,
    },
    signals: {
        showAddExternalClicked,
        dismissClicked,
        selectionChanged,
        saveClicked,
    },
})
