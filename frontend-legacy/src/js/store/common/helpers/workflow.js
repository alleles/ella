import angular from 'angular'
import { deepCopy } from '../../../util'
import { findReferencesFromIds, getReferencesIdsForAllele } from './reference'

export function prepareInterpretationPayload(
    type,
    state,
    alleles,
    alleleIds,
    excludedAlleleIdsByCallerType,
) {
    // Collect info about this interpretation.
    const annotation_ids = []
    const custom_annotation_ids = []
    const alleleassessment_ids = []
    const allelereport_ids = []
    const reported_allele_ids = []
    const technical_allele_ids = []
    const notrelevant_allele_ids = []

    /**
     * In the front end, we have two interpretation modes: cnv and snv, and the
     * excluded alleles are operated on separately for each mode, input for
     * this loop is:
     * {
     *   excluded_alleles_by_caller_type:
     *   {
     *       snv: {
     *          region_filter: [1,2,3]
     *          // other filters
     *       },
     *       cnv: {
     *          region_filter: [23,34],
     *          // other filters
     *       }
     *
     *   }
     * }
     *
     * In the backend, there are no modes, thus we need to merge common filters of the excluded
     * alleles from the snv mode and cnv mode. Output from this loop is:
     * {
     *      excluded_alleles: {
     *          region_filter: [1,2,3,23,34]
     *      }
     * }
     */
    const excludedAlleleIds = {}
    if (excludedAlleleIdsByCallerType != null) {
        for (const excluded of Object.values(excludedAlleleIdsByCallerType)) {
            for (const [category, allele_ids] of Object.entries(excluded)) {
                if (!(category in excludedAlleleIds)) {
                    excludedAlleleIds[category] = []
                }
                excludedAlleleIds[category] = excludedAlleleIds[category].concat(allele_ids)
            }
        }
    }

    // Collect presented data for snapshotting
    // and data needed for verification in backend
    for (let alleleState of Object.values(state.allele)) {
        if (!alleleState.allele_id) {
            throw Error('Missing mandatory property allele_id in allele state', alleleState)
        }
        if (alleleState.allele_id in alleles) {
            let allele = alleles[alleleState.allele_id]
            annotation_ids.push(allele.annotation.annotation_id)
            if (allele.annotation.custom_annotation_id) {
                custom_annotation_ids.push(allele.annotation.custom_annotation_id)
            }
            if (allele.allele_assessment) {
                alleleassessment_ids.push(allele.allele_assessment.id)
            }
            if (allele.allele_report) {
                allelereport_ids.push(allele.allele_report.id)
            }
            if (type === 'analysis') {
                if (alleleState.report.included) {
                    reported_allele_ids.push(alleleState.allele_id)
                }
                if (alleleState.analysis.verification === 'technical') {
                    technical_allele_ids.push(alleleState.allele_id)
                }
                if (alleleState.analysis.notrelevant) {
                    notrelevant_allele_ids.push(alleleState.allele_id)
                }
            }
        }
    }

    const payload = {
        annotation_ids,
        custom_annotation_ids,
        alleleassessment_ids,
        allelereport_ids,
        allele_ids: alleleIds,
    }
    if (type === 'analysis') {
        Object.assign(payload, {
            reported_allele_ids,
            technical_allele_ids,
            notrelevant_allele_ids,
            excluded_allele_ids: excludedAlleleIds,
        })
    }
    return payload
}

export function prepareAlleleFinalizePayload(allele, alleleState, references) {
    if (
        !alleleState.alleleassessment ||
        !(alleleState.alleleassessment.classification || alleleState.alleleassessment.reuse)
    ) {
        throw Error('Cannot finalize allele, no valid alleleassessment')
    }

    const payload = {
        allele_id: allele.id,
        annotation_id: allele.annotation.annotation_id,
        custom_annotation_id: allele.annotation.custom_annotation_id || null,
    }

    payload.alleleassessment = _prepareAlleleAssessmentPayload(allele, alleleState)

    // Allele report is submitted independently of alleleassessment
    payload.allelereport = _prepareAlleleReportPayload(allele, alleleState)

    // Get reference ids for allele, we only submit referenceassessments
    // belonging to this allele
    const alleleReferences = _getAlleleReferences(allele, references)
    payload.referenceassessments = _prepareReferenceAssessmentsPayload(
        alleleState,
        alleleReferences,
    )

    return payload
}

function _getAlleleReferences(allele, references) {
    const alleleReferenceIds = getReferencesIdsForAllele(allele)
    return findReferencesFromIds(references, alleleReferenceIds).references
}

function _prepareAlleleAssessmentPayload(allele, allelestate) {
    const assessment_data = {}

    if (allele.allele_assessment) {
        assessment_data.reuse = allelestate.alleleassessment.reuse || false
        assessment_data.reuseCheckedId = allele.allele_assessment.id
    }

    if (!allelestate.alleleassessment.reuse) {
        Object.assign(assessment_data, {
            classification: allelestate.alleleassessment.classification,
            evaluation: allelestate.alleleassessment.evaluation,
            attachment_ids: allelestate.alleleassessment.attachment_ids,
        })
    }

    return assessment_data
}

function _prepareReferenceAssessmentsPayload(allelestate, references) {
    let referenceassessments_data = []
    if ('referenceassessments' in allelestate) {
        // Iterate over all referenceassessments for this allele
        for (let referenceState of allelestate.referenceassessments) {
            // If not present among references, skip it
            if (!references.find((r) => r.id === referenceState.reference_id)) {
                continue
            }

            // ra should match pydantic model NewReferenceAssessment or ReusedReferenceAssessment
            let ra = {
                reference_id: referenceState.reference_id,
                allele_id: referenceState.allele_id,
            }

            if (referenceState.reuse) {
                if (!referenceState.reuseCheckedId) {
                    throw Error('Missing mandatory property reuseCheckedId in reference state')
                }
                ra.reuse = true
                ra.reuseCheckedId = referenceState.reuseCheckedId
            } else {
                // Fill in fields expected by backend
                ra.evaluation = referenceState.evaluation
            }
            referenceassessments_data.push(ra)
        }
    }
    return referenceassessments_data
}

function _prepareAlleleReportPayload(allele, alleleState) {
    let report_data = {}

    if (allele.allele_report) {
        report_data.reuse = alleleState.allelereport.reuse || false
        report_data.reuseCheckedId = allele.allele_report.id
    }

    if (!report_data.reuse) {
        Object.assign(report_data, {
            evaluation: alleleState.allelereport.evaluation,
        })
    }
    return report_data
}
