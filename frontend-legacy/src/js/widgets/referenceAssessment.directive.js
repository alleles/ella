import { connect } from '@cerebral/angularjs'
import { props, signal, state } from 'cerebral/tags'
import app from '../ng-decorators'
import isReadOnly from '../store/modules/views/workflows/computed/isReadOnly'
import getReferenceAssessment from '../store/modules/views/workflows/interpretation/computed/getReferenceAssessment'
import isAlleleAssessmentReused from '../store/modules/views/workflows/interpretation/computed/isAlleleAssessmentReused'
import template from './referenceAssessment.ngtmpl.html' // eslint-disable-line no-unused-vars

app.component('referenceassessment', {
    bindings: {
        referenceId: '=',
    },
    templateUrl: 'referenceAssessment.ngtmpl.html',
    controller: connect(
        {
            referenceAssessment: getReferenceAssessment(
                state`views.workflows.selectedAllele`,
                props`referenceId`,
            ),
            isAlleleAssessmentReused: isAlleleAssessmentReused(
                state`views.workflows.selectedAllele`,
            ),
            readOnly: isReadOnly,
            selectedAllele: state`views.workflows.selectedAllele`,
            referenceAssessmentCommentChanged: signal`views.workflows.interpretation.referenceAssessmentCommentChanged`,
        },
        'ReferenceAssessment',
    ),
})
