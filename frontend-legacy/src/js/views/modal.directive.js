import { connect } from '@cerebral/angularjs'
import { props, signal, state } from 'cerebral/tags'
import app from '../ng-decorators'
import template from './modal.ngtmpl.html' // eslint-disable-line no-unused-vars

app.component('modal', {
    bindings: {
        showPath: '=',
        outsideClickPath: '=?', // If given, run signal when clicking outside
    },
    templateUrl: 'modal.ngtmpl.html',
    transclude: true,
    controller: connect(
        {
            show: state`${props`showPath`}`,
            outsideClick: signal`${props`outsideClickPath`}`,
        },
        'Modal',
        ['$scope', ($scope) => {}],
    ),
})
