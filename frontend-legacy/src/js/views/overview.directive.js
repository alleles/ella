import { connect } from '@cerebral/angularjs'
import { signal, state } from 'cerebral/tags'
import app from '../ng-decorators'

import template from './overview.ngtmpl.html' // eslint-disable-line no-unused-vars

app.component('overview', {
    templateUrl: 'overview.ngtmpl.html',
    controller: connect(
        {
            sectionKeys: state`views.overview.sectionKeys`,
            sections: state`views.overview.sections`,
            selectedSection: state`views.overview.state.selectedSection`,
            loading: state`views.overview.loading`,
        },
        'Overview',
        [
            '$scope',
            ($scope) => {
                const $ctrl = $scope.$ctrl
                Object.assign($ctrl, {
                    isSelected: (section) => section === $ctrl.selectedSection,
                })
            },
        ],
    ),
})
