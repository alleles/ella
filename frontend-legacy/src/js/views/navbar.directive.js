import { connect } from '@cerebral/angularjs'
import { signal, state } from 'cerebral/tags'
import app from '../ng-decorators'

import workflowInterpretationRoundsTemplate from '../widgets/allelebar/interpretationRoundsPopover.ngtmpl.html'
import template from './navbar.ngtmpl.html' // eslint-disable-line no-unused-vars

app.component('navbar', {
    transclude: true,
    templateUrl: 'navbar.ngtmpl.html',
    controller: connect(
        {
            config: state`app.config`,
            user: state`app.user`,
            broadcast: state`app.broadcast`,
            currentView: state`views.current`,
            title: state`app.navbar.title`,
        },
        'Navbar',
    ),
})
