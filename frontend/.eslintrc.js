module.exports = {
    extends: ['react-app', 'airbnb', 'airbnb-typescript', 'prettier'],
    root: true,
    plugins: ['prettier'],
    parserOptions: {
        project: './tsconfig.json',
        tsconfigRootDir: __dirname,
    },
    settings: {
        'import/resolver': {
            node: {
                paths: ['src'],
            },
        },
        react: {
            version: '18.2.0',
        },
    },
    rules: {
        semi: 'off',
        '@typescript-eslint/naming-convention': 'off',
        'react/jsx-no-bind': 'off',
        'react/function-component-definition': [
            2,
            {
                namedComponents: 'function-declaration',
            },
        ],
        'react/require-default-props': 'off',
        'react/jsx-props-no-spreading': 'off',
        'jsx-a11y/label-has-associated-control': [
            2,
            {
                assert: 'either',
            },
        ],
        'jsx-a11y/click-events-have-key-events': 'off',
        'no-console': [
            'warn',
            {
                allow: ['warn', 'error'],
            },
        ],
        'react/button-has-type': 'off',
        '@typescript-eslint/no-unused-vars': 'warn',
        'import/prefer-default-export': 'off',
        'import/no-extraneous-dependencies': [
            'error',
            {
                devDependencies: true,
            },
        ],
        'react-hooks/exhaustive-deps': 'off',
        'no-debugger': 'warn',
        'spaced-comment': 'warn',
        'react/no-unused-prop-types': 'warn',
        'jsx-a11y/no-static-element-interactions': 'off',
        '@typescript-eslint/no-use-before-define': 'off',
        'import/order': 'warn',
        'no-restricted-syntax': ['error', 'LabeledStatement', 'WithStatement'],
        'no-await-in-loop': 'off',
    },
}
