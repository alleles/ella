// @ts-nocheck

const plugin = require('tailwindcss/plugin')

module.exports = {
    content: ['./src/**/*.{js,jsx,ts,tsx}'],
    theme: {
        colors: {
            transparent: 'transparent',
            current: 'currentColor',
            editor: {
                red: '#ff0000',
                green: '#008000',
                blue: '#0000ff',
                yellow: '#ffff00',
                orange: '#ffa500',
            },
            ellablue: {
                lightest: '#eaeff2',
                lighter: '#cbd7df',
                DEFAULT: '#9caebf',
                darker: '#859baf',
                darkest: '#567490',
            },
            ellapurple: {
                lightest: '#f5f5f9',
                lighter: '#d6d8e6',
                DEFAULT: '#c3c6db',
                dark: '#9597bc',
            },
            ellagreen: {
                light: '#f6f9f8',
                DEFAULT: '#c9dbd9',
                dark: '#72979d',
            },
            ellared: {
                light: '#f8f5f5',
                DEFAULT: '#d8c8c5',
                dark: '#9c6a62',
            },
            ellayellow: {
                light: '#fcf8f2',
                DEFAULT: '#f1ddc0',
                dark: '#d59844',
            },
            ellagray: {
                50: '#f9fafb', //default gray-50
                100: '#f3f4f6', //default gray-100
                DEFAULT: '#d1d5db', //default gray-300
                400: '#9ca3af', //default gray-400
                500: '#6b7280', //default gray-500
                700: '#374151', //default gray-700
                900: '#111827', //default gray-900
            },
            black: {
                DEFAULT: '#000000',
            },
            white: {
                DEFAULT: '#ffffff',
            },
        },
        extend: {
            gridTemplateColumns: {
                '2-auto': 'auto auto',
            },
            width: {
                128: '32rem',
            },
            maxHeight: {
                '1/2': '50%',
                '3/4': '75%',
                '1/3': '33.333333%',
                128: '32rem',
                192: '48rem',
                256: '64rem',
                512: '128rem',
            },
            spacing: {
                128: '32rem',
                256: '64rem',
                512: '128rem',
            },
        },
    },
    plugins: [
        require('prettier-plugin-tailwindcss'),
        require('@tailwindcss/forms'),
        require('@tailwindcss/line-clamp'),
        plugin(function ({ addUtilities, addComponents, e, prefix, config }) {
            const newUtilities = {
                '.vertical-rl': {
                    writingMode: 'vertical-rl',
                },
            }
            addUtilities(newUtilities)
        }),
    ],
}
