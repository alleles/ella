# Scripts

A collection of useful scripts.

## locate-duplicates.ts

Will find all exported interfaces that exist in two places with the same name

    npx ts-node locate-duplicates.ts
