#!/bin/bash -e

log() {
    echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $*"
}

max_attempts=10
for attempt in $(seq 1 $max_attempts); do
    if docker ps &>/dev/null; then
        log "Docker is up"
        exit 0
    fi
    if [[ "$attempt" -lt "$max_attempts" ]]; then
        log "Docker is not up - sleeping"
        sleep $((2 ** attempt))
    fi
done

log "Docker still not up after ${attempt} attempts - giving up"
exit 1
