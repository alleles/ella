import * as fs from 'fs'

const glob = require('glob')

const getFiles = (src: string, callback: Parameters<typeof glob>[1]) => {
    glob(src + '/**/*.{ts,tsx}', callback)
}

const interfaces: Record<string, string[]> = {}

getFiles('../src', (err: Error, filenames: string[]) => {
    filenames.forEach((filename) => {
        const file = fs.readFileSync(filename).toString()
        const lines = file.split(/\r?\n/)

        lines.forEach((line) => {
            const matches = line.match(/export(?: default )?\s?interface ([a-zA-Z]*)/)
            if (matches) {
                const interfaceName = matches[1]

                if (interfaces[interfaceName]) {
                    interfaces[interfaceName].push(filename)
                } else {
                    interfaces[interfaceName] = [filename]
                }
            }
        })
    })

    Object.keys(interfaces).forEach((interfaceName) => {
        if (interfaces[interfaceName].length > 1) {
            console.log(interfaceName, interfaces[interfaceName])
        }
    })
})
