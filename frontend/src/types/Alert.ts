export interface AlertArgs {
    title?: string
    body: string
    timeout?: number
    type?: 'info' | 'warning' | 'error'
}
export interface Alert extends AlertArgs {
    id: number
    timeout: number
}
