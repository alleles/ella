import { CallerType } from 'types/api/Pydantic'
import * as ApiTypes from 'types/api/Pydantic'
import { AlleleInterpretation } from 'types/store/AlleleInterpretation'
import { Analysis } from 'types/store/Analysis'
import {
    AnalysisInterpretation,
    AnalysisInterpretationState,
} from 'types/store/AnalysisInterpretation'
import { InterpretationLogItem } from 'types/store/InterpretationLog'
import { Sample } from 'types/store/Sample'
import * as Genotype from 'types/view/Genotype'
import * as GenotypeSampleData from 'types/view/GenotypeSampleData'
import * as VariantCalling from 'types/view/VariantCalling'

import { InterpretationState } from 'views/interpretation/InterpretationStateContext'

export type PresentationAllele = {
    tags: string[]
    alleleId: number
    callerType: CallerType
    annotationId: number
    customAnnotationId: number | null
    assessmentId: number | null
    reportId: number | null
}

export interface AnalysisPresentationAllele extends PresentationAllele {
    genotypes: { [sampleId: number]: Genotype.Genotype | null }
    genotypeSampleData: {
        [sampleId: number]: GenotypeSampleData.GenotypeSampleData | null
    }
    variantCalling: VariantCalling.VariantCalling[]
    [k: string]: unknown
}

export type FilteredAlleles = {
    alleleIds: number[]
    excludedAllelesByCallerType: {
        [key in CallerType]: { [filterName: string]: number[] }
    }
}

export const createPresentationAllele = (
    allele: ApiTypes.Allele,
): Record<number, PresentationAllele> => {
    const assessment = allele.allele_assessment
        ? (allele.allele_assessment as ApiTypes.AlleleAssessment)
        : undefined
    const report = allele.allele_report
        ? (allele.allele_report as ApiTypes.AlleleReport)
        : undefined
    const presentationAllele = {
        [allele.id]: {
            alleleId: allele.id,
            callerType: allele.caller_type as CallerType,
            annotationId: assessment
                ? (assessment.annotation_id as number)
                : allele.annotation.annotation_id,
            customAnnotationId:
                (assessment
                    ? assessment.custom_annotation_id
                    : (allele.annotation?.custom_annotation_id as number | undefined)) ?? null,
            assessmentId: assessment ? assessment.id : null,
            reportId: report ? report.id : null,
            tags: allele.tags,
        },
    }
    return presentationAllele
}

export const createAnalysisPresentationAlleles = (
    alleles: ApiTypes.AlleleListResponse,
): Record<number, AnalysisPresentationAllele> =>
    alleles.reduce(
        (reducedAlleles, currentAllele) => ({
            ...reducedAlleles,
            [currentAllele.id]: {
                ...createPresentationAllele(currentAllele)[currentAllele.id],
                genotypes: (currentAllele.samples || []).reduce(
                    (reducedGenotypes, currentSample) => ({
                        ...reducedGenotypes,
                        [currentSample.id]: Genotype.mapFromApi(currentSample),
                    }),
                    {} as Record<number, Genotype.Genotype | null>,
                ),
                genotypeSampleData: (currentAllele.samples || []).map(
                    GenotypeSampleData.mapFromApi,
                ),
                variantCalling: (currentAllele.samples || []).map(VariantCalling.mapFromApi),
            },
        }),
        {} as Record<number, AnalysisPresentationAllele>,
    )

interface CommonInterpretationContext {
    id: number
    reloadAlleles: (alleleIds: number[]) => void
    start: () => void
    reopen: () => void
    reassignToMe: () => void
    deleteMessage: (id: number) => void
    updateMessage: (id: number, updatedMessage: string) => void
    updateInterpretationLog: (payload: any) => void
    interpretationLogItems: InterpretationLogItem[]
    ready: boolean
}

export interface AnalysisInterpretationContext extends CommonInterpretationContext {
    analysis: Analysis
    interpretation: Omit<AnalysisInterpretation, 'state'> // State is handled in interpretationStateContext
    samples: { [sampleId: number]: Sample }
    presentationAlleles: { [alleleId: number]: AnalysisPresentationAllele }
    filterConfigId: number
    filteredAlleles: FilteredAlleles
    finalizeWorkflow: (interpretationState: AnalysisInterpretationState) => void
    changeWorkflowStatus: (
        newWorkflowStatus: AnalysisInterpretation['workflowStatus'],
        interpretationState: InterpretationState,
    ) => void
}

export interface AlleleInterpretationContext extends CommonInterpretationContext {
    interpretation: Omit<AlleleInterpretation, 'state'> // State is handled in interpretationStateContext
    presentationAlleles: { [alleleId: number]: PresentationAllele }
    finalizeWorkflow: (interpretationState: InterpretationState) => void
    changeWorkflowStatus: (
        newWorkflowStatus: AlleleInterpretation['workflowStatus'],
        interpretationState: InterpretationState,
    ) => void
}
