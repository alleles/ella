import * as ApiTypes from 'types/api/Pydantic'

export interface Genotype {
    type: 'Homozygous' | 'Heterozygous' | 'Reference' | 'No coverage'
    multiallelic: boolean
    genotypeQuality: number | null
    sequencingDepth: number | null
    needsVerification: boolean | null
    filterStatus: string | null
    alleleRatio: number | null
    pDenovo: number | null
    variantQuality: number | null
    formatted: string | null
    id: number
    alleleDepth: {
        [k: string]: any
    } | null
    copyNumber: number | null
    mother?: Genotype | null
    father?: Genotype | null
    siblings: Genotype[]
}

export type SingleGenotypeSampleData = {
    type: 'Homozygous' | 'Heterozygous' | 'Reference' | 'No coverage'
    genotypeQuality: number | null
    sequencingDepth: number | null
    needsVerification: boolean | null
    alleleRatio: number | null
    pDenovo: number | null
    formatted: string | null
    id: number
    sampleId: number
    alleleDepth: {
        [k: string]: any
    } | null
    copyNumber: number | null
}

export type GenotypeSampleData = {
    father: SingleGenotypeSampleData | null
    mother: SingleGenotypeSampleData | null
    proband: SingleGenotypeSampleData | null
    affectedSiblings: SingleGenotypeSampleData[]
    unaffectedSiblings: SingleGenotypeSampleData[]
}

// function extractSampleSpecific(sampleSpecific: undefined): null
function extractSampleSpecific(
    sampleSpecific: ApiTypes.GenotypeSampleData,
    sampleId: number,
): SingleGenotypeSampleData {
    return {
        id: sampleSpecific.id,
        sampleId,
        type: sampleSpecific.type,
        // multiallelic: sampleSpecific.multiallelic,
        genotypeQuality: sampleSpecific.genotype_quality || null,
        sequencingDepth: sampleSpecific.sequencing_depth || null,
        needsVerification: sampleSpecific.needs_verification || null,
        // filterStatus: sampleSpecific.filter_status || null,
        alleleRatio: sampleSpecific.allele_ratio || null,
        pDenovo: sampleSpecific.p_denovo || null,
        formatted: sampleSpecific.formatted || null,
        // variantQuality: sampleSpecific.variant_quality || null,
        alleleDepth: sampleSpecific.allele_depth || null,
        copyNumber: sampleSpecific.copy_number || null,
    }
}

export function mapFromApi(sample: ApiTypes.Sample): GenotypeSampleData {
    if (!sample.genotype) {
        return {
            proband: null,
            mother: null,
            father: null,
            affectedSiblings: [],
            unaffectedSiblings: [],
        }
    }

    const { mother, father, siblings } = sample

    return {
        proband: extractSampleSpecific(sample.genotype, sample.id),
        mother: mother?.genotype ? extractSampleSpecific(mother?.genotype, mother.id) : null,
        father: father?.genotype ? extractSampleSpecific(father?.genotype, father.id) : null,
        unaffectedSiblings: (siblings || [])
            .filter((sibling) => !sibling.affected)
            .filter((sibling) => sibling.genotype)
            .map((sibling) =>
                extractSampleSpecific(sibling.genotype as ApiTypes.GenotypeSampleData, sibling.id),
            ),
        affectedSiblings: (siblings || [])
            .filter((sibling) => sibling.affected)
            .filter((sibling) => sibling.genotype)
            .map((sibling) =>
                extractSampleSpecific(sibling.genotype as ApiTypes.GenotypeSampleData, sibling.id),
            ),
    }
}
