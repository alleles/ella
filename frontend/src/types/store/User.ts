import * as ApiTypes from 'types/api/Pydantic'
import * as UserGroup from 'types/store/UserGroup'

export interface User {
    id: number
    username: string
    firstName: string
    lastName: string
    fullName: string
    abbrevName: string
    active: boolean
    userGroupName: string
    email: string
    passwordExpiry: string
    group: UserGroup.UserGroup | null
}

export function mapFromApi(userFromApi: ApiTypes.UserResponse): User {
    return {
        id: userFromApi.id,
        username: userFromApi.username,
        firstName: userFromApi.first_name,
        lastName: userFromApi.last_name,
        fullName: userFromApi.full_name,
        abbrevName: userFromApi.abbrev_name,
        active: userFromApi.active,
        userGroupName: userFromApi.user_group_name,
        email: userFromApi.email,
        passwordExpiry: userFromApi.password_expiry,
        group: userFromApi.group ? UserGroup.mapFromApi(userFromApi.group) : null,
    }
}

export function mapToApi(user: User): ApiTypes.UserResponse {
    return {
        abbrev_name: user.abbrevName,
        active: user.active,
        email: user.email,
        first_name: user.firstName,
        full_name: user.fullName,
        group: user.group ? UserGroup.mapToApi(user.group) : ({} as ApiTypes.UserGroup),
        id: user.id,
        last_name: user.lastName,
        password_expiry: user.passwordExpiry,
        user_group_name: user.userGroupName,
        username: user.username,
    }
}
