import * as ApiTypes from 'types/api/Pydantic'

export type GenePanel = {
    name: string
    version: string
    official: boolean | null
    genes: GeneAlternative[] | null
}

export type GeneAlternative = {
    hgncId: number
    hgncSymbol: string
    phenotypes: PhenotypeAlternative[]
    transcripts: TranscriptAlternative[]
}

type TranscriptAlternative = {
    id: number
    transcriptName: string
}

type PhenotypeAlternative = {
    id: number
    inheritance: string
    description: string
}

function mapFromApiGeneAlternative(
    geneAlternativeFromApi: ApiTypes.GeneAlternative,
): GeneAlternative {
    return {
        hgncId: geneAlternativeFromApi.hgnc_id,
        hgncSymbol: geneAlternativeFromApi.hgnc_symbol,
        phenotypes: geneAlternativeFromApi.phenotypes,
        transcripts: geneAlternativeFromApi.transcripts.map((tx) => ({
            id: tx.id,
            transcriptName: tx.transcript_name,
        })),
    }
}

export function mapFromApi(
    genePanelFromApi: ApiTypes.GenepanelBasic | ApiTypes.GenePanelResponse | ApiTypes.Genepanel,
): GenePanel {
    return {
        name: genePanelFromApi.name,
        version: genePanelFromApi.version,
        official: 'official' in genePanelFromApi ? genePanelFromApi.official : null,
        genes:
            'genes' in genePanelFromApi
                ? genePanelFromApi.genes.map(mapFromApiGeneAlternative)
                : null,
    }
}
