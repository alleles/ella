import * as ApiTypes from 'types/api/Pydantic'

export interface Sample {
    id: number
    sex: 'Female' | 'Male' | null
    identifier: string
    sampleType: 'HTS' | 'Sanger'
    affected: boolean
    proband: boolean
    familyId: string | null
    fatherId: number | null
    motherId: number | null
    siblingId: number | null
}

export function mapFromApi(apiSample: ApiTypes.Sample): Sample {
    return {
        id: apiSample.id,
        sex: apiSample.sex || null,
        identifier: apiSample.identifier,
        sampleType: apiSample.sample_type,
        affected: apiSample.affected,
        proband: apiSample.proband,
        familyId: apiSample.family_id || null,
        fatherId: apiSample.father_id || null,
        motherId: apiSample.mother_id || null,
        siblingId: apiSample.sibling_id || null,
    }
}
