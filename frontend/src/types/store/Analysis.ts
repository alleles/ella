import * as ApiTypes from 'types/api/Pydantic'

export interface Analysis {
    name: string
    dateRequested: string | null
    dateDeposited: string
    filterConfigIds: number[]
    interpretationIds: number[]
    genePanelName: string
    genePanelVersion: string
    sampleIds: number[]
    warnings: string | null
    report: string | null
}
export function mapFromApi(apiAnalysis: ApiTypes.Analysis1, filterConfigIds: number[]): Analysis {
    return {
        name: apiAnalysis.name,
        dateRequested: apiAnalysis.date_requested || null,
        dateDeposited: apiAnalysis.date_deposited,
        filterConfigIds,
        genePanelName: apiAnalysis.genepanel.name,
        genePanelVersion: apiAnalysis.genepanel.version,
        interpretationIds: (apiAnalysis.interpretations || []).map(
            (interpretation) => interpretation.id,
        ),
        sampleIds: apiAnalysis.samples.map((sample) => sample.id),
        warnings: apiAnalysis.warnings || null,
        report: apiAnalysis.report || null,
    }
}
