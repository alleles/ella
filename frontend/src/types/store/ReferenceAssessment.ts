import * as ApiTypes from 'types/api/Pydantic'

export type ReferenceAssessment = {
    referenceId: number
    dateCreated: string
    evaluation: {
        comment: string
        relevance: 'Yes' | 'No' | 'Ignore' | 'Indirectly' | null
        refQuality: string | null
        refAuthClassification: string | null
        refSegregation: string | null
        refSegregationQuality: string | null
        refIhc: string | null
        refMsi: string | null
        refProt: string | null
        refPrediction: string | null
        refProtQuality: string | null
        refPopulationAffecteds: string | null
        refRna: string | null
        refRnaQuality: string | null
        refAaOverlap: string | null
        refAaOverlapSameNovel: string | null
        refDomainOverlap: string | null
        refPopulationHealthy: string | null
        refPredictionTool: string | null
        refMsiQuality: string | null
        refAaOverlapAa: string | null
        refAaOverlapSim: string | null
        refAaOverlapAaRef: string | null
        refAaOverlapQuality: string | null
        refPhase: string | null
        refIhcQuality: string | null
        refDomainBenign: string | null
        refDeNovo: string | null
    }
}

export function mapFromApi(
    referenceAssessmentFromApi:
        | ApiTypes.NewReferenceAssessment
        | ApiTypes.ReferenceAssessment
        | ApiTypes.OptReferenceAssessment,
): ReferenceAssessment {
    return {
        referenceId: referenceAssessmentFromApi.reference_id,
        dateCreated:
            'date_created' in referenceAssessmentFromApi
                ? referenceAssessmentFromApi.date_created || 'N/A'
                : 'N/A',
        evaluation: {
            comment: referenceAssessmentFromApi.evaluation.comment || '',
            relevance: referenceAssessmentFromApi.evaluation.relevance || null,
            refQuality: referenceAssessmentFromApi.evaluation.ref_quality || null,
            refAuthClassification:
                referenceAssessmentFromApi.evaluation.ref_auth_classification || null,
            refSegregation: referenceAssessmentFromApi.evaluation.ref_segregation || null,
            refSegregationQuality:
                referenceAssessmentFromApi.evaluation.ref_segregation_quality || null,
            refIhc: referenceAssessmentFromApi.evaluation.ref_ihc || null,
            refMsi: referenceAssessmentFromApi.evaluation.ref_msi || null,
            refProt: referenceAssessmentFromApi.evaluation.ref_prot || null,
            refPrediction: referenceAssessmentFromApi.evaluation.ref_prediction || null,
            refProtQuality: referenceAssessmentFromApi.evaluation.ref_prot_quality || null,
            refPopulationAffecteds:
                referenceAssessmentFromApi.evaluation.ref_population_affecteds || null,
            refRna: referenceAssessmentFromApi.evaluation.ref_rna || null,
            refRnaQuality: referenceAssessmentFromApi.evaluation.ref_rna_quality || null,
            refAaOverlap: referenceAssessmentFromApi.evaluation.ref_aa_overlap || null,
            refAaOverlapSameNovel:
                referenceAssessmentFromApi.evaluation.ref_aa_overlap_same_novel || null,
            refDomainOverlap: referenceAssessmentFromApi.evaluation.ref_domain_overlap || null,
            refPopulationHealthy:
                referenceAssessmentFromApi.evaluation.ref_population_healthy || null,
            refPredictionTool: referenceAssessmentFromApi.evaluation.ref_prediction_tool || null,
            refMsiQuality: referenceAssessmentFromApi.evaluation.ref_msi_quality || null,
            refAaOverlapAa: referenceAssessmentFromApi.evaluation.ref_aa_overlap_aa || null,
            refAaOverlapSim: referenceAssessmentFromApi.evaluation.ref_aa_overlap_sim || null,
            refAaOverlapAaRef: referenceAssessmentFromApi.evaluation.ref_aa_overlap_aa_ref || null,
            refAaOverlapQuality:
                referenceAssessmentFromApi.evaluation.ref_aa_overlap_quality || null,
            refPhase: referenceAssessmentFromApi.evaluation.ref_phase || null,
            refIhcQuality: referenceAssessmentFromApi.evaluation.ref_ihc_quality || null,
            refDomainBenign: referenceAssessmentFromApi.evaluation.ref_domain_benign || null,
            refDeNovo: referenceAssessmentFromApi.evaluation.ref_de_novo || null,
        },
    }
}
