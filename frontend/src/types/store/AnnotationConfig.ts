import * as ApiTypes from 'types/api/Pydantic'

export interface AnnotationConfig {
    view: View[]
}

export type SectionType = 'external' | 'frequency' | 'prediction'
export type TemplateType = 'keyValue' | 'itemList' | 'frequencyDetails' | 'clinvarDetails'
interface View {
    section: SectionType
    template: TemplateType
    source: string
    title: string | null
    url: string | null
    urlEmpty: string | null
    config: Record<string, any>
    [k: string]: unknown
}

export type TemplateConfig =
    | FrequencyConfig
    | ItemListConfig
    | KeyValueConfig
    | ClinvarDetailsConfig

export interface FrequencyConfig {
    key_column: string
    columns: Record<string, string>
    rows: Record<string, string>
    filter?: string
    precision?: number
    scientified_treshold?: number
    indications?: {
        keys: string[]
        threshold?: number
    }
}

export interface ViewItem {
    type?: 'primitives' | 'objects'
    subsource?: string
    url?: string
}

export interface ObjectItem extends ViewItem {
    key: string
    type: 'objects'
}
export interface ItemListConfig {
    items: ViewItem[]
}

export interface KeyValueConfig {
    names: {
        [k: string]: string
    }
}

export interface ClinvarDetailsConfig {
    [k: string]: any
}

export interface AnnotationSchema<T extends TemplateConfig> {
    source: string
    title: string | null
    url: string | null
    urlEmpty: string | null
    config: T
    template: TemplateType
}

export interface KeyValueSchema extends AnnotationSchema<KeyValueConfig> {
    template: 'keyValue'
}

export interface ItemListSchema extends AnnotationSchema<ItemListConfig> {
    template: 'itemList'
}
export interface FrequencySchema extends AnnotationSchema<FrequencyConfig> {
    template: 'frequencyDetails'
}
export interface ClinvarDetailsSchema extends AnnotationSchema<ClinvarDetailsConfig> {
    template: 'clinvarDetails'
}

function schemaFactory(v: View, config: TemplateConfig): AnnotationSchema<TemplateConfig> {
    return {
        source: v.source,
        title: v.title,
        url: v.url,
        urlEmpty: v.urlEmpty, // v.url_empty || null,
        config,
        template: v.template,
    }
}

function createAnnotationSchema(view: View): AnnotationSchema<TemplateConfig> {
    switch (view.template) {
        case 'frequencyDetails':
            return schemaFactory(view, view.config as FrequencyConfig)
        case 'clinvarDetails':
            return schemaFactory(view, view.config as ClinvarDetailsConfig)
        case 'itemList':
            return schemaFactory(view, view.config as ItemListConfig)
        case 'keyValue':
            return schemaFactory(view, view.config as KeyValueConfig)
        default:
            throw Error(`template: ${view.template} is not supported`)
    }
}
export function getAnnotationSchemasBySection(
    section: SectionType,
    annotationConfig: AnnotationConfig,
): Array<AnnotationSchema<TemplateConfig>> {
    return annotationConfig.view
        .filter((v) => v.section === section)
        .map((v) => createAnnotationSchema(v))
}

export function mapFromApi(annotationConfigFromApi: ApiTypes.AnnotationConfig): AnnotationConfig {
    return {
        view: annotationConfigFromApi.view.map((v) => ({
            section: v.section,
            template: v.template,
            source: v.source,
            title: v.title || null,
            url: v.url || null,
            urlEmpty: v.url_empty || null,
            config: v.config,
        })),
    }
}
