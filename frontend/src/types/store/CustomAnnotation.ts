import * as ApiTypes from 'types/api/Pydantic'

export interface CustomAnnotation {
    alleleId: number
    userId: number
    dateSuperseded?: string
    dateCreated: string

    annotations: {
        references?: { id: number; pubMedId?: number; source: string }[]
        prediction?: {
            dnaConservation: string | null
            domain: string | null
            orthologConservation: string | null
            paralogConservation: string | null
            repeat: string | null
            spliceEffectManual: string | null
        }
        external?: Record<string, string>
        [k: string]: unknown
    }
    [k: string]: unknown
}

export interface UpdateCustomAnnotationPayload {
    alleleId: number
    userId: number
    annotations: Partial<CustomAnnotation['annotations']>
}

export function mapFromApi(customAnnotationsFromApi: ApiTypes.CustomAnnotation): CustomAnnotation {
    return {
        alleleId: customAnnotationsFromApi.allele_id,
        annotations: {
            references: (customAnnotationsFromApi.annotations.references ?? []).map(
                (referenceFromApi) => {
                    if (referenceFromApi.id === undefined) {
                        throw new Error('Reference has no ID')
                    }
                    return {
                        id: referenceFromApi.id,
                        source: referenceFromApi.source,
                    }
                },
            ),
            prediction: {
                dnaConservation:
                    customAnnotationsFromApi.annotations.prediction?.dna_conservation || null,
                domain: customAnnotationsFromApi.annotations.prediction?.domain || null,
                orthologConservation:
                    customAnnotationsFromApi.annotations.prediction?.ortholog_conservation || null,
                paralogConservation:
                    customAnnotationsFromApi.annotations.prediction?.paralog_conservation || null,
                repeat: customAnnotationsFromApi.annotations.prediction?.repeat || null,
                spliceEffectManual:
                    customAnnotationsFromApi.annotations.prediction?.splice_Effect_manual || null,
            },
            external: customAnnotationsFromApi.annotations.external,
        },

        dateCreated: customAnnotationsFromApi.date_created,
        dateSuperseded: customAnnotationsFromApi.date_superceeded,
        userId: customAnnotationsFromApi.user_id,
    }
}

export function mapToApiAnnotations(
    annotations: CustomAnnotation['annotations'],
): Partial<ApiTypes.CustomAnnotation['annotations']> {
    console.warn(
        'This is lacking proper handlind of prediction and external, with unkown consequences',
    )

    return {
        references: (annotations.references ?? []).map((reference) => ({
            id: reference.id,
            source: 'User',
        })),
        prediction: {
            dna_conservation: annotations.prediction?.dnaConservation ?? '',
            domain: annotations.prediction?.domain ?? '',
            ortholog_conservation: annotations.prediction?.orthologConservation ?? '',
            paralog_conservation: annotations.prediction?.paralogConservation ?? '',
            repeat: annotations.prediction?.repeat ?? '',
            splice_Effect_manual: annotations.prediction?.spliceEffectManual ?? '',
        },
        external: annotations.external ?? {},
    }
}
