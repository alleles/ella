import * as ApiTypes from 'types/api/Pydantic'

export interface Reference {
    id: number
    authors: string | null
    title: string | null
    journal: string | null
    abstract: string | null
    pubmedId: number | null
    published: boolean
    year: number | null
}

export function mapFromApi(apiReference: ApiTypes.Reference): Reference {
    return {
        id: apiReference.id,
        authors: apiReference.authors || null,
        title: apiReference.title || null,
        journal: apiReference.journal || null,
        abstract: apiReference.abstract || null,
        pubmedId: apiReference.pubmed_id || null,
        published: apiReference.published,
        year: apiReference.year || null,
    }
}
