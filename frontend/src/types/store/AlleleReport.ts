import * as ApiTypes from 'types/api/Pydantic'

export interface AlleleReport {
    alleleId: number
    id: number
    evaluation: {
        comment: string
    }
    userId: number
    dateCreated: string
    dateSuperseded: string | null
    previousReportId: number | null
    analysisId: number | null
}

export function mapFromApi(alleleReportFromApi: ApiTypes.AlleleReport): AlleleReport {
    return {
        alleleId: alleleReportFromApi.allele_id,
        id: alleleReportFromApi.id,
        evaluation: {
            comment: alleleReportFromApi.evaluation?.comment || '',
        },
        userId: alleleReportFromApi.user_id,
        dateCreated: alleleReportFromApi.date_created,
        dateSuperseded: alleleReportFromApi.date_superceeded || null,
        previousReportId: alleleReportFromApi.previous_report_id || null,
        analysisId: alleleReportFromApi.analysis_id || null,
    }
}
