import * as ApiTypes from 'types/api/Pydantic'

type GenePanel = {
    name: string
    version: string
    official: boolean
}

export interface UserGroup {
    id: number
    name: string
    genePanels: GenePanel[]
    defaultImportGenePanel: GenePanel
    importGroups: string[]
}

export function mapFromApi(userGroupFromApi: ApiTypes.UserGroup): UserGroup {
    return {
        id: userGroupFromApi.id,
        name: userGroupFromApi.name,
        genePanels: userGroupFromApi.genepanels,
        defaultImportGenePanel: userGroupFromApi.default_import_genepanel,
        importGroups: userGroupFromApi.import_groups,
    }
}

export function mapToApi(userGroup: UserGroup): ApiTypes.UserGroup {
    return {
        default_import_genepanel: userGroup.defaultImportGenePanel,
        genepanels: userGroup.genePanels,
        id: userGroup.id,
        import_groups: userGroup.importGroups,
        name: userGroup.name,
    }
}
