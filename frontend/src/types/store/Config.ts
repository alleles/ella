// The config is kept as-is for the time being
import { ConfigResponse as ApiConfig } from 'types/api/Pydantic'

export type { ApiConfig as Config }
