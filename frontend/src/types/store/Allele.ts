import * as ApiTypes from 'types/api/Pydantic'

export interface Allele {
    callerType: string
    changeFrom: string
    changeTo: string
    changeType: string
    chromosome: string
    genomeReference: ApiTypes.GenomeReference
    length: number
    openEndPosition: number
    startPosition: number
    vcfAlt: string
    vcfPos: number
    vcfRef: string
    [k: string]: unknown
}

export function mapFromApi(alleleFromApi: ApiTypes.Allele): Allele {
    return {
        callerType: alleleFromApi.caller_type,
        changeFrom: alleleFromApi.change_from,
        changeTo: alleleFromApi.change_to,
        changeType: alleleFromApi.change_type,
        chromosome: alleleFromApi.chromosome,
        genomeReference: alleleFromApi.genome_reference,
        length: alleleFromApi.length,
        openEndPosition: alleleFromApi.open_end_position,
        startPosition: alleleFromApi.start_position,
        vcfAlt: alleleFromApi.vcf_alt,
        vcfPos: alleleFromApi.vcf_pos,
        vcfRef: alleleFromApi.vcf_ref,
    }
}

export function mapToPartialApi(
    allele: Allele,
    alleleId: number,
): Omit<ApiTypes.Allele, 'annotation' | 'tags'> {
    return {
        id: alleleId,
        caller_type: allele.callerType,
        change_from: allele.changeFrom,
        change_to: allele.changeTo,
        change_type: allele.changeType,
        chromosome: allele.chromosome,
        genome_reference: allele.genomeReference,
        length: allele.length,
        open_end_position: allele.openEndPosition,
        start_position: allele.startPosition,
        vcf_alt: allele.vcfAlt,
        vcf_pos: allele.vcfPos,
        vcf_ref: allele.vcfRef,
    }
}
