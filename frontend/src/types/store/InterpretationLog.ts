import * as ApiTypes from 'types/api/Pydantic'

export interface InterpretationLogItem {
    id: number
    userId: number | null
    dateCreated: string
    message: string | null
    priority: number | null
    reviewComment: string | null
    warningCleared: boolean | null
    editable: boolean
    alleleAssessment: {
        alleleId: number
        hgvsc: string[]
        classification: string
        previousClassification: string
    } | null
    alleleReport: {
        alleleId: number
        hgvsc: string[]
    } | null
}

export function mapFromApi(
    interpretationLogFromApi: ApiTypes.InterpretationLog,
): InterpretationLogItem {
    const { allelereport, alleleassessment } = interpretationLogFromApi

    return {
        id: interpretationLogFromApi.id,
        userId: interpretationLogFromApi.user_id || null,
        dateCreated: interpretationLogFromApi.date_created || '1970-01-01',
        message: interpretationLogFromApi.message || null,
        priority: interpretationLogFromApi.priority || null,
        reviewComment: interpretationLogFromApi.review_comment || null,
        warningCleared: interpretationLogFromApi.warning_cleared || null,
        editable: interpretationLogFromApi.editable,
        alleleAssessment: alleleassessment
            ? {
                  alleleId: alleleassessment.allele_id,
                  hgvsc: alleleassessment.hgvsc,
                  classification: alleleassessment.classification,
                  previousClassification: alleleassessment.previous_classification,
              }
            : null,
        alleleReport: allelereport
            ? {
                  alleleId: allelereport.allele_id,
                  hgvsc: allelereport.hgvsc,
              }
            : null,
    }
}
