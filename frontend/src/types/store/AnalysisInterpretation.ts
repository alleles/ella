import * as ApiTypes from 'types/api/Pydantic'
import {
    AlleleState,
    mapFromApiAlleleInterpretationState,
    mapToApiAlleleInterpretationState,
} from 'types/store/AlleleInterpretation'
import * as User from 'types/store/User'

export interface AnalysisAlleleState extends AlleleState {
    analysis: {
        comment: string
        notrelevant: boolean
        verification: string | null
    }
    report: {
        included: boolean | null
    }
    workflow: {
        reviewed: boolean
    }
}
export interface AnalysisInterpretationState {
    allele: {
        [k: string]: AnalysisAlleleState
    }
    filterConfigId: number | null
    manuallyAddedAlleles: number[]
    report: {
        comment: string
        indicationscomment: string
    }
}

export interface AnalysisInterpretation {
    id: number
    analysisId: number
    status: 'Not started' | 'Ongoing' | 'Done'
    finalized: boolean | null
    genePanelName: string
    genePanelVersion: string
    dateLastUpdate: string
    userId: number | null
    workflowStatus: 'Not ready' | 'Interpretation' | 'Review' | 'Medical review'
    state: AnalysisInterpretationState
    userState: {
        [k: string]: any
    }
    dateCreated: string
}

export function mapFromApi(
    analysisId: number,
    apiAnalysisInterpretation: ApiTypes.AnalysisInterpretation,
): AnalysisInterpretation {
    const alleleStateBase = mapFromApiAlleleInterpretationState(apiAnalysisInterpretation.state)
    const analysisAlleleStateByAlleleId: Record<string, AnalysisAlleleState> = Object.fromEntries(
        Object.entries(alleleStateBase.allele).map(([alleleId, alleleState]) => {
            const apiAlleleState = apiAnalysisInterpretation.state?.allele?.[alleleId]
            return [
                alleleId,
                {
                    ...alleleState,
                    analysis: {
                        verification: apiAlleleState?.analysis?.verification || null,
                        notrelevant: apiAlleleState?.analysis?.notrelevant || false,
                        comment: apiAlleleState?.analysis?.comment || '',
                    },
                    report: {
                        included: apiAlleleState?.report?.included || null,
                    },
                    workflow: {
                        reviewed: apiAlleleState?.workflow?.reviewed || false,
                    },
                },
            ]
        }),
    )

    return {
        id: apiAnalysisInterpretation.id,
        analysisId,
        genePanelName: apiAnalysisInterpretation.genepanel_name,
        genePanelVersion: apiAnalysisInterpretation.genepanel_version,
        status: apiAnalysisInterpretation.status,
        finalized: apiAnalysisInterpretation.finalized || null,
        dateLastUpdate: apiAnalysisInterpretation.date_last_update,
        dateCreated: apiAnalysisInterpretation.date_created || 'N/A',
        userState: apiAnalysisInterpretation.user_state || {},
        state: {
            allele: analysisAlleleStateByAlleleId,
            filterConfigId: apiAnalysisInterpretation.state?.filterconfigId || null,
            manuallyAddedAlleles: apiAnalysisInterpretation.state?.manuallyAddedAlleles || [],
            report: {
                comment: '',
                indicationscomment: '',
                ...apiAnalysisInterpretation.state?.report,
            },
        },
        userId: apiAnalysisInterpretation.user_id || null,
        workflowStatus: apiAnalysisInterpretation.workflow_status,
    }
}

function mapToApiAnalysisInterpretationState(
    analysisInterpretationState: AnalysisInterpretationState,
    genePanelName: string,
    genePanelVersion: string,
    user: User.User,
): ApiTypes.AnalysisInterpretation['state'] {
    const apiAlleleStateBase = {
        ...mapToApiAlleleInterpretationState(
            analysisInterpretationState,
            genePanelName,
            genePanelVersion,
            user,
        ),
    }

    const analysisAlleleStateByAlleleId = Object.fromEntries(
        Object.entries(apiAlleleStateBase.allele || {}).map(([alleleId, apiAlleleState]) => {
            const alleleState = analysisInterpretationState.allele[alleleId]
            return [
                alleleId,
                {
                    ...apiAlleleState,
                    analysis: {
                        verification: alleleState.analysis.verification || undefined,
                        notrelevant: alleleState.analysis.notrelevant,
                        comment: alleleState.analysis.comment,
                    },
                    report: {
                        included: alleleState.report.included ?? false,
                    },
                    workflow: {
                        reviewed: alleleState?.workflow?.reviewed || false,
                    },
                },
            ]
        }),
    )

    return {
        allele: analysisAlleleStateByAlleleId,
        report: analysisInterpretationState.report,
        filterconfigId: analysisInterpretationState.filterConfigId || undefined,
        manuallyAddedAlleles: analysisInterpretationState.manuallyAddedAlleles,
    }
}

export function mapToApi(
    analysisInterpretation: AnalysisInterpretation,
    user: User.User,
): ApiTypes.AnalysisInterpretation {
    return {
        id: analysisInterpretation.id,
        genepanel_name: analysisInterpretation.genePanelName,
        genepanel_version: analysisInterpretation.genePanelVersion,
        status: analysisInterpretation.status,
        finalized: analysisInterpretation.finalized || undefined,
        date_last_update: analysisInterpretation.dateLastUpdate,
        date_created: analysisInterpretation.dateCreated,
        user_id: analysisInterpretation.userId || undefined,
        workflow_status: analysisInterpretation.workflowStatus,
        user_state: analysisInterpretation.userState,
        state: {
            ...mapToApiAnalysisInterpretationState(
                analysisInterpretation.state,
                analysisInterpretation.genePanelName,
                analysisInterpretation.genePanelVersion,
                user,
            ),
        },
    }
}
