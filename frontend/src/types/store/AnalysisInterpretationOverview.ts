import * as ApiTypes from 'types/api/Pydantic'
import { AnalysisInterpretation } from 'types/store/AnalysisInterpretation'

export interface AnalysisInterpretationOverview
    extends Omit<
        AnalysisInterpretation,
        'state' | 'userState' | 'dateCreated' | 'genePanelName' | 'genePanelVersion' | 'analysisId'
    > {}

export function mapFromApi(
    analysisInterpretationOverviewFromApi: ApiTypes.AnalysisInterpretationOverview,
): AnalysisInterpretationOverview {
    return {
        dateLastUpdate: analysisInterpretationOverviewFromApi.date_last_update,
        finalized: analysisInterpretationOverviewFromApi.finalized || false,
        id: analysisInterpretationOverviewFromApi.id,
        status: analysisInterpretationOverviewFromApi.status,
        userId: analysisInterpretationOverviewFromApi.user_id || null,
        workflowStatus: analysisInterpretationOverviewFromApi.workflow_status,
    }
}
