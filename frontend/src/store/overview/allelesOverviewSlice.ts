import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { createSelector } from 'reselect'

import API from 'app/API'

import { processAndStoreAllele } from 'store/interpretation/alleleSlice'
import type { RootState } from 'store/store'
import { selectCurrentUser } from 'store/users/usersSlice'

import * as ApiTypes from 'types/api/Pydantic'
import { AlleleOverview, mapFromApi } from 'types/store/AlleleOverview'

interface SliceState {
    notStarted: AlleleOverview[]
    markedReview: AlleleOverview[]
    ongoing: AlleleOverview[]
    finalized: AlleleOverview[]
}

const initialState: SliceState = {
    notStarted: [],
    markedReview: [],
    ongoing: [],
    finalized: [],
}

export const fetchAlleles = createAsyncThunk<SliceState>(
    'allelesOverview/fetchAlleles',
    async (_, { dispatch }) => {
        const apiAlleles = await API.NonFinalizedAlleles.get().then((response) => response.data)
        const apiFinalizedAlleles = await API.FinalizedAlleles.get().then(
            (response) => response.data,
        )
        const promises: Promise<any>[] = []
        const mapFunction = (allele: ApiTypes.AlleleOverview) => {
            promises.push(dispatch(processAndStoreAllele(allele.allele)))
            return mapFromApi(allele)
        }

        const alleles = {
            notStarted: apiAlleles.not_started.map(mapFunction),
            markedReview: apiAlleles.marked_review.map(mapFunction),
            ongoing: apiAlleles.ongoing.map(mapFunction),
            finalized: apiFinalizedAlleles.map(mapFunction),
        }

        await Promise.all(promises)

        return alleles
    },
)

const allelesSlice = createSlice({
    name: 'allelesOverview',
    initialState,
    reducers: {
        setAlleles: (state, action) => {
            Object.assign(state, action.payload)
        },
    },
    extraReducers: (builder) => {
        builder.addCase(fetchAlleles.fulfilled, (state, action) => action.payload)
    },
})

export const selectOwnOngoingAlleles = createSelector(
    (state: RootState) => state.allelesOverview.ongoing,
    (state: RootState) => selectCurrentUser(state)?.id,
    (ongoing, userId) =>
        ongoing.filter((allele) => allele.interpretations.at(-1)?.user?.id === userId),
)

export const selectOthersOngoingAlleles = createSelector(
    (state: RootState) => state.allelesOverview.ongoing,
    (state: RootState) => selectCurrentUser(state)?.id,
    (ongoing, userId) =>
        ongoing.filter((allele) => allele.interpretations.at(-1)?.user?.id !== userId),
)

export const selectOngoingAlleles = (state: RootState) => state.allelesOverview.ongoing
export const selectNotStartedAlleles = (state: RootState) => state.allelesOverview.notStarted
export const selectMarkedReviewAlleles = (state: RootState) => state.allelesOverview.markedReview
export const selectFinalizedAlleles = (state: RootState) => state.allelesOverview.finalized

export default allelesSlice.reducer
