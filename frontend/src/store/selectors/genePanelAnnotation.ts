import intersectionWith from 'lodash/intersectionWith'
import isEqual from 'lodash/isEqual'
import { createCachedSelector } from 're-reselect'

import { selectGenePanel } from 'store/genePanels/genePanelsSlice'
import { selectAlleleById } from 'store/interpretation/alleleSlice'
import { selectAnnotationByAnnotationId } from 'store/interpretation/annotationSlice'

import { GenePanel } from 'types/store/GenePanel'

type TranscriptSplit = [string, string, number]

const getBestMatches = (
    annotationTranscripts: TranscriptSplit[],
    genePanelTranscripts: TranscriptSplit[],
): string[] => {
    const annotationTranscriptBases = annotationTranscripts.map((tx) => tx[1])
    const genePanelBaseMatches = genePanelTranscripts.filter((tx) =>
        annotationTranscriptBases.includes(tx[1]),
    )

    const identicalMatches = intersectionWith(annotationTranscripts, genePanelBaseMatches, isEqual)

    const maxVersionForBase = (base, transcripts) =>
        Math.max(...transcripts.filter((tx) => tx[1] === base).map((tx) => tx[2]))

    // Return transcripts that
    // Matches genepanel transcript (excluding version)
    // AND
    //     Matches gene panel transcript exactly
    //     OR
    //     No other version of this transcript matches genepanel transcript excactly
    //     AND The transcript has the highest version number
    const filteredTranscripts = annotationTranscripts.filter((annotationTranscript) => {
        const hasGenePanelBaseMatch = genePanelBaseMatches
            .map((gp_tx) => gp_tx[1])
            .includes(annotationTranscript[1])

        const matchesIdentical = identicalMatches
            .map((tx) => tx[0])
            .includes(annotationTranscript[0])

        const baseMatchesIdentical = identicalMatches
            .map((tx) => tx[1])
            .includes(annotationTranscript[1])

        const isMaxVersion =
            annotationTranscript[2] ===
            maxVersionForBase(annotationTranscript[1], annotationTranscripts)

        return (
            hasGenePanelBaseMatch && (matchesIdentical || (!baseMatchesIdentical && isMaxVersion))
        )
    })

    return filteredTranscripts.length
        ? filteredTranscripts.map((tx) => tx[0])
        : annotationTranscripts.map((tx) => tx[0])
}

const selectAnnotationFilteredTranscripts = createCachedSelector(
    selectAnnotationByAnnotationId,
    selectGenePanel,
    (annotation, genePanel) => {
        if (!annotation?.transcripts) {
            return []
        }

        if (!genePanel) {
            return annotation.transcripts.map((tx) => tx.transcript)
        }

        const annotationTranscripts: TranscriptSplit[] = annotation.transcripts.map((tx) => {
            const [base, version] = tx.transcript.split('.')
            return [tx.transcript, base, parseInt(version, 10)]
        })
        const genePanelTranscripts: TranscriptSplit[] = (genePanel.genes ?? []).flatMap((gene) =>
            gene.transcripts.map<TranscriptSplit>((tx) => {
                const [base, version] = tx.transcriptName.split('.')
                return [tx.transcriptName, base, parseInt(version, 10)]
            }),
        )
        const bestMatches = getBestMatches(annotationTranscripts, genePanelTranscripts)
        return bestMatches
    },
)((state, params) => JSON.stringify(params))

export type AlleleDisplay = {
    geneSymbol: string | null
    inheritance: string | null
    hgvsc: string | null
    hgvscShort: string | null
    hgvsp: string | null
    exonIntron: string | null
    consequence: (string | null)[]
    transcript: string | null
}

const selectAlleleDisplay = createCachedSelector(
    selectAlleleById,
    selectAnnotationByAnnotationId,
    selectAnnotationFilteredTranscripts,
    selectGenePanel,
    (allele, annotation, filteredTranscripts, genepanel: GenePanel): AlleleDisplay[] => {
        if (!allele || !annotation || filteredTranscripts.length === 0) {
            return []
        }

        const results = filteredTranscripts.map((txName): AlleleDisplay => {
            const transcript = annotation.transcripts?.find((tx) => tx.transcript === txName)
            if (!transcript) {
                throw new Error(`Unable to find transcript in annotation:`)
            }
            const gene = genepanel?.genes?.find((e) => e.hgncId === transcript.hgncId)
            const inhCodes: string[] = gene
                ? gene.phenotypes.map((p) => p.inheritance).filter((v) => v.length)
                : ([] as string[])

            return {
                geneSymbol: transcript.symbol || null,
                hgvsc: transcript.HGVSc || null,
                hgvscShort: transcript.HGVScShort || null,
                hgvsp: transcript.HGVSp || null,
                inheritance: inhCodes.length ? [...new Set(inhCodes)].join('/') : null,
                exonIntron: transcript.exon || transcript.intron || null,
                consequence: transcript.consequences,
                transcript: transcript.transcript,
            }
        })

        return results
    },
)((state, params) => JSON.stringify(params))

export { selectAnnotationFilteredTranscripts, selectAlleleDisplay }
