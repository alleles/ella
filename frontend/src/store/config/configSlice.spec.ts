import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

import API from 'app/API'

import configReducer, { fetchConfig, setConfig } from 'store/config/configSlice'

const mock = new MockAdapter(axios)

describe('config reducer', () => {
    afterEach(() => mock.reset())

    it('should handle initial state', () => {
        expect(configReducer(undefined, { type: 'unknown' })).toEqual({})
    })

    it('should handle setting a config', () => {
        const actual = configReducer(undefined, setConfig({ foo: 'bar' }))
        expect(actual).toEqual({ foo: 'bar' })
    })

    it('should handle fetching the config from the backend', async () => {
        mock.onGet(API.Config.path).reply(200, { foo: 'bar' })

        const dispatch = jest.fn()
        await fetchConfig()(dispatch, () => undefined, {})
        expect(dispatch).toHaveBeenCalledWith(expect.objectContaining({ payload: { foo: 'bar' } }))
    })
})
