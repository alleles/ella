import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

import API from 'app/API'

import type { RootState } from 'store/store'

import { GenePanel, mapFromApi } from 'types/store/GenePanel'

type SliceState = Record<string, GenePanel>

const initialState: SliceState = {}

export const fetchGenePanels = createAsyncThunk<SliceState>('config/fetchGenePanels', async () => {
    const simpleGenePanels = await API.GenePanels.get().then((response) => response.data)

    const promises: Promise<any>[] = []

    simpleGenePanels.forEach((genePanel) => {
        promises.push(
            API.GenePanel.get(genePanel.name, genePanel.version).then((response) => response.data),
        )
    })

    const genePanels = await Promise.all(promises)

    return genePanels.reduce(
        (prev, current) => ({
            ...prev,
            [`${current.name}_${current.version}`]: mapFromApi(current),
        }),
        {},
    )
})

const genePanelsSlice = createSlice({
    name: 'genePanels',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(fetchGenePanels.fulfilled, (state, action) => action.payload)
    },
})

export const selectGenePanels = (state: RootState) => state.genePanels

export const selectGenePanel = (
    state: RootState,
    {
        genePanelName,
        genePanelVersion,
    }: { genePanelName: string | undefined; genePanelVersion: string | undefined },
) => state.genePanels[`${genePanelName}_${genePanelVersion}`]

export default genePanelsSlice.reducer
