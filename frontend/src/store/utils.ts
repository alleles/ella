import {
    ActionReducerMapBuilder,
    AsyncThunk,
    PayloadAction,
    SerializedError,
} from '@reduxjs/toolkit'
import {
    AsyncThunkFulfilledActionCreator,
    AsyncThunkPendingActionCreator,
} from '@reduxjs/toolkit/dist/createAsyncThunk'

import { RootState } from 'store/store'

export function throwSerializedError(serializedError: SerializedError) {
    throw {
        message: serializedError.message ?? '',
        name: serializedError.name ?? '',
        stack: serializedError.stack,
    } as Error
}
export function thunkIsRejected(thunk): boolean {
    return thunk.type.includes('rejected')
}

/*
Generic logic to handle conditional fetching. This can be used on fetches for static data, e.g. fetches that return identical responses
for same arguments.

The logic can be used on any slice that is types like this `any & Requests`

This will add an object under the key `requests` on the slice, that can be used to track actions pending/fulfilled/rejected

We then use a generic `condition` on an asyncThunk to stop a thunk before running (if it is already fulfilled or pending for that argument):

const exampleThunk = createAsyncThunk(
  "foo/bar",
  async (id: number) {
    const result = await API.SomeNamespace.get(id).then(result => result.data)
    return {[id]: result}
  },
  CommonRequestCondition("sliceName")
)

The CommonRequestCondition will check whether `state.requests[id]` is in a pending or fulfilled state, and abort the thunk if so.

For this to work, we also need to actually set `state.requests[id]` with reducers. This is done with the `CommonRequestBuilder`.
This function supports callbacks for each thunk case (pending, fulfilled, rejected), to allow handling the thunk like normal:

const slice = createSlice(
  name: "sliceName",
  {requests: {}}, // Initial state
  reducers: {},
  extraReducers: (builder) => {
    CommonRequestBuilder(builder, exampleThunk, {
      fulfilled: (state, action) => {
        // Callback for specific handling of the fulfilled case
      },
      pending: (state, action) => {
        // Callback for specific handling of the pending case
      },
      rejected: (state, action) => {
        // Callback for specific handling of the rejected case
      }
    })
  }
)


*/
export type Requests = {
    requests: Record<number, 'pending' | 'fulfilled' | 'rejected'>
}

// Only allow CommonRequestCondition to be used on slices that extends `Requests`
type KeysMatching<T, V> = { [K in keyof T]-?: T[K] extends V ? K : never }[keyof T]
type SlicesWithRequests = KeysMatching<RootState, Requests>

export const CommonRequestCondition = (sliceName: SlicesWithRequests) => ({
    condition: (id: number | string, { getState }) => {
        const state = getState() as RootState
        const fetchStatus = state[sliceName].requests[id]

        if (fetchStatus === 'fulfilled' || fetchStatus === 'pending') {
            // Already fetched or in progress, don't need to re-fetch
            return false
        }

        return true
    },
})

export const CommonRequestBuilder = <
    T extends ActionReducerMapBuilder<any>,
    V extends AsyncThunk<any, any, any>,
>(
    builder: T,
    thunk: V,
    extraReducers?: {
        pending?: (
            state: Parameters<Parameters<T['addCase']>[1]>[0],
            action: ReturnType<AsyncThunkPendingActionCreator<Parameters<V>[0]>>,
        ) => void
        fulfilled?: (
            state: Parameters<Parameters<T['addCase']>[1]>[0],
            action: ReturnType<AsyncThunkFulfilledActionCreator<V, Parameters<V>[0]>>,
        ) => void
        // Gave up typing the reject action
        rejected?: (state: any, action: PayloadAction<any, any, any, any>) => void
    },
) =>
    builder
        .addCase(thunk.pending, (state, action) => {
            Object.assign(state.requests, { [action.meta.arg]: 'pending' })
            if (extraReducers?.pending) {
                extraReducers.pending(state, action)
            }
        })
        .addCase(thunk.rejected, (state, action) => {
            Object.assign(state.requests, { [action.meta.arg]: 'rejected' })
            if (extraReducers?.rejected) {
                extraReducers.rejected(state, action)
            }
        })
        .addCase(thunk.fulfilled, (state, action) => {
            Object.assign(state.requests, { [action.meta.arg]: 'fulfilled' })
            if (extraReducers?.fulfilled) {
                extraReducers.fulfilled(state, action)
            }
        })
