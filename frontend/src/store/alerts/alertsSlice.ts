import { PayloadAction, createAsyncThunk, createSlice } from '@reduxjs/toolkit'

import type { RootState } from 'store/store'

import { Alert, AlertArgs } from 'types/Alert'

let id = 0
export const DEFAULT_TIMEOUT = 5000

type SliceState = Alert[]
const initialState: SliceState = []

const alertSlice = createSlice({
    name: 'alerts',
    initialState,
    reducers: {
        insertAlert: (state, action: PayloadAction<AlertArgs>) => {
            id += 1
            const timeout = action.payload?.timeout || DEFAULT_TIMEOUT
            const type = action.payload?.type || 'info'
            const alert: Alert = {
                id,
                timeout,
                type,
                ...action.payload,
            }
            state.push(alert)
        },
        removeAlert: (state, action: PayloadAction<number>) =>
            state.filter((alert) => alert.id !== action.payload),
    },
})

export const createAlert = createAsyncThunk<Promise<number>, AlertArgs>(
    'alerts/createAlert',
    async (payload, { dispatch }) => {
        const { insertAlert } = alertSlice.actions
        dispatch(insertAlert(payload))
        const theId = id
        if (payload.timeout !== 0) {
            setTimeout(() => {
                dispatch(alertSlice.actions.removeAlert(theId))
            }, payload.timeout || DEFAULT_TIMEOUT)
        }
        return Promise.resolve(id)
    },
)

export const selectAlerts = (state: RootState) => state.alerts
export const { removeAlert } = alertSlice.actions
export default alertSlice.reducer
