import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

import API from 'app/API'

import alleleCollisionsReducer, {
    fetchAlleleCollisions,
    selectAlleleCollisionsByAlleleId,
} from 'store/interpretation/alleleCollisionsSlice'
import { store } from 'store/store'

import { AlleleCollision } from 'types/store/AlleleCollisions'

const mock = new MockAdapter(axios)

function buildAlleleCollision(override: Partial<AlleleCollision>): AlleleCollision {
    return {
        type: 'allele',
        analysisName: null,
        workflowStatus: 'Interpretation',
        userId: null,
        ...override,
    }
}

describe('allele collision reducer', () => {
    afterEach(() => mock.reset())

    it('should handle initial state', () => {
        expect(alleleCollisionsReducer(undefined, { type: 'unknown' })).toEqual({})
    })
    it('should handle fetching allele collisions from the backend', async () => {
        mock.onGet(API.AlleleCollisions.path(1)).reply(200, [
            {
                analysis_name: 'foo',
                type: 'allele',
                workflow_status: 'Interpretation',
                analysis_id: 'REMOVED',
                user: { id: 1 }, // Moved
                allele_id: 'REMOVED',
            },
            {
                analysis_name: 'bar',
                type: 'analysis',
                workflow_status: 'Review',
                analysis_id: 'REMOVED',
                user: { id: 2 }, // Moved
                allele_id: 'REMOVED',
            },
        ])

        const dispatch = jest.fn()
        await fetchAlleleCollisions(1)(dispatch, () => undefined, {})
        expect(dispatch).toHaveBeenCalledWith(
            expect.objectContaining({
                // Cleans out unwanted
                payload: {
                    1: [
                        {
                            analysisName: 'foo',
                            type: 'allele',
                            workflowStatus: 'Interpretation',
                            userId: 1,
                        },
                        {
                            analysisName: 'bar',
                            type: 'analysis',
                            workflowStatus: 'Review',
                            userId: 2,
                        },
                    ],
                },
            }),
        )
    })

    it('should handle getting allele collisions by allele id', async () => {
        const state = {
            ...store.getState(),
            alleleCollisions: {
                1: [buildAlleleCollision({ analysisName: 'foo' })],
                2: [
                    buildAlleleCollision({ analysisName: 'bar' }),
                    buildAlleleCollision({ analysisName: 'baz' }),
                ],
            },
        }

        expect(selectAlleleCollisionsByAlleleId(state, { alleleId: 1 })).toEqual([
            buildAlleleCollision({ analysisName: 'foo' }),
        ])
        expect(selectAlleleCollisionsByAlleleId(state, { alleleId: 2 })).toEqual([
            buildAlleleCollision({ analysisName: 'bar' }),
            buildAlleleCollision({ analysisName: 'baz' }),
        ])
        expect(selectAlleleCollisionsByAlleleId(state, { alleleId: 3 })).toEqual(undefined)
    })
})
