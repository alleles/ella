import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

import API from 'app/API'

import type { RootState } from 'store/store'

import { Attachment, mapFromApi } from 'types/store/Attachment'

const initialState: Record<number, Attachment> = {}

const fetchAttachments = createAsyncThunk<Record<number, Attachment>, number[]>(
    'attachment/fetchAttachmentForAlleleId',
    async (attachmentIds) => {
        if (attachmentIds.length === 0) {
            return {}
        }
        const attachments = await API.Attachments.get(attachmentIds).then((result) => result.data)

        const attachmentsById = attachments.reduce(
            (previous, current) => ({
                ...previous,
                [current.id]: mapFromApi(current),
            }),
            {} as Record<number, Attachment>,
        )

        return attachmentsById
    },
)

const attachmentSlice = createSlice({
    name: 'attachments',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(fetchAttachments.fulfilled, (state, action) => {
            Object.assign(state, action.payload)
        })
    },
})

const selectAttachmentById = (state: RootState, attachmentId: number) =>
    state.attachments[attachmentId]

export { fetchAttachments, selectAttachmentById }

export default attachmentSlice.reducer
