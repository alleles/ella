import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

import API from 'app/API'

import alleleReportReducer, {
    fetchAlleleReportsForAlleleId,
    selectAlleleReportById,
} from 'store/interpretation/alleleReportSlice'
import { store } from 'store/store'

import { AlleleReport } from 'types/store/AlleleReport'

const mock = new MockAdapter(axios)

function buildAlleleReport(override: Partial<AlleleReport>): AlleleReport {
    return {
        alleleId: 1,
        id: 1,
        analysisId: null,
        dateCreated: '1970-01-01',
        dateSuperseded: null,
        evaluation: {
            comment: '',
        },
        userId: 0,
        previousReportId: null,
        ...override,
    }
}

describe('allele report reducer', () => {
    afterEach(() => mock.reset())

    it('should handle initial state', () => {
        expect(alleleReportReducer(undefined, { type: 'unknown' })).toEqual({})
    })

    it('should handle fetching allele reports from the backend', async () => {
        mock.onGet(API.AlleleReports.path).reply((config) => {
            if (config.params.q.allele_id === 1) {
                return [
                    200,
                    [
                        {
                            user: 'REMOVED',
                            usergroup: 'REMOVED',
                            seconds_since_update: 'REMOVED',
                            evaluation: {
                                comment: 'comment',
                            },
                            id: 3,
                            allele_id: 2,
                            user_id: 2,
                            usergroup_id: 3,
                            date_created: '2021-11-22',
                            date_superceeded: null,
                            previous_report_id: null,
                            analysis_id: 1,
                        },
                    ],
                ]
            }
            return [400]
        })

        const dispatch = jest.fn()
        await fetchAlleleReportsForAlleleId(1)(dispatch, () => undefined, {})

        expect(dispatch).toHaveBeenCalledWith(
            expect.objectContaining({
                // Cleans out unwanted
                payload: {
                    3: {
                        alleleId: 2,
                        id: 3,
                        evaluation: {
                            comment: 'comment',
                        },
                        userId: 2,
                        dateCreated: '2021-11-22',
                        dateSuperseded: null,
                        previousReportId: null,
                        analysisId: 1,
                    },
                },
            }),
        )
    })

    it('should handle getting allele reports by id', async () => {
        const state = {
            ...store.getState(),
            alleleReports: {
                1: buildAlleleReport({ evaluation: { comment: 'foo' } }),
                2: buildAlleleReport({ evaluation: { comment: 'bar' } }),
            },
        }

        expect(selectAlleleReportById(state, { alleleReportId: 1 })).toEqual(
            buildAlleleReport({ evaluation: { comment: 'foo' } }),
        )
        expect(selectAlleleReportById(state, { alleleReportId: 2 })).toEqual(
            buildAlleleReport({ evaluation: { comment: 'bar' } }),
        )
        expect(selectAlleleReportById(state, { alleleReportId: 3 })).toEqual(undefined)
    })
})
