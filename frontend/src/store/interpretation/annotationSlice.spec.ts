import annotationReducer, {
    processAndStoreAnnotation,
    selectAnnotationByAnnotationId,
    selectWorstConsequenceById,
} from 'store/interpretation/annotationSlice'
import { store } from 'store/store'

import * as Pydantic from 'types/api/Pydantic'
import { Annotation } from 'types/store/Annotation'

export function buildAnnotation(override: Partial<Annotation>): Annotation {
    return {
        alleleId: 1,
        filteredTranscriptsIds: [],
        schemaVersion: '1',
        annotationConfigId: 1,
        frequencies: {}, // as Annotation['frequencies'],
        dateSuperseeded: null,
        dateCreated: '1970-01-01',
        references: [], // as Annotation['references'],
        transcripts: [],
        ...override,
    }
}

describe('annotation reducer', () => {
    it('should handle initial state', () => {
        expect(annotationReducer(undefined, { type: 'unknown' })).toEqual({})
    })

    it('should handle processing annotations', async () => {
        // Create a config for custom annotation
        // Current implementation strips out custom annotation from the annotation object, that
        // is currently added in the backend
        const configState = { config: { custom_annotation: { external: [{ key: 'foo' }] } } }

        const inputAnnotation: Pydantic.Annotation = {
            external: {
                dabla: 'SHOULD BE KEPT',
                foo: {
                    bar: 'CUSTOM ANNOTATION REMOVED',
                },
            },
            schema_version: '2',
            references: [
                {
                    id: 1,
                    source: 'KEEP',
                },
                {
                    pubmed_id: 1,
                    source: 'KEEP',
                },
                {
                    pubmed_id: 3,
                    source: 'User', // REMOVED
                },
            ],
            annotation_config_id: 1,
            date_superseded: '',
            annotation_id: 1,
            transcripts: [],
        }

        const outputAnnotation: { [key: string]: Annotation } = {
            1: {
                alleleId: 1,
                external: { dabla: 'SHOULD BE KEPT' },
                schemaVersion: '2',
                annotationConfigId: 1,
                dateSuperseeded: null,
                frequencies: {},
                references: [
                    {
                        id: 1,
                        pubmedId: undefined,
                        source: 'KEEP',
                    },
                    {
                        id: undefined,
                        pubmedId: 1,
                        source: 'KEEP',
                    },
                ],
                transcripts: [],
                filteredTranscriptsIds: [],
            },
        }

        const dispatch = jest.fn()
        await processAndStoreAnnotation({
            annotationsFromApi: inputAnnotation,
            alleleId: 1,
        })(dispatch, () => configState, {})

        expect(dispatch).toHaveBeenCalledWith(
            expect.objectContaining({
                payload: {
                    ...outputAnnotation,
                },
            }),
        )
    })

    it('should handle getting annotation by id', async () => {
        const state = {
            ...store.getState(),
            annotations: {
                1: buildAnnotation({ dateCreated: '2022-01-10' }),
                2: buildAnnotation({ dateCreated: '2022-01-11' }),
            },
        }

        expect(selectAnnotationByAnnotationId(state, { annotationId: 1 })).toEqual(
            buildAnnotation({ dateCreated: '2022-01-10' }),
        )
        expect(
            selectAnnotationByAnnotationId(state, {
                annotationId: 2,
            }),
        ).toEqual(buildAnnotation({ dateCreated: '2022-01-11' }))
        expect(selectAnnotationByAnnotationId(state, { annotationId: 3 })).toEqual(undefined)
    })

    it('should get worst consequence', async () => {
        const storeTemplate = (transcripts) => ({
            ...store.getState(),
            annotations: {
                1: buildAnnotation({
                    transcripts: transcripts.map((tx) => ({
                        transcript: tx[0],
                        consequences: tx[1],
                    })),
                }),
            },
        })
        let state = storeTemplate([
            ['NM_001.1', ['intron_variant']],
            ['NM_002.1', ['missense_variant']],
            ['NM_003.1', ['transcript_ablation']],
        ])

        expect(selectWorstConsequenceById(state, { annotationId: 1 })).toEqual({
            consequence: 'transcript_ablation',
            transcripts: ['NM_003.1'],
        })

        state = storeTemplate([
            ['NM_001.1', ['intron_variant']],
            ['NM_002.1', ['transcript_ablation']],
            ['NM_003.1', ['intron_variant', 'transcript_ablation']],
        ])

        expect(selectWorstConsequenceById(state, { annotationId: 1 })).toEqual({
            consequence: 'transcript_ablation',
            transcripts: ['NM_002.1', 'NM_003.1'],
        })
    })
})
