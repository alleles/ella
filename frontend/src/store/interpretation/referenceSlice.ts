import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

import API from 'app/API'

import type { RootState } from 'store/store'

import type * as ApiTypes from 'types/api/Pydantic'
import { Reference, mapFromApi } from 'types/store/Reference'

const initialState: Record<number, Reference> = {}

type InputType = {
    ids: number[]
    pubMedIds: number[]
}

const fetchReferences = createAsyncThunk<typeof initialState, InputType>(
    'references/fetchReferences',
    async ({ ids, pubMedIds }: InputType) => {
        let referencesByPubMedIds: ApiTypes.ApiModel['reference_list_response'] = []
        if (pubMedIds?.length) {
            referencesByPubMedIds = await API.References.getByPubMedIds(pubMedIds).then(
                (result) => result.data,
            )
        }

        const referencesByIds = ids?.length
            ? await API.References.getByIds(ids).then((result) => result.data)
            : []

        const references = [...referencesByPubMedIds, ...referencesByIds].reduce(
            (prev, current) => ({
                ...prev,
                [current.id]: mapFromApi(current),
            }),
            {} as typeof initialState,
        )

        return references
    },
)

const references = createSlice({
    name: 'references',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(fetchReferences.fulfilled, (state, action) => {
            Object.assign(state, action.payload)
        })
    },
})

const selectReferenceById = (state: RootState, { referenceId }: { referenceId: number }) =>
    state.references[referenceId]

const selectReferencesByIds = (state: RootState, { referenceIds }: { referenceIds: number[] }) =>
    referenceIds
        .map((referenceId) => selectReferenceById(state, { referenceId }))
        .filter((reference) => !!reference)

const selectReferenceByPubMedId = (state: RootState, { pubMedId }: { pubMedId: number }) =>
    Object.values(state.references).find((r) => r.pubmedId === pubMedId)

const selectReferencesByPubMedIds = (state: RootState, { pubmedIds }: { pubmedIds: number[] }) =>
    pubmedIds
        .map((pubMedId) => selectReferenceByPubMedId(state, { pubMedId }))
        .filter((reference) => !!reference) as Reference[]

export {
    fetchReferences,
    selectReferenceById,
    selectReferencesByIds,
    selectReferenceByPubMedId,
    selectReferencesByPubMedIds,
}

export default references.reducer
