import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

import API from 'app/API'

import type { RootState } from 'store/store'

import * as ApiTypes from 'types/api/Pydantic'
import { FilterConfig, mapFromApi } from 'types/store/FilterConfig'

const initialState: Record<number, FilterConfig> = {}

const fetchFilterConfigById = createAsyncThunk<typeof initialState, number>(
    'filterConfig/fetchFilterConfigForAlleleId',
    async (filterConfigId: number) => {
        const apiFilterConfig = await API.FilterConfigs.get(filterConfigId).then(
            (result) => result.data,
        )
        return { [apiFilterConfig.id]: mapFromApi(apiFilterConfig) }
    },
)

const processAndStoreFilterConfig = createAsyncThunk<typeof initialState, ApiTypes.FilterConfig>(
    'filterConfig/processAndStoreFilterConfig',
    async (apiFilterConfig) => ({
        [apiFilterConfig.id]: mapFromApi(apiFilterConfig),
    }),
)

const filterConfigSlice = createSlice({
    name: 'filterConfigs',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(fetchFilterConfigById.fulfilled, (state, action) => {
            Object.assign(state, action.payload)
        })
        builder.addCase(processAndStoreFilterConfig.fulfilled, (state, action) => {
            Object.assign(state, action.payload)
        })
    },
})

const selectFilterConfigById = (state: RootState, { filterConfigId }: { filterConfigId: number }) =>
    state.filterConfigs[filterConfigId]

export { fetchFilterConfigById, processAndStoreFilterConfig, selectFilterConfigById }

export default filterConfigSlice.reducer
