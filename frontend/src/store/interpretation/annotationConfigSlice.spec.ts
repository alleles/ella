import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

import API from 'app/API'

import annotationConfigReducer, {
    fetchAnnotationConfig,
    selectAnnotationConfigById,
} from 'store/interpretation/annotationConfigSlice'
import { store } from 'store/store'

import { AnnotationConfig } from 'types/store/AnnotationConfig'

function buildAnnotationConfigItem(
    override: Partial<AnnotationConfig['view'][0]>,
): AnnotationConfig['view'][0] {
    return {
        section: 'external',
        template: 'keyValue',
        source: '',
        title: null,
        url: null,
        urlEmpty: null,
        config: {},
        ...override,
    }
}

const mock = new MockAdapter(axios)

describe('annotation config reducer', () => {
    afterEach(() => mock.reset())

    it('should handle initial state', () => {
        expect(annotationConfigReducer(undefined, { type: 'unknown' })).toEqual({
            requests: {},
        })
    })

    it('should handle fetching annotation config from the backend', async () => {
        mock.onGet(API.AnnotationConfigs.path).reply(200, [
            {
                id: 1,
                view: [
                    {
                        section: 'bar',
                    },
                ],
            },
        ])

        const dispatch = jest.fn()
        await fetchAnnotationConfig(1)(
            dispatch,
            () => ({ annotationConfigs: { requests: {} } }),
            {},
        )

        expect(dispatch).toHaveBeenCalledWith(
            expect.objectContaining({
                payload: {
                    1: {
                        view: [expect.objectContaining({ section: 'bar' })],
                    },
                },
            }),
        )
    })

    it('should handle getting annotation configs by id', async () => {
        const state = {
            ...store.getState(),
            annotationConfigs: {
                1: { view: [buildAnnotationConfigItem({ source: 'foo' })] },
                2: { view: [buildAnnotationConfigItem({ source: 'bar' })] },
                requests: {},
            },
        }

        expect(
            selectAnnotationConfigById(state, {
                annotationConfigId: 1,
            }),
        ).toEqual({ view: [buildAnnotationConfigItem({ source: 'foo' })] })
        expect(
            selectAnnotationConfigById(state, {
                annotationConfigId: 2,
            }),
        ).toEqual({ view: [buildAnnotationConfigItem({ source: 'bar' })] })
        expect(
            selectAnnotationConfigById(state, {
                annotationConfigId: 3,
            }),
        ).toEqual(undefined)
    })
})
