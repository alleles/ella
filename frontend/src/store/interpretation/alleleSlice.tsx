import { createAsyncThunk, createSelector, createSlice } from '@reduxjs/toolkit'

import { processAndStoreAnnotation } from 'store/interpretation/annotationSlice'
import { fetchCustomAnnotationsForAlleleId } from 'store/interpretation/customAnnotationSlice'
import type { RootState } from 'store/store'

import * as ApiTypes from 'types/api/Pydantic'
import { Allele, mapFromApi } from 'types/store/Allele'

const initialState: Record<number, Allele> = {}

const processAndStoreAllele = createAsyncThunk<typeof initialState, ApiTypes.Allele>(
    'alleles/processAndStoreAllele',
    async (apiAllele, { dispatch }) => {
        const allele = mapFromApi(apiAllele)

        await dispatch(
            processAndStoreAnnotation({
                annotationsFromApi: apiAllele.annotation,
                alleleId: apiAllele.id,
            }),
        )

        dispatch(fetchCustomAnnotationsForAlleleId(apiAllele.id))

        return { [apiAllele.id]: allele }
    },
)

const alleleSlice = createSlice({
    name: 'alleles',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(processAndStoreAllele.fulfilled, (state, action) => {
            Object.assign(state, action.payload)
        })
    },
})

const selectAlleleById = (state: RootState, { alleleId }: { alleleId: number }) =>
    state.alleles[alleleId]

const selectAllelesById = (state: RootState, { alleleIds }: { alleleIds: number[] }) =>
    alleleIds.reduce((acc, alleleId) => {
        acc[alleleId] = selectAlleleById(state, { alleleId })
        return acc
    }, {}) as Record<number, Allele>

const selectAlleles = (state: RootState) => state.alleles

const selectHGVSgByAlleleId = createSelector(selectAlleleById, (allele): string => {
    if (!allele) {
        return ''
    }
    let hgvsg
    const start = allele.startPosition
    const end = allele.openEndPosition
    switch (allele.changeType) {
        case 'SNP':
            hgvsg = `g.${start + 1}${allele.changeFrom}>${allele.changeTo}`
            break
        case 'del':
            if (start + 1 === end) {
                hgvsg = `g.${start + 1}del`
            } else {
                hgvsg = `g.${start + 1}_${end}del`
            }
            break
        case 'indel':
            if (start + 1 === end) {
                hgvsg = `g.${start + 1}delins${allele.changeTo}`
            } else {
                hgvsg = `g.${start + 1}_${end}delins${allele.changeTo}`
            }
            break
        case 'ins':
            hgvsg = `g.${start + 1}_${end + 1}ins${allele.changeTo}`
            break
        default:
            throw new Error(`Unsupported change type detected: ${allele.changeType}`)
    }
    return hgvsg
})

const selectAlamutStringByAlleleId = createSelector(
    selectAlleleById,
    selectHGVSgByAlleleId,
    (allele, hgvsg): string => {
        if (!allele) {
            return ''
        }

        return `Chr${allele.chromosome}(${allele.genome_reference}):${hgvsg}`
    },
)

export {
    processAndStoreAllele,
    selectAlleleById,
    selectAllelesById,
    selectAlleles,
    selectHGVSgByAlleleId,
    selectAlamutStringByAlleleId,
}

export default alleleSlice.reducer
