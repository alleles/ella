import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'

import Editor from 'components/editor/Editor'
import { Card } from 'components/elements/Card'
import { RadioButtons } from 'components/elements/RadioButtons'
import SectionBox from 'components/elements/SectionBox'
import ToggleButton from 'components/elements/ToggleButton'

import { selectRichTextTemplates } from 'store/config/configSlice'

import { SingleGenotypeSampleData } from 'types/view/GenotypeSampleData'

import { booleanToYesNo, numberFormat } from 'utils/formatting'

import useAnalysisInterpretation from 'views/interpretation/AnalysisInterpretationContext'
import useInterpretationStateContext from 'views/interpretation/InterpretationStateContext'

function QualityPanel({
    genotypeSampleData,
    title,
}: {
    genotypeSampleData: SingleGenotypeSampleData
    title: string
}) {
    const warningStyle = 'bg-ellared-dark text-ellagray-100 p-2 -ml-1 my-2 rounded-sm'

    return (
        <div className="max-w-xs pr-8 text-sm">
            <span className={`mb-2 font-semibold `}>{title}</span>
            {genotypeSampleData.needsVerification ? (
                <div className={`${warningStyle}`}> NEEDS VERIFICATION</div>
            ) : null}
            <div className="table table-fixed">
                <div className="table-row">
                    <span className="table-cell pr-4 font-semibold">Genotype</span>
                    <span className="table-cell">{genotypeSampleData.formatted ?? '-'}</span>
                </div>
                <div className="table-row">
                    <span className="table-cell pr-4 font-semibold">P(de novo)</span>
                    <span className="table-cell">{genotypeSampleData.pDenovo ?? '-'}</span>
                </div>
                <div className="table-row">
                    <span className="table-cell font-semibold">GQ</span>
                    <span className="table-cell">{genotypeSampleData.genotypeQuality ?? '-'}</span>
                </div>
                <div className="table-row">
                    <span className="table-cell font-semibold">Depth</span>
                    <span className="table-cell">{genotypeSampleData.sequencingDepth ?? '-'}</span>
                </div>
                <div className="table-row">
                    <span className="table-cell font-semibold">Ratio</span>
                    <span className="table-cell">
                        {genotypeSampleData.alleleRatio
                            ? numberFormat(genotypeSampleData.alleleRatio, 2)
                            : '-'}
                    </span>
                </div>
                {genotypeSampleData.alleleDepth
                    ? Object.entries(genotypeSampleData.alleleDepth)
                          .sort(([key]) => (key.startsWith('REF') ? -1 : 1))
                          .map(([key, value]) => (
                              <div className="table-row" key={key}>
                                  <span className="table-cell">- {key}:</span>
                                  <span className="table-cell">{value ?? '-'}</span>
                              </div>
                          ))
                    : null}

                <div />
            </div>
        </div>
    )
}

export default function AnalysisSpecific() {
    const analysisInterpretation = useAnalysisInterpretation()
    const { presentationAlleles, samples } = analysisInterpretation

    const { alleleState, setAlleleState, readOnly, selectedAlleleId } =
        useInterpretationStateContext()
    const { genotypeSampleData, variantCalling } = presentationAlleles[selectedAlleleId]

    if (!('analysis' in alleleState)) {
        throw new Error('Allele state is not an instance of AnalysisAlleleState')
    }

    const templates = useSelector(selectRichTextTemplates('classificationAnalysisSpecific'))

    const [comment, setComment] = useState(alleleState?.analysis.comment)

    const onChange = (value: string) => {
        setAlleleState({
            analysis: {
                comment: value,
            },
        })
    }

    useEffect(() => {
        setComment(alleleState?.analysis.comment)
    }, [selectedAlleleId])

    const headerComponents = [
        <RadioButtons
            key="analysis-specific-section-header-radio-buttons"
            direction="row"
            className={`gap-x-2 ${readOnly ? 'cursor: none' : ''}`}
            selectedButton={alleleState.analysis.verification}
            nullable
            onChange={(v) => setAlleleState({ analysis: { verification: v } })}
            disabled={readOnly}
            buttons={[
                { key: 'verified', label: 'Verified' },
                { key: 'technical', label: 'Technical' },
            ]}
        />,
        <ToggleButton
            key="analysis-specific-section-header-toggle-button"
            title="Not Relevant"
            onClick={(v) => setAlleleState({ analysis: { notrelevant: v } })}
            selected={alleleState.analysis.notrelevant}
            disabled={readOnly}
        >
            Not Relevant
        </ToggleButton>,
    ]

    return (
        <SectionBox
            title="Analysis specific"
            color="ellablue-lighter"
            initCollapsed={false}
            headerComponents={headerComponents}
        >
            <Editor
                key={`analysis-specific-comment-${selectedAlleleId}`}
                id="analysis-specific-comment"
                placeholder="Analysis - comments"
                initialValue={comment}
                onBlur={onChange}
                templates={templates}
                readOnly={readOnly}
            />
            <div className="flex flex-wrap">
                <Card label="Variant quality">
                    <div className="max-w-xs text-sm">
                        {variantCalling.map((variant) => (
                            <div className="table table-fixed" key={`${JSON.stringify(variant)}`}>
                                <div className="table-row">
                                    <span className="table-cell pr-4 font-semibold">FILTER</span>
                                    <span className="table-cell">
                                        {variant.filterStatus ?? '-'}
                                    </span>
                                </div>
                                <div className="table-row">
                                    <span className="table-cell pr-4 font-semibold">QUALITY</span>
                                    <span className="table-cell">
                                        {variant.variantQuality ?? '-'}
                                    </span>
                                </div>
                                <div className="table-row">
                                    <span className="table-cell pr-4 font-semibold">
                                        MULTIALLELIC
                                    </span>
                                    <span className="table-cell">
                                        {variant.multiAllelic === null
                                            ? '-'
                                            : booleanToYesNo(variant.multiAllelic)}
                                    </span>
                                </div>
                            </div>
                        ))}
                    </div>
                </Card>

                <Card label="Genotype">
                    {Object.entries(genotypeSampleData).map(([key, gsd]) => (
                        <div className="flex flex-wrap" key={key}>
                            {gsd?.proband ? (
                                <QualityPanel
                                    genotypeSampleData={gsd.proband}
                                    title={`PROBAND (${samples[gsd.proband.sampleId].sex ?? '?'})`}
                                />
                            ) : null}
                            {gsd?.mother ? (
                                <QualityPanel genotypeSampleData={gsd.mother} title="MOTHER" />
                            ) : null}
                            {gsd?.father ? (
                                <QualityPanel genotypeSampleData={gsd.father} title="FATHER" />
                            ) : null}
                            {gsd?.unaffectedSiblings.length
                                ? gsd?.unaffectedSiblings.map((sibling) => (
                                      <QualityPanel
                                          key={sibling.id}
                                          genotypeSampleData={sibling}
                                          title={`UNAFFECTED SIBLING (${
                                              samples[sibling.sampleId].sex ?? '?'
                                          })`}
                                      />
                                  ))
                                : null}
                            {gsd?.affectedSiblings.length
                                ? gsd?.affectedSiblings.map((sibling) => (
                                      <QualityPanel
                                          key={sibling.id}
                                          genotypeSampleData={sibling}
                                          title={`AFFECTED SIBLING (${
                                              samples[sibling.sampleId].sex ?? '?'
                                          })`}
                                      />
                                  ))
                                : null}
                        </div>
                    ))}
                </Card>
            </div>
        </SectionBox>
    )
}
