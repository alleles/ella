import { AlleleState } from 'types/store/AlleleInterpretation'
import { AnnotationReference, CustomAnnotationReference } from 'types/store/Annotation'
import { Reference } from 'types/store/Reference'
import { ReferenceAssessment } from 'types/store/ReferenceAssessment'

interface ReferenceAnnotationProp {
    alleleState: AlleleState
    references: Reference[]
    annotationReferences: AnnotationReference[]
    customAnnotationReferences: CustomAnnotationReference[]
}

export type ReferenceInfo = {
    reference: Reference
    assessment?: ReferenceAssessment
    source?: string
}

export type ReferenceStatus = {
    // references that has not been evaluated
    pending: ReferenceInfo[]
    // evaluted and relevant references
    evaluated: ReferenceInfo[]
    // evaluated references found not to be relevant
    not_relevant: ReferenceInfo[]
    // references that are ignored by the interpreter
    ignored: ReferenceInfo[]
    // references that has not yet been added to the ELLA database,
    // must be downloaded and added manually by the interpreter
    // (Add Studies button),
    // then they can be evaluated
    missing: AnnotationReference[]
}

function sortByPublishedYear(refInfo1: ReferenceInfo, refInfo2: ReferenceInfo): number {
    const y1 = refInfo1.reference.year
    const y2 = refInfo2.reference.year
    if (y1 === y2) return 0
    if (!y1) return -1
    if (!y2) return 1
    return y2 - y1
}

// unpublished has presedence
function sortReferenceInfoByPublished(refInfo1: ReferenceInfo, refInfo2: ReferenceInfo): number {
    const p1 = refInfo1.reference.published
    const p2 = refInfo2.reference.published
    if ((p1 && p2) || (!p1 && !p2)) return sortByPublishedYear(refInfo1, refInfo2)
    if (!p1) return -1
    return 1
}

// convert an array to an object, based on the key specified
// if key is non-existent in the object it is omitted.
// duplicated keys will be replaced by the last object encountered in the array.
function arrayToObjectMap<A>(array: Array<A>, key: string) {
    return array.reduce(
        (acc, nextValue) => (key in nextValue ? { ...acc, [nextValue[key]]: nextValue } : acc),
        {} as { [id: number]: A },
    )
}

export function referenceAnnotationByStatus({
    alleleState,
    references,
    annotationReferences,
    customAnnotationReferences,
}: ReferenceAnnotationProp): ReferenceStatus {
    if (!alleleState && !references) {
        return {
            pending: [],
            evaluated: [],
            not_relevant: [],
            ignored: [],
            missing: [],
        }
    }

    const referenceAssessmentsById = arrayToObjectMap<ReferenceAssessment>(
        alleleState.referenceAssessments,
        'referenceId',
    )

    const customisableAnnotationReferenceById = arrayToObjectMap<CustomAnnotationReference>(
        customAnnotationReferences,
        'id',
    )

    // Needs special handling of duplicated pubmedId's
    const annotationReferenceById = annotationReferences.reduce((accum, ref) => {
        if (ref.pubmedId) {
            if (ref.pubmedId in accum) {
                return {
                    ...accum,
                    [ref.pubmedId]: {
                        ...accum[ref.pubmedId],
                        source: `${accum[ref.pubmedId].source}, ${ref.source}`,
                    },
                }
            }
            return { ...accum, [ref.pubmedId]: ref }
        }

        if (ref.id) {
            if (ref.id in accum) {
                return {
                    ...accum,
                    [ref.id]: {
                        ...accum[ref.id],
                        source: `${accum[ref.id].source}, ${ref.source}`,
                    },
                }
            }
            return { ...accum, [ref.id]: ref }
        }

        return accum
    }, {} as { [id: number]: AnnotationReference })

    // closure used to simplify immutable update of reference status, i.e. returns a function
    // used to append a value to the array given by key, (pending, evaluated etc.)
    function updateReferenceStatus(refStatus: ReferenceStatus, refInfo: ReferenceInfo) {
        return (key: keyof ReferenceStatus) => ({
            ...refStatus,
            [key]: [refInfo, ...refStatus[key]],
        })
    }

    // classifying a reference based upon the annotation source and the assessment given.
    const referenceStatus = references.reduce(
        (referenceInfo, nextReference): ReferenceStatus => {
            // The source can either be an official source, or internal given by customisable annotation
            const source = nextReference.pubmedId
                ? annotationReferenceById[nextReference.pubmedId]?.source || undefined
                : customisableAnnotationReferenceById[nextReference.id]?.source || undefined
            if (nextReference.id in referenceAssessmentsById) {
                const referenceAssesment = referenceAssessmentsById[nextReference.id]
                // initialise the closure
                const appendReferenceInfoTo = updateReferenceStatus(referenceInfo, {
                    assessment: referenceAssesment,
                    reference: nextReference,
                    source,
                })

                // appending evaluated references by key using the closure
                if (referenceAssesment.evaluation.relevance === 'Ignore') {
                    return appendReferenceInfoTo('ignored')
                }
                if (referenceAssesment.evaluation.relevance === 'No') {
                    return appendReferenceInfoTo('not_relevant')
                }
                return appendReferenceInfoTo('evaluated')
            }

            // The annotated references may contain duplicates, checking if nextReference already
            // has been added to pending, if so, we ignore it.
            const ids = [...referenceInfo.pending.map((p) => p.reference.id), nextReference.id]
            if (ids.length !== new Set(ids).size) {
                return referenceInfo
            }

            // references without an assessment are added to pending
            return updateReferenceStatus(referenceInfo, { reference: nextReference, source })(
                'pending',
            )
        },
        {
            pending: [],
            evaluated: [],
            not_relevant: [],
            ignored: [],
            missing: [],
        } as ReferenceStatus,
    )

    const referenceByPubmedId = arrayToObjectMap(references, 'pubmedId')

    // collecting all annotations with a pubmedId that doesn't exist in the database
    const missing: { [id: string]: AnnotationReference } = annotationReferences.reduce(
        (accum, ref) => {
            if (ref.pubmedId) {
                if (!(ref.pubmedId in referenceByPubmedId)) {
                    return { ...accum, [ref.pubmedId]: ref }
                }
            }
            return accum
        },
        {} as { [id: string]: AnnotationReference },
    )

    return {
        pending: referenceStatus.pending.sort(sortReferenceInfoByPublished),
        evaluated: referenceStatus.evaluated.sort(sortReferenceInfoByPublished),
        not_relevant: referenceStatus.not_relevant.sort(sortReferenceInfoByPublished),
        ignored: referenceStatus.ignored.sort(sortReferenceInfoByPublished),
        missing: Object.values(missing),
    }
}
