import React, { useEffect, useMemo, useState } from 'react'
import { useSelector } from 'react-redux'

import Editor from 'components/editor/Editor'
import { Card } from 'components/elements/Card'
import Input from 'components/elements/Input'
import SectionBox from 'components/elements/SectionBox'
import Select from 'components/elements/Select'

import { selectConfig, selectRichTextTemplates } from 'store/config/configSlice'
import { selectCustomAnnotationsByAnnotationId } from 'store/interpretation/customAnnotationSlice'
import { RootState } from 'store/store'
import { selectCurrentUser } from 'store/users/usersSlice'

import useAnalysisInterpretation from 'views/interpretation/AnalysisInterpretationContext'
import { useInterpretation } from 'views/interpretation/InterpretationContext'
import useInterpretationStateContext from 'views/interpretation/InterpretationStateContext'
import ConfigurableAnnotation from 'views/interpretation/components/annotation/configurable/ConfigurableAnnotation'
import updateCustomAnnotations from 'views/interpretation/helpers/annotation'
import { AddCancelButtons } from 'views/interpretation/sections/external/AddCancelButtons'
import { DatabaseCommentList } from 'views/interpretation/sections/external/DatabaseCommentList'

export default function External() {
    const { alleleState, setAlleleState, selectedAlleleId, readOnly, save } =
        useInterpretationStateContext()

    const { presentationAlleles } = useInterpretation()
    const { reloadAlleles } = useAnalysisInterpretation()
    const { customAnnotationId } = presentationAlleles[selectedAlleleId]

    const customAnnotations = useSelector((state: RootState) =>
        customAnnotationId
            ? selectCustomAnnotationsByAnnotationId(state, {
                  customAnnotationId,
              })
            : undefined,
    )

    const [newEntry, setNewEntry] = useState({ database: '', comment: '' })
    const config = useSelector(selectConfig)
    const currentUser = useSelector(selectCurrentUser)
    const databases = config.custom_annotation.external

    const [comment, setComment] = useState(
        alleleState?.alleleAssessment.evaluation.external.comment,
    )

    const onCommentChange = (value: string) => {
        setAlleleState({
            alleleAssessment: {
                evaluation: {
                    external: {
                        comment: value,
                    },
                },
            },
        })
    }

    useEffect(() => {
        setComment(alleleState?.alleleAssessment.evaluation.external.comment)
    }, [selectedAlleleId])

    if (!currentUser) {
        throw new Error('No logged in user found.')
    }

    const sortedDatabases = useMemo(
        () =>
            [...databases]
                .filter((database) => !database.name.includes('(not in use)'))
                .sort((a, b) => a.name.localeCompare(b.name)),
        [databases],
    )

    return (
        <SectionBox title="External" color="ellapurple" initCollapsed={false}>
            <Editor
                key={`external-comment-${selectedAlleleId}`}
                id="external-comment"
                placeholder="External - comments"
                initialValue={comment}
                onBlur={onCommentChange}
                templates={useSelector(selectRichTextTemplates('classificationExternal'))}
                readOnly={readOnly}
            />
            <Card label="LSDB">
                {!readOnly && (
                    <div className="flex">
                        <Select
                            placeholder="Database"
                            options={sortedDatabases.map((database) => ({
                                label: database.name,
                                value: database.key,
                            }))}
                            onChange={(databaseKey) =>
                                setNewEntry({ ...newEntry, database: databaseKey })
                            }
                            selected={newEntry.database}
                        />

                        {newEntry.database && (
                            <>
                                <Input
                                    label="Comment"
                                    value={newEntry.comment}
                                    onChange={(event) =>
                                        setNewEntry({ ...newEntry, comment: event.target.value })
                                    }
                                />

                                <AddCancelButtons
                                    onOk={async () => {
                                        await updateCustomAnnotations({
                                            user: currentUser,
                                            alleleId: selectedAlleleId,
                                            customAnnotationsType: 'external',
                                            currentCustomAnnotations: customAnnotations,
                                            newAnnotationValues: {
                                                ...customAnnotations?.annotations.external,

                                                [newEntry.database]: newEntry.comment,
                                            },
                                        })
                                        await save()
                                        await reloadAlleles([selectedAlleleId])

                                        setNewEntry({ database: '', comment: '' })
                                    }}
                                    onCancel={() => setNewEntry({ database: '', comment: '' })}
                                />
                            </>
                        )}
                    </div>
                )}

                <DatabaseCommentList
                    alleleId={selectedAlleleId}
                    customAnnotationId={customAnnotationId || undefined}
                />
            </Card>
            <ConfigurableAnnotation section="external" />
        </SectionBox>
    )
}
