import { Config } from 'types/store/Config'
import { CustomAnnotation } from 'types/store/CustomAnnotation'

type ExternalDatabase = Config['custom_annotation']['external'][number]

const getDatabaseByKey = (databaseKey: string, databases: ExternalDatabase[]) =>
    databases.find((database) => database.key === databaseKey)

const getDatabaseName = (databaseKey: string, databases: ExternalDatabase[]) => {
    const foundDatabase = getDatabaseByKey(databaseKey, databases)

    return foundDatabase ? foundDatabase.name : databaseKey
}

export const parseAndSortCustomAnnotations = (
    externalCustomAnnotations: CustomAnnotation['annotations']['external'],
    databases: ExternalDatabase[],
) => {
    if (!externalCustomAnnotations) {
        return []
    }

    const parsedDatabaseComments = Object.entries(externalCustomAnnotations).reduce(
        (previous, [databaseKey, comment]) => [
            ...previous,
            { comment, name: getDatabaseName(databaseKey, databases), key: databaseKey },
        ],
        [] as { comment: string; name: string; key: string }[],
    )

    const sortedByName = parsedDatabaseComments.sort((a, b) => a.name.localeCompare(b.name))

    return sortedByName
}

export const removeDatabaseComment = (
    databaseKey: string,
    listOfDatabaseComments: Record<string, string>,
) =>
    Object.entries(listOfDatabaseComments).reduce((previous, [currentKey, currentComment]) => {
        if (currentKey === databaseKey) {
            return previous
        }
        return { ...previous, [currentKey]: currentComment }
    }, {} as Record<string, string>)
