import { TrashIcon, XMarkIcon } from '@heroicons/react/24/solid'
import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { selectConfig } from 'store/config/configSlice'
import {
    fetchCustomAnnotationsForAlleleId,
    selectCustomAnnotationsByAnnotationId,
} from 'store/interpretation/customAnnotationSlice'
import { RootState } from 'store/store'
import { selectCurrentUser } from 'store/users/usersSlice'

import updateCustomAnnotations from 'views/interpretation/helpers/annotation'
import {
    parseAndSortCustomAnnotations,
    removeDatabaseComment,
} from 'views/interpretation/sections/external/externalHelpers'

interface Props {
    alleleId: number
    customAnnotationId?: number
}

export function DatabaseCommentList({ alleleId, customAnnotationId }: Props) {
    const customAnnotations = useSelector(
        customAnnotationId
            ? (state: RootState) =>
                  selectCustomAnnotationsByAnnotationId(state, { customAnnotationId })
            : () => undefined,
    )

    const config = useSelector(selectConfig)
    const databases = config.custom_annotation.external
    const dispatch = useDispatch()
    const user = useSelector(selectCurrentUser)

    if (!user) {
        throw new Error('No logged in user found.')
    }

    const [databaseCommentToDelete, setDatabaseCommentToDelete] = useState<{
        databaseKey: string
        databaseName: string
    } | null>()

    const deleteComment = async (databaseKey: string) => {
        await updateCustomAnnotations({
            user,
            alleleId,
            customAnnotationsType: 'external',
            currentCustomAnnotations: customAnnotations,
            newAnnotationValues: {
                ...removeDatabaseComment(
                    databaseKey,
                    customAnnotations?.annotations.external ?? {},
                ),
            },
        })
        dispatch(fetchCustomAnnotationsForAlleleId(alleleId))
        setDatabaseCommentToDelete(null)
    }

    return (
        <table>
            <tbody>
                {customAnnotations?.annotations.external &&
                    parseAndSortCustomAnnotations(
                        customAnnotations.annotations.external,
                        databases,
                    ).map((databaseComment) => (
                        <tr key={databaseComment.key}>
                            <td className="cursor-pointer">
                                {databaseCommentToDelete?.databaseKey !== databaseComment.key ? (
                                    <div
                                        className="w-min p-2"
                                        onClick={() =>
                                            setDatabaseCommentToDelete({
                                                databaseKey: databaseComment.key,
                                                databaseName: databaseComment.name,
                                            })
                                        }
                                    >
                                        <TrashIcon height={20} />
                                    </div>
                                ) : (
                                    <div className="flex">
                                        <span
                                            className="text-ellared-dark flex items-center p-2"
                                            onClick={() =>
                                                databaseCommentToDelete &&
                                                deleteComment(databaseCommentToDelete.databaseKey)
                                            }
                                        >
                                            Delete <TrashIcon height={20} />
                                        </span>
                                        <div
                                            className="flex items-center p-2"
                                            onClick={() => setDatabaseCommentToDelete(null)}
                                        >
                                            <XMarkIcon height={20} />
                                            Cancel
                                        </div>
                                    </div>
                                )}
                            </td>
                            <td className="p-4">
                                {databaseComment.name} ({databaseComment.key})
                            </td>
                            <td className="p-4">{databaseComment.comment}</td>
                        </tr>
                    ))}
            </tbody>
        </table>
    )
}
