import React, { useRef, useState } from 'react'

import Tooltip from 'components/elements/Tooltip'

import { Annotation } from 'types/store/Annotation'

interface Props {
    transcriptKey: string
    annotation: Annotation
}

export function ConsequenceTranscript({ transcriptKey, annotation }: Props) {
    const tooltipAnchorElement = useRef<HTMLButtonElement>(null)
    const [showTooltip, setShowTooltip] = useState(false)

    const detailedTranscriptFound = annotation.transcripts?.find(
        (detailedTranscript) => detailedTranscript.transcript === transcriptKey,
    )
    if (!detailedTranscriptFound) {
        throw new Error(`Detailed transcript ${transcriptKey} not found.`)
    }

    return (
        <>
            <button
                onClick={() => setShowTooltip(true)}
                ref={tooltipAnchorElement}
                className="hover:text-ellablue-darker border-b border-dotted"
            >
                {detailedTranscriptFound.consequences
                    .join(', ')
                    .replace('_variant', '')
                    .replaceAll('_', ' ')}
            </button>

            {showTooltip && (
                <Tooltip
                    anchorElement={tooltipAnchorElement.current}
                    onClose={() => setShowTooltip(false)}
                >
                    <ul>
                        {annotation.transcripts?.map((transcript) => (
                            <li>
                                {transcript.transcript} ({transcript.symbol}):{' '}
                                {transcript.consequences.join(', ')}
                            </li>
                        ))}
                    </ul>
                </Tooltip>
            )}
        </>
    )
}
