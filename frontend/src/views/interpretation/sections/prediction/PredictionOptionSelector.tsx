import React from 'react'
import { useSelector } from 'react-redux'

import { RadioButtons } from 'components/elements/RadioButtons'

import { selectConfig } from 'store/config/configSlice'

import { CustomAnnotation } from 'types/store/CustomAnnotation'

type PredictionCustomAnnotations = Required<CustomAnnotation['annotations']>['prediction']

interface Props {
    values: PredictionCustomAnnotations
    propertyKey: keyof PredictionCustomAnnotations
    onChange: (newValues: PredictionCustomAnnotations) => void
    disabled?: boolean
}

export function PredictionOptionSelector({
    values,
    propertyKey,
    onChange,
    disabled = false,
}: Props) {
    const config = useSelector(selectConfig)
    const predictionConfigMap = {
        dnaConservation: 'dna_conservation',
        domain: 'domain',
        orthologConservation: 'ortholog_conservation',
        paralogConservation: 'paralog_conservation',
        repeat: 'repeat',
        spliceEffectManual: 'splice_Effect_manual',
    }
    const property = config.custom_annotation.prediction.find(
        (p) => p.key === predictionConfigMap[propertyKey],
    )

    if (!property) {
        throw new Error(`Property ${propertyKey} not found in config`)
    }

    const { options } = property

    return (
        <RadioButtons
            selectedButton={values[propertyKey]}
            onChange={(newValue) => onChange({ ...values, [propertyKey]: newValue })}
            direction="column"
            className="mb-4"
            disabled={disabled}
            nullable
            buttons={options.map(([label, key]) => ({
                label,
                key,
            }))}
        />
    )
}
