import {
    AlleleInterpretationContext,
    AnalysisInterpretationContext,
} from 'types/view/Interpretation'

import useAlleleInterpretation from 'views/interpretation/AlleleInterpretationContext'
import useAnalysisInterpretation from 'views/interpretation/AnalysisInterpretationContext'

export function useInterpretation():
    | (AnalysisInterpretationContext & { type: 'analysis' })
    | (AlleleInterpretationContext & { type: 'allele' }) {
    // Will return whatever type of interpretation context we're within
    // Either allele or interpretation context
    // We check if the context has keys (meaning it has been initialized),
    // and select context to return based on that
    const analysisInterpretationContext = useAnalysisInterpretation()
    const alleleInterpretationContext = useAlleleInterpretation()

    if (Object.keys(analysisInterpretationContext).length) {
        return { ...analysisInterpretationContext, type: 'analysis' }
    }
    if (Object.keys(alleleInterpretationContext).length) {
        return { ...alleleInterpretationContext, type: 'allele' }
    }
    throw Error('Not able to detect interpretation context')
}
