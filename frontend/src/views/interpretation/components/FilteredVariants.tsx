import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import API from 'app/API'

import Paginator from 'components/elements/Paginator'
import Separator from 'components/elements/Separator'
import Spinner from 'components/elements/Spinner'
import Table from 'components/elements/Table'

import { processAndStoreAlleleAssessment } from 'store/interpretation/alleleAssessmentSlice'
import { processAndStoreAlleleReport } from 'store/interpretation/alleleReportSlice'
import { processAndStoreAllele } from 'store/interpretation/alleleSlice'
import { RootState } from 'store/store'

import * as ApiTypes from 'types/api/Pydantic'
import { PresentationAllele, createAnalysisPresentationAlleles } from 'types/view/Interpretation'

import { nonNullable } from 'utils/general'

import useAnalysisInterpretationContext from 'views/interpretation/AnalysisInterpretationContext'
import useAnalysisInterpretationUIContext from 'views/interpretation/analysis/UIContext'
import {
    AlleleTableDataRow,
    selectTableData,
} from 'views/interpretation/helpers/alleleTable/alleleTable'
import { columnDefinitions } from 'views/interpretation/helpers/alleleTable/columnDefinitions'

const pageSize = 20

export default function FilteredVariants() {
    const [page, setPage] = React.useState(1)
    const [alleleIds, setAlleleIds] = React.useState<number[]>([])
    const {
        id: analysisId,
        filteredAlleles,
        filterConfigId,
        interpretation,
    } = useAnalysisInterpretationContext()
    const { genePanelName, genePanelVersion } = interpretation
    const { callerTypeMode } = useAnalysisInterpretationUIContext()

    const alleleIdsInPage = alleleIds.slice((page - 1) * pageSize, page * pageSize)
    const [alleleIdsInTable, setAlleleIdsInTable] = useState<number[] | undefined>()

    const dispatch = useDispatch()

    // TODO: can potentially be done outside useEffect.
    useEffect(() => {
        const excludedAllelesByFilter: { [filterName: string]: number[] } =
            filteredAlleles.excludedAllelesByCallerType[callerTypeMode]
        const excludedAlleleIds: number[] = Object.values(excludedAllelesByFilter).flat()

        API.AllelesByGene.get(excludedAlleleIds).then((result) => {
            const alleleIdsRes = result.data
                .sort((a, b) => a.symbol.localeCompare(b.symbol))
                .reduce((acc, curr) => [...acc, ...curr.allele_ids], [] as number[])
            // note: allele IDs seem to be non-unique here.
            // (testuser 4, analysis 1, allele ID 456)
            // As a workaround make them unique here
            setAlleleIds(Array.from(new Set(alleleIdsRes)))
        })
    }, [filteredAlleles.excludedAllelesByCallerType, callerTypeMode])

    const [presentationAlleles, setPresentationAlleles] = useState<
        Record<number, PresentationAllele>
    >({})

    // TODO: can potentially be done outside useEffect.
    useEffect(() => {
        setAlleleIdsInTable(undefined)
        API.AnalysisInterpretationAlleles.get(
            analysisId,
            interpretation.id,
            alleleIdsInPage,
            interpretation.status,
            filterConfigId,
        ).then(async ({ data: alleles }) => {
            // Ensure we have all the required data in the store
            const promises = [
                ...alleles.map((allele) => dispatch(processAndStoreAllele(allele))),
                ...alleles
                    .map((allele) => allele.allele_assessment)
                    .filter(nonNullable)
                    .map((alleleAssessment) =>
                        dispatch(
                            processAndStoreAlleleAssessment(
                                alleleAssessment as ApiTypes.AlleleAssessment,
                            ),
                        ),
                    ),
                ...alleles
                    .map((allele) => allele.allele_report)
                    .filter(nonNullable)
                    .map((alleleReport) => dispatch(processAndStoreAlleleReport(alleleReport))),
            ]
            setPresentationAlleles(createAnalysisPresentationAlleles(alleles))
            await Promise.all(promises)
            setAlleleIdsInTable(alleleIdsInPage)
        })
    }, [alleleIds, page])

    const tableData = useSelector((state: RootState) =>
        selectTableData(state, {
            presentationAlleles: Object.values(presentationAlleles) || [],
            genePanelName,
            genePanelVersion,
            filterConfigId,
            interpretationState: undefined,
        }),
    )

    const tableDataReady =
        alleleIdsInTable &&
        tableData.filter((row) => row.allele === undefined || row.annotation === undefined)
            .length === 0

    return (
        <div>
            <Separator label="Included variants" />

            <Separator label="Filtered variants" />
            <Paginator
                current={page}
                total={alleleIds.length}
                pageSize={pageSize}
                onChange={(p) => setPage(p)}
            />
            {tableDataReady ? (
                <Table<AlleleTableDataRow>
                    data={tableData}
                    columns={Object.values(columnDefinitions)}
                    classNames={{
                        table: 'w-full my-2 table-fixed',
                    }}
                />
            ) : (
                <Spinner />
            )}
        </div>
    )
}
