import get from 'lodash/get'
import React from 'react'

import Anchor from 'components/elements/Anchor'

import { ViewItem } from 'types/api/Pydantic'
import { ItemListSchema, ObjectItem } from 'types/store/AnnotationConfig'

interface ItemListTemplateProp {
    itemListData: any
    schema: ItemListSchema
}

class ItemProperty {
    readonly url?: string

    readonly text: string

    constructor(text: string, url?: string) {
        this.text = text
        this.url = url
    }
}

function itemListChildren(viewItem: ViewItem, subRawData: Object): Array<ItemProperty> {
    const valueType = viewItem.type || 'primitives'

    if (valueType === 'primitives') {
        return [new ItemProperty(subRawData.toString(), viewItem.url)]
    }

    if (valueType === 'objects') {
        const viewItemObj = viewItem as ObjectItem
        const subItem = get(viewItem, viewItemObj.key)
        if (subItem === null) {
            return []
        }
        if (!Array.isArray(subItem)) {
            return [new ItemProperty(subItem.toString(), viewItem.url)]
        }
        const subItems = subItem as Array<string>
        return subItems.map((l) => new ItemProperty(l, viewItem.url))
    }

    return []
}

function itemListFromSchema(items: ViewItem[], data: Object) {
    return items.reduce((accum, viewItem) => {
        if (viewItem.subsource) {
            const dataObject = get(data, viewItem.subsource)
            if (!dataObject) {
                return accum
            }
            return [...accum, ...itemListChildren(viewItem, dataObject)]
        }
        return [...accum, ...itemListChildren(viewItem, data)]
    }, [] as Array<ItemProperty>)
}

interface UrlProp {
    item: ItemProperty
}
function UrlOrText({ item }: UrlProp) {
    if (item.url) {
        return (
            <Anchor href={item.url} className="hover:text-ellablue-darkest underline">
                {item.text}
            </Anchor>
        )
    }
    return <span>{item.text}</span>
}

export function ItemListTemplate({ itemListData, schema }: ItemListTemplateProp) {
    const listOfProps = itemListFromSchema(schema.config.items, itemListData)
    return (
        <div>
            {listOfProps.map((itemProperty) => (
                <div key={itemProperty.text}>
                    <UrlOrText item={itemProperty} />
                </div>
            ))}
        </div>
    )
}
