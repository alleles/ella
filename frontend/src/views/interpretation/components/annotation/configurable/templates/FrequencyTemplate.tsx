import React from 'react'

import { Frequency } from 'types/store/Annotation'
import { FrequencyConfig, FrequencySchema } from 'types/store/AnnotationConfig'

import { numberFormat } from 'utils/formatting'

interface FrequencyTemplateProp {
    frequencyData: any
    schema: FrequencySchema
}

type FrequencyColumn = {
    name: string
    properties: Array<string>
}

export function FrequencyTemplate({ frequencyData, schema }: FrequencyTemplateProp) {
    const { config } = schema
    function frequencyRow(rowConfig: Record<string, string>, data: any): [string] {
        const tableRow = Object.entries(rowConfig).reduce((rowArray: any, [rowKey]) => {
            if (rowKey in data) {
                return [
                    ...rowArray,
                    numberFormat(data[rowKey], config.precision, config.scientified_treshold),
                ]
            }
            return [...rowArray, '-']
        }, [])

        return tableRow
    }

    function frequencyTable(data: Frequency): Array<FrequencyColumn> {
        const row: Record<string, string> = config.rows
        const tableData: Array<FrequencyColumn> = Object.entries(config.columns).reduce(
            (columnArray, [columnKey, value]) => {
                const columnLabel = value.replace(/_/g, ' ')
                if (columnKey in data) {
                    return [
                        ...columnArray,
                        { name: columnLabel, properties: frequencyRow(row, data[columnKey]) },
                    ]
                }
                return [...columnArray, { name: columnLabel, properties: [] }]
            },

            [
                // Inserting first row as the row labels
                {
                    name: config.key_column,
                    properties: Object.entries(row).map((r) => r[1]),
                },
            ],
        )
        return tableData
    }

    function prettyPrintFreqValue(index: number, freq: string) {
        if (freq === '-') return <span>&nbsp;</span>
        return <span>{freq}</span>
    }

    const tableData = frequencyTable(frequencyData)
    return (
        <div>
            <FailedFilterView frequencyData={frequencyData} />
            <div className="flex text-sm">
                {tableData.map((column, index) => (
                    <div
                        className="mr-4 font-semibold uppercase"
                        key={`${index.toString()}-col-${column.name}`}
                    >
                        {column.name}
                        <div className="flex-col font-normal uppercase">
                            {column.properties.map((freqValue, rowIndex) => (
                                <div key={`${rowIndex.toString()}-col-${column.name}`}>
                                    {prettyPrintFreqValue(index, freqValue)}
                                </div>
                            ))}
                        </div>
                    </div>
                ))}
            </div>
            {frequencyData.indications && (
                <IndicationsView
                    config={config}
                    indications={frequencyData.indications}
                    count={frequencyData.count}
                />
            )}
        </div>
    )
}

interface FrequencyFilterWarningProp {
    frequencyData: Frequency
}

function FailedFilterView({ frequencyData }: FrequencyFilterWarningProp) {
    if (frequencyData.filter) {
        const filterKeys: { [k: string]: string[] } = frequencyData.filter
        const failedFilters = Object.keys(filterKeys)
            .reduce((acc, filterKey) => [...acc, ...filterKeys[filterKey]], [] as Array<string>)
            .filter((filterValue) => filterValue !== 'PASS')

        if (failedFilters.length <= 0) return null
        return (
            <div className="bg-ellared-dark text-ellagray-100 rounded-sm p-2 text-sm">
                FILTER: {failedFilters.join(', ')}
            </div>
        )
    }
    return null
}

interface IndicationViewProp {
    count: Frequency['count']
    config: FrequencyConfig
    indications?: Frequency['indications']
}

function IndicationsView({ config, indications, count }: IndicationViewProp) {
    const threshold = config.indications?.threshold || Infinity

    function shouldShowIndications(key: string) {
        return !threshold || (count && count?.[key] < threshold)
    }

    if (!indications) return null

    return (
        <div className="text-sm">
            {Object.keys(indications)
                .filter(shouldShowIndications)
                .map((objKey) => (
                    <div key={`key-${objKey}`} className="pt-4">
                        {objKey} indications:
                        <div>
                            {Object.entries(indications[objKey])
                                .sort((a, b) => b[1] - a[1])
                                .map(([key, value]) => (
                                    <div key={key} className="grid grid-cols-3">
                                        <span>{`- ${key}: `}</span>
                                        <span>{value.toString()}</span>
                                    </div>
                                ))}
                        </div>
                    </div>
                ))}
        </div>
    )
}
