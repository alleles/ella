import React from 'react'

import { ClinvarDetailObject } from 'types/store/Annotation'

interface ClinvarDetailsTemplateProp {
    clinvarDetailsData: any
}

const NUM_STARS = {
    'no assertion criteria provided': 0,
    'no assertion provided': 0,
    'criteria provided, conflicting interpretations': 1,
    'criteria provided, single submitter': 1,
    'criteria provided, multiple submitters, no conflicts': 2,
    'reviewed by expert panel': 3,
    'practice guideline': 4,
}

type VariantDescription = keyof typeof NUM_STARS

function getStarClass(variantDescription: VariantDescription, index: number) {
    const numStars = NUM_STARS[variantDescription]
    if (numStars === undefined) {
        return 'unavailable'
    }

    return index < numStars ? 'fill' : 'none'
}

interface VariantDescriptionProp {
    variantDescription: VariantDescription
}

function ReviewStatusView({ variantDescription }: VariantDescriptionProp) {
    return (
        <span className="flex space-x-2">
            <span>Review status:</span>
            {[0, 1, 2, 3].map((nrOfStars) => (
                <svg
                    key={nrOfStars}
                    id="i-star"
                    fill={`${getStarClass(variantDescription, nrOfStars)}`}
                    viewBox="0 0 32 32"
                    width="16"
                    height="16"
                    stroke="currentcolor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="6.25%"
                >
                    <path d="M16 2 L20 12 30 12 22 19 25 30 16 23 7 30 10 19 2 12 12 12 Z" />
                </svg>
            ))}
            <span>&#40;{variantDescription}&#41;</span>
        </span>
    )
}

interface ClinvarDetailObjProp {
    clinvarObj: ClinvarDetailObject
    index: number
}

function sortByLastEvaluated(a: ClinvarDetailObject, b: ClinvarDetailObject) {
    const dateA = a.last_evaluated
    const dateB = b.last_evaluated
    if (dateA < dateB) return 1
    if (dateA > dateB) return -1
    return 0
}

function ClinvarDetailRow({ clinvarObj, index }: ClinvarDetailObjProp) {
    return (
        <div className="text-sm" key={`${clinvarObj.last_evaluated}-${index}`}>
            <p>
                {`${clinvarObj.last_evaluated} - ${
                    clinvarObj.clinical_significance_descr || 'not-specified'
                } - ${clinvarObj.traitnames || 'not-specified'}`}
            </p>
        </div>
    )
}
export function ClinvarDetailsTemplate({ clinvarDetailsData }: ClinvarDetailsTemplateProp) {
    return (
        <div>
            <div>
                <ReviewStatusView variantDescription={clinvarDetailsData.variant_description} />
            </div>
            <div className="pt-3">Submissions:</div>
            <div className="pl-2">
                {clinvarDetailsData.items
                    .filter((c) => c.rcv.startsWith('SCV'))
                    .sort(sortByLastEvaluated)
                    .map((clinvarObj, index) => ClinvarDetailRow({ clinvarObj, index }))}
            </div>
        </div>
    )
}
