import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'

import { RadioButtons } from 'components/elements/RadioButtons'
import { OkCancelModal } from 'components/elements/modals/OkCancelModal'

import { selectConfig } from 'store/config/configSlice'

import { AlleleInterpretation } from 'types/store/AlleleInterpretation'
import {
    AnalysisAlleleState,
    AnalysisInterpretation,
    AnalysisInterpretationState,
} from 'types/store/AnalysisInterpretation'
import { Config } from 'types/store/Config'

import { useInterpretation } from 'views/interpretation/InterpretationContext'
import useInterpretationStateContext from 'views/interpretation/InterpretationStateContext'

type InterpretationWorkflowOptions =
    | AlleleInterpretation['workflowStatus']
    | AnalysisInterpretation['workflowStatus']
    | 'Finalized'

interface Props {
    onCancel: () => void
}

export const getCanFinalize = ({
    type,
    config,
    workflowStatus,
    interpretationState,
}:
    | {
          type: 'allele'
          config: Config
          workflowStatus: AlleleInterpretation['workflowStatus']
          interpretationState: AlleleInterpretation['state']
      }
    | {
          type: 'analysis'
          config: Config
          workflowStatus: AnalysisInterpretation['workflowStatus']
          interpretationState: AnalysisInterpretation['state']
      }):
    | {
          canFinalize: true
          message: null
      }
    | { canFinalize: false; message: string } => {
    if (type === 'allele') {
        const requiredWorflows =
            config.user.user_config.workflows.allele?.finalize_requirements?.workflow_status

        if (requiredWorflows && !requiredWorflows.includes(workflowStatus)) {
            return {
                canFinalize: false,
                message: `Cannot finalize allele interpretation with workflow status ${workflowStatus}`,
            }
        }

        // Allele interpretations should never be finalized without an allele assessment
        const alleleState = Object.values(interpretationState.allele)[0]
        if (alleleState.alleleAssessmentId === null || !alleleState.reuseAlleleAssessment) {
            return {
                canFinalize: false,
                message: 'Cannot finalize allele interpretation without allele assessment',
            }
        }
        return { canFinalize: true, message: null }
    }

    // type analysis
    const requiredWorflows =
        config.user.user_config.workflows.analysis?.finalize_requirements?.workflow_status

    if (requiredWorflows && !requiredWorflows.includes(workflowStatus)) {
        return {
            canFinalize: false,
            message: `Cannot finalize analysis interpretation with workflow status ${workflowStatus}`,
        }
    }

    const isUnclassified = (alleleState: AnalysisAlleleState) =>
        (alleleState.alleleAssessmentId === null || !alleleState.reuseAlleleAssessment) && // Lacking allele assessment
        alleleState?.analysis.verification !== 'technical' && // Not technical
        !alleleState?.analysis.notrelevant // Not not relevant

    const allowUnclassified =
        config.user.user_config.workflows.analysis?.finalize_requirements.allow_unclassified ?? true
    if (!allowUnclassified) {
        const hasUnclassifiedAlleles = Object.values(interpretationState.allele).some(
            isUnclassified,
        )
        if (hasUnclassifiedAlleles) {
            return {
                canFinalize: false,
                message: `Cannot finalize analysis interpretation with unclassified alleles`,
            }
        }
    }

    // All checks passed
    return { canFinalize: true, message: null }
}

export function FinishModal({ onCancel }: Props) {
    const { interpretationState } = useInterpretationStateContext()
    const { interpretation, type, changeWorkflowStatus, finalizeWorkflow } = useInterpretation()
    const config = useSelector(selectConfig)

    const navigate = useNavigate()
    const workflowStatuses =
        type === 'allele'
            ? ['Interpretation', 'Review', 'Finalized']
            : ['Not ready', 'Interpretation', 'Review', 'Medical review', 'Finalized']

    const [selectedWorkflowStatus, setSelectedWorkflowStatus] =
        useState<InterpretationWorkflowOptions>(interpretation.workflowStatus)

    const buttons = workflowStatuses.map((v) => ({ key: v, label: v }))
    const { canFinalize, message } = getCanFinalize({
        type: type as any,
        config,
        workflowStatus: interpretation.workflowStatus,
        interpretationState: interpretationState as AnalysisInterpretation['state'],
    })

    return (
        <OkCancelModal
            onCancel={onCancel}
            okDisabled={!canFinalize && selectedWorkflowStatus === 'Finalized'}
            onOk={async () => {
                if (selectedWorkflowStatus === 'Finalized') {
                    if (!canFinalize) {
                        throw new Error("Can't finalize. Ok button should be disabled")
                    }
                    await finalizeWorkflow(interpretationState as AnalysisInterpretationState) // Type as analysis, as this is a superset of AlleleInterpretationState
                } else {
                    await changeWorkflowStatus(selectedWorkflowStatus as any, interpretationState) // Hard to type this (it depends on the type of interpretation)
                }
                if (type === 'allele') {
                    navigate('/overview/variants')
                } else {
                    navigate('/overview/analyses')
                }
            }}
        >
            <p className="mb-4">
                Send from <b>{interpretation.workflowStatus}</b> to <b>{selectedWorkflowStatus}</b>
            </p>

            <div className="flex">
                <RadioButtons
                    direction="row"
                    buttons={buttons}
                    selectedButton={selectedWorkflowStatus}
                    className="gap-1"
                    onChange={(e) => setSelectedWorkflowStatus(e)}
                    disabled={false}
                />
            </div>
            {selectedWorkflowStatus === 'Finalized' && !canFinalize && (
                <p className="text-ellayellow-dark w-full text-center text-sm font-normal">
                    {message}
                </p>
            )}
        </OkCancelModal>
    )
}
