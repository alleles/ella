import React, { useRef, useState } from 'react'
import { useSelector } from 'react-redux'

import Tooltip from 'components/elements/Tooltip'

import { selectHGVSgByAlleleId } from 'store/interpretation/alleleSlice'
import { selectAnnotationByAnnotationId } from 'store/interpretation/annotationSlice'
import { selectAlleleDisplay } from 'store/selectors/genePanelAnnotation'
import { RootState } from 'store/store'

import { useInterpretation } from 'views/interpretation/InterpretationContext'
import useInterpretationStateContext from 'views/interpretation/InterpretationStateContext'

export default function AlleleBar() {
    const { selectedAlleleId } = useInterpretationStateContext()

    const { interpretation, presentationAlleles } = useInterpretation()
    const { annotationId } = presentationAlleles[selectedAlleleId]
    const alleleDisplay = useSelector((state: RootState) =>
        selectAlleleDisplay(state, {
            alleleId: selectedAlleleId,
            annotationId,
            genePanelName: interpretation.genePanelName,
            genePanelVersion: interpretation.genePanelVersion,
        }),
    )

    const hgvsg = useSelector((state: RootState) =>
        selectHGVSgByAlleleId(state, { alleleId: selectedAlleleId }),
    )

    // Tooltip content
    const annotations = useSelector((state: RootState) =>
        selectAnnotationByAnnotationId(state, { annotationId }),
    )
    const popupConsequences = annotations?.transcripts.map(
        (t) => `${t.transcript}(${t.symbol}): ${t.consequences.join(', ')}`,
    )
    const popupHGVSC = annotations?.transcripts.map(
        (t) => `${t.transcript}(${t.symbol}): ${t.HGVScShort || ''}`,
    )
    const consequencePopupRef = useRef<HTMLDivElement>(null)
    const [showConsequencesPopup, setShowConsequencesPopup] = useState(false)
    const HGVSCPopupRef = useRef<HTMLDivElement>(null)
    const [showHGVSCPopup, setShowHGVSCPopup] = useState(false)

    return (
        <div className="overflow-y-show mt-4 flex h-[88.5%] flex-col justify-between px-2">
            <div className="max-h-16 overflow-y-auto">
                {alleleDisplay.map((item, idx) => (
                    <div
                        key={JSON.stringify(item)}
                        className="bg-ellablue-lighter grid grid-cols-6 text-sm"
                    >
                        <span>{item.inheritance}</span>
                        <span>{`${item.geneSymbol} (${item.exonIntron})`}</span>
                        <div
                            className="cursor-pointer hover:underline"
                            ref={HGVSCPopupRef}
                            onClick={() => setShowHGVSCPopup(!showHGVSCPopup)}
                        >
                            {`${item.transcript} ${item.hgvsc ? item.hgvsc : ''}`}
                            {showHGVSCPopup && (
                                <Tooltip
                                    anchorElement={HGVSCPopupRef.current}
                                    onClose={() => {
                                        setShowHGVSCPopup(false)
                                    }}
                                    placement="bottom"
                                >
                                    <div className="font-bold uppercase">HGVSc</div>
                                    <ul>
                                        {popupHGVSC.map((c) => (
                                            <li key={c}>{c}</li>
                                        ))}
                                    </ul>
                                </Tooltip>
                            )}
                        </div>
                        <div
                            className="cursor-pointer justify-self-center hover:underline"
                            ref={consequencePopupRef}
                            onClick={() => setShowConsequencesPopup(!showConsequencesPopup)}
                        >
                            {item.consequence}
                            {showConsequencesPopup && (
                                <Tooltip
                                    anchorElement={consequencePopupRef.current}
                                    onClose={() => {
                                        setShowConsequencesPopup(false)
                                    }}
                                    placement="bottom"
                                >
                                    <div className="font-bold uppercase">Consequence</div>
                                    <ul>
                                        {popupConsequences.map((c) => (
                                            <li key={c}>{c}</li>
                                        ))}
                                    </ul>
                                </Tooltip>
                            )}
                        </div>
                        <span className="justify-self-end">{idx === 0 ? hgvsg : ''}</span>
                    </div>
                ))}
            </div>
        </div>
    )
}
