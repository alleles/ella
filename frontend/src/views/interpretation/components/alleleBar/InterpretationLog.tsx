import { Popover } from '@headlessui/react'
import { ChevronDownIcon } from '@heroicons/react/24/solid'
import { format } from 'date-fns'
import React, { ReactNode, useState } from 'react'
import { useSelector } from 'react-redux'

import Editor from 'components/editor/Editor'
import Button from 'components/elements/Button'
import ConfirmableAction from 'components/elements/ConfirmableAction'
import Input from 'components/elements/Input'
import Toggle from 'components/elements/Toggle'
import FadeTransition from 'components/transitions/FadeTransition'

import { RootState } from 'store/store'
import { selectUserById } from 'store/users/usersSlice'

import { InterpretationLogItem as InterpretationLogItemType } from 'types/store/InterpretationLog'

import { useInterpretation } from 'views/interpretation/InterpretationContext'

const Priority = {
    1: 'Normal',
    2: 'High',
    3: 'Urgent',
}

function InterpretationLogItem({ item }: { item: InterpretationLogItemType }) {
    const userId = item.userId || 0
    const dateCreated = new Date(item.dateCreated || '')
    const [updatedMessage, setUpdatedMessage] = useState(item.message || '')
    const [editing, setEditing] = useState(false)
    const messageUpdated = updatedMessage !== item.message
    const { updateMessage, deleteMessage } = useInterpretation()
    const {
        id,
        priority,
        reviewComment,
        warningCleared,
        message,
        editable,
        alleleAssessment,
        alleleReport,
    } = item

    const userFullname = useSelector(
        (state: RootState) => selectUserById(state, { userId })?.fullName,
    )

    let content: ReactNode = null

    if (reviewComment) {
        content = (
            <div>
                <span className="text-ellagray-700">Overview comment changed to </span>
                <span className="font-semibold">{reviewComment}</span>
            </div>
        )
    } else if (priority) {
        content = (
            <div>
                <span className="text-ellagray-700"> Priority changed to </span>
                <span className="font-semibold">{Priority[priority]}</span>
            </div>
        )
    } else if (warningCleared === true) {
        content = (
            <div>
                <span className="text-ellagray-700">Warning cleared</span>
            </div>
        )
    } else if (warningCleared === false) {
        content = (
            <div>
                <span className="text-ellagray-700">Warning reinstated</span>
            </div>
        )
    } else if (alleleAssessment && alleleAssessment.hgvsc && alleleAssessment.classification) {
        const { hgvsc, classification, previousClassification } = alleleAssessment
        content = (
            <div>
                <span className="text-ellagray-700">
                    {`Updated classification ${
                        alleleReport?.alleleId ? 'and variant report' : ''
                    } for `}
                </span>
                <span className="font-bold">{`${hgvsc} `}</span>
                <span className="font-bold">{`(Class ${
                    previousClassification || ''
                }→${classification})`}</span>
            </div>
        )
    } else if (alleleReport && alleleReport.hgvsc) {
        const { hgvsc } = alleleReport
        content = (
            <div>
                <span className="text-ellagray-700">Updated variant report for</span>{' '}
                <span className="font-bold">{`${hgvsc}`}</span>
            </div>
        )
    } else if (message) {
        content = (
            <div className="flex flex-row gap-2">
                <div className="flex w-full flex-col">
                    <Editor
                        id={`msg-${format(dateCreated, 'yyyyMMddHHMMSS')}`}
                        readOnly={!editable}
                        placeholder="Message"
                        initialValue={message}
                        onBlur={(value) => {
                            setUpdatedMessage(value)
                            setEditing(false)
                        }}
                        onFocus={() => {
                            setEditing(true)
                        }}
                    />
                </div>
                <div className="pr-2">
                    {editing || messageUpdated ? (
                        <Button onClick={() => updateMessage(id, updatedMessage)}>Update</Button>
                    ) : (
                        <ConfirmableAction onConfirm={() => deleteMessage(id)}>
                            Delete
                        </ConfirmableAction>
                    )}
                </div>
            </div>
        )
    }

    return (
        <li className="pb-2">
            <div className="text-ellagray-500 text-sm italic">
                <span>{userFullname} </span>
                <span>{format(dateCreated, 'yyyy-MM-dd HH:MM')}</span>
            </div>
            {content}
        </li>
    )
}

export default function InterpretationLog() {
    const [newOverviewComment, setNewOverviewComment] = useState('')
    const [newMessage, setNewMessage] = useState('')
    const [editing, setEditing] = useState(false)
    const { updateInterpretationLog, interpretationLogItems } = useInterpretation()

    const overviewComment =
        [...interpretationLogItems].reverse().find((item) => item.reviewComment)?.reviewComment ??
        ''
    const priority =
        [...interpretationLogItems].reverse().find((item) => item.priority)?.priority ?? 1

    const [messagesOnly, setMessagesOnly] = useState(true)

    const items = messagesOnly
        ? interpretationLogItems.filter((item) => item.message).reverse()
        : interpretationLogItems.slice().reverse()

    return (
        <div>
            <Popover className="bg-ellablue-darkest hover:bg-ellablue-darker flex h-full w-full items-center rounded-md px-2 shadow-sm hover:shadow-md">
                {({ open }) => (
                    <>
                        <Popover.Button className="flex px-2 text-sm font-medium tracking-wide text-white">
                            Worklog
                            <ChevronDownIcon className="ml-2 -mr-1 h-5 w-5" />
                        </Popover.Button>
                        {open && (
                            <FadeTransition>
                                <Popover.Panel className="absolute top-10 right-2 mt-3 w-1/3 rounded-md bg-white py-4 px-4 shadow-lg ring-1 ring-black/5">
                                    <div className="my-2 flex flex-row gap-2">
                                        <Input
                                            name="overview-comment"
                                            label="Overview"
                                            placeholder="Overview comment"
                                            value={newOverviewComment || overviewComment}
                                            onChange={(e) => setNewOverviewComment(e.target.value)}
                                        />
                                        <div className="w-20 shrink">
                                            <Button
                                                onClick={() =>
                                                    updateInterpretationLog({
                                                        review_comment: newOverviewComment,
                                                    })
                                                }
                                                disabled={
                                                    !newOverviewComment ||
                                                    overviewComment === newOverviewComment
                                                }
                                            >
                                                Save
                                            </Button>
                                        </div>
                                    </div>
                                    <div className="my-2 flex flex-row gap-2 ">
                                        <div className=" w-full">
                                            <Editor
                                                // small hack to make sure the value is reset in uncontrolled editor
                                                key={newMessage}
                                                id="new-worklog-message"
                                                placeholder="New message"
                                                initialValue={newMessage}
                                                onBlur={(msg) => {
                                                    setNewMessage(msg)
                                                    setEditing(false)
                                                }}
                                                readOnly={false}
                                                onFocus={() => {
                                                    setEditing(true)
                                                }}
                                            />
                                        </div>
                                        <div className="w-20 shrink">
                                            <Button
                                                disabled={!editing && !newMessage}
                                                onClick={() => {
                                                    updateInterpretationLog({ message: newMessage })
                                                    setNewMessage('')
                                                }}
                                            >
                                                Add
                                            </Button>
                                        </div>
                                    </div>
                                    <div className="my-2 flex">
                                        <div className="text-ellagray-700 mr-2 flex items-center">
                                            Priority
                                        </div>
                                        <select
                                            id="priority"
                                            name="priority"
                                            className="border-ellagray focus:border-ellablue focus:ring-ellablue block w-full max-w-xs rounded-md text-sm shadow-sm"
                                            value={priority.toString()}
                                            onChange={(e) => {
                                                updateInterpretationLog({
                                                    priority: parseInt(e.target.value, 10),
                                                })
                                            }}
                                        >
                                            {Object.entries(Priority).map(([v, k]) => (
                                                <option key={k} value={v}>
                                                    {k}
                                                </option>
                                            ))}
                                        </select>
                                    </div>
                                    <div className="text-ellagray-700 my-2 flex flex-row justify-between text-base font-bold">
                                        <div>Events</div>
                                        <Toggle
                                            name="interpretation-log-messages-only"
                                            label="Messages only"
                                            checked={messagesOnly}
                                            onChange={setMessagesOnly}
                                        />
                                    </div>
                                    <div className="max-h-192 overflow-y-scroll">
                                        <ul id="worklog-messages">
                                            {items.map((item) => (
                                                <InterpretationLogItem key={item.id} item={item} />
                                            ))}
                                        </ul>
                                    </div>
                                </Popover.Panel>
                            </FadeTransition>
                        )}
                    </>
                )}
            </Popover>
        </div>
    )
}
