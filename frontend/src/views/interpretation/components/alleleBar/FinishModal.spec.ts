import {
    AlleleFinalizeRequirementsConfig,
    AnalysisFinalizeRequirementsConfig,
} from 'types/api/Pydantic'
import { AlleleInterpretation } from 'types/store/AlleleInterpretation'
import { AnalysisInterpretation } from 'types/store/AnalysisInterpretation'
import { Config } from 'types/store/Config'

import { DeepPartial } from 'utils/types'

import { getCanFinalize } from 'views/interpretation/components/alleleBar/FinishModal'

const buildConfig = (
    alleleFinalizeRequirements?: AlleleFinalizeRequirementsConfig,
    analysisFinalizeRequirements?: AnalysisFinalizeRequirementsConfig,
): DeepPartial<Config> => ({
    user: {
        user_config: {
            workflows: {
                allele: {
                    finalize_requirements: alleleFinalizeRequirements,
                },
                analysis: {
                    finalize_requirements: analysisFinalizeRequirements,
                },
            },
        },
    },
})

describe('can finalize allele interpretation', () => {
    it('allele interpretation in different workflow states', () => {
        const config = buildConfig({ workflow_status: ['Review'] }) as Config

        const interpretation = {
            state: {
                allele: {
                    1: {
                        alleleAssessmentId: 1,
                        reuseAlleleAssessment: true,
                    },
                },
            },
            workflowStatus: 'Interpretation',
        } as DeepPartial<AlleleInterpretation> as AlleleInterpretation

        expect(
            getCanFinalize({
                type: 'allele',
                config,
                workflowStatus: interpretation.workflowStatus,
                interpretationState: interpretation.state as AlleleInterpretation['state'],
            }),
        ).toStrictEqual({
            canFinalize: false,
            message: 'Cannot finalize allele interpretation with workflow status Interpretation',
        })

        // Change to accepted workflow
        interpretation.workflowStatus = 'Review'
        expect(
            getCanFinalize({
                type: 'allele',
                config,
                workflowStatus: interpretation.workflowStatus,
                interpretationState: interpretation.state as AlleleInterpretation['state'],
            }),
        ).toStrictEqual({
            canFinalize: true,
            message: null,
        })
    })

    it('allele interpretation with and without assessment', () => {
        const config = buildConfig({
            workflow_status: ['Interpretation', 'Review'],
        }) as Config

        const interpretation = {
            state: {
                allele: {
                    1: {},
                },
            },
            workflowStatus: 'Review',
        } as DeepPartial<AlleleInterpretation> as AlleleInterpretation

        // No assessment
        interpretation.state.allele[1].alleleAssessmentId = null
        interpretation.state.allele[1].reuseAlleleAssessment = false

        expect(
            getCanFinalize({
                type: 'allele',
                config,
                workflowStatus: interpretation.workflowStatus,
                interpretationState: interpretation.state as AlleleInterpretation['state'],
            }),
        ).toStrictEqual({
            canFinalize: false,
            message: 'Cannot finalize allele interpretation without allele assessment',
        })

        // Assessment not reused
        interpretation.state.allele[1].alleleAssessmentId = 1
        interpretation.state.allele[1].reuseAlleleAssessment = false

        expect(
            getCanFinalize({
                type: 'allele',
                config,
                workflowStatus: interpretation.workflowStatus,
                interpretationState: interpretation.state as AlleleInterpretation['state'],
            }),
        ).toStrictEqual({
            canFinalize: false,
            message: 'Cannot finalize allele interpretation without allele assessment',
        })

        // Assessment exists and is reused
        interpretation.state.allele[1].alleleAssessmentId = 1
        interpretation.state.allele[1].reuseAlleleAssessment = true

        expect(
            getCanFinalize({
                type: 'allele',
                config,
                workflowStatus: interpretation.workflowStatus,
                interpretationState: interpretation.state as AlleleInterpretation['state'],
            }),
        ).toStrictEqual({
            canFinalize: true,
            message: null,
        })
    })
})

describe('can finalize analysis interpretation', () => {
    it('analysis interpretation in different workflow states', () => {
        const config = buildConfig(undefined, {
            workflow_status: ['Review', 'Medical review'],
        }) as Config
        const interpretation = {
            state: {
                allele: {},
            },
            workflowStatus: 'Interpretation',
        } as DeepPartial<AnalysisInterpretation> as AnalysisInterpretation

        expect(
            getCanFinalize({
                type: 'analysis',
                config,
                workflowStatus: interpretation.workflowStatus,
                interpretationState: interpretation.state as AnalysisInterpretation['state'],
            }),
        ).toStrictEqual({
            canFinalize: false,
            message: 'Cannot finalize analysis interpretation with workflow status Interpretation',
        })

        // Change to accepted workflow
        interpretation.workflowStatus = 'Medical review'

        expect(
            getCanFinalize({
                type: 'analysis',
                config,
                workflowStatus: interpretation.workflowStatus,
                interpretationState: interpretation.state as AnalysisInterpretation['state'],
            }),
        ).toStrictEqual({
            canFinalize: true,
            message: null,
        })
    })
    it('analysis interpretation with assessments', () => {
        const config = buildConfig(undefined, {
            allow_unclassified: false,
        }) as Config

        // All valid assessments
        const interpretation = {
            state: {
                allele: {
                    1: {
                        alleleAssessmentId: 1,
                        reuseAlleleAssessment: true,
                        analysis: {
                            notrelevant: false,
                            verification: null,
                        },
                    },
                    2: {
                        alleleAssessmentId: 1,
                        reuseAlleleAssessment: true,
                        analysis: {
                            notrelevant: false,
                            verification: null,
                        },
                    },
                },
            },
            workflowStatus: 'Review',
        } as DeepPartial<AnalysisInterpretation> as AnalysisInterpretation

        expect(
            getCanFinalize({
                type: 'analysis',
                config,
                workflowStatus: interpretation.workflowStatus,
                interpretationState: interpretation.state as AnalysisInterpretation['state'],
            }),
        ).toStrictEqual({
            canFinalize: true,
            message: null,
        })

        // Technical / not relevant shouldn't matter
        interpretation.state.allele[1].analysis.notrelevant = true
        interpretation.state.allele[2].analysis.verification = 'technical'

        expect(
            getCanFinalize({
                type: 'analysis',
                config,
                workflowStatus: interpretation.workflowStatus,
                interpretationState: interpretation.state as AnalysisInterpretation['state'],
            }),
        ).toStrictEqual({
            canFinalize: true,
            message: null,
        })
    })

    it('analysis interpretation without assessments', () => {
        const config = buildConfig(undefined, {
            allow_unclassified: false,
        }) as Config

        // All valid assessments
        const interpretation = {
            state: {
                allele: {
                    1: {
                        alleleAssessmentId: 1,
                        reuseAlleleAssessment: true,
                        analysis: {
                            notrelevant: false,
                            verification: null,
                        },
                    },
                    2: {
                        alleleAssessmentId: 1,
                        reuseAlleleAssessment: true,
                        analysis: {
                            notrelevant: false,
                            verification: null,
                        },
                    },
                },
            },
            workflowStatus: 'Review',
        } as DeepPartial<AnalysisInterpretation> as AnalysisInterpretation

        // One allele assessment is missing
        interpretation.state.allele[1].alleleAssessmentId = null
        expect(
            getCanFinalize({
                type: 'analysis',
                config,
                workflowStatus: interpretation.workflowStatus,
                interpretationState: interpretation.state as AnalysisInterpretation['state'],
            }),
        ).toStrictEqual({
            canFinalize: false,
            message: 'Cannot finalize analysis interpretation with unclassified alleles',
        })

        // One allele assessment is not reused
        interpretation.state.allele[1].alleleAssessmentId = 1
        interpretation.state.allele[1].reuseAlleleAssessment = true
        interpretation.state.allele[2].alleleAssessmentId = 2
        interpretation.state.allele[2].reuseAlleleAssessment = false

        expect(
            getCanFinalize({
                type: 'analysis',
                config,
                workflowStatus: interpretation.workflowStatus,
                interpretationState: interpretation.state as AnalysisInterpretation['state'],
            }),
        ).toStrictEqual({
            canFinalize: false,
            message: 'Cannot finalize analysis interpretation with unclassified alleles',
        })

        // No allele assessments, but variants are not relevant or technical
        interpretation.state.allele[1].alleleAssessmentId = null
        interpretation.state.allele[1].reuseAlleleAssessment = false
        interpretation.state.allele[2].alleleAssessmentId = null
        interpretation.state.allele[2].reuseAlleleAssessment = false
        interpretation.state.allele[1].analysis.notrelevant = true
        interpretation.state.allele[2].analysis.verification = 'technical'

        expect(
            getCanFinalize({
                type: 'analysis',
                config,
                workflowStatus: interpretation.workflowStatus,
                interpretationState: interpretation.state as AnalysisInterpretation['state'],
            }),
        ).toStrictEqual({
            canFinalize: true,
            message: null,
        })

        // No allele assessments, and allow unclassified set to true
        interpretation.state.allele[1].alleleAssessmentId = null
        interpretation.state.allele[1].reuseAlleleAssessment = false
        interpretation.state.allele[2].alleleAssessmentId = null
        interpretation.state.allele[2].reuseAlleleAssessment = false
        interpretation.state.allele[1].analysis.notrelevant = false
        interpretation.state.allele[2].analysis.verification = null
        config.user.user_config.workflows.analysis!.finalize_requirements.allow_unclassified = true

        expect(
            getCanFinalize({
                type: 'analysis',
                config,
                workflowStatus: interpretation.workflowStatus,
                interpretationState: interpretation.state as AnalysisInterpretation['state'],
            }),
        ).toStrictEqual({
            canFinalize: true,
            message: null,
        })
    })
})
