import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import Menu from 'components/elements/DropDownMenu'
import Tab from 'components/elements/Tab'

import { selectAcmgCodesByCategory } from 'store/config/configSlice'

import { AcmgCode, mapFromApi } from 'types/store/Acmg'

import useInterpretationStateContext from 'views/interpretation/InterpretationStateContext'
import { AcmgChip } from 'views/interpretation/components/acmg/AcmgChip'
import { AcmgHelper } from 'views/interpretation/helpers/acmg/AcmgHelper'

export default function AcmgDropdown() {
    const dispatch = useDispatch()
    const { alleleState, setAlleleState } = useInterpretationStateContext()

    const acmgCodes = useSelector(selectAcmgCodesByCategory)

    const [selectedAcmgCode, setSelectedAcmgCode] = useState<AcmgCode | null>(null)

    const onAddAcmg = (code) =>
        AcmgHelper.addAcmgCode({
            code,
            assessment: alleleState.alleleAssessment,
            setAlleleState,
            dispatch,
            source: 'custom',
        })

    const showAcmgList = (codes: string[]) => (
        <>
            {codes.map((code) =>
                selectedAcmgCode &&
                AcmgHelper.getCodeBase(selectedAcmgCode?.code ?? '') ===
                    AcmgHelper.getCodeBase(code) ? (
                    <AcmgChip
                        code={selectedAcmgCode}
                        key={code}
                        onAdd={(codeToAdd) => {
                            onAddAcmg(codeToAdd)
                            setSelectedAcmgCode(null)
                        }}
                        onStrengthChange={(newCode) => setSelectedAcmgCode({ ...newCode })}
                        onRichTextChange={(newText) => {
                            setSelectedAcmgCode({
                                ...selectedAcmgCode,
                                comment: newText,
                            })
                        }}
                    />
                ) : (
                    <AcmgChip
                        key={code}
                        code={mapFromApi({ code })}
                        onClick={() => {
                            setSelectedAcmgCode(mapFromApi({ code }))
                        }}
                    />
                ),
            )}
        </>
    )

    return (
        <Menu title="Add ACMG">
            <Tab.Group>
                <Tab.List>
                    <Tab>Pathogenic</Tab>
                    <Tab>Benign</Tab>
                    <Tab>Other</Tab>
                </Tab.List>
                <Tab.Panels>
                    <Tab.Panel>{showAcmgList(acmgCodes.pathogenic)}</Tab.Panel>
                    <Tab.Panel>{showAcmgList(acmgCodes.benign)} </Tab.Panel>
                    <Tab.Panel>{showAcmgList(acmgCodes.other)} </Tab.Panel>
                </Tab.Panels>
            </Tab.Group>
        </Menu>
    )
}
