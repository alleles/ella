import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'

import API from 'app/API'

import Button from 'components/elements/Button'
import { Card } from 'components/elements/Card'
import Spinner from 'components/elements/Spinner'

import { AcmgCode, mapFromApi } from 'types/store/Acmg'

import { useInterpretation } from 'views/interpretation/InterpretationContext'
import useInterpretationStateContext from 'views/interpretation/InterpretationStateContext'
import { AcmgChip } from 'views/interpretation/components/acmg/AcmgChip'
import { AcmgHelper } from 'views/interpretation/helpers/acmg/AcmgHelper'

export default function Acmg({
    onSuggestedClassChange,
    disabled,
}: {
    disabled: boolean
    onSuggestedClassChange: (newClass: string) => void
}) {
    const { alleleState, setAlleleState, selectedAlleleId } = useInterpretationStateContext()

    const { presentationAlleles, interpretation } = useInterpretation()
    const { annotationId, customAnnotationId } = presentationAlleles[selectedAlleleId]
    const { genePanelName, genePanelVersion } = interpretation

    const [showReqs, setShowReqs] = useState(false)
    const [loading, setLoading] = useState(true)

    const dispatch = useDispatch()

    useEffect(() => {
        API.AcmgSuggestions.post(genePanelName, genePanelVersion, [selectedAlleleId], [])
            .then((response) => response.data)
            .then((response) => response[selectedAlleleId].codes.map(mapFromApi))
            .then((response) => {
                setAlleleState({
                    alleleAssessment: {
                        evaluation: {
                            acmg: {
                                suggested: response,
                            },
                        },
                    },
                })
                setLoading(false)
            })
    }, [customAnnotationId, annotationId, selectedAlleleId])

    // TODO: can potentially be done outside useEffect.
    useEffect(() => {
        const assessment = alleleState.alleleAssessment
        if (AcmgHelper.getIncludedAcmgCodes(assessment).length === 0) {
            onSuggestedClassChange('')
        } else {
            API.AcmgClassificationSuggestion.get(
                AcmgHelper.getIncludedAcmgCodes(assessment)
                    .map((code) => code.code)
                    .join(','),
            )
                .then((response) => response.data)
                .then((response) => onSuggestedClassChange(response.class.toString()))
        }
    }, [alleleState.alleleAssessment.evaluation.acmg.included])

    const removeAcmgCode = (code: AcmgCode) => {
        const includedCodes = [...AcmgHelper.getIncludedAcmgCodes(alleleState.alleleAssessment)]

        const codesWithOneRemoved = includedCodes.filter(
            (includedCode) => includedCode.code !== code.code,
        )

        setAlleleState({
            alleleAssessment: {
                evaluation: {
                    acmg: {
                        included: codesWithOneRemoved,
                    },
                },
            },
        })
    }

    const updateCodeComment = (newValue: string, code: AcmgCode) => {
        const includedCodes = AcmgHelper.getIncludedAcmgCodes(alleleState.alleleAssessment)

        const codesWithUpdatedComment = includedCodes.reduce((prev: AcmgCode[], current) => {
            if (current.code === code.code) {
                const newCode = { ...current, comment: newValue }
                prev.push(newCode)
            } else {
                prev.push(current)
            }
            return prev
        }, [])

        setAlleleState({
            alleleAssessment: {
                evaluation: {
                    acmg: {
                        included: codesWithUpdatedComment,
                    },
                },
            },
        })
    }

    const updateCodeStrength = (oldCode: string, newCode: string) => {
        const includedCodes = AcmgHelper.getIncludedAcmgCodes(alleleState.alleleAssessment)

        const codesWithUpdatedComment = includedCodes.reduce((prev: AcmgCode[], current) => {
            if (current.code === oldCode) {
                const objectWithUpdatedStrength = { ...current, code: newCode }
                prev.push(objectWithUpdatedStrength)
            } else {
                prev.push(current)
            }
            return prev
        }, [])

        setAlleleState({
            alleleAssessment: {
                evaluation: {
                    acmg: {
                        included: codesWithUpdatedComment,
                    },
                },
            },
        })
    }

    const suggestedCodes = AcmgHelper.getSuggestedAcmgCodes(alleleState.alleleAssessment)

    const reqCodes = AcmgHelper.getReqCodes(alleleState.alleleAssessment)

    if (loading) {
        return <Spinner />
    }

    const includedAcmgCodes = AcmgHelper.getIncludedAcmgCodes(alleleState.alleleAssessment)

    return (
        <>
            <div className="mt-4 mb-6 flex flex-wrap gap-2">
                {includedAcmgCodes.map((code) => (
                    <AcmgChip
                        code={code}
                        onRemove={removeAcmgCode}
                        onRichTextChange={updateCodeComment}
                        onStrengthChange={(codeWithNewStrength) =>
                            updateCodeStrength(code.code, codeWithNewStrength.code)
                        }
                        key={code.code}
                        disabled={disabled}
                    />
                ))}
            </div>
            <Card label="Suggested">
                <div>
                    <div className="pb-4 text-lg">Suggested ACMG (+ to add)</div>

                    <div className="flex flex-wrap gap-2">
                        {suggestedCodes.map((acmgCode) => (
                            <AcmgChip
                                code={acmgCode}
                                key={JSON.stringify(acmgCode)}
                                onAdd={(code) => {
                                    AcmgHelper.addAcmgCode({
                                        code,
                                        source: 'suggested',
                                        assessment: alleleState.alleleAssessment,
                                        setAlleleState,
                                        dispatch,
                                    })
                                }}
                                disabled={disabled}
                            />
                        ))}
                    </div>
                    <div className="mt-4 w-28">
                        <Button onClick={() => setShowReqs(!showReqs)}>
                            {showReqs ? 'Hide REQ' : 'Show REQ'}
                        </Button>
                    </div>

                    {showReqs && (
                        <>
                            <div className="my-4">Requirements for ACMG</div>
                            <div className="flex flex-wrap gap-2">
                                {reqCodes.map((group) =>
                                    group.map((acmgCode) => (
                                        <AcmgChip
                                            code={acmgCode}
                                            key={acmgCode.code}
                                            onAdd={(code) =>
                                                AcmgHelper.addAcmgCode({
                                                    code,
                                                    source: 'suggested',
                                                    assessment: alleleState.alleleAssessment,
                                                    setAlleleState,
                                                    dispatch,
                                                })
                                            }
                                            disabled={disabled}
                                        />
                                    )),
                                )}
                            </div>
                        </>
                    )}
                </div>
            </Card>
        </>
    )
}
