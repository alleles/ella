import React from 'react'

import { useInterpretation } from 'views/interpretation/InterpretationContext'
import AnalysisSpecific from 'views/interpretation/sections/AnalysisSpecific'
import Frequency from 'views/interpretation/sections/Frequency'
import Region from 'views/interpretation/sections/Region'
import External from 'views/interpretation/sections/external/External'
import Prediction from 'views/interpretation/sections/prediction/Prediction'
import References from 'views/interpretation/sections/references/References'

export function AlleleInterpretationForm() {
    const { type } = useInterpretation()

    return (
        <div className="grow">
            {type === 'analysis' && <AnalysisSpecific />}
            <Region />
            <Frequency />
            <Prediction />
            <External />
            <References />
        </div>
    )
}
