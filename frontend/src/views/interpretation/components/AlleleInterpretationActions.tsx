import React from 'react'

import Button from 'components/elements/Button'
import Menu from 'components/elements/DropDownMenu'
import { useLayout } from 'components/layout/LayoutContainer'

import WorkflowActions from 'views/interpretation/components/WorkflowActions'
import InterpretationLog from 'views/interpretation/components/alleleBar/InterpretationLog'

export default function AlleleIntepretationActions() {
    const { setSectionBoxAllCollapsed, sectionBoxAllCollapsed } = useLayout()

    return (
        <div className="flex justify-end p-2">
            <div className="flex gap-x-2 pr-4">
                <Button onClick={() => setSectionBoxAllCollapsed(!sectionBoxAllCollapsed)}>
                    Collapse all
                </Button>
                <Menu title="Actions">
                    <Menu.MenuItem title="Copy variant" />
                </Menu>
                <InterpretationLog />
            </div>
            <WorkflowActions />
        </div>
    )
}
