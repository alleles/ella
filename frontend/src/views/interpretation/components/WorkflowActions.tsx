import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import Button from 'components/elements/Button'

import { createAlert } from 'store/alerts/alertsSlice'
import { selectCurrentUser } from 'store/users/usersSlice'

import { useInterpretation } from 'views/interpretation/InterpretationContext'
import useInterpretationStateContext from 'views/interpretation/InterpretationStateContext'
import { FinishModal } from 'views/interpretation/components/alleleBar/FinishModal'
import { ReassignInterpretationConfirmationModal } from 'views/interpretation/components/alleleBar/ReassignInterpretationConfirmationModal'

export default function WorkflowActions() {
    const dispatch = useDispatch()

    const { save } = useInterpretationStateContext()
    const { interpretation, start, reassignToMe, reopen } = useInterpretation()
    const currentUser = useSelector(selectCurrentUser)!

    const [showReassignToMeConfirmationModal, setShowReassignToMeConfirmationModal] =
        useState(false)
    const [showFinishModal, setShowFinishModal] = useState(false)

    const shouldShowButton = {
        startNotReady:
            interpretation.status === 'Not started' &&
            interpretation.workflowStatus === 'Not ready',
        start:
            interpretation.status === 'Not started' &&
            interpretation.workflowStatus === 'Interpretation',
        save:
            interpretation.status === 'Ongoing' &&
            (interpretation.workflowStatus === 'Interpretation' ||
                interpretation.workflowStatus === 'Review' ||
                interpretation.workflowStatus === 'Medical review' ||
                interpretation.workflowStatus === 'Not ready') &&
            interpretation.userId === currentUser.id,
        reassignToMe:
            interpretation.status === 'Ongoing' && interpretation.userId !== currentUser.id,
        finish: interpretation.status === 'Ongoing' && interpretation.userId === currentUser.id,
        startReview:
            interpretation.status === 'Not started' && interpretation.workflowStatus === 'Review',
        startMedicalReview:
            interpretation.status === 'Not started' &&
            interpretation.workflowStatus === 'Medical review',
        reopen: interpretation.finalized,
    }

    return (
        <div>
            <div className="flex gap-x-2 pl-3">
                {shouldShowButton.finish && (
                    <Button onClick={() => setShowFinishModal(true)}>Finish</Button>
                )}
                {shouldShowButton.save && (
                    <Button
                        onClick={() => {
                            save().then(() => {
                                dispatch(
                                    createAlert({
                                        body: 'Your interpretation was saved',
                                        type: 'info',
                                    }),
                                )
                            })
                        }}
                    >
                        Save
                    </Button>
                )}
                {shouldShowButton.startNotReady && <Button onClick={start}>Start not ready</Button>}
                {shouldShowButton.start && <Button onClick={start}>Start interpretation</Button>}
                {shouldShowButton.reassignToMe && (
                    <Button onClick={() => setShowReassignToMeConfirmationModal(true)}>
                        Reassign to me
                    </Button>
                )}
                {shouldShowButton.startReview && <Button onClick={start}>Start review</Button>}
                {shouldShowButton.startMedicalReview && (
                    <Button onClick={start}>Start medical review</Button>
                )}
                {shouldShowButton.reopen && <Button onClick={reopen}>Reopen</Button>}
            </div>
            {showReassignToMeConfirmationModal && (
                <ReassignInterpretationConfirmationModal
                    onNo={() => setShowReassignToMeConfirmationModal(false)}
                    onYes={() => {
                        reassignToMe()
                        setShowReassignToMeConfirmationModal(false)
                        dispatch(
                            createAlert({ body: 'Interpretation reassigned to you', type: 'info' }),
                        )
                    }}
                />
            )}
            {showFinishModal && <FinishModal onCancel={() => setShowFinishModal(false)} />}
        </div>
    )
}
