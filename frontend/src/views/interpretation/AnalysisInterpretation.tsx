/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { useParams } from 'react-router-dom'

import { fetchAnalysisInterpretationLogForAnalysisId } from 'store/interpretation/analysisInterpretationLogSlice'
import { fetchAnalysisInterpretationsByAnalysisId } from 'store/interpretation/analysisInterpretationSlice'
import { fetchAnalysisByAnalysisId } from 'store/interpretation/analysisSlice'

import { AnalysisInterpretationProvider } from 'views/interpretation/AnalysisInterpretationContext'
import { InterpretationStateProvider } from 'views/interpretation/InterpretationStateContext'
import { AnalysisInterpretationUIProvider } from 'views/interpretation/analysis/UIContextProvider'

export default function AnalysisInterpretation() {
    const { analysisId } = useParams<'analysisId'>()
    const dispatch = useDispatch()
    const [ready, setReady] = useState(false)

    if (!analysisId) {
        throw new Error('No analysis ID passed to page')
    }

    useEffect(() => {
        const fetchData = async () => {
            await dispatch(fetchAnalysisByAnalysisId(Number(analysisId)))
            await dispatch(fetchAnalysisInterpretationsByAnalysisId(Number(analysisId)))
            await dispatch(fetchAnalysisInterpretationLogForAnalysisId(Number(analysisId)))
        }
        fetchData()
            .then(() => {
                setReady(true)
            })
            .catch((e) => {
                console.error(e)
            })
    }, [])

    if (!ready) return null

    return (
        <AnalysisInterpretationProvider analysisId={Number(analysisId)}>
            <InterpretationStateProvider>
                <AnalysisInterpretationUIProvider />
            </InterpretationStateProvider>
        </AnalysisInterpretationProvider>
    )
}
