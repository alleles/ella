import igv from 'igv'
import React, { useEffect, useRef, useState } from 'react'
import { useSelector } from 'react-redux'

import { selectConfig } from 'store/config/configSlice'
import { selectAlleleById } from 'store/interpretation/alleleSlice'
import { RootState } from 'store/store'

import { Allele } from 'types/store/Allele'

import useInterpretationStateContext from 'views/interpretation/InterpretationStateContext'
import { TrackSelectionContext } from 'views/interpretation/genomicsViewer/TrackSelectionContext'
import { TrackTypeBase } from 'views/interpretation/genomicsViewer/TrackTypeBase'
import { TrackTypeDefault } from 'views/interpretation/genomicsViewer/TrackTypeDefault'
import { TrackTypeRoi } from 'views/interpretation/genomicsViewer/TrackTypeRoi'

// VERY HACKY!!!
// Load igv and unload it to stop it from rendering twice on first render.
const initIgv = async () => {
    try {
        await igv.createBrowser(document.getElementById('root'))
        // eslint-disable-next-line no-empty
    } catch (e) {}
    igv.removeAllBrowsers()
}
initIgv()

const getIgvLocus = (allele: Allele | undefined) => {
    const fallBackLocus = '1:1000'
    if (allele === undefined) {
        return fallBackLocus
    }
    return `${allele.chromosome}:${allele.startPosition}-${allele.startPosition + allele.length}`
}

export default function IgvJs() {
    const config = useSelector(selectConfig)
    const [igvbrowser, setIgvbrowser] = useState<any>(null)
    const igvContainerRef = useRef(null)
    const context = React.useContext(TrackSelectionContext)
    const trackTypes: TrackTypeBase[] = [new TrackTypeDefault(), new TrackTypeRoi()]

    const { selectedAlleleId } = useInterpretationStateContext()
    const allele: Allele | undefined = useSelector((state: RootState) => {
        if (selectedAlleleId === undefined) {
            return undefined
        }
        return selectAlleleById(state, {
            alleleId: selectedAlleleId,
        })
    })

    const options = {
        tracks: [],
        reference: {
            id: 'hg19',
            fastaURL: config.igv.reference.fastaURL,
            cytobandURL: config.igv.reference.cytobandURL,
        },
        loadDefaultGenomes: false,
        locus: getIgvLocus(allele),
        minimumBases: 50,
        search: {
            url: '/api/v1/igv/search/?q=$FEATURE$',
            coords: 0,
        },
        showKaryo: true,
        showCursorGuide: true,
        showCenterGuide: false,
        showSVGButton: false,
        showSampleNameButton: false,
        doubleClickDelay: 300,
        promisified: true,
    }

    const loadedTrackIds: String[] = igvbrowser
        ? [...trackTypes.map((t: TrackTypeBase) => t.getIgvLoadedTrackIds(igvbrowser)).flat()]
        : []

    // load igv.js
    useEffect(() => {
        const initBrowser = async () => {
            const container = igvContainerRef.current
            if (container) {
                const browser: any = await igv.createBrowser(container, options)
                browser.root.classList.add('w-full')
                setIgvbrowser(browser)
            }
        }
        initBrowser()
        return () => {
            igv.removeAllBrowsers()
        }
    }, [])

    useEffect(() => {
        if (igvbrowser) {
            igvbrowser.search(getIgvLocus(allele))
        }
    }, [igvbrowser, allele])

    // load tracks
    useEffect(() => {
        if (!igvbrowser) {
            return
        }

        // list tracks ocf the current type
        for (let i = 0; i < trackTypes.length; i += 1) {
            const trackType = trackTypes[i]
            const igvLoadedTrackIds = trackType.getIgvLoadedTrackIds(igvbrowser)

            // remove tracks that are not in the state anymore
            igvLoadedTrackIds
                .filter(
                    (trackId) =>
                        !Object.values(context.trackConfigs).find(
                            (cfg) => cfg.igv.name === trackId,
                        ),
                )
                .forEach((trackId) => {
                    trackType.removeTrack(igvbrowser, trackId)
                })

            // list tracks to add
            Object.entries(context.trackConfigs)
                .filter(([, cfg]) => trackType.matchesType(cfg.type))
                .filter(([, cfg]) => !igvLoadedTrackIds.includes(cfg.igv.name))
                .map(([, cfg]) => cfg.igv)
                .forEach((track) => {
                    trackType.loadTrack(igvbrowser, track)
                })
        }
    }, [context.trackConfigs, loadedTrackIds])

    return <div ref={igvContainerRef} className="-mt-1.5 flex h-full w-full bg-white pb-2" />
}
