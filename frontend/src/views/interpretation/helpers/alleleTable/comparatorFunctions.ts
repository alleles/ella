import { firstBy } from 'thenby'

import { ColumnDefinition } from 'components/elements/Table'

import { store } from 'store/store'

import { Allele } from 'types/store/Allele'

import { compareStrings, nonNullable } from 'utils/general'

import {
    AlleleTableDataRow,
    ColumnNames,
} from 'views/interpretation/helpers/alleleTable/alleleTable'
import { renderFunctions } from 'views/interpretation/helpers/alleleTable/renderFunctions'

export const comparatorFunctions: Record<
    ColumnNames,
    ColumnDefinition<AlleleTableDataRow>['comparator']
> = {
    position: ({ allele: allele1 }, { allele: allele2 }) => {
        const chromosome = (allele: Allele) => {
            const chr = allele.chromosome
            if (chr === 'X') return 23
            if (chr === 'Y') return 24
            if (chr === 'MT') return 25
            return Number(chr)
        }

        const start = (allele: Allele) => allele.startPosition

        return firstBy(chromosome).thenBy(start)(allele1, allele2)
    },
    hgvsc: undefined,
    inheritance: (row1, row2) =>
        compareStrings(renderFunctions.inheritance(row1), renderFunctions.inheritance(row2)),
    geneSymbols: (row1, row2) =>
        compareStrings(renderFunctions.geneSymbols(row1), renderFunctions.geneSymbols(row2)),
    consequence: ({ displayAllele: displayAllele1 }, { displayAllele: displayAllele2 }) => {
        const consequences1 = displayAllele1.flatMap((x) => x.consequence)
        const consequences2 = displayAllele2.flatMap((x) => x.consequence)

        const sortedConsequences = store.getState().config.transcripts.consequences
        return (
            Math.min(
                ...consequences1.map((c) =>
                    c ? sortedConsequences.indexOf(c) : sortedConsequences.length,
                ),
            ) -
            Math.min(
                ...consequences2.map((c) =>
                    c ? sortedConsequences.indexOf(c) : sortedConsequences.length,
                ),
            )
        )
    },
    hgvsp: undefined,
    genotype: undefined,
    quality: (
        { presentationAllele: presentationAllele1 },
        { presentationAllele: presentationAllele2 },
    ) => {
        if (
            !('variantCalling' in presentationAllele1) ||
            !('variantCalling' in presentationAllele2)
        ) {
            return 0
        }
        // Sort on max quality (if multiple callers)
        return (
            Math.max(
                ...presentationAllele1.variantCalling
                    .map((x) => x.variantQuality)
                    .filter(nonNullable),
            ) -
            Math.max(
                ...presentationAllele2.variantCalling
                    .map((x) => x.variantQuality)
                    .filter(nonNullable),
            )
        )
    },
    depth: (
        { presentationAllele: presentationAllele1 },
        { presentationAllele: presentationAllele2 },
    ) => {
        if (
            !('genotypeSampleData' in presentationAllele1) ||
            !('genotypeSampleData' in presentationAllele2)
        ) {
            return 0
        }
        // Sort on maximum depth of proband
        return (
            Math.max(
                ...Object.values(presentationAllele1.genotypeSampleData)
                    .map((x) => x?.proband?.sequencingDepth)
                    .filter(nonNullable),
            ) -
            Math.max(
                ...Object.values(presentationAllele2.genotypeSampleData)
                    .map((x) => x?.proband?.sequencingDepth)
                    .filter(nonNullable),
            )
        )
    },
    ratio: (
        { presentationAllele: presentationAllele1 },
        { presentationAllele: presentationAllele2 },
    ) => {
        if (
            !('genotypeSampleData' in presentationAllele1) ||
            !('genotypeSampleData' in presentationAllele2)
        ) {
            return 0
        }
        // Sort on maximum allele ratio of proband
        return (
            Math.max(
                ...Object.values(presentationAllele1.genotypeSampleData)
                    .map((x) => x?.proband?.alleleRatio)
                    .filter(nonNullable),
            ) -
            Math.max(
                ...Object.values(presentationAllele2.genotypeSampleData)
                    .map((x) => x?.proband?.alleleRatio)
                    .filter(nonNullable),
            )
        )
    },
    hiCount: (
        { displayFrequencies: displayFrequencies1 },
        { displayFrequencies: displayFrequencies2 },
    ) => (displayFrequencies1.count.maxValue || 0) - (displayFrequencies2.count.maxValue || 0),
    hiFreq: (
        { displayFrequencies: displayFrequencies1 },
        { displayFrequencies: displayFrequencies2 },
    ) => (displayFrequencies1.freq.maxValue || 0) - (displayFrequencies2.freq.maxValue || 0),
    externalDbs: ({ annotation: annotation1 }, { annotation: annotation2 }) =>
        Object.keys(annotation1?.external || {}).length -
        Object.keys(annotation2?.external || {}).length,
    classification: ({ assessment: assessment1 }, { assessment: assessment2 }) => {
        const a = assessment1.state?.classification || assessment1.existing?.classification || ''
        const b = assessment2.state?.classification || assessment2.existing?.classification || ''

        return compareStrings(b, a)
    },
}
