import React from 'react'

import { ColumnDefinition } from 'components/elements/Table'

import { store } from 'store/store'

import { GenotypeSampleData, SingleGenotypeSampleData } from 'types/view/GenotypeSampleData'

import { numberFormat } from 'utils/formatting'
import { nonNullable } from 'utils/general'

import {
    AlleleTableDataRow,
    ColumnNames,
} from 'views/interpretation/helpers/alleleTable/alleleTable'

const naSymbol = '-'

const extractGenotypeString = (
    genotypeSampleData: GenotypeSampleData,
    key: keyof SingleGenotypeSampleData,
    formatter: (x: any) => string | number = (x) => x || '?',
) => {
    const multipleSamples =
        genotypeSampleData.mother ||
        genotypeSampleData.father ||
        genotypeSampleData.affectedSiblings.length ||
        genotypeSampleData.unaffectedSiblings.length

    if (!multipleSamples) {
        return formatter(genotypeSampleData.proband?.[key])
    }

    let s = `P: ${formatter(genotypeSampleData.proband?.[key])}`
    if (genotypeSampleData.mother) {
        s += ` | M: ${formatter(genotypeSampleData.mother?.[key])}`
    }

    if (genotypeSampleData.father) {
        s += ` | F: ${formatter(genotypeSampleData.father?.[key])}`
    }

    genotypeSampleData.unaffectedSiblings
        .map((x) => x[key])
        .forEach((gt) => {
            s += ` | US: ${formatter(gt)}`
        })

    genotypeSampleData.affectedSiblings
        .map((x) => x[key])
        .forEach((gt) => {
            s += ` | AS: ${formatter(gt)}`
        })

    return s
}

export const renderFunctions: Record<ColumnNames, ColumnDefinition<AlleleTableDataRow>['render']> =
    {
        position: ({ allele }) => {
            const oneBasedStart = allele.startPosition + 1
            let r = `${allele.chromosome.toUpperCase()}:${oneBasedStart}`
            r += oneBasedStart !== allele.openEndPosition ? `-${allele.openEndPosition}` : ''
            return r
        },
        inheritance: ({ displayAllele }) => {
            const inheritances = displayAllele.map((x) =>
                x.inheritance ? x.inheritance : naSymbol,
            )
            return inheritances.length ? inheritances.join(' | ') : naSymbol
        },
        hgvsc: ({ displayAllele }) => {
            const hgvsc = displayAllele.map((x) => (x.hgvscShort ? x.hgvscShort : naSymbol))
            return hgvsc.length ? hgvsc.join(' | ') : naSymbol
        },
        geneSymbols: ({ displayAllele }) => {
            const geneSymbols = displayAllele.map((x) => {
                const geneSymbol = x.geneSymbol ? x.geneSymbol : naSymbol
                const exonIntron = x.exonIntron ? ` (${x.exonIntron})` : ''
                return `${geneSymbol}${exonIntron}`
            })
            return geneSymbols.length ? geneSymbols.join(' | ') : naSymbol
        },
        hgvsp: ({ displayAllele }) => {
            const hgvsp = displayAllele.map((x) => (x.hgvsp ? x.hgvsp : naSymbol))
            return hgvsp.length ? hgvsp.join(' | ') : naSymbol
        },
        consequence: ({ displayAllele }) => {
            const sortedConsequences = store.getState().config.transcripts.consequences
            const consequences = displayAllele.map((x) =>
                x.consequence
                    ? x.consequence
                          .filter(nonNullable)
                          .sort(
                              (csq1, csq2) =>
                                  sortedConsequences.indexOf(csq1) -
                                  sortedConsequences.indexOf(csq2),
                          )
                          .map((csq) => csq.replace(/_variant$/, ''))
                          .join(', ')
                    : naSymbol,
            )

            return consequences.length ? consequences.join(' | ') : naSymbol
        },
        genotype: ({ presentationAllele }) => {
            if (!('genotypeSampleData' in presentationAllele)) {
                return naSymbol
            }
            return Object.values(presentationAllele.genotypeSampleData)
                .filter(nonNullable)
                .map((singleGsd) => extractGenotypeString(singleGsd, 'formatted'))
                .join(' | ')
        },
        quality: ({ presentationAllele }) => {
            if (!('variantCalling' in presentationAllele)) {
                return naSymbol
            }
            return presentationAllele.variantCalling
                .map(({ variantQuality }) => variantQuality)
                .join(' | ')
        },
        depth: ({ presentationAllele }) => {
            if (!('genotypeSampleData' in presentationAllele)) {
                return naSymbol
            }
            return Object.values(presentationAllele.genotypeSampleData)
                .filter(nonNullable)
                .map((singleGsd) => extractGenotypeString(singleGsd, 'sequencingDepth'))
                .join(' | ')
        },
        ratio: ({ presentationAllele }) => {
            if (!('genotypeSampleData' in presentationAllele)) {
                return naSymbol
            }
            return Object.values(presentationAllele.genotypeSampleData)
                .filter(nonNullable)
                .map((singleGsd) =>
                    extractGenotypeString(singleGsd, 'alleleRatio', (ar: number) =>
                        ar ? ar.toFixed(2) : '?',
                    ),
                )
                .join(' | ')
        },
        hiCount: ({ displayFrequencies }) => (
            <span className="text-right">
                {displayFrequencies.count.maxValue
                    ? numberFormat(displayFrequencies.count.maxValue)
                    : naSymbol}
            </span>
        ),
        hiFreq: ({ displayFrequencies }) => (
            <span className="text-right">
                {displayFrequencies.freq.maxValue
                    ? numberFormat(displayFrequencies.freq.maxValue)
                    : naSymbol}
            </span>
        ),
        externalDbs: (row) =>
            row.annotation.external ? Object.keys(row.annotation.external).join(' ') : naSymbol,
        classification: ({ assessment }) => {
            const fromClass = `${assessment.existing?.classification || ''}`
            const toClass = assessment.state?.classification
            const isReused = assessment.state === null

            if (isReused) {
                return <span>{fromClass}</span>
            }
            if (toClass) {
                return (
                    <span>
                        {`${fromClass}${assessment.existingIsOutdated ? '*' : ''}`}
                        <span className="align-text-bottom">{'\u2192'}</span>
                        {`${toClass}`}
                    </span>
                )
            }
            return <span />
        },
    }
