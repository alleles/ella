import { ColumnDefinition } from 'components/elements/Table'

import {
    AlleleTableDataRow,
    ColumnNames,
} from 'views/interpretation/helpers/alleleTable/alleleTable'
import { comparatorFunctions } from 'views/interpretation/helpers/alleleTable/comparatorFunctions'
import { renderFunctions } from 'views/interpretation/helpers/alleleTable/renderFunctions'

export const columnDefinitions: Record<ColumnNames, ColumnDefinition<AlleleTableDataRow>> = {
    position: {
        comparator: comparatorFunctions.position,
        field: 'position',
        render: renderFunctions.position,
        title: 'Position',
        titleTooltip: 'Position',
        tooltip: true,
    },
    inheritance: {
        comparator: comparatorFunctions.inheritance,
        field: 'inheritance',
        render: renderFunctions.inheritance,
        title: 'Inh',
        titleTooltip: 'Inheritance',
        tooltip: true,
    },
    geneSymbols: {
        comparator: comparatorFunctions.geneSymbols,
        field: 'geneSymbols',
        render: renderFunctions.geneSymbols,
        title: 'Genes',
        titleTooltip: 'Genes',
        tooltip: true,
    },
    hgvsc: {
        comparator: comparatorFunctions.hgvsc,
        field: 'hgvsc',
        render: renderFunctions.hgvsc,
        title: 'HGVSc',
        titleTooltip: 'HGVSc',
        tooltip: true,
    },
    hgvsp: {
        comparator: comparatorFunctions.hgvsp,
        field: 'hgvsp',
        render: renderFunctions.hgvsp,
        title: 'HGVSp',
        titleTooltip: 'HGVSp',
        tooltip: true,
    },
    consequence: {
        comparator: comparatorFunctions.consequence,
        field: 'consequence',
        render: renderFunctions.consequence,
        title: 'CSQ',
        titleTooltip: 'Consequence',
        tooltip: true,
    },
    genotype: {
        comparator: comparatorFunctions.genotype,
        field: 'genotype',
        render: renderFunctions.genotype,
        title: 'Genotype',
        titleTooltip:
            'Genotype - [Ref]/[Alt]; (P) Proband (F) Father (M) Mother (US) Unaffected Sibling (AS) Affected Sibling',
        tooltip: true,
    },
    quality: {
        field: 'quality',
        title: 'Qual',
        titleTooltip: 'Quality',
        comparator: comparatorFunctions.quality,
        render: renderFunctions.quality,
    },
    depth: {
        comparator: comparatorFunctions.depth,
        field: 'depth',
        render: renderFunctions.depth,
        title: 'Depth',
        titleTooltip:
            'Sequencing depth - [Ref]/[Alt]; (P) Proband (F) Father (M) Mother (US) Unaffected Sibling (AS) Affected Sibling',
        tooltip: true,
    },
    ratio: {
        comparator: comparatorFunctions.ratio,
        field: 'ratio',
        render: renderFunctions.ratio,
        title: 'Ratio',
        titleTooltip:
            'Allele ratio - [Ref]/[Alt]; (P) Proband (F) Father (M) Mother (US) Unaffected Sibling (AS) Affected Sibling',
        tooltip: true,
    },
    hiFreq: {
        comparator: comparatorFunctions.hiFreq,
        field: 'hiFreq',
        render: renderFunctions.hiFreq,
        title: 'Hi. freq.',
        titleTooltip: 'Highest frequency in external DB',
        tooltip: true,
    },
    hiCount: {
        comparator: comparatorFunctions.hiCount,
        field: 'hiCount',
        render: renderFunctions.hiCount,
        title: 'Hi. count',
        titleTooltip: 'Highest allele count in external DB',
        tooltip: true,
    },
    externalDbs: {
        comparator: comparatorFunctions.externalDbs,
        field: 'externalDbs',
        render: renderFunctions.externalDbs,
        title: 'External',
        titleTooltip: 'Annotation from external databases',
        tooltip: true,
    },
    classification: {
        comparator: comparatorFunctions.classification,
        field: 'classification',
        render: renderFunctions.classification,
        title: 'Class',
        tooltip: true,
    },
}
