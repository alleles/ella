import { Config } from 'types/store/Config'

import { AcmgHelper } from 'views/interpretation/helpers/acmg/AcmgHelper'
import { AcmgStrengthHelper } from 'views/interpretation/helpers/acmg/AcmgStrengthHelper'

const STRENGTHS_CLINGEN = {
    benign: {
        BNW: 'Not weighted',
        BP: 'Supportive',
        BS: 'Strong',
        BA: 'Stand-alone',
    },
    pathogenic: {
        PNW: 'Not weighted',
        PP: 'Supportive',
        PM: 'Moderate',
        PS: 'Strong',
        PVS: 'Very_strong',
    },
}

export class AcmgFormatter {
    static makeCodeNameReadable(name: string): string {
        let modifiedName = name.replace(/REQ_GP/g, 'GP - ').replace(/REQ_/g, 'R - ')

        modifiedName = modifiedName.replaceAll('_', ' ')

        return modifiedName
    }

    static formatCode(codeStr: string, config: Config): string {
        if (AcmgStrengthHelper.isModifiedStrength(codeStr)) {
            const strength = AcmgStrengthHelper.getCodeStrength(codeStr, config)
            const base = AcmgHelper.getCodeBase(codeStr)
            const codeType = AcmgHelper.getCodeType(codeStr)
            return `${base} ${STRENGTHS_CLINGEN[codeType][strength].toUpperCase()}`
        }
        return codeStr
    }

    static getColorKey(code: string) {
        if (AcmgHelper.getCodeType(code) === 'other') {
            return 'other'
        }
        return code.substring(0, 2).toLowerCase()
    }
}
