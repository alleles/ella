import { v4 as uuidv4 } from 'uuid'

import { createAlert } from 'store/alerts/alertsSlice'

import { AcmgCode, AcmgType } from 'types/store/Acmg'
import { AlleleAssessment } from 'types/store/AlleleAssessment'
import { AlleleState, StateAlleleAssessment } from 'types/store/AlleleInterpretation'
import { Config } from 'types/store/Config'

import { DeepPartial } from 'utils/types'

export class AcmgHelper {
    /**
     * Returns the base ACMG code, without strength applier.
     *
     * @param {String} codeStr
     */
    static getCodeBase(codeStr: string): string {
        if (codeStr.startsWith('REQ')) {
            return codeStr
        }
        const codeParts = codeStr.split('x')
        if (codeParts.length > 2) {
            throw Error(`Too many 'x' present in ${codeStr}`)
        }
        return codeParts[codeParts.length - 1]
    }

    static getCodeType(codeStr: string): AcmgType {
        const base = this.getCodeBase(codeStr)
        if (base.startsWith('B')) {
            return 'benign'
        }
        if (base.startsWith('P')) {
            return 'pathogenic'
        }
        return 'other'
    }

    static getSuggestedAcmgCodes = (alleleAssessment: StateAlleleAssessment): AcmgCode[] => {
        if (!alleleAssessment) {
            return []
        }
        return alleleAssessment.evaluation.acmg.suggested.filter((c) => !c.code.startsWith('REQ'))
    }

    static getReqCodes(alleleAssessment: StateAlleleAssessment): AcmgCode[][] {
        const reqGroups: AcmgCode[][] = [] // array of req groups (arrays)
        if (!alleleAssessment) {
            return reqGroups
        }
        const reqs =
            alleleAssessment.evaluation.acmg.suggested?.filter((c) => c.code.startsWith('REQ')) ??
            []
        reqs.forEach((req) => {
            const matchingGroup = reqGroups.find((rg) => rg.find((r) => r.code === req.code))
            if (matchingGroup) {
                matchingGroup.push(req)
            } else {
                reqGroups.push([req])
            }
        })
        return reqGroups
    }

    private static getMatch(code: AcmgCode) {
        if (code.source === 'user') {
            return 'Added by user'
        }
        if (Array.isArray(code.match)) {
            return code.match.join(', ')
        }
        return code.match
    }

    private static getOperator(code: AcmgCode, config: Config) {
        return code.op ? config.acmg.formatting.operators[code.op] : ''
    }

    private static getSource(code: AcmgCode) {
        if (code.source) {
            return code.source.split('.').slice(1).join('->')
        }
        return 'N/A'
    }

    static getMatches(code: AcmgCode, config: Config) {
        return {
            source: AcmgHelper.getSource(code),
            match: AcmgHelper.getMatch(code),
            op: AcmgHelper.getOperator(code, config),
        }
    }

    static getCriteria(code: AcmgCode, config: Config): string {
        if (AcmgHelper.getCodeBase(code.code) in config.acmg.explanation) {
            return config.acmg.explanation[this.getCodeBase(code.code)].criteria
        }

        return ''
    }

    static getShortCriteria(code: AcmgCode, config: Config): string {
        if (this.getCodeBase(code.code) in config.acmg.explanation) {
            return config.acmg.explanation[this.getCodeBase(code.code)].short_criteria
        }

        return ''
    }

    static getRequiredFor(code: AcmgCode, config: Config): string[] {
        if (code.code in config.acmg.explanation) {
            if (code.code.startsWith('REQ_GP')) {
                return []
            }
            return config.acmg.explanation[code.code].sources
        }

        return []
    }

    static getNotes(code: AcmgCode, config: Config): string[] {
        if (
            this.getCodeBase(code.code) in config.acmg.explanation &&
            'notes' in config.acmg.explanation[this.getCodeBase(code.code)]
        ) {
            return config.acmg.explanation[this.getCodeBase(code.code)].notes?.split(/\n/) ?? []
        }

        return []
    }

    static getAcmgCodesPath(alleleId: number) {
        return `state.allele[${alleleId}].alleleAssessment.evaluation.acmg`
    }

    static getIncludedAcmgCodes(
        alleleAssessment: AlleleAssessment | StateAlleleAssessment,
    ): AcmgCode[] {
        return alleleAssessment.evaluation.acmg.included ?? []
    }

    static acmgCodeAlreadyIncluded(
        code: string,
        assessment: AlleleAssessment | StateAlleleAssessment,
    ): boolean {
        return !!this.getIncludedAcmgCodes(assessment)?.find(
            (alreadyIncludedCode) =>
                AcmgHelper.getCodeBase(alreadyIncludedCode.code) === AcmgHelper.getCodeBase(code),
        )
    }

    static addAcmgCode({
        code,
        source = 'suggested',
        assessment,
        setAlleleState,
        dispatch,
    }: {
        code: AcmgCode
        source: string
        assessment: AlleleAssessment | StateAlleleAssessment
        setAlleleState: (state: DeepPartial<AlleleState>) => void
        dispatch
    }) {
        const alreadyIncludedCodes = this.getIncludedAcmgCodes(assessment)

        if (this.acmgCodeAlreadyIncluded(code.code, assessment)) {
            dispatch(
                createAlert({
                    body: `ACMG criterion has already been added`,
                    type: 'error',
                }),
            )
            return
        }
        setAlleleState({
            alleleAssessment: {
                evaluation: {
                    acmg: {
                        included: [
                            ...(alreadyIncludedCodes ?? []),
                            { ...code, source, uuid: uuidv4() },
                        ],
                    },
                },
            },
        })
    }
}
