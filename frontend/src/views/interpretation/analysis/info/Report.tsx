import React from 'react'
import ReactMarkdown from 'react-markdown'
import gfm from 'remark-gfm'

// eslint-disable-next-line jsx-a11y/heading-has-content
const h3 = ({ node, ...props }) => <h3 {...props} className="my-4 text-lg" />
const table = ({ node, ...props }) => <table {...props} className="divide-ellagray divide-y" />
const thead = ({ node, ...props }) => <thead {...props} className="bg-ellagray-100" />
const th = ({ node, ...props }) => (
    <th {...props} className="text-ellagray-900 py-2 pl-2 pr-6 text-left text-sm font-semibold" />
)
const tbody = ({ node, ...props }) => (
    <tbody {...props} className="divide-ellagray divide-y bg-white" />
)
const td = ({ node, ...props }) => (
    <td {...props} className="text-ellagray-700 whitespace-nowrap py-1 pl-2 pr-6 text-sm" />
)
const p = ({ node, ...props }) => <p {...props} className="my-2 text-sm" />

export default function Report({ report }: { report: string }) {
    return (
        <ReactMarkdown
            remarkPlugins={[gfm]}
            components={{
                h3,
                table,
                thead,
                th,
                tbody,
                td,
                p,
            }}
        >
            {report}
        </ReactMarkdown>
    )
}
