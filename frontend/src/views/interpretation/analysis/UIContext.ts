import { Context, createContext, useContext, useState } from 'react'

import { CallerType } from 'types/api/Pydantic'

import { DeepPartial } from 'utils/types'

interface UIContext {
    uiMode: InterpretationUIMode
    setUIMode: (mode: InterpretationUIMode) => void
    callerTypeMode: CallerType
    setCallerTypeMode: (mode: CallerType) => void
}

export enum InterpretationUIMode {
    INFO = 'INFO',
    DETAILS = 'DETAILS',
    VISUAL = 'VISUAL',
    REPORT = 'REPORT',
    FILTER = 'FILTER',
}

export const analysisInterpretationUIContext = createContext<DeepPartial<UIContext>>({})

export function useAnalysisInterpretationUIContextState(): UIContext {
    const [uiMode, setUIMode] = useState<InterpretationUIMode>(InterpretationUIMode.DETAILS)
    const [callerTypeMode, setCallerTypeMode] = useState<CallerType>(CallerType.SNV)

    return {
        uiMode,
        setUIMode,
        callerTypeMode,
        setCallerTypeMode,
    }
}

export default function useAnalysisInterpretationUIContext(): UIContext {
    return useContext(analysisInterpretationUIContext as unknown as Context<UIContext>)
}
