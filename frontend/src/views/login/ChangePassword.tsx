import { ExclamationCircleIcon } from '@heroicons/react/24/solid'
import React, { FormEvent, useEffect, useMemo, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import * as yup from 'yup'

import API from 'app/API'
import useAuth from 'app/auth'

import Button from 'components/elements/Button'
import Form from 'components/elements/Form'
import Input from 'components/elements/Input'

import logo from 'resources/logo_blue.svg'

import { createAlert } from 'store/alerts/alertsSlice'
import { fetchConfig, selectPasswordPolicy } from 'store/config/configSlice'
import { selectCurrentUsername } from 'store/users/usersSlice'

import validateSchema from 'utils/forms'

import buildValidatorFromPasswordPolicy from 'views/login/passwordPolicy'

export default function ChangePassword() {
    const dispatch = useDispatch()
    const { login } = useAuth()
    const navigate = useNavigate()
    const currentUsername = useSelector(selectCurrentUsername)

    const [username, setUsername] = useState('')
    const [currentPassword, setCurrentPassword] = useState('')
    const [newPassword, setNewPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [formError, setFormError] = useState('')
    const [fieldErrors, setFieldErrors] = useState<Record<string, string>>({
        username: '',
        currentPassword: '',
        newPassword: '',
        confirmPassword: '',
    })

    useEffect(() => {
        ;(async () => {
            await dispatch(fetchConfig())
        })()
    }, [])
    const passwordPolicy = useSelector(selectPasswordPolicy)
    const passwordValidator = useMemo(
        () => buildValidatorFromPasswordPolicy(passwordPolicy),
        [passwordPolicy],
    )

    useEffect(() => {
        if (currentUsername) {
            setUsername(currentUsername)
        }
    }, [currentUsername])

    const validationSchema = yup.object().shape({
        username: yup.string().label('Username').required(),
        currentPassword: yup.string().label('Current password').required(),
        newPassword: passwordValidator.label('New password').required(),
        confirmPassword: yup
            .string()
            .label('Confirm password')
            .required()
            .oneOf([yup.ref('newPassword')], 'Passwords must match'),
    })

    const validateForm = () => {
        const { valid, errors } = validateSchema(validationSchema, {
            username,
            currentPassword,
            newPassword,
            confirmPassword,
        })
        if (!valid) {
            setFieldErrors(errors)
        } else {
            setFieldErrors({
                username: '',
                currentPassword: '',
                newPassword: '',
                confirmPassword: '',
            })
        }
        setFormError('')
        return valid
    }

    async function handleSubmit(ev: FormEvent) {
        ev.preventDefault()
        const valid = await validateForm()
        if (!valid) return
        API.ChangePassword.post(username, currentPassword, newPassword)
            .then(async () => {
                await login(username, newPassword)
                dispatch(createAlert({ body: 'Password changed successfully' }))
                navigate('/')
            })
            .catch(async (err) => {
                if (err.response?.status === 401) {
                    setFormError('Invalid username or password')
                } else {
                    setFormError('A server error occurred')
                }
            })
    }

    return (
        <div className="flex min-h-full flex-col justify-center py-24 px-8">
            <div className="mx-auto w-full max-w-md">
                <img className="mx-auto h-12 w-auto" src={logo} alt="ELLA" />
                <h2 className="text-ellagray-900 mt-6 text-center text-3xl font-semibold">
                    Change your password
                </h2>
            </div>

            <div className="mx-auto mt-8 w-full max-w-md">
                <div className="rounded-lg bg-white py-8 px-10 shadow">
                    <div className="flex">
                        <ExclamationCircleIcon className="text-ellared-dark mr-4 w-14" />

                        <p className="text-ellagray-700 text-sm">
                            Your password needs to have at least{' '}
                            {passwordPolicy.password_minimum_length} characters and contain at least{' '}
                            {passwordPolicy.password_num_match_groups} of the following:
                        </p>
                    </div>

                    <ul className="mt-4 text-center">
                        {passwordPolicy.password_match_groups_descr.map((descr) => (
                            <li className="text-ellagray-700 text-sm">{descr}</li>
                        ))}
                    </ul>
                </div>
            </div>
            <div className="mx-auto mt-8 w-full max-w-md">
                <div className="rounded-lg bg-white py-8 px-10 shadow">
                    <Form className="space-y-6" error={formError} onSubmit={handleSubmit}>
                        <div className="flex rounded-md shadow-sm">
                            <Input
                                type="text"
                                name="username"
                                label="Username"
                                value={username}
                                onChange={(ev) => setUsername(ev.target.value)}
                                error={fieldErrors?.username}
                                disabled={!!currentUsername}
                            />
                        </div>
                        <div className="flex rounded-md shadow-sm">
                            <Input
                                type="password"
                                name="password"
                                label="Password"
                                value={currentPassword}
                                onChange={(ev) => setCurrentPassword(ev.target.value)}
                                error={fieldErrors?.currentPassword}
                            />
                        </div>
                        <div className="flex rounded-md shadow-sm">
                            <Input
                                type="password"
                                name="new-password"
                                label="New password"
                                value={newPassword}
                                onChange={(ev) => setNewPassword(ev.target.value)}
                                error={fieldErrors?.newPassword}
                            />
                        </div>
                        <div className="flex rounded-md shadow-sm">
                            <Input
                                type="password"
                                name="confirm-Password"
                                label="Confirm password"
                                value={confirmPassword}
                                onChange={(ev) => setConfirmPassword(ev.target.value)}
                                error={fieldErrors?.confirmPassword}
                            />
                        </div>
                        <div>
                            <Button type="submit">Change password</Button>
                        </div>
                    </Form>
                </div>
            </div>
        </div>
    )
}
