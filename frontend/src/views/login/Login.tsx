import React, { FormEvent, useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { useLocation, useNavigate } from 'react-router-dom'
import * as yup from 'yup'

import useAuth from 'app/auth'

import Button from 'components/elements/Button'
import Form from 'components/elements/Form'
import Input from 'components/elements/Input'

import logo from 'resources/logo_blue.svg'

import { fetchCurrentUserId } from 'store/users/usersSlice'
import { thunkIsRejected } from 'store/utils'

import validateSchema from 'utils/forms'

type LocationState = {
    from: Location
}

const validationSchema = yup.object().shape({
    username: yup.string().label('Username').required(),
    password: yup.string().label('Password').required(),
})

export default function Login() {
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const location = useLocation()
    const { login } = useAuth()

    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [generalFormError, setGeneralFormError] = useState('')

    const [formFieldErrors, setFormFieldErrors] = useState<Record<string, string>>({
        username: '',
        password: '',
    })

    const validateForm = () => {
        const { valid, errors } = validateSchema(validationSchema, {
            username,
            password,
        })
        if (!valid) {
            setFormFieldErrors(errors)
        } else {
            setFormFieldErrors({ username: '', password: '' })
        }
        setGeneralFormError('')
        return valid
    }

    useEffect(() => {
        ;(async () => {
            try {
                const userFetch = await dispatch(fetchCurrentUserId())
                if (thunkIsRejected(userFetch)) return
                login()
                navigate((location.state as LocationState)?.from || '/')
            } catch (error) {
                console.error(error)
            }
        })()
    }, [])

    async function handleSubmit(ev: FormEvent) {
        ev.preventDefault()
        const valid = validateForm()
        if (!valid) return
        try {
            await login(username, password)
            navigate((location.state as LocationState)?.from || '/')
        } catch (err: any) {
            if (err.response?.status === 401) {
                setGeneralFormError('Invalid username or password')
            } else {
                setGeneralFormError('A server error occurred')
            }
            console.error(err)
        }
    }

    return (
        <div className="flex min-h-full flex-col justify-center py-40 px-8">
            <div className="mx-auto w-full max-w-md">
                <img className="mx-auto h-28 w-auto" src={logo} alt="ELLA" />
                <h2 className="text-ellagray-900 mt-6 text-center text-3xl font-semibold">
                    Sign in to your account
                </h2>
            </div>

            <div className="mx-auto mt-8 w-full max-w-md">
                <div className="rounded-lg bg-white py-8 px-10 shadow">
                    <Form className="space-y-6" error={generalFormError} onSubmit={handleSubmit}>
                        <div className="flex rounded shadow-sm">
                            <Input
                                type="text"
                                name="username"
                                label="Username"
                                value={username}
                                onChange={(ev) => setUsername(ev.target.value)}
                                error={formFieldErrors?.username}
                            />
                        </div>
                        <div className="flex rounded shadow-sm">
                            <Input
                                type="password"
                                name="password"
                                label="Password"
                                value={password}
                                onChange={(ev) => setPassword(ev.target.value)}
                                error={formFieldErrors?.password}
                            />
                        </div>
                        <div>
                            <Button type="submit">Sign in</Button>
                        </div>
                    </Form>
                </div>
            </div>
        </div>
    )
}
