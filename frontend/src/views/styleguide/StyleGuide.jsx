import React from 'react'

import Palette from 'views/styleguide//Palette'
import FontSizes from 'views/styleguide/FontSizes'

function StyleGuide() {
    return (
        <div className="flex min-h-full flex-col justify-center py-12 px-8">
            <div>
                <h2 className="mb-10 text-3xl">Palette</h2>
                <Palette />
            </div>
            <div className="my-10">
                <h2 className="mb-10 text-3xl">Font-size</h2>
                <FontSizes />
            </div>
        </div>
    )
}
export default StyleGuide
