import React from 'react'

function Palette() {
    return (
        <div className="text-ellagray-900 grid grid-cols-1 gap-8 rounded bg-white p-6 font-mono text-xs shadow">
            <div>
                <div className="flex flex-row space-y-0 space-x-4">
                    <div className="w-32 shrink-0">
                        <div className="flex h-10 flex-col justify-center">
                            <div className="font-sans text-sm">ELLAblue</div>
                        </div>
                    </div>
                    <div className="grid min-w-0 flex-1 grid-cols-7 gap-x-4 gap-y-3">
                        <div className="space-y-1.5">
                            <div className="bg-ellablue-lightest h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>ellablue-lightest</div>
                            </div>
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-ellablue-lighter h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>ellablue-lighter</div>
                            </div>
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-ellablue h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>ellablue</div>
                            </div>
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-ellablue-darker h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>ellablue-darker</div>
                            </div>
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-ellablue-darkest h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>ellablue-darkest</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <div className="flex flex-row space-y-0 space-x-4">
                    <div className="w-32 shrink-0">
                        <div className="flex h-10 flex-col justify-center">
                            <div className="font-sans text-sm">ELLApurple</div>
                        </div>
                    </div>
                    <div className="grid min-w-0 flex-1 grid-cols-7  gap-x-4 gap-y-3">
                        <div className="space-y-1.5">
                            <div className="bg-ellapurple-lightest h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>ellapurple-lightest</div>
                            </div>
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-ellapurple-lighter h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>ellapurple-lighter</div>
                            </div>
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-ellapurple h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>ellapurple</div>
                            </div>
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-ellapurple-dark h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>ellapurple-dark</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <div className="flex flex-row space-y-0 space-x-4">
                    <div className="w-32 shrink-0">
                        <div className="flex h-10 flex-col justify-center">
                            <div className="font-sans text-sm">ELLAgreen</div>
                        </div>
                    </div>
                    <div className="grid min-w-0 flex-1 grid-cols-7  gap-x-4 gap-y-3">
                        <div className="space-y-1.5">
                            <div className="h-10 w-full rounded" />
                            <div className="px-0.5" />
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-ellagreen-light h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>ellagreen-light</div>
                            </div>
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-ellagreen h-10 w-full  rounded" />
                            <div className="px-0.5">
                                <div>ellagreen</div>
                            </div>
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-ellagreen-dark h-10 w-full  rounded" />
                            <div className="px-0.5">
                                <div>ellagreen-dark</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <div className="flex flex-row space-y-0 space-x-4">
                    <div className="w-32 shrink-0">
                        <div className="flex h-10 flex-col justify-center">
                            <div className="font-sans text-sm">ELLAred</div>
                        </div>
                    </div>
                    <div className="grid min-w-0 flex-1 grid-cols-7  gap-x-4 gap-y-3">
                        <div className="space-y-1.5">
                            <div className="h-10 w-full rounded" />
                            <div className="px-0.5" />
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-ellared-light h-10 w-full  rounded" />
                            <div className="px-0.5">
                                <div>ellared-light</div>
                            </div>
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-ellared h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>ellared</div>
                            </div>
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-ellared-dark h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>ellared-dark</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <div className="flex flex-row space-y-0 space-x-4">
                    <div className="w-32 shrink-0">
                        <div className="flex h-10 flex-col justify-center">
                            <div className="font-sans text-sm">ELLAyellow</div>
                        </div>
                    </div>
                    <div className="grid min-w-0 flex-1 grid-cols-7 gap-x-4 gap-y-3">
                        <div className="space-y-1.5">
                            <div className="h-10 w-full rounded" />
                            <div className="px-0.5" />
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-ellayellow-light h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>ellayellow-light</div>
                            </div>
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-ellayellow h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>ellayellow</div>
                            </div>
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-ellayellow-dark h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>ellayellow-dark</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <div className="flex flex-row space-y-0 space-x-4">
                    <div className="w-32 shrink-0">
                        <div className="flex h-10 flex-col justify-center">
                            <div className="font-sans text-sm">ELLAgray</div>
                        </div>
                    </div>
                    <div className="grid min-w-0 flex-1 grid-cols-7  gap-x-4 gap-y-3">
                        <div className="space-y-1.5">
                            <div className="bg-ellagray-50 h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>ellagray-50</div>
                            </div>
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-ellagray-100 h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>ellagray-100</div>
                            </div>
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-ellagray h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>ellagray</div>
                            </div>
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-ellagray-400 h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>ellagray-400</div>
                            </div>
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-ellagray-500 h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>ellagray-500</div>
                            </div>
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-ellagray-700 h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>ellagray-700</div>
                            </div>
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-ellagray-900 h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>ellagray-900</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="pt-20">
                <div className="flex flex-row space-y-0 space-x-4">
                    <div className="w-32 shrink-0">
                        <div className="flex h-10 flex-col justify-center">
                            <div className="font-sans text-sm">Editor</div>
                        </div>
                    </div>
                    <div className="grid min-w-0 flex-1 grid-cols-7  gap-x-4 gap-y-3">
                        <div className="space-y-1.5">
                            <div className="bg-editor-red h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>editor-red</div>
                            </div>
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-editor-green h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>editor-green</div>
                            </div>
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-editor-blue h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>editor-blue</div>
                            </div>
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-editor-yellow h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>editor-yellow</div>
                            </div>
                        </div>
                        <div className="space-y-1.5">
                            <div className="bg-editor-orange h-10 w-full rounded" />
                            <div className="px-0.5">
                                <div>editor-orange</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <div className="flex flex-row space-y-0 space-x-4">
                    <div className="w-32 shrink-0">
                        <div className="flex h-10 flex-col justify-center">
                            <div className="font-sans text-sm">B/W</div>
                        </div>
                    </div>
                    <div className="grid min-w-0 flex-1 grid-cols-7  gap-x-4 gap-y-3">
                        <div className="space-y-1.5">
                            <div className="h-10 w-full rounded bg-white" />
                            <div className="px-0.5">
                                <div>white</div>
                            </div>
                        </div>
                        <div className="space-y-1.5">
                            <div className="h-10 w-full rounded bg-black" />
                            <div className="px-0.5">
                                <div>black</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Palette
