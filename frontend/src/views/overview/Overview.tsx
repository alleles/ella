import React from 'react'
import { useParams } from 'react-router-dom'

import AllelesOverview from 'views/overview/Alleles'
import Analyses from 'views/overview/Analyses'

function Overview() {
    const { overviewType } = useParams()
    return (
        <>
            {overviewType === 'analyses' && <Analyses />}
            {overviewType === 'variants' && <AllelesOverview />}
            {overviewType === 'import' && <h4>Import</h4>}
        </>
    )
}

export default Overview
