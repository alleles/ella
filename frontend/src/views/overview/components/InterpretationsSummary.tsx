import { format } from 'date-fns'
import React, { ReactElement } from 'react'
import { useSelector } from 'react-redux'

import { selectUsers } from 'store/users/usersSlice'

import { AlleleInterpretationOverview } from 'types/store/AlleleInterpretationOverview'
import { AnalysisInterpretationOverview } from 'types/store/AnalysisInterpretationOverview'

type Interpretation = AlleleInterpretationOverview | AnalysisInterpretationOverview

const getEndAction = (interpretation: Interpretation) => {
    let endAction = interpretation.workflowStatus
    if (interpretation.finalized) {
        endAction += ' (Finalized) '
    } else if (interpretation.status === 'Ongoing') {
        endAction += ' (ongoing) '
    } else {
        endAction += ' '
    }

    if (interpretation.userId) {
        return `${endAction} • `
    }
    return endAction
}

export default function InterpretationsSummary({
    interpretations,
}: {
    interpretations: Interpretation[]
}): ReactElement {
    const startedInterpretations = interpretations.filter(
        (interpretation) => interpretation.status !== 'Not started',
    )

    const users = useSelector(selectUsers)

    return (
        <ul>
            {startedInterpretations.map((interpretation, index) => {
                const { dateLastUpdate, userId } = interpretation
                const date = new Date(dateLastUpdate || '')

                const user = userId ? users[userId] : null

                return (
                    <li className="flow" key={interpretation.id}>
                        <span>
                            {index + 1} • {getEndAction(interpretation)}
                        </span>
                        <span className="text-sm font-bold">{user?.abbrevName}</span>
                        <span className="text-sm "> • {format(date, 'yyyy-MM-dd HH:MM')}</span>
                    </li>
                )
            })}
        </ul>
    )
}
