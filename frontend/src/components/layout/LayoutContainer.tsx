import {
    ChevronDownIcon,
    ChevronLeftIcon,
    ChevronRightIcon,
    ChevronUpIcon,
} from '@heroicons/react/24/solid'
import React, { Context, ReactElement, ReactNode, useEffect, useState } from 'react'

type PanelState = 'expanded' | 'collapsed' | 'full' | 'hidden'
type RightPanelState = PanelState
type BottomPanelState = PanelState

interface LayoutContextType {
    sectionBoxAllCollapsed: boolean
    setSectionBoxAllCollapsed: (x: boolean) => void
    rightPanelState: RightPanelState
    setRightPanelState: (state: RightPanelState) => void
    bottomPanelState: BottomPanelState
    setBottomPanelState: (state: BottomPanelState) => void
}

export const LayoutContext = React.createContext<Partial<LayoutContextType>>({})

function useLayoutContext(): LayoutContextType {
    // const [rightPanel, setRightPanel] = React.useState<React.ReactNode | null>(null)
    const [rightPanelState, setRightPanelState] = React.useState<RightPanelState>('hidden')
    const [bottomPanelState, setBottomPanelState] = React.useState<BottomPanelState>('hidden')
    const [sectionBoxAllCollapsed, setSectionBoxAllCollapsed] = useState(false)
    return {
        sectionBoxAllCollapsed,
        setSectionBoxAllCollapsed,
        rightPanelState,
        setRightPanelState,
        bottomPanelState,
        setBottomPanelState,
    }
}

export function LayoutProvider({ children }: { children: ReactNode }): ReactElement {
    const context = useLayoutContext()
    return <LayoutContext.Provider value={context}>{children}</LayoutContext.Provider>
}

export function useLayout(): LayoutContextType {
    return React.useContext(LayoutContext as Context<LayoutContextType>)
}

function RightPanel({ children }: { children: ReactNode }) {
    const { rightPanelState, setRightPanelState } = useLayout()

    // We start with the right panel hidden. When the component loads,
    // if this has content, we show it.
    useEffect(() => {
        if (children) {
            setRightPanelState('expanded')
        }
    }, [])

    function resizeRightPanel(direction) {
        if (direction === 'left') {
            if (rightPanelState === 'expanded') {
                setRightPanelState('full')
            } else {
                setRightPanelState('expanded')
            }
        } else if (rightPanelState === 'expanded') {
            setRightPanelState('collapsed')
        } else {
            setRightPanelState('expanded')
        }
    }

    let chevronClass = 'rounded-l-md right-0 border-r-0'
    let rightPanelHSize = ''
    switch (rightPanelState) {
        case 'expanded':
            rightPanelHSize = 'w-5/12'
            break
        case 'full':
            rightPanelHSize = 'w-full'
            chevronClass = 'rounded-r-md border-l-0'
            break
        default:
            rightPanelHSize = 'w-0'
            break
    }

    if (rightPanelState === 'hidden') return null

    return (
        <aside className={`${rightPanelHSize} flex flex-row`}>
            <div className="relative flex items-center">
                <div
                    className={`${chevronClass} border-ellablue-lighter text-ellablue-darker absolute h-16 w-6 cursor-pointer content-center border bg-white`}
                >
                    <div className="flex flex-col gap-y-1">
                        {['collapsed', 'expanded'].includes(rightPanelState) ? (
                            <ChevronLeftIcon
                                id="expand-right-panel"
                                className="h-6 w-6"
                                onClick={() => resizeRightPanel('left')}
                            />
                        ) : null}
                        {['expanded', 'full'].includes(rightPanelState) ? (
                            <ChevronRightIcon
                                id="collapse-right-panel"
                                className="h-6 w-6"
                                onClick={() => resizeRightPanel('right')}
                            />
                        ) : null}
                    </div>
                </div>
            </div>
            <div className="border-ellablue-lighter w-full overflow-x-hidden overflow-y-scroll border-l p-2">
                {children}
            </div>
        </aside>
    )
}

function BottomPanel({ children }: { children: ReactNode }) {
    const { bottomPanelState, setBottomPanelState } = useLayout()

    // We start with the bottom panel hidden. When the component loads,
    // if it has content, we show it.
    useEffect(() => {
        if (children) {
            setBottomPanelState('expanded')
        }
    }, [])

    function resizeBottomPanel(direction) {
        if (direction === 'up') {
            if (bottomPanelState === 'expanded') {
                setBottomPanelState('full')
            } else {
                setBottomPanelState('expanded')
            }
        } else if (bottomPanelState === 'expanded') {
            setBottomPanelState('collapsed')
        } else {
            setBottomPanelState('expanded')
        }
    }

    let bottomPanelVSize = ''
    let chevronClass = '-top-6 rounded-t-md border-b-0'
    switch (bottomPanelState) {
        case 'expanded':
            bottomPanelVSize = 'bottom-2.5 h-64'
            break
        case 'full':
            bottomPanelVSize = 'top-28 h-full'
            chevronClass = 'top-0 rounded-b-md'
            break
        default:
            bottomPanelVSize = 'bottom-2 h-24'
            break
    }

    if (bottomPanelState === 'hidden') return null

    return (
        <footer className={`fixed  ${bottomPanelVSize} w-full`}>
            <div className="relative flex place-content-center">
                <div
                    className={`absolute h-6 w-16 ${chevronClass} border-ellablue-lighter text-ellablue-darker cursor-pointer border bg-white`}
                >
                    <div className="flex flex-row justify-center gap-x-1">
                        {['collapsed', 'expanded'].includes(bottomPanelState) ? (
                            <ChevronUpIcon
                                id="expand-bottom-panel"
                                className="h-6 w-6"
                                onClick={() => resizeBottomPanel('up')}
                            />
                        ) : null}
                        {['expanded', 'full'].includes(bottomPanelState) ? (
                            <ChevronDownIcon
                                id="collapse-bottom-panel"
                                className="h-6 w-6"
                                onClick={() => resizeBottomPanel('down')}
                            />
                        ) : null}
                    </div>
                </div>
            </div>
            <div className={`${bottomPanelVSize} border-ellablue-lighter border-t bg-white`}>
                {children}
            </div>
        </footer>
    )
}

interface LayoutContainerProps {
    topbar?: React.ReactNode
    mainPanel?: React.ReactNode
    rightPanel?: React.ReactNode
    bottomPanel?: React.ReactNode
}

export default function LayoutContainer({
    topbar,
    mainPanel: main,
    rightPanel: sidebar,
    bottomPanel: toolbar,
}: LayoutContainerProps) {
    const { rightPanelState, setRightPanelState, bottomPanelState, setBottomPanelState } =
        useLayout()

    useEffect(() => {
        if (!sidebar) {
            setRightPanelState('hidden')
        }

        if (!toolbar) {
            setBottomPanelState('hidden')
        }
    })

    let mainHSize = 'w-7/12'
    if (rightPanelState === 'collapsed' || rightPanelState === 'hidden') {
        mainHSize = 'w-full'
    } else if (rightPanelState === 'full') {
        mainHSize = 'hidden'
    }

    let mainBottomPadding = ''
    switch (bottomPanelState) {
        case 'collapsed':
            mainBottomPadding = 'bottom-24'
            break
        case 'hidden':
            mainBottomPadding = 'bottom-0'
            break
        case 'full':
            mainBottomPadding = 'hidden'
            break
        default:
            mainBottomPadding = 'bottom-64'
            break
    }

    return (
        <div className="grid grid-rows-3">
            <header className="bg-ellablue fixed z-10 w-full overflow-visible">{topbar}</header>
            <div className={`fixed top-28 ${mainBottomPadding} w-full`}>
                <div className="flex h-full">
                    <main className={`${mainHSize} overflow-x-hidden overflow-y-scroll p-2`}>
                        {main}
                    </main>
                    {sidebar ? <RightPanel>{sidebar}</RightPanel> : null}
                </div>
            </div>
            {toolbar ? <BottomPanel>{toolbar}</BottomPanel> : null}
        </div>
    )
}
