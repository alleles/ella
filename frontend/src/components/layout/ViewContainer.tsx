import React from 'react'
import { Outlet } from 'react-router-dom'

import { LayoutProvider } from 'components/layout/LayoutContainer'
import TopNavBar from 'components/navbar/TopNavBar'

function ViewContainer() {
    return (
        <>
            <TopNavBar />
            <LayoutProvider>
                <Outlet />
            </LayoutProvider>
        </>
    )
}

export default ViewContainer
