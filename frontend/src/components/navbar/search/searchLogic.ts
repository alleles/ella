import API from 'app/API'

export const fetchGeneOptions = async (text, callback) => {
    if (text === '') {
        callback([])
        return
    }
    try {
        const res = await API.SearchOptions.getGeneOptions(text).then((response) => response.data)
        const options = res.gene.map((entry) => ({ label: entry.symbol, value: entry }))
        callback(options)
    } catch (err) {
        console.error(err)
    }
}

export const fetchUserOptions = async (text, callback) => {
    if (text === '') {
        callback([])
        return
    }
    try {
        const res = await API.SearchOptions.getUserOptions(text).then((response) => response.data)
        const options = res.user.map((entry) => ({
            label: `${entry.first_name} ${entry.last_name}`,
            value: entry,
        }))
        callback(options)
    } catch (err) {
        console.error(err)
    }
}

export const shouldSearch = ({ gene, searchType, searchText, user }): boolean =>
    user?.username || // Always allow search on user
    // Allow search if freetext length > 2 if gene is specified (alleles) or if type is analyses
    (searchText.length > 2 && (gene?.symbol || searchType === 'analyses')) ||
    // Allow search if freetext length > 2 if gene is specified (alleles) or if type is analyses
    (searchText.length > 2 && (gene?.symbol || searchType === 'analyses')) ||
    // Allow search if searching for full HGVSc
    searchText.match(/.*:.*/g)
