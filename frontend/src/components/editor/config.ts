import { IAllProps } from '@tinymce/tinymce-react'

import API from 'app/API'

import { CommentTemplates } from 'types/api/Pydantic'
import { Reference } from 'types/store/Reference'

/* TODO: Change template keys on backend,
 * TinyMCE expects: {title, content, description}
 */
const convertTemplates = (templates: CommentTemplates[] = []) =>
    templates
        .map(({ name: title, template: content, ...rest }) => ({
            title,
            content,
            ...rest,
        }))
        .map((t) => Object.assign(t, { description: '' }))

const styleFormats = [
    {
        title: 'Block Format',
        items: [
            { title: 'Heading 1', block: 'h1' },
            { title: 'Heading 2', block: 'h2' },
            { title: 'Heading 3', block: 'h3' },
            { title: 'Paragraph', block: 'p' },
            { title: 'Preformatted', block: 'code' },
        ],
    },
    {
        title: 'Text Color',
        items: [
            {
                title: 'Red text',
                inline: 'span',
                attributes: { 'data-editor-text-color': 'red' },
            },
            {
                title: 'Green text',
                inline: 'span',
                attributes: { 'data-editor-text-color': 'green' },
            },
            {
                title: 'Blue text',
                inline: 'span',
                attributes: { 'data-editor-text-color': 'blue' },
            },
            {
                title: 'Highlight',
                inline: 'span',
                attributes: { 'data-editor-highlight': 'true' },
            },
            {
                title: 'Red background',
                inline: 'span',
                attributes: { 'data-editor-bg-color': 'red' },
            },
            {
                title: 'Green background',
                inline: 'span',
                attributes: { 'data-editor-bg-color': 'green' },
            },
            {
                title: 'Blue background',
                inline: 'span',
                attributes: { 'data-editor-bg-color': 'blue' },
            },
        ],
    },
]

const generateEditorConfig = (
    templates: CommentTemplates[] | undefined,
    currentUser: string,
    placeholder: string,
    toolbarContainer: string,
    references: Set<Reference>,
): IAllProps['init'] => ({
    skin: false,
    placeholder,
    inline: true,
    height: 200,
    current_user: currentUser,
    plugins:
        'advlist autolink lists link image charmap preview anchor searchreplace visualblocks code fullscreen media table code help wordcount template table references signature',
    toolbar:
        'bold italic underline | bullist numlist | blockquote | styles | link image table | template signature references',
    toolbar_items_size: 'small',
    fixed_toolbar_container: `#${toolbarContainer}`,
    insertdatetime_dateformat: '%Y%m%d',
    paste_block_drop: true,
    paste_data_images: true,
    paste_as_text: false,
    templates: convertTemplates(templates),
    branding: false,
    menubar: false,
    contextmenu: 'link image table imagetools template',
    style_formats: styleFormats,
    images_upload_url: '/api/v1/attachments',
    automatic_uploads: true,
    images_upload_handler: (blobInfo) =>
        new Promise((success) => {
            const file = new File([blobInfo.blob()], blobInfo.filename())
            API.Attachments.post(file).then((result) =>
                success(`/api/v1/attachments/${result.data.id}`),
            )
        }),
    references,
})

export default generateEditorConfig
