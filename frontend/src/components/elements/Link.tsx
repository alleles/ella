import React from 'react'
import { NavLink, NavLinkProps } from 'react-router-dom'

export default function Link({ children, ...props }: NavLinkProps) {
    return (
        <div className="hover:text-ellablue-darkest truncate underline">
            <NavLink {...props}>{children}</NavLink>
        </div>
    )
}
