import React from 'react'

interface Props {
    tag: string
}

function Tag({ tag }: Props) {
    const color = tag === 'WARNING' ? 'ellared-dark' : 'gray-500'
    return (
        <div className={`h-5 rounded-sm border py-0.5 px-1 border-${color}`}>
            <p className={`font-mono text-xs capitalize text-${color} tracking-wide`}>{tag}</p>
        </div>
    )
}

export default Tag
