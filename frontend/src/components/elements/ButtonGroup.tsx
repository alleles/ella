import React, { useState } from 'react'

interface ButtonGroupProps {
    options: string[]
    selected?: string
    onChange?: (value: string) => void
}
export default function ButtonGroup({ options, selected, onChange }: ButtonGroupProps) {
    if (options.length < 2) {
        throw new Error('ButtonGroup requires at least 2 options')
    }

    const [current, setCurrent] = useState(selected)

    const middleOptions = options.slice(1, options.length - 1)
    const commonClasses =
        'relative inline-flex items-center px-4 py-2 border border-ellablue-darker text-sm font-medium shadow-sm hover:shadow-md tracking-wide'
    const selectedClasses = 'text-white bg-ellablue-darkest hover:bg-ellablue-darker'
    const unselectedClasses = 'text-ellagray-700 bg-white hover:bg-ellagray'

    const onClick = (value: string) => {
        setCurrent(value)
        if (onChange) {
            onChange(value)
        }
    }

    return (
        <span className="relative z-0 inline-flex rounded-md shadow-sm">
            <button
                type="button"
                onClick={() => onClick(options[0])}
                className={`${commonClasses} rounded-l-md  ${
                    current === options[0] ? selectedClasses : unselectedClasses
                }`}
            >
                {options[0]}
            </button>
            {middleOptions.map((option) => (
                <button
                    type="button"
                    key={option}
                    onClick={() => onClick(option)}
                    className={`${commonClasses} -ml-px ${
                        current === option ? selectedClasses : unselectedClasses
                    } `}
                >
                    {option}
                </button>
            ))}
            <button
                type="button"
                onClick={() => onClick(options[options.length - 1])}
                className={`${commonClasses} -ml-px rounded-r-md  ${
                    current === options[options.length - 1] ? selectedClasses : unselectedClasses
                }`}
            >
                {options[options.length - 1]}
            </button>
        </span>
    )
}
