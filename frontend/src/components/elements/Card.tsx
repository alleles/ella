import React, { ReactNode } from 'react'

import Anchor from 'components/elements/Anchor'

interface ItemProps {
    link?: string
    label: string
}
interface Props extends ItemProps {
    children: ReactNode
}

function CardItem({ label, link }: ItemProps) {
    if (!link) {
        return <span>{label}</span>
    }
    return (
        <Anchor
            href={link}
            className="hover:text-ellablue-lighter tracking-wider text-white underline"
        >
            {label}
        </Anchor>
    )
}

export function Card({ children, label, link }: Props) {
    let color = 'bg-ellablue-darker'
    if (!children) {
        color = 'bg-ellagray-400'
    }

    const commonStyles = `${color} w-6 p-4 inline-flex justify-center flex-col text-right text-sm font-medium relative text-white uppercase tracking-wider vertical-rl rotate-180 rounded-sm`

    return (
        <div className="bg-ellagray-100 mx-1 mt-2 flex">
            <div className={commonStyles}>
                <CardItem label={label} link={link} />
            </div>
            {children && <div className="h-full w-full p-4">{children}</div>}
        </div>
    )
}
