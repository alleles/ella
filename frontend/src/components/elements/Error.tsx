import React from 'react'

interface Props {
    error: string
}

export default function Error({ error }: Props) {
    if (!error) return null
    return <div className="text-ellared-dark p-1 text-center text-sm">{error}</div>
}
