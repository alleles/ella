import React, { HTMLProps } from 'react'

type Props = HTMLProps<HTMLDivElement> & {
    buttons: { key: string; label: string }[]
    selectedButton: string | null
    onChange: (buttonKey: string) => void
    direction?: 'row' | 'column'
    nullable?: boolean // If set to true, you can click a selected button again to remove the selected option and set thew value to an empty string.
    disabled: boolean
}

export function RadioButtons({
    buttons,
    selectedButton,
    onChange,
    className = '',
    direction = 'column',
    nullable = false,
    disabled,
    ...props
}: Props) {
    const columnDirectionClasses = 'flex-col gap-y-1'

    const onOptionButtonClick = (optionKey: string) => {
        if (nullable && optionKey === selectedButton) {
            onChange('')
        }

        if (optionKey !== selectedButton) {
            onChange(optionKey)
        }
    }

    return (
        <div
            className={`${className} flex ${direction === 'column' ? columnDirectionClasses : ''}`}
            {...props}
        >
            {buttons.map((button) => (
                <button
                    className={`border-ellablue-darker flex w-full justify-center whitespace-nowrap rounded-md border py-2 px-4 text-xs font-medium shadow-sm ${getSpecificButtonClasses(
                        selectedButton === button.key,
                        nullable,
                        disabled,
                    )}`}
                    key={button.key}
                    onClick={() => !disabled && onOptionButtonClick(button.key)}
                >
                    {button.label}
                </button>
            ))}
        </div>
    )
}

const getSpecificButtonClasses = (selected, nullable, disabled) => {
    if (selected) {
        return `bg-ellablue-darkest text-ellagray-100 ${
            !nullable || disabled
                ? 'cursor-default'
                : 'hover:ellablue-darker hover:shadow-md cursor-pointer'
        }`
    }

    return `bg-white text-ellagray-700 ${
        disabled
            ? 'cursor-default'
            : 'focus:outline-none focus:ring-ellablue-darker cursor-pointer hover:bg-ellagray hover:shadow-md'
    }`
}
