import React, { InputHTMLAttributes } from 'react'

interface Props extends InputHTMLAttributes<HTMLInputElement> {
    label?: string
    error?: string
}

export default function Input({ label, error, ...props }: Props) {
    return (
        <div className="flex w-full flex-col">
            {error && <div className="text-ellared-dark p-1 text-sm">{error}</div>}
            <div className="flex w-full">
                {label && (
                    <span className="border-ellagray bg-ellagray-50 text-ellagray-500 inline-flex items-center rounded-l-md border border-r-0 px-3 text-sm">
                        {label}
                    </span>
                )}
                <input
                    className="border-ellagray focus:border-ellablue focus:ring-ellablue disabled:text-ellared block w-full min-w-0 flex-1 rounded-r-md border p-2 text-sm"
                    {...props}
                />
            </div>
        </div>
    )
}
