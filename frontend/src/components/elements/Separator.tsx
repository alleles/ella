import React from 'react'

export default function Separator({ label }: { label: string }) {
    return (
        <div className="bg-ellablue-lightest text-ellagray-900 mb-6 px-2 py-1 text-sm tracking-wider">
            {label}
        </div>
    )
}
