import { Menu as HeadlessUiMenu, Transition } from '@headlessui/react'
import { ChevronDownIcon } from '@heroicons/react/24/solid'
import React, { FC, Fragment, ReactNode } from 'react'

interface Props {
    id?: string
    title: string
    children: ReactNode
}

function Menu({ id, title, children }: Props) {
    return (
        <HeadlessUiMenu as="div" className="relative z-10">
            <div
                id={id}
                className="bg-ellablue-darkest hover:bg-ellablue-darker h-full w-full rounded-md font-medium tracking-wide text-white shadow-sm hover:shadow-md"
            >
                <HeadlessUiMenu.Button className="flex h-full w-full items-center justify-center py-2 px-4 text-sm font-medium tracking-wide ">
                    {title}
                    <ChevronDownIcon className="ml-2 -mr-1 h-5 w-5" aria-hidden="true" />
                </HeadlessUiMenu.Button>
            </div>
            <Transition
                as={Fragment}
                enter="transition ease-out duration-200"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-75"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
            >
                <HeadlessUiMenu.Items className="absolute mt-2 whitespace-nowrap rounded bg-white pb-6 shadow-lg ring-1 ring-black/5 focus:outline-none">
                    {children}
                </HeadlessUiMenu.Items>
            </Transition>
        </HeadlessUiMenu>
    )
}

interface MenuItemProps {
    onClick?
    title
    LeftIcon?: FC<any>
}

function MenuItem({ onClick, title, LeftIcon }: MenuItemProps) {
    return (
        <HeadlessUiMenu.Item>
            {({ active }) => (
                <button
                    className={`${
                        active ? 'bg-ellablue-lightest' : ''
                    } text-md text-ellagray-700 group flex w-full rounded px-6 py-2`}
                    onClick={onClick}
                    type="button"
                >
                    {LeftIcon && <LeftIcon className="mr-2 h-5 w-5" aria-hidden="true" />}
                    {title}
                </button>
            )}
        </HeadlessUiMenu.Item>
    )
}

Menu.MenuItem = MenuItem
export default Menu
