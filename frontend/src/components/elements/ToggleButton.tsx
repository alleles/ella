import React, { ReactNode } from 'react'

interface ToggleButtonProps {
    children: ReactNode
    onClick: (value: boolean) => void
    selected?: boolean
    title?: string
    disabled?: boolean
}

// TODO: Needs more work to make it more generic, such that
// we can use this element component as ToggleButton in TrackControl
export default function ToggleButton({
    children,
    onClick,
    selected = true,
    title = undefined,
    disabled = false,
}: ToggleButtonProps) {
    return (
        <button
            className={`border-ellablue-darker text-ellagray-700 focus:ring-ellablue-darker ml-2 flex shrink justify-center whitespace-nowrap rounded-md border py-2 px-4 text-xs font-medium shadow-sm focus:outline-none ${getSpecificButtonClasses(
                selected,
                disabled,
            )}`}
            onClick={() => onClick(!selected)}
            disabled={disabled}
            title={title}
        >
            {children}
        </button>
    )
}

const getSpecificButtonClasses = (selected, disabled) => {
    if (selected && !disabled)
        return 'bg-ellablue-darkest text-ellagray-100 hover:ellablue-darker hover:shadow-md cursor-pointer'

    if (selected && disabled) return 'bg-ellablue-darkest text-ellagray-100 cursor-default'

    if (!selected && !disabled)
        return 'bg-white text-ellagray-700 hover:bg-ellagray cursor-pointer hover:shadow-md'

    if (!selected && disabled) return 'focus:outline-none cursor-default'
    return ''
}
