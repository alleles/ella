import React, { ReactNode } from 'react'

import Button from 'components/elements/Button'
import Modal from 'components/elements/modals/Modal'

interface Props {
    onOk: () => void
    onCancel: () => void
    children: ReactNode
    okDisabled?: boolean
}

export function OkCancelModal({ onOk, onCancel, children, okDisabled = false }: Props) {
    return (
        <Modal onClose={onCancel}>
            <div className="mb-4">{children}</div>
            <div className="mb-4 flex gap-x-2">
                <Button onClick={onOk} disabled={okDisabled}>
                    OK
                </Button>
                <Button onClick={onCancel}>Cancel</Button>
            </div>
        </Modal>
    )
}
