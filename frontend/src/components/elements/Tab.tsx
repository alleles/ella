import { Tab } from '@headlessui/react'
import React from 'react'

const StyledTabGroup = Tab.Group
const StyledTabPanel = Tab.Panel
const StyledTabPanels = Tab.Panels

function StyledTabList(props: Parameters<typeof Tab.List>[0]) {
    return <Tab.List className="flex h-16" {...props} />
}

function StyledTab(props: Parameters<typeof Tab>[0]) {
    return (
        <Tab
            className={({ selected }) =>
                `text-ellagray-500 inline-flex items-center border-b-2 px-4 pt-1 text-sm font-medium ${
                    selected
                        ? 'border-ellablue-darker text-ellagray-900'
                        : 'hover:border-ellagray hover:text-ellagray-700 border-transparent'
                }`
            }
            {...props}
        />
    )
}

StyledTab.Group = StyledTabGroup
StyledTab.List = StyledTabList
StyledTab.Panel = StyledTabPanel
StyledTab.Panels = StyledTabPanels

export default StyledTab
