import { format } from 'date-fns'
import React from 'react'
import { useSelector } from 'react-redux'

import Link from 'components/elements/Link'
import Tag from 'components/elements/Tag'
import Row from 'components/elements/table/Row'
import Table from 'components/elements/table/Table'

import { selectPriorityText } from 'store/config/configSlice'
import { selectAnalysisById } from 'store/interpretation/analysisSlice'
import { RootState } from 'store/store'

import { OverviewAnalysis } from 'types/store/AnalysisOverview'

import InterpretationsSummary from 'views/overview/components/InterpretationsSummary'

function AnalysisItem({ overviewAnalysis }: { overviewAnalysis: OverviewAnalysis }) {
    const { analysisId, overviewInterpretations, reviewComment, priority, tags } = overviewAnalysis

    const priorityText = useSelector(selectPriorityText)
    const analysis = useSelector((state: RootState) => selectAnalysisById(state, { analysisId }))
    if (analysis) {
        return (
            analysis && (
                <Row>
                    <td className="w-1/12">
                        {format(
                            new Date(analysis.dateRequested || analysis.dateDeposited),
                            'yyyy-MM-dd',
                        )}
                    </td>
                    {Boolean(priority) && (
                        <td className="w-1/12 font-mono text-xs uppercase tracking-wide">
                            {priorityText[priority]}
                        </td>
                    )}
                    <td className="flex gap-x-2 pr-2">
                        <Link to={`/analyses/${analysisId}`}>{analysis.name}</Link>
                        <div className="flex gap-x-1">
                            {tags.map((tag) => (
                                <Tag key={tag} tag={tag} />
                            ))}
                        </div>
                    </td>

                    <td className="font-normal">{reviewComment}</td>
                    <td className="text-right">
                        <InterpretationsSummary interpretations={overviewInterpretations} />
                    </td>
                </Row>
            )
        )
    }
    return <div>foobar</div>
}
interface Props {
    overviewAnalyses?: OverviewAnalysis[]
}

export default function AnalysesTable({ overviewAnalyses = [] }: Props) {
    return (
        <Table className="mb-1 -mt-1 w-full table-fixed">
            {overviewAnalyses.map((overviewAnalysis) => (
                <AnalysisItem
                    overviewAnalysis={overviewAnalysis}
                    key={overviewAnalysis.analysisId}
                />
            ))}
        </Table>
    )
}
