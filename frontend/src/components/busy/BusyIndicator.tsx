import { Dialog } from '@headlessui/react'
import React, {
    Context,
    ReactElement,
    ReactNode,
    createContext,
    useContext,
    useRef,
    useState,
} from 'react'

import Spinner from 'components/elements/Spinner'

interface BusyContext {
    busy: boolean
    message: string
    addWork: <T>(p: Promise<T>, msg: string) => Promise<T>
}

let busyContext: Context<BusyContext>

function useBusyIndicatorHook(): BusyContext {
    const [work, setWork] = useState<Promise<any>[]>([])
    const [message, setMessage] = useState<string>('Loading...')

    return {
        busy: work.length > 0,
        message,
        addWork(p, msg = '') {
            setWork([
                ...work,
                p.finally(() => {
                    setWork(work.filter((w) => w !== p))
                }),
            ])
            setMessage(msg)
            return p
        },
    }
}

function BusyOverlay() {
    const { busy, message } = useBusyIndicator()

    return (
        <Dialog as="div" open={busy} className="relative z-20" onClose={() => {}}>
            <div className="bg-ellapurple-lighter fixed inset-0 bg-opacity-75 " />

            <div className="fixed inset-0 z-10 overflow-y-auto">
                <div className="flex min-h-full items-center justify-center p-4 text-center sm:p-0">
                    <Dialog.Panel className="bg-ellapurple-lightest relative my-8  w-full max-w-sm overflow-hidden rounded-lg py-3 text-left shadow-xl ">
                        <div id="busy-indicator">
                            <div className="mt-3 text-center sm:mt-5">
                                <Dialog.Title
                                    as="h3"
                                    className="text-ellagray-700 text-lg font-medium leading-6 opacity-50"
                                >
                                    {message}
                                </Dialog.Title>
                                <div className="mt-2 h-32">
                                    <Spinner />
                                    <button className="text-white" onClick={() => {}}>
                                        {' '}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </Dialog.Panel>
                </div>
            </div>
        </Dialog>
    )
}

export function BusyProvider({ children }: { children: ReactNode }): ReactElement {
    // We create and assign a ref in order to force HMR to keep the state of the context.
    // Once https://github.com/vitejs/vite/issues/3301 is resolved the ref and assignment can go away.
    const contextRef = useRef()
    const busy = useBusyIndicatorHook()

    // eslint-disable-next-line no-multi-assign
    busyContext = (contextRef.current as any) ??= createContext<Partial<BusyContext>>({})
    return (
        <busyContext.Provider value={busy}>
            <BusyOverlay />
            {children}
        </busyContext.Provider>
    )
}

export default function useBusyIndicator(): BusyContext {
    return useContext(busyContext as Context<BusyContext>)
}
