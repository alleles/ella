import isEqual from 'lodash/isEqual'

import * as ApiTypes from 'types/api/Pydantic'
import { AnalysisInterpretationState } from 'types/store/AnalysisInterpretation'
import { FilteredAlleles, PresentationAllele } from 'types/view/Interpretation'

import { nonNullable } from 'utils/general'

import { InterpretationState } from 'views/interpretation/InterpretationStateContext'

export const prepareAllelePayload = (
    interpretationState: InterpretationState,
    presentationAlleles: Record<number, PresentationAllele>,
): ApiTypes.MarkAlleleInterpretationRequest => {
    if (
        !isEqual(
            Object.keys(presentationAlleles).map(Number).sort(),
            Object.keys(interpretationState.allele).map(Number).sort(),
        )
    ) {
        throw new Error("Allele ids do not match. This shouldn't happen.")
    }

    return {
        allele_ids: Object.keys(presentationAlleles).map(Number),
        alleleassessment_ids: Object.values(interpretationState.allele)
            .filter((alleleState) => alleleState.reuseAlleleAssessment)
            .map((alleleState) => alleleState.alleleAssessmentId)
            .filter(nonNullable),
        annotation_ids: Object.values(presentationAlleles).map(
            (presentationAllele) => presentationAllele.annotationId,
        ),
        custom_annotation_ids: Object.values(presentationAlleles)
            .map((presentationAllele) => presentationAllele.customAnnotationId)
            .filter(nonNullable),
        allelereport_ids: [], // TODO
    }
}

export const prepareAnalysisPayload = (
    interpretationState: AnalysisInterpretationState,
    presentationAlleles: Record<number, PresentationAllele>,
    filteredAlleles: FilteredAlleles,
): ApiTypes.MarkAnalysisInterpretationRequest => ({
    ...prepareAllelePayload(interpretationState, presentationAlleles),
    // Merge cnv and snv filter results (backend doesn't care, or have the tools to figure out the allele type)
    excluded_allele_ids: Object.keys(filteredAlleles.excludedAllelesByCallerType.snv).reduce(
        (acc, filterName) => ({
            ...acc,
            [filterName]: [
                ...filteredAlleles.excludedAllelesByCallerType.snv[filterName],
                ...filteredAlleles.excludedAllelesByCallerType.cnv[filterName],
            ],
        }),
        {},
    ),
    technical_allele_ids: Object.entries(interpretationState.allele)
        .filter(([, alleleState]) => alleleState.analysis.verification === 'technical')
        .map(([alleleId]) => Number(alleleId)),
    notrelevant_allele_ids: Object.entries(interpretationState.allele)
        .filter(([, alleleState]) => alleleState.analysis.notrelevant)
        .map(([alleleId]) => Number(alleleId)),
})
