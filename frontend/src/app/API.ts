import axios, { AxiosResponse } from 'axios'
import React from 'react'

import { prepareAllelePayload, prepareAnalysisPayload } from 'app/api.utils'

import type * as ApiTypes from 'types/api/Pydantic'
import { ACMGCode } from 'types/api/Pydantic'
import {
    AnalysisInterpretation,
    AnalysisInterpretationState,
} from 'types/store/AnalysisInterpretation'
import {
    FilteredAlleles as FilteredAllelesType,
    PresentationAllele,
} from 'types/view/Interpretation'

import { InterpretationState } from 'views/interpretation/InterpretationStateContext'

axios.defaults.withCredentials = true
axios.defaults.timeout = 15_000
axios.defaults.baseURL = '/'

namespace API {
    /* Auth */
    export namespace Login {
        export const path = '/api/v1/users/actions/login/'
        type Response = ApiTypes.ApiModel['empty_response']
        type Request = ApiTypes.ApiModel['login_request']
        export const post = (username: string, password: string) =>
            axios.post<Response, AxiosResponse<Response>, Request>(path, { username, password })
    }

    export namespace Logout {
        export const path = '/api/v1/users/actions/logout/'
        type Response = ApiTypes.ApiModel['empty_response']
        export const post = () => axios.post<Response>(path)
    }

    /* Config and user */
    export namespace Config {
        export const path = '/api/v1/config/'
        type Response = ApiTypes.ApiModel['config_response']
        export const get = () => axios.get<Response>(path)
    }

    export namespace CurrentUser {
        export const path = '/api/v1/users/currentuser/'
        type Response = ApiTypes.ApiModel['user_response']
        export const get = () => axios.get<Response>(path)
    }

    export namespace Users {
        export const path = '/api/v1/users/'
        type Response = ApiTypes.ApiModel['user_list_response']
        export const get = () => axios.get<Response>(path)
    }

    export namespace ChangePassword {
        export const path = '/api/v1/users/actions/changepassword/'
        type Response = ApiTypes.ApiModel['empty_response']
        type Request = ApiTypes.ApiModel['change_password_request']
        export const post = (username, password, newPassword) =>
            axios.post<Response, AxiosResponse<Response>, Request>(path, {
                username,
                password,
                new_password: newPassword,
            })
    }

    /* Overview */
    export namespace NonFinalizedAlleles {
        export const path = '/api/v1/overviews/alleles/'
        type Response = ApiTypes.ApiModel['overview_allele_response']
        export const get = () => axios.get<Response>(path)
    }
    export namespace FinalizedAlleles {
        export const path = '/api/v1/overviews/alleles/finalized/'
        type Response = ApiTypes.ApiModel['overview_allele_finalized_response']
        export const get = () => axios.get<Response>(path)
    }

    export namespace NonFinalizedAnalyses {
        export const path = '/api/v1/overviews/analyses/'
        type Response = ApiTypes.ApiModel['overview_analysis_response']
        export const get = () => axios.get<Response>(path)
    }

    export namespace FinalizedAnalyses {
        export const path = '/api/v1/overviews/analyses/finalized/'
        type Response = ApiTypes.ApiModel['overview_analysis_finalized_response']
        export const get = () => axios.get<Response>(path)
    }

    export namespace SearchOptions {
        const path = '/api/v1/search/options/'

        type UserResponse = Required<Pick<ApiTypes.ApiModel['search_options_response'], 'user'>>
        export const getUserOptions = (phrase: string) => {
            const params = {
                params: { q: JSON.stringify({ user: phrase }) },
            }
            return axios.get<UserResponse>(path, params)
        }

        type GeneResponse = Required<Pick<ApiTypes.ApiModel['search_options_response'], 'gene'>>
        export const getGeneOptions = (phrase: string) => {
            const params = {
                params: { q: JSON.stringify({ gene: phrase }) },
            }
            return axios.get<GeneResponse>(path, params)
        }
    }

    export namespace SearchAllelesAndAnalyses {
        export const path = '/api/v1/search/'
        type Response = ApiTypes.ApiModel['search_response']
        export const get = (
            freetext: string,
            gene: ApiTypes.SearchGene | null,
            user: ApiTypes.SearchUser | null,
            type: 'alleles' | 'analyses',
        ) => {
            const query = {
                freetext,
                gene,
                user,
                type,
            }
            const params = {
                params: { q: JSON.stringify(query), page: 1, per_page: 10, limit: 100 },
            }
            return axios.get<Response>(path, params)
        }
    }

    /* Gene panels */
    export namespace GenePanel {
        export const path = (name: string, version: string) =>
            `/api/v1/genepanels/${name}/${version}/`
        type Response = ApiTypes.ApiModel['gene_panel_response']
        export const get = (name: string, version: string) =>
            axios.get<Response>(path(name, version))
    }

    export namespace GenePanels {
        export const path = '/api/v1/genepanels/'
        type Response = ApiTypes.ApiModel['gene_panel_list_response']
        export const get = () => axios.get<Response>(path)
    }

    /* Allele interpretation */
    export namespace AlleleCollisions {
        export const path = (alleleId: number) =>
            `/api/v1/workflows/alleles/${alleleId}/collisions/`
        type Response = ApiTypes.ApiModel['allele_collision_response']
        export const get = (alleleId: number) =>
            axios.get<Response>(path(alleleId), {
                params: { allele_ids: alleleId },
            })
    }

    export namespace Alleles {
        const path = '/api/v1/alleles/'
        type Response = ApiTypes.ApiModel['allele_list_response']
        export const get = (alleleIds: number[]) =>
            axios.get<Response>(path, {
                params: { q: JSON.stringify({ id: alleleIds }) },
            })
    }

    export namespace AlleleInterpretationList {
        const path = (alleleId: number) => `/api/v1/workflows/alleles/${alleleId}/interpretations/`
        type Response = ApiTypes.ApiModel['allele_interpretation_list_response']
        export const get = (alleleId: number) => axios.get<Response>(path(alleleId))
    }

    export namespace AlleleInterpretationAllele {
        export const path = (alleleId, interpretationId) =>
            `/api/v1/workflows/alleles/${alleleId}/interpretations/${interpretationId}/alleles/`
        type Response = ApiTypes.ApiModel['allele_list_response']
        export const get = (alleleId: number, interpretationId: number) => {
            const params = {
                params: { allele_ids: alleleId },
            }
            return axios.get<Response>(path(alleleId, interpretationId), params)
        }
    }

    export namespace SimilarAlleles {
        export const path = (genePanelName, genePanelVersion) =>
            `/api/v1/workflows/similar_alleles/${genePanelName}/${genePanelVersion}/`
        type Response = ApiTypes.ApiModel['similar_alleles_response']
        export const get = (genePanelName, genePanelVersion, alleleIds: number[]) =>
            axios.get<Response>(path(genePanelName, genePanelVersion), {
                params: { allele_ids: alleleIds.join(',') },
            })
    }

    export namespace AlleleAssessments {
        export const path = '/api/v1/alleleassessments/'
        type Response = ApiTypes.ApiModel['allele_assessment_list_response']
        export const get = (alleleId: number) => {
            const params = {
                params: { q: { allele_id: alleleId } },
            }
            return axios.get<Response>(path, params)
        }
    }

    export namespace AlleleReports {
        export const path = '/api/v1/allelereports/'
        type Response = ApiTypes.ApiModel['allele_report_list_response']
        export const get = (alleleId: number) => {
            const params = { params: { q: { allele_id: alleleId } } }
            return axios.get<Response>(path, params)
        }
    }

    export namespace AlleleInterpreationLogs {
        export const path = (alleleId) => `/api/v1/workflows/alleles/${alleleId}/logs/`
        type Response = ApiTypes.ApiModel['interpretation_log_list_response']
        export const get = (alleleId) => axios.get<Response>(path(alleleId))
        export const post = (alleleId, data) => axios.post<unknown>(path(alleleId), data)
        export const del = (alleleId, messageId) => axios.delete(`${path(alleleId)}${messageId}/`)
        export const patch = (alleleId, messageId, data) =>
            axios.patch(`${path(alleleId)}${messageId}/`, data)
    }

    export namespace StartAlleleInterpretationOrReview {
        export const path = (alleleId) => `/api/v1/workflows/alleles/${alleleId}/actions/start/`
        type Response = ApiTypes.ApiModel['empty_response']
        type Request = ApiTypes.ApiModel['allele_action_start_request']

        export const post = ({ alleleId, genePanelName, genePanelVersion }) =>
            axios.post<Response, AxiosResponse<Response>, Request>(path(alleleId), {
                gp_name: genePanelName,
                gp_version: genePanelVersion,
            })
    }

    export namespace SaveAlleleInterpretation {
        export const path = (alleleId, alleleInterpretationId) =>
            `/api/v1/workflows/alleles/${alleleId}/interpretations/${alleleInterpretationId}/`
        type Response = ApiTypes.ApiModel['empty_response']
        type Payload = ApiTypes.PatchInterpretationRequest
        export const patch = ({
            alleleInterpretationId,
            alleleId,
            alleleInterpretation,
        }: {
            alleleInterpretationId: number
            alleleId: number
            alleleInterpretation: ApiTypes.AlleleInterpretation
        }) =>
            axios.patch<Response, AxiosResponse<Response>, Payload>(
                path(alleleId, alleleInterpretationId),
                {
                    id: alleleInterpretationId,
                    state: alleleInterpretation.state || {},
                    user_state: alleleInterpretation.user_state || {},
                },
            )
    }

    export namespace SubmitAlleleAssessment {
        const path = (id, type) =>
            type === 'allele'
                ? `/api/v1/workflows/alleles/${id}/actions/finalizeallele/`
                : `/api/v1/workflows/analyses/${id}/actions/finalizeallele/`

        type Response = ApiTypes.ApiModel['finalize_allele_interpretation_response']
        type Payload = ApiTypes.FinalizeAlleleRequest
        export const post = (id, type: 'analysis' | 'allele', payload: Payload) =>
            axios.post<Response, AxiosResponse<Response>, Payload>(path(id, type), payload)
    }

    export namespace ReassignAlleleInterpretationToMe {
        const path = (alleleId) => `/api/v1/workflows/alleles/${alleleId}/actions/override/`
        type Response = ApiTypes.ApiModel['empty_response']
        export const post = (alleleId) => axios.post<Response>(path(alleleId))
    }

    export namespace ReopenAlleleInterpretation {
        const path = (alleleId) => `/api/v1/workflows/alleles/${alleleId}/actions/reopen/`
        type Response = ApiTypes.ApiModel['empty_response']
        export const post = (alleleId) => axios.post<Response>(path(alleleId))
    }
    export namespace ChangeAlleleInterpretationWorkflowStatus {
        const path = (
            alleleId: number,
            newWorkflowStatus: ApiTypes.AlleleInterpretationWorkflowStatus,
        ) =>
            `/api/v1/workflows/alleles/${alleleId}/actions/mark${newWorkflowStatus
                .toLowerCase()
                .replaceAll(' ', '')}/`
        type Response = null
        type Payload = ApiTypes.ApiModel['mark_allele_interpretation_request']
        export const post = (
            alleleId: number,
            interpretationState: InterpretationState,
            newWorkflowStatus: ApiTypes.AlleleInterpretationWorkflowStatus,
            presentationAlleles: Record<number, PresentationAllele>,
        ) =>
            axios.post<Response, AxiosResponse<Response>, Payload>(
                path(alleleId, newWorkflowStatus),
                prepareAllelePayload(interpretationState, presentationAlleles),
            )
    }

    export namespace FinalizeAlleleInterpretation {
        const path = (alleleId: number) => `/api/v1/workflows/alleles/${alleleId}/actions/finalize/`
        type Response = null
        type Payload = ApiTypes.ApiModel['mark_allele_interpretation_request']
        export const post = (
            alleleId: number,
            interpretationState: InterpretationState,
            presentationAlleles: Record<number, PresentationAllele>,
        ) =>
            axios.post<Response, AxiosResponse<Response>, Payload>(
                path(alleleId),
                prepareAllelePayload(interpretationState, presentationAlleles),
            )
    }

    /* ACMG */

    export namespace AcmgSuggestions {
        const path = '/api/v1/acmg/alleles/'
        type Response = { [k: number]: { codes: ACMGCode[] } }
        type Request = ApiTypes.ApiModel['acmg_allele_request']

        export const post = (
            genePanelName: string,
            genePanelVersion: string,
            alleleIds: number[],
            referenceAssessments: never[],
        ) => {
            const payload: Request = {
                gp_name: genePanelName,
                gp_version: genePanelVersion,
                allele_ids: alleleIds,
                referenceassessments: referenceAssessments,
            }
            return axios.post<Response, AxiosResponse<Response>, Request>(path, payload)
        }
    }

    export namespace AcmgClassificationSuggestion {
        export const path = '/api/v1/acmg/classifications/'
        type Response = ApiTypes.ApiModel['acmg_classification_response']
        export const get = (codes: string) => {
            const params = {
                params: { codes },
            }
            return axios.get<Response>(path, params)
        }
    }

    /* Annotation */
    export namespace AnnotationConfigs {
        export const path = '/api/v1/annotationconfigs/'
        type Response = ApiTypes.ApiModel['annotation_config_list_response']
        export const get = (annotationConfigId: number) => {
            const params = {
                params: { annotation_config_ids: annotationConfigId },
            }
            return axios.get<Response>(path, params)
        }
    }

    export namespace CustomAnnotations {
        export const path = '/api/v1/customannotations/'
        type Response = ApiTypes.ApiModel['custom_annotation_list_response']
        export const get = (alleleId) => {
            const params = { params: { q: { allele_id: alleleId } } }
            return axios.get<Response>(path, params)
        }
        export const post = (
            alleleId,
            userId,
            newCustomAnnotations: Partial<ApiTypes.CustomAnnotation['annotations']>,
        ) =>
            axios.post<unknown>(path, {
                allele_id: alleleId,
                annotations: newCustomAnnotations,
            })
    }

    export namespace References {
        export const path = '/api/v1/references/'
        type Response = ApiTypes.ApiModel['reference_list_response']
        export const getByIds = (ids: number[]) => {
            const params = { params: { q: JSON.stringify({ id: ids }) } }
            return axios.get<Response>(path, params)
        }
        export const getByPubMedIds = (pubmedIds: number[]) => {
            const params = { params: { q: JSON.stringify({ pubmed_id: pubmedIds }) } }
            return axios.get<Response>(path, params)
        }
    }

    /* Analysis interpretation */
    export namespace Analysis {
        export const path = (analysisId: number) => `/api/v1/analyses/${analysisId}/`
        type Response = ApiTypes.ApiModel['analysis_response']
        export const get = (analysisId: number) => axios.get<Response>(path(analysisId))
    }
    export namespace AnalysisInterpretations {
        export const path = (analysisId: number) =>
            `/api/v1/workflows/analyses/${analysisId}/interpretations/`
        type Response = ApiTypes.ApiModel['analysis_interpretation_list_response']
        export const get = (analysisId: number) => axios.get<Response>(path(analysisId))
    }

    export namespace StartAnalysisInterpretation {
        export const path = (analysisId) =>
            `/api/v1/workflows/analyses/${analysisId}/actions/start/`
        type Response = ApiTypes.ApiModel['empty_response']
        type Request = ApiTypes.ApiModel['empty_request']

        export const post = ({ analysisId }) =>
            axios.post<Response, AxiosResponse<Response>, Request>(path(analysisId))
    }

    export namespace SaveAnalysisInterpretation {
        export const path = (analysisId, analysisInterpretationId) =>
            `/api/v1/workflows/analyses/${analysisId}/interpretations/${analysisInterpretationId}/`
        type Response = ApiTypes.ApiModel['empty_response']
        type Payload = ApiTypes.ApiModel['patch_interpretation_request']
        export const patch = ({
            analysisInterpretationId,
            analysisId,
            analysisInterpretation,
        }: {
            analysisInterpretationId: number
            analysisId: number
            analysisInterpretation: ApiTypes.AnalysisInterpretation
        }) =>
            axios.patch<Response, AxiosResponse<Response>, Payload>(
                path(analysisId, analysisInterpretationId),
                {
                    id: analysisInterpretationId,
                    state: analysisInterpretation.state || {},
                    user_state: analysisInterpretation.user_state || {},
                },
            )
    }

    export namespace ReassignAnalysisInterpretationToMe {
        const path = (analysisId) => `/api/v1/workflows/analyses/${analysisId}/actions/override/`
        type Response = ApiTypes.ApiModel['empty_response']
        export const post = (analysisId) => axios.post<Response>(path(analysisId))
    }

    export namespace ReopenAnalysisInterpretation {
        const path = (analysisId) => `/api/v1/workflows/analyses/${analysisId}/actions/reopen/`
        type Response = ApiTypes.ApiModel['empty_response']
        export const post = (analysisId) => axios.post<Response>(path(analysisId))
    }

    export namespace ChangeAnalysisInterpretationWorkflowStatus {
        const path = (
            analysisId: number,
            newWorkflowStatus: ApiTypes.AnalysisInterpretationWorkflowStatus,
        ) =>
            `/api/v1/workflows/analyses/${analysisId}/actions/mark${newWorkflowStatus
                .toLowerCase()
                .replaceAll(' ', '')}/`
        type Response = ApiTypes.EmptyResponse
        type Payload = ApiTypes.ApiModel['mark_analysis_interpretation_request']
        export const post = (
            analysisId: number,
            interpretationState: AnalysisInterpretationState,
            newWorkflowStatus: ApiTypes.AnalysisInterpretationWorkflowStatus,
            presentationAlleles: Record<string, PresentationAllele>,
            filteredAlleles: FilteredAllelesType,
        ) =>
            axios.post<Response, AxiosResponse<Response>, Payload>(
                path(analysisId, newWorkflowStatus),
                prepareAnalysisPayload(interpretationState, presentationAlleles, filteredAlleles),
            )
    }

    export namespace FinalizeAnalysisInterpretation {
        const path = (analysisId: number) =>
            `/api/v1/workflows/analyses/${analysisId}/actions/finalize/`
        type Response = ApiTypes.EmptyResponse
        type Payload = ApiTypes.ApiModel['mark_analysis_interpretation_request']

        export const post = (
            analysisId: number,
            interpretationState: AnalysisInterpretationState,
            presentationAlleles: Record<string, PresentationAllele>,
            filteredAlleles: FilteredAllelesType,
        ) =>
            axios.post<Response, AxiosResponse<Response>, Payload>(
                path(analysisId),
                prepareAnalysisPayload(interpretationState, presentationAlleles, filteredAlleles),
            )
    }

    export namespace AnalysisFilterConfigs {
        export const path = (analysisId: number) =>
            `/api/v1/workflows/analyses/${analysisId}/filterconfigs`
        type Response = ApiTypes.ApiModel['filter_config_list_response']
        export const get = (analysisId: number) => axios.get<Response>(path(analysisId))
    }

    export namespace AnalysisInterpretationAlleles {
        export const path = (analysisId: number, interpretationId: number) =>
            `/api/v1/workflows/analyses/${analysisId}/interpretations/${interpretationId}/alleles/`
        type Response = ApiTypes.ApiModel['allele_list_response']
        export const get = (
            analysisId: number,
            interpretationId: number,
            alleleIds: number[],
            interpretationStatus: AnalysisInterpretation['status'],
            filterConfigId: number,
        ) => {
            const params = {
                params: {
                    filterconfig_id: filterConfigId,
                    allele_ids: alleleIds.join(','),
                    current: interpretationStatus === 'Not started',
                },
            }
            return axios.get<Response>(path(analysisId, interpretationId), params)
        }
    }

    export namespace AnalysisStats {
        export const path = (analysisId: number) =>
            `/api/v1/workflows/analyses/${analysisId}/stats/`
        type Response = ApiTypes.ApiModel['analysis_stats_response']
        export const get = (analysisId: number) => axios.get<Response>(path(analysisId))
    }

    export namespace AnalysisInterpreationLogs {
        export const path = (analysisId) => `/api/v1/workflows/analyses/${analysisId}/logs/`
        type Response = ApiTypes.ApiModel['interpretation_log_list_response']
        export const get = (analysisId) => axios.get<Response>(path(analysisId))
        export const post = (analysisId, data) => axios.post<unknown>(path(analysisId), data)
        export const del = (analysisId, messageId) =>
            axios.delete(`${path(analysisId)}${messageId}/`)
        export const patch = (analysisId, messageId, data) =>
            axios.patch(`${path(analysisId)}${messageId}/`, data)
    }

    /* Filtering */
    export namespace FilterConfigs {
        export const path = (filterConfigId: number) => `/api/v1/filterconfigs/${filterConfigId}/`
        type Response = ApiTypes.ApiModel['filter_config_response']
        export const get = (filterConfigId: number) => axios.get<Response>(path(filterConfigId))
    }

    export namespace FilteredAlleles {
        export const path = (analysisId: number, interpretationId: number) =>
            `/api/v1/workflows/analyses/${analysisId}/interpretations/${interpretationId}/filteredalleles/`
        type Response = ApiTypes.ApiModel['filtered_alleles_response']
        export const get = (
            analysisId: number,
            interpretationId: number,
            filterConfigId: number,
        ) => {
            const params = {
                params: { filterconfig_id: filterConfigId },
            }
            return axios.get<Response>(path(analysisId, interpretationId), params)
        }
    }

    export namespace AllelesByGene {
        const path = '/api/v1/alleles/by-gene'
        type Response = ApiTypes.ApiModel['allele_gene_list_response']
        export const get = (alleleIds: number[]) => {
            const params = { params: { allele_ids: alleleIds.join(',') } }
            return axios.get<Response>(path, params)
        }
    }

    /* Other */
    export namespace Broadcasts {
        export const path = '/api/v1/broadcasts/'
        type Response = ApiTypes.BroadcastResponse
        export const get = () => axios.get<Response>(path)
    }

    export namespace Attachments {
        export const path = '/api/v1/attachments/'
        type Response = ApiTypes.ApiModel['attachment_list_response']
        export const get = (attachmentIds: number[]) => {
            const params = { params: { q: JSON.stringify({ id: attachmentIds }) } }
            return axios.get<Response>(path, params)
        }

        export const post = (file: File) => {
            const form = new FormData()
            form.append('file', file)
            const config = {
                headers: {
                    'Content-Type': 'multipart/form-data',
                },
            }
            return axios.post<{ id: number }>(`${path}/upload/`, form, config)
        }

        export const downloadPath = (id) => `${path}/${id}`
    }

    export namespace Exception {
        export const path = '/api/v1/ui/exceptionlog/'
        type Response = unknown
        export const post = (error: Error, errorInfo: React.ErrorInfo, state) =>
            axios.post<Response>(path, {
                message: `${error.name}: ${error.message}`,
                location: `${window.location.pathname}`,
                stacktrace: error.stack,
                componentstack: errorInfo.componentStack,
                state,
            })
    }

    export namespace TrackConfigs {
        const path = (analysisId) => `/api/v1/igv/tracks/${analysisId}/`
        type Response = any
        export const get = (analysisId: number, alleleIds: string[]) => {
            const params = { params: { allele_ids: alleleIds.join(',') } }
            return axios.get<Response>(path(analysisId), params)
        }
    }
}

export default API
