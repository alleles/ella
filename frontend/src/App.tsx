import React from 'react'
import { Navigate, Route, BrowserRouter as Router, Routes, useLocation } from 'react-router-dom'

import ErrorBoundary from 'app/ErrorBoundary'
import useAuth, { AuthProvider } from 'app/auth'

import { BusyProvider } from 'components/busy/BusyIndicator'
import ViewContainer from 'components/layout/ViewContainer'

import AlleleInterpretation from 'views/interpretation/AlleleInterpretation'
import AnalysisInterpretation from 'views/interpretation/AnalysisInterpretation'
import ChangePassword from 'views/login/ChangePassword'
import Login from 'views/login/Login'
import Overview from 'views/overview/Overview'
import Profile from 'views/profile/Profile'
import StyleGuide from 'views/styleguide/StyleGuide'

function AuthenticatedRoute() {
    const { authenticated } = useAuth()
    const location = useLocation()

    return authenticated ? (
        <ViewContainer />
    ) : (
        <Navigate to={{ pathname: '/login' }} state={{ from: location }} />
    )
}
export default function App() {
    return (
        <ErrorBoundary>
            <Router>
                <BusyProvider>
                    <AuthProvider>
                        <Routes>
                            <Route path="/" element={<AuthenticatedRoute />}>
                                <Route path="/" element={<Navigate to="/overview/analyses" />} />
                            </Route>

                            <Route path="/login" element={<Login />} />
                            <Route path="/change_password" element={<ChangePassword />} />

                            <Route path="/overview/*" element={<AuthenticatedRoute />}>
                                <Route path=":overviewType" element={<Overview />} />
                            </Route>

                            <Route path={'/profile/*'} element={<AuthenticatedRoute />}>
                                <Route path="/profile/*" element={<Profile />} />
                            </Route>

                            <Route path="/variants/:alleleId" element={<AuthenticatedRoute />}>
                                <Route
                                    path="/variants/:alleleId"
                                    element={<AlleleInterpretation />}
                                />
                            </Route>

                            <Route path="/analyses/:analysisId" element={<AuthenticatedRoute />}>
                                <Route
                                    path="/analyses/:analysisId"
                                    element={<AnalysisInterpretation />}
                                />
                            </Route>
                            <Route path="/styleguide" element={<AuthenticatedRoute />}>
                                <Route path="/styleguide" element={<StyleGuide />} />
                            </Route>
                            <Route path="*" element={<Navigate to="/overview/analyses" />} />
                        </Routes>
                    </AuthProvider>
                </BusyProvider>
            </Router>
        </ErrorBoundary>
    )
}
