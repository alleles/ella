import isEqual from 'lodash/isEqual'

export function sleep(ms: number): Promise<void> {
    return new Promise((resolve) => {
        setTimeout(resolve, ms)
    })
}

export const flushPromises = () =>
    new Promise((resolve) => {
        setTimeout(resolve)
    })

const counter = {}
let prevCounter = {}
export const CallCounter = (name) => {
    if (counter[name] === undefined) {
        counter[name] = 0
    }
    counter[name] += 1
}

setInterval(() => {
    if (isEqual(counter, prevCounter)) {
        return
    }
    // eslint-disable-next-line
    console.log(counter)
    prevCounter = JSON.parse(JSON.stringify(counter))
}, 5000)
