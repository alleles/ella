export function numberFormat(
    input: any,
    numberPrecision?: number,
    scientificThresholdValue?: number,
) {
    const precision = numberPrecision === undefined ? 6 : numberPrecision
    const scientificThreshold =
        scientificThresholdValue === undefined ? 4 : scientificThresholdValue
    if (input === null) {
        return ''
    }
    if (Number.isNaN(input)) {
        return input
    }
    if (Number.isInteger(input)) {
        return input
    }
    if (input < 10 ** -scientificThreshold) {
        return input.toExponential(precision - scientificThreshold + 1)
    }
    return input.toFixed(precision)
}

export function booleanToYesNo(input: boolean) {
    return input ? 'Yes' : 'No'
}
