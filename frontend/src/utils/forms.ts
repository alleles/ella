import { SchemaOf } from 'yup'

interface ValidationResult {
    valid: boolean
    errors: Record<string, string>
}
// Validates form data using yup and returns a map of errors for the fields.
export default function validateSchema(
    schema: SchemaOf<any>,
    data: Record<string, any>,
): ValidationResult {
    try {
        schema.validateSync(data, { abortEarly: false })
    } catch (errors: any) {
        const errorMap = errors.inner.reduce((acc, err) => {
            acc[err.path] = err.message
            return acc
        }, {})
        return { valid: false, errors: errorMap }
    }
    return { valid: true, errors: {} }
}
