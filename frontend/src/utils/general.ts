import cloneDeep from 'lodash/cloneDeep'
import mergeWith from 'lodash/mergeWith'

export const deepCopy = <T extends Object>(obj: T): T => cloneDeep(obj)

// We modify lodash's mergeWith to handle merging arrays
// by replacing the original with the modified array.
export const deepMerge = (objValue, srcValue) =>
    mergeWith(objValue, srcValue, (obj, src) => {
        if (Array.isArray(src)) {
            return src
        }
        return undefined
    })

export function compareStrings(a: string | JSX.Element, b: string | JSX.Element) {
    if (a < b) {
        return -1
    }
    if (a > b) {
        return 1
    }
    return 0
}

// https://stackoverflow.com/a/58110124
export function nonNullable<T>(value: T): value is NonNullable<T> {
    return value !== null && value !== undefined
}
