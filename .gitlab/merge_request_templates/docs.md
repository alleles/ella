## Description

Update docs for [ELLA version].

## Notes to review

## Checklist

-   [ ] Updated version in `backend/pyproject.toml`

## Related issues/MRs

[Docs issue]

[List related MRs]

/label ~"code review::needs"
