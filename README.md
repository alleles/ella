<div align="center" style="padding-bottom: 20px">
  <a href="http://allel.es">
    <img width="350px" style="border: 0;" src="/docs/logo/ella_logo_blue.svg"/>
  </a>
</div>

[![Latest Release](https://gitlab.com/alleles/ella/-/badges/release.svg)](https://gitlab.com/alleles/ella/-/releases)
[![dev pipeline status](https://gitlab.com/alleles/ella/badges/dev/pipeline.svg)](https://gitlab.com/alleles/ella/-/commits/dev)

ELLA is an analysis tool for interpretation of genetic variants, see the [official website](http://allel.es) for more information.

### Setup

For details on how to setup ELLA for demo, production, development and/or testing purposes, please see the [technical documentation](http://allel.es/docs/technical/setup.html).

<!-- prettier-ignore -->
**NOTE**: ELLA relies on a separate annotation service, [ella-anno](https://gitlab.com/alleles/ella-anno), to annotate and import data. The [documentation for this service](http://allel.es/anno-docs) is work in progress, please contact [ella-support](ma&#105;lt&#111;&#58;&#101;%6&#67;la&#37;2&#68;s&#117;pport&#64;m&#101;&#100;i&#115;&#105;&#110;&#46;%75i%&#54;F&#46;n%&#54;F) for details on how to configure your own production setup.

### Contact

<!-- prettier-ignore -->
For support and suggestions, please contact [ella-support](ma&#105;lt&#111;&#58;&#101;%6&#67;la&#37;2&#68;s&#117;pport&#64;m&#101;&#100;i&#115;&#105;&#110;&#46;%75i%&#54;F&#46;n%&#54;F).

### License and copyright

ELLA  
Copyright (C) 2018-2024 ELLA contributors

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
