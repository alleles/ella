import os

TAG = os.environ.get("TAG")

gitlab_project = "alleles"
gitlab_repo = "ella"

# derived globals
url_root = f"https://gitlab.com/{gitlab_project}/{gitlab_repo}"
docker_root = f"registry.gitlab.com/{gitlab_project}/{gitlab_repo}"


def gen_release_notes() -> str:
    """Generate release notes with docker image details and contents from existing docs"""
    # this is non-portable, so replace if using script with a diff repo
    notes_text = [
        "#### Docker Images\n\n",
        f"{docker_root}/ella-backend:{TAG}\n\n",
        f"{docker_root}/ella-frontend-legacy:{TAG}\n\n",
    ]

    from urllib.request import urlopen

    # get the release notes from the tag
    readme = urlopen(f"{url_root}/raw/{TAG}/docs/releasenotes/README.md")

    skip_lines = True
    for line in readme:
        line = line.decode()
        if line.startswith("### Highlights") and skip_lines:
            skip_lines = False
        elif line.startswith("## Version") and skip_lines is False:
            # only include most recent notes
            break

        if skip_lines is False:
            notes_text.append(
                line.replace(
                    "./img/",
                    f"{url_root}/raw/{TAG}/docs/releasenotes/img/",
                )
            )
    return "".join(notes_text)


if __name__ == "__main__":
    assert TAG, "TAG env var not set"
    print(gen_release_notes())
