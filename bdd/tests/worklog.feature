Feature: Making changes in allele interpretation Worklog

  Background:
    Given state "test-bdd"

  Scenario: Make changes related to Variants Overview
    Given I am logged in as "testuser1"
    When I view the allele with id 848
    Then I should see "Worklog"

    # Add a Variants Overview comment
    When I press "Worklog"
    And I clear field "overview-comment"
    And I fill in "overview-comment" with "Test Overview comment"
    And I press "Save"

    # Change Variant Priority to Urgent
    And I select "3" from "priority"

    # See changes in system messages
    And I check interpretation-log-messages-only
    Then I should see "$fullname"
    And I should see an element with xpath "//*/*[contains(text(), 'Overview comment changed to')]/../*[contains(text(), 'Test Overview comment')]"
    And I should see an element with xpath "//*/*[contains(text(), 'Priority changed to')]/../*[contains(text(), 'Urgent')]"

    # See changes in Variants Overview
    When I visit "overview/variants"
    Then I should see "Test Overview comment"
    Then I should see "URGENT"

    # Check that changes are visible to other user
    Given I am logged in as "testuser2"
    When I visit "overview/variants"
    Then I should see "Test Overview comment"
    Then I should see "URGENT"

  Scenario: Make changes to allele interpretation Worklog messages
    Given I am logged in as "testuser1"
    When I view the allele with id 848
    And I press "Worklog"

    # Add and check an interpretation Worklog message
    And I type "Test message delete" into the editor with xpath "//div[@id='new-worklog-message-editor-content']"
    And I press "Add"
    Then I should see "$fullname"
    And I should see "Test message delete"

    # Check that message can be deleted
    When I press "Delete"
    And I press "Confirm"
    Then I should not see "Test message delete"

    # # Edit and check a Worklog message
    When I type "Test message" into the editor with xpath "//div[@id='new-worklog-message-editor-content']"
    And I press "Add"
    Then I should see "Test message"

    When I type " edited" into the editor with xpath "//ul[@id='worklog-messages']//div[contains(@id, 'editor-content')]"
    And I press "Update"
    Then I should see "Test message edited"

    # Check that changes are visible to other user
    Given I am logged in as "testuser2"
    When I view the allele with id 848
    And I press "Worklog"
    Then I should see "Test message edited"
    And I should not see "Test message delete"

  Scenario: Make changes related to Analysis Overview
    Given I am logged in as "testuser1"
    When I view the analysis with id 4
    Then I should see "Worklog"

    # Add an Analysis Overview comment
    When I press "Worklog"
    And I clear field "overview-comment"
    And I fill in "overview-comment" with "Test Overview comment"
    And I press "Save"

    # Change Variant Priority to Urgent
    And I select "3" from "priority"

    # See changes in system messages
    And I check interpretation-log-messages-only
    Then I should see "$fullname"
    And I should see an element with xpath "//*/*[contains(text(), 'Overview comment changed to')]/../*[contains(text(), 'Test Overview comment')]"
    And I should see an element with xpath "//*/*[contains(text(), 'Priority changed to')]/../*[contains(text(), 'Urgent')]"

    # See changes in Variants Overview
    When I visit "overview/analyses"
    Then I should see "Test Overview comment"
    Then I should see "URGENT"

    # Check that changes are visible to other user
    Given I am logged in as "testuser2"
    When I visit "overview/analyses"
    Then I should see "Test Overview comment"
    Then I should see "URGENT"

  Scenario: Make changes to analysis interpretation Worklog messages
    Given I am logged in as "testuser1"
    When I view the analysis with id 4
    And I press "Worklog"

    # Add and check an interpretation Worklog message
    And I type "Test message delete" into the editor with xpath "//div[@id='new-worklog-message-editor-content']"
    And I press "Add"
    Then I should see "$fullname"
    And I should see "Test message delete"

    # Check that message can be deleted
    When I press "Delete"
    And I press "Confirm"
    Then I should not see "Test message delete"

    # # Edit and check a Worklog message
    When I type "Test message" into the editor with xpath "//div[@id='new-worklog-message-editor-content']"
    And I press "Add"
    Then I should see "Test message"

    When I type " edited" into the editor with xpath "//ul[@id='worklog-messages']//div[contains(@id, 'editor-content')]"
    And I press "Update"
    Then I should see "Test message edited"

    # Check that changes are visible to other user
    Given I am logged in as "testuser2"
    When I view the analysis with id 4
    And I press "Worklog"
    Then I should see "Test message edited"
    And I should not see "Test message delete"
