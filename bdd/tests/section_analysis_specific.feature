Feature: View and edit Analysis specific section

  Background:
    Given state "test-bdd"

  Scenario: Check Analysis specific section
    Given I am logged in as "testuser1"
    When I view the analysis with id 4
    Then I should see "ANALYSIS SPECIFIC"
    And I should see "QUALITY"

    # Can edit comment field
    When I start the interpretation
    And I type "Test Analysis specific comment" into the editor with xpath "//div[@id='analysis-specific-comment-editor-content']"
    Then I should see "Test Analysis specific comment"

    # Check annotation for 2 variants
    # BRCA2 c.10G>T
    When I select the allele row 0 from the allele table
    Then I should see "- REF (G)"
    And I should not see "NEEDS VERIFICATION"
    # BRCA2 c.51_52del
    When I select the allele row 1 from the allele table
    Then I should see "- REF (GAC)"
    And I should see "NEEDS VERIFICATION"

# TODO: Check that comment is stored correctly and visible in old frontend; depends on #205
