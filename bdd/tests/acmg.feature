Feature: ACMG criteria

  Background:
    Given state "test-bdd"
    And I am logged in as "testuser1"

  Scenario: Adding/removing an ACMG criterion
    When I view the analysis with id 5
    And I start the interpretation

    # Add criterion
    And I add the ACMG criterion "PS2"
    Then I should see "PS2"
    And I should see "De novo (confirmed)"

    # Duplicates not allowed
    When I add the ACMG criterion "PS2"
    Then I should see "ACMG criterion has already been added"

    # Strengthen criterion
    When I strengthen the ACMG criterion "PS2"
    Then I should see "PS2 VERY STRONG"

    When I weaken the ACMG criterion "PVSxPS2"
    And I weaken the ACMG criterion "PS2"
    Then I should see "PS2 MODERATE"

    # Bring it back to normal
    When I strengthen the ACMG criterion "PMxPS2"
    # Remove criterion
    And I remove the ACMG criterion "PS2"
    Then I should not see "PS2"
    And I should not see "De novo (confirmed)"
