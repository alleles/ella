# TODO: Several disabled steps pending ella-testdata#13

Feature: Overview filter

  Background:
    Given state "test-bdd"
    # testuser7 has access to all analyses
    And I am logged in as "testuser7"

  Scenario: Filter on name, comment, date, type, priority

    # Preparations

    # Add Overview comment and change priority to High for one analysis (brca_sample_2.HBOCUTV_v01)
    When I view the analysis with id 6
    And I press "Worklog"
    And I fill in "overview-comment" with "Test Overview comment"
    And I select "2" from "priority"
    And I press "Save"

    ## Add Overview comment and change priority to Urgent for one analysis (brca_sample_3.HBOCUTV_v01)
    When I view the analysis with id 8
    And I press "Worklog"
    And I fill in "overview-comment" with "Test another Overview comment"
    And I select "3" from "priority"
    And I press "Save"

    When I visit "overview/analyses"

    # Filter: Analysis name

    When I go to the overview
    And I fill in "analysisText" with "cil"
    Then I should see "NA12878.Ciliopati_v05"
    But I should not see "HG002-Trio.Mendeliome_v01"
    And I should not see "brca_long_variants.HBOCUTV_v01"
    And I should not see "brca_sample_1.HBOCUTV_v01"
    And I should not see "brca_sample_1.HBOC_v01"
    And I should not see "brca_sample_2.HBOCUTV_v01"
    And I should not see "brca_sample_2.HBOC_v01"
    And I should not see "brca_sample_3.HBOCUTV_v01"
    And I should not see "brca_sample_allfiltered.HBOC_v01"
    And I should not see "brca_sample_master.HBOCUTV_v01"

    # Test reset button (also resets for next step)

    When I press "Reset filter"
    Then I should see "HG002-Trio.Mendeliome_v01"
    And I should see "NA12878.Ciliopati_v05"
    And I should see "brca_long_variants.HBOCUTV_v01"
    And I should see "brca_sample_1.HBOCUTV_v01"
    And I should see "brca_sample_1.HBOC_v01"
    And I should see "brca_sample_2.HBOCUTV_v01"
    And I should see "brca_sample_2.HBOC_v01"
    And I should see "brca_sample_3.HBOCUTV_v01"
    And I should see "brca_sample_allfiltered.HBOC_v01"
    And I should see "brca_sample_master.HBOCUTV_v01"

    # Filter: Overview comment

    When I fill in "analysisText" with "test"
    Then I should see "brca_sample_2.HBOCUTV_v01"
    And I should see "brca_sample_3.HBOCUTV_v01"
    But I should not see "HG002-Trio.Mendeliome_v01"
    And I should not see "NA12878.Ciliopati_v05"
    And I should not see "brca_long_variants.HBOCUTV_v01"
    And I should not see "brca_sample_1.HBOCUTV_v01"
    And I should not see "brca_sample_1.HBOC_v01"
    And I should not see "brca_sample_2.HBOC_v01"
    And I should not see "brca_sample_allfiltered.HBOC_v01"
    And I should not see "brca_sample_master.HBOCUTV_v01"

    When I fill in "analysisText" with "another"
    Then I should see "brca_sample_3.HBOCUTV_v01"
    But I should not see "brca_sample_2.HBOCUTV_v01"

    # Filter: Date requested; testing filter result not possible, only check that options are available
    When I press "Reset filter"
    When I select "l3d" from "dateRequested"
    When I select "l1w" from "dateRequested"
    When I select "l1m" from "dateRequested"
    When I select "l3m" from "dateRequested"
    When I select "g3m" from "dateRequested"

    # Filter on HTS / Sanger
    ## Reset; Uncheck "HTS", see "Sanger" only
    When I press "Reset filter"
    And I press the element with id "hts"
    # DISABLED pending ella-testdata#13 // Then I should see "[sanger sample]"
    Then I should not see "HG002-Trio.Mendeliome_v01"
    And I should not see "NA12878.Ciliopati_v05"
    And I should not see "brca_long_variants.HBOCUTV_v01"
    And I should not see "brca_sample_1.HBOCUTV_v01"
    And I should not see "brca_sample_1.HBOC_v01"
    And I should not see "brca_sample_2.HBOCUTV_v01"
    And I should not see "brca_sample_2.HBOC_v01"
    And I should not see "brca_sample_3.HBOCUTV_v01"
    And I should not see "brca_sample_allfiltered.HBOC_v01"
    And I should not see "brca_sample_master.HBOCUTV_v01"

    ## Uncheck "Sanger", check "HTS", see "HTS" only
    When I press the element with id "sanger"
    And I press the element with id "hts"
    Then I should see "HG002-Trio.Mendeliome_v01"
    And I should see "NA12878.Ciliopati_v05"
    And I should see "brca_long_variants.HBOCUTV_v01"
    And I should see "brca_sample_1.HBOCUTV_v01"
    And I should see "brca_sample_1.HBOC_v01"
    And I should see "brca_sample_2.HBOCUTV_v01"
    And I should see "brca_sample_2.HBOC_v01"
    And I should see "brca_sample_3.HBOCUTV_v01"
    And I should see "brca_sample_allfiltered.HBOC_v01"
    And I should see "brca_sample_master.HBOCUTV_v01"
    # DISABLED pending ella-testdata#13 // But I should not see "[sanger sample]"

    # Test filter on priority

    ## Uncheck "Normal", see "High" and "Urgent" only
    When I press the element with id "normal"
    Then I should see "brca_sample_2.HBOCUTV_v01"
    And I should see "brca_sample_3.HBOCUTV_v01"
    Then I should not see "brca_sample_1.HBOCUTV_v01"

    ## Uncheck "High", see "Urgent" only
    When I press the element with id "high"
    Then I should see "brca_sample_3.HBOCUTV_v01"
    Then I should not see "brca_sample_1.HBOCUTV_v01"
    And I should not see "brca_sample_2.HBOCUTV_v01"

    ## Uncheck "Urgent", check "Normal", see "Normal" only
    When I press the element with id "urgent"
    And I press the element with id "normal"
    Then I should see "brca_sample_1.HBOCUTV_v01"
    But I should not see "brca_sample_2.HBOCUTV_v01"
    And I should not see "brca_sample_3.HBOCUTV_v01"
