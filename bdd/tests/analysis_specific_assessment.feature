Feature: Analysis specific allele assessments
  Background:
    Given state "test-bdd"

  Scenario: Mark a variant as Technical, Verified or Not Relevant
    Given I am logged in as "testuser1"

    # Test that buttons are disabled before Start, using c.10G>T and allele table tabs
    # TODO: Test "Verified", depends on #150 (tags in table)
    When I view the analysis with id 4
    And I press "Technical"
    Then I should see "c.10G>T"

    When I select the "Technical" tab
    Then I should not see "c.10G>T"

    When I select the "Unclassified" tab
    And I press "Not Relevant"
    Then I should see "c.10G>T"

    When I select the "Not Relevant" tab
    Then I should not see "c.10G>T"

    # Test that buttons can be set and unset after Start
    When I start the interpretation
    And I select the "Unclassified" tab

    ## Set Technical
    When I press "Technical"
    Then I should not see "c.10G>T"

    When I select the "Technical" tab
    Then I should see "c.10G>T"

    ## Unset Technical
    When I press "Technical"
    Then I should not see "c.10G>T"

    When I select the "Unclassified" tab
    Then I should see "c.10G>T"

    ## Set Verified after Technical; test that they are mutually exclusive
    When I press "Technical"
    And I select the "Technical" tab
    And I press "Verified"
    Then I should not see "c.10G>T"

    When I select the "Unclassified" tab
    Then I should see "c.10G>T"

    ## Set Not Relevant
    When I press "Not Relevant"
    Then I should not see "c.10G>T"

    When I select the "Not Relevant" tab
    Then I should see "c.10G>T"

    ## Unset Not Relevant
    When I press "Not Relevant"
    Then I should not see "c.10G>T"

    When I select the "Unclassified" tab
    Then I should see "c.10G>T"

    # Test that buttons can be set for Classified variant; using c.97G>T that overlaps with analysis id 10
    When I select the allele row 4 from the allele table
    And I submit the class as "Class 3"
    And I select the "Classified" tab
    And I press "Technical"
    Then I should not see "c.97G>T"

    When I select the "Technical" tab
    Then I should see "c.97G>T"

    When I press "Verified"
    Then I should not see "c.97G>T"

    When I select the "Classified" tab
    Then I should see "c.97G>T"

    When I press "Not Relevant"
    Then I should not see "c.97G>T"

    When I select the "Not Relevant" tab
    Then I should see "c.97G>T"

    # Preparation for next step: set Technical for additional variant c.72A>T and send to Review
    When I select the "Unclassified" tab
    And I select the allele row 3 from the allele table
    And I press "Technical"
    ## TODO: This can be removed when #258 is resolved (autosave on Finish)
    And I press "Save"
    ## TODO: change to using step after !179 is merged
    And I press "Finish"
    And I press "Review"
    And I press "OK"

    # Test that choices are visible for other user for same analysis: Should now see:
    # - c.10G>T Verified (Unclassified tab)
    # - c.72A>T Technical (Technical tab)
    # - c.97G>T Class 3 + Not Relevant + Verified (Not Relevant tab)
    Given I am logged in as "testuser2"

    When I view the analysis with id 4
    Then I should see "c.10G>T"
    But I should not see "c.72A>T"
    And I should not see "c.97G>T"

    When I select the "Technical" tab
    Then I should see "c.72A>T"

    When I select the "Not Relevant" tab
    Then I should see "c.97G>T"

    # Test that choices are NOT visible for same variant in other analysis
    When I view the analysis with id 10
    When I select the "Classified" tab
    Then I should see "c.97G>T"

    When I select the "Not Relevant" tab
    Then I should not see "c.97G>T"
