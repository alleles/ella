from behave import step
from behaving.personas.persona import persona_vars


@step('I press the element with id "{element_id}"')
@persona_vars
def i_press_id(context, element_id: str):
    button = context.browser.find_by_id(element_id)
    assert button, "Element not found"
    button.first.click()
