from behave import given, when


@when("I go to home")
def go_home(context):
    context.browser.visit(context.base_url)
    # import time
    # time.sleep(10)
    # context.browser.visit("http://nginx:8080")
    # time.sleep(10)
    # context.browser.visit("http://nginx:80")
    # raise RuntimeError(f"I am here: {context.browser.url} ({context.base_url})")
    context.browser.driver.maximize_window()


@when("I log in")
@when('I log in with the password "{password}"')
def login(context, password: str | None = None):
    assert context.persona is not None, "Persona is not set"
    if password is None:
        password = context.persona["password"]

    context.execute_steps(
        f"""
        When I go to home
        Then I should see "Sign in to your account"
        When I fill in "username" with "$username"
        And I fill in "password" with "{password}"
        And I press "Sign in"
        """
    )


@when("I log out")
def logout(context):
    context.execute_steps(
        """
        When I press "$abbrev_name"
        And I press "Logout"
        """
    )


@given("I am logged in")
@given('I am logged in as "{username}"')
def logged_in(context, username: str | None = None):
    context.execute_steps(f'Given "{username}" as the persona')
    if not context.browser.find_by_id(
        "user-menu", 0
    ):  # is_text_present(context.persona["abbrev_name"]):
        context.execute_steps(
            f"""
            When I log in
            When I name the current window "{username}"
            Then I should see "$abbrev_name"
            """
        )
    # Need to explicitly focus correct window for animations to run
    context.execute_steps(
        f"""
        When I switch to the window named "{username}"
        """
    )


@when("I invoke the debugger")
def debug(context):
    import pdb

    pdb.set_trace()
