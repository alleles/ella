from behave import when


@when('I type "{text}" into the editor with xpath "{xpath}"')
def step_impl(context, xpath: str, text: str):
    xpath_expr = f"{xpath}//div[@contenteditable='true']"
    editor = context.browser.find_by_xpath(xpath_expr).first
    for key in text:
        editor._element.send_keys(key)
    context.browser.execute_script("document.activeElement.blur()")
