import requests
from behave import given

STATE_API_URL = "http://testdata:23232"


@given('state "{state}"')
def load_state(context, state: str):
    res = requests.get(f"{STATE_API_URL}/reset", params={"testset": state})
    assert res.status_code == 200, "Failed to load state"
