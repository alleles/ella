Feature: Logging in and out from ELLA

  Background:
    Given state "test-bdd"

  Scenario: Successfully logging in
    Given I am logged in as "testuser1"
    Then I should see "$abbrev_name"

  Scenario: Successfully logging out
    Given I am logged in as "testuser1"
    When I log out
    Then I should see "Sign in to your account"

  Scenario: Unsuccessfully logging in
    Given "testuser1" as the persona
    When I go to home
    Then I should see "Sign in to your account"

    When I press "Sign in"
    Then I should see "Username is a required field"

    When I fill in "username" with "some_user"
    And I press "Sign in"
    Then I should see "Password is a required field"

    When I log in with the password "wrongpassword"
    Then I should see "Invalid username or password"

    When I log in
    Then I should see "$abbrev_name"
