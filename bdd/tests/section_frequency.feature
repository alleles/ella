Feature: View and edit Frequency section

  Background:
    Given state "test-bdd"

  Scenario: Check Frequency section
    Given I am logged in as "testuser1"
    When I view the analysis with id 4
    Then I should see "FREQUENCY"
    And I should see "GNOMAD EXOMES"
    And I should see "GNOMAD GENOMES"
    And I should see "INDB"

    # Can edit comment field
    When I start the interpretation
    And I type "Test Frequency comment" into the editor with xpath "//div[@id='frequency-comment-editor-content']"
    Then I should see "Test Frequency comment"

    # Check annotation for 2 variants
    # BRCA2 c.72A>T
    When I select the allele row 3 from the allele table
    Then I should see "22286"
    And I should see "245534"
    # BRCA2 c.198A>G
    When I select the allele row 5 from the allele table
    Then I should see "33580"
    And I should see "111674"
