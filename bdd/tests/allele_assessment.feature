Feature: Allele assessments
  Background:
    Given state "test-bdd"

  Scenario: Creating an allele assessment
    Given I am logged in as "testuser1"
    When I view the allele with id 848
    And I start the interpretation

    # Add criterion
    And I add the ACMG criterion "PS2"
    Then I should see "PS2"
    And I should see "De novo (confirmed)"

    # Select and submit a class
    When I set the class to "Class 4"
    And I submit the class as "Class 3"

    Given I am logged in as "testuser2"
    When I view the allele with id 848
    Then I should see "Class 3"
    And I should see "PS2"
    And I should see "De novo (confirmed)"
