Feature: View and edit Classification section

  # Note: ACMG covered by acmg.feature, classification by allele_assessment.feature

  Background:
    Given state "test-bdd"

  Scenario: Check Classification section
    Given I am logged in as "testuser1"
    When I view the analysis with id 4
    Then I should see "CLASSIFICATION"
    And I should see "SUGGESTED"

    # Can edit comment fields
    When I start the interpretation
    And I type "Test Evaluation comment" into the editor with xpath "//div[@id='evaluation-comment-editor-content']"
    And I type "Test Report comment" into the editor with xpath "//div[@id='classification-report-comment-editor-content']"
    Then I should see "Test Evaluation comment"
    And I should see "Test Report comment"
