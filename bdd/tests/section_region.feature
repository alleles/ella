Feature: View and edit Region section

  Background:
    Given state "test-bdd"

  Scenario: Check Region section
    Given I am logged in as "testuser1"
    When I view the analysis with id 4
    Then I should see "REGION"
    And I should see "VARDB SNV"

    # Can edit comment field
    When I start the interpretation
    And I type "Test Region comment" into the editor with xpath "//div[@id='region-comment-editor-content']"
    Then I should see "Test Region comment"

    # View nearby classified variants
    ## Classify BRCA2 c.72A>T
    When I select the allele row 3 from the allele table
    And I submit the class as "Class 4"
    ## Check that classified allele is reported for nearby variant BRCA2 c.97G>T
    And I select the allele row 3 from the allele table
    Then I should see "BRCA2 c.72A>T (p.Leu24Phe)"
    And I should see "CLASS 4"
    ## Check that same is visible for other user in overlapping analysis
    Given I am logged in as "testuser2"
    When I view the analysis with id 10
    ## View BRCA2 c.97G>T
    And I select the allele row 0 from the allele table
    When I wait for 1 seconds
    Then I should see "BRCA2 c.72A>T (p.Leu24Phe)"
    And I should see "CLASS 4"
