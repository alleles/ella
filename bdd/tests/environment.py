import os
from typing import Any

from behaving.web import environment as benv
from selenium.webdriver.chrome.options import Options


from personas import PERSONAS

# CI needs more time than local testing
LOCAL_TIMEOUT = 30
CI_TIMEOUT = 60

if os.getenv("CI"):
    WITHIN_TIMEOUT = CI_TIMEOUT
else:
    WITHIN_TIMEOUT = LOCAL_TIMEOUT

chrome_options = Options()


def before_all(context):
    context.remote_webdriver_url = "http://selenium-chrome:4444/wd/hub"
    context.base_url = os.environ.get("SITE_URL", "http://react.nginx:80")
    context.default_browser = os.environ.get("ELLA_DEFAULT_BROWSER", "chrome")
    context.max_browser_attempts = 10
    context.mail_path = "/tmp/mail"
    context.sms_path = "/tmp/sms"
    context.gcm_path = "/tmp/gcm"
    context.screenshots_dir = "/ella/bdd/screenshots"

    browser_args: dict[str, Any] = {
        "wait_time": WITHIN_TIMEOUT,  # for `I should/should not see ...` steps
    }

    # Firefox certs are already handled in behaving
    # ref: https://github.com/ggozad/behaving/commit/71b12e9fa258d002d20c448bf7c0982b585e67c4
    if context.default_browser == "chrome":
        browser_args["options"] = chrome_options
    elif context.default_browser != "firefox":
        raise ValueError(f"Browser not supported: {context.default_browser}")

    # set on context in separate step because typer is dumb
    context.browser_args = browser_args

    # Save the db state to a temp state so that we can load it after the tests
    # from tests.steps.state import save_state

    # save_state(context, "before_tests")
    benv.before_all(context)


def after_all(context):
    # Load the db state we had before the tests
    # from tests.steps.state import load_state

    # load_state(context, "before_tests")
    benv.after_all(context)


def before_feature(context, feature):
    benv.before_feature(context, feature)


def after_feature(context, feature):
    benv.after_feature(context, feature)


def before_scenario(context, scenario):
    benv.before_scenario(context, scenario)
    context.personas = PERSONAS
    context.persona = None


def after_scenario(context, scenario):
    benv.after_scenario(context, scenario)
